package ProductGroup;

import junit.framework.Assert;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.common.Configurable;
import com.netappsid.erp.configurator.gui.Configurator;
import com.netappsid.wadconfigurator.Assembly;
import com.netappsid.wadconfigurator.ProductGroup;
import com.netappsid.wadconfigurator.ProductTemplate;
import com.netappsid.wadconfigurator.enums.ChoiceForm;

/**
 * PRODUCT GROUP TEST for Half arc quarter circle
 * 
 * @author jcfortier
 */
public class Template2HalfArcQuarterCircleTest extends TestUtils
{
	Assembly assembly;

	@BeforeClass
	public static void init() throws Exception
	{}

	@AfterClass
	public static void tearDown()
	{}

	@Before
	public void beforeTest()
	{
		assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		initProperties();

		((ProductGroup) pg).setForm(ChoiceForm.F07_HALFARC_QUARTERCIRCLE);
		resize(30d);

		Assert.assertEquals(30d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(30d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);
	}

	@Test
	/**
	 * Verify the resizing of a Quarter circle
	 */
	public void resizeHalfArcTest()
	{
		ProductGroup pg2 = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, pg.getConfigurables().get(0));
		Assert.assertNotNull(pg2);
		pg2.setForm(ChoiceForm.F07_HALFARC_QUARTERCIRCLE);
		pg.bindConfigurableToTemplate(0, pg2);

		int index = 0;
		for (com.netappsid.erp.configurator.Configurable configurable : pg.getConfigurables())
		{
			ProductTemplate template1 = (ProductTemplate) configurable;
			Assert.assertNotNull(template1);

			Assert.assertEquals(template1.getDimension().getWidth().doubleValue(Units.INCH), pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
			Assert.assertEquals(template1.getDimension().getHeight().doubleValue(Units.INCH), pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);

			Assert.assertEquals(template1.getDimension().getForm().getOrigin().getX(), pg.getConfigurablesPosition().get(index).getX(), DELTA);
			Assert.assertEquals(template1.getDimension().getForm().getOrigin().getY(), pg.getConfigurablesPosition().get(index).getY(), DELTA);

			++index;
		}
	}

	// @Test
	// /**
	// * Verify the resizing of a Quarter circle group
	// */
	// public void resizeGroupHalfArcTest()
	// {
	// resizeHalfArcTest();
	//
	// // -----------------------------------------------------------------------
	// pg.getDimension().setWidth(Units.inch(30d));
	//
	// //printAll();
	//
	// checkSize (0, 6.0, 18.0, 0.0, null);
	// checkPosition(0, 0.0, 12.0, "HalfArc");
	// checkRay(0, 30.0);
	//
	// //TODO : this case is incorrect H2 > H
	// checkSize (1, 24.0, 5.7171431429143205, 6.282856857085701, null);
	// checkPosition(1, 6.0, 0.0, "ElongatedHalfArc");
	// checkRay(1, 509.29);
	// Assert.fail("this case is incorrect H2 > H");
	//
	// checkSize (2, 24.0, 24.282856857085697);
	// checkPosition(2, 6.0, 5.717143142914303, "Rectangle");
	// }

	/**
	 * Split the HalfArc in 3. Verify the result.
	 */
	// private void splitHalfArc()
	// {
	// pg.splitForm(Orientation.VERTICAL, new Point2D.Double(10d,10d));
	// pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(15d,10d));
	// Assert.assertEquals(3, pg.getConfigurables().size());
	//
	// //printAll();
	//
	// verifyInitial();
	// }

	// private void verifyInitial()
	// {
	// checkSize (0, 10.0, 17.320508075688775, 0.0, null);
	// checkPosition(0, 0.0, 2.6794919243112254, "HalfArc");
	// checkRay(0, 20.0);
	//
	// checkSize (1, 10.0, 10.0, 7.320508075688775, null);
	// checkPosition(1, 10.0, 0.0, "ElongatedHalfArc");
	// checkRay(1, 20.0);
	//
	// checkSize (2, 10.0, 10.0);
	// checkPosition(2, 10.0, 10.0, "Rectangle");
	// }

	/**
	 * Resize the ProductGroup with the specified value.
	 * 
	 * @param size
	 */
	protected void resize(Double size)
	{
		pg.getDimension().setDefaultWidth(Units.inch(size));
		pg.getDimension().setDefaultHeight(Units.inch(size));
	}

	private void verify()
	{
		int index = 0;
		for (com.netappsid.erp.configurator.Configurable configurable : pg.getConfigurables())
		{
			ProductTemplate template1 = (ProductTemplate) configurable;
			Assert.assertNotNull(template1);

			ProductTemplate template2 = template1.getTemplate2();
			Assert.assertNotNull(template2);

			Assert.assertEquals(template1.getDimension().getWidth().doubleValue(Units.INCH), template1.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
			Assert.assertEquals(template1.getDimension().getHeight().doubleValue(Units.INCH), template1.getDimension().getHeight().doubleValue(Units.INCH),
					DELTA);

			Assert.assertEquals(template2.getDimension().getForm().getOrigin().getX(), pg.getConfigurablesPosition().get(index).getX(), DELTA);
			Assert.assertEquals(template2.getDimension().getForm().getOrigin().getY(), pg.getConfigurablesPosition().get(index).getY(), DELTA);

			++index;
		} /* end for */
	}

}
