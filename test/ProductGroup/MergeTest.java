package ProductGroup;

import java.awt.geom.Point2D;

import junit.framework.Assert;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.netappsid.commonutils.geo.enums.Orientation;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.common.Configurable;
import com.netappsid.erp.configurator.gui.Configurator;
import com.netappsid.wadconfigurator.Assembly;
import com.netappsid.wadconfigurator.ProductGroup;
import com.netappsid.wadconfigurator.ProductTemplate;
import com.netappsid.wadconfigurator.enums.ChoiceForm;
import com.netappsid.wadconfigurator.productGroup.PGTemplateMerger;

/**
 * PRODUCT GROUP TEST for template merging
 * 
 * @author sboule
 */
public class MergeTest extends TestUtils
{
	Assembly assembly;

	@BeforeClass
	public static void init() throws Exception
	{}

	@AfterClass
	public static void tearDown()
	{}

	@Test
	/**
	 * Verify valid rectangle mergings
	 */
	public void rectangleValidMergingTest()
	{
		assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		initProperties();

		((ProductGroup) pg).setForm(ChoiceForm.F02_RECTANGLE);
		pg.getDimension().setDefaultWidth(Units.inch(100d));
		pg.getDimension().setDefaultHeight(Units.inch(100d));

		Assert.assertEquals(100d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(100d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);

		splitRectangle();

		// Merge

		// -----------------------------------------------------------
		Assert.assertTrue(PGTemplateMerger.mergeProductTemplates(pg, 0, 1));

		checkSize(0, 66.66667, 50.0, null, null);
		checkPosition(0, 0.0, 0.0, "Rectangle");
		checkSize(1, 33.333330000000004, 50.0, null, null);
		checkPosition(1, 66.66667, 0.0, "Rectangle");
		checkSize(2, 33.33333, 50.0, null, null);
		checkPosition(2, 0.0, 50.0, "Rectangle");
		checkSize(3, 33.33334, 50.0, null, null);
		checkPosition(3, 33.33333, 50.0, "Rectangle");
		checkSize(4, 33.333330000000004, 50.0, null, null);
		checkPosition(4, 66.66667, 50.0, "Rectangle");

		// -----------------------------------------------------------
		((ProductTemplate) pg.getConfigurables().get(4)).getDimension().setHeight(Units.inch(60d));
		Assert.assertTrue(PGTemplateMerger.mergeProductTemplates(pg, 4, 1));

		checkSize(0, 66.66667, 40.0, null, null);
		checkPosition(0, 0.0, 0.0, "Rectangle");
		checkSize(1, 33.333330000000004, 100.0, null, null);
		checkPosition(1, 66.66667, 0.0, "Rectangle");
		checkSize(2, 33.33333, 60.00000000000001, null, null);
		checkPosition(2, 0.0, 40.0, "Rectangle");
		checkSize(3, 33.33334, 60.00000000000001, null, null);
		checkPosition(3, 33.33333, 40.0, "Rectangle");

		// -----------------------------------------------------------
		((ProductTemplate) pg.getConfigurables().get(2)).getDimension().setHeight(Units.inch(50d));
		((ProductTemplate) pg.getConfigurables().get(3)).getDimension().setHeight(Units.inch(50d));
		Assert.assertTrue(PGTemplateMerger.mergeProductTemplates(pg, 2, 3));

		checkSize(0, 66.66667, 50.0, null, null);
		checkPosition(0, 0.0, 0.0, "Rectangle");
		checkSize(1, 33.333330000000004, 100.0, null, null);
		checkPosition(1, 66.66667, 0.0, "Rectangle");
		checkSize(2, 66.66667, 50.0, null, null);
		checkPosition(2, 0.0, 50.0, "Rectangle");

		// -----------------------------------------------------------
		Assert.assertTrue(PGTemplateMerger.mergeProductTemplates(pg, 2, 0));

		checkSize(0, 66.66667, 100.0, null, null);
		checkPosition(0, 0.0, 0.0, "Rectangle");
		checkSize(1, 33.333330000000004, 100.0, null, null);
		checkPosition(1, 66.66667, 0.0, "Rectangle");

		// -----------------------------------------------------------
		Assert.assertTrue(PGTemplateMerger.mergeProductTemplates(pg, 1, 0));

		checkSize(0, 100.0, 100.0, null, null);
		checkPosition(0, 0.0, 0.0, "Rectangle");
	}

	@Test
	/**
	 * Verify invalid rectangle mergings
	 */
	public void rectangleInvalidMergingTest()
	{
		assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		initProperties();

		((ProductGroup) pg).setForm(ChoiceForm.F02_RECTANGLE);
		pg.getDimension().setDefaultWidth(Units.inch(100d));
		pg.getDimension().setDefaultHeight(Units.inch(100d));

		Assert.assertEquals(100d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(100d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);

		splitRectangle();

		// Invalid Merges

		// -----------------------------------------------------------
		Assert.assertFalse(PGTemplateMerger.mergeProductTemplates(pg, 0, 2));
		verifyRectangle();

		// -----------------------------------------------------------
		Assert.assertFalse(PGTemplateMerger.mergeProductTemplates(pg, 0, 4));
		verifyRectangle();

		// -----------------------------------------------------------
		Assert.assertFalse(PGTemplateMerger.mergeProductTemplates(pg, 1, 3));
		verifyRectangle();

		// -----------------------------------------------------------
		Assert.assertFalse(PGTemplateMerger.mergeProductTemplates(pg, 1, 5));
		verifyRectangle();

		// -----------------------------------------------------------
		Assert.assertTrue(PGTemplateMerger.mergeProductTemplates(pg, 1, 4));

		checkSize(0, 33.33333, 50.0, null, null);
		checkPosition(0, 0.0, 0.0, "Rectangle");
		checkSize(1, 33.33334, 100.0, null, null);
		checkPosition(1, 33.33333, 0.0, "Rectangle");
		checkSize(2, 33.333330000000004, 50.0, null, null);
		checkPosition(2, 66.66667, 0.0, "Rectangle");
		checkSize(3, 33.33333, 50.0, null, null);
		checkPosition(3, 0.0, 50.0, "Rectangle");
		checkSize(4, 33.333330000000004, 50.0, null, null);
		checkPosition(4, 66.66667, 50.0, "Rectangle");

		// -----------------------------------------------------------
		Assert.assertFalse(PGTemplateMerger.mergeProductTemplates(pg, 1, 4));

		checkSize(0, 33.33333, 50.0, null, null);
		checkPosition(0, 0.0, 0.0, "Rectangle");
		checkSize(1, 33.33334, 100.0, null, null);
		checkPosition(1, 33.33333, 0.0, "Rectangle");
		checkSize(2, 33.333330000000004, 50.0, null, null);
		checkPosition(2, 66.66667, 0.0, "Rectangle");
		checkSize(3, 33.33333, 50.0, null, null);
		checkPosition(3, 0.0, 50.0, "Rectangle");
		checkSize(4, 33.333330000000004, 50.0, null, null);
		checkPosition(4, 66.66667, 50.0, "Rectangle");

		// -----------------------------------------------------------
		Assert.assertFalse(PGTemplateMerger.mergeProductTemplates(pg, 1, 4));

		checkSize(0, 33.33333, 50.0, null, null);
		checkPosition(0, 0.0, 0.0, "Rectangle");
		checkSize(1, 33.33334, 100.0, null, null);
		checkPosition(1, 33.33333, 0.0, "Rectangle");
		checkSize(2, 33.333330000000004, 50.0, null, null);
		checkPosition(2, 66.66667, 0.0, "Rectangle");
		checkSize(3, 33.33333, 50.0, null, null);
		checkPosition(3, 0.0, 50.0, "Rectangle");
		checkSize(4, 33.333330000000004, 50.0, null, null);
		checkPosition(4, 66.66667, 50.0, "Rectangle");
	}

	@Test
	/**
	 * Verify elongated arc mergings
	 */
	public void elongatedArcMergingTest()
	{
		assembly = (Assembly) Configurator.getConfigurableByName("F_EA", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		initProperties();

		((ProductGroup) pg).setForm(ChoiceForm.F05_ELONGATEDARC);
		pg.getDimension().setDefaultWidth(Units.inch(120d));
		pg.getDimension().setDefaultHeight(Units.inch(120d));
		pg.getDimension().setDefaultHeight2(Units.inch(75d));

		Assert.assertEquals(120d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(120d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(75d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight2().doubleValue(Units.INCH), DELTA);
		Assert.assertTrue(((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getForm().isDefined());

		splitElongatedArc();

		// Merge

		// -----------------------------------------------------------
		Assert.assertTrue(PGTemplateMerger.mergeProductTemplates(pg, 0, 1));

		checkSize(0, 80.0, 120.0, 75.0, null);
		checkPosition(0, 0.0, 0.0, "ElongatedIrregularArc");
		checkSize(1, 40.0, 71.71359641163508, 30.0, null);
		checkPosition(1, 80.0, 3.2864035883649194, "ElongatedHalfArc");
		checkSize(2, 40.0, 45.0, null, null);
		checkPosition(2, 80.0, 75.0, "Rectangle");

		// -----------------------------------------------------------
		Assert.assertFalse(PGTemplateMerger.mergeProductTemplates(pg, 0, 1));

		checkSize(0, 80.0, 120.0, 75.0, null);
		checkPosition(0, 0.0, 0.0, "ElongatedIrregularArc");
		checkSize(1, 40.0, 71.71359641163508, 30.0, null);
		checkPosition(1, 80.0, 3.2864035883649194, "ElongatedHalfArc");
		checkSize(2, 40.0, 45.0, null, null);
		checkPosition(2, 80.0, 75.0, "Rectangle");

		// -----------------------------------------------------------
		Assert.assertTrue(PGTemplateMerger.mergeProductTemplates(pg, 1, 2));

		checkSize(0, 80.0, 120.0, 75.0, null);
		checkPosition(0, 0.0, 0.0, "ElongatedIrregularArc");
		checkSize(1, 40.0, 116.71359641163508, 75.0, null);
		checkPosition(1, 80.0, 3.2864035883649123, "ElongatedHalfArc");

		// -----------------------------------------------------------
		Assert.assertTrue(PGTemplateMerger.mergeProductTemplates(pg, 0, 1));
		checkSize(0, 120.0, 120.0, 75.0, null);
		checkPosition(0, 0.0, 0.0, "ElongatedArc");

		// H2 split & merge
		// -----------------------------------------------------------
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(60d, 20d));
		checkSize(0, 120.0, 45.0, null, null);
		checkPosition(0, 0.0, 0.0, "Arc");
		checkSize(1, 120.0, 75.0, null, null);
		checkPosition(1, 0.0, 45.0, "Rectangle");

		Assert.assertTrue(PGTemplateMerger.mergeProductTemplates(pg, 0, 1));
		checkSize(0, 120.0, 120.0, 75.0, null);
		checkPosition(0, 0.0, 0.0, "ElongatedArc");
	}

	@Test
	/**
	 * Verify dog house mergings
	 */
	public void dogHouseMergingTest()
	{
		assembly = (Assembly) Configurator.getConfigurableByName("F_DH", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		initProperties();

		((ProductGroup) pg).setForm(ChoiceForm.F10_DOGHOUSE_SYMETRIC);
		pg.getDimension().setDefaultWidth(Units.inch(120d));
		pg.getDimension().setDefaultHeight(Units.inch(120d));
		pg.getDimension().setDefaultHeight2(Units.inch(75d));

		Assert.assertEquals(120d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(120d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(75d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight2().doubleValue(Units.INCH), DELTA);
		Assert.assertTrue(((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getForm().isDefined());

		splitDogHouse();

		// Merge

		// -----------------------------------------------------------
		Assert.assertFalse(PGTemplateMerger.mergeProductTemplates(pg, 0, 1));
		verifyDogHouse();

		// -----------------------------------------------------------
		Assert.assertFalse(PGTemplateMerger.mergeProductTemplates(pg, 2, 1));
		verifyDogHouse();

		// -----------------------------------------------------------
		Assert.assertFalse(PGTemplateMerger.mergeProductTemplates(pg, 3, 4));
		verifyDogHouse();

		// -----------------------------------------------------------
		Assert.assertTrue(PGTemplateMerger.mergeProductTemplates(pg, 3, 1));

		checkSize(0, 40.0, 60.0, 30.0, null);
		checkPosition(0, 0.0, 15.0, "Trapeze");
		checkSize(1, 40.0, 75.0, 60.0, 20.0);
		checkPosition(1, 40.0, 0.0, "DogHouse");
		checkSize(2, 40.0, 60.0, 30.0, null);
		checkPosition(2, 80.0, 15.0, "Trapeze");
		checkSize(3, 120.0, 45.0, null, null);
		checkPosition(3, 0.0, 75.0, "Rectangle");

		// -----------------------------------------------------------
		Assert.assertTrue(PGTemplateMerger.mergeProductTemplates(pg, 2, 1));

		checkSize(0, 40.0, 60.0, 30.0, null);
		checkPosition(0, 0.0, 15.0, "Trapeze");
		checkSize(1, 80.0, 75.0, 60.0, 20.0);
		checkPosition(1, 40.0, 0.0, "DogHouse");
		checkSize(2, 120.0, 45.0, null, null);
		checkPosition(2, 0.0, 75.0, "Rectangle");

		// -----------------------------------------------------------
		Assert.assertTrue(PGTemplateMerger.mergeProductTemplates(pg, 0, 1));

		checkSize(0, 120.0, 75.0, 30.0, 60.0);
		checkPosition(0, 0.0, 0.0, "DogHouse");
		checkSize(1, 120.0, 45.0, null, null);
		checkPosition(1, 0.0, 75.0, "Rectangle");

		// -----------------------------------------------------------
		Assert.assertTrue(PGTemplateMerger.mergeProductTemplates(pg, 0, 1));

		checkSize(0, 120.0, 120.0, 75.0, 60.0);
		checkPosition(0, 0.0, 0.0, "DogHouse");

		// H2 split & merge
		// -----------------------------------------------------------
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(60d, 20d));
		checkSize(0, 120.0, 45.0, null, null);
		checkPosition(0, 0.0, 0.0, "Triangle");
		checkSize(1, 120.0, 75.0, null, null);
		checkPosition(1, 0.0, 45.0, "Rectangle");

		Assert.assertTrue(PGTemplateMerger.mergeProductTemplates(pg, 0, 1));
		checkSize(0, 120.0, 120.0, 75.0, 60.0);
		checkPosition(0, 0.0, 0.0, "DogHouse");
	}

	/**
	 * Split the rectangle in 6.
	 */
	private void splitRectangle()
	{
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(50d, 50d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(33.33333d, 25d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(66.66667d, 25d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(33.33333d, 75d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(66.66667d, 75d));
		Assert.assertEquals(6, pg.getConfigurables().size());

		verifyRectangle();
	}

	/**
	 * Verify the initial rectangle.
	 */
	private void verifyRectangle()
	{
		checkSize(0, 33.33333, 50.0, null, null);
		checkPosition(0, 0.0, 0.0, "Rectangle");
		checkSize(1, 33.33334, 50.0, null, null);
		checkPosition(1, 33.33333, 0.0, "Rectangle");
		checkSize(2, 33.333330000000004, 50.0, null, null);
		checkPosition(2, 66.66667, 0.0, "Rectangle");
		checkSize(3, 33.33333, 50.0, null, null);
		checkPosition(3, 0.0, 50.0, "Rectangle");
		checkSize(4, 33.33334, 50.0, null, null);
		checkPosition(4, 33.33333, 50.0, "Rectangle");
		checkSize(5, 33.333330000000004, 50.0, null, null);
		checkPosition(5, 66.66667, 50.0, "Rectangle");
	}

	/**
	 * Split the elongated arc in 4.
	 */
	private void splitElongatedArc()
	{
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(40d, 75d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(80d, 75d));
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(100d, 75d));
		Assert.assertEquals(4, pg.getConfigurables().size());

		verifyElongatedArc();
	}

	/**
	 * Verify the initial elongated arc.
	 */
	private void verifyElongatedArc()
	{
		checkSize(0, 40.0, 116.71359641163507, 75.0, null);
		checkPosition(0, 0.0, 3.2864035883649336, "ElongatedHalfArc");
		checkSize(1, 40.0, 120.00000000000004, 116.71359641163507, null);
		checkPosition(1, 40.0, 0.0, "ElongatedArc");
		checkSize(2, 40.0, 71.71359641163511, 30.0, null);
		checkPosition(2, 80.0, 3.286403588364891, "ElongatedHalfArc");
		checkSize(3, 40.0, 45.0, null, null);
		checkPosition(3, 80.0, 75.0, "Rectangle");
	}

	/**
	 * Split the dog house in 5.
	 */
	private void splitDogHouse()
	{
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(60d, 75d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(40d, 60d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(80d, 60d));
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(60d, 55d));
		Assert.assertEquals(5, pg.getConfigurables().size());

		verifyDogHouse();
	}

	/**
	 * Verify the initial dog house.
	 */
	private void verifyDogHouse()
	{
		checkSize(0, 40.0, 60.0, 30.0, null);
		checkPosition(0, 0.0, 15.0, "Trapeze");
		checkSize(1, 40.0, 55.0, 40.0, 20.0);
		checkPosition(1, 40.0, 0.0, "DogHouse");
		checkSize(2, 40.0, 60.0, 30.0, null);
		checkPosition(2, 80.0, 15.0, "Trapeze");
		checkSize(3, 40.0, 20.0, null, null);
		checkPosition(3, 40.0, 55.0, "Rectangle");
		checkSize(4, 120.0, 45.0, null, null);
		checkPosition(4, 0.0, 75.0, "Rectangle");
	}

}
