package ProductGroup;

import java.awt.geom.Point2D;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.netappsid.commonutils.geo.enums.Orientation;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.common.Configurable;
import com.netappsid.erp.configurator.gui.Configurator;
import com.netappsid.wadconfigurator.Assembly;
import com.netappsid.wadconfigurator.ProductGroup;
import com.netappsid.wadconfigurator.ProductTemplate;
import com.netappsid.wadconfigurator.enums.ChoiceForm;

/**
 * PRODUCT GROUP TEST for Triangle rectangle
 * 
 * @author jcfortier
 */
public class NoFlipTriangleRectangleTest extends TestUtils
{
	@Before
	public void init() throws Exception
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_TR", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		initProperties();

		pg.setForm(ChoiceForm.F13_TRIANGLE_RECTANGLE);
		pg.getDimension().setDefaultWidth(Units.inch(20d));
		pg.getDimension().setDefaultHeight(Units.inch(20d));

		checkSize(0, 20d, 20d, null, 20d);
		checkPosition(0, 0d, 0d, "Triangle");
		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);
	}

	@Test
	/**
	 * Verify the split of a Triangle
	 */
	public void splitTriangleVerticalTest()
	{
		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(10d, 4d));
		Assert.assertEquals(2, pg.getConfigurables().size());

		checkSize(0, 10.0, 10.0, 0.0, 10.0);
		checkPosition(0, 0.0, 10.0, "Triangle");

		checkSize(1, 10.0, 20.0, 10.0, 10.0);
		checkPosition(1, 10.0, 0.0, "Trapeze");

		// Assert.assertEquals(TriangleFormType.RECTANGLE,
		// ((Triangle)((ProductTemplate)pg.getConfigurables().get(0)).getDimension().getForm()).getTriangleFormType());

		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(15d, 4d));
		Assert.assertEquals(3, pg.getConfigurables().size());

		checkSize(0, 10.0, 10.0, 0.0, 10.0);
		checkPosition(0, 0.0, 10.0, "Triangle");

		checkSize(1, 5.0, 15.0, 10.0, 5.0);
		checkPosition(1, 10.0, 5.0, "Trapeze");

		checkSize(2, 5.0, 20.0, 15.0, 5.0);
		checkPosition(2, 15.0, 0.0, "Trapeze");

		// Assert.assertEquals(TriangleFormType.RECTANGLE,
		// ((Triangle)((ProductTemplate)pg.getConfigurables().get(0)).getDimension().getForm()).getTriangleFormType());
	}

	@Test
	/**
	 * Verify the resizing of a Triangle
	 */
	public void resizeTriangleHorizontalTest()
	{
		splitTriangleVertical();

		// -----------------------------------------------------------------------
		((ProductTemplate) pg.getConfigurables().get(2)).getDimension().setWidth(Units.inch(6d));

		checkSize(0, 10.0, 10.0, 0.0, 10.0);
		checkPosition(0, 0.0, 10.0, "Triangle");

		checkSize(1, 4.0, 14.0, 10.0, 4.0);
		checkPosition(1, 10.0, 6.0, "Trapeze");

		checkSize(2, 6.0, 20.0, 14.0, 6.0);
		checkPosition(2, 14.0, 0.0, "Trapeze");

		// -----------------------------------------------------------------------
		((ProductTemplate) pg.getConfigurables().get(2)).getDimension().removeWidth();
		verifyInitialVertical();
	}

	@Test
	/**
	 * Verify the resizing of a Triangle group
	 */
	public void resizeGroupTriangleHorizontalTest()
	{
		splitTriangleVertical();

		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(18d));

		checkSize(0, 9.0, 10.0, 0.0, 9.0);
		checkPosition(0, 0.0, 10.0, "Triangle");

		checkSize(1, 4.5, 15.0, 10.0, 4.5);
		checkPosition(1, 9.0, 5.0, "Trapeze");

		checkSize(2, 4.5, 20.0, 15.0, 4.5);
		checkPosition(2, 13.5, 0.0, "Trapeze");

		pg.getDimension().removeWidth();
		verifyInitialVertical();

		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(22d));

		checkSize(0, 11.0, 10.0, 0.0, 11.0);
		checkPosition(0, 0.0, 10.0, "Triangle");

		checkSize(1, 5.5, 15.0, 10.0, 5.5);
		checkPosition(1, 11.0, 5.0, "Trapeze");

		checkSize(2, 5.5, 20.0, 15.0, 5.5);
		checkPosition(2, 16.5, 0.0, "Trapeze");

		pg.getDimension().removeWidth();
		verifyInitialVertical();

		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(18d));

		checkSize(0, 10.0, 9.0, 0.0, 10.0);
		checkPosition(0, 0.0, 9.0, "Triangle");

		checkSize(1, 5.0, 13.5, 9.0, 5.0);
		checkPosition(1, 10.0, 4.5, "Trapeze");

		checkSize(2, 5.0, 18.0, 13.5, 5.0);
		checkPosition(2, 15.0, 0.0, "Trapeze");

		pg.getDimension().removeHeight();
		verifyInitialVertical();

		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(22d));

		checkSize(0, 10.0, 11.0, 0.0, 10.0);
		checkPosition(0, 0.0, 11.0, "Triangle");

		checkSize(1, 5.0, 16.5, 11.0, 5.0);
		checkPosition(1, 10.0, 5.5, "Trapeze");

		checkSize(2, 5.0, 22.0, 16.5, 5.0);
		checkPosition(2, 15.0, 0.0, "Trapeze");

		pg.getDimension().removeHeight();
		verifyInitialVertical();
	}

	@Test
	/**
	 * Verify the resizing of a Triangle group
	 */
	public void resizeGroupTriangleTest2()
	{
		pg.getDimension().setDefaultWidth(Units.inch(100d));
		pg.getDimension().setDefaultHeight(Units.inch(100d));

		Assert.assertEquals(100d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(100d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);

		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(50d, 80d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(75d, 80d));
		Assert.assertEquals(3, pg.getConfigurables().size());

		pg.getDimension().setDefaultWidth(Units.inch(20d));
		pg.getDimension().setDefaultHeight(Units.inch(20d));
		pg.getDimension().setHeight(Units.inch(20d));

		verifyInitialVertical();
	}

	@Test
	@Ignore
	/**
	 * Verify the split of a Triangle horizontally
	 * It is not possible for now to split a triangle rectangle horizontally
	 */
	public void splitTriangleHorizontalTest()
	{
		splitTriangleHorizontal();
	}

	/**
	 * Split the Triangle in 3. Verify the result.
	 */
	private void splitTriangleVertical()
	{
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(10d, 4d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(15d, 4d));
		Assert.assertEquals(3, pg.getConfigurables().size());

		verifyInitialVertical();
	}

	private void verifyInitialVertical()
	{
		checkSize(0, 10.0, 20.0, 0.0, 10.0);
		checkPosition(0, 0.0, 0.0, "Triangle");

		checkSize(1, 5.0, 15.0, 10.0, 5.0);
		checkPosition(1, 10.0, 5.0, "Trapeze");

		checkSize(2, 5.0, 20.0, 15.0, 5.0);
		checkPosition(2, 15.0, 0.0, "Trapeze");

		// Assert.assertEquals(TriangleFormType.RECTANGLE,
		// ((Triangle)((ProductTemplate)pg.getConfigurables().get(0)).getDimension().getForm()).getTriangleFormType());
	}

	/**
	 * Split the Triangle in 3. Verify the result.
	 */
	private void splitTriangleHorizontal()
	{
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(15d, 8d));
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(15d, 16d));
		Assert.assertEquals(3, pg.getConfigurables().size());

		verifyInitialHorizontal();
	}

	private void verifyInitialHorizontal()
	{
		printAll();
		// checkSize (0, 10.0, 10.0, 0.0, 10.0);
		// checkPosition(0, 0.0, 10.0, "Triangle");
		//
		// checkSize (1, 5.0, 15.0, 10.0, 5.0);
		// checkPosition(1, 10.0, 5.0, "Trapeze");
		//
		// checkSize (2, 5.0, 20.0, 15.0, 5.0);
		// checkPosition(2, 15.0, 0.0, "Trapeze");

		// Assert.assertEquals(TriangleFormType.RECTANGLE,
		// ((Triangle)((ProductTemplate)pg.getConfigurables().get(0)).getDimension().getForm()).getTriangleFormType());
	}
}
