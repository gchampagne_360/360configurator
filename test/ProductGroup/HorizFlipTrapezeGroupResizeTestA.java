package ProductGroup;

import java.awt.geom.Point2D;

import junit.framework.Assert;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.netappsid.commonutils.geo.enums.Orientation;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.common.Configurable;
import com.netappsid.configuration.common.Dimensionnable;
import com.netappsid.configuration.common.enums.ChoiceFlip;
import com.netappsid.erp.configurator.gui.Configurator;
import com.netappsid.wadconfigurator.Assembly;
import com.netappsid.wadconfigurator.Dimension;
import com.netappsid.wadconfigurator.ProductGroup;
import com.netappsid.wadconfigurator.enums.ChoiceForm;

/**
 * PRODUCT GROUP TEST for group resize trapezes (overall resizes) Scenario A
 * 
 * @author sboule
 */
public class HorizFlipTrapezeGroupResizeTestA extends TestUtils
{
	private static double SIZE_OVERALL = 18d;

	@BeforeClass
	public static void init() throws Exception
	{}

	@AfterClass
	public static void tearDown()
	{}

	@Test
	/**
	 * Verify the height increase
	 */
	public void heightIncreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);
		pg.getDimension().setChoiceFlip(ChoiceFlip.FLIPHORIZONTAL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double height = 19d;
		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(height));
		Assert.assertEquals(height, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);

		checkSize(0, 6.0, 19.0, 16.0, null);
		checkPosition(0, 0.0, 0.0, "Trapeze");
		checkSize(1, 6.0, 16.0, 13.0, null);
		checkPosition(1, 6.0, 3.0, "Trapeze");
		checkSize(2, 6.0, 13.0, 10.0, null);
		checkPosition(2, 12.0, 6.0, "Trapeze");

		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT);
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the height decrease
	 */
	public void heightDecreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);
		pg.getDimension().setChoiceFlip(ChoiceFlip.FLIPHORIZONTAL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double height = 17d;
		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(height));
		Assert.assertEquals(height, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);

		checkSize(0, 6.0, 17.0, 14.0, null);
		checkPosition(0, 0.0, 0.0, "Trapeze");
		checkSize(1, 6.0, 14.0, 11.0, null);
		checkPosition(1, 6.0, 3.0, "Trapeze");
		checkSize(2, 6.0, 11.0, 8.0, null);
		checkPosition(2, 12.0, 6.0, "Trapeze");

		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT);
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the height2 increase
	 */
	public void height2IncreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);
		pg.getDimension().setChoiceFlip(ChoiceFlip.FLIPHORIZONTAL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double height2 = 12d;
		// -----------------------------------------------------------------------
		pg.getDimension().setHeight2(Units.inch(height2));
		Assert.assertEquals(height2, pg.getDimension().getHeight2().doubleValue(Units.INCH), DELTA);

		checkSize(0, 6d, 18d, 16d);
		checkSize(1, 6d, 16d, 14d);
		checkSize(2, 6d, 14d, 12d);

		checkPosition(0, 0d, 0d, "Trapeze");
		checkPosition(1, 6d, 2d, "Trapeze");
		checkPosition(2, 12d, 4d, "Trapeze");
		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT2);
		Assert.assertEquals(SIZE_OVERALL / 2d, pg.getDimension().getHeight2().doubleValue(Units.INCH), DELTA);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the height2 decrease
	 */
	public void height2DecreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);
		pg.getDimension().setChoiceFlip(ChoiceFlip.FLIPHORIZONTAL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double height2 = 8d;
		// -----------------------------------------------------------------------
		pg.getDimension().setHeight2(Units.inch(height2));
		Assert.assertEquals(height2, pg.getDimension().getHeight2().doubleValue(Units.INCH), DELTA);

		checkSize(0, 6d, 18d, 14.66667d);
		checkSize(1, 6d, 14.66667d, 11.33333d);
		checkSize(2, 6d, 11.33333d, 8d);

		checkPosition(0, 0d, 0d, "Trapeze");
		checkPosition(1, 6d, 3.33333d, "Trapeze");
		checkPosition(2, 12d, 6.66667d, "Trapeze");
		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT2);
		Assert.assertEquals(SIZE_OVERALL / 2d, pg.getDimension().getHeight2().doubleValue(Units.INCH), DELTA);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the width increase
	 */
	public void widthIncreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);
		pg.getDimension().setChoiceFlip(ChoiceFlip.FLIPHORIZONTAL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double width = 21d;
		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(width));
		Assert.assertEquals(width, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		checkSize(0, 7.0, 18.0, 14.0, null);
		checkPosition(0, 0.0, 0.0, "Trapeze");
		checkSize(1, 7.0, 14.0, 10.0, null);
		checkPosition(1, 7.0, 4.0, "Trapeze");
		checkSize(2, 7.0, 10.0, 6.0, null);
		checkPosition(2, 14.0, 8.0, "Trapeze");

		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the width decrease
	 */
	public void widthDecreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);
		pg.getDimension().setChoiceFlip(ChoiceFlip.FLIPHORIZONTAL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double width = 15d;
		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(width));
		Assert.assertEquals(width, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		checkSize(0, 5.0, 18.0, 16.0, null);
		checkPosition(0, 0.0, 0.0, "Trapeze");
		checkSize(1, 5.0, 16.0, 14.0, null);
		checkPosition(1, 5.0, 2.0, "Trapeze");
		checkSize(2, 5.0, 14.0, 12.0, null);
		checkPosition(2, 10.0, 4.0, "Trapeze");

		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the height increase with override
	 * Cannot override the trapeze height...
	 */
	public void heightIncreaseWithOverrideTest()
	{
		Assert.assertTrue(true);
	}

	@Test
	/**
	 * Verify the height decrease with override
	 * Cannot override the trapeze height...
	 */
	public void heightDecreaseWithOverrideTest()
	{
		Assert.assertTrue(true);
	}

	@Test
	/**
	 * Verify the width increase with override
	 */
	public void widthIncreaseWithOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);
		pg.getDimension().setChoiceFlip(ChoiceFlip.FLIPHORIZONTAL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double width = 20d;
		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().setWidth(Units.inch(4d));
		checkSize(0, 7d, 18d, 14.5d);
		checkSize(1, 4d, 14.5d, 12.5d);
		checkSize(2, 7d, 12.5d, 9d);

		pg.getDimension().setWidth(Units.inch(width));
		Assert.assertEquals(width, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		checkSize(0, 8.0, 18.0, 13.6, null);
		checkPosition(0, 0.0, 0.0, "Trapeze");
		checkSize(1, 4.0, 13.6, 11.4, null);
		checkPosition(1, 8.0, 4.4, "Trapeze");
		checkSize(2, 8.0, 11.4, 7.0, null);
		checkPosition(2, 12.0, 6.6, "Trapeze");

		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the width decrease with override
	 */
	public void widthDecreaseWithOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);
		pg.getDimension().setChoiceFlip(ChoiceFlip.FLIPHORIZONTAL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double width = 16d;
		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().setWidth(Units.inch(4d));
		checkSize(0, 7d, 18d, 14.5d);
		checkSize(1, 4d, 14.5d, 12.5d);
		checkSize(2, 7d, 12.5d, 9d);

		pg.getDimension().setWidth(Units.inch(width));
		Assert.assertEquals(width, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		checkSize(0, 6.0, 18.0, 15.375, null);
		checkPosition(0, 0.0, 0.0, "Trapeze");
		checkSize(1, 4.0, 15.375, 13.625, null);
		checkPosition(1, 6.0, 2.625, "Trapeze");
		checkSize(2, 6.0, 13.625, 11.0, null);
		checkPosition(2, 10.0, 4.375, "Trapeze");

		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the height change to the minimum
	 */
	public void heightChangeMinTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);
		pg.getDimension().setChoiceFlip(ChoiceFlip.FLIPHORIZONTAL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double height = 1d;
		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(height));
		// printAll();
		// FIXME: This test fails
		// Assert.assertEquals(10d, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		// checkSize (0, 6.0, 10.0, 9.6667, null);
		// checkPosition(0, 0.0, 0.0, "Trapeze");
		// checkSize (1, 6.0, 9.66667, 9.33333, null);
		// checkPosition(1, 6.0, 0.33333, "Trapeze");
		// checkSize (2, 6.0, 9.33333, 9.0, null);
		// checkPosition(2, 12.0, 0.66667, "Trapeze");
		//
		// pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT);
		// Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		// pg.getDimension().setDefaultHeight2(Units.inch(SIZE_OVERALL/2d));
		// verifyInitial();
	}

	@Test
	/**
	 * Verify the width change to the minimum
	 */
	public void widthChangeMinTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);
		pg.getDimension().setChoiceFlip(ChoiceFlip.FLIPHORIZONTAL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double width = 1d;
		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(width));
		Assert.assertEquals(3d, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		checkSize(0, 1.0, 18.0, 17.666666666666668, null);
		checkPosition(0, 0.0, 0.0, "Trapeze");
		checkSize(1, 1.0, 17.666666666666668, 17.333333333333332, null);
		checkPosition(1, 1.0, 0.3333333333333333, "Trapeze");
		checkSize(2, 1.0, 17.333333333333332, 17.0, null);
		checkPosition(2, 2.0, 0.6666666666666666, "Trapeze");

		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		checkSize(0, 6.0, 18.0, 12.666666666666668, null);
		checkPosition(0, 0.0, 0.0, "Trapeze");
		checkSize(1, 6.0, 12.666666666666668, 7.333333333333334, null);
		checkPosition(1, 6.0, 5.333333333333333, "Trapeze");
		checkSize(2, 6.0, 7.333333333333334, 2.0, null);
		checkPosition(2, 12.0, 10.666666666666666, "Trapeze");
	}

	/**
	 * Split a trapeze into 3 pieces:
	 * 
	 * | | | | | 0 | | | | | | 1 | | | | | 2 | |----------------------------|
	 */
	private void splitTrapeze()
	{
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(6d, 7d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(12d, 7d));
		Assert.assertEquals(3, pg.getConfigurables().size());

		initProperties();
		verifyInitial();
	}

	/**
	 * Verify that all the components have the initial sizes.
	 */
	private void verifyInitial()
	{
		checkSize(0, 6d, 18d, 15d);
		checkSize(1, 6d, 15d, 12d);
		checkSize(2, 6d, 12d, 9d);

		checkPosition(0, 0d, 0d, "Trapeze");
		checkPosition(1, 6d, 3d, "Trapeze");
		checkPosition(2, 12d, 6d, "Trapeze");
	}
}
