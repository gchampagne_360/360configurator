package ProductGroup;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

import java.awt.geom.Point2D;

import org.hamcrest.number.IsCloseTo;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.client360.configuration.wad.ProductGroup;
import com.client360.configuration.wad.ProductTemplate;
import com.netappsid.commonutils.geo.enums.Orientation;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.common.Configurable;
import com.netappsid.configuration.common.Dimensionnable;
import com.netappsid.configuration.common.enums.ChoiceFlip;
import com.netappsid.erp.configurator.gui.Configurator;
import com.netappsid.wadconfigurator.Assembly;
import com.netappsid.wadconfigurator.enums.ChoiceForm;

/**
 * PRODUCT GROUP TEST for horizontally flipped trapezes
 * 
 * @author sboule
 */
public class HorizFlipTrapezeTest extends TestUtils
{
	private static double HEIGHT2 = 5d;
	private static double HEIGHT1 = 6.5d;
//	private static double MIN_HEIGHT2 = 5d;

	@BeforeClass
	public static void init() throws Exception
	{}

	@AfterClass
	public static void tearDown()
	{}

	@Test
	/**
	 * Verify the split of a Trapeze
	 */
	public void splitTrapezeTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		assertThat(assembly, notNullValue());

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		assertThat(pg, notNullValue());

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);

		pg.getDimension().setChoiceFlip(ChoiceFlip.FLIPHORIZONTAL);
		assertThat(pg.getDimension().getHeight().doubleValue(Units.INCH), IsCloseTo.closeTo(10d, DELTA));
		assertThat(pg.getDimension().getWidth().doubleValue(Units.INCH), IsCloseTo.closeTo(10d, DELTA));

		assertThat(((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), IsCloseTo.closeTo(10d, DELTA));
		assertThat(((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), IsCloseTo.closeTo(10d, DELTA));
		// OLD assertThat(((ProductTemplate)pg.getConfigurables().get(0)).getDimension().getHeight2().doubleValue(Units.INCH), IsCloseTo.closeTo(5d, DELTA));
		assertThat(((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight2(), nullValue());

		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 8d));
		assertThat(pg.getConfigurables().size(), equalTo(2));

		checkPosition(0, 0d, 0d, "Trapeze");
		checkSize(0, 10d, 8d);
		checkPosition(1, 0d, 8d, "Rectangle");
		checkSize(1, 10d, 2d);

		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(5d, 3d));
		assertThat(pg.getConfigurables().size(), equalTo(3));

		checkPosition(0, 0d, 0d, "Trapeze");
		checkSize(0, 5d, 8d);
		checkPosition(1, 5d, 2.5d, "Trapeze");
		checkSize(1, 5d, 5.5d);
		checkPosition(2, 0d, 8d, "Rectangle");
		checkSize(2, 10d, 2d);

		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(8d, 6d));
		assertThat(pg.getConfigurables().size(), equalTo(4));

		checkPosition(0, 0d, 0d, "Trapeze");
		checkSize(0, 5d, 8d);
		checkPosition(1, 5d, 2.5d, "Trapeze");
		checkSize(1, 5d, 3.5d);
		checkPosition(2, 5d, 6d, "Rectangle");
		checkSize(2, 5d, 2d);
		checkPosition(3, 0d, 8d, "Rectangle");
		checkSize(3, 10d, 2d);
	}

	@Test
	/**
	 * Verify the getEastComponents method
	 */
	public void findEastNeighborsTest()
	{
		//List<com.netappsid.erp.configurator.Configurable> neighborsList;

		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		assertThat(assembly, notNullValue());

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		assertThat(pg, notNullValue());

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);
		pg.getDimension().setChoiceFlip(ChoiceFlip.FLIPHORIZONTAL);

		splitTrapeze();

		// // -----------------------------------------------------------------------
		// neighborsList = pg.getEastComponents(pg.getConfigurables().get(0));
		// Assert.assertEquals(1,neighborsList.size());
		// assertThat(neighborsList.get(0), equalTo(pg.getConfigurables().get(1)));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getEastComponents(pg.getConfigurables().get(1));
		// Assert.assertEquals(1,neighborsList.size());
		// assertThat(neighborsList.get(0), equalTo(pg.getConfigurables().get(2)));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getEastComponents(pg.getConfigurables().get(2));
		// Assert.assertEquals(0,neighborsList.size());
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getEastComponents(pg.getConfigurables().get(3));
		// Assert.assertEquals(0,neighborsList.size());
	}

	@Test
	/**
	 * Verify the getWestComponents method
	 */
	public void findWestNeighborsTest()
	{
		//List<com.netappsid.erp.configurator.Configurable> neighborsList;

		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		assertThat(assembly, notNullValue());

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		assertThat(pg, notNullValue());

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);
		pg.getDimension().setChoiceFlip(ChoiceFlip.FLIPHORIZONTAL);

		splitTrapeze();

		// // -----------------------------------------------------------------------
		// neighborsList = pg.getWestComponents(pg.getConfigurables().get(0));
		// Assert.assertEquals(0,neighborsList.size());
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getWestComponents(pg.getConfigurables().get(1));
		// Assert.assertEquals(1,neighborsList.size());
		// assertThat(neighborsList.get(0), equalTo(pg.getConfigurables().get(0)));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getWestComponents(pg.getConfigurables().get(2));
		// Assert.assertEquals(1,neighborsList.size());
		// assertThat(neighborsList.get(0), equalTo(pg.getConfigurables().get(1)));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getWestComponents(pg.getConfigurables().get(3));
		// Assert.assertEquals(0,neighborsList.size());
	}

	@Test
	/**
	 * Verify the getNorthComponents method
	 */
	public void findNorthNeighborsTest()
	{
		//List<com.netappsid.erp.configurator.Configurable> neighborsList;

		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		assertThat(assembly, notNullValue());

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		assertThat(pg, notNullValue());

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);
		pg.getDimension().setChoiceFlip(ChoiceFlip.FLIPHORIZONTAL);

		splitTrapeze();

		// // -----------------------------------------------------------------------
		// neighborsList = pg.getNorthComponents(pg.getConfigurables().get(0));
		// Assert.assertEquals(0,neighborsList.size());
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getNorthComponents(pg.getConfigurables().get(1));
		// Assert.assertEquals(0,neighborsList.size());
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getNorthComponents(pg.getConfigurables().get(2));
		// Assert.assertEquals(0,neighborsList.size());
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getNorthComponents(pg.getConfigurables().get(3));
		// Assert.assertEquals(3,neighborsList.size());
		// assertThat(neighborsList.get(0), equalTo(pg.getConfigurables().get(0)));
		// assertThat(neighborsList.get(1), equalTo(pg.getConfigurables().get(1)));
		// assertThat(neighborsList.get(2), equalTo(pg.getConfigurables().get(2)));
	}

	@Test
	/**
	 * Verify the getSouthComponents method
	 */
	public void findSouthNeighborsTest()
	{
		//List<com.netappsid.erp.configurator.Configurable> neighborsList;

		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		assertThat(assembly, notNullValue());

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		assertThat(pg, notNullValue());

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);
		pg.getDimension().setChoiceFlip(ChoiceFlip.FLIPHORIZONTAL);

		splitTrapeze();

		// // -----------------------------------------------------------------------
		// neighborsList = pg.getSouthComponents(pg.getConfigurables().get(0));
		// Assert.assertEquals(1,neighborsList.size());
		// assertThat(neighborsList.get(0), equalTo(pg.getConfigurables().get(3)));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getSouthComponents(pg.getConfigurables().get(1));
		// Assert.assertEquals(1,neighborsList.size());
		// assertThat(neighborsList.get(0), equalTo(pg.getConfigurables().get(3)));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getSouthComponents(pg.getConfigurables().get(2));
		// Assert.assertEquals(1,neighborsList.size());
		// assertThat(neighborsList.get(0), equalTo(pg.getConfigurables().get(3)));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getSouthComponents(pg.getConfigurables().get(3));
		// Assert.assertEquals(0,neighborsList.size());
	}

	@Test
	/**
	 * Verify the West Stack
	 */
	public void findWestStackTest()
	{
		//List<com.netappsid.erp.configurator.Configurable> stackList;

		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		assertThat(assembly, notNullValue());

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		assertThat(pg, notNullValue());

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);
		pg.getDimension().setChoiceFlip(ChoiceFlip.FLIPHORIZONTAL);

		splitTrapeze();

		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(0), Side.WEST, null);
		// Assert.assertEquals(1,stackList.size());
		// assertThat(stackList.get(0), equalTo(pg.getConfigurables().get(3)));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(1), Side.WEST, null);
		// Assert.assertEquals(0,stackList.size());
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(2), Side.WEST, null);
		// Assert.assertEquals(0,stackList.size());
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(3), Side.WEST, null);
		// Assert.assertEquals(1,stackList.size());
		// assertThat(stackList.get(0), equalTo(pg.getConfigurables().get(0)));
	}

	@Test
	/**
	 * Verify the East Stack
	 */
	public void findEastStackTest()
	{
		//List<com.netappsid.erp.configurator.Configurable> stackList;

		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		assertThat(assembly, notNullValue());

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		assertThat(pg, notNullValue());

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);
		pg.getDimension().setChoiceFlip(ChoiceFlip.FLIPHORIZONTAL);

		splitTrapeze();

		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(0), Side.EAST, null);
		// Assert.assertEquals(0,stackList.size());
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(1), Side.EAST, null);
		// Assert.assertEquals(0,stackList.size());
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(2), Side.EAST, null);
		// Assert.assertEquals(1,stackList.size());
		// assertThat(stackList.get(0), equalTo(pg.getConfigurables().get(3)));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(3), Side.EAST, null);
		// Assert.assertEquals(1,stackList.size());
		// assertThat(stackList.get(0), equalTo(pg.getConfigurables().get(2)));
	}

	@Test
	/**
	 * Verify the North Stack
	 */
	public void findNorthStackTest()
	{
		//List<com.netappsid.erp.configurator.Configurable> stackList;

		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		assertThat(assembly, notNullValue());

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		assertThat(pg, notNullValue());

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);
		pg.getDimension().setChoiceFlip(ChoiceFlip.FLIPHORIZONTAL);

		splitTrapeze();

		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(0), Side.NORTH, null);
		// Assert.assertEquals(0,stackList.size());
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(1), Side.NORTH, null);
		// Assert.assertEquals(0,stackList.size());
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(2), Side.NORTH, null);
		// Assert.assertEquals(0,stackList.size());
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(3), Side.NORTH, null);
		// Assert.assertEquals(0,stackList.size());
	}

	@Test
	/**
	 * Verify the South Stack
	 */
	public void findSouthStackTest()
	{
		//List<com.netappsid.erp.configurator.Configurable> stackList;

		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		assertThat(assembly, notNullValue());

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		assertThat(pg, notNullValue());

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);
		pg.getDimension().setChoiceFlip(ChoiceFlip.FLIPHORIZONTAL);

		splitTrapeze();

		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(0), Side.SOUTH, null);
		// Assert.assertEquals(2,stackList.size());
		// assertThat(stackList.get(0), equalTo(pg.getConfigurables().get(1)));
		// assertThat(stackList.get(1), equalTo(pg.getConfigurables().get(2)));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(1), Side.SOUTH, null);
		// Assert.assertEquals(2,stackList.size());
		// assertThat(stackList.get(0), equalTo(pg.getConfigurables().get(0)));
		// assertThat(stackList.get(1), equalTo(pg.getConfigurables().get(2)));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(2), Side.SOUTH, null);
		// Assert.assertEquals(2,stackList.size());
		// assertThat(stackList.get(0), equalTo(pg.getConfigurables().get(0)));
		// assertThat(stackList.get(1), equalTo(pg.getConfigurables().get(1)));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(3), Side.SOUTH, null);
		// Assert.assertEquals(0,stackList.size());
	}

	@Test
	/**
	 * Verify the width increase
	 */
	public void widthIncreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		assertThat(assembly, notNullValue());

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		assertThat(pg, notNullValue());

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);
		pg.getDimension().setChoiceFlip(ChoiceFlip.FLIPHORIZONTAL);

		splitTrapeze();

		initProperties();
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().setWidth(Units.inch(4d));
		checkSize(0, 4d, null);
		checkSize(1, 2d, null);
		checkSize(2, 4d, null);
		checkSize(3, 10d, null);
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().removeWidth();
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().setWidth(Units.inch(5d));
		checkSize(0, 2d, null);
		checkSize(1, 5d, null);
		checkSize(2, 3d, null);
		checkSize(3, 10d, null);
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().removeWidth();
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().setWidth(Units.inch(5d));
		checkSize(0, 3d, null);
		checkSize(1, 2d, null);
		checkSize(2, 5d, null);
		checkSize(3, 10d, null);
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().removeWidth();
		verifyInitial();
	}

	@Test
	/**
	 * Verify the height increase
	 */
	public void heightIncreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		assertThat(assembly, notNullValue());

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		assertThat(pg, notNullValue());

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);
		pg.getDimension().setChoiceFlip(ChoiceFlip.FLIPHORIZONTAL);

		splitTrapeze();

		initProperties();
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setHeight(Units.inch(3d));
		checkSize(0, null, 7d);
		checkSize(1, null, 5.5);
		checkSize(2, null, 4d);
		checkSize(3, null, 3d);
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removeHeight();
		verifyInitial();
	}

	@Test
	/**
	 * Verify the width decrease
	 */
	public void widthDecreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		assertThat(assembly, notNullValue());

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		assertThat(pg, notNullValue());

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);
		pg.getDimension().setChoiceFlip(ChoiceFlip.FLIPHORIZONTAL);

		splitTrapeze();

		initProperties();
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().setWidth(Units.inch(2d));
		checkSize(0, 2d, null);
		checkSize(1, 4d, null);
		checkSize(2, 4d, null);
		checkSize(3, 10d, null);
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().removeWidth();
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().setWidth(Units.inch(1d));
		checkSize(0, 4d, null);
		checkSize(1, 1d, null);
		checkSize(2, 5d, null);
		checkSize(3, 10d, null);
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().removeWidth();
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().setWidth(Units.inch(3d));
		checkSize(0, 3d, null);
		checkSize(1, 4d, null);
		checkSize(2, 3d, null);
		checkSize(3, 10d, null);
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().removeWidth();
		verifyInitial();
	}

	@Test
	/**
	 * Verify the height decrease
	 */
	public void heightDecreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		assertThat(assembly, notNullValue());

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		assertThat(pg, notNullValue());

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);
		pg.getDimension().setChoiceFlip(ChoiceFlip.FLIPHORIZONTAL);

		splitTrapeze();

		initProperties();
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setHeight(Units.inch(1d));
		checkSize(0, null, 9d);
		checkSize(1, null, 7.5d);
		checkSize(2, null, 6d);
		checkSize(3, null, 1d);
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removeHeight();
		verifyInitial();
	}

	@Test
	/**
	 * Verify the width change with overrides
	 */
	public void widthChangeWithOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		assertThat(assembly, notNullValue());

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		assertThat(pg, notNullValue());

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);
		pg.getDimension().setChoiceFlip(ChoiceFlip.FLIPHORIZONTAL);

		splitTrapeze();

		initProperties();
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().setWidth(Units.inch(5d));
		checkSize(0, 2d, null);
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().setWidth(Units.inch(3d));
		checkSize(0, 2d, null);
		checkSize(1, 5d, null);
		checkSize(2, 3d, null);
		checkSize(3, 10d, null);
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().removeWidth();
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().removeWidth();
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().setWidth(Units.inch(3d));
		checkSize(1, 4d, null);
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().setWidth(Units.inch(5d));
		checkSize(0, 2d, null);
		checkSize(1, 5d, null);
		checkSize(2, 3d, null);
		checkSize(3, 10d, null);
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().removeWidth();
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().removeWidth();
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().setWidth(Units.inch(5d)); // Override
		checkSize(2, 3d, null);
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().setWidth(Units.inch(4d));
		checkSize(0, 2d, null);
		checkSize(1, 5d, null);
		checkSize(2, 3d, null);
		checkSize(3, 10d, null);
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().removeWidth();
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().removeWidth();
		verifyInitial();
	}

	@Test
	/**
	 * Verify the width decrease with overrides
	 */
	public void widthChangeMinMaxTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		assertThat(assembly, notNullValue());

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		assertThat(pg, notNullValue());

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);
		pg.getDimension().setChoiceFlip(ChoiceFlip.FLIPHORIZONTAL);

		splitTrapeze();

		initProperties();
		verifyInitial();

		// // Change the minimum because of a bug in form (in flipped mode).
		// // When a width is set to a value bellow 1", a crash occurs.
		// // This case will not happen in real life...
		// for (com.netappsid.erp.configurator.Configurable prodTemplate : pg.getConfigurables())
		// {
		// // The minValue is added to the ProductData value.
		// ((ProductTemplate)prodTemplate).setMinSize(Units.inch(0.5));
		// } /* end for */

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().setWidth(Units.inch(11d));
		checkSize(0, 5d, null);
		checkSize(1, 1d, null);
		checkSize(2, 4d, null);
		checkSize(3, 10d, null);
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().removeWidth();
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().setWidth(Units.inch(1d));
		checkSize(0, 1d, null);
		checkSize(1, 5d, null);
		checkSize(2, 4d, null);
		checkSize(3, 10d, null);
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().removeWidth();
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().setWidth(Units.inch(11d));
		checkSize(0, 3d, null);
		checkSize(1, 1d, null);
		checkSize(2, 6d, null);
		checkSize(3, 10d, null);
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().removeWidth();
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().setWidth(Units.inch(1d));
		checkSize(0, 3d, null);
		checkSize(1, 6d, null);
		checkSize(2, 1d, null);
		checkSize(3, 10d, null);
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().removeWidth();
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setWidth(Units.inch(0.5d));
		verifyInitial();
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removeWidth();
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setWidth(Units.inch(11d));
		verifyInitial();
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removeWidth();
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().setWidth(Units.inch(1d));
		checkSize(0, 4d, null);
		checkSize(1, 1d, null);
		checkSize(2, 5d, null);
		checkSize(3, 10d, null);
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().removeWidth();
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().setWidth(Units.inch(11d));
		checkSize(0, 1d, null);
		checkSize(1, 8d, null);
		checkSize(2, 1d, null);
		checkSize(3, 10d, null);
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().removeWidth();
		checkSize(0, 3.5d, null);
		checkSize(1, 3d, null);
		checkSize(2, 3.5d, null);
		checkSize(3, 10d, null);
	}

	@Test
	/**
	 * Verify the height decrease with override
	 */
	public void heightChangeMinMaxTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		assertThat(assembly, notNullValue());

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		assertThat(pg, notNullValue());

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);
		pg.getDimension().setChoiceFlip(ChoiceFlip.FLIPHORIZONTAL);

		splitTrapeze();

		initProperties();
		verifyInitial();

		// There is an exception in Form when doing this test... DO NOT DELETE !
		// FIXME Flipped Trapeze bug
		// // -----------------------------------------------------------------------
		// ((Dimensionnable)pg.getConfigurables().get(3)).getDimension().setHeight(Units.inch(11d));
		// checkSize(0, null, MIN_HEIGHT2);
		// checkSize(1, null, HEIGHT1 - (8d - MIN_HEIGHT2));
		// checkSize(2, null, HEIGHT2 - (8d - MIN_HEIGHT2));
		// checkSize(3, null, 2 + (8d - MIN_HEIGHT2));
		// ((Dimensionnable)pg.getConfigurables().get(3)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT);
		// verifyInitial();
		//
		// // -----------------------------------------------------------------------
		// ((Dimensionnable)pg.getConfigurables().get(3)).getDimension().setHeight(Units.inch(0.5d));
		// checkSize(0, null, 9d);
		// checkSize(1, null, HEIGHT1 + 1d);
		// checkSize(2, null, HEIGHT2 + 1d);
		// checkSize(3, null, 1d);
		// ((Dimensionnable)pg.getConfigurables().get(3)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT);
		// verifyInitial();
	}

	/**
	 * Split a trapeze into 4 pieces:
	 * 
	 * | | | | | 0 | | | | | | 1 | | | | | 2 | |----------------------------| | 3 | |----------------------------|
	 */
	private void splitTrapeze()
	{
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 8d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(3d, 7d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(6d, 7d));
		assertThat(pg.getConfigurables().size(), equalTo(4));
	}

	/**
	 * Verify that all the components have the initial sizes.
	 */
	private void verifyInitial()
	{
		checkSize(0, 3d, 8d);
		checkSize(1, 3d, HEIGHT1);
		checkSize(2, 4d, HEIGHT2);
		checkSize(3, 10d, 2d);
	}

	@Override
	protected void checkSize(int confIndex, Double width, Double height)
	{
		super.checkSize(confIndex, width, height);

		if (width == null)
		{
			width = ((ProductTemplate) pg.getConfigurables().get(confIndex)).getDimension().getWidth().doubleValue(Units.INCH);
		} /* end if */

		if (height == null)
		{
			height = ((ProductTemplate) pg.getConfigurables().get(confIndex)).getDimension().getHeight().doubleValue(Units.INCH);
		} /* end if */

		// Check height2
		if (((ProductTemplate) pg.getConfigurables().get(confIndex)).getDimension().getHeight2() != null)
		{
			assertThat(width / 2d, IsCloseTo.closeTo(Math.abs(height - ((ProductTemplate) pg.getConfigurables().get(confIndex)).getDimension().getHeight2().doubleValue(Units.INCH)), DELTA));
		} /* end if */
	}
}
