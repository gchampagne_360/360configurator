package ProductGroup;

import java.awt.geom.Point2D;

import junit.framework.Assert;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.netappsid.commonutils.geo.enums.Orientation;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.common.Configurable;
import com.netappsid.configuration.common.Dimensionnable;
import com.netappsid.erp.configurator.gui.Configurator;
import com.netappsid.wadconfigurator.Assembly;
import com.netappsid.wadconfigurator.Dimension;
import com.netappsid.wadconfigurator.ProductGroup;
import com.netappsid.wadconfigurator.enums.ChoiceForm;

/**
 * PRODUCT GROUP TEST for group resize trapezes (overall resizes) Scenario B
 * 
 * @author sboule
 */
public class NoFlipTrapezeGroupResizeTestD extends TestUtils
{
	private static double SIZE_OVERALL = 18d;

	@BeforeClass
	public static void init() throws Exception
	{}

	@AfterClass
	public static void tearDown()
	{}

	@Test
	/**
	 * Verify the height increase
	 */
	public void heightIncreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double height = 20d;
		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(height));
		Assert.assertEquals(height, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		checkSize(0, 6.0, 11.777777777777779, 8.777777777777779, null);
		checkPosition(0, 0.0, 6.0, "Trapeze");
		checkSize(1, 6.0, 17.0, 14.0, null);
		checkPosition(1, 6.0, 3.0, "Trapeze");
		checkSize(2, 6.0, 20.0, 17.0, null);
		checkPosition(2, 12.0, 0.0, "Trapeze");
		checkSize(3, 6.0, 2.2222222222222214, null, null);
		checkPosition(3, 0.0, 17.77777777777778, "Rectangle");

		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT);
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the height decrease
	 */
	public void heightDecreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double height = 16d;
		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(height));
		Assert.assertEquals(height, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		checkSize(0, 6.0, 8.222222222222221, 5.222222222222221, null);
		checkPosition(0, 0.0, 6.0, "Trapeze");
		checkSize(1, 6.0, 13.0, 10.0, null);
		checkPosition(1, 6.0, 3.0, "Trapeze");
		checkSize(2, 6.0, 16.0, 13.0, null);
		checkPosition(2, 12.0, 0.0, "Trapeze");
		checkSize(3, 6.0, 1.7777777777777786, null, null);
		checkPosition(3, 0.0, 14.222222222222221, "Rectangle");

		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT);
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the height2 increase
	 */
	public void height2IncreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double height2 = 10d;
		// -----------------------------------------------------------------------
		pg.getDimension().setHeight2(Units.inch(height2));
		Assert.assertEquals(height2, pg.getDimension().getHeight2().doubleValue(Units.INCH), DELTA);

		checkSize(0, 6d, 10.66667d, 8d);
		checkSize(1, 6d, 15.33333d, 12.66667d);
		checkSize(2, 6d, 18d, 15.33333d);
		checkSize(3, 6d, 2d);

		checkPosition(0, 0d, 5.33333d, "Trapeze");
		checkPosition(1, 6d, 2.66667d, "Trapeze");
		checkPosition(2, 12d, 0d, "Trapeze");
		checkPosition(3, 0d, 16d, "Rectangle");

		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT2);
		Assert.assertEquals(SIZE_OVERALL / 2d, pg.getDimension().getHeight2().doubleValue(Units.INCH), DELTA);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the height2 decrease
	 */
	public void height2DecreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double height2 = 8d;
		// -----------------------------------------------------------------------
		pg.getDimension().setHeight2(Units.inch(height2));
		Assert.assertEquals(height2, pg.getDimension().getHeight2().doubleValue(Units.INCH), DELTA);

		checkSize(0, 6d, 9.33333d, 6d);
		checkSize(1, 6d, 14.66667d, 11.33333d);
		checkSize(2, 6d, 18d, 14.66667d);
		checkSize(3, 6d, 2d);

		checkPosition(0, 0d, 6.66667d, "Trapeze");
		checkPosition(1, 6d, 3.33333d, "Trapeze");
		checkPosition(2, 12d, 0d, "Trapeze");
		checkPosition(3, 0d, 16d, "Rectangle");

		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT2);
		Assert.assertEquals(SIZE_OVERALL / 2d, pg.getDimension().getHeight2().doubleValue(Units.INCH), DELTA);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the width increase
	 */
	public void widthIncreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double width = 21d;
		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(width));
		Assert.assertEquals(width, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		checkSize(0, 7.0, 9.333333, 6.0, null);
		checkPosition(0, 0.0, 6.6666667, "Trapeze");
		checkSize(1, 7.0, 14.6666668, 11.3333332, null);
		checkPosition(1, 7.0, 3.333333, "Trapeze");
		checkSize(2, 7.0, 18.0, 14.6666668, null);
		checkPosition(2, 14.0, 0.0, "Trapeze");
		checkSize(3, 7.0, 2.0, null, null);
		checkPosition(3, 0.0, 16.0, "Rectangle");

		pg.getDimension().removeWidth();
		pg.getDimension().setHeight2(Units.inch(SIZE_OVERALL / 2d));
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the width decrease
	 */
	public void widthDecreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double width = 15d;
		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(width));
		Assert.assertEquals(width, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		checkSize(0, 5.0, 12.0, 10.0, null);
		checkPosition(0, 0.0, 4.0, "Trapeze");
		checkSize(1, 5.0, 16.0, 14.0, null);
		checkPosition(1, 5.0, 2.0, "Trapeze");
		checkSize(2, 5.0, 18.0, 16.0, null);
		checkPosition(2, 10.0, 0.0, "Trapeze");
		checkSize(3, 5.0, 2.0, null, null);
		checkPosition(3, 0.0, 16.0, "Rectangle");

		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the height increase with override
	 */
	public void heightIncreaseWithOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double height = 20d;
		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setHeight(Units.inch(4d));
		checkSize(0, 6d, 8d, 5d);
		checkSize(1, 6d, 15d, 12d);
		checkSize(2, 6d, 18d, 15d);
		checkSize(3, 6d, 4d);

		pg.getDimension().setHeight(Units.inch(height));
		Assert.assertEquals(height, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		checkSize(0, 6.0, 10.0, 7.0, null);
		checkPosition(0, 0.0, 6.0, "Trapeze");
		checkSize(1, 6.0, 17.0, 14.0, null);
		checkPosition(1, 6.0, 3.0, "Trapeze");
		checkSize(2, 6.0, 20.0, 17.0, null);
		checkPosition(2, 12.0, 0.0, "Trapeze");
		checkSize(3, 6.0, 4.0, null, null);
		checkPosition(3, 0.0, 16.0, "Rectangle");

		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT);
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the height decrease with override
	 */
	public void heightDecreaseWithOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double height = 16d;
		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setHeight(Units.inch(4d));
		checkSize(0, 6d, 8d, 5d);
		checkSize(1, 6d, 15d, 12d);
		checkSize(2, 6d, 18d, 15d);
		checkSize(3, 6d, 4d);

		pg.getDimension().setHeight(Units.inch(height));
		Assert.assertEquals(height, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		checkSize(0, 6.0, 6.0, 3.0, null);
		checkPosition(0, 0.0, 6.0, "Trapeze");
		checkSize(1, 6.0, 13.0, 10.0, null);
		checkPosition(1, 6.0, 3.0, "Trapeze");
		checkSize(2, 6.0, 16.0, 13.0, null);
		checkPosition(2, 12.0, 0.0, "Trapeze");
		checkSize(3, 6.0, 4.0, null, null);
		checkPosition(3, 0.0, 12.0, "Rectangle");

		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT);
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the width increase with override
	 */
	public void widthIncreaseWithOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double width = 20d;
		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setWidth(Units.inch(4d));
		checkSize(0, 4d, 9d, 7d);
		checkSize(1, 8d, 15d, 11d);
		checkSize(2, 6d, 18d, 15d);
		checkSize(3, 4d, 2d);

		pg.getDimension().setWidth(Units.inch(width));
		Assert.assertEquals(width, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		checkSize(0, 4.0, 8.8, 7.0, null);
		checkPosition(0, 0.0, 7.2, "Trapeze");
		checkSize(1, 9.1428571, 14.9142857, 10.8, null);
		checkPosition(1, 4.0, 3.0857, "Trapeze");
		checkSize(2, 6.857142857, 18.0, 14.9142857, null);
		checkPosition(2, 13.142857, 0.0, "Trapeze");
		checkSize(3, 4.0, 2.0, null, null);
		checkPosition(3, 0.0, 16.0, "Rectangle");

		pg.getDimension().removeWidth();
		pg.getDimension().setHeight2(Units.inch(SIZE_OVERALL / 2d));
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the width decrease with override
	 */
	public void widthDecreaseWithOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double width = 16d;
		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setWidth(Units.inch(4d));
		checkSize(0, 4d, 9d, 7d);
		checkSize(1, 8d, 15d, 11d);
		checkSize(2, 6d, 18d, 15d);
		checkSize(3, 4d, 2d);

		pg.getDimension().setWidth(Units.inch(width));
		Assert.assertEquals(width, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		checkSize(0, 4.0, 10.75, 9.0, null);
		checkPosition(0, 0.0, 5.25, "Trapeze");
		checkSize(1, 6.857142857142858, 15.75, 12.75, null);
		checkPosition(1, 4.0, 2.25, "Trapeze");
		checkSize(2, 5.142857142857142, 18.0, 15.75, null);
		checkPosition(2, 10.857142857142858, 0.0, "Trapeze");
		checkSize(3, 4.0, 2.0, null, null);
		checkPosition(3, 0.0, 16.0, "Rectangle");

		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the height change to the minimum
	 */
	public void heightChangeMinTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double height = 1d;
		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(height));
		Assert.assertEquals(2d, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);

		checkSize(0, 6.0, 0.33333333333333326, 0.0, 6.0);
		checkPosition(0, 0.0, 0.6666666666666667, "Trapeze");
		checkSize(1, 6.0, 1.6666666666666665, 1.3333333333333333, null);
		checkPosition(1, 6.0, 0.33333333333333337, "Trapeze");
		checkSize(2, 6.0, 2.0, 1.6666666666666665, null);
		checkPosition(2, 12.0, 0.0, "Trapeze");
		checkSize(3, 6.0, 1.0, null, null);
		checkPosition(3, 0.0, 1.0, "Rectangle");

		pg.getDimension().removeHeight();
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		checkSize(0, 6.0, 8.333333333333334, 8.0, 6.0);
		checkPosition(0, 0.0, 0.6666666666666667, "Trapeze");
		checkSize(1, 6.0, 17.666666666666668, 17.333333333333332, null);
		checkPosition(1, 6.0, 0.33333333333333337, "Trapeze");
		checkSize(2, 6.0, 18.0, 17.666666666666668, null);
		checkPosition(2, 12.0, 0.0, "Trapeze");
		checkSize(3, 6.0, 9.0, null, null);
		checkPosition(3, 0.0, 9.0, "Rectangle");
	}

	@Test
	/**
	 * Verify the width change to the minimum
	 */
	public void widthChangeMinTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double width = 1d;
		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(width));
		Assert.assertEquals(3d, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		checkSize(0, 1.0, 15.333333333333334, 15.0, null);
		checkPosition(0, 0.0, 0.6666666666666667, "Trapeze");
		checkSize(1, 1.0, 17.666666666666668, 17.333333333333332, null);
		checkPosition(1, 1.0, 0.33333333333333337, "Trapeze");
		checkSize(2, 1.0, 18.0, 17.666666666666668, null);
		checkPosition(2, 2.0, 0.0, "Trapeze");
		checkSize(3, 1.0, 2.0, null, null);
		checkPosition(3, 0.0, 16.0, "Rectangle");

		pg.getDimension().removeWidth();
		pg.getDimension().setHeight2(Units.inch(SIZE_OVERALL / 2d));
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		verifyInitial();
	}

	/**
	 * Split a trapeze into 4 pieces:
	 * 
	 * | | | | | 2 | | 1 | | | | | | | 0 | | | |------| | | | 3 | | | |----------------------------|
	 */
	private void splitTrapeze()
	{
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(6d, 7d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(12d, 7d));
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(3d, 16d));
		Assert.assertEquals(4, pg.getConfigurables().size());

		initProperties();
		verifyInitial();
	}

	/**
	 * Verify that all the components have the initial sizes and positions.
	 */
	private void verifyInitial()
	{
		checkSize(0, 6d, 10d, 7d);
		checkSize(1, 6d, 15d, 12d);
		checkSize(2, 6d, 18d, 15d);
		checkSize(3, 6d, 2d);

		checkPosition(0, 0d, 6d, "Trapeze");
		checkPosition(1, 6d, 3d, "Trapeze");
		checkPosition(2, 12d, 0d, "Trapeze");
		checkPosition(3, 0d, 16d, "Rectangle");
	}
}
