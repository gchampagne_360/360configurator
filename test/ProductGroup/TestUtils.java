package ProductGroup;

import static org.junit.Assert.assertThat;

import java.awt.geom.Point2D;

import org.hamcrest.number.IsCloseTo;

import com.netappsid.commonutils.geo.Form;
import com.netappsid.commonutils.geo.IdentifiableShape.Attribute;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.common.Dimensionnable;
import com.netappsid.wadconfigurator.Dimension;
import com.netappsid.wadconfigurator.ProductGroup;
import com.netappsid.wadconfigurator.ProductTemplate;

public class TestUtils
{
	public static final double DELTA = 0.01;
	protected ProductGroup pg;

	/**
	 * Check that the position of the specified configurable is at the specified values. Check also the form type name.
	 * 
	 * @param confIndex
	 * @param x
	 * @param y
	 * @param name
	 */
	protected void checkPosition(int confIndex, double x, double y, String name)
	{
		// Assert.assertEquals(name, ((ProductTemplate) pg.getConfigurables().get(confIndex)).getDimension().getForm().getClass().getSimpleName());

		Point2D position = pg.getConfigurablesPosition().get(confIndex);
		assertThat("position X", position.getX(), IsCloseTo.closeTo(x, DELTA));
		assertThat("position Y", position.getY(), IsCloseTo.closeTo(y, DELTA));
	}

	/**
	 * Check that the size of the specified configurable is as the specified values.
	 * 
	 * @param confIndex
	 * @param width
	 * @param height
	 */
	protected void checkSize(int confIndex, Double width, Double height)
	{
		Dimension dim = ((ProductTemplate) pg.getConfigurables().get(confIndex)).getDimension();

		if (width != null)
		{
			assertThat("WIDHT", dim.getWidth().doubleValue(Units.INCH), IsCloseTo.closeTo(width, DELTA));
		} /* end if */

		if (height != null)
		{
			if (dim.getHeight() == null) // Half Circle
			{
				assertThat("HEIGHT for half circle (WIDTH)", dim.getWidth().doubleValue(Units.INCH), IsCloseTo.closeTo(height, DELTA));
			} /* end if */
			else
			{
				assertThat("HEIGHT", dim.getHeight().doubleValue(Units.INCH), IsCloseTo.closeTo(height, DELTA));
			} /* end else */
		} /* end if */
	}

	protected void checkSize(int confIndex, Double width, Double height, Double height2)
	{
		checkSize(confIndex, width, height);

		Dimension dimension = ((ProductTemplate) pg.getConfigurables().get(confIndex)).getDimension();
		if (width == null)
		{
			width = dimension.getWidth().doubleValue(Units.INCH);
		} /* end if */

		if (height == null)
		{
			height = dimension.getHeight().doubleValue(Units.INCH);
		} /* end if */

		// Check height2
		if (height2 != null && dimension.getHeight2() != null)
		{
			assertThat("HEIGHT2", dimension.getHeight2().doubleValue(Units.INCH), IsCloseTo.closeTo(height2, DELTA));
		} /* end if */
	}

	protected void checkSize(int confIndex, Double width, Double height, Double height2, Double width2)
	{
		checkSize(confIndex, width, height, height2);

		// Check width2
		Dimension dimension = ((ProductTemplate) pg.getConfigurables().get(confIndex)).getDimension();
		if (width2 != null && dimension.getWidth2() != null)
		{
			assertThat("WIDTH2", dimension.getForm().getValue(Attribute.WIDTH2), IsCloseTo.closeTo(width2, DELTA));
		} /* end if */
	}

	protected void checkRay(int confIndex, Double ray)
	{
		// Check ray
		Dimension dimension = ((ProductTemplate) pg.getConfigurables().get(confIndex)).getDimension();
		if (ray != null && dimension.getRay() != null)
		{
			assertThat("RAY", dimension.getForm().getValue(Attribute.RAY), IsCloseTo.closeTo(ray, DELTA));
		} /* end if */
	}

	/**
	 * This call is needed in order to create the properties map and activate the addressedFires feature.
	 */
	protected void initProperties()
	{
		pg.getProperties(true);

		for (com.netappsid.erp.configurator.Configurable configurable : pg.getConfigurables())
		{
			configurable.getProperties(true);
		} /* end for */
	}

	/**
	 * Resize the ProductGroup with the specified value.
	 * 
	 * @param size
	 */
	protected void resize(Double size)
	{
		initProperties();

		pg.getDimension().setDefaultWidth(Units.inch(size));
		pg.getDimension().setDefaultHeight(Units.inch(size));
		pg.getDimension().setDefaultHeight2(Units.inch(size / 2d));
	}

	/**
	 * Resize the ElongatedArc ProductGroup with the specified value.
	 * 
	 * @param size
	 */
	protected void resizeElongatedArc(Double size)
	{
		initProperties();

		pg.getDimension().setDefaultWidth(Units.inch(size));
		pg.getDimension().setDefaultHeight(Units.inch(size));
		pg.getDimension().setDefaultHeight2(Units.inch(size / 2d));
	}

	/**
	 * Print all configurables sizes and positions.
	 */
	protected void printAll()
	{
		for (com.netappsid.erp.configurator.Configurable configurable : pg.getConfigurables())
		{
			Dimension dimension = (Dimension) ((Dimensionnable) configurable).getDimension();
			Form form = dimension.getForm();
			StringBuilder message = new StringBuilder("checkSize - index=");
			message.append(configurable.getIndex());
			message.append(" (");

			boolean firstAtt = true;
			for (Attribute att : Attribute.values())
			{
				if (firstAtt)
				{
					firstAtt = false;
				}
				else
				{
					message.append(", ");
				}

				message.append(att.ticker);
				message.append("=");
				message.append(form.getValue(att));
			}
			System.out.println(message.toString());

			message = new StringBuilder("checkPosition(");
			message.append(configurable.getIndex());
			message.append(", ");
			message.append(form.getOrigin().getX());
			message.append(", ");
			message.append(form.getOrigin().getY());
			message.append(", \"");
			message.append(form.getClass().getSimpleName());
			message.append("\");");
			System.out.println(message.toString());

		} /* end for */

		System.out.println("-------------------------------------------------------");
	}

}
