package ProductGroup;

import java.awt.geom.Point2D;

import junit.framework.Assert;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.netappsid.commonutils.geo.enums.Orientation;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.common.Configurable;
import com.netappsid.configuration.common.Dimensionnable;
import com.netappsid.erp.configurator.gui.Configurator;
import com.netappsid.wadconfigurator.Assembly;
import com.netappsid.wadconfigurator.Dimension;
import com.netappsid.wadconfigurator.ProductGroup;
import com.netappsid.wadconfigurator.enums.ChoiceForm;

/**
 * PRODUCT GROUP TEST for group resize trapezes (overall resizes) Scenario B
 * 
 * @author sboule
 */
public class NoFlipTrapezeGroupResizeTestB extends TestUtils
{
	private static double SIZE_OVERALL = 18d;

	private static double HEIGHT_1 = 3d;
	private static double HEIGHT_2 = 6d;
	private static double HEIGHT_3 = 9d;
	private static double HEIGHT_4 = 12d;

	@BeforeClass
	public static void init() throws Exception
	{}

	@AfterClass
	public static void tearDown()
	{}

	@Test
	/**
	 * Verify the height increase
	 */
	public void heightIncreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		pg.setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double height = 20d;
		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(height));
		Assert.assertEquals(height, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		checkSize(2, 6.0, 7.333333333333334, 4.333333333333334, null);
		checkPosition(2, 0.0, 6.0, "Trapeze");
		checkSize(1, 6.0, 10.333333333333334, 7.333333333333334, null);
		checkPosition(1, 6.0, 3.0, "Trapeze");
		checkSize(0, 6.0, 13.333333333333334, 10.333333333333334, null);
		checkPosition(0, 12.0, 0.0, "Trapeze");
		checkSize(3, 18.0, 6.666666666666666, null, null);
		checkPosition(3, 0.0, 13.333333333333334, "Rectangle");

		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT);
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the height decrease
	 */
	public void heightDecreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		pg.setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double height = 16d;
		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(height));
		Assert.assertEquals(height, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);

		checkSize(2, 6.0, 4.666666666666666, 1.666666666666666, null);
		checkPosition(2, 0.0, 6.0, "Trapeze");
		checkSize(1, 6.0, 7.666666666666666, 4.666666666666666, null);
		checkPosition(1, 6.0, 3.0, "Trapeze");
		checkSize(0, 6.0, 10.666666666666666, 7.666666666666666, null);
		checkPosition(0, 12.0, 0.0, "Trapeze");
		checkSize(3, 18.0, 5.333333333333334, null, null);
		checkPosition(3, 0.0, 10.666666666666666, "Rectangle");

		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT);
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the height2 increase
	 */
	public void height2IncreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		pg.setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double height2 = 10d;
		// -----------------------------------------------------------------------
		pg.getDimension().setHeight2(Units.inch(height2));
		Assert.assertEquals(height2, pg.getDimension().getHeight2().doubleValue(Units.INCH), DELTA);
		checkSize(2, 6d, 6.66667d, 4d);
		checkSize(1, 6d, 9.33333d, 6.66667d);
		checkSize(0, 6d, 12d, 9.33333d);
		checkSize(3, 18d, 6d);

		checkPosition(2, 0d, 5.33333d, "Trapeze");
		checkPosition(1, 6d, 2.66667d, "Trapeze");
		checkPosition(0, 12d, 0d, "Trapeze");
		checkPosition(3, 0d, 12d, "Rectangle");

		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT2);
		Assert.assertEquals(SIZE_OVERALL / 2d, pg.getDimension().getHeight2().doubleValue(Units.INCH), DELTA);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the height2 decrease
	 */
	public void height2DecreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		pg.setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double height2 = 8d;
		// -----------------------------------------------------------------------
		pg.getDimension().setHeight2(Units.inch(height2));
		Assert.assertEquals(height2, pg.getDimension().getHeight2().doubleValue(Units.INCH), DELTA);
		checkSize(2, 6d, 5.33333d, 2d);
		checkSize(1, 6d, 8.66667d, 5.33333d);
		checkSize(0, 6d, 12d, 8.66667d);
		checkSize(3, 18d, 6d);

		checkPosition(2, 0d, 6.66667d, "Trapeze");
		checkPosition(1, 6d, 3.33333d, "Trapeze");
		checkPosition(0, 12d, 0d, "Trapeze");
		checkPosition(3, 0d, 12d, "Rectangle");

		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT2);
		Assert.assertEquals(SIZE_OVERALL / 2d, pg.getDimension().getHeight2().doubleValue(Units.INCH), DELTA);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the width increase
	 */
	public void widthIncreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		pg.setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double width = 21d;

		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(width));
		Assert.assertEquals(width, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		checkSize(2, 7.0, 5.0, 6.0, null);
		checkPosition(2, 0.0, 7.0, "Trapeze");
		checkSize(1, 7.0, 8.5, 8.0, null);
		checkPosition(1, 7.0, 3.5, "Trapeze");
		checkSize(0, 7.0, 12.0, 10.0, null);
		checkPosition(0, 14.0, 0.0, "Trapeze");
		checkSize(3, 21.0, 6.0, null, null);
		checkPosition(3, 0.0, 12.0, "Rectangle");

		pg.getDimension().removeWidth();
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the width decrease
	 */
	public void widthDecreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		pg.setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double width = 15d;
		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(width));
		Assert.assertEquals(width, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		checkSize(0, 5.0, 12.0, 10.0, null);
		checkPosition(0, 10.0, 0.0, "Trapeze");
		checkSize(1, 5.0, 9.5, 8.0, null);
		checkPosition(1, 5.0, 2.5, "Trapeze");
		checkSize(2, 5.0, 7.0, 6.0, null);
		checkPosition(2, 0.0, 5.0, "Trapeze");
		checkSize(3, 15.0, 6.0, null, null);
		checkPosition(3, 0.0, 12.0, "Rectangle");

		pg.getDimension().removeWidth();
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the height increase with override
	 */
	public void heightIncreaseWithOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		pg.setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double height = 20d;
		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setHeight(Units.inch(4d));
		checkSize(2, 6d, 8d, 5d);
		checkSize(1, 6d, 11d, 8d);
		checkSize(0, 6d, 14d, 11d);
		checkSize(3, 18d, 4d);

		pg.getDimension().setHeight(Units.inch(height));
		Assert.assertEquals(height, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);

		checkSize(2, 6.0, 10.0, 7.0, null);
		checkPosition(2, 0.0, 6.0, "Trapeze");
		checkSize(1, 6.0, 13.0, 10.0, null);
		checkPosition(1, 6.0, 3.0, "Trapeze");
		checkSize(0, 6.0, 16.0, 13.0, null);
		checkPosition(0, 12.0, 0.0, "Trapeze");
		checkSize(3, 18.0, 4.0, null, null);
		checkPosition(3, 0.0, 16.0, "Rectangle");

		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT);
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the height decrease with override
	 */
	public void heightDecreaseWithOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		pg.setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double height = 16d;
		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setHeight(Units.inch(4d));
		checkSize(2, 6d, 8d, 5d);
		checkSize(1, 6d, 11d, 8d);
		checkSize(0, 6d, 14d, 11d);
		checkSize(3, 18d, 4d);

		pg.getDimension().setHeight(Units.inch(height));

		Assert.assertEquals(height, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		checkSize(2, 6.0, 6.0, 3.0, null);
		checkPosition(2, 0.0, 6.0, "Trapeze");
		checkSize(1, 6.0, 9.0, 6.0, null);
		checkPosition(1, 6.0, 3.0, "Trapeze");
		checkSize(0, 6.0, 12.0, 9.0, null);
		checkPosition(0, 12.0, 0.0, "Trapeze");
		checkSize(3, 18.0, 4.0, null, null);
		checkPosition(3, 0.0, 12.0, "Rectangle");

		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT);
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the width increase with override
	 */
	public void widthIncreaseWithOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		pg.setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double width = 20d;
		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().setWidth(Units.inch(4d));
		checkSize(2, 7d, 6.5d, 3d);
		checkSize(1, 4d, 8.5d, 6.5d);
		checkSize(0, 7d, 12d, 8.5d);
		checkSize(3, 18d, 6d);

		pg.getDimension().setWidth(Units.inch(width));
		Assert.assertEquals(width, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		checkSize(0, 8.0, 12.0, null, null);
		checkPosition(0, 12.0, 0.0, "Trapeze");
		checkSize(1, 4.0, 8.0, null, null);
		checkPosition(1, 8.0, 4.0, "Trapeze");
		checkSize(2, 8.0, 6.0, 5.5, null);
		checkPosition(2, 0.0, 6.0, "Trapeze");
		checkSize(3, 20.0, 6.0, null, null);
		checkPosition(3, 0.0, 12.0, "Rectangle");

		pg.getDimension().removeWidth();
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().removeWidth();
		verifyInitial();
	}

	@Test
	/**
	 * Verify the width decrease with override
	 */
	public void widthDecreaseWithOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		pg.setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double width = 16d;
		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().setWidth(Units.inch(4d));

		checkSize(2, 7d, 6.5d, 3d);
		checkSize(1, 4d, 8.5d, 6.5d);
		checkSize(0, 7d, 12d, 8.5d);
		checkSize(3, 18d, 6d);

		pg.getDimension().setWidth(Units.inch(width));
		Assert.assertEquals(width, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);

		printAll();
		checkSize(0, 6.0, 12.0, null, null);
		checkPosition(0, 10.0, 0.0, "Trapeze");
		checkSize(1, 4.0, 9.0, 7.9375, null);
		checkPosition(1, 6.0, 3.0, "Trapeze");
		checkSize(2, 6.0, 7.0, 5.5, null);
		checkPosition(2, 0.0, 5.0, "Trapeze");
		checkSize(3, 16.0, 6.0, null, null);
		checkPosition(3, 0.0, 12.0, "Rectangle");

		pg.getDimension().removeWidth();
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		verifyInitial();
	}

	@Ignore
	@Test
	/**
	 * Verify the height change to the minimum
	 */
	public void heightChangeMinTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		pg.setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double height = 1d;
		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(height));
		Assert.assertFalse(pg.getDimension().getForm().isDefined());

		pg.getDimension().removeHeight();
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		verifyInitial();
	}

	@Ignore
	@Test
	/**
	 * Verify the width change to the minimum
	 */
	public void widthChangeMinTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		pg.setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double width = 1d;
		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(width));
		Assert.assertEquals(3d, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		checkSize(2, 1.0, 11.3333334, 11.0, null);
		checkPosition(2, 0.0, 0.66666667, "Trapeze");
		checkSize(1, 1.0, 11.6666667, 11.333334, null);
		checkPosition(1, 1.0, 0.3333333, "Trapeze");
		checkSize(0, 1.0, 12.0, 11.666667, null);
		checkPosition(0, 2.0, 0.0, "Trapeze");
		checkSize(3, 3.0, 6.0, null, null);
		checkPosition(3, 0.0, 12.0, "Rectangle");

		pg.getDimension().removeWidth();
		pg.getDimension().setHeight2(Units.inch(SIZE_OVERALL / 2d));
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		verifyInitial();
	}

	/**
	 * Split a trapeze into 4 pieces:
	 * 
	 * | | | 2 | | | | 1 | | | | | | | 0 | | | |----------------------------| | 3 | |----------------------------|
	 */
	private void splitTrapeze()
	{
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 12d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(6d, 7d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(12d, 7d));
		Assert.assertEquals(4, pg.getConfigurables().size());

		initProperties();
		verifyInitial();
	}

	/**
	 * Verify that all the components have the initial sizes and positions.
	 */
	private void verifyInitial()
	{
		checkSize(2, 6d, HEIGHT_2, HEIGHT_1);
		checkSize(1, 6d, HEIGHT_3, HEIGHT_2);
		checkSize(0, 6d, HEIGHT_4, HEIGHT_3);
		checkSize(3, 18d, 6d);

		checkPosition(2, 0d, HEIGHT_4 - HEIGHT_2, "Trapeze");
		checkPosition(1, 6d, HEIGHT_4 - HEIGHT_3, "Trapeze");
		checkPosition(0, 12d, 0d, "Trapeze");
		checkPosition(3, 0d, 12d, "Rectangle");
	}
}
