package ProductGroup;

import java.awt.geom.Point2D;

import junit.framework.Assert;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.netappsid.commonutils.geo.IdentifiableShape.Attribute;
import com.netappsid.commonutils.geo.enums.Orientation;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.common.Configurable;
import com.netappsid.erp.configurator.gui.Configurator;
import com.netappsid.wadconfigurator.Assembly;
import com.netappsid.wadconfigurator.ProductGroup;
import com.netappsid.wadconfigurator.ProductTemplate;
import com.netappsid.wadconfigurator.enums.ChoiceForm;

/**
 * PRODUCT GROUP TEST for Half arc quarter circle
 * 
 * @author jcfortier
 */
public class NoFlipHalfArcQuarterCircleTest extends TestUtils
{
	@BeforeClass
	public static void init() throws Exception
	{}

	@AfterClass
	public static void tearDown()
	{}

	@Before
	public void beforeTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_QC", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		initProperties();

		pg.setForm(ChoiceForm.F09_ELONGATEDHALFARC);
		resize(20d);

		// pg.setQuarterCircle();

		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);
	}

	@Test
	/**
	 * Verify the split of a Half arc quarter circle
	 */
	public void splitHalfArcTest()
	{
		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(5d, 16d));
		Assert.assertEquals(2, pg.getConfigurables().size());

		// printAll();

		checkSize(0, 5.0, 13.228756555322953, 0.0, null);
		checkPosition(0, 0.0, 6.771243444677047, "HalfArc");
		checkRay(0, 20.0);

		checkSize(1, 15.0, 20.0, 13.228756555322953, null);
		checkPosition(1, 5.0, 0.0, "ElongatedHalfArc");
		checkRay(1, 20.0);

		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(10d, 10d));
		Assert.assertEquals(3, pg.getConfigurables().size());

		// printAll();

		checkSize(0, 5.0, 13.228756555322953, 0.0, null);
		checkPosition(0, 0.0, 6.771243444677047, "HalfArc");
		checkRay(0, 20.0);

		checkSize(1, 15.0, 10.0, 3.2287565553229527, null);
		checkPosition(1, 5.0, 0.0, "ElongatedHalfArc");
		checkRay(1, 20.0);

		checkSize(2, 15.0, 10.0);
		checkPosition(2, 5.0, 10.0, "Rectangle");
	}

	@Test
	/**
	 * Verify the resizing of a Half arc quarter circle
	 */
	public void resizeHalfArcTest()
	{
		splitHalfArc();

		// -----------------------------------------------------------------------
		((ProductTemplate) pg.getConfigurables().get(2)).getDimension().setHeight(Units.inch(13d));

		// printAll();

		checkSize(0, 10.0, 17.320508075688775, 0.0, null);
		checkPosition(0, 0.0, 2.6794919243112254, "HalfArc");
		checkRay(0, 20.0);

		checkSize(1, 10.0, 7.0, 4.320508075688771, null);
		checkPosition(1, 10.0, 0.0, "ElongatedHalfArc");
		checkRay(1, 20.0);

		checkSize(2, 10.0, 13.0);
		checkPosition(2, 10.0, 7.0, "Rectangle");

		// ((ProductTemplate)pg.getConfigurables().get(2)).getDimension().removeHeight()

		// -----------------------------------------------------------------------
		((ProductTemplate) pg.getConfigurables().get(0)).getDimension().setWidth(Units.inch(6d));

		// printAll();

		checkSize(0, 6.0, 14.2828568570857, 0.0, null);
		checkPosition(0, 0.0, 5.717143142914301, "HalfArc");
		checkRay(0, 20.0);

		checkSize(1, 14.0, 7.0, 1.2828568570856973, null);
		checkPosition(1, 6.0, 0.0, "ElongatedHalfArc");
		checkRay(1, 20.0);

		checkSize(2, 14.0, 13.0);
		checkPosition(2, 6.0, 7.0, "Rectangle");

		// -----------------------------------------------------------------------
		((ProductTemplate) pg.getConfigurables().get(2)).getDimension().setHeight(Units.inch(15d));

		// printAll();

		checkSize(0, 6.0, 14.2828568570857, 0.0, null);
		checkPosition(0, 0.0, 5.717143142914299, "HalfArc");
		checkRay(0, 20.0);

		checkSize(1, 14.0, 6.717143142914308, 1.0, null);
		checkPosition(1, 6.0, 0.0, "ElongatedHalfArc");
		checkRay(1, 20.0);

		checkSize(2, 14.0, 13.282856857085697);
		checkPosition(2, 6.0, 6.717143142914303, "Rectangle");
	}

	@Test
	/**
	 * Verify the resizing of a Half arc quarter circle group
	 */
	public void resizeGroupHalfArcTest()
	{
		splitHalfArc();

		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(30d));

		// printAll();

		checkSize(0, 15.0, 16.331406486676983, 2.679491924311229, null);
		checkPosition(0, 0.0, 3.6685935133230174, "HalfArc");
		checkRay(0, 32.500000000000064);

		checkSize(1, 15.0, 10.0, 6.331406486676983, null);
		checkPosition(1, 15.0, 0.0, "ElongatedHalfArc");
		checkRay(1, 32.501);

		checkSize(2, 15.0, 10.0);
		checkPosition(2, 15.0, 10.0, "Rectangle");

		pg.getDimension().removeWidth();

		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(15d));

		// printAll();

		checkSize(0, 7.5, 17.67949192431123, 17.547566009176418, null);
		checkPosition(0, 0.0, 2.320508075688771, "HalfArc");
		checkRay(0, 212.8050000000441);

		checkSize(1, 7.5, 10.0, 7.547566009176414, null);
		checkPosition(1, 7.5, 0.0, "ElongatedHalfArc");
		checkRay(1, 32.501);

		checkSize(2, 7.5, 10.0);
		checkPosition(2, 7.5, 10.0, "Rectangle");

		pg.getDimension().removeWidth();

		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(30d));

		// printAll();

		checkSize(0, 10.0, 27.033184435194755, 22.67949192431123, null);
		checkPosition(0, 0.0, 2.9668155648052448, "HalfArc");
		checkRay(0, 87.35200000000246);

		checkSize(1, 10.0, 15.0, 12.033184435194755, null);
		checkPosition(1, 10.0, 0.0, "ElongatedHalfArc");
		checkRay(1, 87.35200000000037);

		checkSize(2, 10.0, 15.0);
		checkPosition(2, 10.0, 15.0, "Rectangle");

		pg.getDimension().removeHeight();

		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(15d));

		// printAll();

		checkSize(0, 10.0, 12.417611905531913, 8.5, null);
		checkPosition(0, 0.0, 2.5823880944680866, "HalfArc");
		checkRay(0, 87.35200000000312);
		checkSize(1, 10.0, 7.5, 4.917611905531913, null);
		checkPosition(1, 9.999999999999993, 0.0, "ElongatedHalfArc");
		checkRay(1, 87.35200000000101);
		checkSize(2, 10.0, 7.5, null, null);
		checkPosition(2, 10.0, 7.5, "Rectangle");

		pg.getDimension().removeHeight();

		// -----------------------------------------------------------------------

	}

	/**
	 * Split the HalfArc in 3. Verify the result.
	 */
	private void splitHalfArc()
	{
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(10d, 10d));
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(15d, 10d));
		Assert.assertEquals(3, pg.getConfigurables().size());

		// printAll();

		verifyInitial();
	}

	/**
	 * Verify the results from the HalfArc split
	 */
	private void verifyInitial()
	{
		checkSize(0, 10.0, 17.320508075688775, 0.0, null);
		checkPosition(0, 0.0, 2.6794919243112254, "HalfArc");
		checkRay(0, 20.0);

		checkSize(1, 10.0, 10.0, 7.320508075688775, null);
		checkPosition(1, 10.0, 0.0, "ElongatedHalfArc");
		checkRay(1, 20.0);

		checkSize(2, 10.0, 10.0);
		checkPosition(2, 10.0, 10.0, "Rectangle");
	}

	/**
	 * Resize the ProductGroup with the specified value.
	 * 
	 * @param size
	 */
	protected void resize(Double size)
	{
		pg.getDimension().setDefaultHeight(Units.inch(size));

		// Modify Ray value to be minimum valid Ray in order to keep form valid
		double minRay = pg.getDimension().getForm().getValue(Attribute.RAY);

		pg.getDimension().setDefaultRay(Units.inch(minRay));

	}

}
