package ProductGroup;

import java.awt.geom.Point2D;

import junit.framework.Assert;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.client360.configuration.wad.Dimension;
import com.netappsid.commonutils.geo.enums.Orientation;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.common.Configurable;
import com.netappsid.configuration.common.Dimensionnable;
import com.netappsid.erp.configurator.gui.Configurator;
import com.netappsid.wadconfigurator.Assembly;
import com.netappsid.wadconfigurator.ProductGroup;
import com.netappsid.wadconfigurator.enums.ChoiceForm;

/**
 * PRODUCT GROUP TEST for group resize rectangles (overall resizes)
 * 
 * @author sboule
 */
public class NoFlipRectangleGroupResizeTest extends TestUtils
{
	@BeforeClass
	public static void init() throws Exception
	{}

	@AfterClass
	public static void tearDown()
	{}

	@Test
	/**
	 * Verify the height increase
	 */
	public void heightIncreaseNoOverrideTest1()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F02_RECTANGLE);

		splitRectangleVerti();
		verifyInitialVerti();

		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(20d));
		Assert.assertEquals(20d, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		checkSize(0, null, 6d);
		checkSize(1, null, 6d);
		checkSize(2, null, 8d);
		checkSize(3, null, 4d);
		checkSize(4, null, 8d);
		checkSize(5, null, 4d);
		checkSize(6, null, 6d);
		checkSize(7, null, 6d);

		checkPosition(0, 0d, 0d, "Rectangle");
		checkPosition(1, 2d, 0d, "Rectangle");
		checkPosition(2, 0d, 6d, "Rectangle");
		checkPosition(3, 3d, 6d, "Rectangle");
		checkPosition(4, 7d, 6d, "Rectangle");
		checkPosition(5, 3d, 10d, "Rectangle");
		checkPosition(6, 0d, 14d, "Rectangle");
		checkPosition(7, 8d, 14d, "Rectangle");
		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT);
		Assert.assertEquals(10d, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		verifyInitialVerti();
	}

	@Test
	/**
	 * Verify the width increase
	 */
	public void widthIncreaseNoOverrideTest1()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F02_RECTANGLE);

		splitRectangleVerti();
		verifyInitialVerti();

		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(20d));
		Assert.assertEquals(20d, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		checkSize(0, 4d, 3d);
		checkSize(1, 16d, 3d);
		checkSize(2, 6d, 4d);
		checkSize(3, 8d, 2d);
		checkSize(4, 6d, 4d);
		checkSize(5, 8d, 2d);
		checkSize(6, 16d, 3d);
		checkSize(7, 4d, 3d);

		checkPosition(0, 0d, 0d, "Rectangle");
		checkPosition(1, 4d, 0d, "Rectangle");
		checkPosition(2, 0d, 3d, "Rectangle");
		checkPosition(3, 6d, 3d, "Rectangle");
		checkPosition(4, 14d, 3d, "Rectangle");
		checkPosition(5, 6d, 5d, "Rectangle");
		checkPosition(6, 0d, 7d, "Rectangle");
		checkPosition(7, 16d, 7d, "Rectangle");
		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		Assert.assertEquals(10d, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		verifyInitialVerti();
	}

	@Test
	/**
	 * Verify the height increase
	 */
	public void heightIncreaseNoOverrideTest2()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F02_RECTANGLE);

		splitRectangleHoriz();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(20d));
		Assert.assertEquals(20d, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		checkSize(0, null, 16d);
		checkSize(1, null, 8d);
		checkSize(2, null, 4d);
		checkSize(3, null, 16d);
		checkSize(4, null, 6d);
		checkSize(5, null, 6d);
		checkSize(6, null, 6d);
		checkSize(7, null, 4d);

		checkPosition(0, 0d, 0d, "Rectangle");
		checkPosition(1, 2d, 0d, "Rectangle");
		checkPosition(2, 8d, 0d, "Rectangle");
		checkPosition(3, 8d, 4d, "Rectangle");
		checkPosition(4, 2d, 8d, "Rectangle");
		checkPosition(5, 5d, 8d, "Rectangle");
		checkPosition(6, 2d, 14d, "Rectangle");
		checkPosition(7, 0d, 16d, "Rectangle");
		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT);
		Assert.assertEquals(10d, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		verifyInitialHoriz();
	}

	@Test
	/**
	 * Verify the width increase
	 */
	public void widthIncreaseNoOverrideTest2()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F02_RECTANGLE);

		splitRectangleHoriz();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(20d));
		Assert.assertEquals(20d, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		checkSize(0, 4d, 8d);
		checkSize(1, 12d, 4d);
		checkSize(2, 4d, 2d);
		checkSize(3, 4d, 8d);
		checkSize(4, 6d, 3d);
		checkSize(5, 6d, 3d);
		checkSize(6, 12d, 3d);
		checkSize(7, 4d, 2d);

		checkPosition(0, 0d, 0d, "Rectangle");
		checkPosition(1, 4d, 0d, "Rectangle");
		checkPosition(2, 16d, 0d, "Rectangle");
		checkPosition(3, 16d, 2d, "Rectangle");
		checkPosition(4, 4d, 4d, "Rectangle");
		checkPosition(5, 10d, 4d, "Rectangle");
		checkPosition(6, 4d, 7d, "Rectangle");
		checkPosition(7, 0d, 8d, "Rectangle");
		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		Assert.assertEquals(10d, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		verifyInitialHoriz();
	}

	@Test
	/**
	 * Verify the height decrease
	 */
	public void heightDecreaseNoOverrideTest1()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F02_RECTANGLE);

		splitRectangleVerti();
		verifyInitialVerti();

		pg.getDimension().setHeight(Units.inch(5d));
		Assert.assertEquals(5d, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		checkSize(0, null, 1.5d);
		checkSize(1, null, 1.5d);
		checkSize(2, null, 2d);
		checkSize(3, null, 1d);
		checkSize(4, null, 2d);
		checkSize(5, null, 1d);
		checkSize(6, null, 1.5d);
		checkSize(7, null, 1.5d);

		checkPosition(0, 0d, 0d, "Rectangle");
		checkPosition(1, 2d, 0d, "Rectangle");
		checkPosition(2, 0d, 1.5d, "Rectangle");
		checkPosition(3, 3d, 1.5d, "Rectangle");
		checkPosition(4, 7d, 1.5d, "Rectangle");
		checkPosition(5, 3d, 2.5d, "Rectangle");
		checkPosition(6, 0d, 3.5d, "Rectangle");
		checkPosition(7, 8d, 3.5d, "Rectangle");
		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT);
		Assert.assertEquals(10d, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		verifyInitialVerti();
	}

	@Test
	/**
	 * Verify the width decrease
	 */
	public void widthDecreaseNoOverrideTest1()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F02_RECTANGLE);

		splitRectangleVerti();
		verifyInitialVerti();

		pg.getDimension().setWidth(Units.inch(5d));
		Assert.assertEquals(5d, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		checkSize(0, 1d, 3d);
		checkSize(1, 4d, 3d);
		checkSize(2, 1.5d, 4d);
		checkSize(3, 2d, 2d);
		checkSize(4, 1.5d, 4d);
		checkSize(5, 2d, 2d);
		checkSize(6, 4d, 3d);
		checkSize(7, 1d, 3d);

		checkPosition(0, 0d, 0d, "Rectangle");
		checkPosition(1, 1d, 0d, "Rectangle");
		checkPosition(2, 0d, 3d, "Rectangle");
		checkPosition(3, 1.5d, 3d, "Rectangle");
		checkPosition(4, 3.5d, 3d, "Rectangle");
		checkPosition(5, 1.5d, 5d, "Rectangle");
		checkPosition(6, 0d, 7d, "Rectangle");
		checkPosition(7, 4d, 7d, "Rectangle");
		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		Assert.assertEquals(10d, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		verifyInitialVerti();
	}

	@Test
	/**
	 * Verify the height decrease
	 */
	public void heightDecreaseNoOverrideTest2()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F02_RECTANGLE);

		splitRectangleHoriz();
		verifyInitialHoriz();

		pg.getDimension().setHeight(Units.inch(5d));
		Assert.assertEquals(5d, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		checkSize(0, null, 4d);
		checkSize(1, null, 2d);
		checkSize(2, null, 1d);
		checkSize(3, null, 4d);
		checkSize(4, null, 1.5d);
		checkSize(5, null, 1.5d);
		checkSize(6, null, 1.5d);
		checkSize(7, null, 1d);

		checkPosition(0, 0d, 0d, "Rectangle");
		checkPosition(1, 2d, 0d, "Rectangle");
		checkPosition(2, 8d, 0d, "Rectangle");
		checkPosition(3, 8d, 1d, "Rectangle");
		checkPosition(4, 2d, 2d, "Rectangle");
		checkPosition(5, 5d, 2d, "Rectangle");
		checkPosition(6, 2d, 3.5d, "Rectangle");
		checkPosition(7, 0d, 4d, "Rectangle");
		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT);
		Assert.assertEquals(10d, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		verifyInitialHoriz();
	}

	@Test
	/**
	 * Verify the width decrease
	 */
	public void widthDecreaseNoOverrideTest2()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F02_RECTANGLE);

		splitRectangleHoriz();
		verifyInitialHoriz();

		pg.getDimension().setWidth(Units.inch(5d));
		Assert.assertEquals(5d, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		checkSize(0, 1d, 8d);
		checkSize(1, 3d, 4d);
		checkSize(2, 1d, 2d);
		checkSize(3, 1d, 8d);
		checkSize(4, 1.5d, 3d);
		checkSize(5, 1.5d, 3d);
		checkSize(6, 3d, 3d);
		checkSize(7, 1d, 2d);

		checkPosition(0, 0d, 0d, "Rectangle");
		checkPosition(1, 1d, 0d, "Rectangle");
		checkPosition(2, 4d, 0d, "Rectangle");
		checkPosition(3, 4d, 2d, "Rectangle");
		checkPosition(4, 1d, 4d, "Rectangle");
		checkPosition(5, 2.5d, 4d, "Rectangle");
		checkPosition(6, 1d, 7d, "Rectangle");
		checkPosition(7, 0d, 8d, "Rectangle");
		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		Assert.assertEquals(10d, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		verifyInitialHoriz();
	}

	@Test
	/**
	 * Verify the height change with override
	 */
	public void heightChangeWithOverrideTest1()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F02_RECTANGLE);

		splitRectangleVerti();
		verifyInitialVerti();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setHeight(Units.inch(4d));
		pg.getDimension().setHeight(Units.inch(20d));
		Assert.assertEquals(20d, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		checkSize(0, null, 4d);
		checkSize(1, null, 4d);
		checkSize(2, null, 10d);
		checkSize(3, null, 4d);
		checkSize(4, null, 10d);
		checkSize(5, null, 6d);
		checkSize(6, null, 6d);
		checkSize(7, null, 6d);

		checkPosition(0, 0d, 0d, "Rectangle");
		checkPosition(1, 2d, 0d, "Rectangle");
		checkPosition(2, 0d, 4d, "Rectangle");
		checkPosition(3, 3d, 4d, "Rectangle");
		checkPosition(4, 7d, 4d, "Rectangle");
		checkPosition(5, 3d, 8d, "Rectangle");
		checkPosition(6, 0d, 14d, "Rectangle");
		checkPosition(7, 8d, 14d, "Rectangle");
		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT);
		Assert.assertEquals(10d, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT);
		verifyInitialVerti();
	}

	@Test
	/**
	 * Verify the width change with override
	 */
	public void widthChangeWithOverrideTest1()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F02_RECTANGLE);

		splitRectangleVerti();
		verifyInitialVerti();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setWidth(Units.inch(2d));
		pg.getDimension().setWidth(Units.inch(20d));
		Assert.assertEquals(20d, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		checkSize(0, 4d, 3d);
		checkSize(1, 16d, 3d);
		checkSize(2, 9d, 4d);
		checkSize(3, 2d, 2d);
		checkSize(4, 9d, 4d);
		checkSize(5, 2d, 2d);
		checkSize(6, 16d, 3d);
		checkSize(7, 4d, 3d);

		checkPosition(0, 0d, 0d, "Rectangle");
		checkPosition(1, 4d, 0d, "Rectangle");
		checkPosition(2, 0d, 3d, "Rectangle");
		checkPosition(3, 9d, 3d, "Rectangle");
		checkPosition(4, 11d, 3d, "Rectangle");
		checkPosition(5, 9d, 5d, "Rectangle");
		checkPosition(6, 0d, 7d, "Rectangle");
		checkPosition(7, 16d, 7d, "Rectangle");
		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		Assert.assertEquals(10d, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		verifyInitialVerti();
	}

	@Test
	/**
	 * Verify the height change with override
	 */
	public void heightChangeWithOverrideTest2()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F02_RECTANGLE);

		splitRectangleHoriz();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(5)).getDimension().setHeight(Units.inch(4d));
		pg.getDimension().setHeight(Units.inch(20d));
		Assert.assertEquals(20d, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		checkSize(0, null, 16d);
		checkSize(1, null, 9.333333d);
		checkSize(2, null, 4d);
		checkSize(3, null, 16d);
		checkSize(4, null, 4d);
		checkSize(5, null, 4d);
		checkSize(6, null, 6.666666d);
		checkSize(7, null, 4d);

		checkPosition(0, 0d, 0d, "Rectangle");
		checkPosition(1, 2d, 0d, "Rectangle");
		checkPosition(2, 8d, 0d, "Rectangle");
		checkPosition(3, 8d, 4d, "Rectangle");
		checkPosition(4, 2d, 9.33333d, "Rectangle");
		checkPosition(5, 5d, 9.33333d, "Rectangle");
		checkPosition(6, 2d, 13.33333d, "Rectangle");
		checkPosition(7, 0d, 16d, "Rectangle");
		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT);
		Assert.assertEquals(10d, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		((Dimensionnable) pg.getConfigurables().get(5)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT);
		verifyInitialHoriz();
	}

	@Test
	/**
	 * Verify the width change with override
	 */
	public void widthChangeWithOverrideTest2()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F02_RECTANGLE);

		splitRectangleHoriz();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(5)).getDimension().setWidth(Units.inch(5d));
		pg.getDimension().setWidth(Units.inch(20d));
		Assert.assertEquals(20d, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		checkSize(0, 4d, 8d);
		checkSize(1, 14d, 4d);
		checkSize(2, 2d, 2d);
		checkSize(3, 2d, 8d);
		checkSize(4, 9d, 3d);
		checkSize(5, 5d, 3d);
		checkSize(6, 14d, 3d);
		checkSize(7, 4d, 2d);

		checkPosition(0, 0d, 0d, "Rectangle");
		checkPosition(1, 4d, 0d, "Rectangle");
		checkPosition(2, 18d, 0d, "Rectangle");
		checkPosition(3, 18d, 2d, "Rectangle");
		checkPosition(4, 4d, 4d, "Rectangle");
		checkPosition(5, 13d, 4d, "Rectangle");
		checkPosition(6, 4d, 7d, "Rectangle");
		checkPosition(7, 0d, 8d, "Rectangle");
		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		Assert.assertEquals(10d, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		((Dimensionnable) pg.getConfigurables().get(5)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		verifyInitialHoriz();
	}

	@Test
	/**
	 * Verify the height change to the minimum (each component cannot be smaller than 1").
	 */
	public void heightChangeMinTest1()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F02_RECTANGLE);

		splitRectangleVerti();
		verifyInitialVerti();

		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(2d));
		Assert.assertEquals(4d, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		checkSize(0, null, 1d);
		checkSize(1, null, 1d);
		checkSize(2, null, 2d);
		checkSize(3, null, 1d);
		checkSize(4, null, 2d);
		checkSize(5, null, 1d);
		checkSize(6, null, 1d);
		checkSize(7, null, 1d);

		checkPosition(0, 0d, 0d, "Rectangle");
		checkPosition(1, 2d, 0d, "Rectangle");
		checkPosition(2, 0d, 1d, "Rectangle");
		checkPosition(3, 3d, 1d, "Rectangle");
		checkPosition(4, 7d, 1d, "Rectangle");
		checkPosition(5, 3d, 2d, "Rectangle");
		checkPosition(6, 0d, 3d, "Rectangle");
		checkPosition(7, 8d, 3d, "Rectangle");

		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT);
		checkSize(0, null, 2.5d);
		checkSize(1, null, 2.5d);
		checkSize(2, null, 5d);
		checkSize(3, null, 2.5d);
		checkSize(4, null, 5d);
		checkSize(5, null, 2.5d);
		checkSize(6, null, 2.5d);
		checkSize(7, null, 2.5d);

		checkPosition(0, 0d, 0d, "Rectangle");
		checkPosition(1, 2d, 0d, "Rectangle");
		checkPosition(2, 0d, 2.5d, "Rectangle");
		checkPosition(3, 3d, 2.5d, "Rectangle");
		checkPosition(4, 7d, 2.5d, "Rectangle");
		checkPosition(5, 3d, 5d, "Rectangle");
		checkPosition(6, 0d, 7.5d, "Rectangle");
		checkPosition(7, 8d, 7.5d, "Rectangle");
	}

	@Test
	/**
	 * Verify the width change to the minimum (each component cannot be smaller than 1").
	 */
	public void widthChangeMinTest1()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F02_RECTANGLE);

		splitRectangleVerti();
		verifyInitialVerti();

		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(2d));
		Assert.assertEquals(3d, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		checkSize(0, 1d, 3d);
		checkSize(1, 2d, 3d);
		checkSize(2, 1d, 4d);
		checkSize(3, 1d, 2d);
		checkSize(4, 1d, 4d);
		checkSize(5, 1d, 2d);
		checkSize(6, 2d, 3d);
		checkSize(7, 1d, 3d);

		checkPosition(0, 0d, 0d, "Rectangle");
		checkPosition(1, 1d, 0d, "Rectangle");
		checkPosition(2, 0d, 3d, "Rectangle");
		checkPosition(3, 1d, 3d, "Rectangle");
		checkPosition(4, 2d, 3d, "Rectangle");
		checkPosition(5, 1d, 5d, "Rectangle");
		checkPosition(6, 0d, 7d, "Rectangle");
		checkPosition(7, 2d, 7d, "Rectangle");

		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		checkSize(0, 3.3333d, 3d);
		checkSize(1, 6.6667d, 3d);
		checkSize(2, 3.3333d, 4d);
		checkSize(3, 3.3333d, 2d);
		checkSize(4, 3.3333d, 4d);
		checkSize(5, 3.3333d, 2d);
		checkSize(6, 6.6667d, 3d);
		checkSize(7, 3.3333d, 3d);

		checkPosition(0, 0d, 0d, "Rectangle");
		checkPosition(1, 3.3333d, 0d, "Rectangle");
		checkPosition(2, 0d, 3d, "Rectangle");
		checkPosition(3, 3.3333d, 3d, "Rectangle");
		checkPosition(4, 6.6667d, 3d, "Rectangle");
		checkPosition(5, 3.3333d, 5d, "Rectangle");
		checkPosition(6, 0d, 7d, "Rectangle");
		checkPosition(7, 6.6667d, 7d, "Rectangle");
	}

	@Test
	/**
	 * Verify the height change to the minimum (each component cannot be smaller than 1").
	 */
	public void heightChangeMinTest2()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F02_RECTANGLE);

		splitRectangleHoriz();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(2d));
		Assert.assertEquals(3d, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		checkSize(0, null, 2d);
		checkSize(1, null, 1d);
		checkSize(2, null, 1d);
		checkSize(3, null, 1d); // was 4
		checkSize(4, null, 1d); // was 5
		checkSize(5, null, 2d); // was 3
		checkSize(6, null, 1d); // was 7
		checkSize(7, null, 1d); // was 6

		checkPosition(0, 0d, 0d, "Rectangle");
		checkPosition(1, 2d, 0d, "Rectangle");
		checkPosition(2, 8d, 0d, "Rectangle");
		checkPosition(3, 2d, 1d, "Rectangle");
		checkPosition(4, 5d, 1d, "Rectangle");
		checkPosition(5, 8d, 1d, "Rectangle");
		checkPosition(6, 0d, 2d, "Rectangle");
		checkPosition(7, 2d, 2d, "Rectangle");

		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT);
		checkSize(0, null, 6.6667d);
		checkSize(1, null, 3.3333d);
		checkSize(2, null, 3.3333d);
		checkSize(3, null, 3.3333d);
		checkSize(4, null, 3.3333d);
		checkSize(5, null, 6.6667d);
		checkSize(6, null, 3.3333d);
		checkSize(7, null, 3.3333d);

		checkPosition(0, 0d, 0d, "Rectangle");
		checkPosition(1, 2d, 0d, "Rectangle");
		checkPosition(2, 8d, 0d, "Rectangle");
		checkPosition(3, 2d, 3.3333d, "Rectangle");
		checkPosition(4, 5d, 3.3333d, "Rectangle");
		checkPosition(5, 8d, 3.3333d, "Rectangle");
		checkPosition(6, 0d, 6.6667d, "Rectangle");
		checkPosition(7, 2d, 6.6667d, "Rectangle");
	}

	@Test
	/**
	 * Verify the width change to the minimum (each component cannot be smaller than 1").
	 */
	public void widthChangeMinTest2()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F02_RECTANGLE);

		splitRectangleHoriz();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(2d));
		Assert.assertEquals(4d, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		checkSize(0, 1d, 8d);
		checkSize(1, 2d, 4d);
		checkSize(2, 1d, 2d);
		checkSize(3, 1d, 8d);
		checkSize(4, 1d, 3d);
		checkSize(5, 1d, 3d);
		checkSize(6, 2d, 3d);
		checkSize(7, 1d, 2d);

		checkPosition(0, 0d, 0d, "Rectangle");
		checkPosition(1, 1d, 0d, "Rectangle");
		checkPosition(2, 3d, 0d, "Rectangle");
		checkPosition(3, 3d, 2d, "Rectangle");
		checkPosition(4, 1d, 4d, "Rectangle");
		checkPosition(5, 2d, 4d, "Rectangle");
		checkPosition(6, 1d, 7d, "Rectangle");
		checkPosition(7, 0d, 8d, "Rectangle");

		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		checkSize(0, 2.5d, 8d);
		checkSize(1, 5d, 4d);
		checkSize(2, 2.5d, 2d);
		checkSize(3, 2.5d, 8d);
		checkSize(4, 2.5d, 3d);
		checkSize(5, 2.5d, 3d);
		checkSize(6, 5d, 3d);
		checkSize(7, 2.5d, 2d);

		checkPosition(0, 0d, 0d, "Rectangle");
		checkPosition(1, 2.5d, 0d, "Rectangle");
		checkPosition(2, 7.5d, 0d, "Rectangle");
		checkPosition(3, 7.5d, 2d, "Rectangle");
		checkPosition(4, 2.5d, 4d, "Rectangle");
		checkPosition(5, 5d, 4d, "Rectangle");
		checkPosition(6, 2.5d, 7d, "Rectangle");
		checkPosition(7, 0d, 8d, "Rectangle");
	}

	@Test
	/**
	 * Verify a split after a resize.
	 */
	public void splitAfterResizeTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F02_RECTANGLE);
		pg.getDimension().setDefaultWidth(Units.inch(20d));
		pg.getDimension().setDefaultHeight(Units.inch(20d));

		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(5d, 1d));
		Assert.assertEquals(2, pg.getConfigurables().size());

		pg.getDimension().setWidth(Units.inch(40d));

		checkSize(0, 10d, 20d);
		checkSize(1, 30d, 20d);

		checkPosition(0, 0d, 0d, "Rectangle");
		checkPosition(1, 10d, 0d, "Rectangle");

		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(30d, 1d));
		Assert.assertEquals(3, pg.getConfigurables().size());

		checkSize(0, 10d, 20d);
		checkSize(1, 20d, 20d);
		checkSize(2, 10d, 20d);

		checkPosition(0, 0d, 0d, "Rectangle");
		checkPosition(1, 10d, 0d, "Rectangle");
		checkPosition(2, 30d, 0d, "Rectangle");
	}

	/**
	 * Split a rectangle into 7 pieces: ------------------------ | 0 | 1 | 2 | | | |------| | | | 3 | | |---------| | | | 4 | 5 | | | |---------| | | | 6 | |
	 * |-----| | | | 7 | | | ------------------------
	 */
	private void splitRectangleHoriz()
	{
		initProperties();

		pg.getDimension().setDefaultWidth(Units.inch(10d));
		pg.getDimension().setDefaultHeight(Units.inch(10d));

		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(2d, 1d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(8d, 5d));
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 4d));
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 7d));
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(9d, 2d));
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(1d, 8d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(5d, 5d));
		Assert.assertEquals(8, pg.getConfigurables().size());

		initProperties();
	}

	/**
	 * Split a rectangle into 7 pieces: ------------------------| | 0 | 1 | |-----------------------| | | 3 | | | 2 |-----| 4 | | | 5 | |
	 * |-----------------------| | 6 | 7 | |-----------------------|
	 */
	private void splitRectangleVerti()
	{
		initProperties();

		pg.getDimension().setDefaultWidth(Units.inch(10d));
		pg.getDimension().setDefaultHeight(Units.inch(10d));

		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 3d));
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 7d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(2d, 1d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(3d, 5d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(7d, 5d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(8d, 8d));
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 5d));
		Assert.assertEquals(8, pg.getConfigurables().size());

		initProperties();
	}

	/**
	 * Verify that all the components have the initial sizes.
	 */
	private void verifyInitialHoriz()
	{
		checkSize(0, 2d, 8d);
		checkSize(1, 6d, 4d);
		checkSize(2, 2d, 2d);
		checkSize(3, 2d, 8d);
		checkSize(4, 3d, 3d);
		checkSize(5, 3d, 3d);
		checkSize(6, 6d, 3d);
		checkSize(7, 2d, 2d);

		checkPosition(0, 0d, 0d, "Rectangle");
		checkPosition(1, 2d, 0d, "Rectangle");
		checkPosition(2, 8d, 0d, "Rectangle");
		checkPosition(3, 8d, 2d, "Rectangle");
		checkPosition(4, 2d, 4d, "Rectangle");
		checkPosition(5, 5d, 4d, "Rectangle");
		checkPosition(6, 2d, 7d, "Rectangle");
		checkPosition(7, 0d, 8d, "Rectangle");
	}

	/**
	 * Verify that all the components have the initial sizes.
	 */
	private void verifyInitialVerti()
	{
		checkSize(0, 2d, 3d);
		checkSize(1, 8d, 3d);
		checkSize(2, 3d, 4d);
		checkSize(3, 4d, 2d);
		checkSize(4, 3d, 4d);
		checkSize(5, 4d, 2d);
		checkSize(6, 8d, 3d);
		checkSize(7, 2d, 3d);

		checkPosition(0, 0d, 0d, "Rectangle");
		checkPosition(1, 2d, 0d, "Rectangle");
		checkPosition(2, 0d, 3d, "Rectangle");
		checkPosition(3, 3d, 3d, "Rectangle");
		checkPosition(4, 7d, 3d, "Rectangle");
		checkPosition(5, 3d, 5d, "Rectangle");
		checkPosition(6, 0d, 7d, "Rectangle");
		checkPosition(7, 8d, 7d, "Rectangle");
	}
}
