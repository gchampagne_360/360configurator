package ProductGroup;

import java.awt.geom.Point2D;

import junit.framework.Assert;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.netappsid.commonutils.geo.enums.Orientation;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.common.Configurable;
import com.netappsid.configuration.common.Dimensionnable;
import com.netappsid.erp.configurator.gui.Configurator;
import com.netappsid.wadconfigurator.Assembly;
import com.netappsid.wadconfigurator.Dimension;
import com.netappsid.wadconfigurator.ProductGroup;
import com.netappsid.wadconfigurator.enums.ChoiceForm;

/**
 * PRODUCT GROUP TEST for group resize trapezes (overall resizes) Scenario C
 * 
 * @author sboule
 */
public class NoFlipTrapezeGroupResizeTestC extends TestUtils
{
	private static double SIZE_OVERALL = 18d;

	@BeforeClass
	public static void init() throws Exception
	{}

	@AfterClass
	public static void tearDown()
	{}

	@Test
	/**
	 * Verify the height increase
	 */
	public void heightIncreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double height = 20d;
		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(height));
		Assert.assertEquals(height, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		checkSize(0, 6.0, 7.333333333333334, 4.333333333333334, null);
		checkPosition(0, 0.0, 6.0, "Trapeze");
		checkSize(1, 6.0, 8.111111111111112, 5.1111111111111125, null);
		checkPosition(1, 6.0, 3.0, "Trapeze");
		checkSize(2, 6.0, 13.333333333333334, 10.333333333333334, null);
		checkPosition(2, 12.0, 0.0, "Trapeze");
		checkSize(3, 6.0, 2.2222222222222214, null, null);
		checkPosition(3, 6.0, 11.111111111111112, "Rectangle");
		checkSize(4, 18.0, 6.666666666666666, null, null);
		checkPosition(4, 0.0, 13.333333333333334, "Rectangle");

		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT);
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		// verifyInitial();
	}

	@Test
	/**
	 * Verify the height decrease
	 */
	public void heightDecreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double height = 16d;
		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(height));
		Assert.assertEquals(height, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		checkSize(0, 6.0, 4.66667, 1.66667, null);
		checkPosition(0, 0.0, 6.0, "Trapeze");
		checkSize(1, 6.0, 5.888885, 2.888888, null);
		checkPosition(1, 6.0, 3.0, "Trapeze");
		checkSize(2, 6.0, 10.666667, 7.66667, null);
		checkPosition(2, 12.0, 0.0, "Trapeze");
		checkSize(3, 6.0, 1.77777, null, null);
		checkPosition(3, 6.0, 8.88888, "Rectangle");
		checkSize(4, 18.0, 5.3333, null, null);
		checkPosition(4, 0.0, 10.666667, "Rectangle");

		pg.getDimension().removeHeight();
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);

		checkSize(0, 6.0, 6.0, 3.0, null);
		checkPosition(0, 0.0, 6.0, "Trapeze");
		checkSize(1, 6.0, 7.0, 4.0, null);
		checkPosition(1, 6.0, 3.0, "Trapeze");
		checkSize(2, 6.0, 12.0, 9.0, null);
		checkPosition(2, 12.0, 0.0, "Trapeze");
		checkSize(3, 6.0, 2.0, null, null);
		checkPosition(3, 6.0, 10.0, "Rectangle");
		checkSize(4, 18.0, 6.0, null, null);
		checkPosition(4, 0.0, 12.0, "Rectangle");
	}

	@Test
	/**
	 * Verify the height2 increase
	 */
	public void height2IncreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double height2 = 10d;
		// -----------------------------------------------------------------------
		pg.getDimension().setHeight2(Units.inch(height2));
		Assert.assertEquals(height2, pg.getDimension().getHeight2().doubleValue(Units.INCH), DELTA);
		checkSize(0, 6d, 6.66667d, 4d);
		checkSize(1, 6d, 7.33333d, 4.66667d);
		checkSize(2, 6d, 12d, 9.33333d);
		checkSize(3, 6d, 2d);
		checkSize(4, 18d, 6d);

		checkPosition(0, 0d, 5.33333d, "Trapeze");
		checkPosition(1, 6d, 2.66667d, "Trapeze");
		checkPosition(2, 12d, 0d, "Trapeze");
		checkPosition(3, 6d, 10d, "Rectangle");
		checkPosition(4, 0d, 12d, "Rectangle");

		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT2);
		Assert.assertEquals(SIZE_OVERALL / 2d, pg.getDimension().getHeight2().doubleValue(Units.INCH), DELTA);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the height2 decrease
	 */
	public void height2DecreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double height2 = 8d;
		// -----------------------------------------------------------------------
		pg.getDimension().setHeight2(Units.inch(height2));
		Assert.assertEquals(height2, pg.getDimension().getHeight2().doubleValue(Units.INCH), DELTA);
		checkSize(0, 6d, 5.33333d, 2d);
		checkSize(1, 6d, 6.66667d, 3.33333d);
		checkSize(2, 6d, 12d, 8.66667d);
		checkSize(3, 6d, 2d);
		checkSize(4, 18d, 6d);

		checkPosition(0, 0d, 6.66667d, "Trapeze");
		checkPosition(1, 6d, 3.33333d, "Trapeze");
		checkPosition(2, 12d, 0d, "Trapeze");
		checkPosition(3, 6d, 10d, "Rectangle");
		checkPosition(4, 0d, 12d, "Rectangle");

		pg.getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT2);
		Assert.assertEquals(SIZE_OVERALL / 2d, pg.getDimension().getHeight2().doubleValue(Units.INCH), DELTA);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the width increase
	 */
	public void widthIncreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double width = 21d;
		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(width));
		Assert.assertEquals(width, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		checkSize(0, 7.0, 8.0, 6.0, null);
		checkPosition(0, 0.0, 4.0, "Trapeze");
		checkSize(1, 7.0, 8.0, 6.0, null);
		checkPosition(1, 7.0, 2.0, "Trapeze");
		checkSize(2, 7.0, 12.0, 10.0, null);
		checkPosition(2, 14.0, 0.0, "Trapeze");
		checkSize(3, 7.0, 2.0, null, null);
		checkPosition(3, 7.0, 10.0, "Rectangle");
		checkSize(4, 21.0, 6.0, null, null);
		checkPosition(4, 0.0, 12.0, "Rectangle");

		pg.getDimension().removeWidth();
		pg.getDimension().setHeight2(Units.inch(SIZE_OVERALL / 2d));
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the width decrease
	 */
	public void widthDecreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double width = 15d;
		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(width));
		Assert.assertEquals(width, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		checkSize(0, 5.0, 8.0, 6.0, null);
		checkPosition(0, 0.0, 4.0, "Trapeze");
		checkSize(1, 5.0, 8.0, 6.0, null);
		checkPosition(1, 5.0, 2.0, "Trapeze");
		checkSize(2, 5.0, 12.0, 10.0, null);
		checkPosition(2, 10.0, 0.0, "Trapeze");
		checkSize(3, 5.0, 2.0, null, null);
		checkPosition(3, 5.0, 10.0, "Rectangle");
		checkSize(4, 15.0, 6.0, null, null);
		checkPosition(4, 0.0, 12.0, "Rectangle");

		pg.getDimension().removeWidth();
		pg.getDimension().setHeight2(Units.inch(SIZE_OVERALL / 2d));
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the height increase with override
	 */
	public void heightIncreaseWithOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double height = 20d;
		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setHeight(Units.inch(4d));
		checkSize(0, 6d, 7d, 4d);
		checkSize(1, 6d, 6d, 3d);
		checkSize(2, 6d, 13d, 10d);
		checkSize(3, 6d, 4d);
		checkSize(4, 18d, 5d);

		pg.getDimension().setHeight(Units.inch(height));
		Assert.assertEquals(height, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		checkSize(0, 6.0, 8.444444444444445, 5.444444444444445, null);
		checkPosition(0, 0.0, 6.0, "Trapeze");
		checkSize(1, 6.0, 7.444444444444445, 4.444444444444445, null);
		checkPosition(1, 6.0, 3.0, "Trapeze");
		checkSize(2, 6.0, 14.444444444444445, 11.444444444444445, null);
		checkPosition(2, 12.0, 0.0, "Trapeze");
		checkSize(3, 6.0, 4.0, null, null);
		checkPosition(3, 6.0, 10.444444444444445, "Rectangle");
		checkSize(4, 18.0, 5.555555555555555, null, null);
		checkPosition(4, 0.0, 14.444444444444445, "Rectangle");

		pg.getDimension().removeHeight();
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removeHeight();
		checkSize(0, 6.0, 6.0, 3.0, null);
		checkPosition(0, 0.0, 6.0, "Trapeze");
		checkSize(1, 6.0, 7.0, 4.0, null);
		checkPosition(1, 6.0, 3.0, "Trapeze");
		checkSize(2, 6.0, 12.0, 9.0, null);
		checkPosition(2, 12.0, 0.0, "Trapeze");
		checkSize(3, 6.0, 2.0, null, null);
		checkPosition(3, 6.0, 10.0, "Rectangle");
		checkSize(4, 18.0, 6.0, null, null);
		checkPosition(4, 0.0, 12.0, "Rectangle");
	}

	@Test
	/**
	 * Verify the height decrease with override
	 */
	public void heightDecreaseWithOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double height = 16d;
		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setHeight(Units.inch(4d));
		checkSize(0, 6d, 7d, 4d);
		checkSize(1, 6d, 6d, 3d);
		checkSize(2, 6d, 13d, 10d);
		checkSize(3, 6d, 4d);
		checkSize(4, 18d, 5d);

		pg.getDimension().setHeight(Units.inch(height));
		Assert.assertEquals(height, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		checkSize(0, 6.0, 5.555555555555555, 2.5555555555555554, null);
		checkPosition(0, 0.0, 6.0, "Trapeze");
		checkSize(1, 6.0, 4.555555555555555, 1.5555555555555554, null);
		checkPosition(1, 6.0, 3.0, "Trapeze");
		checkSize(2, 6.0, 11.555555555555555, 8.555555555555555, null);
		checkPosition(2, 12.0, 0.0, "Trapeze");
		checkSize(3, 6.0, 4.0, null, null);
		checkPosition(3, 6.0, 7.555555555555555, "Rectangle");
		checkSize(4, 18.0, 4.444444444444445, null, null);
		checkPosition(4, 0.0, 11.555555555555555, "Rectangle");

		pg.getDimension().removeHeight();
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removeHeight();

		checkSize(0, 6.0, 6.0, 3.0, null);
		checkPosition(0, 0.0, 6.0, "Trapeze");
		checkSize(1, 6.0, 7.0, 4.0, null);
		checkPosition(1, 6.0, 3.0, "Trapeze");
		checkSize(2, 6.0, 12.0, 9.0, null);
		checkPosition(2, 12.0, 0.0, "Trapeze");
		checkSize(3, 6.0, 2.0, null, null);
		checkPosition(3, 6.0, 10.0, "Rectangle");
		checkSize(4, 18.0, 6.0, null, null);
		checkPosition(4, 0.0, 12.0, "Rectangle");
	}

	@Test
	/**
	 * Verify the width increase with override
	 */
	public void widthIncreaseWithOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double width = 20d;
		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setWidth(Units.inch(4d));
		checkSize(0, 7d, 6.5d, 3d);
		checkSize(1, 4d, 6.5d, 4.5d);
		checkSize(2, 7d, 12d, 8.5d);
		checkSize(3, 4d, 2d);
		checkSize(4, 18d, 6d);

		pg.getDimension().setWidth(Units.inch(width));
		Assert.assertEquals(width, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		checkSize(0, 8.0, 8.1, 5.5, null);
		checkPosition(0, 0.0, 3.9, "Trapeze");
		checkSize(1, 4.0, 7.4, 6.1, null);
		checkPosition(1, 8.0, 2.6, "Trapeze");
		checkSize(2, 8.0, 12.0, 9.4, null);
		checkPosition(2, 12.0, 0.0, "Trapeze");
		checkSize(3, 4.0, 2.0, null, null);
		checkPosition(3, 8.0, 10.0, "Rectangle");
		checkSize(4, 20.0, 6.0, null, null);
		checkPosition(4, 0.0, 12.0, "Rectangle");

		pg.getDimension().removeWidth();
		pg.getDimension().setHeight2(Units.inch(SIZE_OVERALL / 2d));
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the width decrease with override
	 */
	public void widthDecreaseWithOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double width = 16d;
		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setWidth(Units.inch(4d));
		checkSize(0, 7d, 6.5d, 3d);
		checkSize(1, 4d, 6.5d, 4.5d);
		checkSize(2, 7d, 12d, 8.5d);
		checkSize(3, 4d, 2d);
		checkSize(4, 18d, 6d);

		pg.getDimension().setWidth(Units.inch(width));
		Assert.assertEquals(width, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		checkSize(0, 6.0, 7.9375, 5.5, null);
		checkPosition(0, 0.0, 4.0625, "Trapeze");
		checkSize(1, 4.0, 7.5625, 5.9375, null);
		checkPosition(1, 6.0, 2.4375, "Trapeze");
		checkSize(2, 6.0, 12.0, 9.5625, null);
		checkPosition(2, 10.0, 0.0, "Trapeze");
		checkSize(3, 4.0, 2.0, null, null);
		checkPosition(3, 6.0, 10.0, "Rectangle");
		checkSize(4, 16.0, 6.0, null, null);
		checkPosition(4, 0.0, 12.0, "Rectangle");

		pg.getDimension().removeWidth();
		pg.getDimension().setHeight2(Units.inch(SIZE_OVERALL / 2d));
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the height change to the minimum
	 */
	public void heightChangeMinTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double height = 5d;
		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(height));
		Assert.assertEquals(5d, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);

		checkSize(0, 6.0, 1.1111111111111116, 0.0, 6.0);
		checkPosition(0, 0.0, 2.2222222222222223, "Trapeze");
		checkSize(1, 6.0, 1.2222222222222228, 0.1111111111111116, null);
		checkPosition(1, 6.0, 1.1111111111111112, "Trapeze");
		checkSize(2, 6.0, 3.333333333333334, 2.2222222222222228, null);
		checkPosition(2, 12.0, 0.0, "Trapeze");
		checkSize(3, 6.0, 1.0, null, null);
		checkPosition(3, 6.0, 2.333333333333334, "Rectangle");
		checkSize(4, 18.0, 1.666666666666666, null, null);
		checkPosition(4, 0.0, 3.333333333333334, "Rectangle");

		pg.getDimension().removeHeight();
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		checkSize(0, 6.0, 9.777777777777782, 8.666666666666671, 6.0);
		checkPosition(0, 0.0, 2.2222222222222214, "Trapeze");
		checkSize(1, 6.0, 7.288888888888891, 6.177777777777781, null);
		checkPosition(1, 6.0, 1.1111111111111107, "Trapeze");
		checkSize(2, 6.0, 12.000000000000004, 10.888888888888893, null);
		checkPosition(2, 12.0, 0.0, "Trapeze");
		checkSize(3, 6.0, 3.6000000000000014, null, null);
		checkPosition(3, 6.0, 8.400000000000002, "Rectangle");
		checkSize(4, 18.0, 5.9999999999999964, null, null);
		checkPosition(4, 0.0, 12.000000000000004, "Rectangle");
	}

	@Test
	/**
	 * Verify the width change to the minimum
	 */
	public void widthChangeMinTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		initProperties();
		resize(SIZE_OVERALL);

		checkSize(0, SIZE_OVERALL, SIZE_OVERALL);

		splitTrapeze();

		double width = 1d;
		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(width));
		Assert.assertEquals(3d, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		checkSize(0, 1.0, 11.33333, 11.0, null);
		checkPosition(0, 0.0, 0.666666, "Trapeze");
		checkSize(1, 1.0, 9.666666, 9.3333333, null);
		checkPosition(1, 1.0, 0.3333333, "Trapeze");
		checkSize(2, 1.0, 12.0, 11.66666, null);
		checkPosition(2, 2.0, 0.0, "Trapeze");
		checkSize(3, 1.0, 2.0, null, null);
		checkPosition(3, 1.0, 10.0, "Rectangle");
		checkSize(4, 3.0, 6.0, null, null);
		checkPosition(4, 0.0, 12.0, "Rectangle");

		pg.getDimension().removeWidth();
		pg.getDimension().setHeight2(Units.inch(SIZE_OVERALL / 2d));
		Assert.assertEquals(SIZE_OVERALL, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		verifyInitial();
	}

	/**
	 * Split a trapeze into 5 pieces:
	 * 
	 * | | | | | 2 | | 1 | | | |----------| | | 0 | 3 | | |----------------------------| | 4 | |----------------------------|
	 */
	private void splitTrapeze()
	{
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 12d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(6d, 7d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(12d, 7d));
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(9d, 10d));
		Assert.assertEquals(5, pg.getConfigurables().size());

		initProperties();
		verifyInitial();
	}

	/**
	 * Verify that all the components have the initial sizes and positions.
	 */
	private void verifyInitial()
	{
		checkSize(0, 6d, 6d, 3d);
		checkSize(1, 6d, 7d, 4d);
		checkSize(2, 6d, 12d, 9d);
		checkSize(3, 6d, 2d);
		checkSize(4, 18d, 6d);

		checkPosition(0, 0d, 6d, "Trapeze");
		checkPosition(1, 6d, 3d, "Trapeze");
		checkPosition(2, 12d, 0d, "Trapeze");
		checkPosition(3, 6d, 10d, "Rectangle");
		checkPosition(4, 0d, 12d, "Rectangle");
	}
}
