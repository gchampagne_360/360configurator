package ProductGroup;

import java.awt.geom.Point2D;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.measure.quantities.Length;

import junit.framework.Assert;

import org.jscience.physics.measures.Measure;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.netappsid.commonutils.geo.enums.Orientation;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.common.Dimensionnable;
import com.netappsid.erp.configurator.configmanagement.ConfigurationManager;
import com.netappsid.erp.configurator.gui.Configurator;
import com.netappsid.wadconfigurator.Assembly;
import com.netappsid.wadconfigurator.ProductGroup;
import com.netappsid.wadconfigurator.ProductTemplate;

/**
 * PRODUCT GROUP TESTS for rectangles
 * 
 * @author sboule
 */
public class NoFlipRectangleTest extends TestUtils
{
	@BeforeClass
	public static void init() throws Exception
	{}

	@AfterClass
	public static void tearDown()
	{}

	@SuppressWarnings("unchecked")
	@Test
	/**
	 * Verify the resize of a rectangle
	 */
	public void resizeRectangleTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = (ProductGroup) assembly.getConfigurables().get(0);
		Assert.assertNotNull(pg);

		initProperties();

		pg.getDimension().setDefaultWidth(Units.inch(100d));
		pg.getDimension().setDefaultHeight(Units.inch(100d));

		((ProductGroup) pg).setForm("RECTANGLE_N");

		Assert.assertEquals(100d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(100d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);

		Assert.assertEquals(100d, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(100d, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);

		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToGet.put("assembly.configurable[0].dimension.width", "");
		propertiesToGet.put("assembly.configurable[0].dimension.height", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(assembly, propertiesToGet));

		Assert.assertEquals(Units.inch(100d).doubleValue(Units.MM),
				((Measure<Length>) propertiesToGet.get("assembly.configurable[0].dimension.width")).doubleValue(Units.MM));
		Assert.assertEquals(Units.inch(100d).doubleValue(Units.MM),
				((Measure<Length>) propertiesToGet.get("assembly.configurable[0].dimension.height")).doubleValue(Units.MM));

		propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToGet.put("configurable.dimension.width", "");
		propertiesToGet.put("configurable.dimension.height", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(pg, propertiesToGet));

		Assert.assertEquals(Units.inch(100d).doubleValue(Units.MM),
				((Measure<Length>) propertiesToGet.get("configurable.dimension.width")).doubleValue(Units.MM));
		Assert.assertEquals(Units.inch(100d).doubleValue(Units.MM),
				((Measure<Length>) propertiesToGet.get("configurable.dimension.height")).doubleValue(Units.MM));

		Map<String, String> propertiesToSet = new LinkedHashMap<String, String>();
		propertiesToSet.put("configurable.dimension.width", "2200mm");
		propertiesToSet.put("configurable.dimension.height", "2200mm");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(pg, propertiesToSet));

		propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToGet.put("assembly.configurable[0].dimension.width", "");
		propertiesToGet.put("assembly.configurable[0].dimension.height", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(assembly, propertiesToGet));

		Assert.assertEquals(2200d, ((Measure<Length>) propertiesToGet.get("assembly.configurable[0].dimension.width")).doubleValue(Units.MM));
		Assert.assertEquals(2200d, ((Measure<Length>) propertiesToGet.get("assembly.configurable[0].dimension.height")).doubleValue(Units.MM));

		propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToGet.put("configurable.dimension.width", "");
		propertiesToGet.put("configurable.dimension.height", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(pg, propertiesToGet));

		Assert.assertEquals(2200d, ((Measure<Length>) propertiesToGet.get("configurable.dimension.width")).doubleValue(Units.MM));
		Assert.assertEquals(2200d, ((Measure<Length>) propertiesToGet.get("configurable.dimension.height")).doubleValue(Units.MM));
	}

	@Test
	/**
	 * Verify the split of a rectangle
	 */
	public void splitRectangleTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = (ProductGroup) assembly.getConfigurables().get(0);
		Assert.assertNotNull(pg);

		initProperties();
		pg.getDimension().setDefaultWidth(Units.inch(10d));
		pg.getDimension().setDefaultHeight(Units.inch(10d));

		Assert.assertEquals(10d, pg.getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(10d, pg.getDimension().getWidth().doubleValue(Units.INCH), DELTA);

		((ProductGroup) pg).setForm("RECTANGLE_N");

		Assert.assertEquals(10d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(10d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);

		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 2d));
		Assert.assertEquals(2, pg.getConfigurables().size());

		checkPosition(0, 0d, 0d, "Rectangle");
		checkSize(0, 10d, 2d);
		checkPosition(1, 0d, 2d, "Rectangle");
		checkSize(1, 10d, 8d);

		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 4d));
		Assert.assertEquals(3, pg.getConfigurables().size());

		checkPosition(0, 0d, 0d, "Rectangle");
		checkSize(0, 10d, 2d);
		checkPosition(1, 0d, 2d, "Rectangle");
		checkSize(1, 10d, 2d);
		checkPosition(2, 0d, 4d, "Rectangle");
		checkSize(2, 10d, 6d);

		// Methode qui imprime les tests � faire � la console.
		// printAll();

		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 6d));
		Assert.assertEquals(4, pg.getConfigurables().size());

		checkPosition(0, 0d, 0d, "Rectangle");
		checkSize(0, 10d, 2d);
		checkPosition(1, 0d, 2d, "Rectangle");
		checkSize(1, 10d, 2d);
		checkPosition(2, 0d, 4d, "Rectangle");
		checkSize(2, 10d, 2d);
		checkPosition(3, 0d, 6d, "Rectangle");
		checkSize(3, 10d, 4d);

		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(5d, 1d));
		Assert.assertEquals(5, pg.getConfigurables().size());

		checkPosition(0, 0d, 0d, "Rectangle");
		checkSize(0, 5d, 2d);
		checkPosition(1, 5d, 0d, "Rectangle");
		checkSize(1, 5d, 2d);
		checkPosition(2, 0d, 2d, "Rectangle");
		checkSize(2, 10d, 2d);
		checkPosition(3, 0d, 4d, "Rectangle");
		checkSize(3, 10d, 2d);
		checkPosition(4, 0d, 6d, "Rectangle");
		checkSize(4, 10d, 4d);

		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(2d, 5d));
		Assert.assertEquals(6, pg.getConfigurables().size());

		checkPosition(0, 0d, 0d, "Rectangle");
		checkSize(0, 5d, 2d);
		checkPosition(1, 5d, 0d, "Rectangle");
		checkSize(1, 5d, 2d);
		checkPosition(2, 0d, 2d, "Rectangle");
		checkSize(2, 10d, 2d);
		checkPosition(3, 0d, 4d, "Rectangle");
		checkSize(3, 2d, 2d);
		checkPosition(4, 2d, 4d, "Rectangle");
		checkSize(4, 8d, 2d);
		checkPosition(5, 0d, 6d, "Rectangle");
		checkSize(5, 10d, 4d);

		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(8d, 8d));
		Assert.assertEquals(7, pg.getConfigurables().size());

		checkPosition(0, 0d, 0d, "Rectangle");
		checkSize(0, 5d, 2d);
		checkPosition(1, 5d, 0d, "Rectangle");
		checkSize(1, 5d, 2d);
		checkPosition(2, 0d, 2d, "Rectangle");
		checkSize(2, 10d, 2d);
		checkPosition(3, 0d, 4d, "Rectangle");
		checkSize(3, 2d, 2d);
		checkPosition(4, 2d, 4d, "Rectangle");
		checkSize(4, 8d, 2d);
		checkPosition(5, 0d, 6d, "Rectangle");
		checkSize(5, 8d, 4d);
		checkPosition(6, 8d, 6d, "Rectangle");
		checkSize(6, 2d, 4d);

		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 8d));
		Assert.assertEquals(8, pg.getConfigurables().size());

		checkPosition(0, 0d, 0d, "Rectangle");
		checkSize(0, 5d, 2d);
		checkPosition(1, 5d, 0d, "Rectangle");
		checkSize(1, 5d, 2d);
		checkPosition(2, 0d, 2d, "Rectangle");
		checkSize(2, 10d, 2d);
		checkPosition(3, 0d, 4d, "Rectangle");
		checkSize(3, 2d, 2d);
		checkPosition(4, 2d, 4d, "Rectangle");
		checkSize(4, 8d, 2d);
		checkPosition(5, 0d, 6d, "Rectangle");
		checkSize(5, 8d, 2d);
		checkPosition(6, 8d, 6d, "Rectangle");
		checkSize(6, 2d, 4d);
		checkPosition(7, 0d, 8d, "Rectangle");
		checkSize(7, 8d, 2d);

		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(9d, 9d));
		Assert.assertEquals(9, pg.getConfigurables().size());

		checkPosition(0, 0d, 0d, "Rectangle");
		checkSize(0, 5d, 2d);
		checkPosition(1, 5d, 0d, "Rectangle");
		checkSize(1, 5d, 2d);
		checkPosition(2, 0d, 2d, "Rectangle");
		checkSize(2, 10d, 2d);
		checkPosition(3, 0d, 4d, "Rectangle");
		checkSize(3, 2d, 2d);
		checkPosition(4, 2d, 4d, "Rectangle");
		checkSize(4, 8d, 2d);
		checkPosition(5, 0d, 6d, "Rectangle");
		checkSize(5, 8d, 2d);
		checkPosition(6, 8d, 6d, "Rectangle");
		checkSize(6, 2d, 3d);
		checkPosition(7, 0d, 8d, "Rectangle");
		checkSize(7, 8d, 2d);
		checkPosition(8, 8d, 9d, "Rectangle");
		checkSize(8, 2d, 1d);
	}

	@Test
	/**
	 * Verify the horizontal test rectangle positions and sizes
	 */
	public void horizRectangleTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = (ProductGroup) assembly.getConfigurables().get(0);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm("RECTANGLE_N");

		splitRectangleHoriz();

		checkPosition(0, 0d, 0d, "Rectangle");
		checkSize(0, 2d, 8d);
		checkPosition(1, 2d, 0d, "Rectangle");
		checkSize(1, 6d, 4d);
		checkPosition(2, 8d, 0d, "Rectangle");
		checkSize(2, 2d, 2d);
		checkPosition(3, 8d, 2d, "Rectangle");
		checkSize(3, 2d, 8d);
		checkPosition(4, 2d, 4d, "Rectangle");
		checkSize(4, 6d, 3d);
		checkPosition(5, 2d, 7d, "Rectangle");
		checkSize(5, 6d, 3d);
		checkPosition(6, 0d, 8d, "Rectangle");
		checkSize(6, 2d, 2d);
	}

	@Test
	/**
	 * Verify the vertical test rectangle positions and sizes
	 */
	public void vertiRectangleTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = (ProductGroup) assembly.getConfigurables().get(0);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm("RECTANGLE_N");

		splitRectangleVerti();

		checkPosition(0, 0d, 0d, "Rectangle");
		checkSize(0, 2d, 3d);
		checkPosition(1, 2d, 0d, "Rectangle");
		checkSize(1, 8d, 3d);
		checkPosition(2, 0d, 3d, "Rectangle");
		checkSize(2, 3d, 4d);
		checkPosition(3, 3d, 3d, "Rectangle");
		checkSize(3, 4d, 4d);
		checkPosition(4, 7d, 3d, "Rectangle");
		checkSize(4, 3d, 4d);
		checkPosition(5, 0d, 7d, "Rectangle");
		checkSize(5, 9d, 3d);
		checkPosition(6, 9d, 7d, "Rectangle");
		checkSize(6, 1d, 3d);
	}

	@Test
	/**
	 * Verify the getEastComponents method
	 */
	public void findEastNeighborsTest()
	{
		List<com.netappsid.erp.configurator.Configurable> neighborsList;

		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = (ProductGroup) assembly.getConfigurables().get(0);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm("RECTANGLE_N");

		splitRectangleHoriz();

		// // -----------------------------------------------------------------------
		// neighborsList = pg.getEastComponents(pg.getConfigurables().get(0));
		// Assert.assertEquals(3,neighborsList.size());
		// Assert.assertEquals(pg.getConfigurables().get(1), neighborsList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(4), neighborsList.get(1));
		// Assert.assertEquals(pg.getConfigurables().get(5), neighborsList.get(2));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getEastComponents(pg.getConfigurables().get(1));
		// Assert.assertEquals(2,neighborsList.size());
		// Assert.assertEquals(pg.getConfigurables().get(2), neighborsList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(3), neighborsList.get(1));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getEastComponents(pg.getConfigurables().get(2));
		// Assert.assertEquals(0,neighborsList.size());
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getEastComponents(pg.getConfigurables().get(3));
		// Assert.assertEquals(0,neighborsList.size());
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getEastComponents(pg.getConfigurables().get(4));
		// Assert.assertEquals(2,neighborsList.size());
		// Assert.assertEquals(pg.getConfigurables().get(2), neighborsList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(3), neighborsList.get(1));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getEastComponents(pg.getConfigurables().get(5));
		// Assert.assertEquals(2,neighborsList.size());
		// Assert.assertEquals(pg.getConfigurables().get(2), neighborsList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(3), neighborsList.get(1));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getEastComponents(pg.getConfigurables().get(6));
		// Assert.assertEquals(3,neighborsList.size());
		// Assert.assertEquals(pg.getConfigurables().get(1), neighborsList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(4), neighborsList.get(1));
		// Assert.assertEquals(pg.getConfigurables().get(5), neighborsList.get(2));
	}

	@Test
	/**
	 * Verify the getWestComponents method
	 */
	public void findWestNeighborsTest()
	{
		List<com.netappsid.erp.configurator.Configurable> neighborsList;

		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = (ProductGroup) assembly.getConfigurables().get(0);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm("RECTANGLE_N");

		splitRectangleHoriz();

		// // -----------------------------------------------------------------------
		// neighborsList = pg.getWestComponents(pg.getConfigurables().get(0));
		// Assert.assertEquals(0,neighborsList.size());
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getWestComponents(pg.getConfigurables().get(1));
		// Assert.assertEquals(2,neighborsList.size());
		// Assert.assertEquals(pg.getConfigurables().get(0), neighborsList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(6), neighborsList.get(1));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getWestComponents(pg.getConfigurables().get(2));
		// Assert.assertEquals(3,neighborsList.size());
		// Assert.assertEquals(pg.getConfigurables().get(1), neighborsList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(4), neighborsList.get(1));
		// Assert.assertEquals(pg.getConfigurables().get(5), neighborsList.get(2));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getWestComponents(pg.getConfigurables().get(3));
		// Assert.assertEquals(3,neighborsList.size());
		// Assert.assertEquals(pg.getConfigurables().get(1), neighborsList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(4), neighborsList.get(1));
		// Assert.assertEquals(pg.getConfigurables().get(5), neighborsList.get(2));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getWestComponents(pg.getConfigurables().get(4));
		// Assert.assertEquals(2,neighborsList.size());
		// Assert.assertEquals(pg.getConfigurables().get(0), neighborsList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(6), neighborsList.get(1));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getWestComponents(pg.getConfigurables().get(5));
		// Assert.assertEquals(2,neighborsList.size());
		// Assert.assertEquals(pg.getConfigurables().get(0), neighborsList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(6), neighborsList.get(1));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getWestComponents(pg.getConfigurables().get(6));
		// Assert.assertEquals(0,neighborsList.size());
	}

	@Test
	/**
	 * Verify the getNorthComponents method
	 */
	public void findNorthNeighborsTest()
	{
		List<com.netappsid.erp.configurator.Configurable> neighborsList;

		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = (ProductGroup) assembly.getConfigurables().get(0);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm("RECTANGLE_N");

		splitRectangleVerti();

		// // -----------------------------------------------------------------------
		// neighborsList = pg.getNorthComponents(pg.getConfigurables().get(0));
		// Assert.assertEquals(0,neighborsList.size());
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getNorthComponents(pg.getConfigurables().get(1));
		// Assert.assertEquals(0,neighborsList.size());
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getNorthComponents(pg.getConfigurables().get(2));
		// Assert.assertEquals(2,neighborsList.size());
		// Assert.assertEquals(pg.getConfigurables().get(0), neighborsList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(1), neighborsList.get(1));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getNorthComponents(pg.getConfigurables().get(3));
		// Assert.assertEquals(2,neighborsList.size());
		// Assert.assertEquals(pg.getConfigurables().get(0), neighborsList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(1), neighborsList.get(1));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getNorthComponents(pg.getConfigurables().get(4));
		// Assert.assertEquals(2,neighborsList.size());
		// Assert.assertEquals(pg.getConfigurables().get(0), neighborsList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(1), neighborsList.get(1));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getNorthComponents(pg.getConfigurables().get(5));
		// Assert.assertEquals(3,neighborsList.size());
		// Assert.assertEquals(pg.getConfigurables().get(2), neighborsList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(3), neighborsList.get(1));
		// Assert.assertEquals(pg.getConfigurables().get(4), neighborsList.get(2));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getNorthComponents(pg.getConfigurables().get(6));
		// Assert.assertEquals(3,neighborsList.size());
		// Assert.assertEquals(pg.getConfigurables().get(2), neighborsList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(3), neighborsList.get(1));
		// Assert.assertEquals(pg.getConfigurables().get(4), neighborsList.get(2));
	}

	@Test
	/**
	 * Verify the getSouthComponents method
	 */
	public void findSouthNeighborsTest()
	{
		List<com.netappsid.erp.configurator.Configurable> neighborsList;

		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = (ProductGroup) assembly.getConfigurables().get(0);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm("RECTANGLE_N");

		splitRectangleVerti();

		// // -----------------------------------------------------------------------
		// neighborsList = pg.getSouthComponents(pg.getConfigurables().get(0));
		// Assert.assertEquals(3,neighborsList.size());
		// Assert.assertEquals(pg.getConfigurables().get(2), neighborsList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(3), neighborsList.get(1));
		// Assert.assertEquals(pg.getConfigurables().get(4), neighborsList.get(2));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getSouthComponents(pg.getConfigurables().get(1));
		// Assert.assertEquals(3,neighborsList.size());
		// Assert.assertEquals(pg.getConfigurables().get(2), neighborsList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(3), neighborsList.get(1));
		// Assert.assertEquals(pg.getConfigurables().get(4), neighborsList.get(2));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getSouthComponents(pg.getConfigurables().get(2));
		// Assert.assertEquals(2,neighborsList.size());
		// Assert.assertEquals(pg.getConfigurables().get(5), neighborsList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(6), neighborsList.get(1));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getSouthComponents(pg.getConfigurables().get(3));
		// Assert.assertEquals(2,neighborsList.size());
		// Assert.assertEquals(pg.getConfigurables().get(5), neighborsList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(6), neighborsList.get(1));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getSouthComponents(pg.getConfigurables().get(4));
		// Assert.assertEquals(2,neighborsList.size());
		// Assert.assertEquals(pg.getConfigurables().get(5), neighborsList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(6), neighborsList.get(1));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getSouthComponents(pg.getConfigurables().get(5));
		// Assert.assertEquals(0,neighborsList.size());
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getSouthComponents(pg.getConfigurables().get(6));
		// Assert.assertEquals(0,neighborsList.size());
	}

	@Test
	/**
	 * Verify the West Stack
	 */
	public void findWestStackTest()
	{
		List<com.netappsid.erp.configurator.Configurable> stackList;

		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = (ProductGroup) assembly.getConfigurables().get(0);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm("RECTANGLE_N");

		splitRectangleHoriz();
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(5d, 5d));

		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(0), Side.WEST, null);
		// Assert.assertEquals(1,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(7), stackList.get(0));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(1), Side.WEST, null);
		// Assert.assertEquals(2,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(4), stackList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(6), stackList.get(1));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(2), Side.WEST, null);
		// Assert.assertEquals(1,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(3), stackList.get(0));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(3), Side.WEST, null);
		// Assert.assertEquals(1,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(2), stackList.get(0));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(4), Side.WEST, null);
		// Assert.assertEquals(2,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(1), stackList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(6), stackList.get(1));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(5), Side.WEST, null);
		// Assert.assertEquals(0,stackList.size());
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(6), Side.WEST, null);
		// Assert.assertEquals(2,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(1), stackList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(4), stackList.get(1));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(7), Side.WEST, null);
		// Assert.assertEquals(1,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(0), stackList.get(0));
	}

	@Test
	/**
	 * Verify the East Stack
	 */
	public void findEastStackTest()
	{
		List<com.netappsid.erp.configurator.Configurable> stackList;

		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = (ProductGroup) assembly.getConfigurables().get(0);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm("RECTANGLE_N");

		splitRectangleHoriz();
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(5d, 5d));

		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(0), Side.EAST, null);
		// Assert.assertEquals(1,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(7), stackList.get(0));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(1), Side.EAST, null);
		// Assert.assertEquals(2,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(5), stackList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(6), stackList.get(1));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(2), Side.EAST, null);
		// Assert.assertEquals(1,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(3), stackList.get(0));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(3), Side.EAST, null);
		// Assert.assertEquals(1,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(2), stackList.get(0));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(4), Side.EAST, null);
		// Assert.assertEquals(0,stackList.size());
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(5), Side.EAST, null);
		// Assert.assertEquals(2,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(1), stackList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(6), stackList.get(1));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(6), Side.EAST, null);
		// Assert.assertEquals(2,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(1), stackList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(5), stackList.get(1));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(7), Side.EAST, null);
		// Assert.assertEquals(1,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(0), stackList.get(0));
	}

	@Test
	/**
	 * Verify the North Stack
	 */
	public void findNorthStackTest()
	{
		List<com.netappsid.erp.configurator.Configurable> stackList;

		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = (ProductGroup) assembly.getConfigurables().get(0);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm("RECTANGLE_N");

		splitRectangleVerti();
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 5d));

		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(0), Side.NORTH, null);
		// Assert.assertEquals(1,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(1), stackList.get(0));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(1), Side.NORTH, null);
		// Assert.assertEquals(1,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(0), stackList.get(0));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(2), Side.NORTH, null);
		// Assert.assertEquals(2,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(3), stackList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(4), stackList.get(1));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(3), Side.NORTH, null);
		// Assert.assertEquals(2,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(2), stackList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(4), stackList.get(1));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(4), Side.NORTH, null);
		// Assert.assertEquals(2,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(2), stackList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(3), stackList.get(1));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(5), Side.NORTH, null);
		// Assert.assertEquals(0,stackList.size());
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(6), Side.NORTH, null);
		// Assert.assertEquals(1,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(7), stackList.get(0));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(7), Side.NORTH, null);
		// Assert.assertEquals(1,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(6), stackList.get(0));
	}

	@Test
	/**
	 * Verify the South Stack
	 */
	public void findSouthStackTest()
	{
		List<com.netappsid.erp.configurator.Configurable> stackList;

		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = (ProductGroup) assembly.getConfigurables().get(0);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm("RECTANGLE_N");

		splitRectangleVerti();
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 5d));

		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(0), Side.SOUTH, null);
		// Assert.assertEquals(1,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(1), stackList.get(0));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(1), Side.SOUTH, null);
		// Assert.assertEquals(1,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(0), stackList.get(0));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(2), Side.SOUTH, null);
		// Assert.assertEquals(2,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(4), stackList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(5), stackList.get(1));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(3), Side.SOUTH, null);
		// Assert.assertEquals(0,stackList.size());
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(4), Side.SOUTH, null);
		// Assert.assertEquals(2,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(2), stackList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(5), stackList.get(1));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(5), Side.SOUTH, null);
		// Assert.assertEquals(2,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(2), stackList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(4), stackList.get(1));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(6), Side.SOUTH, null);
		// Assert.assertEquals(1,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(7), stackList.get(0));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(7), Side.SOUTH, null);
		// Assert.assertEquals(1,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(6), stackList.get(0));
	}

	@Test
	/**
	 * Verify the width increase
	 */
	public void widthIncreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = (ProductGroup) assembly.getConfigurables().get(0);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm("RECTANGLE_N");

		splitRectangleHoriz();
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(5d, 5d));

		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().setWidth(Units.inch(3d));
		checkSize(0, 3d, null);
		checkSize(1, 5d, null);
		checkSize(2, 2d, null);
		checkSize(3, 2d, null);
		checkSize(4, 2d, null);
		checkSize(5, 3d, null);
		checkSize(6, 5d, null);
		checkSize(7, 3d, null);
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().removeWidth();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().setWidth(Units.inch(8d));
		checkSize(0, 1d, null);
		checkSize(1, 8d, null);
		checkSize(2, 1d, null);
		checkSize(3, 1d, null);
		checkSize(4, 4d, null);
		checkSize(5, 4d, null);
		checkSize(6, 8d, null);
		checkSize(7, 1d, null);
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().removeWidth();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().setWidth(Units.inch(3d));
		checkSize(0, 2d, null);
		checkSize(1, 5d, null);
		checkSize(2, 3d, null);
		checkSize(3, 3d, null);
		checkSize(4, 3d, null);
		checkSize(5, 2d, null);
		checkSize(6, 5d, null);
		checkSize(7, 2d, null);
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().removeWidth();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setWidth(Units.inch(3d));
		checkSize(0, 2d, null);
		checkSize(1, 5d, null);
		checkSize(2, 3d, null);
		checkSize(3, 3d, null);
		checkSize(4, 3d, null);
		checkSize(5, 2d, null);
		checkSize(6, 5d, null);
		checkSize(7, 2d, null);
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removeWidth();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(4)).getDimension().setWidth(Units.inch(5d));
		checkSize(0, 1d, null);
		checkSize(1, 7d, null);
		checkSize(2, 2d, null);
		checkSize(3, 2d, null);
		checkSize(4, 5d, null);
		checkSize(5, 2d, null);
		checkSize(6, 7d, null);
		checkSize(7, 1d, null);
		((Dimensionnable) pg.getConfigurables().get(4)).getDimension().removeWidth();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(5)).getDimension().setWidth(Units.inch(5d));
		checkSize(0, 2d, null);
		checkSize(1, 7d, null);
		checkSize(2, 1d, null);
		checkSize(3, 1d, null);
		checkSize(4, 2d, null);
		checkSize(5, 5d, null);
		checkSize(6, 7d, null);
		checkSize(7, 2d, null);
		((Dimensionnable) pg.getConfigurables().get(5)).getDimension().removeWidth();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(6)).getDimension().setWidth(Units.inch(8d));
		checkSize(0, 1d, null);
		checkSize(1, 8d, null);
		checkSize(2, 1d, null);
		checkSize(3, 1d, null);
		checkSize(4, 4d, null);
		checkSize(5, 4d, null);
		checkSize(6, 8d, null);
		checkSize(7, 1d, null);
		((Dimensionnable) pg.getConfigurables().get(6)).getDimension().removeWidth();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(7)).getDimension().setWidth(Units.inch(3d));
		checkSize(0, 3d, null);
		checkSize(1, 5d, null);
		checkSize(2, 2d, null);
		checkSize(3, 2d, null);
		checkSize(4, 2d, null);
		checkSize(5, 3d, null);
		checkSize(6, 5d, null);
		checkSize(7, 3d, null);
		((Dimensionnable) pg.getConfigurables().get(7)).getDimension().removeWidth();
		verifyInitialHoriz();
	}

	@Test
	/**
	 * Verify the height increase
	 */
	public void heightIncreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = (ProductGroup) assembly.getConfigurables().get(0);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm("RECTANGLE_N");

		splitRectangleVerti();
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 5d));

		verifyInitialVerti();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().setHeight(Units.inch(4d));
		checkSize(0, null, 4d);
		checkSize(1, null, 4d);
		checkSize(2, null, 3d);
		checkSize(3, null, 1d);
		checkSize(4, null, 3d);
		checkSize(5, null, 2d);
		checkSize(6, null, 3d);
		checkSize(7, null, 3d);
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().removeHeight();
		verifyInitialVerti();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().setHeight(Units.inch(4d));
		checkSize(0, null, 4d);
		checkSize(1, null, 4d);
		checkSize(2, null, 3d);
		checkSize(3, null, 1d);
		checkSize(4, null, 3d);
		checkSize(5, null, 2d);
		checkSize(6, null, 3d);
		checkSize(7, null, 3d);
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().removeHeight();
		verifyInitialVerti();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().setHeight(Units.inch(6d));
		checkSize(0, null, 2d);
		checkSize(1, null, 2d);
		checkSize(2, null, 6d);
		checkSize(3, null, 3d);
		checkSize(4, null, 6d);
		checkSize(5, null, 3d);
		checkSize(6, null, 2d);
		checkSize(7, null, 2d);
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().removeHeight();
		verifyInitialVerti();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setHeight(Units.inch(4d));
		checkSize(0, null, 2d);
		checkSize(1, null, 2d);
		checkSize(2, null, 5d);
		checkSize(3, null, 4d);
		checkSize(4, null, 5d);
		checkSize(5, null, 1d);
		checkSize(6, null, 3d);
		checkSize(7, null, 3d);
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removeHeight();
		verifyInitialVerti();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(4)).getDimension().setHeight(Units.inch(6d));
		checkSize(0, null, 2d);
		checkSize(1, null, 2d);
		checkSize(2, null, 6d);
		checkSize(3, null, 3d);
		checkSize(4, null, 6d);
		checkSize(5, null, 3d);
		checkSize(6, null, 2d);
		checkSize(7, null, 2d);
		((Dimensionnable) pg.getConfigurables().get(4)).getDimension().removeHeight();
		verifyInitialVerti();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(5)).getDimension().setHeight(Units.inch(4d));
		checkSize(0, null, 3d);
		checkSize(1, null, 3d);
		checkSize(2, null, 5d);
		checkSize(3, null, 1d);
		checkSize(4, null, 5d);
		checkSize(5, null, 4d);
		checkSize(6, null, 2d);
		checkSize(7, null, 2d);
		((Dimensionnable) pg.getConfigurables().get(5)).getDimension().removeHeight();
		verifyInitialVerti();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(6)).getDimension().setHeight(Units.inch(4d));
		checkSize(0, null, 3d);
		checkSize(1, null, 3d);
		checkSize(2, null, 3d);
		checkSize(3, null, 2d);
		checkSize(4, null, 3d);
		checkSize(5, null, 1d);
		checkSize(6, null, 4d);
		checkSize(7, null, 4d);
		((Dimensionnable) pg.getConfigurables().get(6)).getDimension().removeHeight();
		verifyInitialVerti();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(7)).getDimension().setHeight(Units.inch(4d));
		checkSize(0, null, 3d);
		checkSize(1, null, 3d);
		checkSize(2, null, 3d);
		checkSize(3, null, 2d);
		checkSize(4, null, 3d);
		checkSize(5, null, 1d);
		checkSize(6, null, 4d);
		checkSize(7, null, 4d);
		((Dimensionnable) pg.getConfigurables().get(7)).getDimension().removeHeight();
		verifyInitialVerti();
	}

	@Test
	/**
	 * Verify the width decrease
	 */
	public void widthDecreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = (ProductGroup) assembly.getConfigurables().get(0);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm("RECTANGLE_N");

		splitRectangleHoriz();
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(5d, 5d));

		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().setWidth(Units.inch(1d));
		checkSize(0, 1d, null);
		checkSize(1, 7d, null);
		checkSize(2, 2d, null);
		checkSize(3, 2d, null);
		checkSize(4, 4d, null);
		checkSize(5, 3d, null);
		checkSize(6, 7d, null);
		checkSize(7, 1d, null);
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().removeWidth();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().setWidth(Units.inch(4d));
		checkSize(0, 3d, null);
		checkSize(1, 4d, null);
		checkSize(2, 3d, null);
		checkSize(3, 3d, null);
		checkSize(4, 2d, null);
		checkSize(5, 2d, null);
		checkSize(6, 4d, null);
		checkSize(7, 3d, null);
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().removeWidth();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().setWidth(Units.inch(1d));
		checkSize(0, 2d, null);
		checkSize(1, 7d, null);
		checkSize(2, 1d, null);
		checkSize(3, 1d, null);
		checkSize(4, 3d, null);
		checkSize(5, 4d, null);
		checkSize(6, 7d, null);
		checkSize(7, 2d, null);
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().removeWidth();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setWidth(Units.inch(1d));
		checkSize(0, 2d, null);
		checkSize(1, 7d, null);
		checkSize(2, 1d, null);
		checkSize(3, 1d, null);
		checkSize(4, 3d, null);
		checkSize(5, 4d, null);
		checkSize(6, 7d, null);
		checkSize(7, 2d, null);
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removeWidth();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(4)).getDimension().setWidth(Units.inch(1d));
		checkSize(0, 3d, null);
		checkSize(1, 5d, null);
		checkSize(2, 2d, null);
		checkSize(3, 2d, null);
		checkSize(4, 1d, null);
		checkSize(5, 4d, null);
		checkSize(6, 5d, null);
		checkSize(7, 3d, null);
		((Dimensionnable) pg.getConfigurables().get(4)).getDimension().removeWidth();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(5)).getDimension().setWidth(Units.inch(1d));
		checkSize(0, 2d, null);
		checkSize(1, 5d, null);
		checkSize(2, 3d, null);
		checkSize(3, 3d, null);
		checkSize(4, 4d, null);
		checkSize(5, 1d, null);
		checkSize(6, 5d, null);
		checkSize(7, 2d, null);
		((Dimensionnable) pg.getConfigurables().get(5)).getDimension().removeWidth();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(6)).getDimension().setWidth(Units.inch(1d));
		checkSize(0, 4d, null);
		checkSize(1, 2d, null);
		checkSize(2, 4d, null);
		checkSize(3, 4d, null);
		checkSize(4, 1d, null);
		checkSize(5, 1d, null);
		checkSize(6, 2d, null);
		checkSize(7, 4d, null);
		((Dimensionnable) pg.getConfigurables().get(6)).getDimension().removeWidth();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(7)).getDimension().setWidth(Units.inch(1d));
		checkSize(0, 1d, null);
		checkSize(1, 7d, null);
		checkSize(2, 2d, null);
		checkSize(3, 2d, null);
		checkSize(4, 4d, null);
		checkSize(5, 3d, null);
		checkSize(6, 7d, null);
		checkSize(7, 1d, null);
		((Dimensionnable) pg.getConfigurables().get(7)).getDimension().removeWidth();
		verifyInitialHoriz();
	}

	@Test
	/**
	 * Verify the height decrease
	 */
	public void heightDecreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = (ProductGroup) assembly.getConfigurables().get(0);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm("RECTANGLE_N");

		splitRectangleVerti();
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 5d));

		verifyInitialVerti();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().setHeight(Units.inch(2d));
		checkSize(0, null, 2d);
		checkSize(1, null, 2d);
		checkSize(2, null, 5d);
		checkSize(3, null, 3d);
		checkSize(4, null, 5d);
		checkSize(5, null, 2d);
		checkSize(6, null, 3d);
		checkSize(7, null, 3d);
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().removeHeight();
		verifyInitialVerti();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().setHeight(Units.inch(2d));
		checkSize(0, null, 2d);
		checkSize(1, null, 2d);
		checkSize(2, null, 5d);
		checkSize(3, null, 3d);
		checkSize(4, null, 5d);
		checkSize(5, null, 2d);
		checkSize(6, null, 3d);
		checkSize(7, null, 3d);
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().removeHeight();
		verifyInitialVerti();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().setHeight(Units.inch(2d));
		checkSize(0, null, 4d);
		checkSize(1, null, 4d);
		checkSize(2, null, 2d);
		checkSize(3, null, 1d);
		checkSize(4, null, 2d);
		checkSize(5, null, 1d);
		checkSize(6, null, 4d);
		checkSize(7, null, 4d);
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().removeHeight();
		verifyInitialVerti();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setHeight(Units.inch(1d));
		checkSize(0, null, 3.5d);
		checkSize(1, null, 3.5d);
		checkSize(2, null, 3.5d);
		checkSize(3, null, 1d);
		checkSize(4, null, 3.5d);
		checkSize(5, null, 2.5d);
		checkSize(6, null, 3d);
		checkSize(7, null, 3d);
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removeHeight();
		verifyInitialVerti();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(4)).getDimension().setHeight(Units.inch(1d));
		checkSize(0, null, 4d);
		checkSize(1, null, 4d);
		checkSize(2, null, 2d);
		checkSize(3, null, 1d);
		checkSize(4, null, 2d);
		checkSize(5, null, 1d);
		checkSize(6, null, 4d);
		checkSize(7, null, 4d);
		((Dimensionnable) pg.getConfigurables().get(4)).getDimension().removeHeight();
		verifyInitialVerti();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(5)).getDimension().setHeight(Units.inch(1d));
		checkSize(0, null, 3d);
		checkSize(1, null, 3d);
		checkSize(2, null, 3.5d);
		checkSize(3, null, 2.5d);
		checkSize(4, null, 3.5d);
		checkSize(5, null, 1d);
		checkSize(6, null, 3.5d);
		checkSize(7, null, 3.5d);
		((Dimensionnable) pg.getConfigurables().get(5)).getDimension().removeHeight();
		verifyInitialVerti();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(6)).getDimension().setHeight(Units.inch(2d));
		checkSize(0, null, 3d);
		checkSize(1, null, 3d);
		checkSize(2, null, 5d);
		checkSize(3, null, 2d);
		checkSize(4, null, 5d);
		checkSize(5, null, 3d);
		checkSize(6, null, 2d);
		checkSize(7, null, 2d);
		((Dimensionnable) pg.getConfigurables().get(6)).getDimension().removeHeight();
		verifyInitialVerti();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(7)).getDimension().setHeight(Units.inch(2d));
		checkSize(0, null, 3d);
		checkSize(1, null, 3d);
		checkSize(2, null, 5d);
		checkSize(3, null, 2d);
		checkSize(4, null, 5d);
		checkSize(5, null, 3d);
		checkSize(6, null, 2d);
		checkSize(7, null, 2d);
		((Dimensionnable) pg.getConfigurables().get(7)).getDimension().removeHeight();
		verifyInitialVerti();
	}

	@Test
	/**
	 * Verify the width change with overrides
	 */
	public void widthChangeWithOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = (ProductGroup) assembly.getConfigurables().get(0);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm("RECTANGLE_N");

		splitRectangleHoriz();
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(5d, 5d));

		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().setWidth(Units.inch(4d)); // Override
		checkSize(0, 3d, null);
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().setWidth(Units.inch(4d));
		checkSize(0, 3d, null);
		checkSize(1, 4d, null);
		checkSize(2, 3d, null);
		checkSize(3, 3d, null);
		checkSize(4, 2d, null);
		checkSize(5, 2d, null);
		checkSize(6, 4d, null);
		checkSize(7, 3d, null);
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().removeWidth();
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().removeWidth();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().setWidth(Units.inch(3d));
		checkSize(1, 5d, null);
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().setWidth(Units.inch(6d));
		checkSize(0, 3d, null);
		checkSize(1, 6d, null);
		checkSize(2, 1d, null);
		checkSize(3, 1d, null);
		checkSize(4, 2d, null);
		checkSize(5, 4d, null);
		checkSize(6, 6d, null);
		checkSize(7, 3d, null);
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().removeWidth();
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().removeWidth();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setWidth(Units.inch(3d));
		checkSize(2, 3d, null);
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().setWidth(Units.inch(2d));
		checkSize(0, 2d, null);
		checkSize(1, 5d, null);
		checkSize(2, 3d, null);
		checkSize(3, 3d, null);
		checkSize(4, 3d, null);
		checkSize(5, 2d, null);
		checkSize(6, 5d, null);
		checkSize(7, 2d, null);
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().removeWidth();
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removeWidth();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(5)).getDimension().setWidth(Units.inch(5d));
		checkSize(3, 1d, null);
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setWidth(Units.inch(3d));
		checkSize(0, 2d, null);
		checkSize(1, 7d, null);
		checkSize(2, 1d, null);
		checkSize(3, 1d, null);
		checkSize(4, 2d, null);
		checkSize(5, 5d, null);
		checkSize(6, 7d, null);
		checkSize(7, 2d, null);
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removeWidth();
		((Dimensionnable) pg.getConfigurables().get(5)).getDimension().removeWidth();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(5)).getDimension().setWidth(Units.inch(5d));
		checkSize(4, 2d, null);
		((Dimensionnable) pg.getConfigurables().get(4)).getDimension().setWidth(Units.inch(3d));
		checkSize(0, 1d, null);
		checkSize(1, 8d, null);
		checkSize(2, 1d, null);
		checkSize(3, 1d, null);
		checkSize(4, 3d, null);
		checkSize(5, 5d, null);
		checkSize(6, 8d, null);
		checkSize(7, 1d, null);
		((Dimensionnable) pg.getConfigurables().get(4)).getDimension().removeWidth();
		((Dimensionnable) pg.getConfigurables().get(5)).getDimension().removeWidth();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().setWidth(Units.inch(3d));
		checkSize(5, 2d, null);
		((Dimensionnable) pg.getConfigurables().get(7)).getDimension().setWidth(Units.inch(3d));
		checkSize(5, 2d, null);
		((Dimensionnable) pg.getConfigurables().get(5)).getDimension().setWidth(Units.inch(3d));
		checkSize(0, 3d, null);
		checkSize(1, 4d, null);
		checkSize(2, 3d, null);
		checkSize(3, 3d, null);
		checkSize(4, 1d, null);
		checkSize(5, 3d, null);
		checkSize(6, 4d, null);
		checkSize(7, 3d, null);
		((Dimensionnable) pg.getConfigurables().get(5)).getDimension().removeWidth();
		((Dimensionnable) pg.getConfigurables().get(7)).getDimension().removeWidth();
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().removeWidth();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().setWidth(Units.inch(3d));
		checkSize(6, 5d, null);
		((Dimensionnable) pg.getConfigurables().get(7)).getDimension().setWidth(Units.inch(3d));
		checkSize(6, 4d, null);
		((Dimensionnable) pg.getConfigurables().get(6)).getDimension().setWidth(Units.inch(6d));
		checkSize(0, 3d, null);
		checkSize(1, 4d, null);
		checkSize(2, 3d, null);
		checkSize(3, 3d, null);
		checkSize(4, 2d, null);
		checkSize(5, 2d, null);
		checkSize(6, 4d, null);
		checkSize(7, 3d, null);
		((Dimensionnable) pg.getConfigurables().get(6)).getDimension().removeWidth();
		((Dimensionnable) pg.getConfigurables().get(7)).getDimension().removeWidth();
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().removeWidth();
		verifyInitialHoriz();
	}

	@Test
	/**
	 * Verify the height change with override
	 */
	public void heightChangeWithOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = (ProductGroup) assembly.getConfigurables().get(0);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm("RECTANGLE_N");

		splitRectangleVerti();
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 5d));

		verifyInitialVerti();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(4)).getDimension().setHeight(Units.inch(6d));
		checkSize(0, null, 2d);
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().setHeight(Units.inch(3d));
		checkSize(0, null, 2d);
		checkSize(1, null, 2d);
		checkSize(2, null, 6d);
		checkSize(3, null, 3d);
		checkSize(4, null, 6d);
		checkSize(5, null, 3d);
		checkSize(6, null, 2d);
		checkSize(7, null, 2d);
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().removeHeight();
		((Dimensionnable) pg.getConfigurables().get(4)).getDimension().removeHeight();
		verifyInitialVerti();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(5)).getDimension().setHeight(Units.inch(1d));
		checkSize(1, null, 3d);
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().setHeight(Units.inch(4d));
		checkSize(0, null, 4d);
		checkSize(1, null, 4d);
		checkSize(2, null, 2.5d);
		checkSize(3, null, 1.5d);
		checkSize(4, null, 2.5d);
		checkSize(5, null, 1d);
		checkSize(6, null, 3.5d);
		checkSize(7, null, 3.5d);
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().removeHeight();
		((Dimensionnable) pg.getConfigurables().get(5)).getDimension().removeHeight();
		verifyInitialVerti();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setHeight(Units.inch(4d));
		checkSize(2, null, 5d);
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().setHeight(Units.inch(6d));
		checkSize(0, null, 2d);
		checkSize(1, null, 2d);
		checkSize(2, null, 6d);
		checkSize(3, null, 4d);
		checkSize(4, null, 6d);
		checkSize(5, null, 2d);
		checkSize(6, null, 2d);
		checkSize(7, null, 2d);
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().removeHeight();
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removeHeight();
		verifyInitialVerti();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(4)).getDimension().setHeight(Units.inch(6d));
		checkSize(3, null, 3d);
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setHeight(Units.inch(4d));
		checkSize(0, null, 2d);
		checkSize(1, null, 2d);
		checkSize(2, null, 6d);
		checkSize(3, null, 4d);
		checkSize(4, null, 6d);
		checkSize(5, null, 2d);
		checkSize(6, null, 2d);
		checkSize(7, null, 2d);
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removeHeight();
		((Dimensionnable) pg.getConfigurables().get(4)).getDimension().removeHeight();
		verifyInitialVerti();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().setHeight(Units.inch(6d));
		checkSize(4, null, 6d);
		((Dimensionnable) pg.getConfigurables().get(4)).getDimension().setHeight(Units.inch(8d));
		checkSize(0, null, 2d);
		checkSize(1, null, 2d);
		checkSize(2, null, 6d);
		checkSize(3, null, 3d);
		checkSize(4, null, 6d);
		checkSize(5, null, 3d);
		checkSize(6, null, 2d);
		checkSize(7, null, 2d);
		((Dimensionnable) pg.getConfigurables().get(4)).getDimension().removeHeight();
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().removeHeight();
		verifyInitialVerti();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(5)).getDimension().setHeight(Units.inch(4d));
		checkSize(7, null, 2d);
		((Dimensionnable) pg.getConfigurables().get(7)).getDimension().setHeight(Units.inch(4d));
		checkSize(0, null, 3d);
		checkSize(1, null, 3d);
		checkSize(2, null, 5d);
		checkSize(3, null, 1d);
		checkSize(4, null, 5d);
		checkSize(5, null, 4d);
		checkSize(6, null, 2d);
		checkSize(7, null, 2d);
		((Dimensionnable) pg.getConfigurables().get(7)).getDimension().removeHeight();
		((Dimensionnable) pg.getConfigurables().get(5)).getDimension().removeHeight();
		verifyInitialVerti();
	}

	@Test
	/**
	 * Verify the width change to the minimum (1") and maximum (neighbors cannot be set lower than 1").
	 */
	public void widthChangeMinMaxTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = (ProductGroup) assembly.getConfigurables().get(0);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm("RECTANGLE_N");

		splitRectangleHoriz();
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(5d, 5d));

		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().setWidth(Units.inch(11d));
		checkSize(0, 4d, null);
		checkSize(1, 4d, null);
		checkSize(2, 2d, null);
		checkSize(3, 2d, null);
		checkSize(4, 1d, null);
		checkSize(5, 3d, null);
		checkSize(6, 4d, null);
		checkSize(7, 4d, null);
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().removeWidth();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().setWidth(Units.inch(0.5d));
		checkSize(0, 1d, null);
		checkSize(1, 7d, null);
		checkSize(2, 2d, null);
		checkSize(3, 2d, null);
		checkSize(4, 4d, null);
		checkSize(5, 3d, null);
		checkSize(6, 7d, null);
		checkSize(7, 1d, null);
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().removeWidth();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setWidth(Units.inch(11d));
		checkSize(0, 2d, null);
		checkSize(1, 4d, null);
		checkSize(2, 4d, null);
		checkSize(3, 4d, null);
		checkSize(4, 3d, null);
		checkSize(5, 1d, null);
		checkSize(6, 4d, null);
		checkSize(7, 2d, null);
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removeWidth();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setWidth(Units.inch(0.5d));
		checkSize(0, 2d, null);
		checkSize(1, 7d, null);
		checkSize(2, 1d, null);
		checkSize(3, 1d, null);
		checkSize(4, 3d, null);
		checkSize(5, 4d, null);
		checkSize(6, 7d, null);
		checkSize(7, 2d, null);
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removeWidth();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().setWidth(Units.inch(11d));
		checkSize(0, 1d, null);
		checkSize(1, 8d, null);
		checkSize(2, 1d, null);
		checkSize(3, 1d, null);
		checkSize(4, 4d, null);
		checkSize(5, 4d, null);
		checkSize(6, 8d, null);
		checkSize(7, 1d, null);
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().removeWidth();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().setWidth(Units.inch(0.5d));
		checkSize(0, 4d, null);
		checkSize(1, 2d, null);
		checkSize(2, 4d, null);
		checkSize(3, 4d, null);
		checkSize(4, 1d, null);
		checkSize(5, 1d, null);
		checkSize(6, 2d, null);
		checkSize(7, 4d, null);
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().removeWidth();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(4)).getDimension().setWidth(Units.inch(0.5d));
		checkSize(0, 3d, null);
		checkSize(1, 5d, null);
		checkSize(2, 2d, null);
		checkSize(3, 2d, null);
		checkSize(4, 1d, null);
		checkSize(5, 4d, null);
		checkSize(6, 5d, null);
		checkSize(7, 3d, null);
		((Dimensionnable) pg.getConfigurables().get(4)).getDimension().removeWidth();
		verifyInitialHoriz();

		// -----------------------------------------------------------------------
		// The size is decreased of 1 west and 2 east.
		// When the override is removed, the 3 is divided equally between west and east.
		// The result is that sizes are not as the where in the begining.
		((Dimensionnable) pg.getConfigurables().get(4)).getDimension().setWidth(Units.inch(11d));
		checkSize(0, 1d, null);
		checkSize(1, 7d, null);
		checkSize(2, 2d, null);
		checkSize(3, 2d, null);
		checkSize(4, 6d, null);
		checkSize(5, 1d, null);
		checkSize(6, 7d, null);
		checkSize(7, 1d, null);
		((Dimensionnable) pg.getConfigurables().get(4)).getDimension().removeWidth();
		// This would fail and it is normal : verifyInitialHoriz();

		// Check this special configuration:
		checkSize(0, 2.5d, null);
		checkSize(1, 5.5d, null);
		checkSize(2, 2d, null);
		checkSize(3, 2d, null);
		checkSize(4, 3d, null);
		checkSize(5, 2.5d, null);
		checkSize(6, 5.5d, null);
		checkSize(7, 2.5d, null);
	}

	@Test
	/**
	 * Verify the height change to the minimum (1") and maximum (neighbors cannot be set lower than 1").
	 */
	public void heightChangeMinMaxTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = (ProductGroup) assembly.getConfigurables().get(0);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm("RECTANGLE_N");

		splitRectangleVerti();
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 5d));

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().setHeight(Units.inch(11d));
		checkSize(0, null, 4d);
		checkSize(1, null, 4d);
		checkSize(2, null, 3d);
		checkSize(3, null, 1d);
		checkSize(4, null, 3d);
		checkSize(5, null, 2d);
		checkSize(6, null, 3d);
		checkSize(7, null, 3d);
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().removeHeight();
		verifyInitialVerti();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().setHeight(Units.inch(0.5d));
		checkSize(0, null, 1d);
		checkSize(1, null, 1d);
		checkSize(2, null, 6d);
		checkSize(3, null, 4d);
		checkSize(4, null, 6d);
		checkSize(5, null, 2d);
		checkSize(6, null, 3d);
		checkSize(7, null, 3d);
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().removeHeight();
		verifyInitialVerti();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(6)).getDimension().setHeight(Units.inch(11d));
		checkSize(0, null, 3d);
		checkSize(1, null, 3d);
		checkSize(2, null, 3d);
		checkSize(3, null, 2d);
		checkSize(4, null, 3d);
		checkSize(5, null, 1d);
		checkSize(6, null, 4d);
		checkSize(7, null, 4d);
		((Dimensionnable) pg.getConfigurables().get(6)).getDimension().removeHeight();
		verifyInitialVerti();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(6)).getDimension().setHeight(Units.inch(0.5d));
		checkSize(0, null, 3d);
		checkSize(1, null, 3d);
		checkSize(2, null, 6d);
		checkSize(3, null, 2d);
		checkSize(4, null, 6d);
		checkSize(5, null, 4d);
		checkSize(6, null, 1d);
		checkSize(7, null, 1d);
		((Dimensionnable) pg.getConfigurables().get(6)).getDimension().removeHeight();
		verifyInitialVerti();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setHeight(Units.inch(0.5d));
		checkSize(0, null, 3.5d);
		checkSize(1, null, 3.5d);
		checkSize(2, null, 3.5d);
		checkSize(3, null, 1d);
		checkSize(4, null, 3.5d);
		checkSize(5, null, 2.5d);
		checkSize(6, null, 3d);
		checkSize(7, null, 3d);
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removeHeight();
		verifyInitialVerti();

		// -----------------------------------------------------------------------
		// The size is decreased of 1 south and 2 north.
		// When the override is removed, the 3 is divided equally between south and north.
		// The result is that sizes are not as the where in the beginning.
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setHeight(Units.inch(11d));
		checkSize(0, null, 1d);
		checkSize(1, null, 1d);
		checkSize(2, null, 6d);
		checkSize(3, null, 5d);
		checkSize(4, null, 6d);
		checkSize(5, null, 1d);
		checkSize(6, null, 3d);
		checkSize(7, null, 3d);
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removeHeight();
		// This would fail and it is normal : verifyInitialVerti();

		// Check this special configuration:
		checkSize(0, null, 2.5d);
		checkSize(1, null, 2.5d);
		checkSize(2, null, 4.5d);
		checkSize(3, null, 2d);
		checkSize(4, null, 4.5d);
		checkSize(5, null, 2.5d);
		checkSize(6, null, 3d);
		checkSize(7, null, 3d);
	}

	@Test
	/**
	 * Verify the index reordering (the rendering cannot be tested here)
	 */
	public void indexChangeTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = (ProductGroup) assembly.getConfigurables().get(0);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm("RECTANGLE_N");

		initProperties();
		pg.getDimension().setDefaultWidth(Units.inch(10d));
		pg.getDimension().setDefaultHeight(Units.inch(10d));

		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(4d, 5d));
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(2d, 7d));
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(8d, 6d));
		initProperties();

		// -----------------------------------------------------------------------
		// At first the rendering index == rendering index
		int index = 0;
		for (com.netappsid.erp.configurator.Configurable configurable : pg.getConfigurables())
		{
			Assert.assertEquals((Integer) index, ((com.netappsid.erp.configurator.Configurable) configurable).getIndex());
			Assert.assertEquals((Integer) index, ((com.netappsid.erp.configurator.Configurable) configurable).getRenderingIndex());
			++index;
		} /* end for */

		// Make the configurable #2 smaller so it comes after the #3 (2 and 3 are switched in the model)
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().setHeight(Units.inch(1d));

		// The index has been reordered.
		index = 0;
		for (com.netappsid.erp.configurator.Configurable configurable : pg.getConfigurables())
		{
			Assert.assertEquals((Integer) index, ((com.netappsid.erp.configurator.Configurable) configurable).getIndex());
			++index;
		} /* end for */

		// The rendering index is still as it was before the change (2 and 3 are switched).
		// Assert.assertEquals((Integer)0,((Configurable)pg.getConfigurables().get(0)).getRenderingIndex());
		// Assert.assertEquals((Integer)1,((Configurable)pg.getConfigurables().get(1)).getRenderingIndex());
		// Assert.assertEquals((Integer)2,((Configurable)pg.getConfigurables().get(3)).getRenderingIndex());
		// Assert.assertEquals((Integer)3,((Configurable)pg.getConfigurables().get(2)).getRenderingIndex());

		// Back the way it was (2 is now 3...)
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removeHeight();
		index = 0;
		for (com.netappsid.erp.configurator.Configurable configurable : pg.getConfigurables())
		{
			Assert.assertEquals((Integer) index, ((com.netappsid.erp.configurator.Configurable) configurable).getIndex());
			Assert.assertEquals((Integer) index, ((com.netappsid.erp.configurator.Configurable) configurable).getRenderingIndex());
			++index;
		} /* end for */
	}

	/**
	 * Split a rectangle into 7 pieces: ----------------------- | 0 | 1 | 2 | | | |------| | | | 3 | | |--------| | | | 4 | | | |--------| | | | 5 | | |-----| |
	 * | | 6 | | | -----------------------
	 */
	private void splitRectangleHoriz()
	{
		initProperties();

		pg.getDimension().setDefaultWidth(Units.inch(10d));
		pg.getDimension().setDefaultHeight(Units.inch(10d));

		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(2d, 1d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(8d, 5d));
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 4d));
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 7d));
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(9d, 2d));
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(1d, 8d));
		Assert.assertEquals(7, pg.getConfigurables().size());

		initProperties();
	}

	/**
	 * Split a rectangle into 7 pieces: ----------------------- | 0 | 1 | |---------------------| | 2 | 3 | 4 | |---------------------| | 5 | 6 |
	 * |---------------------|
	 */
	private void splitRectangleVerti()
	{
		initProperties();

		pg.getDimension().setDefaultWidth(Units.inch(10d));
		pg.getDimension().setDefaultHeight(Units.inch(10d));

		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 3d));
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 7d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(2d, 1d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(3d, 5d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(7d, 5d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(9d, 8d));
		Assert.assertEquals(7, pg.getConfigurables().size());

		initProperties();
	}

	/**
	 * Verify that all the components have the initial sizes.
	 */
	private void verifyInitialHoriz()
	{
		checkSize(0, 2d, null);
		checkSize(1, 6d, null);
		checkSize(2, 2d, null);
		checkSize(3, 2d, null);
		checkSize(4, 3d, null);
		checkSize(5, 3d, null);
		checkSize(6, 6d, null);
		checkSize(7, 2d, null);
	}

	/**
	 * Verify that all the components have the initial sizes.
	 */
	private void verifyInitialVerti()
	{
		checkSize(0, null, 3d);
		checkSize(1, null, 3d);
		checkSize(2, null, 4d);
		checkSize(3, null, 2d);
		checkSize(4, null, 4d);
		checkSize(5, null, 2d);
		checkSize(6, null, 3d);
		checkSize(7, null, 3d);
	}
}
