package ProductGroup;

import junit.framework.Assert;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.common.Configurable;
import com.netappsid.erp.configurator.gui.Configurator;
import com.netappsid.wadconfigurator.Assembly;
import com.netappsid.wadconfigurator.ProductGroup;
import com.netappsid.wadconfigurator.ProductTemplate;
import com.netappsid.wadconfigurator.enums.ChoiceForm;

/**
 * PRODUCT GROUP TEST for rectangle double corner cut
 * 
 * @author jcfortier
 */
public class NoFlipRectangleDoubleCornerCutTest extends TestUtils
{
	@BeforeClass
	public static void init() throws Exception
	{}

	@AfterClass
	public static void tearDown()
	{}

	@Before
	public void beforeTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_RDCC", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		initProperties();

		((ProductGroup) pg).setForm(ChoiceForm.F15_RECTANGLEDOUBLECORNERCUT);
		pg.getDimension().setDefaultHeight(Units.inch(50));
		pg.getDimension().setDefaultHeight2(Units.inch(30));
		pg.getDimension().setDefaultWidth(Units.inch(50));
		pg.getDimension().setDefaultWidth2(Units.inch(20));

		Assert.assertEquals("RectangleDoubleCornerCut", ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getForm().getClass().getSimpleName());
		Assert.assertEquals(50d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(50d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);
	}

	@Test
	/**
	 * Verify the resizing of a Half arc quarter circle group
	 */
	public void resizeGroupTest()
	{
		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(25));
		pg.getDimension().setHeight2(Units.inch(0));

		checkSize(0, 50.0, 25.0, 0.0, 20.0);
		checkPosition(0, 0.0, 0.0, "RectangleDoubleCornerCut");

		pg.getDimension().setHeight2(Units.inch(10));

		checkSize(0, 50.0, 25.0, 10.0, 20.0);
		checkPosition(0, 0.0, 0.0, "RectangleDoubleCornerCut");
	}

}
