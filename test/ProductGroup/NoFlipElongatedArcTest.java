package ProductGroup;

import java.awt.geom.Point2D;

import junit.framework.Assert;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.netappsid.commonutils.geo.enums.Orientation;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.common.Configurable;
import com.netappsid.erp.configurator.gui.Configurator;
import com.netappsid.wadconfigurator.Assembly;
import com.netappsid.wadconfigurator.ProductGroup;
import com.netappsid.wadconfigurator.ProductTemplate;
import com.netappsid.wadconfigurator.enums.ChoiceForm;

/**
 * PRODUCT GROUP TEST for ElongatedArc
 * 
 * @author sboule
 */
public class NoFlipElongatedArcTest extends TestUtils
{
	@BeforeClass
	public static void init() throws Exception
	{}

	@AfterClass
	public static void tearDown()
	{}

	@Test
	/**
	 * Verify the split of an elongated arc
	 */
	public void splitElongatedArcTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_EA", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		initProperties();

		((ProductGroup) pg).setForm(ChoiceForm.F05_ELONGATEDARC);
		resizeElongatedArc(20d);

		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);

		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 16d));
		Assert.assertEquals(2, pg.getConfigurables().size());

		checkSize(0, 20d, 16d);
		checkSize(1, 20d, 4d);

		checkPosition(0, 0d, 0d, "ElongatedArc");
		checkPosition(1, 0d, 16d, "Rectangle");
		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(10d, 4d));
		Assert.assertEquals(3, pg.getConfigurables().size());

		checkSize(0, 10d, 16d);
		checkSize(1, 10d, 16d);
		checkSize(2, 20d, 4d);

		checkPosition(0, 0d, 0d, "ElongatedHalfArc");
		checkPosition(1, 10d, 0d, "ElongatedHalfArc");
		checkPosition(2, 0d, 16d, "Rectangle");
	}

	@Test
	/**
	 * Verify the split of an elongated arc
	 */
	public void splitElongatedArcTest2()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_EA", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		initProperties();

		((ProductGroup) pg).setForm(ChoiceForm.F05_ELONGATEDARC);
		resizeElongatedArc(20d);

		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);

		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 11d));
		Assert.assertEquals(2, pg.getConfigurables().size());

		checkSize(0, 20d, 11d);
		checkSize(1, 20d, 9d);

		checkPosition(0, 0d, 0d, "ElongatedArc");
		checkPosition(1, 0d, 11d, "Rectangle");
		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(10d, 4d));
		Assert.assertEquals(3, pg.getConfigurables().size());

		checkSize(0, 10d, 11d);
		checkSize(1, 10d, 11d);
		checkSize(2, 20d, 9d);

		checkPosition(0, 0d, 0d, "ElongatedHalfArc");
		checkPosition(1, 10d, 0d, "ElongatedHalfArc");
		checkPosition(2, 0d, 11d, "Rectangle");
	}

	@Test
	/**
	 * Verify the resizing of an elongated arc
	 */
	public void resizeElongatedArcTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_EA", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		initProperties();

		((ProductGroup) pg).setForm(ChoiceForm.F05_ELONGATEDARC);
		resizeElongatedArc(20d);

		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(10d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight2().doubleValue(Units.INCH), DELTA);

		splitElongatedArc();

		// -----------------------------------------------------------------------
		((ProductTemplate) pg.getConfigurables().get(2)).getDimension().setWidth(Units.inch(6d));

		checkSize(0, 10d, 16d, 6d);
		checkSize(1, 4d, 16d, 15.16515d);
		checkSize(2, 6d, 15.16515d, 6d);
		checkSize(3, 20d, 4d);

		checkPosition(0, 0d, 0d, "ElongatedHalfArc");
		checkPosition(1, 10d, 0d, "ElongatedHalfArc");
		checkPosition(2, 14d, 0.834848d, "ElongatedHalfArc");
		checkPosition(3, 0d, 16d, "Rectangle");

		// -----------------------------------------------------------------------
		((ProductTemplate) pg.getConfigurables().get(3)).getDimension().setHeight(Units.inch(6d));

		checkSize(0, 10d, 14d, 4d);
		checkSize(1, 4d, 14d, 13.16515d);
		checkSize(2, 6d, 13.16515d, 4d);
		checkSize(3, 20d, 6d);

		checkPosition(0, 0d, 0d, "ElongatedHalfArc");
		checkPosition(1, 10d, 0d, "ElongatedHalfArc");
		checkPosition(2, 14d, 0.834848d, "ElongatedHalfArc");
		checkPosition(3, 0d, 14d, "Rectangle");

		// -----------------------------------------------------------------------
		((ProductTemplate) pg.getConfigurables().get(3)).getDimension().removeHeight();

		checkSize(0, 10d, 16d, 6d);
		checkSize(1, 4d, 16d, 15.16515d);
		checkSize(2, 6d, 15.16515d, 6d);
		checkSize(3, 20d, 4d);

		checkPosition(0, 0d, 0d, "ElongatedHalfArc");
		checkPosition(1, 10d, 0d, "ElongatedHalfArc");
		checkPosition(2, 14d, 0.834848d, "ElongatedHalfArc");
		checkPosition(3, 0d, 16d, "Rectangle");

		// -----------------------------------------------------------------------
		((ProductTemplate) pg.getConfigurables().get(2)).getDimension().removeWidth();
		verifyInitial();
	}

	@Test
	/**
	 * Verify the resizing of an elongated arc group
	 */
	public void resizeGroupElongatedArcTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_EA", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		initProperties();

		((ProductGroup) pg).setForm(ChoiceForm.F05_ELONGATEDARC);
		resizeElongatedArc(20d);

		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);

		splitElongatedArc();

		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(18d));
		pg.getDimension().setHeight2(Units.inch(12d));

		checkSize(0, 9d, 16d, 8d);
		checkSize(1, 4.5d, 16d, 14.80381d);
		checkSize(2, 4.5d, 14.80381d, 8d);
		checkSize(3, 18d, 4d);

		checkPosition(0, 0d, 0d, "ElongatedHalfArc");
		checkPosition(1, 9d, 0d, "ElongatedHalfArc");
		checkPosition(2, 13.5d, 1.19618d, "ElongatedHalfArc");
		checkPosition(3, 0d, 16d, "Rectangle");

		pg.getDimension().removeHeight2();
		pg.getDimension().removeWidth();
		verifyInitial();

		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(22d));
		checkSize(0, 11.0, 16.0, 5.0, null);
		checkPosition(0, 0.0, 0.0, "ElongatedHalfArc");
		checkRay(0, 11.0);
		checkSize(1, 5.5, 16.0, 14.5262, null);
		checkPosition(1, 11.0, 0.0, "ElongatedHalfArc");
		checkRay(1, 11.0);
		checkSize(2, 5.5, 14.5262, 5.0, null);
		checkPosition(2, 16.5, 1.47372, "ElongatedHalfArc");
		checkRay(2, 11.0);
		checkSize(3, 22.0, 4.0, null, null);
		checkPosition(3, 0.0, 16.0, "Rectangle");

		pg.getDimension().removeWidth();
		pg.getDimension().setHeight2(Units.inch(10d));
		verifyInitial();

		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(18d));
		checkSize(0, 10.0, 14.4, 4.4, null);
		checkPosition(0, 0.0, 0.0, "ElongatedHalfArc");
		checkRay(0, 10.0);
		checkSize(1, 5.0, 14.4, 13.06025, null);
		checkPosition(1, 10.0, 0.0, "ElongatedHalfArc");
		checkRay(1, 10.0);
		checkSize(2, 5.0, 13.06025, 4.4, null);
		checkPosition(2, 15.0, 1.33974, "ElongatedHalfArc");
		checkRay(2, 10.0);
		checkSize(3, 20.0, 3.6, null, null);
		checkPosition(3, 0.0, 14.4, "Rectangle");

		pg.getDimension().removeHeight2();
		pg.getDimension().removeHeight();
		pg.getDimension().setHeight2(Units.inch(10d));
		verifyInitial();

		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(22d));
		pg.getDimension().setHeight2(Units.inch(12d));

		checkSize(0, 10d, 17.6d, 7.6d);
		checkSize(1, 5d, 17.6d, 16.260254d);
		checkSize(2, 5d, 16.260254d, 7.6d);
		checkSize(3, 20d, 4.4d);

		checkPosition(0, 0d, 0d, "ElongatedHalfArc");
		checkPosition(1, 10d, 0d, "ElongatedHalfArc");
		checkPosition(2, 15d, 1.3397459d, "ElongatedHalfArc");
		checkPosition(3, 0d, 17.6d, "Rectangle");

		pg.getDimension().removeHeight2();
		pg.getDimension().removeHeight();
		pg.getDimension().setHeight2(Units.inch(10d));
		verifyInitial();
	}

	@Test
	/**
	 * Verify the resizing of an elongated arc group that has an override
	 */
	public void resizeGroupElongatedArcTestWithOverride()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_EA", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		initProperties();

		((ProductGroup) pg).setForm(ChoiceForm.F05_ELONGATEDARC);
		resizeElongatedArc(20d);

		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);

		splitElongatedArc();

		// -----------------------------------------------------------------------
		((ProductTemplate) pg.getConfigurables().get(2)).getDimension().setWidth(Units.inch(6d));
		checkSize(0, 10.0, 16.0, 6.0, null);
		checkPosition(0, 0.0, 0.0, "ElongatedHalfArc");
		checkRay(0, 10.0);
		checkSize(1, 4.0, 16.0, 15.16515, null);
		checkPosition(1, 10.0, 0.0, "ElongatedHalfArc");
		checkRay(1, 10.0);
		checkSize(2, 6.0, 15.16515, 6.0, null);
		checkPosition(2, 14.0, 0.8348, "ElongatedHalfArc");
		checkRay(2, 10.0);
		checkSize(3, 20.0, 4.0, null, null);
		checkPosition(3, 0.0, 16.0, "Rectangle");

		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(25d));
		checkSize(0, 13.571428, 16.0, 3.5, null);
		checkPosition(0, 0.0, 0.0, "ElongatedHalfArc");
		checkRay(0, 13.622);
		checkSize(1, 5.4285714, 15.954, 14.177, null);
		checkPosition(1, 13.5714288, 0.046003, "ElongatedHalfArc");
		checkRay(1, 12.5);
		checkSize(2, 6.0, 14.177078, 3.5, null);
		checkPosition(2, 19.0, 1.8229, "ElongatedHalfArc");
		checkRay(2, 12.5);
		checkSize(3, 25.0, 4.0, null, null);
		checkPosition(3, 0.0, 16.0, "Rectangle");
	}

	@Test
	/**
	 * Verify the resizing of an h2 split elongated arc
	 * The arc is split in the middle
	 */
	public void h2SplitTest1()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_EA", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		initProperties();

		((ProductGroup) pg).setForm(ChoiceForm.F05_ELONGATEDARC);
		resizeElongatedArc(20d);

		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(10d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight2().doubleValue(Units.INCH), DELTA);

		// Make the splits
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(10d, 5d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(10d, 5d));

		checkSize(0, 10.0, 10.0, null, null);
		checkPosition(0, 0.0, 0.0, "HalfArc");
		checkSize(1, 10.0, 10.0, null, null);
		checkPosition(1, 10.0, 0.0, "HalfArc");
		checkSize(2, 20.0, 10.0, null, null);
		checkPosition(2, 0.0, 10.0, "Rectangle");

		// -----------------------------------------------------------------------
		pg.getDimension().setHeight2(Units.inch(15d));
		checkSize(0, 10.0, 5.0, null, null);
		checkPosition(0, 0.0, 0.0, "HalfArc");
		checkSize(1, 10.0, 5.0, null, null);
		checkPosition(1, 10.0, 0.0, "HalfArc");
		checkSize(2, 20.0, 15.0, null, null);
		checkPosition(2, 0.0, 5.0, "Rectangle");
		checkRay(0, 12.5);
		checkRay(1, 12.5);

		// -----------------------------------------------------------------------
		pg.getDimension().removeHeight2();
		checkSize(0, 10.0, 10.0, null, null);
		checkPosition(0, 0.0, 0.0, "HalfArc");
		checkSize(1, 10.0, 10.0, null, null);
		checkPosition(1, 10.0, 0.0, "HalfArc");
		checkSize(2, 20.0, 10.0, null, null);
		checkPosition(2, 0.0, 10.0, "Rectangle");
		checkRay(0, 10.0);
		checkRay(1, 10.0);

		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(15d));
		checkSize(0, 10.0, 7.5, 0.0, null);
		checkPosition(0, 0.0, 0.0, "HalfArc");
		checkRay(0, 10.417);
		checkSize(1, 10.0, 7.5, 0.0, null);
		checkPosition(1, 10.0, 0.0, "HalfArc");
		checkRay(1, 10.417);
		checkSize(2, 20.0, 7.5, null, null);
		checkPosition(2, 0.0, 7.5, "Rectangle");

		// -----------------------------------------------------------------------
		pg.getDimension().removeHeight();
		pg.getDimension().setHeight2(Units.inch(10d));
		checkSize(0, 10.0, 10.0, null, null);
		checkPosition(0, 0.0, 0.0, "HalfArc");
		checkSize(1, 10.0, 10.0, null, null);
		checkPosition(1, 10.0, 0.0, "HalfArc");
		checkSize(2, 20.0, 10.0, null, null);
		checkPosition(2, 0.0, 10.0, "Rectangle");
		checkRay(0, 10.0);
		checkRay(1, 10.0);

		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(25d));
		checkSize(0, 12.5, 10.0, 0.0, null);
		checkPosition(0, 0.0, 0.0, "HalfArc");
		checkRay(0, 12.813);
		checkSize(1, 12.5, 10.0, 0.0, null);
		checkPosition(1, 12.5, 0.0, "HalfArc");
		checkRay(1, 12.8125);
		checkSize(2, 25.0, 10.0, null, null);
		checkPosition(2, 0.0, 10.0, "Rectangle");

		// -----------------------------------------------------------------------
		pg.getDimension().removeWidth();
		pg.getDimension().setHeight2(Units.inch(10));
		checkSize(0, 10.0, 10.0, null, null);
		checkPosition(0, 0.0, 0.0, "HalfArc");
		checkSize(1, 10.0, 10.0, null, null);
		checkPosition(1, 10.0, 0.0, "HalfArc");
		checkSize(2, 20.0, 10.0, null, null);
		checkPosition(2, 0.0, 10.0, "Rectangle");
		checkRay(0, 10.0);
		checkRay(1, 10.0);
	}

	@Test
	/**
	 * Verify the resizing of an h2 split elongated arc
	 * The arc is split in 3.
	 */
	public void h2SplitTest2()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_EA", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		initProperties();

		((ProductGroup) pg).setForm(ChoiceForm.F05_ELONGATEDARC);
		resizeElongatedArc(18d);

		Assert.assertEquals(18d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(18d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(9d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight2().doubleValue(Units.INCH), DELTA);

		// Make the splits
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(10d, 5d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(9d, 5d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(12d, 5d));
		checkSize(0, 9.0, 9.0, 0.0, null);
		checkPosition(0, 0.0, 0.0, "HalfArc");
		checkRay(0, 9.0);
		checkSize(1, 3.0, 9.0, 8.48528137, null);
		checkPosition(1, 9.0, 0.0, "ElongatedHalfArc");
		checkRay(1, 9.0);
		checkSize(2, 6.0, 8.48528137, 0.0, null);
		checkPosition(2, 12.0, 0.514718, "HalfArc");
		checkRay(2, 9.0);
		checkSize(3, 18.0, 9.0, null, null);
		checkPosition(3, 0.0, 9.0, "Rectangle");

		// -----------------------------------------------------------------------
		pg.getDimension().setHeight2(Units.inch(15d));
		checkSize(0, 9.0, 3.0, 0.0, null);
		checkPosition(0, 0.0, 0.0, "HalfArc");
		checkRay(0, 15.0);
		checkSize(1, 3.0, 3.0, 2.696938, null);
		checkPosition(1, 9.0, 0.0, "ElongatedHalfArc");
		checkRay(1, 15.0);
		checkSize(2, 6.0, 2.696938, 0.0, null);
		checkPosition(2, 12.0, 0.30306, "HalfArc");
		checkRay(2, 15.0);
		checkSize(3, 18.0, 15.0, null, null);
		checkPosition(3, 0.0, 3.0, "Rectangle");

		// -----------------------------------------------------------------------
		pg.getDimension().removeHeight2();
		checkSize(0, 9.0, 9.0, 0.0, null);
		checkPosition(0, 0.0, 0.0, "HalfArc");
		checkRay(0, 9.0);
		checkSize(1, 3.0, 9.0, 8.48528137, null);
		checkPosition(1, 9.0, 0.0, "ElongatedHalfArc");
		checkRay(1, 9.0);
		checkSize(2, 6.0, 8.48528137, 0.0, null);
		checkPosition(2, 12.0, 0.514718, "HalfArc");
		checkRay(2, 9.0);
		checkSize(3, 18.0, 9.0, null, null);
		checkPosition(3, 0.0, 9.0, "Rectangle");

		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(15d));
		checkSize(0, 9.0, 7.5, 0.0, null);
		checkPosition(0, 0.0, 0.0, "HalfArc");
		checkRay(0, 9.15);
		checkSize(1, 3.0, 7.5, 7.0, null);
		checkPosition(1, 9.0, 0.0, "ElongatedHalfArc");
		checkRay(1, 9.151);
		checkSize(2, 6.0, 7.0, 0.0, null);
		checkPosition(2, 12.0, 0.50578, "HalfArc");
		checkRay(2, 9.15);
		checkSize(3, 18.0, 7.5, null, null);
		checkPosition(3, 0.0, 7.5, "Rectangle");

		// -----------------------------------------------------------------------
		pg.getDimension().removeHeight();
		pg.getDimension().setHeight2(Units.inch(9d));
		checkSize(0, 9.0, 9.0, 0.0, null);
		checkPosition(0, 0.0, 0.0, "HalfArc");
		checkRay(0, 9.0);
		checkSize(1, 3.0, 9.0, 8.48528137, null);
		checkPosition(1, 9.0, 0.0, "ElongatedHalfArc");
		checkRay(1, 9.0);
		checkSize(2, 6.0, 8.48528137, 0.0, null);
		checkPosition(2, 12.0, 0.514718, "HalfArc");
		checkRay(2, 9.0);
		checkSize(3, 18.0, 9.0, null, null);
		checkPosition(3, 0.0, 9.0, "Rectangle");

		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(25d));
		checkSize(0, 12.5, 9.0, 0.0, null);
		checkPosition(0, 0.0, 0.0, "HalfArc");
		checkRay(0, 13.18);
		checkSize(1, 4.16666, 9.0, 8.32408, null);
		checkPosition(1, 12.5, 0.0, "ElongatedHalfArc");
		checkRay(1, 13.18);
		checkSize(2, 8.333332, 8.324, 0.0, null);
		checkPosition(2, 16.666666, 0.675919, "HalfArc");
		checkRay(2, 13.18);
		checkSize(3, 25.0, 9.0, null, null);
		checkPosition(3, 0.0, 9.0, "Rectangle");

		// -----------------------------------------------------------------------
		pg.getDimension().removeWidth();
		pg.getDimension().setHeight2(Units.inch(9d));
		checkSize(0, 9.0, 9.0, 0.0, null);
		checkPosition(0, 0.0, 0.0, "HalfArc");
		checkRay(0, 9.0);
		checkSize(1, 3.0, 9.0, 8.48528137, null);
		checkPosition(1, 9.0, 0.0, "ElongatedHalfArc");
		checkRay(1, 9.0);
		checkSize(2, 6.0, 8.48528137, 0.0, null);
		checkPosition(2, 12.0, 0.514718, "HalfArc");
		checkRay(2, 9.0);
		checkSize(3, 18.0, 9.0, null, null);
		checkPosition(3, 0.0, 9.0, "Rectangle");

		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(15d));
		checkSize(0, 7.5, 6.0, 0.0, null);
		checkPosition(0, 0.0, 0.0, "HalfArc");
		checkRay(0, 7.6875);
		checkSize(1, 2.5, 6.0, 5.582139, null);
		checkPosition(1, 7.5, 0.0, "ElongatedHalfArc");
		checkRay(1, 7.6875);
		checkSize(2, 5.0, 5.582139, 0.0, null);
		checkPosition(2, 10.0, 0.41786, "HalfArc");
		checkRay(2, 7.6875);
		checkSize(3, 15.0, 12.0, null, null);
		checkPosition(3, 0.0, 6.0, "Rectangle");

		// -----------------------------------------------------------------------
		pg.getDimension().removeWidth();
		pg.getDimension().setHeight2(Units.inch(9d));
		checkSize(0, 9.0, 9.0, 0.0, null);
		checkPosition(0, 0.0, 0.0, "HalfArc");
		checkRay(0, 9.0);
		checkSize(1, 3.0, 9.0, 8.48528137, null);
		checkPosition(1, 9.0, 0.0, "ElongatedHalfArc");
		checkRay(1, 9.0);
		checkSize(2, 6.0, 8.48528137, 0.0, null);
		checkPosition(2, 12.0, 0.514718, "HalfArc");
		checkRay(2, 9.0);
		checkSize(3, 18.0, 9.0, null, null);
		checkPosition(3, 0.0, 9.0, "Rectangle");
	}

	/**
	 * Split the elongated arc in 4. Verify the result.
	 */
	private void splitElongatedArc()
	{
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 16d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(10d, 10d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(15d, 10d));
		Assert.assertEquals(4, pg.getConfigurables().size());

		verifyInitial();
	}

	private void verifyInitial()
	{
		checkSize(0, 10d, 16d, 6d);
		checkSize(1, 5d, 16d, 14.66025d);
		checkSize(2, 5d, 14.66025d, 6d);
		checkSize(3, 20d, 4d);

		checkPosition(0, 0d, 0d, "ElongatedHalfArc");
		checkPosition(1, 10d, 0d, "ElongatedHalfArc");
		checkPosition(2, 15d, 1.33974d, "ElongatedHalfArc");
		checkPosition(3, 0d, 16d, "Rectangle");
	}
}
