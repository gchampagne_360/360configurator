package ProductGroup;

import java.awt.geom.Point2D;

import junit.framework.Assert;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.netappsid.commonutils.geo.enums.Orientation;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.common.Configurable;
import com.netappsid.erp.configurator.gui.Configurator;
import com.netappsid.wadconfigurator.Assembly;
import com.netappsid.wadconfigurator.ProductGroup;
import com.netappsid.wadconfigurator.ProductTemplate;
import com.netappsid.wadconfigurator.enums.ChoiceForm;

/**
 * PRODUCT GROUP TEST for DogHouse
 * 
 * @author sboule
 */
public class NoFlipDogHouseTest extends TestUtils
{
	@BeforeClass
	public static void init() throws Exception
	{}

	@AfterClass
	public static void tearDown()
	{}

	@Test
	/**
	 * Verify the split of a DogHouse
	 */
	public void splitDogHouseTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_DH", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		initProperties();

		((ProductGroup) pg).setForm(ChoiceForm.F10_DOGHOUSE_SYMETRIC);
		resize(20d);

		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);

		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 16d));
		Assert.assertEquals(2, pg.getConfigurables().size());

		checkSize(0, 20d, 16d);
		checkSize(1, 20d, 4d);

		checkPosition(0, 0d, 0d, "DogHouse");
		checkPosition(1, 0d, 16d, "Rectangle");
		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(10d, 4d));
		Assert.assertEquals(3, pg.getConfigurables().size());

		checkSize(0, 10d, 16d);
		checkSize(1, 10d, 16d);
		checkSize(2, 20d, 4d);

		checkPosition(0, 0d, 0d, "Trapeze");
		checkPosition(1, 10d, 0d, "Trapeze");
		checkPosition(2, 0d, 16d, "Rectangle");
		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 12d));
		Assert.assertEquals(4, pg.getConfigurables().size());

		checkSize(0, 10d, 12d);
		checkSize(1, 10d, 16d);
		checkSize(2, 10d, 4d);
		checkSize(3, 20d, 4d);

		checkPosition(0, 0d, 0d, "Trapeze");
		checkPosition(1, 10d, 0d, "Trapeze");
		checkPosition(2, 0d, 12d, "Rectangle");
		checkPosition(3, 0d, 16d, "Rectangle");
		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(15d, 12d));
		Assert.assertEquals(5, pg.getConfigurables().size());

		checkSize(0, 10d, 12d);
		checkSize(1, 5d, 16d);
		checkSize(2, 5d, 11d);
		checkSize(3, 10d, 4d);
		checkSize(4, 20d, 4d);

		checkPosition(0, 0d, 0d, "Trapeze");
		checkPosition(1, 10d, 0d, "Trapeze");
		checkPosition(2, 15d, 5d, "Trapeze");
		checkPosition(3, 0d, 12d, "Rectangle");
		checkPosition(4, 0d, 16d, "Rectangle");
	}

	@Test
	/**
	 * Verify the resizing of a DogHouse
	 */
	public void resizeDogHouseTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_DH", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		initProperties();

		((ProductGroup) pg).setForm(ChoiceForm.F10_DOGHOUSE_SYMETRIC);
		resize(20d);

		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);

		splitDogHouse();

		// -----------------------------------------------------------------------
		((ProductTemplate) pg.getConfigurables().get(0)).getDimension().setWidth(Units.inch(18d));

		checkSize(0, 18d, 16d);
		checkSize(1, 2d, 8d);
		checkSize(2, 20d, 4d);

		checkPosition(0, 0d, 0d, "DogHouse");
		checkPosition(1, 18d, 8d, "Trapeze");
		checkPosition(2, 0d, 16d, "Rectangle");

		// -----------------------------------------------------------------------
		((ProductTemplate) pg.getConfigurables().get(2)).getDimension().setHeight(Units.inch(6d));

		checkSize(0, 18d, 14d);
		checkSize(1, 2d, 6d);
		checkSize(2, 20d, 6d);

		checkPosition(0, 0d, 0d, "DogHouse");
		checkPosition(1, 18d, 8d, "Trapeze");
		checkPosition(2, 0d, 14d, "Rectangle");

		// -----------------------------------------------------------------------
		((ProductTemplate) pg.getConfigurables().get(2)).getDimension().removeHeight();
		checkSize(0, 18d, 16d);
		checkSize(1, 2d, 8d);
		checkSize(2, 20d, 4d);

		checkPosition(0, 0d, 0d, "DogHouse");
		checkPosition(1, 18d, 8d, "Trapeze");
		checkPosition(2, 0d, 16d, "Rectangle");

		// -----------------------------------------------------------------------
		((ProductTemplate) pg.getConfigurables().get(0)).getDimension().removeWidth();
		verifyInitial();
	}

	@Test
	/**
	 * Verify the max resizing of a DogHouse
	 */
	public void resizeMaxDogHouseTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_DH", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		initProperties();

		((ProductGroup) pg).setForm(ChoiceForm.F10_DOGHOUSE_SYMETRIC);
		resize(20d);

		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);

		splitDogHouse();

		// -----------------------------------------------------------------------
		((ProductTemplate) pg.getConfigurables().get(0)).getDimension().setWidth(Units.inch(99d));

		checkSize(0, 19d, 16d);
		checkSize(1, 1d, 7d);
		checkSize(2, 20d, 4d);

		checkPosition(0, 0d, 0d, "DogHouse");
		checkPosition(1, 19d, 9d, "Trapeze");
		checkPosition(2, 0d, 16d, "Rectangle");
		((ProductTemplate) pg.getConfigurables().get(0)).getDimension().removeWidth();

		// -----------------------------------------------------------------------
		((ProductTemplate) pg.getConfigurables().get(1)).getDimension().setWidth(Units.inch(99d));

		checkSize(0, 11d, 16d);
		checkSize(1, 9d, 15d);
		checkSize(2, 20d, 4d);

		checkPosition(0, 0d, 0d, "DogHouse");
		checkPosition(1, 11d, 1d, "Trapeze");
		checkPosition(2, 0d, 16d, "Rectangle");
		((ProductTemplate) pg.getConfigurables().get(1)).getDimension().removeWidth();

		// -----------------------------------------------------------------------
		((ProductTemplate) pg.getConfigurables().get(2)).getDimension().setHeight(Units.inch(99d));

		checkSize(0, 15d, 11d);
		checkSize(1, 5d, 6d);
		checkSize(2, 20d, 9d);

		checkPosition(0, 0d, 0d, "DogHouse");
		checkPosition(1, 15d, 5d, "Trapeze");
		checkPosition(2, 0d, 11d, "Rectangle");

		// -----------------------------------------------------------------------
		((ProductTemplate) pg.getConfigurables().get(2)).getDimension().removeHeight();
		verifyInitial();
	}

	@Test
	/**
	 * Verify the resizing of an h2 split elongated arc
	 * The triangle is split in the middle
	 */
	public void h2SplitTest1()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_DH", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		initProperties();

		((ProductGroup) pg).setForm(ChoiceForm.F10_DOGHOUSE_SYMETRIC);
		resize(20d);

		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(10d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight2().doubleValue(Units.INCH), DELTA);

		// Make the splits
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(10d, 5d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(10d, 5d));

		checkSize(0, 10.0, 10.0, null, null);
		checkPosition(0, 0.0, 0.0, "Triangle");
		checkSize(1, 10.0, 10.0, null, null);
		checkPosition(1, 10.0, 0.0, "Triangle");
		checkSize(2, 20.0, 10.0, null, null);
		checkPosition(2, 0.0, 10.0, "Rectangle");

		// -----------------------------------------------------------------------
		pg.getDimension().setHeight2(Units.inch(15d));

		checkSize(0, 10.0, 5.0, null, null);
		checkPosition(0, 0.0, 0.0, "Triangle");
		checkSize(1, 10.0, 5.0, null, null);
		checkPosition(1, 10.0, 0.0, "Triangle");
		checkSize(2, 20.0, 15.0, null, null);
		checkPosition(2, 0.0, 5.0, "Rectangle");

		// -----------------------------------------------------------------------
		pg.getDimension().removeHeight2();

		checkSize(0, 10.0, 10.0, null, null);
		checkPosition(0, 0.0, 0.0, "Triangle");
		checkSize(1, 10.0, 10.0, null, null);
		checkPosition(1, 10.0, 0.0, "Triangle");
		checkSize(2, 20.0, 10.0, null, null);
		checkPosition(2, 0.0, 10.0, "Rectangle");

		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(15d));

		checkSize(0, 10.0, 7.5, 0.0, 10.0);
		checkPosition(0, 0.0, 0.0, "Triangle");
		checkSize(1, 10.0, 7.5, 0.0, 10.0);
		checkPosition(1, 10.0, 0.0, "Triangle");
		checkSize(2, 20.0, 7.5, null, null);
		checkPosition(2, 0.0, 7.5, "Rectangle");

		// -----------------------------------------------------------------------
		pg.getDimension().removeHeight();
		pg.getDimension().setHeight2(Units.inch(10d));

		checkSize(0, 10.0, 10.0, null, null);
		checkPosition(0, 0.0, 0.0, "Triangle");
		checkSize(1, 10.0, 10.0, null, null);
		checkPosition(1, 10.0, 0.0, "Triangle");
		checkSize(2, 20.0, 10.0, null, null);
		checkPosition(2, 0.0, 10.0, "Rectangle");

		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(25d));
		checkSize(0, 12.5, 10.0, 0.0, 12.5);
		checkPosition(0, 0.0, 0.0, "Triangle");
		checkSize(1, 12.5, 10.0, 0.0, 12.5);
		checkPosition(1, 12.5, 0.0, "Triangle");
		checkSize(2, 25.0, 10.0, null, null);
		checkPosition(2, 0.0, 10.0, "Rectangle");

		// -----------------------------------------------------------------------
		pg.getDimension().removeWidth();
		pg.getDimension().setHeight2(Units.inch(10d));
		checkSize(0, 10.0, 10.0, null, null);
		checkPosition(0, 0.0, 0.0, "Triangle");
		checkSize(1, 10.0, 10.0, null, null);
		checkPosition(1, 10.0, 0.0, "Triangle");
		checkSize(2, 20.0, 10.0, null, null);
		checkPosition(2, 0.0, 10.0, "Rectangle");
	}

	@Test
	/**
	 * Verify the resizing of an h2 split elongated arc
	 * The arc is split in the middle
	 */
	public void h2SplitTest2()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_DH", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		initProperties();

		((ProductGroup) pg).setForm(ChoiceForm.F10_DOGHOUSE_SYMETRIC);
		resize(20d);

		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(10d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight2().doubleValue(Units.INCH), DELTA);

		// Make the splits
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(10d, 5d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(10d, 5d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(5d, 5d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(15d, 5d));

		checkSize(0, 5.0, 5.0, null, null);
		checkPosition(0, 0.0, 5.0, "Triangle");
		checkSize(1, 5.0, 10.0, 5.0, null);
		checkPosition(1, 5.0, 0.0, "Trapeze");
		checkSize(2, 5.0, 10.0, 5.0, null);
		checkPosition(2, 10.0, 0.0, "Trapeze");
		checkSize(3, 5.0, 5.0, null, null);
		checkPosition(3, 15.0, 5.0, "Triangle");
		checkSize(4, 20.0, 10.0, null, null);
		checkPosition(4, 0.0, 10.0, "Rectangle");

		// -----------------------------------------------------------------------
		pg.getDimension().setHeight2(Units.inch(15d));
		checkSize(0, 5.0, 2.5, null, null);
		checkPosition(0, 0.0, 2.5, "Triangle");
		checkSize(1, 5.0, 5.0, 2.5, null);
		checkPosition(1, 5.0, 0.0, "Trapeze");
		checkSize(2, 5.0, 5.0, 2.5, null);
		checkPosition(2, 10.0, 0.0, "Trapeze");
		checkSize(3, 5.0, 2.5, null, null);
		checkPosition(3, 15.0, 2.5, "Triangle");
		checkSize(4, 20.0, 15.0, null, null);
		checkPosition(4, 0.0, 5.0, "Rectangle");

		// -----------------------------------------------------------------------
		pg.getDimension().removeHeight2();
		checkSize(0, 5.0, 5.0, null, null);
		checkPosition(0, 0.0, 5.0, "Triangle");
		checkSize(1, 5.0, 10.0, 5.0, null);
		checkPosition(1, 5.0, 0.0, "Trapeze");
		checkSize(2, 5.0, 10.0, 5.0, null);
		checkPosition(2, 10.0, 0.0, "Trapeze");
		checkSize(3, 5.0, 5.0, null, null);
		checkPosition(3, 15.0, 5.0, "Triangle");
		checkSize(4, 20.0, 10.0, null, null);
		checkPosition(4, 0.0, 10.0, "Rectangle");

		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(15d));
		checkSize(0, 5.0, 3.75, 0.0, 5.0);
		checkPosition(0, 0.0, 3.75, "Triangle");
		checkSize(1, 5.0, 7.5, 3.75, null);
		checkPosition(1, 5.0, 0.0, "Trapeze");
		checkSize(2, 5.0, 7.5, 3.75, null);
		checkPosition(2, 10.0, 0.0, "Trapeze");
		checkSize(3, 5.0, 3.75, 0.0, 5.0);
		checkPosition(3, 15.0, 3.75, "Triangle");
		checkSize(4, 20.0, 7.5, null, null);
		checkPosition(4, 0.0, 7.5, "Rectangle");

		// -----------------------------------------------------------------------
		pg.getDimension().removeHeight();
		pg.getDimension().setHeight2(Units.inch(10d));
		checkSize(0, 5.0, 5.0, null, null);
		checkPosition(0, 0.0, 5.0, "Triangle");
		checkSize(1, 5.0, 10.0, 5.0, null);
		checkPosition(1, 5.0, 0.0, "Trapeze");
		checkSize(2, 5.0, 10.0, 5.0, null);
		checkPosition(2, 10.0, 0.0, "Trapeze");
		checkSize(3, 5.0, 5.0, null, null);
		checkPosition(3, 15.0, 5.0, "Triangle");
		checkSize(4, 20.0, 10.0, null, null);
		checkPosition(4, 0.0, 10.0, "Rectangle");

		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(25d));
		checkSize(0, 6.25, 2.5, 0.0, 6.25);
		checkPosition(0, 0.0, 2.5, "Triangle");
		checkSize(1, 6.25, 5.0, 2.5, null);
		checkPosition(1, 6.25, 0.0, "Trapeze");
		checkSize(2, 6.25, 5.0, 2.5, null);
		checkPosition(2, 12.5, 0.0, "Trapeze");
		checkSize(3, 6.25, 2.5, 0.0, 6.25);
		checkPosition(3, 18.75, 2.5, "Triangle");
		checkSize(4, 25.0, 15.0, null, null);
		checkPosition(4, 0.0, 5.0, "Rectangle");

		// -----------------------------------------------------------------------
		pg.getDimension().removeWidth();
		pg.getDimension().setHeight2(Units.inch(10d));
		checkSize(0, 5.0, 5.0, null, null);
		checkPosition(0, 0.0, 5.0, "Triangle");
		checkSize(1, 5.0, 10.0, 5.0, null);
		checkPosition(1, 5.0, 0.0, "Trapeze");
		checkSize(2, 5.0, 10.0, 5.0, null);
		checkPosition(2, 10.0, 0.0, "Trapeze");
		checkSize(3, 5.0, 5.0, null, null);
		checkPosition(3, 15.0, 5.0, "Triangle");
		checkSize(4, 20.0, 10.0, null, null);
		checkPosition(4, 0.0, 10.0, "Rectangle");
	}

	@Test
	/**
	 * Verify the resize of a DogHouse to triangle then back to a doghouse
	 */
	public void resizeDogHouseToTriangleTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_DH", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		initProperties();

		((ProductGroup) pg).setForm(ChoiceForm.F10_DOGHOUSE_SYMETRIC);
		resize(20d);

		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);

		// -----------------------------------------------------------------------
		pg.getDimension().setHeight2(Units.inch(0d));
		checkSize(0, 20.0, 20.0, 0.0, 10.0);
		checkPosition(0, 0.0, 0.0, "DogHouse");

		pg.getDimension().removeHeight2();
		checkSize(0, 20.0, 20.0, 10.0, 10.0);
		checkPosition(0, 0.0, 0.0, "DogHouse");
	}

	/**
	 * Split the dog house in 3. Verify the result.
	 */
	private void splitDogHouse()
	{
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 16d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(15d, 10d));
		Assert.assertEquals(3, pg.getConfigurables().size());

		verifyInitial();
	}

	private void verifyInitial()
	{
		checkSize(0, 15d, 16d);
		checkSize(1, 5d, 11d);
		checkSize(2, 20d, 4d);

		checkPosition(0, 0d, 0d, "DogHouse");
		checkPosition(1, 15d, 5d, "Trapeze");
		checkPosition(2, 0d, 16d, "Rectangle");
	}
}
