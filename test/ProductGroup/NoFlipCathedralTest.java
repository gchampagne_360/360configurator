package ProductGroup;

import java.awt.geom.Point2D;

import junit.framework.Assert;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.netappsid.commonutils.geo.enums.Orientation;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.common.Configurable;
import com.netappsid.erp.configurator.gui.Configurator;
import com.netappsid.wadconfigurator.Assembly;
import com.netappsid.wadconfigurator.ProductGroup;
import com.netappsid.wadconfigurator.ProductTemplate;
import com.netappsid.wadconfigurator.enums.ChoiceForm;

/**
 * PRODUCT GROUP TEST for Cathedral
 * 
 * @author sboule
 */
public class NoFlipCathedralTest extends TestUtils
{
	@BeforeClass
	public static void init() throws Exception
	{}

	@AfterClass
	public static void tearDown()
	{}

	@Test
	/**
	 * Verify the split of a cathedral
	 */
	public void splitCathedralTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_C", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		initProperties();

		((ProductGroup) pg).setForm(ChoiceForm.F18_CATHEDRAL);
		resize(20d);

		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);

		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 16d));
		Assert.assertEquals(2, pg.getConfigurables().size());

		checkSize(0, 20d, 16d);
		checkSize(1, 20d, 4d);

		checkPosition(0, 0d, 0d, "Cathedral");
		checkPosition(1, 0d, 16d, "Rectangle");
		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(10d, 4d));
		Assert.assertEquals(3, pg.getConfigurables().size());

		checkSize(0, 10d, 16d);
		checkSize(1, 10d, 16d);
		checkSize(2, 20d, 4d);

		checkPosition(0, 0d, 0d, "ElongatedHalfArc");
		checkPosition(1, 10d, 0d, "ElongatedHalfArc");
		checkPosition(2, 0d, 16d, "Rectangle");
	}

	@Test
	/**
	 * Verify the split of a Cathedral
	 */
	public void splitCathedralTest2()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_C", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		initProperties();

		((ProductGroup) pg).setForm(ChoiceForm.F18_CATHEDRAL);
		resize(20d);

		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);

		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 16d));
		Assert.assertEquals(2, pg.getConfigurables().size());

		checkSize(0, 20d, 16d);
		checkSize(1, 20d, 4d);

		checkPosition(0, 0d, 0d, "Cathedral");
		checkPosition(1, 0d, 16d, "Rectangle");
		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(10d, 4d));
		Assert.assertEquals(3, pg.getConfigurables().size());

		checkSize(0, 10d, 16d);
		checkSize(1, 10d, 16d);
		checkSize(2, 20d, 4d);

		checkPosition(0, 0d, 0d, "ElongatedHalfArc");
		checkPosition(1, 10d, 0d, "ElongatedHalfArc");
		checkPosition(2, 0d, 16d, "Rectangle");
	}

	@Test
	/**
	 * Verify the resizing of a Cathedral
	 */
	public void resizeCathedralTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_C", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		initProperties();

		((ProductGroup) pg).setForm(ChoiceForm.F18_CATHEDRAL);
		resize(20d);

		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);

		splitCathedral();

		// -----------------------------------------------------------------------
		((ProductTemplate) pg.getConfigurables().get(2)).getDimension().setWidth(Units.inch(6d));

		checkSize(0, 10d, 16d, 1d);
		checkSize(1, 4d, 16d, 13.6095d);
		checkSize(2, 6d, 13.6095d, 1d);
		checkSize(3, 20d, 4d);

		checkPosition(0, 0d, 0d, "ElongatedHalfArc");
		checkPosition(1, 10d, 0d, "ElongatedHalfArc");
		checkPosition(2, 14d, 2.3905d, "ElongatedHalfArc");
		checkPosition(3, 0d, 16d, "Rectangle");

		// -----------------------------------------------------------------------
		pg.getDimension().setHeight2(Units.inch(8d));
		checkSize(0, 10d, 16d, 4d);
		checkSize(1, 4d, 16d, 14.5071d);
		checkSize(2, 6d, 14.5071d, 4d);
		checkSize(3, 20d, 4d);

		checkPosition(0, 0d, 0d, "ElongatedHalfArc");
		checkPosition(1, 10d, 0d, "ElongatedHalfArc");
		checkPosition(2, 14d, 1.49285d, "ElongatedHalfArc");
		checkPosition(3, 0d, 16d, "Rectangle");

		// -----------------------------------------------------------------------
		((ProductTemplate) pg.getConfigurables().get(3)).getDimension().setHeight(Units.inch(6d));
		checkSize(0, 10d, 14d, 2d);
		checkSize(1, 4d, 14d, 12.5071d);
		checkSize(2, 6d, 12.5071d, 2d);
		checkSize(3, 20d, 6d);

		checkPosition(0, 0d, 0d, "ElongatedHalfArc");
		checkPosition(1, 10d, 0d, "ElongatedHalfArc");
		checkPosition(2, 14d, 1.4928d, "ElongatedHalfArc");
		checkPosition(3, 0d, 14d, "Rectangle");

		// -----------------------------------------------------------------------
		((ProductTemplate) pg.getConfigurables().get(3)).getDimension().removeHeight();
		checkSize(0, 10d, 16d, 4d);
		checkSize(1, 4d, 16d, 14.5071d);
		checkSize(2, 6d, 14.5071d, 4d);
		checkSize(3, 20d, 4d);

		checkPosition(0, 0d, 0d, "ElongatedHalfArc");
		checkPosition(1, 10d, 0d, "ElongatedHalfArc");
		checkPosition(2, 14d, 1.4928d, "ElongatedHalfArc");
		checkPosition(3, 0d, 16d, "Rectangle");

		// -----------------------------------------------------------------------
		pg.getDimension().removeHeight2();
		checkSize(0, 10d, 16d, 1d);
		checkSize(1, 4d, 16d, 13.6095d);
		checkSize(2, 6d, 13.6095d, 1d);
		checkSize(3, 20d, 4d);

		checkPosition(0, 0d, 0d, "ElongatedHalfArc");
		checkPosition(1, 10d, 0d, "ElongatedHalfArc");
		checkPosition(2, 14d, 2.3905d, "ElongatedHalfArc");
		checkPosition(3, 0d, 16d, "Rectangle");

		// -----------------------------------------------------------------------
		((ProductTemplate) pg.getConfigurables().get(2)).getDimension().removeWidth();
		verifyInitial();
	}

	@Test
	/**
	 * Verify the resizing of a cathedral group
	 */
	public void resizeGroupCathedralTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_C", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		initProperties();

		((ProductGroup) pg).setForm(ChoiceForm.F18_CATHEDRAL);
		resize(20d);

		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);

		splitCathedral();

		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(18d));
		pg.getDimension().setHeight2(Units.inch(8d));
		checkSize(0, 9d, 16d, 4d);
		checkSize(1, 4.5d, 16d, 13.60468d);
		checkSize(2, 4.5d, 13.60468d, 4d);
		checkSize(3, 18d, 4d);

		checkPosition(0, 0d, 0d, "ElongatedHalfArc");
		checkPosition(1, 9d, 0d, "ElongatedHalfArc");
		checkPosition(2, 13.5d, 2.3953d, "ElongatedHalfArc");
		checkPosition(3, 0d, 16d, "Rectangle");

		pg.getDimension().removeHeight2();
		pg.getDimension().removeWidth();
		verifyInitial();

		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(22d));
		checkSize(0, 11d, 16d, 1d);
		checkSize(1, 5.5d, 16d, 12.9478d);
		checkSize(2, 5.5d, 12.9478d, 1d);
		checkSize(3, 22d, 4d);

		checkPosition(0, 0d, 0d, "ElongatedHalfArc");
		checkPosition(1, 11d, 0d, "ElongatedHalfArc");
		checkPosition(2, 16.5d, 3.05219d, "ElongatedHalfArc");
		checkPosition(3, 0d, 16d, "Rectangle");

		pg.getDimension().removeWidth();
		verifyInitial();

		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(18d));

		checkSize(0, 10.0, 14.4, 1.4, null);
		checkPosition(0, 0.0, 0.0, "ElongatedHalfArc");
		checkRay(0, 13.45);
		checkSize(1, 5.0, 14.4, 11.8642, null);
		checkPosition(1, 10.0, 0.0, "ElongatedHalfArc");
		checkRay(1, 13.45);
		checkSize(2, 5.0, 11.86422, 1.4, null);
		checkPosition(2, 15.0, 2.53577, "ElongatedHalfArc");
		checkRay(2, 13.45);
		checkSize(3, 20.0, 3.6, null, null);
		checkPosition(3, 0.0, 14.4, "Rectangle");

		pg.getDimension().removeHeight();
		verifyInitial();

		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(22d));
		pg.getDimension().setHeight2(Units.inch(6d));

		checkSize(0, 10d, 17.6d, 1.6d);
		checkSize(1, 5d, 17.6d, 13.9693d);
		checkSize(2, 5d, 13.9693d, 1.6d);
		checkSize(3, 20d, 4.4d);

		checkPosition(0, 0d, 0d, "ElongatedHalfArc");
		checkPosition(1, 10d, 0d, "ElongatedHalfArc");
		checkPosition(2, 15d, 3.63068d, "ElongatedHalfArc");
		checkPosition(3, 0d, 17.6d, "Rectangle");

		pg.getDimension().removeHeight2();
		pg.getDimension().removeHeight();
		verifyInitial();
	}

	/**
	 * Split the Cathedral in 4. Verify the result.
	 */
	private void splitCathedral()
	{
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 16d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(10d, 10d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(15d, 10d));
		Assert.assertEquals(4, pg.getConfigurables().size());

		verifyInitial();
	}

	private void verifyInitial()
	{
		checkSize(0, 10d, 16d, 1d);
		checkSize(1, 5d, 16d, 12.7260d);
		checkSize(2, 5d, 12.7260d, 1d);
		checkSize(3, 20d, 4d);

		checkPosition(0, 0d, 0d, "ElongatedHalfArc");
		checkPosition(1, 10d, 0d, "ElongatedHalfArc");
		checkPosition(2, 15d, 3.27398d, "ElongatedHalfArc");
		checkPosition(3, 0d, 16d, "Rectangle");
	}

	/**
	 * Resize the ProductGroup with the specified value.
	 * 
	 * @param size
	 */
	protected void resize(Double size)
	{
		pg.getDimension().setDefaultWidth(Units.inch(size));
		pg.getDimension().setDefaultHeight2(Units.inch(size / 4d));
		pg.getDimension().setDefaultHeight(Units.inch(size));
		pg.getDimension().setDefaultHeight2(Units.inch(size / 4d));
	}

}
