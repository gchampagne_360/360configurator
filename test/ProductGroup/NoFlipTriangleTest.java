package ProductGroup;

import java.awt.geom.Point2D;

import junit.framework.Assert;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.netappsid.commonutils.geo.enums.Orientation;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.common.Configurable;
import com.netappsid.erp.configurator.gui.Configurator;
import com.netappsid.wadconfigurator.Assembly;
import com.netappsid.wadconfigurator.ProductGroup;
import com.netappsid.wadconfigurator.ProductTemplate;
import com.netappsid.wadconfigurator.enums.ChoiceForm;

/**
 * PRODUCT GROUP TEST for Triangle
 * 
 * @author sboule
 */
public class NoFlipTriangleTest extends TestUtils
{
	@BeforeClass
	public static void init() throws Exception
	{}

	@AfterClass
	public static void tearDown()
	{}

	@Test
	/**
	 * Verify the split of a Triangle
	 */
	public void splitTriangleTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_IT", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		initProperties();

		((ProductGroup) pg).setForm(ChoiceForm.F13_TRIANGLE_ISOSCELES);

		pg.getDimension().setDefaultWidth(Units.inch(20d));
		pg.getDimension().setDefaultHeight(Units.inch(20d));

		checkSize(0, 20d, 20d, null, 10d);
		checkPosition(0, 0d, 0d, "Triangle");
		// Assert.assertEquals(TriangleFormType.ISOSCELES,
		// ((Triangle)((ProductTemplate)pg.getConfigurables().get(0)).getDimension().getForm()).getTriangleFormType());

		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(10d, 4d));
		Assert.assertEquals(2, pg.getConfigurables().size());

		checkSize(0, 10d, 20d, null, 10d);
		checkSize(1, 10d, 20d, null, 10d);

		checkPosition(0, 0d, 0d, "Triangle");
		checkPosition(1, 10d, 0d, "Triangle");

		// Assert.assertEquals(TriangleFormType.RECTANGLE,
		// ((Triangle)((ProductTemplate)pg.getConfigurables().get(0)).getDimension().getForm()).getTriangleFormType());
		// Assert.assertEquals(TriangleFormType.RECTANGLE,
		// ((Triangle)((ProductTemplate)pg.getConfigurables().get(1)).getDimension().getForm()).getTriangleFormType());

		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(15d, 4d));
		Assert.assertEquals(3, pg.getConfigurables().size());

		checkSize(0, 10d, 20d, null, 10d);
		checkSize(1, 5d, 20d, 10d);
		checkSize(2, 5d, 10d, null, 5d);

		checkPosition(0, 0d, 0d, "Triangle");
		checkPosition(1, 10d, 0d, "Trapeze");
		checkPosition(2, 15d, 10d, "Triangle");

		// Assert.assertEquals(TriangleFormType.RECTANGLE,
		// ((Triangle)((ProductTemplate)pg.getConfigurables().get(0)).getDimension().getForm()).getTriangleFormType());
		// Assert.assertEquals(TriangleFormType.RECTANGLE,
		// ((Triangle)((ProductTemplate)pg.getConfigurables().get(2)).getDimension().getForm()).getTriangleFormType());
	}

	@Test
	/**
	 * Verify the resizing of a Triangle
	 */
	public void resizeTriangleTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_IT", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		initProperties();

		((ProductGroup) pg).setForm(ChoiceForm.F13_TRIANGLE_ISOSCELES);
		pg.getDimension().setDefaultWidth(Units.inch(20d));
		pg.getDimension().setDefaultHeight(Units.inch(20d));

		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);

		splitTriangle();

		// -----------------------------------------------------------------------
		((ProductTemplate) pg.getConfigurables().get(2)).getDimension().setWidth(Units.inch(6d));

		checkSize(0, 10d, 20d, null, 10d);
		checkSize(1, 4d, 20d, 12d);
		checkSize(2, 6d, 12d, null, 6d);

		checkPosition(0, 0d, 0d, "Triangle");
		checkPosition(1, 10d, 0d, "Trapeze");
		checkPosition(2, 14d, 8d, "Triangle");

		// -----------------------------------------------------------------------
		((ProductTemplate) pg.getConfigurables().get(2)).getDimension().removeWidth();
		verifyInitial();
	}

	@Test
	/**
	 * Verify the resizing of a Triangle group
	 */
	public void resizeGroupTriangleTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_IT", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		initProperties();

		((ProductGroup) pg).setForm(ChoiceForm.F13_TRIANGLE_ISOSCELES);
		pg.getDimension().setDefaultWidth(Units.inch(20d));
		pg.getDimension().setDefaultHeight(Units.inch(20d));

		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(20d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);

		splitTriangle();

		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(18d));
		checkSize(0, 9d, 20d, null, 9d);
		checkSize(1, 4.5d, 20d, 10d);
		checkSize(2, 4.5d, 10d, null, 4.5d);

		checkPosition(0, 0d, 0d, "Triangle");
		checkPosition(1, 9d, 0d, "Trapeze");
		checkPosition(2, 13.5d, 10d, "Triangle");

		pg.getDimension().removeWidth();
		verifyInitial();

		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(22d));
		checkSize(0, 11d, 20d, null, 11d);
		checkSize(1, 5.5d, 20d, 10d);
		checkSize(2, 5.5d, 10d, null, 5.5d);

		checkPosition(0, 0d, 0d, "Triangle");
		checkPosition(1, 11d, 0d, "Trapeze");
		checkPosition(2, 16.5d, 10d, "Triangle");

		pg.getDimension().removeWidth();
		verifyInitial();

		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(18d));

		checkSize(0, 10d, 18d, null, 10d);
		checkSize(1, 5d, 18d, 9d);
		checkSize(2, 5d, 9d, null, 5d);

		checkPosition(0, 0d, 0d, "Triangle");
		checkPosition(1, 10d, 0d, "Trapeze");
		checkPosition(2, 15d, 9d, "Triangle");

		pg.getDimension().removeHeight();
		verifyInitial();

		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(22d));

		checkSize(0, 10d, 22d, null, 10d);
		checkSize(1, 5d, 22d, 11d);
		checkSize(2, 5d, 11d, null, 5d);

		checkPosition(0, 0d, 0d, "Triangle");
		checkPosition(1, 10d, 0d, "Trapeze");
		checkPosition(2, 15d, 11d, "Triangle");

		pg.getDimension().removeHeight();
		verifyInitial();
	}

	@Test
	/**
	 * Verify the resizing of a Triangle group
	 */
	public void resizeGroupTriangleTest2()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_IT", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		initProperties();

		((ProductGroup) pg).setForm(ChoiceForm.F13_TRIANGLE_ISOSCELES);
		pg.getDimension().setDefaultWidth(Units.inch(100d));
		pg.getDimension().setDefaultHeight(Units.inch(100d));

		Assert.assertEquals(100d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(100d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);

		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(50d, 80d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(75d, 80d));
		Assert.assertEquals(3, pg.getConfigurables().size());

		pg.getDimension().setDefaultWidth(Units.inch(20d));
		pg.getDimension().setDefaultHeight(Units.inch(20d));
		pg.getDimension().setHeight(Units.inch(20d));

		verifyInitial();
	}

	/**
	 * Split the Triangle in 3. Verify the result.
	 */
	private void splitTriangle()
	{
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(10d, 4d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(15d, 4d));
		Assert.assertEquals(3, pg.getConfigurables().size());

		verifyInitial();
	}

	private void verifyInitial()
	{
		checkSize(0, 10d, 20d, null, 10d);
		checkSize(1, 5d, 20d, 10d);
		checkSize(2, 5d, 10d, null, 5d);

		checkPosition(0, 0d, 0d, "Triangle");
		checkPosition(1, 10d, 0d, "Trapeze");
		checkPosition(2, 15d, 10d, "Triangle");

		// Assert.assertEquals(TriangleFormType.RECTANGLE,
		// ((Triangle)((ProductTemplate)pg.getConfigurables().get(0)).getDimension().getForm()).getTriangleFormType());
		// Assert.assertEquals(TriangleFormType.RECTANGLE,
		// ((Triangle)((ProductTemplate)pg.getConfigurables().get(2)).getDimension().getForm()).getTriangleFormType());
	}
}
