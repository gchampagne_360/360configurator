package ProductGroup;

import java.awt.geom.Point2D;

import junit.framework.Assert;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.netappsid.commonutils.geo.enums.Orientation;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.common.Configurable;
import com.netappsid.configuration.common.enums.ChoiceFlip;
import com.netappsid.erp.configurator.gui.Configurator;
import com.netappsid.wadconfigurator.Assembly;
import com.netappsid.wadconfigurator.ProductGroup;
import com.netappsid.wadconfigurator.ProductTemplate;
import com.netappsid.wadconfigurator.enums.ChoiceForm;

/**
 * PRODUCT GROUP TEST for template inside a template
 * 
 * @author sboule
 */
public class Template2Test extends TestUtils
{
	Assembly assembly;

	@BeforeClass
	public static void init() throws Exception
	{}

	@AfterClass
	public static void tearDown()
	{}

	@Test
	/**
	 * Verify the element resizing
	 */
	public void elementResizeTest()
	{
		assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		initProperties();

		((ProductGroup) pg).setForm(ChoiceForm.F02_RECTANGLE);
		pg.getDimension().setDefaultWidth(Units.inch(100d));
		pg.getDimension().setDefaultHeight(Units.inch(100d));

		Assert.assertEquals(100d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(100d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);

		split();

		// -----------------------------------------------------------------------
		for (int i = 0; i < pg.getConfigurables().size(); i++)
		{
			((ProductTemplate) pg.getConfigurables().get(i)).getDimension().setWidth(Units.inch(44d));
			verify();
			((ProductTemplate) pg.getConfigurables().get(i)).getDimension().removeWidth();
			verify();
		} /* end for */

		// -----------------------------------------------------------------------
		for (int i = 0; i < pg.getConfigurables().size(); i++)
		{
			((ProductTemplate) pg.getConfigurables().get(i)).getDimension().setWidth(Units.inch(22d));
			verify();
			((ProductTemplate) pg.getConfigurables().get(i)).getDimension().removeWidth();
			verify();
		} /* end for */

		// -----------------------------------------------------------------------
		for (int i = 0; i < pg.getConfigurables().size(); i++)
		{
			((ProductTemplate) pg.getConfigurables().get(i)).getDimension().setHeight(Units.inch(66d));
			verify();
			((ProductTemplate) pg.getConfigurables().get(i)).getDimension().removeHeight();
			verify();
		} /* end for */

		// -----------------------------------------------------------------------
		for (int i = 0; i < pg.getConfigurables().size(); i++)
		{
			((ProductTemplate) pg.getConfigurables().get(i)).getDimension().setHeight(Units.inch(33d));
			verify();
			((ProductTemplate) pg.getConfigurables().get(i)).getDimension().removeHeight();
			verify();
		} /* end for */
	}

	@Test
	/**
	 * Verify the group resizing
	 */
	public void groupResizeTest()
	{
		assembly = (Assembly) Configurator.getConfigurableByName("F_R", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		initProperties();

		((ProductGroup) pg).setForm(ChoiceForm.F02_RECTANGLE);
		pg.getDimension().setDefaultWidth(Units.inch(100d));
		pg.getDimension().setDefaultHeight(Units.inch(100d));

		Assert.assertEquals(100d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(100d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);

		split();

		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(88d));
		verify();
		pg.getDimension().removeWidth();
		verify();

		// -----------------------------------------------------------------------
		pg.getDimension().setWidth(Units.inch(122d));
		verify();
		pg.getDimension().removeWidth();
		verify();

		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(88d));
		verify();
		pg.getDimension().removeHeight();
		verify();

		// -----------------------------------------------------------------------
		pg.getDimension().setHeight(Units.inch(122d));
		verify();
		pg.getDimension().removeHeight();
		verify();
	}

	/**
	 * Split the rectangle in 6 and add template2 in each. Verify the result.
	 */
	private void split()
	{
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(50d, 50d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(33.33333d, 25d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(66.66667d, 25d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(33.33333d, 75d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(66.66667d, 75d));
		Assert.assertEquals(6, pg.getConfigurables().size());

		// Add a trapeze in 0
		ProductGroup pg2 = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, pg.getConfigurables().get(0));
		Assert.assertNotNull(pg2);
		pg2.setForm(ChoiceForm.F16_TRAPEZE);
		pg.bindConfigurableToTemplate(0, pg2);

		// Add a doghouse in 1
		ProductGroup pg3 = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, pg.getConfigurables().get(1));
		Assert.assertNotNull(pg3);
		pg3.setForm(ChoiceForm.F10_DOGHOUSE_SYMETRIC);
		pg.bindConfigurableToTemplate(1, pg3);

		// Add a flipped trapeze in 2
		ProductGroup pg4 = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, pg.getConfigurables().get(2));
		Assert.assertNotNull(pg4);
		pg4.setForm(ChoiceForm.F16_TRAPEZE);
		pg.bindConfigurableToTemplate(2, pg4);
		((ProductTemplate) pg4.getConfigurables().get(0)).getDimension().setChoiceFlip(ChoiceFlip.FLIPHORIZONTAL);

		// Add a elongated arc in 3
		ProductGroup pg5 = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, pg.getConfigurables().get(3));
		Assert.assertNotNull(pg5);
		pg5.setForm(ChoiceForm.F05_ELONGATEDARC);
		pg.bindConfigurableToTemplate(3, pg5);

		// Add a triangle in 4
		ProductGroup pg6 = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, pg.getConfigurables().get(4));
		Assert.assertNotNull(pg6);
		pg6.setForm(ChoiceForm.F13_TRIANGLE_ISOSCELES);
		pg.bindConfigurableToTemplate(4, pg6);

		// Add a cathedral in 5
		ProductGroup pg7 = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, pg.getConfigurables().get(5));
		Assert.assertNotNull(pg7);
		pg7.setForm(ChoiceForm.F18_CATHEDRAL);
		pg.bindConfigurableToTemplate(5, pg7);

		verify();
	}

	/**
	 * Make sure the size and position of template2 forms are the same as their template parent.
	 */
	private void verify()
	{
		int index = 0;
		for (com.netappsid.erp.configurator.Configurable configurable : pg.getConfigurables())
		{
			ProductTemplate template1 = (ProductTemplate) configurable;
			Assert.assertNotNull(template1);

			ProductTemplate template2 = template1.getTemplate2();
			Assert.assertNotNull(template2);

			Assert.assertEquals(template1.getDimension().getWidth().doubleValue(Units.INCH), template1.getDimension().getWidth().doubleValue(Units.INCH), DELTA);
			Assert.assertEquals(template1.getDimension().getHeight().doubleValue(Units.INCH), template1.getDimension().getHeight().doubleValue(Units.INCH),
					DELTA);

			Assert.assertEquals(template2.getDimension().getForm().getOrigin().getX(), pg.getConfigurablesPosition().get(index).getX(), DELTA);
			Assert.assertEquals(template2.getDimension().getForm().getOrigin().getY(), pg.getConfigurablesPosition().get(index).getY(), DELTA);

			++index;
		} /* end for */
	}
}
