package ProductGroup;

import java.awt.geom.Point2D;
import java.util.List;

import junit.framework.Assert;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.client360.configuration.wad.Dimension;
import com.netappsid.commonutils.geo.enums.Orientation;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.common.Configurable;
import com.netappsid.configuration.common.Dimensionnable;
import com.netappsid.erp.configurator.gui.Configurator;
import com.netappsid.wadconfigurator.Assembly;
import com.netappsid.wadconfigurator.ProductGroup;
import com.netappsid.wadconfigurator.ProductTemplate;
import com.netappsid.wadconfigurator.enums.ChoiceForm;

/**
 * PRODUCT GROUP TEST for trapezes
 * 
 * @author sboule
 */
public class NoFlipTrapezeTest extends TestUtils
{
	private static double HEIGHT2 = 4.5d;
	private static double HEIGHT1 = 6d;
	private static double MIN_HEIGHT2 = 5d;

	@BeforeClass
	public static void init() throws Exception
	{}

	@AfterClass
	public static void tearDown()
	{}

	@Test
	/**
	 * Verify the split of a Trapeze
	 */
	public void splitTrapezeTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);

		// Assert.assertEquals("Trapeze",((ProductTemplate)pg.getConfigurables().get(0)).getDimension().getForm().getClass().getSimpleName());
		Assert.assertEquals(10d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getWidth().doubleValue(Units.INCH), DELTA);
		Assert.assertEquals(10d, ((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight().doubleValue(Units.INCH), DELTA);
		Assert.assertNull(((ProductTemplate) pg.getConfigurables().get(0)).getDimension().getHeight2());

		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 8d));
		Assert.assertEquals(2, pg.getConfigurables().size());

		checkPosition(0, 0d, 0d, "Trapeze");
		checkSize(0, 10d, 8d);

		checkPosition(1, 0d, 8d, "Rectangle");
		checkSize(1, 10d, 2d);

		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(5d, 3d));
		Assert.assertEquals(3, pg.getConfigurables().size());

		checkPosition(1, 0d, 2.5d, "Trapeze");
		checkSize(1, 5d, 5.5d);

		checkPosition(0, 5d, 0d, "Trapeze");
		checkSize(0, 5d, 8d);

		checkPosition(2, 0d, 8d, "Rectangle");
		checkSize(2, 10d, 2d);

		// -----------------------------------------------------------------------
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(8d, 5d));
		Assert.assertEquals(4, pg.getConfigurables().size());

		checkPosition(1, 0d, 2.5d, "Trapeze");
		checkSize(1, 5d, 5.5d);

		checkPosition(0, 5d, 0d, "Trapeze");
		checkSize(0, 5d, 5d);

		checkPosition(2, 5d, 5d, "Rectangle");
		checkSize(2, 5d, 3d);

		checkPosition(3, 0d, 8d, "Rectangle");
		checkSize(3, 10d, 2d);
	}

	@Test
	/**
	 * Verify the getEastComponents method
	 */
	public void findEastNeighborsTest()
	{
		List<com.netappsid.erp.configurator.Configurable> neighborsList;

		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);
		splitTrapeze();

		// // -----------------------------------------------------------------------
		// neighborsList = pg.getEastComponents(pg.getConfigurables().get(2));
		// Assert.assertEquals(1,neighborsList.size());
		// Assert.assertEquals(pg.getConfigurables().get(1), neighborsList.get(0));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getEastComponents(pg.getConfigurables().get(1));
		// Assert.assertEquals(1,neighborsList.size());
		// Assert.assertEquals(pg.getConfigurables().get(0), neighborsList.get(0));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getEastComponents(pg.getConfigurables().get(0));
		// Assert.assertEquals(0,neighborsList.size());
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getEastComponents(pg.getConfigurables().get(3));
		// Assert.assertEquals(0,neighborsList.size());
	}

	@Test
	/**
	 * Verify the getWestComponents method
	 */
	public void findWestNeighborsTest()
	{
		List<com.netappsid.erp.configurator.Configurable> neighborsList;

		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);
		splitTrapeze();

		// // -----------------------------------------------------------------------
		// neighborsList = pg.getWestComponents(pg.getConfigurables().get(2));
		// Assert.assertEquals(0,neighborsList.size());
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getWestComponents(pg.getConfigurables().get(1));
		// Assert.assertEquals(1,neighborsList.size());
		// Assert.assertEquals(pg.getConfigurables().get(2), neighborsList.get(0));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getWestComponents(pg.getConfigurables().get(0));
		// Assert.assertEquals(1,neighborsList.size());
		// Assert.assertEquals(pg.getConfigurables().get(1), neighborsList.get(0));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getWestComponents(pg.getConfigurables().get(3));
		// Assert.assertEquals(0,neighborsList.size());
	}

	@Test
	/**
	 * Verify the getNorthComponents method
	 */
	public void findNorthNeighborsTest()
	{
		List<com.netappsid.erp.configurator.Configurable> neighborsList;

		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);
		splitTrapeze();

		// // -----------------------------------------------------------------------
		// neighborsList = pg.getNorthComponents(pg.getConfigurables().get(0));
		// Assert.assertEquals(0,neighborsList.size());
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getNorthComponents(pg.getConfigurables().get(1));
		// Assert.assertEquals(0,neighborsList.size());
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getNorthComponents(pg.getConfigurables().get(2));
		// Assert.assertEquals(0,neighborsList.size());
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getNorthComponents(pg.getConfigurables().get(3));
		// Assert.assertEquals(3,neighborsList.size());
		// Assert.assertEquals(pg.getConfigurables().get(0), neighborsList.get(0));
		// Assert.assertEquals(pg.getConfigurables().get(1), neighborsList.get(1));
		// Assert.assertEquals(pg.getConfigurables().get(2), neighborsList.get(2));
	}

	@Test
	/**
	 * Verify the getSouthComponents method
	 */
	public void findSouthNeighborsTest()
	{
		List<com.netappsid.erp.configurator.Configurable> neighborsList;

		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);
		splitTrapeze();

		// // -----------------------------------------------------------------------
		// neighborsList = pg.getSouthComponents(pg.getConfigurables().get(0));
		// Assert.assertEquals(1,neighborsList.size());
		// Assert.assertEquals(pg.getConfigurables().get(3), neighborsList.get(0));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getSouthComponents(pg.getConfigurables().get(1));
		// Assert.assertEquals(1,neighborsList.size());
		// Assert.assertEquals(pg.getConfigurables().get(3), neighborsList.get(0));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getSouthComponents(pg.getConfigurables().get(2));
		// Assert.assertEquals(1,neighborsList.size());
		// Assert.assertEquals(pg.getConfigurables().get(3), neighborsList.get(0));
		//
		// // -----------------------------------------------------------------------
		// neighborsList = pg.getSouthComponents(pg.getConfigurables().get(3));
		// Assert.assertEquals(0,neighborsList.size());
	}

	@Test
	/**
	 * Verify the West Stack
	 */
	public void findWestStackTest()
	{
		List<com.netappsid.erp.configurator.Configurable> stackList;

		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);
		splitTrapeze();

		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(2), Side.WEST, null);
		// Assert.assertEquals(1,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(3), stackList.get(0));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(1), Side.WEST, null);
		// Assert.assertEquals(0,stackList.size());
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(0), Side.WEST, null);
		// Assert.assertEquals(0,stackList.size());
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(3), Side.WEST, null);
		// Assert.assertEquals(1,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(2), stackList.get(0));
	}

	@Test
	/**
	 * Verify the East Stack
	 */
	public void findEastStackTest()
	{
		List<com.netappsid.erp.configurator.Configurable> stackList;

		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);
		splitTrapeze();

		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(2), Side.EAST, null);
		// Assert.assertEquals(0,stackList.size());
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(1), Side.EAST, null);
		// Assert.assertEquals(0,stackList.size());
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(0), Side.EAST, null);
		// Assert.assertEquals(1,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(3), stackList.get(0));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(3), Side.EAST, null);
		// Assert.assertEquals(1,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(0), stackList.get(0));
	}

	@Test
	/**
	 * Verify the North Stack
	 */
	public void findNorthStackTest()
	{
		List<com.netappsid.erp.configurator.Configurable> stackList;

		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);
		splitTrapeze();

		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(2), Side.NORTH, null);
		// Assert.assertEquals(0,stackList.size());
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(1), Side.NORTH, null);
		// Assert.assertEquals(0,stackList.size());
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(0), Side.NORTH, null);
		// Assert.assertEquals(0,stackList.size());
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(3), Side.NORTH, null);
		// Assert.assertEquals(0,stackList.size());
	}

	@Test
	/**
	 * Verify the South Stack
	 */
	public void findSouthStackTest()
	{
		List<com.netappsid.erp.configurator.Configurable> stackList;

		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);
		splitTrapeze();

		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(2), Side.SOUTH, null);
		// Assert.assertEquals(2,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(1), stackList.get(1));
		// Assert.assertEquals(pg.getConfigurables().get(0), stackList.get(0));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(1), Side.SOUTH, null);
		// Assert.assertEquals(2,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(2), stackList.get(1));
		// Assert.assertEquals(pg.getConfigurables().get(0), stackList.get(0));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(0), Side.SOUTH, null);
		// Assert.assertEquals(2,stackList.size());
		// Assert.assertEquals(pg.getConfigurables().get(2), stackList.get(1));
		// Assert.assertEquals(pg.getConfigurables().get(1), stackList.get(0));
		//
		// // -----------------------------------------------------------------------
		// stackList = pg.getComponentsStack(pg.getConfigurables().get(3), Side.SOUTH, null);
		// Assert.assertEquals(0,stackList.size());
	}

	@Test
	/**
	 * Verify the width increase
	 */
	public void widthIncreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);
		splitTrapeze();
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().setWidth(Units.inch(5d));
		checkSize(0, 5d, null);
		checkSize(1, 2d, null);
		checkSize(2, 3d, null);
		checkSize(3, 10d, null);
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().removeWidth();
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().setWidth(Units.inch(5d));
		checkSize(2, 2d, null);
		checkSize(1, 5d, null);
		checkSize(0, 3d, null);
		checkSize(3, 10d, null);
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().removeWidth();
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().setWidth(Units.inch(4d));
		checkSize(0, 4d, null);
		checkSize(1, 2d, null);
		checkSize(2, 4d, null);
		checkSize(3, 10d, null);
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().removeWidth();
		verifyInitial();
	}

	@Test
	/**
	 * Verify the height increase
	 */
	public void heightIncreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);
		splitTrapeze();
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setHeight(Units.inch(3d));
		checkSize(0, null, 7d);
		checkSize(1, null, HEIGHT1 - 1d);
		checkSize(2, null, HEIGHT2 - 1d);
		checkSize(3, null, 3d);
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removeHeight();
		verifyInitial();
	}

	@Test
	/**
	 * Verify the width decrease
	 */
	public void widthDecreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);
		splitTrapeze();
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().setWidth(Units.inch(3d));
		checkSize(0, 3d, null);
		checkSize(1, 4d, null);
		checkSize(2, 3d, null);
		checkSize(3, 10d, null);
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().removeWidth();
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().setWidth(Units.inch(1d));
		checkSize(0, 5d, null);
		checkSize(1, 1d, null);
		checkSize(2, 4d, null);
		checkSize(3, 10d, null);
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().removeWidth();
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().setWidth(Units.inch(2d));
		checkSize(0, 4d, null);
		checkSize(1, 4d, null);
		checkSize(2, 2d, null);
		checkSize(3, 10d, null);
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().removeWidth();
		verifyInitial();
	}

	@Test
	/**
	 * Verify the height decrease
	 */
	public void heightDecreaseNoOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);
		splitTrapeze();
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setHeight(Units.inch(1d));
		checkSize(0, null, 9d);
		checkSize(1, null, HEIGHT1 + 1d);
		checkSize(2, null, HEIGHT2 + 1d);
		checkSize(3, null, 1d);
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the width change with overrides
	 */
	public void widthChangeWithOverrideTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);
		splitTrapeze();
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().setWidth(Units.inch(5d)); // Override
		checkSize(0, 3d, null);
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().setWidth(Units.inch(4d));
		checkSize(0, 3d, null);
		checkSize(1, 5d, null);
		checkSize(2, 2d, null);
		checkSize(3, 10d, null);
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().setWidth(Units.inch(3d));
		checkSize(1, 4d, null);
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().setWidth(Units.inch(5d));
		checkSize(0, 3d, null);
		checkSize(1, 5d, null);
		checkSize(2, 2d, null);
		checkSize(3, 10d, null);
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().setWidth(Units.inch(5d));
		checkSize(2, 2d, null);
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().setWidth(Units.inch(3d));
		checkSize(0, 3d, null);
		checkSize(1, 5d, null);
		checkSize(2, 2d, null);
		checkSize(3, 10d, null);
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		verifyInitial();
	}

	@Test
	/**
	 * Verify the width decrease with overrides
	 */
	public void widthChangeMinMaxTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);
		splitTrapeze();
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().setWidth(Units.inch(11d));
		checkSize(0, 6d, null);
		checkSize(1, 1d, null);
		checkSize(2, 3d, null);
		checkSize(3, 10d, null);
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().setWidth(Units.inch(0.5d));
		checkSize(0, 1d, null);
		checkSize(1, 6d, null);
		checkSize(2, 3d, null);
		checkSize(3, 10d, null);
		((Dimensionnable) pg.getConfigurables().get(0)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().setWidth(Units.inch(11d));
		checkSize(0, 4d, null);
		checkSize(1, 1d, null);
		checkSize(2, 5d, null);
		checkSize(3, 10d, null);
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().setWidth(Units.inch(0.5d));
		checkSize(0, 4d, null);
		checkSize(1, 5d, null);
		checkSize(2, 1d, null);
		checkSize(3, 10d, null);
		((Dimensionnable) pg.getConfigurables().get(2)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setWidth(Units.inch(0.5d));
		verifyInitial();
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setWidth(Units.inch(11d));
		verifyInitial();
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().setWidth(Units.inch(0.5d));
		checkSize(0, 5d, null);
		checkSize(1, 1d, null);
		checkSize(2, 4d, null);
		checkSize(3, 10d, null);
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().setWidth(Units.inch(11d));
		checkSize(0, 1d, null);
		checkSize(1, 8d, null);
		checkSize(2, 1d, null);
		checkSize(3, 10d, null);
		((Dimensionnable) pg.getConfigurables().get(1)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_WIDTH);
		checkSize(0, 3.5d, null);
		checkSize(1, 3d, null);
		checkSize(2, 3.5d, null);
		checkSize(3, 10d, null);
	}

	@Test
	/**
	 * Verify the height decrease with override
	 */
	public void heightChangeMinMaxTest()
	{
		Assembly assembly = (Assembly) Configurator.getConfigurableByName("F_T", null);
		Assert.assertNotNull(assembly);

		pg = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, assembly);
		Assert.assertNotNull(pg);

		((ProductGroup) pg).setForm(ChoiceForm.F16_TRAPEZE);
		resize(10d);
		splitTrapeze();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setHeight(Units.inch(11d));

		printAll();
		checkSize(2, 3d, 2.5d, 1d);
		checkSize(1, 3d, 4d, 2.5d);
		checkSize(0, 4d, 6d, 4d);
		checkSize(3, 10d, 4d);
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT);
		verifyInitial();

		// -----------------------------------------------------------------------
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().setHeight(Units.inch(0.5d));
		checkSize(0, null, 9d);
		checkSize(1, null, HEIGHT1 + 1d);
		checkSize(2, null, HEIGHT2 + 1d);
		checkSize(3, null, 1d);
		((Dimensionnable) pg.getConfigurables().get(3)).getDimension().removePropertyOverride(Dimension.PROPERTYNAME_HEIGHT);
		verifyInitial();
	}

	/**
	 * Split a trapeze into 4 pieces:
	 * 
	 * | | | | | | | | 0 | | | 1 | | | 2 | | | |----------------------------| | 3 | |----------------------------|
	 */
	private void splitTrapeze()
	{
		pg.splitForm(Orientation.HORIZONTAL, new Point2D.Double(5d, 8d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(3d, 7d));
		pg.splitForm(Orientation.VERTICAL, new Point2D.Double(6d, 7d));
		Assert.assertEquals(4, pg.getConfigurables().size());

		initProperties();
	}

	/**
	 * Verify that all the components have the initial sizes.
	 */
	private void verifyInitial()
	{
		checkSize(2, 3d, HEIGHT2);
		checkSize(1, 3d, HEIGHT1);
		checkSize(0, 4d, 8d);
		checkSize(3, 10d, 2d);
	}

	@Override
	protected void checkSize(int confIndex, Double width, Double height)
	{
		super.checkSize(confIndex, width, height);

		if (width == null)
		{
			width = ((ProductTemplate) pg.getConfigurables().get(confIndex)).getDimension().getWidth().doubleValue(Units.INCH);
		} /* end if */

		if (height == null)
		{
			height = ((ProductTemplate) pg.getConfigurables().get(confIndex)).getDimension().getHeight().doubleValue(Units.INCH);
		} /* end if */

		// Check height2
		if (((ProductTemplate) pg.getConfigurables().get(confIndex)).getDimension().getHeight2() != null)
		{
			Assert.assertEquals(
					Math.abs(height - ((ProductTemplate) pg.getConfigurables().get(confIndex)).getDimension().getHeight2().doubleValue(Units.INCH)),
					width / 2d, DELTA);
		} /* end if */
	}
}
