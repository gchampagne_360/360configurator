package com.netappsid.test;

import java.util.HashMap;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.netappsid.commonutils.math.Units;
import com.netappsid.erp.configurator.configmanagement.ConfigurationManager;
import com.netappsid.erp.configurator.gui.Configurator;
import com.netappsid.test.common.Common;
import com.netappsid.wadconfigurator.Door;
import com.netappsid.wadconfigurator.SideLight;
import com.netappsid.wadconfigurator.enums.ChoiceBuildType;
import com.netappsid.wadconfigurator.enums.ChoiceOpening;
import com.netappsid.wadconfigurator.enums.ChoiceSideLightSide;
import com.netappsid.wadconfigurator.enums.ChoiceSwing;

public class FTestBaseEntranceDoor
{
	private static Map<Boolean, Map<String, String[]>> colorsPrice = new HashMap<Boolean, Map<String, String[]>>();

	@SuppressWarnings("unused")
	private Map<String, String[]> map;
	private String quickLibCode;

	@BeforeClass
	public static void init() throws Exception
	{}

	@AfterClass
	public static void tearDown()
	{
		colorsPrice = null;
	}

	// -------------- 2 DOOR 0 SIDELIGHT---------------

	/**
	 * Standard 2 fixed doors
	 */
	@Test
	public void EntranceDoorCase01()
	{
		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator;
			try
			{
				configurator = Common.instanciateConfigurable("STD");

				Map<String, String> properties = new HashMap<String, String>();
				Common.setDimensions(configurator, 80d, 80d);
				Common.setDoorNbSlab(configurator, 2);
				// Common.setDoorOpenings(configurator, new ChoiceOpening[] { ChoiceOpening.FIXED, ChoiceOpening.FIXED });
				properties.put("assembly.configurable[0].configurable[0].product.opening[0].choiceOpening", "FIXED");
				properties.put("assembly.configurable[0].configurable[0].product.opening[1].choiceOpening", "FIXED");
				Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurator, properties));

				Assert.assertEquals("FIXED", Common.getDoorOpening(configurator, 0));
				Assert.assertEquals("FIXED", Common.getDoorOpening(configurator, 1));

				Common.setDoorBuildType(configurator, ChoiceBuildType.FULLSTRUCTURAL);
				Common.setDoorSwingSide(configurator, choiceSwing);

				Assert.assertEquals(0.125, Common.getDoorSlabGap(configurator, 0), Common.DELTA);
				Assert.assertEquals(0.125, Common.getDoorSlabGap(configurator, 1), Common.DELTA);
				Assert.assertEquals(1.25, Common.getDoorRightJambWidthOS(configurator), Common.DELTA);
				Assert.assertEquals(1.25, Common.getDoorLeftJambWidthOS(configurator), Common.DELTA);
				Assert.assertEquals(1, Common.getDoorMullionsFix(configurator).length, Common.DELTA);
				Assert.assertEquals(80.0, Common.getDoorWidth(configurator), Common.DELTA);
				Assert.assertEquals(38.25, Common.getDoorSlabWidth(configurator), Common.DELTA);

				for (Door slab : Common.getDoorSlabs(configurator))
				{
					Assert.assertNotNull(slab);
					Assert.assertEquals(38.25, slab.getSlabWidth().doubleValue(Units.INCH), Common.DELTA);
					Assert.assertEquals(78.625, slab.getSlabHeight().doubleValue(Units.INCH), Common.DELTA);
				}

				configurator.dispose();

			}
			catch (Exception e)
			{
				e.printStackTrace();
				Assert.fail();
			}

		}
	}

	// -------------- 2 DOOR 1 SIDELIGHT---------------

	/**
	 * 2 fixed door with 1 15" left side light the slab width are calculated
	 */
	@Test
	public void EntranceDoorCase02()
	{
		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDimensions(configurator, 80d, 80d);
			Common.setDoorNbSlab(configurator, 2);
			Common.setDoorOpenings(configurator, new ChoiceOpening[] { ChoiceOpening.FIXED, ChoiceOpening.FIXED });
			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLSTRUCTURAL);
			Common.setDoorNbSideLight(configurator, 1);
			Common.setDoorSwingSide(configurator, choiceSwing);
			Common.setSideLightSide(configurator, ChoiceSideLightSide.LEFTSIDE);
			Common.setDoorSideLightWidth(configurator, 15d);

			Assert.assertEquals(30.375, Common.getDoorSlabWidth(configurator), Common.DELTA);
			Assert.assertEquals(80.0, Common.getDoorWidth(configurator), Common.DELTA);

			for (SideLight sl : Common.getDoorSideLights(configurator))
			{
				Assert.assertNotNull(sl);
				Assert.assertEquals(15.0, sl.getSlabWidth().doubleValue(Units.INCH), Common.DELTA);
			}

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(30.375, slab.getSlabWidth().doubleValue(Units.INCH), Common.DELTA);
			}

			configurator.dispose();

		}
	}

	/**
	 * same things as EntranceDoorCase2 but we change the input sequence.
	 */
	@Test
	public void EntranceDoorCase03()
	{
		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDimensions(configurator, 80d, 80d);
			Common.setDoorNbSlab(configurator, 2);
			Common.setDoorNbSideLight(configurator, 1);
			Common.setDoorOpenings(configurator, new ChoiceOpening[] { ChoiceOpening.FIXED, ChoiceOpening.FIXED });
			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLSTRUCTURAL);
			Common.setDoorSwingSide(configurator, choiceSwing);
			Common.setSideLightSide(configurator, ChoiceSideLightSide.LEFTSIDE);
			Common.setDoorSideLightWidth(configurator, 15d);

			Assert.assertEquals(30.375, Common.getDoorSlabWidth(configurator), Common.DELTA);
			Assert.assertEquals(80.0, Common.getDoorWidth(configurator), Common.DELTA);

			for (SideLight sl : Common.getDoorSideLights(configurator))
			{
				Assert.assertNotNull(sl);
				Assert.assertEquals(15.0, sl.getSlabWidth().doubleValue(Units.INCH), Common.DELTA);
			}

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(30.375, slab.getSlabWidth().doubleValue(Units.INCH), Common.DELTA);
			}

			configurator.dispose();

		}
	}

	/**
	 * 2 32" fixed door with 1 left side light the slab width are calculated
	 * 
	 * We try to put the 23" value before set the number of side light... the door replace the logical value. We set the number of sidelight and set the door
	 * width it should stay at 32"
	 * 
	 */
	@Test
	public void EntranceDoorCase04()
	{
		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDoorNbSlab(configurator, 2);
			Common.setDoorNbSideLight(configurator, 1);
			Common.setDimensions(configurator, 80d, 80d);
			Common.setDoorOpenings(configurator, new ChoiceOpening[] { ChoiceOpening.FIXED, ChoiceOpening.FIXED });
			Common.setDoorSlabWidth(configurator, 32.0);
			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLSTRUCTURAL);
			Common.setDoorSwingSide(configurator, choiceSwing);
			Common.setSideLightSide(configurator, ChoiceSideLightSide.LEFTSIDE);

			Assert.assertEquals(32.0, Common.getDoorSlabWidth(configurator), Common.DELTA);
			Assert.assertEquals(80.0, Common.getDoorWidth(configurator), Common.DELTA);

			for (SideLight sl : Common.getDoorSideLights(configurator))
			{
				Assert.assertNotNull(sl);
				Assert.assertEquals(11.75, sl.getSlabWidth().doubleValue(Units.INCH), Common.DELTA);
			}

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(32.0, slab.getSlabWidth().doubleValue(Units.INCH), Common.DELTA);
			}

			configurator.dispose();

		}
	}

	// -------------- 2 DOOR 2 SIDELIGHT---------------

	@Test
	public void EntranceDoorCase05()
	{
		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLSTRUCTURAL);
			Common.setDoorNbSlab(configurator, 2);
			Common.setDimensions(configurator, 80d, 80d);
			Common.setDoorNbSideLight(configurator, 2);

			Common.setDoorOpenings(configurator, new ChoiceOpening[] { ChoiceOpening.FIXED, ChoiceOpening.FIXED });
			Common.setDoorSwingSide(configurator, choiceSwing);
			Common.setDoorSideLightWidth(configurator, 15d);

			Assert.assertEquals(22.5, Common.getDoorSlabWidth(configurator), Common.DELTA);
			Assert.assertEquals(80.0, Common.getDoorWidth(configurator), Common.DELTA);

			for (SideLight sl : Common.getDoorSideLights(configurator))
			{
				Assert.assertNotNull(sl);
				Assert.assertEquals(15, sl.getSlabWidth().doubleValue(Units.INCH), Common.DELTA);
			}

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(22.5, slab.getSlabWidth().doubleValue(Units.INCH), Common.DELTA);
			}

			configurator.dispose();

		}
	}

	// -------------- 1 DOOR---------------

	/**
	 * Simple door with overall width at 34.75
	 */
	@Test
	public void EntranceDoorCase06()
	{

		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDoorNbSlab(configurator, 1);
			Common.setDimensions(configurator, 34.75d, 80d);
			Common.setDoorOpening(configurator, ChoiceOpening.FIXED, 0);
			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLSTRUCTURAL);
			Common.setDoorSwingSide(configurator, choiceSwing);

			Assert.assertEquals(32.0, Common.getDoorSlabWidth(configurator), Common.DELTA);
			Assert.assertEquals(34.75, Common.getDoorWidth(configurator), Common.DELTA);

			Assert.assertNull(Common.getDoorSideLights(configurator));

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(32.0, slab.getSlabWidth().doubleValue(Units.INCH), Common.DELTA);
			}

			configurator.dispose();

		}
	}

	/**
	 * Simple door with overall width at 34.75 left and right opening
	 */
	@Test
	public void EntranceDoorCase07()
	{

		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{

			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDoorNbSlab(configurator, 1);
			Common.setDimensions(configurator, 34.75d, 80d);
			Common.setDoorSwingSide(configurator, choiceSwing);
			Common.setDoorOpening(configurator, ChoiceOpening.LEFT, 0);
			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLSTRUCTURAL);

			// -------LEFT OPENING TEST -------------
			Assert.assertEquals(Common.getDoorSlabWidth(configurator), 31.75, Common.DELTA);
			Assert.assertEquals(Common.getDoorWidth(configurator), 34.75, Common.DELTA);

			Assert.assertNull(Common.getDoorSideLights(configurator));

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 31.75, Common.DELTA);
			}

			// -------RIGHT OPENING TEST -------------
			Common.setDoorOpening(configurator, ChoiceOpening.RIGHT, 0);

			Assert.assertEquals(Common.getDoorSlabWidth(configurator), 31.75, Common.DELTA);
			Assert.assertEquals(Common.getDoorWidth(configurator), 34.75, Common.DELTA);

			Assert.assertNull(Common.getDoorSideLights(configurator));

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 31.75, Common.DELTA);
			}

			configurator.dispose();

		}
	}

	/**
	 * Standard 2 operating doors, withOUT astragal
	 */
	@Test
	public void EntranceDoorCase08()
	{
		Configurator configurator = Common.instanciateConfigurable("STD");

		Common.setDoorBuildType(configurator, ChoiceBuildType.FULLSTRUCTURAL);
		Common.setDimensions(configurator, 80d, 80d);
		Common.setDoorNbSlab(configurator, 2);
		Common.setDoorSwingSide(configurator, ChoiceSwing.INSWING);
		Common.setDoorOpenings(configurator, new ChoiceOpening[] { ChoiceOpening.LEFT, ChoiceOpening.RIGHT });

		Assert.assertEquals(38.0, Common.getDoorSlabWidth(configurator), Common.DELTA);
		Assert.assertEquals(80.0, Common.getDoorWidth(configurator), Common.DELTA);

		for (Door slab : Common.getDoorSlabs(configurator))
		{
			Assert.assertNotNull(slab);
			Assert.assertEquals(38.0, slab.getSlabWidth().doubleValue(Units.INCH), Common.DELTA);
			Assert.assertEquals(78.625, slab.getSlabHeight().doubleValue(Units.INCH), Common.DELTA);
		}

		configurator.dispose();
	}

	/**
	 * Standard 2 operating doors, with astragal
	 */
	@Test
	public void EntranceDoorCase09()
	{

		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			try
			{
				Configurator configurator = Common.instanciateConfigurable("STD");

				Common.setDimensions(configurator, 80d, 80d);
				Common.setDoorNbSlab(configurator, 2);

				// LEFT SLAB ASTRAGAL
				Common.setDoorOpenings(configurator, new ChoiceOpening[] { ChoiceOpening.LEFTA, ChoiceOpening.RIGHT });

				Common.setDoorBuildType(configurator, ChoiceBuildType.FULLSTRUCTURAL);
				Common.setDoorSwingSide(configurator, choiceSwing);

				Assert.assertEquals(38.25, Common.getDoorSlabWidth(configurator), Common.DELTA);
				Assert.assertEquals(80.0, Common.getDoorWidth(configurator), Common.DELTA);

				for (Door slab : Common.getDoorSlabs(configurator))
				{
					Assert.assertNotNull(slab);
					Assert.assertEquals(38.25, slab.getSlabWidth().doubleValue(Units.INCH), Common.DELTA);
					Assert.assertEquals(78.625, slab.getSlabHeight().doubleValue(Units.INCH), Common.DELTA);
				}

				// RIGHT SLAB ASTRAGAL
				Common.setDoorOpenings(configurator, new ChoiceOpening[] { ChoiceOpening.LEFT, ChoiceOpening.RIGHTA });

				Assert.assertEquals(38.25, Common.getDoorSlabWidth(configurator), Common.DELTA);
				Assert.assertEquals(80.0, Common.getDoorWidth(configurator), Common.DELTA);

				for (Door slab : Common.getDoorSlabs(configurator))
				{
					Assert.assertNotNull(slab);
					Assert.assertEquals(38.25, slab.getSlabWidth().doubleValue(Units.INCH), Common.DELTA);
					Assert.assertEquals(78.625, slab.getSlabHeight().doubleValue(Units.INCH), Common.DELTA);
				}

				configurator.dispose();
			}
			catch (Exception e)
			{
				Assert.fail();
			}

		}
	}

	/**
	 * Standard 2 operating doors, withOUT astragal. Slab width override should change the total width because there is no sidelight
	 */
	@Test
	public void EntranceDoorCase10()
	{
		Configurator configurator = Common.instanciateConfigurable("STD");

		Common.setDoorBuildType(configurator, ChoiceBuildType.FULLSTRUCTURAL);
		Common.setDoorSwingSide(configurator, ChoiceSwing.INSWING);
		Common.setDimensions(configurator, 80d, 80d);
		Common.setDoorNbSlab(configurator, 2);
		Common.setDoorOpenings(configurator, new ChoiceOpening[] { ChoiceOpening.LEFT, ChoiceOpening.RIGHT });
		Common.setDoorSlabWidth(configurator, 32d);

		Assert.assertEquals(32.0, Common.getDoorSlabWidth(configurator), Common.DELTA);
		Assert.assertEquals(80d, Common.getDoorWidth(configurator), Common.DELTA);

		for (Door slab : Common.getDoorSlabs(configurator))
		{
			Assert.assertNotNull(slab);
			Assert.assertEquals(32.0, slab.getSlabWidth().doubleValue(Units.INCH), Common.DELTA);
			Assert.assertEquals(78.625, slab.getSlabHeight().doubleValue(Units.INCH), Common.DELTA);
		}

		configurator.dispose();
	}

	/**
	 * Standard 2 right operating doors should have a mullion
	 */
	@Test
	public void EntranceDoorCase11()
	{
		Configurator configurator = Common.instanciateConfigurable("STD");

		Common.setDoorBuildType(configurator, ChoiceBuildType.FULLSTRUCTURAL);
		Common.setDoorSwingSide(configurator, ChoiceSwing.INSWING);
		Common.setDimensions(configurator, 80d, 80d);
		Common.setDoorNbSlab(configurator, 2);
		Common.setDoorOpenings(configurator, new ChoiceOpening[] { ChoiceOpening.RIGHT, ChoiceOpening.RIGHT });

		Assert.assertNotNull(Common.getDoorMullionsFix(configurator));
		Assert.assertEquals(1, Common.getDoorMullionsFix(configurator).length);

		Assert.assertEquals(80d, Common.getDoorWidth(configurator), Common.DELTA);

		for (Door slab : Common.getDoorSlabs(configurator))
		{
			Assert.assertNotNull(slab);
			Assert.assertEquals(38.0, slab.getSlabWidth().doubleValue(Units.INCH), Common.DELTA);
			Assert.assertEquals(78.625, slab.getSlabHeight().doubleValue(Units.INCH), Common.DELTA);
		}

		configurator.dispose();
	}

	// -------------------- 1 door 2 sidelights
	@Test
	public void EntranceDoorCase12()
	{

		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDoorNbSlab(configurator, 1);
			Common.setDoorNbSideLight(configurator, 2);
			Common.setDimensions(configurator, 80d, 80d);
			Common.setDoorOpening(configurator, ChoiceOpening.LEFT, 0);
			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLSTRUCTURAL);
			Common.setDoorSwingSide(configurator, choiceSwing);
			Common.setDoorSideLightWidth(configurator, 15d);

			Assert.assertEquals(Common.getDoorSlabWidth(configurator), 45.5d, Common.DELTA);
			Assert.assertEquals(Common.getDoorWidth(configurator), 80d, Common.DELTA);

			Common.setDoorNbSideLight(configurator, 0);

			Assert.assertNull(Common.getDoorSideLights(configurator));

			Assert.assertEquals(Common.getDoorSlabWidth(configurator), 77d, Common.DELTA);
			Assert.assertEquals(Common.getDoorWidth(configurator), 80d, Common.DELTA);

			Assert.assertNull(Common.getDoorSideLights(configurator));

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 77d, Common.DELTA);
			}

			configurator.dispose();

		}
	}

	// -------------- 4 DOOR 2 SIDELIGHT---------------

	/**
	 * 4 fixed door with 2 15" left side light the slab width are calculated
	 */
	@Test
	public void EntranceDoorCase13()
	{
		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDimensions(configurator, 158d, 80d);
			Common.setDoorNbSlab(configurator, 4);
			Common.setDoorOpenings(configurator, new ChoiceOpening[] { ChoiceOpening.FIXED, ChoiceOpening.FIXED, ChoiceOpening.FIXED, ChoiceOpening.FIXED });
			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLSTRUCTURAL);
			Common.setDoorNbSideLight(configurator, 2);
			Common.setDoorSwingSide(configurator, choiceSwing);
			Common.setDoorSideLightWidth(configurator, 15d);

			Assert.assertEquals(Common.getDoorSlabWidth(configurator), 30.375, Common.DELTA);
			Assert.assertEquals(Common.getDoorWidth(configurator), 158d, Common.DELTA);

			for (SideLight sl : Common.getDoorSideLights(configurator))
			{
				Assert.assertNotNull(sl);
				Assert.assertEquals(sl.getSlabWidth().doubleValue(Units.INCH), 15.0, Common.DELTA);
			}

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 30.375, Common.DELTA);
			}

			configurator.dispose();

		}
	}

	// -------------- 4 DOOR 2 SIDELIGHT Modular---------------

	/**
	 * 4 fixed door with 2 15" side light the slab width are calculated. Frame Modular
	 */
	@Test
	public void EntranceDoorCase14()
	{
		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDimensions(configurator, 159.5d, 80d);
			Common.setDoorNbSlab(configurator, 4);
			Common.setDoorOpenings(configurator, new ChoiceOpening[] { ChoiceOpening.FIXED, ChoiceOpening.FIXED, ChoiceOpening.FIXED, ChoiceOpening.FIXED });
			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLSTRUCTURAL);
			Common.setDoorNbSideLight(configurator, 2);
			Common.setDoorSwingSide(configurator, choiceSwing);
			Common.setDoorSideLightWidth(configurator, 15d);

			Assert.assertEquals(Common.getDoorSlabWidth(configurator), 30.75, Common.DELTA);
			Assert.assertEquals(Common.getDoorWidth(configurator), 159.5d, Common.DELTA);

			for (SideLight sl : Common.getDoorSideLights(configurator))
			{
				Assert.assertNotNull(sl);
				Assert.assertEquals(sl.getSlabWidth().doubleValue(Units.INCH), 15.0, Common.DELTA);
			}

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 30.75, Common.DELTA);
			}

			configurator.dispose();

		}
	}

	/**
	 * 4 opening door with 2 15" side light the slab width are calculated. Frame Modular
	 */
	@Test
	public void EntranceDoorCase15()
	{
		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDimensions(configurator, 159.5d, 80d);
			Common.setDoorNbSlab(configurator, 4);
			Common.setDoorOpenings(configurator, new ChoiceOpening[] { ChoiceOpening.LEFT, ChoiceOpening.LEFT, ChoiceOpening.LEFT, ChoiceOpening.RIGHT });
			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLSTRUCTURAL);
			Common.setDoorNbSideLight(configurator, 2);
			Common.setDoorSwingSide(configurator, choiceSwing);
			Common.setDoorSideLightWidth(configurator, 15d);

			Assert.assertEquals(Common.getDoorSlabWidth(configurator), 30.5d, Common.DELTA);
			Assert.assertEquals(Common.getDoorWidth(configurator), 159.5d, Common.DELTA);

			for (SideLight sl : Common.getDoorSideLights(configurator))
			{
				Assert.assertNotNull(sl);
				Assert.assertEquals(sl.getSlabWidth().doubleValue(Units.INCH), 15.0, Common.DELTA);
			}

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 30.5d, Common.DELTA);
			}

			configurator.dispose();

		}
	}

	/**
	 * 4 opening door with 2 15" side light the slab width are calculated. Frame Modular
	 */
	@Test
	public void EntranceDoorCase16()
	{
		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDimensions(configurator, 159.5d, 80d);
			Common.setDoorNbSlab(configurator, 4);
			Common.setDoorOpenings(configurator, new ChoiceOpening[] { ChoiceOpening.LEFT, ChoiceOpening.LEFTA, ChoiceOpening.RIGHT, ChoiceOpening.LEFT });

			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLSTRUCTURAL);
			Common.setDoorNbSideLight(configurator, 2);
			Common.setDoorSwingSide(configurator, choiceSwing);
			Common.setDoorSideLightWidth(configurator, 15d);

			Assert.assertEquals(Common.getDoorSlabWidth(configurator), 30.625, Common.DELTA);
			Assert.assertEquals(Common.getDoorWidth(configurator), 159.5d, Common.DELTA);

			for (SideLight sl : Common.getDoorSideLights(configurator))
			{
				Assert.assertNotNull(sl);
				Assert.assertEquals(sl.getSlabWidth().doubleValue(Units.INCH), 15.0, Common.DELTA);
			}

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 30.625, Common.DELTA);
			}

			configurator.dispose();

		}
	}

	@Test
	public void EntranceDoorCase17()
	{
		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDimensions(configurator, 159.5d, 80d);
			Common.setDoorNbSlab(configurator, 4);
			Common.setDoorOpenings(configurator, new ChoiceOpening[] { ChoiceOpening.LEFT, ChoiceOpening.LEFTA, ChoiceOpening.RIGHT, ChoiceOpening.RIGHT });

			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLSTRUCTURAL);
			Common.setDoorNbSideLight(configurator, 1);
			Common.setDoorSwingSide(configurator, choiceSwing);
			Common.setSideLightSide(configurator, ChoiceSideLightSide.LEFTSIDE);
			Common.setDoorSideLightWidth(configurator, 15d);

			Assert.assertEquals(Common.getDoorSlabWidth(configurator), 34.5625, Common.DELTA);
			Assert.assertEquals(Common.getDoorWidth(configurator), 159.5d, Common.DELTA);

			for (SideLight sl : Common.getDoorSideLights(configurator))
			{
				Assert.assertNotNull(sl);
				Assert.assertEquals(sl.getSlabWidth().doubleValue(Units.INCH), 15.0, Common.DELTA);
			}

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 34.5625, Common.DELTA);
			}

			configurator.dispose();

		}
	}

	@Test
	public void EntranceDoorCase18()
	{
		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDimensions(configurator, 159.5d, 80d);
			Common.setDoorNbSlab(configurator, 4);
			Common.setDoorOpenings(configurator, new ChoiceOpening[] { ChoiceOpening.LEFTA, ChoiceOpening.RIGHT, ChoiceOpening.FIXED, ChoiceOpening.FIXED });

			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLSTRUCTURAL);
			Common.setDoorNbSideLight(configurator, 1);
			Common.setDoorSwingSide(configurator, choiceSwing);
			Common.setSideLightSide(configurator, ChoiceSideLightSide.RIGHTSIDE);
			Common.setDoorSideLightWidth(configurator, 15d);

			Assert.assertEquals(Common.getDoorSlabWidth(configurator), 34.6875, Common.DELTA);
			Assert.assertEquals(Common.getDoorWidth(configurator), 159.5d, Common.DELTA);

			for (SideLight sl : Common.getDoorSideLights(configurator))
			{
				Assert.assertNotNull(sl);
				Assert.assertEquals(sl.getSlabWidth().doubleValue(Units.INCH), 15.0, Common.DELTA);
			}

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 34.6875, Common.DELTA);
			}

			configurator.dispose();

		}
	}

	// -------------- 1 slab with form ---------------

	/**
	 * 1 opening door with 1 15" side light the slab width are calculated. Frame Modular
	 */
	// @Test
	// public void EntranceDoorCase19()
	// {
	// for (ChoiceSwing choiceSwing : ChoiceSwing.values())
	// {
	// Configurator configurator = Common.instanciateConfigurable("STD");
	//
	// Common.setDoorForm(configurator, Form.create("ARCHFORM_N_ELONGATEDARC"));
	// Common.setDimensions(configurator, 40d, 80d, 70d);
	// Common.setDoorNbSlab(configurator, 1);
	// Common.setDoorOpening(configurator, ChoiceOpening.LEFT, 0);
	// Common.setDoorBuildType(configurator, ChoiceBuildType.FULLSTRUCTURAL);
	// Common.setDoorNbSideLight(configurator, 0);
	// Common.setDoorSwingSide(configurator, choiceSwing);
	//
	// Assert.assertEquals(Common.getDoorSlabWidth(configurator), 37d, Common.DELTA);
	// Assert.assertEquals(Common.getDoorWidth(configurator), 40d, Common.DELTA);
	//
	// // Assert.assertEquals(Common.getDoorForm(configurator).getClass(), ElongatedArc.class);
	// // Assert.assertEquals(Common.getDoorSlabForm(configurator).getClass(), ElongatedArc.class);
	// // Assert.assertEquals(Common.getFrameForm(configurator).getClass(), ElongatedArc.class);
	//
	// Assert.assertEquals(Common.getDoorSlabs(configurator)[0].getDimension().getWidth().doubleValue(Units.INCH), 37d, Common.DELTA);
	// Assert.assertEquals(Common.getDoorSlabs(configurator)[0].getDimension().getHeight().doubleValue(Units.INCH), 78.625, Common.DELTA);
	// Assert.assertEquals(Common.getDoorSlabs(configurator)[0].getDimension().getHeight2().doubleValue(Units.INCH), 69.9843, Common.DELTA);
	//
	// configurator.dispose();
	//
	// }
	// }

	// - 2 slab; second slab width = first slab width
	@Test
	public void EntranceDoorCase20()
	{
		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator;
			try
			{
				configurator = Common.instanciateConfigurable("STD");

				Common.setDoorBuildType(configurator, ChoiceBuildType.FULLSTRUCTURAL);
				Common.setDoorSwingSide(configurator, choiceSwing);

				Common.setDoorNbSlab(configurator, 2);
				Common.setDoorOpenings(configurator, new ChoiceOpening[] { ChoiceOpening.FIXED, ChoiceOpening.FIXED });

				Common.setDoorHeight(configurator, 80d);

				Common.setDoorSlabWidth(configurator, 30d); // Set all slabs width

				Common.setSlabDoorHeight(configurator, 78.625);

				for (Door slab : Common.getDoorSlabs(configurator))
				{
					Assert.assertNotNull(slab);

					Assert.assertEquals(slab.getSlabHeight().doubleValue(Units.INCH), 78.625, Common.DELTA);
					Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 30.0, Common.DELTA);
				}

				Assert.assertEquals(Common.getDoorWidth(configurator), 63.5, Common.DELTA);

				configurator.dispose();

			}
			catch (Exception e)
			{
				Assert.fail();
			}
		}
	}

	@Test
	public void EntranceDoorCase21()
	{
		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDoorNbSlab(configurator, 4);
			Common.setDoorOpenings(configurator, new ChoiceOpening[] { ChoiceOpening.LEFTA, ChoiceOpening.RIGHT, ChoiceOpening.FIXED, ChoiceOpening.FIXED });

			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLSTRUCTURAL);
			Common.setDoorSwingSide(configurator, choiceSwing);
			Common.setSideliteSwingSide(configurator, choiceSwing);
			Common.setDoorHeight(configurator, 80d);
			Common.setDoorSlabWidth(configurator, 34d);

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 34d, Common.DELTA);
			}

			Assert.assertEquals(Common.getDoorWidth(configurator), 141d, Common.DELTA);

			Common.setDoorNbSideLight(configurator, 1);

			Common.setSideLightSide(configurator, ChoiceSideLightSide.RIGHTSIDE);
			Common.setDoorSideLightWidth(configurator, 15d);

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 34d, Common.DELTA);
			}
			for (SideLight sl : Common.getDoorSideLights(configurator))
			{
				Assert.assertNotNull(sl);
				Assert.assertEquals(sl.getSlabWidth().doubleValue(Units.INCH), 15d, Common.DELTA);
			}
			Assert.assertEquals(Common.getDoorWidth(configurator), 156.75, Common.DELTA);

			configurator.dispose();

		}
	}

	@Test
	public void EntranceDoorCase22()
	{
		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDoorNbSlab(configurator, 3);
			Common.setDoorOpenings(configurator, new ChoiceOpening[] { ChoiceOpening.LEFTA, ChoiceOpening.RIGHT, ChoiceOpening.FIXED });

			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLSTRUCTURAL);
			Common.setDoorSwingSide(configurator, choiceSwing);
			Common.setSideliteSwingSide(configurator, choiceSwing);
			Common.setDoorHeight(configurator, 80d);
			Common.setDoorSlabWidth(configurator, 34d);

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 34d, Common.DELTA);
			}

			Assert.assertEquals(Common.getDoorWidth(configurator), 106.25, Common.DELTA);

			Common.setDoorSlabWidth(configurator, 30d, 0);
			Assert.assertEquals(Common.getDoorSlabs(configurator)[2].getSlabWidth().doubleValue(Units.INCH), 34d, Common.DELTA);

			Common.setDoorNbSideLight(configurator, 1);

			Common.setSideLightSide(configurator, ChoiceSideLightSide.RIGHTSIDE);
			Common.setDoorSideLightWidth(configurator, 15d);

			Assert.assertEquals(Common.getDoorSlabs(configurator)[0].getSlabWidth().doubleValue(Units.INCH), 30d, Common.DELTA);
			Assert.assertEquals(Common.getDoorSlabs(configurator)[1].getSlabWidth().doubleValue(Units.INCH), 34d, Common.DELTA);
			Assert.assertEquals(Common.getDoorSlabs(configurator)[2].getSlabWidth().doubleValue(Units.INCH), 34d, Common.DELTA);

			Assert.assertNotNull(Common.getDoorSideLights(configurator)[0]);
			Assert.assertEquals(Common.getDoorSideLights(configurator)[0].getSlabWidth().doubleValue(Units.INCH), 15d, Common.DELTA);

			Assert.assertEquals(Common.getDoorWidth(configurator), 118d, Common.DELTA);

			configurator.dispose();

		}
	}

	// test isWidthCalculateFromSL == false
	@Test
	public void EntranceDoorCase23()
	{
		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDoorNbSlab(configurator, 2);
			Common.setDoorNbSideLight(configurator, 1);
			Common.setDoorOpenings(configurator, new ChoiceOpening[] { ChoiceOpening.FIXED, ChoiceOpening.FIXED });
			Common.setDoorSideLightWidth(configurator, 32.0);
			Common.setDoorSlabWidth(configurator, 32.0);
			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLSTRUCTURAL);
			Common.setDoorSwingSide(configurator, choiceSwing);
			Common.setSideliteSwingSide(configurator, choiceSwing);
			Common.setSideLightSide(configurator, ChoiceSideLightSide.LEFTSIDE);

			Assert.assertEquals(100.25d, Common.getDoorWidth(configurator), Common.DELTA);

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
			}

			configurator.dispose();

		}
	}

	// test isWidthCalculateFromSL == true
	@Test
	public void EntranceDoorCase24()
	{
		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setWidth(configurator, 133d);

			Common.setDoorNbSlab(configurator, 2);
			Common.setWidthCalculateFromSL(configurator, true);
			Common.setDoorNbSideLight(configurator, 2);
			Common.setDoorOpenings(configurator, new ChoiceOpening[] { ChoiceOpening.FIXED, ChoiceOpening.FIXED });
			Common.setDoorSideLightWidth(configurator, 32.0, 0);
			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLSTRUCTURAL);
			Common.setDoorSwingSide(configurator, choiceSwing);

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 32d, Common.DELTA);
			}
			for (SideLight sl : Common.getDoorSideLights(configurator))
			{
				Assert.assertNotNull(sl);
				Assert.assertEquals(sl.getSlabWidth().doubleValue(Units.INCH), 32d, Common.DELTA);
			}
			Assert.assertEquals(133d, Common.getDoorWidth(configurator), Common.DELTA);

			configurator.dispose();

		}
	}

	// --------------- ZPOST -----------------
	// -------------- 1 DOOR - 1 SIDELITE / RIGHT SIDELITE / door INSWING - sidelite OUTSWING / door LEFT operating - side LEFT operatiing / DOOR
	// 34" - SIDELITE 15" ---------------

	@Test
	public void EntranceDoorCase25()
	{
		Configurator configurator = Common.instanciateConfigurable("STD");

		Common.setDoorNbSlab(configurator, 1);
		Common.setDoorNbSideLight(configurator, 1);
		Common.setDoorOpenings(configurator, new ChoiceOpening[] { ChoiceOpening.LEFT });
		Common.setSideliteOpenings(configurator, new ChoiceOpening[] { ChoiceOpening.LEFT });
		Common.setDoorBuildType(configurator, ChoiceBuildType.FULLSTRUCTURAL);
		Common.setDoorSwingSide(configurator, ChoiceSwing.INSWING);
		Common.setSideliteSwingSide(configurator, ChoiceSwing.OUTSWING);
		Common.setSideLightSide(configurator, ChoiceSideLightSide.RIGHTSIDE);
		Common.setDoorSideLightWidth(configurator, 15d);
		Common.setDoorSlabWidth(configurator, 34d);

		Assert.assertEquals(34d, Common.getDoorSlabWidth(configurator), Common.DELTA);
		Assert.assertEquals(52.5, Common.getDoorWidth(configurator), Common.DELTA);

		for (SideLight sl : Common.getDoorSideLights(configurator))
		{
			Assert.assertNotNull(sl);
			Assert.assertEquals(15.0, sl.getSlabWidth().doubleValue(Units.INCH), Common.DELTA);
		}

		for (Door slab : Common.getDoorSlabs(configurator))
		{
			Assert.assertNotNull(slab);
			Assert.assertEquals(34d, slab.getSlabWidth().doubleValue(Units.INCH), Common.DELTA);
		}

		configurator.dispose();

	}

	// -------------- 1 DOOR - 1 SIDELITE / RIGHT SIDELITE / door INSWING - sidelite INSWING / door LEFT operating - side LEFT operatiing / DOOR
	// 34" - SIDELITE 15" ---------------

	@Test
	public void EntranceDoorCase26()
	{
		Configurator configurator = Common.instanciateConfigurable("STD");

		Common.setDoorNbSlab(configurator, 1);
		Common.setDoorNbSideLight(configurator, 1);
		Common.setDoorOpenings(configurator, new ChoiceOpening[] { ChoiceOpening.LEFT });
		Common.setSideliteOpenings(configurator, new ChoiceOpening[] { ChoiceOpening.LEFT });
		Common.setDoorBuildType(configurator, ChoiceBuildType.FULLSTRUCTURAL);
		Common.setDoorSwingSide(configurator, ChoiceSwing.INSWING);
		Common.setSideliteSwingSide(configurator, ChoiceSwing.INSWING);
		Common.setSideLightSide(configurator, ChoiceSideLightSide.RIGHTSIDE);
		Common.setDoorSideLightWidth(configurator, 15d);
		Common.setDoorSlabWidth(configurator, 34d);

		Assert.assertEquals(34d, Common.getDoorSlabWidth(configurator), Common.DELTA);
		Assert.assertEquals(53, Common.getDoorWidth(configurator), Common.DELTA);

		for (SideLight sl : Common.getDoorSideLights(configurator))
		{
			Assert.assertNotNull(sl);
			Assert.assertEquals(15.0, sl.getSlabWidth().doubleValue(Units.INCH), Common.DELTA);
		}

		for (Door slab : Common.getDoorSlabs(configurator))
		{
			Assert.assertNotNull(slab);
			Assert.assertEquals(34d, slab.getSlabWidth().doubleValue(Units.INCH), Common.DELTA);
		}

		configurator.dispose();

	}

}