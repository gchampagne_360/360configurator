package com.netappsid.test;

import java.awt.geom.Point2D;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.client360.configuration.wad.CasementVisionPvc;
import com.client360.configuration.wad.InternalMuntin;
import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.netappsid.commonutils.geo.Form;
import com.netappsid.commonutils.math.Units;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.erp.configurator.configmanagement.ConfigurationManager;
import com.netappsid.erp.configurator.gui.Configurator;
import com.netappsid.test.common.Common;
import com.netappsid.wadconfigurator.Bar;
import com.netappsid.wadconfigurator.InsulatedGlass;
import com.netappsid.wadconfigurator.Section;

/**
 * AdvancedSectionGrilles tests. Limitation: Product data must be symetric otherwise, some test will fail.
 * 
 * @author sboule
 */
public class AdvancedSectionGrillesTest
{
	private static final double DELTA = 0.001;
	private static CasementVisionPvc configurable;
	private static Double width = 22d;
	private static Double height = 33d;

	@BeforeClass
	public static void init() throws Exception
	{
		System.out.println("*********************************************** Starting tests");
		configurable = (CasementVisionPvc) Configurator.getConfigurableByName("CVP_WOA", null);
		Common.initialSetup(configurable, 1, width, height);
	}

	@AfterClass
	public static void tearDown()
	{
		System.out.println("*********************************************** Tests ended");
	}

	@Test
	/**
	 *  SDL 1x1 offsets.
	 */
	public void sdlOffsets_1x1()
	{
		setMuntinChoice(configurable, null);
		setSdl(configurable, 1, 1, null);
		sdlOffsetsTest();
	}

	@Test
	/**
	 *  SDL 2x2 offsets.
	 */
	public void sdlOffsets_2x2()
	{
		setMuntinChoice(configurable, null);
		setSdl(configurable, 2, 2, null);
		sdlOffsetsTest();
	}

	@Test
	/**
	 *  AdvancedSectionGrilles array must be empty for SDL 1x1.
	 */
	public void sdlArray_1x1()
	{
		setMuntinChoice(configurable, null);
		setSdl(configurable, 1, 1, null);
		sdlArraysTest();
	}

	@Test
	/**
	 *  AdvancedSectionGrilles array must be empty for SDL 2x2.
	 */
	public void sdlArray_2x2()
	{
		setMuntinChoice(configurable, null);
		setSdl(configurable, 2, 2, null);
		sdlArraysTest();
	}

	@Test
	/**
	 *  SDL 1x1 glass
	 */
	public void sdlGlass_1x1()
	{
		setMuntinChoice(configurable, null);
		setSdl(configurable, 1, 1, null);
		sdlGlassTest();
	}

	@Test
	/**
	 *  SDL 2x2 glass
	 */
	public void sdlGlass_2x2()
	{
		setMuntinChoice(configurable, null);
		setSdl(configurable, 2, 2, null);
		sdlGlassTest();
	}

	@Test
	/**
	 *  SDL 1x1 glass
	 */
	public void sdlOverallGrille_1x1()
	{
		setMuntinChoice(configurable, null);
		setSdl(configurable, 1, 1, null);
		sdlOverallGrilleTest();
	}

	@Test
	/**
	 *  SDL 2x2 glass
	 */
	public void sdlOverallGrille_2x2()
	{
		setMuntinChoice(configurable, null);
		setSdl(configurable, 2, 2, null);
		sdlOverallGrilleTest();
	}

	@Test
	/**
	 *  SDL 1x1 bars
	 */
	public void sdlBars_1x1()
	{
		setMuntinChoice(configurable, null);
		setSdl(configurable, 1, 1, null);
		sdlBarsTest(1, 1, ProductData.getValue(ProductType.INTERNAL_MUNTIN, Data.INTERNAL_MUNTIN_SDL_THICKNESS));
	}

	@Test
	/**
	 *  SDL 2x2 bars
	 */
	public void sdlBars_2x2()
	{
		setMuntinChoice(configurable, null);
		setSdl(configurable, 2, 2, null);
		sdlBarsTest(2, 2, ProductData.getValue(ProductType.INTERNAL_MUNTIN, Data.INTERNAL_MUNTIN_SDL_THICKNESS));
	}

	@Test
	/**
	 *  SDL 1x2 bars
	 *  The quantity of bars is changed to test the ability to dynamically change the bars.
	 */
	public void sdlBars_1x2()
	{
		setMuntinChoice(configurable, null);
		setSdl(configurable, 1, 2, null);
		sdlBarsTest(1, 2, ProductData.getValue(ProductType.INTERNAL_MUNTIN, Data.INTERNAL_MUNTIN_SDL_THICKNESS));

		setSdl(configurable, 2, 1, null);
		sdlBarsTest(2, 1, ProductData.getValue(ProductType.INTERNAL_MUNTIN, Data.INTERNAL_MUNTIN_SDL_THICKNESS));
	}

	@Test
	/**
	 *  AdvancedSectionGrilles array must be set for TDL 1x1.
	 */
	public void tdlArray_1x1()
	{
		setMuntinChoice(configurable, null);
		setTdl(configurable, 1, 1, null);
		tdlArraysTest(1, 1);
	}

	@Test
	/**
	 *  AdvancedSectionGrilles array must be set for TDL 2x2.
	 */
	public void tdlArray_2x2()
	{
		setMuntinChoice(configurable, null);
		setTdl(configurable, 2, 2, null);
		tdlArraysTest(2, 2);
	}

	@Test
	/**
	 *  TDL 1x1 overall grille offsets.
	 */
	public void tdlOffsets_1x1()
	{
		setMuntinChoice(configurable, null);
		setTdl(configurable, 1, 1, null);
		tdlOffsetsTest();
	}

	@Test
	/**
	 *  TDL 2x2 overall grille offsets.
	 */
	public void tdlOffsets_2x2()
	{
		setMuntinChoice(configurable, null);
		setTdl(configurable, 2, 2, null);
		tdlOffsetsTest();
	}

	@Test
	/**
	 *  TDL 1x1 glasses.
	 */
	public void tdlGlasses_1x1()
	{
		Measure<Length> thickness = Measure.valueOf(2d, Units.INCH);

		setMuntinChoice(configurable, null);
		setTdl(configurable, 1, 1, thickness);
		tdlGlassesTest(1, 1, thickness);
	}

	@Test
	/**
	 *  TDL 2x2 glasses.
	 */
	public void tdlGlasses_2x2()
	{
		Measure<Length> thickness = Measure.valueOf(2d, Units.INCH);

		setMuntinChoice(configurable, null);
		setTdl(configurable, 2, 2, thickness);
		tdlGlassesTest(2, 2, thickness);
	}

	@Test
	/**
	 *  TDL 1x1 overall grilles.
	 */
	public void tdlGrilles_1x1()
	{
		Measure<Length> thickness = Measure.valueOf(2d, Units.INCH);

		setMuntinChoice(configurable, null);
		setTdl(configurable, 1, 1, thickness);
		tdlGrillesTest(1, 1, thickness);
	}

	@Test
	/**
	 *  TDL 2x2 overall grilles.
	 */
	public void tdlGrilles_2x2()
	{
		Measure<Length> thickness = Measure.valueOf(2d, Units.INCH);

		setMuntinChoice(configurable, null);
		setTdl(configurable, 2, 2, thickness);
		tdlGrillesTest(2, 2, thickness);
	}

	/**
	 * TDL 1x1 bars.
	 */
	@Test
	public void tdlBars_1x1()
	{
		Measure<Length> thickness = Measure.valueOf(2d, Units.INCH);

		setMuntinChoice(configurable, null);
		setTdl(configurable, 1, 1, thickness);
		tdlBarsTest(1, 1, thickness);
	}

	/**
	 * TDL 2x2 bars.
	 */
	@Test
	public void tdlBars_2x2()
	{
		Measure<Length> thickness = Measure.valueOf(2d, Units.INCH);

		setMuntinChoice(configurable, null);
		setTdl(configurable, 2, 2, thickness);
		tdlBarsTest(2, 2, thickness);
	}

	/**
	 * TDL 1x2 bars.
	 */
	@Test
	public void tdlBars_1x2()
	{
		Measure<Length> thickness = Measure.valueOf(2d, Units.INCH);

		setMuntinChoice(configurable, null);
		setTdl(configurable, 1, 2, thickness);
		tdlBarsTest(1, 2, thickness);

		setTdl(configurable, 2, 1, thickness);
		tdlBarsTest(2, 1, thickness);
	}

	/**
	 * Grille offsets.
	 */
	@Test
	public void grillesOffsets()
	{
		setMuntinChoice(configurable, null);
		setRegularGrilles(configurable.getInsulatedGlasses().get(0), 1, 1);
		grillesOffsetsTest(configurable.getInsulatedGlasses().get(0), null, false, false);
	}

	/**
	 * Grille arrays.
	 */
	@Test
	public void grillesArrays()
	{
		setMuntinChoice(configurable, null);
		setRegularGrilles(configurable.getInsulatedGlasses().get(0), 1, 1);
		grillesArraysTest(configurable.getInsulatedGlasses().get(0));
	}

	/**
	 * Grille glass.
	 */
	@Test
	public void grillesGlass()
	{
		setMuntinChoice(configurable, null);
		setRegularGrilles(configurable.getInsulatedGlasses().get(0), 1, 1);
		grillesGlassTest(configurable.getInsulatedGlasses().get(0), null);
	}

	/**
	 * Grille overall grille.
	 */
	@Test
	public void grillesOverallGrille()
	{
		setMuntinChoice(configurable, null);
		setRegularGrilles(configurable.getInsulatedGlasses().get(0), 1, 1);
		grillesOverallGrilleTest(configurable.getInsulatedGlasses().get(0), null);
	}

	/**
	 * Grille bars 1x1.
	 */
	@Test
	public void grillesBars_1x1()
	{
		setMuntinChoice(configurable, null);
		setRegularGrilles(configurable.getInsulatedGlasses().get(0), 1, 1);
		grillesBarsTest(configurable.getInsulatedGlasses().get(0), 1, 1, Measure.valueOf(2 / 8d, Units.INCH), true, true);
	}

	/**
	 * Grille bars 2x2.
	 */
	@Test
	public void grillesBars_2x2()
	{
		setMuntinChoice(configurable, null);
		setRegularGrilles(configurable.getInsulatedGlasses().get(0), 2, 2);
		grillesBarsTest(configurable.getInsulatedGlasses().get(0), 2, 2, Measure.valueOf(2 / 8d, Units.INCH), true, true);
	}

	@Test
	/**
	 *  TDL grille offsets for all thermos.
	 *  Test the grilles inside one of the TDL newly cut glass
	 */
	public void tdlGrillesOffsets()
	{
		setMuntinChoice(configurable, null);
		setSdl(configurable, 0, 0, Units.inch(0d));
		setTdl(configurable, 1, 1, null);

		for (int i = 0; i < 4; i++)
		{
			setRegularGrilles(configurable.getInsulatedGlasses().get(i), 1, 1);
			grillesOffsetsTest(configurable.getInsulatedGlasses().get(i), ProductData.getValue(ProductType.INTERNAL_MUNTIN, Data.INTERNAL_MUNTIN_THICKNESS),
			/* LEFT TOP */
			(i == 1 || i == 3), (i == 2 || i == 3));
		} /* end for */
	}

	@Test
	/**
	 *  TDL grille array.
	 *  Test the grilles inside one of the TDL newly cut glass
	 */
	public void tdlGrillesArray()
	{
		setMuntinChoice(configurable, null);
		setTdl(configurable, 1, 1, null);

		for (int i = 0; i < 4; i++)
		{
			setRegularGrilles(configurable.getInsulatedGlasses().get(i), 1, 1);
			grillesArraysTest(configurable.getInsulatedGlasses().get(i));
		} /* end for */
	}

	@Test
	/**
	 *  TDL grille glass.
	 *  Test the grilles inside one of the TDL newly cut glass
	 */
	public void tdlGrillesGlasses()
	{
		setMuntinChoice(configurable, null);
		setTdl(configurable, 1, 1, null);

		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToGet.put("configurable.sash[0].sashSection.advancedSectionGrilles.visibleGlassForms", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));
		Assert.assertEquals(4, ((Form[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.visibleGlassForms")).length);

		for (int i = 0; i < 4; i++)
		{
			setRegularGrilles(configurable.getInsulatedGlasses().get(i), 1, 1);
			grillesGlassTest(configurable.getInsulatedGlasses().get(i),
					((Form[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.visibleGlassForms"))[i]);
		} /* end for */
	}

	@Test
	/**
	 *  TDL grille overall grille.
	 *  Test the grilles inside one of the TDL newly cut glass
	 */
	public void tdlGrillesOverallGrille()
	{
		setMuntinChoice(configurable, null);
		setTdl(configurable, 1, 1, null);

		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToGet.put("configurable.sash[0].sashSection.advancedSectionGrilles.overallGrillesForms", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));
		Assert.assertEquals(4, ((Form[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.overallGrillesForms")).length);

		for (int i = 0; i < 4; i++)
		{
			setRegularGrilles(configurable.getInsulatedGlasses().get(i), 1, 1);
			grillesOverallGrilleTest(configurable.getInsulatedGlasses().get(i),
					((Form[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.overallGrillesForms"))[i]);
		} /* end for */
	}

	@Test
	/**
	 *  TDL 1x1, each glass having a 1x1 grille bars.
	 *  Test the grilles inside one of the TDL newly cut glass
	 */
	public void tdlGrillesBars_1x1()
	{
		setMuntinChoice(configurable, null);
		setTdl(configurable, 1, 1, null); // Must use default TDL thickness, otherwise the value should be passed to the grillesBarsTest method

		for (int i = 0; i < 4; i++)
		{
			InsulatedGlass glass = configurable.getInsulatedGlasses().get(i);

			setRegularGrilles(glass, 1, 1);
			grillesBarsTest(glass, 1, 1, Measure.valueOf(2 / 8d, Units.INCH), getRelativeLeftPos(i) <= ProductData.getSpacer(
					ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[3].doubleValue(Units.INCH),
					getRelativeTopPos(i) <= ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[0]
							.doubleValue(Units.INCH));
		} /* end for */
	}

	private Double getRelativeLeftPos(int glassIndex)
	{
		InsulatedGlass glass = configurable.getInsulatedGlasses().get(glassIndex);
		Point2D origin = glass.getDimension().getForm().getOrigin();
		Point2D originParent = ((Section) glass.getParent(Section.class)).getDimension().getForm().getOrigin();

		return origin.getX() - originParent.getX();
	}

	private Double getRelativeTopPos(int glassIndex)
	{
		InsulatedGlass glass = configurable.getInsulatedGlasses().get(glassIndex);
		Point2D origin = glass.getDimension().getForm().getOrigin();
		Point2D originParent = ((Section) glass.getParent(Section.class)).getDimension().getForm().getOrigin();

		return origin.getY() - originParent.getY();
	}

	@Test
	/**
	 *  TDL 2x2, each glass having a 2x2 grille bars.
	 *  Test the grilles inside one of the TDL newly cut glass
	 */
	public void tdlGrillesBars_2x2()
	{
		setMuntinChoice(configurable, null);
		setTdl(configurable, 2, 2, null); // Must use default TDL thickness, otherwise the value should be passed to the grillesBarsTest method

		for (int i = 0; i < 9; i++)
		{
			InsulatedGlass glass = configurable.getInsulatedGlasses().get(i);

			setRegularGrilles(glass, 2, 2);
			grillesBarsTest(glass, 2, 2, Measure.valueOf(2 / 8d, Units.INCH), getRelativeLeftPos(i) <= ProductData.getSpacer(
					ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[3].doubleValue(Units.INCH),
					getRelativeTopPos(i) <= ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[0]
							.doubleValue(Units.INCH));
		} /* end for */
	}

	@Test
	/**
	 *  TDL 1x2, each glass having a 2x1 grille bars.
	 *  Test the grilles inside one of the TDL newly cut glass
	 */
	public void tdlGrillesBars_1x2()
	{
		setMuntinChoice(configurable, null);
		setTdl(configurable, 1, 2, null); // Must use default TDL thickness, otherwise the value should be passed to the grillesBarsTest method

		for (int i = 0; i < 6; i++)
		{
			InsulatedGlass glass = configurable.getInsulatedGlasses().get(i);

			setRegularGrilles(glass, 2, 1);
			grillesBarsTest(glass, 2, 1, Measure.valueOf(2 / 8d, Units.INCH), getRelativeLeftPos(i) <= ProductData.getSpacer(
					ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[3].doubleValue(Units.INCH),
					getRelativeTopPos(i) <= ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[0]
							.doubleValue(Units.INCH));
		} /* end for */
	}

	/**
	 * Set TDL (true divided light) muntins. Use of the example choice "MUNTIN".
	 * 
	 * @param configurable
	 *            - Window configurable.
	 * @param nbHorizBars
	 *            - Number of horizontal muntins.
	 * @param nbVertiBars
	 *            - Number of vertical muntins.
	 * @param thickness
	 *            - Thickness of muntins (inches).
	 */
	@SuppressWarnings("unchecked")
	private void setTdl(CasementVisionPvc configurable, Integer nbHorizBars, Integer nbVertiBars, Measure<Length> thickness)
	{
		setMuntinChoice(configurable, "MUNTIN");

		Map<String, String> propertiesToSet = new LinkedHashMap<String, String>();
		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToSet.put("configurable.sash[0].sashSection.advancedSectionGrilles.nbTdlHorizBars", nbHorizBars.toString());
		propertiesToSet.put("configurable.sash[0].sashSection.advancedSectionGrilles.nbTdlVertiBars", nbVertiBars.toString());

		if (thickness != null)
		{
			propertiesToSet.put("configurable.sash[0].sashSection.AdvancedSectionGrilles.tdlThickness", thickness.toString());
		} /* end if */

		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		propertiesToGet.put("configurable.sash[0].sashSection.advancedSectionGrilles.nbTdlHorizBars", "");
		propertiesToGet.put("configurable.sash[0].sashSection.advancedSectionGrilles.nbTdlVertiBars", "");
		propertiesToGet.put("configurable.sash[0].sashSection.advancedSectionGrilles.tdlThickness", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		Assert.assertEquals(nbHorizBars, propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.nbTdlHorizBars"));
		Assert.assertEquals(nbVertiBars, propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.nbTdlVertiBars"));

		if (thickness != null)
		{
			Assert.assertEquals(thickness.doubleValue(Units.INCH),
					((Measure<Length>) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.tdlThickness")).doubleValue(Units.INCH),
					DELTA);
		} /* end if */
		else
		{
			// Default thickness from product data
			Assert.assertEquals((ProductData.getValue(ProductType.INTERNAL_MUNTIN, Data.INTERNAL_MUNTIN_THICKNESS).doubleValue(Units.INCH)),
					((Measure<Length>) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.tdlThickness")).doubleValue(Units.INCH),
					DELTA);
		} /* end else */
	}

	/**
	 * Set SDL (simulated divided light) muntins. Use of the example choice "MUNTIN".
	 * 
	 * @param configurable
	 *            - Window configurable.
	 * @param nbHorizBars
	 *            - Number of horizontal muntins.
	 * @param nbVertiBars
	 *            - Number of vertical muntins.
	 * @param thickness
	 *            - Thickness of muntins (inches).
	 */
	@SuppressWarnings("unchecked")
	private void setSdl(CasementVisionPvc configurable, Integer nbHorizBars, Integer nbVertiBars, Measure<Length> thickness)
	{
		setMuntinChoice(configurable, "MUNTIN");

		Map<String, String> propertiesToSet = new LinkedHashMap<String, String>();
		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToSet.put("configurable.sash[0].sashSection.advancedSectionGrilles.nbSdlHorizBars", nbHorizBars.toString());
		propertiesToSet.put("configurable.sash[0].sashSection.advancedSectionGrilles.nbSdlVertiBars", nbVertiBars.toString());

		if (thickness != null)
		{
			propertiesToSet.put("configurable.sash[0].sashSection.AdvancedSectionGrilles.sdlThickness", thickness.toString());
		} /* end if */

		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		propertiesToGet.put("configurable.sash[0].sashSection.advancedSectionGrilles.nbSdlHorizBars", "");
		propertiesToGet.put("configurable.sash[0].sashSection.advancedSectionGrilles.nbSdlVertiBars", "");
		propertiesToGet.put("configurable.sash[0].sashSection.advancedSectionGrilles.sdlThickness", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		Assert.assertEquals(nbHorizBars, propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.nbSdlHorizBars"));
		Assert.assertEquals(nbVertiBars, propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.nbSdlVertiBars"));

		if (thickness != null)
		{
			Assert.assertEquals(thickness.doubleValue(Units.INCH),
					((Measure<Length>) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.sdlThickness")).doubleValue(Units.INCH),
					DELTA);
		} /* end if */
		else
		{
			// Default thickness from product data
			Assert.assertEquals((ProductData.getValue(ProductType.INTERNAL_MUNTIN, Data.INTERNAL_MUNTIN_SDL_THICKNESS).doubleValue(Units.INCH)),
					((Measure<Length>) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.sdlThickness")).doubleValue(Units.INCH),
					DELTA);
		} /* end else */
	}

	/**
	 * Set a regular grille in the specified configurable. Use of the example choice "REGULAR".
	 * 
	 * @param configurable
	 *            - Configurable to set the grille into.
	 * @param nbHorizBars
	 *            - Number of horizontal bars.
	 * @param nbVertiBars
	 *            - Number of vertical bars.
	 */
	private void setRegularGrilles(Configurable configurable, Integer nbHorizBars, Integer nbVertiBars)
	{
		setMuntinChoice(configurable, "REGULAR");

		Map<String, String> propertiesToSet = new LinkedHashMap<String, String>();
		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		String path = "";

		if (configurable instanceof CasementVisionPvc)
		{
			path = "configurable.sash[0].sashSection.advancedSectionGrilles";
		} /* end if */

		else if (configurable instanceof InsulatedGlass)
		{
			path = "configurable.advancedSectionGrilles";
		} /* end if */
		else
		{
			System.out.println("Unsupported configurable " + configurable.getClass().getName());
			Assert.assertTrue(false);
		} /* end else */

		propertiesToSet.put(path + ".nbHorizBars", nbHorizBars.toString());
		propertiesToSet.put(path + ".nbVertiBars", nbVertiBars.toString());

		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		propertiesToGet.put(path + ".nbHorizBars", "");
		propertiesToGet.put(path + ".nbVertiBars", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		Assert.assertEquals(nbHorizBars, propertiesToGet.get(path + ".nbHorizBars"));
		Assert.assertEquals(nbVertiBars, propertiesToGet.get(path + ".nbVertiBars"));
	}

	/**
	 * Set the specified advancedSectionGrilles choice if not already set.
	 * 
	 * @param configurable
	 *            - CasementVisionPvc or InsulatedGlass configurable.
	 * @param enumChoice
	 *            - Choice to set.
	 */
	private void setMuntinChoice(Configurable configurable, String enumChoice)
	{
		Map<String, String> propertiesToSet = new LinkedHashMap<String, String>();
		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		String path = "";

		if (configurable instanceof CasementVisionPvc)
		{
			path = "configurable.sash[0].sashSection.choiceAdvancedGrilles";
		} /* end if */

		else if (configurable instanceof InsulatedGlass)
		{
			path = "configurable.choiceAdvancedGrilles";
		} /* end if */
		else
		{
			System.out.println("Unsupported configurable " + configurable.getClass().getName());
			Assert.assertTrue(false);
		} /* end else */

		// Select the choice enum if not already selected.
		propertiesToGet.put(path, "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		if (propertiesToGet.get(path) == null || !propertiesToGet.get(path).toString().equals(enumChoice))
		{
			propertiesToSet.put(path, enumChoice);
			Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

			Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

			if (enumChoice != null)
			{
				Assert.assertEquals(enumChoice, propertiesToGet.get(path).toString());
			} /* end if */
		} /* end if */
	}

	/**
	 * Test SDL offsets.
	 */
	private void sdlOffsetsTest()
	{
		String path = "configurable.sash[0].sashSection.advancedSectionGrilles";

		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToGet.put(path + ".leftRelativePosition", "");
		propertiesToGet.put(path + ".topRelativePosition", "");
		propertiesToGet.put(path + ".leftOverallGrillesToVisibleGlassTranslation", "");
		propertiesToGet.put(path + ".topOverallGrillesToVisibleGlassTranslation", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		// ********************** OFFSETS
		Assert.assertEquals(
				ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[3].doubleValue(Units.INCH)
						+ ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.VISIBLEGLASS_TO_GRILLES)[3].doubleValue(Units.INCH),
				propertiesToGet.get(path + ".leftRelativePosition"));
		Assert.assertEquals(
				ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[0].doubleValue(Units.INCH)
						+ ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.VISIBLEGLASS_TO_GRILLES)[0].doubleValue(Units.INCH),
				propertiesToGet.get(path + ".topRelativePosition"));
		Assert.assertEquals(ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.VISIBLEGLASS_TO_GRILLES)[3].doubleValue(Units.INCH) * -1,
				propertiesToGet.get(path + ".leftOverallGrillesToVisibleGlassTranslation"));
		Assert.assertEquals(ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.VISIBLEGLASS_TO_GRILLES)[0].doubleValue(Units.INCH) * -1,
				propertiesToGet.get(path + ".topOverallGrillesToVisibleGlassTranslation"));
	}

	/**
	 * Test for SDL arrays: they must be empty.
	 */
	private void sdlArraysTest()
	{
		String path = "configurable.sash[0].sashSection.advancedSectionGrilles";

		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToGet.put(path + ".overallGrillesForms", "");
		propertiesToGet.put(path + ".visibleGlassForms", "");
		propertiesToGet.put(path + ".leftOverallGrillesToVisibleGlassTranslations", "");
		propertiesToGet.put(path + ".topOverallGrillesToVisibleGlassTranslations", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		// ********************** ARRAY OF OBJECTS: No additional glass are created, the array must be empty
		Assert.assertEquals(0, ((Form[]) propertiesToGet.get(path + ".overallGrillesForms")).length);
		Assert.assertEquals(0, ((Form[]) propertiesToGet.get(path + ".visibleGlassForms")).length);
		Assert.assertEquals(0, ((Double[]) propertiesToGet.get(path + ".leftOverallGrillesToVisibleGlassTranslations")).length);
		Assert.assertEquals(0, ((Double[]) propertiesToGet.get(path + ".topOverallGrillesToVisibleGlassTranslations")).length);
	}

	/**
	 * Test for SDL glass.
	 */
	private void sdlGlassTest()
	{
		String path = "configurable.sash[0].sashSection.advancedSectionGrilles";

		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToGet.put(path + ".visibleGlassForm", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		// ********************** VISIBLE GLASS
		Assert.assertEquals(
				width - ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[1].doubleValue(Units.INCH)
						- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[3].doubleValue(Units.INCH),
				((Form) propertiesToGet.get(path + ".visibleGlassForm")).getWidth(), DELTA);
		Assert.assertEquals(
				height - ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[0].doubleValue(Units.INCH)
						- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[2].doubleValue(Units.INCH),
				((Form) propertiesToGet.get(path + ".visibleGlassForm")).getHeight(), DELTA);
		Assert.assertEquals(
				width - ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[1].doubleValue(Units.INCH)
						- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[3].doubleValue(Units.INCH)
						- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[1].doubleValue(Units.INCH)
						- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[3].doubleValue(Units.INCH),
				((Form) propertiesToGet.get(path + ".visibleGlassForm")).getInteriorWidth(), DELTA);
		Assert.assertEquals(
				height - ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[0].doubleValue(Units.INCH)
						- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[2].doubleValue(Units.INCH)
						- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[0].doubleValue(Units.INCH)
						- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[2].doubleValue(Units.INCH),
				((Form) propertiesToGet.get(path + ".visibleGlassForm")).getInteriorHeight(), DELTA);
		Assert.assertEquals(ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[3].doubleValue(Units.INCH),
				((Form) propertiesToGet.get(path + ".visibleGlassForm")).getLeftInteriorFormTranslation(), DELTA);
		Assert.assertEquals(ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[0].doubleValue(Units.INCH),
				((Form) propertiesToGet.get(path + ".visibleGlassForm")).getTopInteriorFormTranslation(), DELTA);
	}

	/**
	 * Test for SDL overall grille.
	 */
	private void sdlOverallGrilleTest()
	{
		String path = "configurable.sash[0].sashSection.advancedSectionGrilles";

		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToGet.put(path + ".overallGrillesForm", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		// ********************** OVERALL GRILLES
		Assert.assertEquals(
				width - ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[1].doubleValue(Units.INCH)
						- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[3].doubleValue(Units.INCH)
						- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[1].doubleValue(Units.INCH)
						- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[3].doubleValue(Units.INCH),
				((Form) propertiesToGet.get(path + ".overallGrillesForm")).getWidth(), DELTA);
		Assert.assertEquals(
				height - ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[0].doubleValue(Units.INCH)
						- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[2].doubleValue(Units.INCH)
						- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[0].doubleValue(Units.INCH)
						- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[2].doubleValue(Units.INCH),
				((Form) propertiesToGet.get(path + ".overallGrillesForm")).getHeight(), DELTA);
		Assert.assertEquals(
				width - ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[1].doubleValue(Units.INCH)
						- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[3].doubleValue(Units.INCH)
						- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[1].doubleValue(Units.INCH)
						- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[3].doubleValue(Units.INCH)
						- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.VISIBLEGLASS_TO_GRILLES)[1].doubleValue(Units.INCH)
						- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.VISIBLEGLASS_TO_GRILLES)[3].doubleValue(Units.INCH),
				((Form) propertiesToGet.get(path + ".overallGrillesForm")).getInteriorWidth(), DELTA);
		Assert.assertEquals(
				height - ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[0].doubleValue(Units.INCH)
						- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[2].doubleValue(Units.INCH)
						- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[0].doubleValue(Units.INCH)
						- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[2].doubleValue(Units.INCH)
						- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.VISIBLEGLASS_TO_GRILLES)[0].doubleValue(Units.INCH)
						- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.VISIBLEGLASS_TO_GRILLES)[2].doubleValue(Units.INCH),
				((Form) propertiesToGet.get(path + ".overallGrillesForm")).getInteriorHeight(), DELTA);
		Assert.assertEquals(ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.VISIBLEGLASS_TO_GRILLES)[3].doubleValue(Units.INCH),
				((Form) propertiesToGet.get(path + ".overallGrillesForm")).getLeftInteriorFormTranslation(), DELTA);
		Assert.assertEquals(ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.VISIBLEGLASS_TO_GRILLES)[0].doubleValue(Units.INCH),
				((Form) propertiesToGet.get(path + ".overallGrillesForm")).getTopInteriorFormTranslation(), DELTA);
	}

	/**
	 * Test for SDL bars.
	 * 
	 * @param horizBars
	 *            - Number of horizontal muntins.
	 * @param vertiBars
	 *            - Number of vertical muntins.
	 * @param thickness
	 *            - Thickness of the muntins (inches).
	 */
	private void sdlBarsTest(Integer horizBars, Integer vertiBars, Measure<Length> thickness)
	{
		String path = "configurable.sash[0].sashSection.advancedSectionGrilles";
		double halfThickness = 0d;

		if (thickness != null)
		{
			halfThickness = thickness.doubleValue(Units.INCH) / 2;
		} /* end if */

		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToGet.put(path + ".splittingBars", "");
		propertiesToGet.put(path + ".noSplittingBars", "");
		propertiesToGet.put(path + ".visibleGlassForm", "");
		propertiesToGet.put(path + ".overallGrillesForm", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		// ********************** BARS
		// Assert.assertNull(propertiesToGet.get(path + ".splittingBars")); // The set default sets TDL bars...
		Assert.assertEquals(horizBars + vertiBars, ((Bar[]) propertiesToGet.get(path + ".noSplittingBars")).length);

		// ---------------------- Horizontal bars
		int horizBarNo = 0;
		for (int i = 0; i < horizBars; i++)
		{
			if (Math.abs( // Find horizontal bars
			((Bar[]) propertiesToGet.get(path + ".noSplittingBars"))[i].getLine().getY1()
					- ((Bar[]) propertiesToGet.get(path + ".noSplittingBars"))[i].getLine().getY2()) < DELTA)
			{
				// Thickness
				Assert.assertEquals(thickness, ((Bar[]) propertiesToGet.get(path + ".noSplittingBars"))[i].getThickness());
				// Position
				Double horizDistance = (((Form) propertiesToGet.get(path + ".visibleGlassForm")).getInteriorHeight() - (horizBars * thickness
						.doubleValue(Units.INCH))) / (horizBars + 1);

				Assert.assertEquals(
						(horizDistance * (horizBarNo + 1)) + (thickness.doubleValue(Units.INCH) * horizBarNo) + halfThickness
								+ ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.VISIBLEGLASS_TO_GRILLES)[0].doubleValue(Units.INCH) * -1,
						((Bar[]) propertiesToGet.get(path + ".noSplittingBars"))[i].getLine().getY1(), DELTA);
				// Left
				Assert.assertEquals(0d, ((Bar[]) propertiesToGet.get(path + ".noSplittingBars"))[i].getLine().getX2(), DELTA);
				// Right
				Assert.assertEquals(((Form) propertiesToGet.get(path + ".overallGrillesForm")).getInteriorWidth(),
						((Bar[]) propertiesToGet.get(path + ".noSplittingBars"))[i].getLine().getX1(), DELTA);

				// Next horizontal bar
				++horizBarNo;
			} /* end if */
		} /* end for */

		// ---------------------- Vertical bar
		int vertBarNo = 0;
		for (int i = 0; i < vertiBars; i++)
		{
			if (Math.abs( // Find horizontal bars
			((Bar[]) propertiesToGet.get(path + ".noSplittingBars"))[i].getLine().getX1()
					- ((Bar[]) propertiesToGet.get(path + ".noSplittingBars"))[i].getLine().getX2()) < DELTA)
			{
				// Thickness
				Assert.assertEquals(thickness, ((Bar[]) propertiesToGet.get(path + ".noSplittingBars"))[i].getThickness());
				// Position
				Double vertiDistance = (((Form) propertiesToGet.get(path + ".visibleGlassForm")).getInteriorWidth() - (vertiBars * thickness
						.doubleValue(Units.INCH))) / (vertiBars + 1);

				Assert.assertEquals(
						(vertiDistance * (vertBarNo + 1)) + (thickness.doubleValue(Units.INCH) * vertBarNo) + halfThickness
								+ ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.VISIBLEGLASS_TO_GRILLES)[0].doubleValue(Units.INCH) * -1,
						((Bar[]) propertiesToGet.get(path + ".noSplittingBars"))[i].getLine().getX1(), DELTA);
				// Top
				Assert.assertEquals(0d, ((Bar[]) propertiesToGet.get(path + ".noSplittingBars"))[i].getLine().getY1(), DELTA);
				// Bottom
				Assert.assertEquals(((Form) propertiesToGet.get(path + ".overallGrillesForm")).getInteriorHeight(),
						((Bar[]) propertiesToGet.get(path + ".noSplittingBars"))[i].getLine().getY2(), DELTA);

				// Next vertical bar
				++vertBarNo;
			} /* end if */
		} /* end for */
	}

	/**
	 * Test a grilles offsets.
	 * 
	 * @param configurable
	 *            - InsulatedGlass
	 * @param thickness
	 *            - Muntin thickness.
	 * @param leftMuntin
	 *            - True if the glass has a muntin on the left side.
	 * @param topMuntin
	 *            - True if the glass has a muntin on the top side.
	 */
	private void grillesOffsetsTest(Configurable configurable, Measure<Length> thickness, boolean leftMuntin, boolean topMuntin)
	{
		String path = "configurable.advancedSectionGrilles";

		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToGet.put(path + ".leftRelativePosition", "");
		propertiesToGet.put(path + ".topRelativePosition", "");
		propertiesToGet.put(path + ".leftOverallGrillesToVisibleGlassTranslation", "");
		propertiesToGet.put(path + ".topOverallGrillesToVisibleGlassTranslation", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		// ********************** OFFSETS
		Assert.assertEquals(ProductData.getValue(ProductType.INSULATED_GLASS, Data.GLASS_TO_GRILLS_OFFSET_SPACERS).doubleValue(Units.INCH),
				propertiesToGet.get(path + ".leftRelativePosition"));
		Assert.assertEquals(ProductData.getValue(ProductType.INSULATED_GLASS, Data.GLASS_TO_GRILLS_OFFSET_SPACERS).doubleValue(Units.INCH),
				propertiesToGet.get(path + ".topRelativePosition"));

		if (leftMuntin)
		{
			// FIXME leftOverallGrillesToVisibleGlassTranslation test
			// Assert.assertEquals(
			// thickness.doubleValue(Units.INCH)/2 - // Muntin to visible glass
			// ProductData.getValue(ProductType.INTERNAL_MUNTIN, Data.INTERNAL_MUNTIN_TO_GRILLES).doubleValue(Units.INCH),
			// propertiesToGet.get(path + ".leftOverallGrillesToVisibleGlassTranslation"));
		} /* end if */
		else
		{
			Assert.assertEquals(
					ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[3].doubleValue(Units.INCH)
							- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[3].doubleValue(Units.INCH)
							- ProductData.getValue(ProductType.INSULATED_GLASS, Data.GLASS_TO_GRILLS_OFFSET_SPACERS).doubleValue(Units.INCH),
					propertiesToGet.get(path + ".leftOverallGrillesToVisibleGlassTranslation"));
		} /* end else */

		if (topMuntin)
		{
			// FIXME topOverallGrillesToVisibleGlassTranslation test
			// Assert.assertEquals(
			// thickness.doubleValue(Units.INCH)/2 - // Muntin to visible glass
			// ProductData.getValue(ProductType.INTERNAL_MUNTIN, Data.INTERNAL_MUNTIN_TO_GRILLES).doubleValue(Units.INCH),
			// propertiesToGet.get(path + ".topOverallGrillesToVisibleGlassTranslation"));
		} /* end if */
		else
		{
			Assert.assertEquals(
					ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[0].doubleValue(Units.INCH)
							- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[0].doubleValue(Units.INCH)
							- ProductData.getValue(ProductType.INSULATED_GLASS, Data.GLASS_TO_GRILLS_OFFSET_SPACERS).doubleValue(Units.INCH),
					propertiesToGet.get(path + ".topOverallGrillesToVisibleGlassTranslation"));
		} /* end else */
	}

	/**
	 * Test a grilles arrays: they must be empty.
	 * 
	 * @param configurable
	 *            - InsulatedGlass
	 */
	private void grillesArraysTest(Configurable configurable)
	{
		String path = "configurable.advancedSectionGrilles";

		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToGet.put(path + ".overallGrillesForms", "");
		propertiesToGet.put(path + ".visibleGlassForms", "");
		propertiesToGet.put(path + ".leftOverallGrillesToVisibleGlassTranslations", "");
		propertiesToGet.put(path + ".topOverallGrillesToVisibleGlassTranslations", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		// ********************** INVALID OBJECTS: No additional glass are created, the array must be empty
		Assert.assertEquals(0, ((Form[]) propertiesToGet.get(path + ".overallGrillesForms")).length);
		Assert.assertEquals(0, ((Form[]) propertiesToGet.get(path + ".visibleGlassForms")).length);
		Assert.assertEquals(0, ((Double[]) propertiesToGet.get(path + ".leftOverallGrillesToVisibleGlassTranslations")).length);
		Assert.assertEquals(0, ((Double[]) propertiesToGet.get(path + ".topOverallGrillesToVisibleGlassTranslations")).length);
	}

	/**
	 * Test a grilles glass.
	 * 
	 * @param configurable
	 *            - InsulatedGlass
	 * @param tdlVisibleGlassForm
	 *            - TDL splitted glass form (tested in tdlGlasses)
	 */
	private void grillesGlassTest(Configurable configurable, Form tdlVisibleGlassForm)
	{
		String path = "configurable.advancedSectionGrilles";

		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToGet.put(path + ".visibleGlassForm", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		if (tdlVisibleGlassForm == null) // No muntin, only one glass
		{
			Assert.assertEquals(
					width - ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[1].doubleValue(Units.INCH)
							- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[3].doubleValue(Units.INCH)
							- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[1].doubleValue(Units.INCH)
							- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[3].doubleValue(Units.INCH),
					((Form) propertiesToGet.get(path + ".visibleGlassForm")).getWidth(), DELTA);
			Assert.assertEquals(
					height - ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[0].doubleValue(Units.INCH)
							- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[2].doubleValue(Units.INCH)
							- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[0].doubleValue(Units.INCH)
							- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[2].doubleValue(Units.INCH),
					((Form) propertiesToGet.get(path + ".visibleGlassForm")).getHeight(), DELTA);
			Assert.assertEquals(
					width - ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[1].doubleValue(Units.INCH)
							- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[3].doubleValue(Units.INCH)
							- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[1].doubleValue(Units.INCH)
							- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[3].doubleValue(Units.INCH),
					((Form) propertiesToGet.get(path + ".visibleGlassForm")).getInteriorWidth(), DELTA);
			Assert.assertEquals(
					height - ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[0].doubleValue(Units.INCH)
							- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[2].doubleValue(Units.INCH)
							- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[0].doubleValue(Units.INCH)
							- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[2].doubleValue(Units.INCH),
					((Form) propertiesToGet.get(path + ".visibleGlassForm")).getInteriorHeight(), DELTA);
			Assert.assertEquals(
					ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[3].doubleValue(Units.INCH)
							- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[3].doubleValue(Units.INCH),
					((Form) propertiesToGet.get(path + ".visibleGlassForm")).getLeftInteriorFormTranslation(), DELTA);
			Assert.assertEquals(
					ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[0].doubleValue(Units.INCH)
							- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[3].doubleValue(Units.INCH),
					((Form) propertiesToGet.get(path + ".visibleGlassForm")).getTopInteriorFormTranslation(), DELTA);
		} /* end if */
		else
		// The glass is smaller, compare with the specified form.
		{
			Assert.assertEquals(tdlVisibleGlassForm.getWidth(), ((Form) propertiesToGet.get(path + ".visibleGlassForm")).getWidth(), DELTA);
			Assert.assertEquals(tdlVisibleGlassForm.getHeight(), ((Form) propertiesToGet.get(path + ".visibleGlassForm")).getHeight(), DELTA);
			Assert.assertEquals(tdlVisibleGlassForm.getInteriorWidth(), ((Form) propertiesToGet.get(path + ".visibleGlassForm")).getInteriorWidth(), DELTA);
			Assert.assertEquals(tdlVisibleGlassForm.getInteriorHeight(), ((Form) propertiesToGet.get(path + ".visibleGlassForm")).getInteriorHeight(), DELTA);
			Assert.assertEquals(tdlVisibleGlassForm.getLeftInteriorFormTranslation(),
					((Form) propertiesToGet.get(path + ".visibleGlassForm")).getLeftInteriorFormTranslation(), DELTA);
			Assert.assertEquals(tdlVisibleGlassForm.getTopInteriorFormTranslation(),
					((Form) propertiesToGet.get(path + ".visibleGlassForm")).getTopInteriorFormTranslation(), DELTA);
		} /* end else */
	}

	/**
	 * Test a grilles overall grille.
	 * 
	 * @param configurable
	 *            - InsulatedGlass
	 * @param tdlOverallGrilleForm
	 *            - TDL splitted glass overall grilles form (tested in tdlOverallGrille). This grille is the section grille and is not the same as the insulated
	 *            glass grille. It is only used for the overall glass form.
	 */
	private void grillesOverallGrilleTest(Configurable configurable, Form tdlOverallGrilleForm)
	{
		String path = "configurable.advancedSectionGrilles";

		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToGet.put(path + ".overallGrillesForm", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		if (tdlOverallGrilleForm == null)
		{
			Assert.assertEquals(
					width - ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[1].doubleValue(Units.INCH)
							- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[3].doubleValue(Units.INCH)
							- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[1].doubleValue(Units.INCH)
							- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[3].doubleValue(Units.INCH),
					((Form) propertiesToGet.get(path + ".overallGrillesForm")).getWidth(), DELTA);
			Assert.assertEquals(
					height - ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[0].doubleValue(Units.INCH)
							- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[2].doubleValue(Units.INCH)
							- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[0].doubleValue(Units.INCH)
							- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[2].doubleValue(Units.INCH),
					((Form) propertiesToGet.get(path + ".overallGrillesForm")).getHeight(), DELTA);
			Assert.assertEquals(
					width - ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[1].doubleValue(Units.INCH)
							- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[3].doubleValue(Units.INCH)
							- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[1].doubleValue(Units.INCH)
							- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[3].doubleValue(Units.INCH)
							- ProductData.getValue(ProductType.INSULATED_GLASS, Data.GLASS_TO_GRILLS_OFFSET_SPACERS).doubleValue(Units.INCH)
							- ProductData.getValue(ProductType.INSULATED_GLASS, Data.GLASS_TO_GRILLS_OFFSET_SPACERS).doubleValue(Units.INCH),
					((Form) propertiesToGet.get(path + ".overallGrillesForm")).getInteriorWidth(), DELTA);
			Assert.assertEquals(
					height - ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[0].doubleValue(Units.INCH)
							- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[2].doubleValue(Units.INCH)
							- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[0].doubleValue(Units.INCH)
							- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[2].doubleValue(Units.INCH)
							- ProductData.getValue(ProductType.INSULATED_GLASS, Data.GLASS_TO_GRILLS_OFFSET_SPACERS).doubleValue(Units.INCH)
							- ProductData.getValue(ProductType.INSULATED_GLASS, Data.GLASS_TO_GRILLS_OFFSET_SPACERS).doubleValue(Units.INCH),
					((Form) propertiesToGet.get(path + ".overallGrillesForm")).getInteriorHeight(), DELTA);
			Assert.assertEquals(ProductData.getValue(ProductType.INSULATED_GLASS, Data.GLASS_TO_GRILLS_OFFSET_SPACERS).doubleValue(Units.INCH),
					((Form) propertiesToGet.get(path + ".overallGrillesForm")).getLeftInteriorFormTranslation(), DELTA);
			Assert.assertEquals(ProductData.getValue(ProductType.INSULATED_GLASS, Data.GLASS_TO_GRILLS_OFFSET_SPACERS).doubleValue(Units.INCH),
					((Form) propertiesToGet.get(path + ".overallGrillesForm")).getTopInteriorFormTranslation(), DELTA);
		} /* end if */
		else
		{
			Assert.assertEquals(tdlOverallGrilleForm.getInteriorWidth(), ((Form) propertiesToGet.get(path + ".overallGrillesForm")).getWidth(), DELTA);
			Assert.assertEquals(tdlOverallGrilleForm.getInteriorHeight(), ((Form) propertiesToGet.get(path + ".overallGrillesForm")).getHeight(), DELTA);
			Assert.assertEquals(
					tdlOverallGrilleForm.getInteriorWidth() - 2
							* ProductData.getValue(ProductType.INSULATED_GLASS, Data.GLASS_TO_GRILLS_OFFSET_SPACERS).doubleValue(Units.INCH),
					((Form) propertiesToGet.get(path + ".overallGrillesForm")).getInteriorWidth(), DELTA);
			Assert.assertEquals(
					tdlOverallGrilleForm.getInteriorHeight() - 2
							* ProductData.getValue(ProductType.INSULATED_GLASS, Data.GLASS_TO_GRILLS_OFFSET_SPACERS).doubleValue(Units.INCH),
					((Form) propertiesToGet.get(path + ".overallGrillesForm")).getInteriorHeight(), DELTA);
			Assert.assertEquals(ProductData.getValue(ProductType.INSULATED_GLASS, Data.GLASS_TO_GRILLS_OFFSET_SPACERS).doubleValue(Units.INCH),
					((Form) propertiesToGet.get(path + ".overallGrillesForm")).getLeftInteriorFormTranslation(), DELTA);
			Assert.assertEquals(ProductData.getValue(ProductType.INSULATED_GLASS, Data.GLASS_TO_GRILLS_OFFSET_SPACERS).doubleValue(Units.INCH),
					((Form) propertiesToGet.get(path + ".overallGrillesForm")).getTopInteriorFormTranslation(), DELTA);
		} /* end else */
	}

	/**
	 * Test a grilles bars.
	 * 
	 * @param configurable
	 *            - InsulatedGlass
	 * @param horizBars
	 *            - Number of horizontal bars.
	 * @param vertiBars
	 *            - Number of vertical bars.
	 * @param thickness
	 *            - Thickness of the grille bars.
	 * @param left
	 *            - True if the insulated glass containing this grille is touching the sash on the left side, false otherwise.
	 * @param top
	 *            - True if the insulated glass containing this grille is touching the sash on the top side, false otherwise.
	 */
	private void grillesBarsTest(Configurable configurable, Integer horizBars, Integer vertiBars, Measure<Length> thickness, boolean left, boolean top)
	{
		String path = "configurable.advancedSectionGrilles";
		double halfThickness = 0d;

		if (thickness != null)
		{
			halfThickness = thickness.doubleValue(Units.INCH) / 2;
		} /* end if */

		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToGet.put(path + ".splittingBars", "");
		propertiesToGet.put(path + ".noSplittingBars", "");
		propertiesToGet.put(path + ".visibleGlassForm", "");
		propertiesToGet.put(path + ".overallGrillesForm", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		// ********************** BARS
		Assert.assertNull(propertiesToGet.get(path + ".splittingBars"));
		Assert.assertEquals(horizBars + vertiBars, ((Bar[]) propertiesToGet.get(path + ".noSplittingBars")).length);

		// ---------------------- Horizontal bars
		int horizBarNo = 0;
		for (int i = 0; i < horizBars; i++)
		{
			if (Math.abs( // Find horizontal bars
			((Bar[]) propertiesToGet.get(path + ".noSplittingBars"))[i].getLine().getY1()
					- ((Bar[]) propertiesToGet.get(path + ".noSplittingBars"))[i].getLine().getY2()) < DELTA)
			{
				// Thickness
				Assert.assertEquals(thickness, ((Bar[]) propertiesToGet.get(path + ".noSplittingBars"))[i].getThickness());
				// Position
				Double horizDistance = (((Form) propertiesToGet.get(path + ".visibleGlassForm")).getInteriorHeight() - (horizBars * thickness
						.doubleValue(Units.INCH))) / (horizBars + 1);

				if (top)
				{
					Assert.assertEquals(
							(horizDistance * (horizBarNo + 1)) + (thickness.doubleValue(Units.INCH) * horizBarNo) + halfThickness
									+ ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[0].doubleValue(Units.INCH)
									- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[0].doubleValue(Units.INCH)
									- ProductData.getValue(ProductType.INSULATED_GLASS, Data.GLASS_TO_GRILLS_OFFSET_SPACERS).doubleValue(Units.INCH),
							((Bar[]) propertiesToGet.get(path + ".noSplittingBars"))[i].getLine().getY1(), DELTA);
				} /* end if */
				else
				{
					// FIXME SDL Y position test
					// Assert.assertEquals((horizDistance * (horizBarNo+1)) + (thickness.doubleValue(Units.INCH) * horizBarNo) + halfThickness +
					// ProductData.getValue(ProductType.INTERNAL_MUNTIN, Data.INTERNAL_MUNTIN_THICKNESS).doubleValue(Units.INCH) / 2 -
					// ProductData.getValue(ProductType.INTERNAL_MUNTIN, Data.INTERNAL_MUNTIN_TO_GRILLES).doubleValue(Units.INCH),
					// ((Bar[])propertiesToGet.get(path + ".noSplittingBars"))[i].getLine().getY1(), DELTA);
				} /* end else */

				// Left
				Assert.assertEquals(0d, ((Bar[]) propertiesToGet.get(path + ".noSplittingBars"))[i].getLine().getX1(), DELTA);
				// Right
				Assert.assertEquals(((Form) propertiesToGet.get(path + ".overallGrillesForm")).getInteriorWidth(),
						((Bar[]) propertiesToGet.get(path + ".noSplittingBars"))[i].getLine().getX2(), DELTA);

				// Next horizontal bar
				++horizBarNo;
			} /* end if */
		} /* end for */

		// ---------------------- Vertical bar
		int vertBarNo = 0;
		for (int i = 0; i < vertiBars; i++)
		{
			if (Math.abs( // Find horizontal bars
			((Bar[]) propertiesToGet.get(path + ".noSplittingBars"))[i].getLine().getX1()
					- ((Bar[]) propertiesToGet.get(path + ".noSplittingBars"))[i].getLine().getX2()) < DELTA)
			{
				// Thickness
				Assert.assertEquals(thickness, ((Bar[]) propertiesToGet.get(path + ".noSplittingBars"))[i].getThickness());
				// Position
				Double vertiDistance = (((Form) propertiesToGet.get(path + ".visibleGlassForm")).getInteriorWidth() - (vertiBars * thickness
						.doubleValue(Units.INCH))) / (vertiBars + 1);

				if (left)
				{
					Assert.assertEquals(
							(vertiDistance * (vertBarNo + 1)) + (thickness.doubleValue(Units.INCH) * vertBarNo) + halfThickness
									+ ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[3].doubleValue(Units.INCH)
									- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[3].doubleValue(Units.INCH)
									- ProductData.getValue(ProductType.INSULATED_GLASS, Data.GLASS_TO_GRILLS_OFFSET_SPACERS).doubleValue(Units.INCH),
							((Bar[]) propertiesToGet.get(path + ".noSplittingBars"))[i].getLine().getX1(), DELTA);
				} /* end if */
				else
				{
					Assert.assertEquals((vertiDistance * (vertBarNo + 1)) + (thickness.doubleValue(Units.INCH) * vertBarNo) + halfThickness
							+ ProductData.getValue(ProductType.INTERNAL_MUNTIN, Data.INTERNAL_MUNTIN_THICKNESS).doubleValue(Units.INCH) / 2
							- ProductData.getValue(ProductType.INTERNAL_MUNTIN, Data.INTERNAL_MUNTIN_TO_GRILLES).doubleValue(Units.INCH),
							((Bar[]) propertiesToGet.get(path + ".noSplittingBars"))[i].getLine().getX1(), DELTA);
				} /* end else */

				// Top
				Assert.assertEquals(0d, ((Bar[]) propertiesToGet.get(path + ".noSplittingBars"))[i].getLine().getY1(), DELTA);
				// Bottom
				Assert.assertEquals(((Form) propertiesToGet.get(path + ".overallGrillesForm")).getInteriorHeight(),
						((Bar[]) propertiesToGet.get(path + ".noSplittingBars"))[i].getLine().getY2(), DELTA);

				// Next vertical bar
				++vertBarNo;
			} /* end if */
		} /* end for */
	}

	/**
	 * Test TDL arrays.
	 * 
	 * @param horizBars
	 *            - Number of horizontal muntins.
	 * @param vertiBars
	 *            - Number of vertical muntins.
	 */
	private void tdlArraysTest(Integer horizBars, Integer vertiBars)
	{
		int glassQty = (horizBars + 1) * (vertiBars + 1);

		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToGet.put("configurable.sash[0].sashSection.advancedSectionGrilles.overallGrillesForms", "");
		propertiesToGet.put("configurable.sash[0].sashSection.advancedSectionGrilles.visibleGlassForms", "");
		propertiesToGet.put("configurable.sash[0].sashSection.advancedSectionGrilles.leftOverallGrillesToVisibleGlassTranslations", "");
		propertiesToGet.put("configurable.sash[0].sashSection.advancedSectionGrilles.topOverallGrillesToVisibleGlassTranslations", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		// ********************** ARRAY OF OBJECTS
		Assert.assertEquals(glassQty, ((Form[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.overallGrillesForms")).length);
		Assert.assertEquals(glassQty, ((Form[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.visibleGlassForms")).length);
		Assert.assertEquals(glassQty,
				((Double[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.leftOverallGrillesToVisibleGlassTranslations")).length);
		Assert.assertEquals(glassQty,
				((Double[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.topOverallGrillesToVisibleGlassTranslations")).length);
	}

	/**
	 * Test TDL arrays.
	 */
	private void tdlOffsetsTest()
	{
		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToGet.put("configurable.sash[0].sashSection.advancedSectionGrilles.leftRelativePosition", "");
		propertiesToGet.put("configurable.sash[0].sashSection.advancedSectionGrilles.topRelativePosition", "");
		propertiesToGet.put("configurable.sash[0].sashSection.advancedSectionGrilles.leftOverallGrillesToVisibleGlassTranslation", "");
		propertiesToGet.put("configurable.sash[0].sashSection.advancedSectionGrilles.topOverallGrillesToVisibleGlassTranslation", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		// ********************** OVERALL OFFSETS
		// Overall
		Assert.assertEquals(
				ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[3].doubleValue(Units.INCH)
						+ ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.VISIBLEGLASS_TO_GRILLES)[3].doubleValue(Units.INCH),
				propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.leftRelativePosition"));
		Assert.assertEquals(
				ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[0].doubleValue(Units.INCH)
						+ ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.VISIBLEGLASS_TO_GRILLES)[0].doubleValue(Units.INCH),
				propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.topRelativePosition"));
		Assert.assertEquals(ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.VISIBLEGLASS_TO_GRILLES)[3].doubleValue(Units.INCH) * -1,
				propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.leftOverallGrillesToVisibleGlassTranslation"));
		Assert.assertEquals(ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.VISIBLEGLASS_TO_GRILLES)[0].doubleValue(Units.INCH) * -1,
				propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.topOverallGrillesToVisibleGlassTranslation"));
	}

	/**
	 * Test for TDL glasses.
	 * 
	 * @param horizBars
	 *            - Number of horizontal muntins.
	 * @param vertiBars
	 *            - Number of vertical muntins.
	 * @param thickness
	 *            - Thickness of the muntins (inches).
	 */
	@SuppressWarnings("unchecked")
	private void tdlGlassesTest(Integer horizBars, Integer vertiBars, Measure<Length> thickness)
	{
		int glassQty = (horizBars + 1) * (vertiBars + 1);

		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToGet.put("configurable.sash[0].sashSection.advancedSectionGrilles.splittingBars", "");

		for (int i = 1; i <= glassQty; i++)
		{
			propertiesToGet.put("configurable.sash[0].sashSection.insulatedGlass[" + (i - 1) + "].dimension.dimensionHeight", "");
			propertiesToGet.put("configurable.sash[0].sashSection.insulatedGlass[" + (i - 1) + "].dimension.dimensionWidth", "");
			propertiesToGet.put("configurable.sash[0].sashSection.insulatedGlass[" + (i - 1) + "].dimension.width", "");
			propertiesToGet.put("configurable.sash[0].sashSection.insulatedGlass[" + (i - 1) + "].dimension.height", "");
			propertiesToGet.put("configurable.sash[0].sashSection.insulatedGlass[" + (i - 1) + "].dimension.form", "");
			propertiesToGet.put("configurable.sash[0].sashSection.insulatedGlass[" + (i - 1) + "].relativeVisibleGlass", "");
			propertiesToGet.put("configurable.sash[0].sashSection.insulatedGlass[" + (i - 1) + "].leftRelativePosition", "");
			propertiesToGet.put("configurable.sash[0].sashSection.insulatedGlass[" + (i - 1) + "].topRelativePosition", "");
		} /* end for */

		propertiesToGet.put("configurable.sash[0].sashSection.advancedSectionGrilles.visibleGlassForm", "");
		propertiesToGet.put("configurable.sash[0].sashSection.advancedSectionGrilles.visibleGlassForms", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		Double vertiBarsDistance = (((Form) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.visibleGlassForm")).getInteriorWidth() - (vertiBars * thickness
				.doubleValue(Units.INCH))) / (vertiBars + 1);
		Double horizBarsDistance = (((Form) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.visibleGlassForm"))
				.getInteriorHeight() - (horizBars * thickness.doubleValue(Units.INCH))) / (horizBars + 1);

		double glassWidth, visibleGlassWidth;
		double glassHeight, visibleGlassHeight;
		InternalMuntin muntin = new InternalMuntin(configurable);
		muntin.setThickness(thickness);

		for (int i = 1; i <= glassQty; i++)
		{
			// Width
			int horizPos = i - (vertiBars + 1) * ((i - 1) / (vertiBars + 1));

			Double leftVisibleGlassPos = (vertiBarsDistance * (horizPos - 1)) + (thickness.doubleValue(Units.INCH) * (horizPos - 1));
			Double leftGlassPos = (ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[3].doubleValue(Units.INCH) - ProductData
					.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[3].doubleValue(Units.INCH))
					+ (vertiBarsDistance * (horizPos - 1))
					+ (thickness.doubleValue(Units.INCH) * (horizPos - 2))
					+ thickness.doubleValue(Units.INCH)
					- (muntin.getInternalMuntinToVisibleGlassOffset().doubleValue(Units.INCH) - muntin.getInternalMuntinToGlassOffset().doubleValue(Units.INCH));

			Double rightVisibleGlassPos = (vertiBarsDistance * horizPos) + (thickness.doubleValue(Units.INCH) * (horizPos - 1));
			Double rightGlassPos = (ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[3].doubleValue(Units.INCH) - ProductData
					.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[3].doubleValue(Units.INCH))
					+ (vertiBarsDistance * horizPos)
					+ (thickness.doubleValue(Units.INCH) * (horizPos - 1))
					+ (muntin.getInternalMuntinToVisibleGlassOffset().doubleValue(Units.INCH) - muntin.getInternalMuntinToGlassOffset().doubleValue(Units.INCH));

			if (horizPos == 1) // Left edge
			{
				leftGlassPos = 0d;
				leftVisibleGlassPos = 0d;

				// Translation
				Assert.assertEquals(ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[3].doubleValue(Units.INCH)
						- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[3].doubleValue(Units.INCH),
						((Form[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.visibleGlassForms"))[i - 1]
								.getLeftInteriorFormTranslation(), DELTA);
				// Offset
				Assert.assertEquals(ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[3].doubleValue(Units.INCH),
						((Double) propertiesToGet.get("configurable.sash[0].sashSection.insulatedGlass[" + (i - 1) + "].leftRelativePosition")).doubleValue(),
						DELTA);
			} /* end if */
			else
			{
				// Translation
				Assert.assertEquals(muntin.getInternalMuntinToVisibleGlassOffset().doubleValue(Units.INCH)
						- muntin.getInternalMuntinToGlassOffset().doubleValue(Units.INCH), ((Form[]) propertiesToGet
						.get("configurable.sash[0].sashSection.advancedSectionGrilles.visibleGlassForms"))[i - 1].getLeftInteriorFormTranslation(), DELTA);
				// Offset
				Assert.assertEquals(
						leftGlassPos + ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[3].doubleValue(Units.INCH),
						((Double) propertiesToGet.get("configurable.sash[0].sashSection.insulatedGlass[" + (i - 1) + "].leftRelativePosition")).doubleValue(),
						DELTA);
			} /* end else */

			if (horizPos == vertiBars + 1) // Right edge
			{
				rightGlassPos = width
						- 2d
						* (ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[3].doubleValue(Units.INCH) + ProductData
								.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[3].doubleValue(Units.INCH));
				rightVisibleGlassPos = width
						- 2d
						* (ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[3].doubleValue(Units.INCH) + ProductData
								.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[3].doubleValue(Units.INCH));

			} /* end else */

			glassWidth = rightGlassPos - leftGlassPos;
			visibleGlassWidth = rightVisibleGlassPos - leftVisibleGlassPos;

			Assert.assertEquals(glassWidth,
					((Form[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.visibleGlassForms"))[i - 1].getWidth(), DELTA);
			Assert.assertEquals(glassWidth,
					((Form) propertiesToGet.get("configurable.sash[0].sashSection.insulatedGlass[" + (i - 1) + "].relativeVisibleGlass")).getWidth(), DELTA);
			Assert.assertEquals(glassWidth,
					((Form) propertiesToGet.get("configurable.sash[0].sashSection.insulatedGlass[" + (i - 1) + "].dimension.form")).getWidth(), DELTA);
			Assert.assertEquals(glassWidth, ((Measure<Length>) propertiesToGet.get("configurable.sash[0].sashSection.insulatedGlass[" + (i - 1)
					+ "].dimension.width")).doubleValue(Units.INCH), DELTA);
			Assert.assertEquals(glassWidth,
					((Double) propertiesToGet.get("configurable.sash[0].sashSection.insulatedGlass[" + (i - 1) + "].dimension.dimensionWidth")), DELTA);

			Assert.assertEquals(visibleGlassWidth,
					((Form[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.visibleGlassForms"))[i - 1].getInteriorWidth(),
					DELTA);
			Assert.assertEquals(visibleGlassWidth,
					((Form) propertiesToGet.get("configurable.sash[0].sashSection.insulatedGlass[" + (i - 1) + "].relativeVisibleGlass")).getInteriorWidth(),
					DELTA);

			// Height
			int vertiPos = 1 + ((i - 1) / (vertiBars + 1));

			Double topVisibleGlassPos = (horizBarsDistance * (vertiPos - 1)) + (thickness.doubleValue(Units.INCH) * (vertiPos - 1));

			Double topGlassPos = (ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[0].doubleValue(Units.INCH) - ProductData
					.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[0].doubleValue(Units.INCH))
					+ (horizBarsDistance * (vertiPos - 1))
					+ (thickness.doubleValue(Units.INCH) * (vertiPos - 2))
					+ thickness.doubleValue(Units.INCH)
					- (muntin.getInternalMuntinToVisibleGlassOffset().doubleValue(Units.INCH) - muntin.getInternalMuntinToGlassOffset().doubleValue(Units.INCH));

			Double botVisibleGlassPos = (horizBarsDistance * vertiPos) + (thickness.doubleValue(Units.INCH) * (vertiPos - 1));
			Double botGlassPos = (ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[0].doubleValue(Units.INCH) - ProductData
					.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[0].doubleValue(Units.INCH))
					+ (horizBarsDistance * vertiPos)
					+ (thickness.doubleValue(Units.INCH) * (vertiPos - 1))
					+ (muntin.getInternalMuntinToVisibleGlassOffset().doubleValue(Units.INCH) - muntin.getInternalMuntinToGlassOffset().doubleValue(Units.INCH));

			if (vertiPos == 1) // Top edge
			{
				topGlassPos = 0d;
				topVisibleGlassPos = 0d;

				// Translation
				Assert.assertEquals(ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[0].doubleValue(Units.INCH)
						- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[0].doubleValue(Units.INCH),
						((Form[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.visibleGlassForms"))[i - 1]
								.getTopInteriorFormTranslation(), DELTA);
				// Offset
				Assert.assertEquals(ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[0].doubleValue(Units.INCH),
						((Double) propertiesToGet.get("configurable.sash[0].sashSection.insulatedGlass[" + (i - 1) + "].topRelativePosition")).doubleValue(),
						DELTA);
			} /* end if */
			else
			{
				// Translation
				Assert.assertEquals(muntin.getInternalMuntinToVisibleGlassOffset().doubleValue(Units.INCH)
						- muntin.getInternalMuntinToGlassOffset().doubleValue(Units.INCH), ((Form[]) propertiesToGet
						.get("configurable.sash[0].sashSection.advancedSectionGrilles.visibleGlassForms"))[i - 1].getTopInteriorFormTranslation(), DELTA);
				// Offset
				Assert.assertEquals(
						topGlassPos + ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[0].doubleValue(Units.INCH),
						((Double) propertiesToGet.get("configurable.sash[0].sashSection.insulatedGlass[" + (i - 1) + "].topRelativePosition")).doubleValue(),
						DELTA);
			} /* end else */

			if (vertiPos == horizBars + 1) // Bottom edge
			{
				botGlassPos = height
						- 2d
						* (ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[0].doubleValue(Units.INCH) + ProductData
								.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS)[0].doubleValue(Units.INCH));
				botVisibleGlassPos = height
						- 2d
						* (ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[0].doubleValue(Units.INCH) + ProductData
								.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[0].doubleValue(Units.INCH));
			} /* end else */

			glassHeight = botGlassPos - topGlassPos;
			visibleGlassHeight = botVisibleGlassPos - topVisibleGlassPos;

			Assert.assertEquals(glassHeight,
					((Form[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.visibleGlassForms"))[i - 1].getHeight(), DELTA);
			Assert.assertEquals(glassHeight,
					((Form) propertiesToGet.get("configurable.sash[0].sashSection.insulatedGlass[" + (i - 1) + "].relativeVisibleGlass")).getHeight(), DELTA);
			Assert.assertEquals(glassHeight,
					((Form) propertiesToGet.get("configurable.sash[0].sashSection.insulatedGlass[" + (i - 1) + "].dimension.form")).getHeight(), DELTA);
			Assert.assertEquals(glassHeight, ((Measure<Length>) propertiesToGet.get("configurable.sash[0].sashSection.insulatedGlass[" + (i - 1)
					+ "].dimension.height")).doubleValue(Units.INCH), DELTA);
			Assert.assertEquals(glassHeight,
					((Double) propertiesToGet.get("configurable.sash[0].sashSection.insulatedGlass[" + (i - 1) + "].dimension.dimensionHeight")), DELTA);

			Assert.assertEquals(visibleGlassHeight,
					((Form[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.visibleGlassForms"))[i - 1].getInteriorHeight(),
					DELTA);
			Assert.assertEquals(visibleGlassHeight,
					((Form) propertiesToGet.get("configurable.sash[0].sashSection.insulatedGlass[" + (i - 1) + "].relativeVisibleGlass")).getInteriorHeight(),
					DELTA);
		} /* end for */
	}

	/**
	 * Test for TDL grilles.
	 * 
	 * @param horizBars
	 *            - Number of horizontal muntins.
	 * @param vertiBars
	 *            - Number of vertical muntins.
	 * @param thickness
	 *            - Thickness of the muntins (inches).
	 */
	private void tdlGrillesTest(Integer horizBars, Integer vertiBars, Measure<Length> thickness)
	{
		int grillesQty = (horizBars + 1) * (vertiBars + 1);

		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToGet.put("configurable.sash[0].sashSection.advancedSectionGrilles.splittingBars", "");
		propertiesToGet.put("configurable.sash[0].sashSection.advancedSectionGrilles.visibleGlassForm", "");
		propertiesToGet.put("configurable.sash[0].sashSection.advancedSectionGrilles.overallGrillesForms", "");
		propertiesToGet.put("configurable.sash[0].sashSection.advancedSectionGrilles.leftOverallGrillesToVisibleGlassTranslations", "");
		propertiesToGet.put("configurable.sash[0].sashSection.advancedSectionGrilles.topOverallGrillesToVisibleGlassTranslations", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		Double vertiBarsDistance = (((Form) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.visibleGlassForm")).getInteriorWidth() - (vertiBars * thickness
				.doubleValue(Units.INCH))) / (vertiBars + 1);
		Double horizBarsDistance = (((Form) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.visibleGlassForm"))
				.getInteriorHeight() - (horizBars * thickness.doubleValue(Units.INCH))) / (horizBars + 1);

		double grilleWidth;
		double grilleHeight;

		InternalMuntin muntin = new InternalMuntin(configurable);
		muntin.setThickness(thickness);

		for (int i = 1; i <= grillesQty; i++)
		{
			// Width
			int horizPos = i - (vertiBars + 1) * ((i - 1) / (vertiBars + 1));

			Double leftGrillePos = (ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.VISIBLEGLASS_TO_GRILLES)[3].doubleValue(Units.INCH) * -1)
					+ (vertiBarsDistance * (horizPos - 1))
					+ (thickness.doubleValue(Units.INCH) * (horizPos - 2))
					+ thickness.doubleValue(Units.INCH)
					- (muntin.getInternalMuntinToVisibleGlassOffset().doubleValue(Units.INCH) - muntin.getInternalMuntinToGrillesOffset().doubleValue(
							Units.INCH));

			Double rightGrillePos = (ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.VISIBLEGLASS_TO_GRILLES)[3].doubleValue(Units.INCH) * -1)
					+ (vertiBarsDistance * horizPos)
					+ (thickness.doubleValue(Units.INCH) * (horizPos - 1))
					+ (muntin.getInternalMuntinToVisibleGlassOffset().doubleValue(Units.INCH) - muntin.getInternalMuntinToGrillesOffset().doubleValue(
							Units.INCH));

			if (horizPos == 1) // Left edge
			{
				leftGrillePos = 0d;

				// Offset
				Assert.assertEquals(
						(ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.VISIBLEGLASS_TO_GRILLES)[3].doubleValue(Units.INCH)),
						((Double[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.leftOverallGrillesToVisibleGlassTranslations"))[i - 1]
								.doubleValue(), DELTA);
			} /* end if */
			else
			{
				// Offset
				Assert.assertEquals(
						leftGrillePos + ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.VISIBLEGLASS_TO_GRILLES)[3].doubleValue(Units.INCH),
						((Double[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.leftOverallGrillesToVisibleGlassTranslations"))[i - 1]
								.doubleValue(), DELTA);
			} /* end else */

			if (horizPos == vertiBars + 1) // Right edge
			{
				rightGrillePos = width
						- 2d
						* (ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[3].doubleValue(Units.INCH)
								+ ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[3].doubleValue(Units.INCH) + ProductData
									.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.VISIBLEGLASS_TO_GRILLES)[3].doubleValue(Units.INCH));
			} /* end else */

			grilleWidth = rightGrillePos - leftGrillePos;

			Assert.assertEquals(grilleWidth,
					((Form[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.overallGrillesForms"))[i - 1].getWidth(), DELTA);

			// Height
			int vertiPos = 1 + ((i - 1) / (vertiBars + 1));

			Double topGrillePos = (ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.VISIBLEGLASS_TO_GRILLES)[0].doubleValue(Units.INCH) * -1)
					+ (horizBarsDistance * (vertiPos - 1))
					+ (thickness.doubleValue(Units.INCH) * (vertiPos - 2))
					+ thickness.doubleValue(Units.INCH)
					- (muntin.getInternalMuntinToVisibleGlassOffset().doubleValue(Units.INCH) - muntin.getInternalMuntinToGrillesOffset().doubleValue(
							Units.INCH));

			Double botGrillePos = (ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.VISIBLEGLASS_TO_GRILLES)[0].doubleValue(Units.INCH) * -1)
					+ (horizBarsDistance * vertiPos)
					+ (thickness.doubleValue(Units.INCH) * (vertiPos - 1))
					+ (muntin.getInternalMuntinToVisibleGlassOffset().doubleValue(Units.INCH) - muntin.getInternalMuntinToGrillesOffset().doubleValue(
							Units.INCH));

			if (vertiPos == 1) // Top edge
			{
				topGrillePos = 0d;

				// Offset
				Assert.assertEquals(
						(ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.VISIBLEGLASS_TO_GRILLES)[0].doubleValue(Units.INCH)),
						((Double[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.topOverallGrillesToVisibleGlassTranslations"))[i - 1]
								.doubleValue(), DELTA);
			} /* end if */
			else
			{
				// Offset
				Assert.assertEquals(
						topGrillePos + ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.VISIBLEGLASS_TO_GRILLES)[0].doubleValue(Units.INCH),
						((Double[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.topOverallGrillesToVisibleGlassTranslations"))[i - 1]
								.doubleValue(), DELTA);
			} /* end else */

			if (vertiPos == horizBars + 1) // Bottom edge
			{
				botGrillePos = height
						- 2d
						* (ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[0].doubleValue(Units.INCH)
								+ ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[0].doubleValue(Units.INCH) + ProductData
									.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.VISIBLEGLASS_TO_GRILLES)[0].doubleValue(Units.INCH));
			} /* end else */

			grilleHeight = botGrillePos - topGrillePos;

			Assert.assertEquals(grilleHeight,
					((Form[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.overallGrillesForms"))[i - 1].getHeight(), DELTA);
		} /* end for */
	}

	/**
	 * Test for TDL bars.
	 * 
	 * @param horizBars
	 *            - Number of horizontal muntins.
	 * @param vertiBars
	 *            - Number of vertical muntins.
	 * @param thickness
	 *            - Thickness of the muntins (inches).
	 */
	private void tdlBarsTest(Integer horizBars, Integer vertiBars, Measure<Length> thickness)
	{
		double halfThickness = 0d;

		if (thickness != null)
		{
			halfThickness = thickness.doubleValue(Units.INCH) / 2;
		} /* end if */

		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToGet.put("configurable.sash[0].sashSection.advancedSectionGrilles.overallGrillesForm", "");
		propertiesToGet.put("configurable.sash[0].sashSection.advancedSectionGrilles.visibleGlassForm", "");
		propertiesToGet.put("configurable.sash[0].sashSection.advancedSectionGrilles.splittingBars", "");
		propertiesToGet.put("configurable.sash[0].sashSection.advancedSectionGrilles.noSplittingBars", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		// ********************** BARS
		// Assert.assertNull(propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.noSplittingBars"));
		Assert.assertEquals(horizBars + vertiBars,
				((Bar[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.splittingBars")).length);

		// ---------------------- Horizontal bars
		int horizBarNo = 0;
		for (int i = 0; i < horizBars; i++)
		{
			if (Math.abs( // Find horizontal bars
			((Bar[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.splittingBars"))[i].getLine().getY1()
					- ((Bar[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.splittingBars"))[i].getLine().getY2()) < DELTA)
			{
				// Thickness
				Assert.assertEquals(thickness,
						((Bar[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.splittingBars"))[i].getThickness());
				// Position
				Double horizDistance = (((Form) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.visibleGlassForm"))
						.getInteriorHeight() - (horizBars * thickness.doubleValue(Units.INCH))) / (horizBars + 1);

				Assert.assertEquals(
						(horizDistance * (horizBarNo + 1)) + (thickness.doubleValue(Units.INCH) * horizBarNo) + halfThickness
								+ ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.VISIBLEGLASS_TO_GRILLES)[0].doubleValue(Units.INCH) * -1,
						((Bar[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.splittingBars"))[i].getLine().getY1(), DELTA);
				// Left
				Assert.assertEquals(0d, ((Bar[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.splittingBars"))[i].getLine()
						.getX2(), DELTA);
				// Right
				Assert.assertEquals(
						((Form) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.overallGrillesForm")).getInteriorWidth(),
						((Bar[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.splittingBars"))[i].getLine().getX1(), DELTA);

				++horizBarNo;
			} /* end if */
		} /* end for */

		// ---------------------- Vertical bar
		int vertiBarNo = 0;
		for (int i = 0; i < vertiBars; i++)
		{
			if (Math.abs( // Find horizontal bars
			((Bar[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.splittingBars"))[i].getLine().getX1()
					- ((Bar[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.splittingBars"))[i].getLine().getX2()) < DELTA)
			{
				// Thickness
				Assert.assertEquals(thickness,
						((Bar[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.splittingBars"))[i].getThickness());
				// Position
				Double vertiDistance = (((Form) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.visibleGlassForm"))
						.getInteriorWidth() - (vertiBars * thickness.doubleValue(Units.INCH))) / (vertiBars + 1);

				Assert.assertEquals(
						(vertiDistance * (vertiBarNo + 1)) + (thickness.doubleValue(Units.INCH) * vertiBarNo) + halfThickness
								+ ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.VISIBLEGLASS_TO_GRILLES)[0].doubleValue(Units.INCH) * -1,
						((Bar[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.splittingBars"))[i].getLine().getX1(), DELTA);
				// Top
				Assert.assertEquals(0d, ((Bar[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.splittingBars"))[i].getLine()
						.getY1(), DELTA);
				// Bottom
				Assert.assertEquals(
						((Form) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.overallGrillesForm")).getInteriorHeight(),
						((Bar[]) propertiesToGet.get("configurable.sash[0].sashSection.advancedSectionGrilles.splittingBars"))[i].getLine().getY2(), DELTA);

				++vertiBarNo;
			} /* end if */
		} /* end for */
	}
}

/****************************************************************************************************************************************************************
 * TEST STUFF
 * 
 * ((CasementVisionPvcSashSection)configurable.getSash()[0].getSashSection()).getAdvancedSectionGrilles().getNoSplittingBars()[0].getLine().getX1()
 * ((CasementVisionPvcSashSection)configurable.getSash()[0].getSashSection()).getAdvancedSectionGrilles().getNoSplittingBars()[0].getLine().getX1()
 * ((CasementVisionPvcSashSection)configurable.getSash()[0].getSashSection()).getAdvancedSectionGrilles().getVisibleGlassForms()[0];
 * ((CasementVisionPvcSashSection)configurable.getSash()[0].getSashSection()).getInsulatedGlass()[0].getRelativeVisibleGlass()
 * ((CasementVisionPvcSashSection)configurable.getSash()[0].getSashSection()).getAdvancedSectionGrilles().getOverallGrillesForm()[0]
 *****************************************************************************************************************************************************************/

