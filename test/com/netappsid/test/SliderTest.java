package com.netappsid.test;

import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.netappsid.erp.configurator.Configurable;
import com.netappsid.erp.configurator.configmanagement.ConfigurationManager;
import com.netappsid.erp.configurator.gui.Configurator;
import com.netappsid.test.common.Common;

public class SliderTest
{
	@BeforeClass
	public static void init() throws Exception
	{}

	@AfterClass
	public static void tearDown()
	{

	}

	/**
	 * Change from 2 to 3 sections
	 */
	@Test
	public void sliderSectionChange()
	{
		Configurable configurable = Configurator.getConfigurableByName("SLP_WOA", null);

		Map<String, String> setProperties = new LinkedHashMap<String, String>();
		Map<String, Object> getProperties = new LinkedHashMap<String, Object>();

		setProperties.put("configurable.nbSections", "2");
		setProperties.put("configurable.dimension.width", "1000 mm");
		setProperties.put("configurable.dimension.height", "500 mm");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, setProperties));

		getProperties.put("configurable.nbSections", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, getProperties));

		Assert.assertEquals(2, (Integer) getProperties.get("configurable.nbSections"), Common.DELTA);

		setProperties.put("configurable.nbSections", "3");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, setProperties));

		getProperties.put("configurable.nbSections", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, getProperties));

		Assert.assertEquals(3, (Integer) getProperties.get("configurable.nbSections"), Common.DELTA);
	}

	/**
	 * Change from 2 to 3 sections
	 */
	@Test
	public void pocketSlider()
	{
		Configurable configurable = Configurator.getConfigurableByName("SLP_WOA", null);

		Map<String, String> setProperties = new LinkedHashMap<String, String>();
		Map<String, Object> getProperties = new LinkedHashMap<String, Object>();

		setProperties.put("configurable.nbSections", "2");
		setProperties.put("configurable.dimension.width", "1000 mm");
		setProperties.put("configurable.dimension.height", "500 mm");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, setProperties));

		getProperties.put("configurable.nbSections", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, getProperties));

		Assert.assertEquals(2, (Integer) getProperties.get("configurable.nbSections"), Common.DELTA);

		setProperties.put("configurable.nbSections", "3");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, setProperties));

		getProperties.put("configurable.nbSections", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, getProperties));

		Assert.assertEquals(3, (Integer) getProperties.get("configurable.nbSections"), Common.DELTA);
	}

}