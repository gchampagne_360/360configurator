package com.netappsid.test;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.client360.configuration.wad.CasementVisionPvc;
import com.netappsid.erp.configurator.ConfigProperty;
import com.netappsid.erp.configurator.ConfiguratorImpl;

public class LabelDescriptionOverrideTest
{
	private static ConfiguratorImpl configurator;

	@BeforeClass
	public static void init() throws Exception
	{
		configurator = new ConfiguratorImpl();
		configurator.build("CVP_WOA");
	}

	@Test
	public void testOverrideLabel()
	{
		final String expectedLabel = "toto123456";
		ConfigProperty property;
		CasementVisionPvc casement;

		// Retrieve casement and ChoiceSwing property
		casement = (CasementVisionPvc) configurator.getDefaultConfigurable().getObject();
		property = casement.getProperty(CasementVisionPvc.PROPERTYNAME_CHOICESWING);

		// Test initial label
		Assert.assertNotEquals(expectedLabel, property.getDisplayName()); //$NON-NLS-1$

		// Override label
		casement.overrideLabel(CasementVisionPvc.PROPERTYNAME_CHOICESWING, expectedLabel);

		// Test overridden label
		Assert.assertEquals(expectedLabel, property.getDisplayName());

		// Serialization & deserialization
		byte[] serialization = configurator.serialize();
		ConfiguratorImpl newConfigurator = new ConfiguratorImpl();
		newConfigurator.deserialize(serialization);

		// Retrieve new casement and ChoiceSwing property
		casement = (CasementVisionPvc) newConfigurator.getDefaultConfigurable().getObject();
		property = casement.getProperty(CasementVisionPvc.PROPERTYNAME_CHOICESWING);

		// Test overridden label
		Assert.assertEquals(expectedLabel, property.getDisplayName());
	}

	@Test
	public void testOverrideDescription()
	{
		final String expectedDescription = "toto123456qwertyuiop";
		ConfigProperty property;
		CasementVisionPvc casement;

		// Retrieve casement and ChoiceSwing property
		casement = (CasementVisionPvc) configurator.getDefaultConfigurable().getObject();
		property = casement.getProperty(CasementVisionPvc.PROPERTYNAME_CHOICESWING);

		// Test initial label
		Assert.assertNotEquals(expectedDescription, property.getDescription()); //$NON-NLS-1$

		// Override label
		casement.overrideDescription(CasementVisionPvc.PROPERTYNAME_CHOICESWING, expectedDescription);

		// Test overridden label
		Assert.assertEquals(expectedDescription, property.getDescription());

		// Serialization & deserialization
		byte[] serialization = configurator.serialize();
		ConfiguratorImpl newConfigurator = new ConfiguratorImpl();
		newConfigurator.deserialize(serialization);

		// Retrieve new casement and ChoiceSwing property
		casement = (CasementVisionPvc) newConfigurator.getDefaultConfigurable().getObject();
		property = casement.getProperty(CasementVisionPvc.PROPERTYNAME_CHOICESWING);

		// Test overridden label
		Assert.assertEquals(expectedDescription, property.getDescription());
	}

}
