package com.netappsid.test;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;

import com.client360.configuration.wad.BayBow;
import com.netappsid.commonutils.math.Units;
import com.netappsid.erp.configurator.configmanagement.ConfigurationManager;
import com.netappsid.test.common.Common;

public class BayBow3SectionsBase
{

	int precision = 3;
	Double width;
	Double height;
	Double side;
	Double center;
	Double interiorProjection;
	Double extIntProjectionDifference;
	Double extension;
	Double angle;
	Double frameThickness;
	Double xBmThickness;
	Double yBmThickness;
	BayBow baybow;

	Double exteriorCenter;
	Double interiorCenter;
	Double exteriorSide;
	Double interiorSide;
	Double exteriorProjection;

	Double sideDifference;

	Map<String, String> setProperties = new LinkedHashMap<String, String>();
	Map<String, Object> getProperties = new LinkedHashMap<String, Object>();

	@BeforeClass
	public static void init() throws Exception
	{}

	@AfterClass
	public static void tearDown()
	{

	}

	public BayBow3SectionsBase()
	{
		super();
	}

	protected void setConstant()
	{
		// FIXME BayBow
		// baybow = (BayBow) Configurator.getConfigurableByName("BB_WOA", null);
		// baybow.getAdvancedBayBowCalculator().setConstant(extension, frameThickness, xBmThickness, yBmThickness);

		setProperties.put("configurable.nbSections", "3");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(baybow, setProperties));
		setProperties.clear();
	}

	protected void setAngle()
	{
		setProperties.put("configurable.bayBowSpecs.angle", ((Double) Math.toDegrees(angle)).toString());
	}

	protected void setWidth()
	{
		setProperties.put("configurable.dimension.width", width.toString() + "in");
	}

	private void setHeight()
	{
		setProperties.put("configurable.dimension.height", height.toString() + "in");
	}

	protected void setInteriorProjection()
	{
		setProperties.put("configurable.bayBowSpecs.interiorProjection", interiorProjection.toString() + "in");
	}

	protected void setSide()
	{
		setProperties.put("configurable.bayBowSpecs.side", side.toString() + "in");
	}

	protected void setCenter()
	{
		setProperties.put("configurable.bayBowSpecs.center", center.toString() + "in");
	}

	protected void setAutoCadVar1()
	{
		width = 100.0;
		height = 60.0;
		side = 30.0;
		center = 43.10546;
		interiorProjection = 15.125;
		angle = Math.toRadians(30.0);

		frameThickness = 4.0;
		extension = 0.25;

		exteriorCenter = 45.74906;
		interiorCenter = 43.60546;
		exteriorSide = 31.32180;
		interiorSide = 30.25;
		sideDifference = 1.07180;
		extIntProjectionDifference = 0.5359;
		exteriorProjection = 15.6609;
	}

	protected void setAutoCadVar2()
	{
		width = 80.0;
		height = 60.0;
		side = 20.0;
		center = 45.20532;
		interiorProjection = 14.31891;
		angle = Math.toRadians(45.0);

		frameThickness = 4.0;
		extension = 0.25;

		exteriorCenter = 49.0190;
		interiorCenter = 45.70532;
		exteriorSide = 21.90685;
		interiorSide = 20.25;
		sideDifference = 1.65685;
		extIntProjectionDifference = 1.171575;
		exteriorProjection = 15.490485;
	}

	private void setAutoCadVar3()
	{
		width = 160.0;
		height = 60.0;
		side = 10.0;
		center = 137.41413;
		interiorProjection = 6.31906;
		angle = Math.toRadians(37.0);

		frameThickness = 4.0;
		extension = 0.5;

		exteriorCenter = 141.09090;
		interiorCenter = 138.41413;
		exteriorSide = 11.83838;
		interiorSide = 10.5;
		sideDifference = 1.33838;
		extIntProjectionDifference = 0.80545;
		exteriorProjection = 7.12451;
	}

	public void calculation()
	{
		// exteriorCenterCalc();
		// interiorCenterCalc();
		// exteriorSideCalc();
		// interiorSideCalc();
		// sideDifferenceCalc();
		exteriorProjectionCalculation();
		// extIntProjectionDifferenceCalculation();

	}

	public void calculationCore()
	{
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(baybow, setProperties));
		getProperties.clear();

		centerCalculation();
		sideCalculation();
		interiorProjectionCalculation();
		angleCalculation();

	}

	public void exteriorProjectionCalculation()
	{
		getProperties.put("configurable.bayBowSpecs.exteriorProjection", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(baybow, getProperties));
		Assert.assertEquals(exteriorProjection, ((Measure<Length>) getProperties.get("configurable.bayBowSpecs.exteriorProjection")).doubleValue(Units.INCH),
				Common.DELTA);
	}

	public void centerCalculation()
	{
		getProperties.put("configurable.bayBowSpecs.center", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(baybow, getProperties));
		Assert.assertEquals(center, ((Measure<Length>) getProperties.get("configurable.bayBowSpecs.center")).doubleValue(Units.INCH), Common.DELTA);
	}

	public void sideCalculation()
	{
		getProperties.put("configurable.bayBowSpecs.side", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(baybow, getProperties));
		Assert.assertEquals(side, ((Measure<Length>) getProperties.get("configurable.bayBowSpecs.side")).doubleValue(Units.INCH), Common.DELTA);
	}

	public void interiorProjectionCalculation()
	{
		getProperties.put("configurable.bayBowSpecs.interiorProjection", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(baybow, getProperties));
		Assert.assertEquals(interiorProjection, ((Measure<Length>) getProperties.get("configurable.bayBowSpecs.interiorProjection")).doubleValue(Units.INCH),
				Common.DELTA);
	}

	public void angleCalculation()
	{
		getProperties.put("configurable.bayBowSpecs.angle", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(baybow, getProperties));
		Assert.assertEquals(angle, Math.toRadians((Double) getProperties.get("configurable.bayBowSpecs.angle")), Common.DELTA);
	}

}