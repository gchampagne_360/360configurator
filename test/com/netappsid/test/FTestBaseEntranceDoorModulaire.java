package com.netappsid.test;

import java.util.HashMap;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.netappsid.commonutils.math.Units;
import com.netappsid.erp.configurator.gui.Configurator;
import com.netappsid.test.common.Common;
import com.netappsid.wadconfigurator.Door;
import com.netappsid.wadconfigurator.DoorFrame;
import com.netappsid.wadconfigurator.SideLight;
import com.netappsid.wadconfigurator.enums.ChoiceBuildType;
import com.netappsid.wadconfigurator.enums.ChoiceOpening;
import com.netappsid.wadconfigurator.enums.ChoiceSideLightSide;
import com.netappsid.wadconfigurator.enums.ChoiceSwing;

public class FTestBaseEntranceDoorModulaire
{
	private static Map<Boolean, Map<String, String[]>> colorsPrice = new HashMap<Boolean, Map<String, String[]>>();

	@SuppressWarnings("unused")
	private Map<String, String[]> map;
	private String quickLibCode;

	@BeforeClass
	public static void init() throws Exception
	{}

	@AfterClass
	public static void tearDown()
	{
		colorsPrice = null;
	}

	// -------------- 2 DOOR 0 SIDELIGHT---------------

	/**
	 * Standard 2 fixed doors
	 */
	@Test
	public void EntranceDoorCase01()
	{
		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDimensions(configurator, 80d, 80d);
			Common.setDoorNbSlab(configurator, 2);
			Common.setDoorOpening(configurator, ChoiceOpening.FIXED, 0);
			Common.setDoorOpening(configurator, ChoiceOpening.FIXED, 1);
			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLMODULAR);
			Common.setDoorSwingSide(configurator, choiceSwing);

			Assert.assertEquals(Common.getDoorSlabWidth(configurator), 37.25, Common.DELTA);
			Assert.assertEquals(Common.getDoorWidth(configurator), 80.0, Common.DELTA);

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 37.25, Common.DELTA);
				Assert.assertEquals(slab.getSlabHeight().doubleValue(Units.INCH), 78.625, Common.DELTA);
			}

			configurator.dispose();
		}
	}

	/**
	 * Standard 2 operating doors, withOUT astragal
	 */
	@Test
	public void EntranceDoorCase08()
	{
		Configurator configurator = Common.instanciateConfigurable("STD");

		Common.setDoorBuildType(configurator, ChoiceBuildType.FULLMODULAR);
		Common.setDimensions(configurator, 80d, 80d);
		Common.setDoorNbSlab(configurator, 2);
		Common.setDoorSwingSide(configurator, ChoiceSwing.INSWING);
		Common.setDoorOpening(configurator, ChoiceOpening.LEFT, 0);
		Common.setDoorOpening(configurator, ChoiceOpening.RIGHT, 1);

		Assert.assertEquals(Common.getDoorSlabWidth(configurator), 37.0, Common.DELTA);
		Assert.assertEquals(Common.getDoorWidth(configurator), 80.0, Common.DELTA);

		for (Door slab : Common.getDoorSlabs(configurator))
		{
			Assert.assertNotNull(slab);
			Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 37.0, Common.DELTA);
			Assert.assertEquals(slab.getSlabHeight().doubleValue(Units.INCH), 78.625, Common.DELTA);
		}

		configurator.dispose();
	}

	/**
	 * Standard 2 operating doors, with astragal
	 */
	@Test
	public void EntranceDoorCase09()
	{
		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDimensions(configurator, 80d, 80d);
			Common.setDoorNbSlab(configurator, 2);

			// LEFT SLAB ASTRAGAL
			Common.setDoorOpening(configurator, ChoiceOpening.LEFTA, 0);
			Common.setDoorOpening(configurator, ChoiceOpening.RIGHT, 1);
			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLMODULAR);
			Common.setDoorSwingSide(configurator, choiceSwing);

			Assert.assertEquals(Common.getDoorSlabWidth(configurator), 38.25, Common.DELTA);
			Assert.assertEquals(Common.getDoorWidth(configurator), 80.0, Common.DELTA);

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 38.25, Common.DELTA);
				Assert.assertEquals(slab.getSlabHeight().doubleValue(Units.INCH), 78.625, Common.DELTA);
			}

			// RIGHT SLAB ASTRAGAL
			Common.setDoorOpening(configurator, ChoiceOpening.LEFT, 0);
			Common.setDoorOpening(configurator, ChoiceOpening.RIGHTA, 1);

			Assert.assertEquals(Common.getDoorSlabWidth(configurator), 38.25, Common.DELTA);
			Assert.assertEquals(Common.getDoorWidth(configurator), 80.0, Common.DELTA);

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 38.25, Common.DELTA);
				Assert.assertEquals(slab.getSlabHeight().doubleValue(Units.INCH), 78.625, Common.DELTA);
			}

			configurator.dispose();

		}
	}

	/**
	 * Standard 2 operating doors, withOUT astragal. Slab width override should change the total width because there is no sidelight
	 */
	@Test
	public void EntranceDoorCase10()
	{
		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLMODULAR);
			Common.setDoorSwingSide(configurator, choiceSwing);
			Common.setDoorHeight(configurator, 80d);
			Common.setDoorNbSlab(configurator, 2);
			Common.setDoorOpening(configurator, ChoiceOpening.LEFT, 0);
			Common.setDoorOpening(configurator, ChoiceOpening.RIGHT, 1);
			Common.setDoorSlabWidth(configurator, 32d);

			Assert.assertEquals(Common.getDoorSlabWidth(configurator), 32.0, Common.DELTA);
			Assert.assertEquals(Common.getDoorWidth(configurator), 70d, Common.DELTA);

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 32.0, Common.DELTA);
				Assert.assertEquals(slab.getSlabHeight().doubleValue(Units.INCH), 78.625, Common.DELTA);
			}

			for (DoorFrame frame : Common.getDoorFrames(configurator))
			{
				Assert.assertNotNull(frame);
				Assert.assertEquals(frame.getDimension().getWidth().doubleValue(Units.INCH), 35.0, Common.DELTA);
				Assert.assertEquals(frame.getDimension().getHeight().doubleValue(Units.INCH), 80, Common.DELTA);
			}

			configurator.dispose();
		}

	}

	/**
	 * Standard 2 right operating doors should have a mullion
	 */
	@Test
	public void EntranceDoorCase11()
	{
		Configurator configurator = Common.instanciateConfigurable("STD");

		Common.setDoorBuildType(configurator, ChoiceBuildType.FULLMODULAR);
		Common.setDoorSwingSide(configurator, ChoiceSwing.INSWING);
		Common.setDimensions(configurator, 80d, 80d);
		Common.setDoorNbSlab(configurator, 2);
		Common.setDoorOpening(configurator, ChoiceOpening.RIGHT, 0);
		Common.setDoorOpening(configurator, ChoiceOpening.RIGHT, 1);

		Assert.assertNotNull(Common.getDoorMullionsFix(configurator));
		Assert.assertEquals(1, Common.getDoorMullionsFix(configurator).length);

		Assert.assertEquals(Common.getDoorWidth(configurator), 80d, Common.DELTA);
		Assert.assertEquals(Common.getDoorSlabWidth(configurator), 37.0, Common.DELTA);

		for (Door slab : Common.getDoorSlabs(configurator))
		{
			Assert.assertNotNull(slab);
			Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 37.0, Common.DELTA);
			Assert.assertEquals(slab.getSlabHeight().doubleValue(Units.INCH), 78.625, Common.DELTA);
		}

		configurator.dispose();
	}

	// -------------- 2 DOOR 1 SIDELIGHT---------------

	/**
	 * 2 fixed door with 1 15" left side light the slab width are calculated
	 */
	@Test
	public void EntranceDoorCase02()
	{
		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDimensions(configurator, 80d, 80d);
			Common.setDoorNbSlab(configurator, 2);
			Common.setDoorOpening(configurator, ChoiceOpening.FIXED, 0);
			Common.setDoorOpening(configurator, ChoiceOpening.FIXED, 1);
			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLMODULAR);
			Common.setDoorNbSideLight(configurator, 1);
			Common.setDoorSwingSide(configurator, choiceSwing);
			Common.setSideLightSide(configurator, ChoiceSideLightSide.LEFTSIDE);
			Common.setDoorSideLightWidth(configurator, 15d);

			Assert.assertEquals(Common.getDoorSlabWidth(configurator), 28.375, Common.DELTA);
			Assert.assertEquals(Common.getDoorWidth(configurator), 80.0, Common.DELTA);

			for (SideLight sl : Common.getDoorSideLights(configurator))
			{
				Assert.assertNotNull(sl);
				Assert.assertEquals(sl.getSlabWidth().doubleValue(Units.INCH), 15.0, Common.DELTA);
			}

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 28.375, Common.DELTA);
			}

			configurator.dispose();

		}
	}

	/**
	 * same things as EntranceDoorCase2 but we change the input sequence.
	 */
	@Test
	public void EntranceDoorCase03()
	{
		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDimensions(configurator, 80d, 80d);
			Common.setDoorNbSlab(configurator, 2);
			Common.setDoorNbSideLight(configurator, 1);
			Common.setDoorSideLightWidth(configurator, 15d);
			Common.setDoorOpening(configurator, ChoiceOpening.FIXED, 0);
			Common.setDoorOpening(configurator, ChoiceOpening.FIXED, 1);
			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLMODULAR);
			Common.setDoorSwingSide(configurator, choiceSwing);
			Common.setSideLightSide(configurator, ChoiceSideLightSide.LEFTSIDE);

			Assert.assertEquals(Common.getDoorSlabWidth(configurator), 28.375, Common.DELTA);
			Assert.assertEquals(Common.getDoorWidth(configurator), 80.0, Common.DELTA);

			for (SideLight sl : Common.getDoorSideLights(configurator))
			{
				Assert.assertNotNull(sl);
				Assert.assertEquals(sl.getSlabWidth().doubleValue(Units.INCH), 15.0, Common.DELTA);
			}

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 28.375, Common.DELTA);
			}

			configurator.dispose();

		}
	}

	/**
	 * 2 32" fixed door with 1 left side light the slab width are calculated
	 * 
	 * We try to put the 23" value before set the number of side light... the door replace the logical value. We set the number of sidelight and set the door
	 * width it should stay at 32"
	 * 
	 */
	@Test
	public void EntranceDoorCase04()
	{
		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDoorNbSlab(configurator, 2);
			Common.setDimensions(configurator, 80d, 80d);
			Common.setDoorOpening(configurator, ChoiceOpening.FIXED, 0);
			Common.setDoorOpening(configurator, ChoiceOpening.FIXED, 1);
			Common.setDoorNbSideLight(configurator, 1);
			Common.setDoorSlabWidth(configurator, 32.0);
			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLMODULAR);
			Common.setDoorSwingSide(configurator, choiceSwing);
			Common.setSideLightSide(configurator, ChoiceSideLightSide.LEFTSIDE);

			Assert.assertEquals(Common.getDoorSlabWidth(configurator), 32.0, Common.DELTA);
			Assert.assertEquals(Common.getDoorWidth(configurator), 80.0, Common.DELTA);
			Assert.assertEquals(Common.getDoorSideLightWidth(configurator), 7.75, Common.DELTA);

			for (SideLight sl : Common.getDoorSideLights(configurator))
			{
				Assert.assertNotNull(sl);
				Assert.assertEquals(sl.getSlabWidth().doubleValue(Units.INCH), 7.75, Common.DELTA);
			}

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 32.0, Common.DELTA);
			}

			configurator.dispose();

		}
	}

	// -------------- 2 DOOR 2 SIDELIGHT---------------

	@Test
	public void EntranceDoorCase05()
	{
		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLMODULAR);
			Common.setDoorNbSlab(configurator, 2);
			Common.setDimensions(configurator, 80d, 80d);
			Common.setDoorNbSideLight(configurator, 2);

			Common.setDoorOpening(configurator, ChoiceOpening.FIXED, 0);
			Common.setDoorOpening(configurator, ChoiceOpening.FIXED, 1);
			Common.setDoorSwingSide(configurator, choiceSwing);
			Common.setDoorSideLightWidth(configurator, 15d);

			Assert.assertEquals(Common.getDoorSlabWidth(configurator), 19.5, Common.DELTA);
			Assert.assertEquals(Common.getDoorWidth(configurator), 80.0, Common.DELTA);

			for (SideLight sl : Common.getDoorSideLights(configurator))
			{
				Assert.assertNotNull(sl);
				Assert.assertEquals(sl.getSlabWidth().doubleValue(Units.INCH), 15, Common.DELTA);
			}

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 19.5, Common.DELTA);
			}

			configurator.dispose();

		}
	}

	// -------------- 1 DOOR---------------

	/**
	 * Simple door with overall width at 34.75
	 */
	@Test
	public void EntranceDoorCase06()
	{

		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDoorNbSlab(configurator, 1);
			Common.setDimensions(configurator, 34.75d, 80d);
			Common.setDoorOpening(configurator, ChoiceOpening.FIXED, 0);
			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLMODULAR);
			Common.setDoorSwingSide(configurator, choiceSwing);

			Assert.assertEquals(Common.getDoorSlabWidth(configurator), 32.0, Common.DELTA);
			Assert.assertEquals(Common.getDoorWidth(configurator), 34.75, Common.DELTA);

			Assert.assertNull(Common.getDoorSideLights(configurator));

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 32.0, Common.DELTA);
			}

			configurator.dispose();

		}
	}

	/**
	 * Simple door with overall width at 34.75 left and right opening
	 */
	@Test
	public void EntranceDoorCase07()
	{

		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{

			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDoorNbSlab(configurator, 1);
			Common.setDimensions(configurator, 34.75d, 80d);
			Common.setDoorSwingSide(configurator, choiceSwing);
			Common.setDoorOpening(configurator, ChoiceOpening.LEFT, 0);
			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLMODULAR);

			// -------LEFT OPENING TEST -------------
			Assert.assertEquals(Common.getDoorSlabWidth(configurator), 31.75, Common.DELTA);
			Assert.assertEquals(Common.getDoorWidth(configurator), 34.75, Common.DELTA);

			Assert.assertNull(Common.getDoorSideLights(configurator));

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 31.75, Common.DELTA);
			}

			// -------RIGHT OPENING TEST -------------
			Common.setDoorOpening(configurator, ChoiceOpening.RIGHT, 0);

			Assert.assertEquals(Common.getDoorSlabWidth(configurator), 31.75, Common.DELTA);
			Assert.assertEquals(Common.getDoorWidth(configurator), 34.75, Common.DELTA);

			Assert.assertNull(Common.getDoorSideLights(configurator));

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 31.75, Common.DELTA);
			}

			configurator.dispose();

		}
	}

	// -------------------- 1 door 2 sidelights
	@Test
	public void EntranceDoorCase12()
	{

		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDoorNbSlab(configurator, 1);
			Common.setDoorNbSideLight(configurator, 2);
			Common.setDimensions(configurator, 80d, 80d);
			Common.setDoorOpening(configurator, ChoiceOpening.LEFT, 0);
			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLMODULAR);
			Common.setDoorSwingSide(configurator, choiceSwing);
			Common.setDoorSideLightWidth(configurator, 15d);

			Assert.assertEquals(Common.getDoorSlabWidth(configurator), 41.5d, Common.DELTA);
			Assert.assertEquals(Common.getDoorWidth(configurator), 80d, Common.DELTA);

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 41.5d, Common.DELTA);
			}

			Common.setDoorNbSideLight(configurator, 0);

			Assert.assertNull(Common.getDoorSideLights(configurator));

			Assert.assertEquals(Common.getDoorSlabWidth(configurator), 77d, Common.DELTA);
			Assert.assertEquals(Common.getDoorWidth(configurator), 80d, Common.DELTA);

			Assert.assertNull(Common.getDoorSideLights(configurator));

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 77d, Common.DELTA);
			}

			configurator.dispose();

		}
	}

	// -------------- 4 DOOR 2 SIDELIGHT---------------

	/**
	 * 4 fixed door with 2 15" left side light the slab width are calculated
	 */
	@Test
	public void EntranceDoorCase13()
	{
		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDimensions(configurator, 158d, 80d);
			Common.setDoorNbSlab(configurator, 4);
			Common.setDoorOpening(configurator, ChoiceOpening.FIXED, 0);
			Common.setDoorOpening(configurator, ChoiceOpening.FIXED, 1);
			Common.setDoorOpening(configurator, ChoiceOpening.FIXED, 2);
			Common.setDoorOpening(configurator, ChoiceOpening.FIXED, 3);
			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLMODULAR);
			Common.setDoorNbSideLight(configurator, 2);
			Common.setDoorSwingSide(configurator, choiceSwing);
			Common.setDoorSideLightWidth(configurator, 15d);

			Assert.assertEquals(Common.getDoorSlabWidth(configurator), 27.875, Common.DELTA);
			Assert.assertEquals(Common.getDoorWidth(configurator), 158d, Common.DELTA);

			for (SideLight sl : Common.getDoorSideLights(configurator))
			{
				Assert.assertNotNull(sl);
				Assert.assertEquals(sl.getSlabWidth().doubleValue(Units.INCH), 15.0, Common.DELTA);
			}

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 27.875, Common.DELTA);
			}

			configurator.dispose();

		}
	}

	// -------------- 4 DOOR 2 SIDELIGHT Modular---------------

	/**
	 * 4 fixed door with 2 15" side light the slab width are calculated. Frame Modular
	 */
	@Test
	public void EntranceDoorCase14()
	{
		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDimensions(configurator, 159.5d, 80d);
			Common.setDoorNbSlab(configurator, 4);
			Common.setDoorOpening(configurator, ChoiceOpening.FIXED, 0);
			Common.setDoorOpening(configurator, ChoiceOpening.FIXED, 1);
			Common.setDoorOpening(configurator, ChoiceOpening.FIXED, 2);
			Common.setDoorOpening(configurator, ChoiceOpening.FIXED, 3);
			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLMODULAR);
			Common.setDoorNbSideLight(configurator, 2);
			Common.setDoorSwingSide(configurator, choiceSwing);
			Common.setDoorSideLightWidth(configurator, 15d);

			Assert.assertEquals(Common.getDoorSlabWidth(configurator), 28.25, Common.DELTA);
			Assert.assertEquals(Common.getDoorWidth(configurator), 159.5d, Common.DELTA);

			for (SideLight sl : Common.getDoorSideLights(configurator))
			{
				Assert.assertNotNull(sl);
				Assert.assertEquals(sl.getSlabWidth().doubleValue(Units.INCH), 15.0, Common.DELTA);
			}

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 28.25, Common.DELTA);
			}

			configurator.dispose();

		}
	}

	/**
	 * 4 opening door with 2 15" side light the slab width are calculated. Frame Modular
	 */
	@Test
	public void EntranceDoorCase15()
	{
		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDimensions(configurator, 159.5d, 80d);
			Common.setDoorNbSlab(configurator, 4);
			Common.setDoorOpening(configurator, ChoiceOpening.LEFT, 0);
			Common.setDoorOpening(configurator, ChoiceOpening.LEFT, 1);
			Common.setDoorOpening(configurator, ChoiceOpening.LEFT, 2);
			Common.setDoorOpening(configurator, ChoiceOpening.RIGHT, 3);
			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLMODULAR);
			Common.setDoorNbSideLight(configurator, 2);
			Common.setDoorSwingSide(configurator, choiceSwing);
			Common.setDoorSideLightWidth(configurator, 15d);

			Assert.assertEquals(Common.getDoorSlabWidth(configurator), 28d, Common.DELTA);
			Assert.assertEquals(Common.getDoorWidth(configurator), 159.5d, Common.DELTA);

			for (SideLight sl : Common.getDoorSideLights(configurator))
			{
				Assert.assertNotNull(sl);
				Assert.assertEquals(sl.getSlabWidth().doubleValue(Units.INCH), 15.0, Common.DELTA);
			}

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 28d, Common.DELTA);
			}

			configurator.dispose();

		}
	}

	/**
	 * 4 opening door with 2 15" side light the slab width are calculated. Frame Modular
	 */
	@Test
	public void EntranceDoorCase16()
	{
		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDimensions(configurator, 159.5d, 80d);
			Common.setDoorNbSlab(configurator, 4);
			Common.setDoorOpening(configurator, ChoiceOpening.LEFT, 0);
			Common.setDoorOpening(configurator, ChoiceOpening.LEFTA, 1);
			Common.setDoorOpening(configurator, ChoiceOpening.RIGHT, 2);
			Common.setDoorOpening(configurator, ChoiceOpening.RIGHT, 3);
			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLMODULAR);
			Common.setDoorNbSideLight(configurator, 2);
			Common.setDoorSwingSide(configurator, choiceSwing);
			Common.setDoorSideLightWidth(configurator, 15d);

			Assert.assertEquals(Common.getDoorSlabWidth(configurator), 28.625, Common.DELTA);
			Assert.assertEquals(Common.getDoorWidth(configurator), 159.5d, Common.DELTA);

			for (SideLight sl : Common.getDoorSideLights(configurator))
			{
				Assert.assertNotNull(sl);
				Assert.assertEquals(sl.getSlabWidth().doubleValue(Units.INCH), 15.0, Common.DELTA);
			}

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 28.625, Common.DELTA);
			}

			configurator.dispose();

		}
	}

	@Test
	public void EntranceDoorCase17()
	{
		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDimensions(configurator, 159.5d, 80d);
			Common.setDoorNbSlab(configurator, 4);
			Common.setDoorOpening(configurator, ChoiceOpening.LEFT, 0);
			Common.setDoorOpening(configurator, ChoiceOpening.LEFTA, 1);
			Common.setDoorOpening(configurator, ChoiceOpening.RIGHT, 2);
			Common.setDoorOpening(configurator, ChoiceOpening.RIGHT, 3);
			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLMODULAR);
			Common.setDoorNbSideLight(configurator, 1);
			Common.setDoorSwingSide(configurator, choiceSwing);
			Common.setDoorSideLightWidth(configurator, 15d);
			Common.setSideLightSide(configurator, ChoiceSideLightSide.LEFTSIDE);

			Assert.assertEquals(Common.getDoorSlabWidth(configurator), 33.0625, Common.DELTA);
			Assert.assertEquals(Common.getDoorWidth(configurator), 159.5d, Common.DELTA);

			for (SideLight sl : Common.getDoorSideLights(configurator))
			{
				Assert.assertNotNull(sl);
				Assert.assertEquals(sl.getSlabWidth().doubleValue(Units.INCH), 15.0, Common.DELTA);
			}

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 33.0625, Common.DELTA);
			}

			configurator.dispose();

		}
	}

	@Test
	public void EntranceDoorCase18()
	{
		for (ChoiceSwing choiceSwing : ChoiceSwing.values())
		{
			Configurator configurator = Common.instanciateConfigurable("STD");

			Common.setDimensions(configurator, 159.5d, 80d);
			Common.setDoorNbSlab(configurator, 4);
			Common.setDoorOpening(configurator, ChoiceOpening.LEFTA, 0);
			Common.setDoorOpening(configurator, ChoiceOpening.RIGHT, 1);
			Common.setDoorOpening(configurator, ChoiceOpening.FIXED, 2);
			Common.setDoorOpening(configurator, ChoiceOpening.FIXED, 3);
			Common.setDoorBuildType(configurator, ChoiceBuildType.FULLMODULAR);
			Common.setDoorNbSideLight(configurator, 1);
			Common.setDoorSwingSide(configurator, choiceSwing);
			Common.setDoorSideLightWidth(configurator, 15d);
			Common.setSideLightSide(configurator, ChoiceSideLightSide.RIGHTSIDE);

			Assert.assertEquals(Common.getDoorSlabWidth(configurator), 33.1875, Common.DELTA);
			Assert.assertEquals(Common.getDoorWidth(configurator), 159.5d, Common.DELTA);

			for (SideLight sl : Common.getDoorSideLights(configurator))
			{
				Assert.assertNotNull(sl);
				Assert.assertEquals(sl.getSlabWidth().doubleValue(Units.INCH), 15.0, Common.DELTA);
			}

			for (Door slab : Common.getDoorSlabs(configurator))
			{
				Assert.assertNotNull(slab);
				Assert.assertEquals(slab.getSlabWidth().doubleValue(Units.INCH), 33.1875, Common.DELTA);
			}

			configurator.dispose();

		}
	}

}