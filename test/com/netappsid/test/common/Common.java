package com.netappsid.test.common;

import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import org.jscience.physics.measures.Measure;
import org.junit.Assert;

import com.client360.configuration.wad.ProductGroup;
import com.client360.configuration.wad.ProductTemplate;
import com.netappsid.commonutils.geo.Form;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.common.Dimensionnable;
import com.netappsid.erp.configurator.Assembly;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.erp.configurator.configmanagement.ConfigurationManager;
import com.netappsid.erp.configurator.gui.Configurator;
import com.netappsid.erp.configurator.gui.views.PropertiesView;
import com.netappsid.erp.configurator.utils.ConfigurationContext;
import com.netappsid.utils.ConfigFile;
import com.netappsid.wadconfigurator.Casing;
import com.netappsid.wadconfigurator.Combination;
import com.netappsid.wadconfigurator.Door;
import com.netappsid.wadconfigurator.DoorFrame;
import com.netappsid.wadconfigurator.DoorMullionFix;
import com.netappsid.wadconfigurator.EntranceDoor;
import com.netappsid.wadconfigurator.ExteriorMould;
import com.netappsid.wadconfigurator.InteriorMould;
import com.netappsid.wadconfigurator.Opening;
import com.netappsid.wadconfigurator.Product;
import com.netappsid.wadconfigurator.SideLight;
import com.netappsid.wadconfigurator.SillMould;
import com.netappsid.wadconfigurator.enums.ChoiceBuildType;
import com.netappsid.wadconfigurator.enums.ChoiceOpening;
import com.netappsid.wadconfigurator.enums.ChoiceSideLightSide;
import com.netappsid.wadconfigurator.enums.ChoiceSwing;

public class Common
{
	// constantes
	public static final double DELTA = 0.01;
	public static final double DELTA2 = 0.06;
	public static final double DELTA3 = (1d / 64d + 0.0001);

	/**
	 * this method is supposed to be called after any affectation of the configurator
	 */
	public static void refreshPropertiesView(Configurator pconfigurator)
	{
		refreshPropertiesView(pconfigurator, pconfigurator.getDefaultConfigurable());
	}

	public static void refreshPropertiesView(Configurator pconfigurator, Configurable configurable)
	{
		// try
		// {
		PropertiesView propertiesView = pconfigurator.getPropertiesView();

		if (configurable instanceof Assembly)
		{
			// I need to select the assembly manually in the tree of the properties view and fire a modelChanged
			propertiesView.getTree().setSelectionRow(0);
		}
		// Select a configurable in the product matrix and refresh the properties view by firing a modelChanged
		configurable.getProductMatrix().setSelectedConfigurable(configurable);
		propertiesView.modelChanged();
		// }
		// catch (Exception e)
		// {
		// e.printStackTrace();
		// }
	}

	public static Configurator loadConfigurable(String prdCode)
	{
		try
		{
			ConfigFile configFile = new ConfigFile(Thread.currentThread().getContextClassLoader().getResourceAsStream("META-INF/configuration_parameters.xml"));

			configFile.getEntries();

			Configurable configurable = Configurator.getConfigurableByName(prdCode, null);
			// configurable.getProductMatrix().setAssembly((Assembly)configurable);
			configurable.getProductMatrix().setSelectedConfigurable(((Assembly) configurable).getConfigurables().get(0));

			Configurator configurator = new Configurator(new ConfigurationContext(), (Assembly) configurable, "");

			configurator.setModal(false);

			return configurator;

		}
		catch (Exception e)
		{
			throw new RuntimeException("There's an error launching the configurable", e);
		}
	}

	public static Combination getCombination(Configurator configurator)
	{
		return ((Combination) configurator.getMatrix().getAssembly());
	}

	public static InteriorMould getInteriorMould(Configurator configurator)
	{
		return getCombination(configurator).getFrame().getInteriorMould();
	}

	public static Casing getCasing(Configurator configurator)
	{
		return getCombination(configurator).getFrame().getCasing();
	}

	public static SillMould getSillMould(Configurator configurator)
	{
		return getCombination(configurator).getFrame().getSillMould();
	}

	public static EntranceDoor getDefaultConfigurable(Configurator configurator)
	{
		return (EntranceDoor) ((ProductTemplate) ((ProductGroup) configurator.getDefaultConfigurable()).getConfigurables().get(0)).getProduct();
	}

	public static ExteriorMould getExteriorMould(Configurator configurator)
	{
		return getCombination(configurator).getFrame().getExteriorMould();
	}

	public static double getTotalPrice(Configurator configurator)
	{
		double price = 0d;
		for (com.netappsid.erp.configurator.Configurable config : configurator.getMatrix().getConfigurables())
		{
			price += config.getPriceWithOptions();
		}

		if (Common.getCombination(configurator) != null)
		{
			price += Common.getCombination(configurator).getPriceWithOptions();
		}

		return price;
	}

	public static void setDimensions(Configurator configurator, Double width, Double height)
	{
		((Dimensionnable) Common.getDefaultConfigurable(configurator)).getDimension().setWidth(Measure.valueOf(width, Units.INCH));
		Common.refreshPropertiesView(configurator);

		((Dimensionnable) Common.getDefaultConfigurable(configurator)).getDimension().setHeight(Measure.valueOf(height, Units.INCH));
		Common.refreshPropertiesView(configurator);
	}

	public static void setDimensions(Configurator configurator, Double width, Double height, Double height2)
	{
		((Product) Common.getDefaultConfigurable(configurator)).getDimension().setWidth(Measure.valueOf(width, Units.INCH));
		Common.refreshPropertiesView(configurator);

		((Product) Common.getDefaultConfigurable(configurator)).getDimension().setHeight(Measure.valueOf(height, Units.INCH));
		Common.refreshPropertiesView(configurator);

		((Product) Common.getDefaultConfigurable(configurator)).getDimension().setHeight2(Measure.valueOf(height2, Units.INCH));
		Common.refreshPropertiesView(configurator);
	}

	public static void setWidth(Configurator configurator, Double width)
	{
		((Product) Common.getDefaultConfigurable(configurator)).getDimension().setWidth(Measure.valueOf(width, Units.INCH));
		Common.refreshPropertiesView(configurator);
	}

	public static void setDoorSlabWidth(Configurator configurator, Double width)
	{
		for (Door door : Common.getDefaultConfigurable(configurator).getDoor())
		{
			door.getDimension().setWidth(Units.inch(width));
		}

		Common.refreshPropertiesView(configurator);
	}

	public static void setDoorSideLightWidth(Configurator configurator, Double width)
	{
		for (SideLight sideLight : Common.getDefaultConfigurable(configurator).getSideLight())
		{
			sideLight.getDimension().setWidth(Units.inch(width));
		}

		Common.refreshPropertiesView(configurator);
	}

	public static void setDoorSideLightWidth(Configurator configurator, Double width, int index)
	{

		Common.getDefaultConfigurable(configurator).getSideLight()[index].getDimension().setWidth(Units.inch(width));

		Common.refreshPropertiesView(configurator);
	}

	public static void setDoorOpening(Configurator configurator, ChoiceOpening choiceOpening, int openingNumber)
	{
		Common.getDefaultConfigurable(configurator).getOpening()[openingNumber].setChoiceOpening(choiceOpening);
		Common.refreshPropertiesView(configurator);
	}

	public static void setSideLightSide(Configurator configurator, ChoiceSideLightSide OpeningSide)
	{
		Common.getDefaultConfigurable(configurator).setChoiceSideLightSide(OpeningSide);
		Common.refreshPropertiesView(configurator);
	}

	public static void setDoorSwingSide(Configurator configurator, ChoiceSwing swing)
	{
		Common.getDefaultConfigurable(configurator).setChoiceSwing(swing);
		Common.refreshPropertiesView(configurator);
	}

	public static void setSideliteSwingSide(Configurator configurator, ChoiceSwing swing)
	{
		Common.getDefaultConfigurable(configurator).setChoiceSideLiteSwing(swing);
		Common.refreshPropertiesView(configurator);
	}

	public static void setDoorBuildType(Configurator configurator, ChoiceBuildType choiceSteelDoorBuildType)
	{
		Common.getDefaultConfigurable(configurator).setChoiceBuildType(choiceSteelDoorBuildType);
		Common.refreshPropertiesView(configurator);
	}

	public static void setDoorNbSlab(Configurator configurator, Integer nbDoor)
	{
		Common.getDefaultConfigurable(configurator).setNbDoors(nbDoor);
		Common.refreshPropertiesView(configurator);
	}

	public static void setDoorNbSideLight(Configurator configurator, Integer nbSideLight)
	{
		Common.getDefaultConfigurable(configurator).setNbSideLight(nbSideLight);
		Common.refreshPropertiesView(configurator);
	}

	public static Double getDoorSlabWidth(Configurator configurator)
	{
		return Common.getDefaultConfigurable(configurator).getDoor()[0].getDimension().getWidth().doubleValue(Units.INCH);
	}

	public static Double getDoorSlabGap(Configurator configurator, int doorNumber)
	{
		return Common.getDefaultConfigurable(configurator).getDoor()[doorNumber].getSlabGap().doubleValue(Units.INCH);
	}

	public static String getDoorOpening(Configurator configurator, int doorNumber)
	{
		return Common.getDefaultConfigurable(configurator).getDoor()[doorNumber].getChoiceOpening().toString();
	}

	public static Double getDoorLeftJambWidthOS(Configurator configurator)
	{
		return Common.getDefaultConfigurable(configurator).getJambWidthLeftOS().doubleValue(Units.INCH);
	}

	public static Double getDoorRightJambWidthOS(Configurator configurator)
	{
		return Common.getDefaultConfigurable(configurator).getJambWidthRightOS().doubleValue(Units.INCH);
	}

	public static Double getDoorMullionWidthOS(Configurator configurator)
	{
		return Common.getDefaultConfigurable(configurator).getMullionFixWidthOS().doubleValue(Units.INCH);
	}

	public static Form getDoorSlabForm(Configurator configurator)
	{
		return Common.getDefaultConfigurable(configurator).getDoor()[0].getDimension().getForm();
	}

	public static Double getDoorSideLightWidth(Configurator configurator)
	{
		return Common.getDefaultConfigurable(configurator).getSideLight()[0].getDimension().getWidth().doubleValue(Units.INCH);
	}

	public static Double getDoorWidth(Configurator configurator)
	{
		if (Common.getDefaultConfigurable(configurator).getDimension().getWidth() != null)
		{
			return Common.getDefaultConfigurable(configurator).getDimension().getWidth().doubleValue(Units.INCH);
		}
		else
		{
			return null;
		}
	}

	public static Form getDoorForm(Configurator configurator)
	{
		return Common.getDefaultConfigurable(configurator).getDimension().getForm();
	}

	public static void setDoorForm(Configurator configurator, Form form)
	{
		Common.getDefaultConfigurable(configurator).getDimension().setForm(form);
	}

	public static Form getFrameForm(Configurator configurator)
	{
		return Common.getDefaultConfigurable(configurator).getDoorFrame()[0].getDimension().getForm();
	}

	public static Double getDoorHeight(Configurator configurator)
	{
		return Common.getDefaultConfigurable(configurator).getDimension().getHeight().doubleValue(Units.INCH);
	}

	public static SideLight[] getDoorSideLights(Configurator configurator)
	{
		return Common.getDefaultConfigurable(configurator).getSideLight();
	}

	public static Door[] getDoorSlabs(Configurator configurator)
	{
		return Common.getDefaultConfigurable(configurator).getDoor();
	}

	public static DoorMullionFix[] getDoorMullionsFix(Configurator configurator)
	{
		return Common.getDefaultConfigurable(configurator).getDoorMullionFix();
	}

	public static void addToAssembly(Configurator configurator, String prdCode, Integer col, Integer row)
	{
		configurator.getDefaultConfigurable().getProductMatrix().setConfigurable(col, row, Configurator.getConfigurableByName(prdCode, null));
		Common.refreshPropertiesView(configurator, configurator.getDefaultConfigurable().getAssembly());
	}

	public static Configurable getConfigurableInAssembly(Configurator configurator, int position)
	{
		return configurator.getDefaultConfigurable().getProductMatrix().getConfigurables().get(position);
	}

	public static Configurator instanciateConfigurable(String prdCode)
	{
		Configurator configurator = Common.loadConfigurable(prdCode);

		// configurator.showConfigurator();

		return configurator;
	}

	public static DoorFrame[] getDoorFrames(Configurator configurator)
	{
		return Common.getDefaultConfigurable(configurator).getDoorFrame();
	}

	public static void setDoorOpenings(Configurator configurator, ChoiceOpening[] openingTab)
	{
		for (Opening opening : Common.getDefaultConfigurable(configurator).getOpening())
		{
			opening.setChoiceOpening(openingTab[opening.getIndex()]);
		}

		Common.refreshPropertiesView(configurator);

	}

	public static void setSideliteOpenings(Configurator configurator, ChoiceOpening[] openingTab)
	{
		for (Opening opening : Common.getDefaultConfigurable(configurator).getSideliteOpening())
		{
			opening.setChoiceOpening(openingTab[opening.getIndex()]);
		}

		Common.refreshPropertiesView(configurator);
	}

	public static void setDoorSlabWidth(Configurator configurator, double width, int index)
	{
		Common.getDefaultConfigurable(configurator).getDoor()[index].getDimension().setWidth(Units.inch(width));

		Common.refreshPropertiesView(configurator);

	}

	public static void setSlabDoorHeight(Configurator configurator, double height)
	{
		for (Door door : Common.getDefaultConfigurable(configurator).getDoor())
		{
			door.getDimension().setHeight(Units.inch(height));
		}

		Common.refreshPropertiesView(configurator);

	}

	public static void setDoorHeight(Configurator configurator, double height)
	{
		Common.getDefaultConfigurable(configurator).getDimension().setHeight(Units.inch(height));

		Common.refreshPropertiesView(configurator);

	}

	public static void setWidthCalculateFromSL(Configurator configurator, boolean b)
	{
		Common.getDefaultConfigurable(configurator).setWidthCalculateFromSL(b);

	}

	/**
	 * Set the window minimum parameters.
	 * 
	 * @param configurable
	 *            - Window configurable.
	 * @param nbSections
	 *            - Number of sections.
	 * @param width
	 *            - Window width (inches).
	 * @param height
	 *            - Window height (inches).
	 */
	public static void initialSetup(Configurable configurable, Integer nbSections, Double width, Double height)
	{
		Map<String, String> propertiesToSet = new LinkedHashMap<String, String>();
		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToSet.put("configurable.nbSections", nbSections.toString());
		propertiesToSet.put("configurable.dimension.width", width + " in");
		propertiesToSet.put("configurable.dimension.height", height + " in");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		propertiesToGet.put("configurable.nbSections", "");
		propertiesToGet.put("configurable.dimension.width", "");
		propertiesToGet.put("configurable.dimension.height", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		Assert.assertEquals(nbSections, (Integer) propertiesToGet.get("configurable.nbSections"), Common.DELTA);
		Assert.assertEquals(Measure.valueOf(width, Units.INCH), propertiesToGet.get("configurable.dimension.width"));
		Assert.assertEquals(Measure.valueOf(height, Units.INCH), propertiesToGet.get("configurable.dimension.height"));
	}
}
