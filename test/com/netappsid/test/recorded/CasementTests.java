package com.netappsid.test.recorded;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.client360.configuration.wad.enums.ChoiceAdvancedGrilles;
import com.netappsid.commonutils.math.Units;
import com.netappsid.erp.configurator.configmanagement.ConfigurationTestManager;
import com.netappsid.wadconfigurator.enums.ChoicePresetWindowWidth;

@Ignore
public class CasementTests extends ConfigurationTestManager
{
	ConfigurationTestManagerMode testMode = ConfigurationTestManagerMode.RECORD;
	ConfigurationTestManagerVerboseLevel verbose = ConfigurationTestManagerVerboseLevel.VALUES_ONLY;

	@BeforeClass
	public static void init() throws Exception
	{}

	@AfterClass
	public static void tearDown()
	{}

	@Test
	public void CasementVisionHybrid()
	{
		initialize("CVH");
		setVerbose(verbose);

		addPropertyToSet("assembly.configurable[0].dimension.width", Units.inch(66d));
		addPropertyToSet("assembly.configurable[0].dimension.height", Units.inch(66d));
		addPropertyToSet("assembly.configurable[0].nbSections", 1);

		execute(testMode);
	}

	@Test
	public void CasementVisionHybrid_Special()
	{
		initialize("CVH");
		setVerbose(verbose);

		addPropertyToSet("assembly.configurable[0].dimension.width", Units.inch(66d));
		addPropertyToSet("assembly.configurable[0].dimension.height", Units.inch(66d));
		addPropertyToSet("assembly.configurable[0].nbSections", 1);

		execute(testMode);
	}

	@Override
	public void addDynamicProperties()
	{
		// Limited list of properties only for the special test case.
		if (getTestName().equals("CasementVisionHybrid_Special"))
		{
			clearPropertiesToVerify();
			addPropertyToVerify("assembly.configurable[0].dimension.width");
			addPropertyToVerify("assembly.configurable[0].dimension.height");
		} /* end if */
		else
		{
			super.addDynamicProperties();
		} /* end else */
	}

	@Test
	public void CasementVisionHybrid_Sections()
	{
		initialize("CVH");
		setVerbose(verbose);

		addPropertyToSet("assembly.configurable[0].dimension.width", Units.inch(66d));
		addPropertyToSet("assembly.configurable[0].dimension.height", Units.inch(66d));
		addPropertyToSet("assembly.configurable[0].nbSections", 1);
		addPropertyToSet("assembly.configurable[0].nbSections", 2);

		addPropertyToSet("assembly.configurable[0].choicePresetWindowWidth", ChoicePresetWindowWidth.D13_23.toString());
		addPropertyToSet("assembly.configurable[0].choicePresetWindowWidth", ChoicePresetWindowWidth.D14_34.toString());
		addPropertyToSet("assembly.configurable[0].choicePresetWindowWidth", ChoicePresetWindowWidth.D23_13.toString());
		addPropertyToSet("assembly.configurable[0].choicePresetWindowWidth", ChoicePresetWindowWidth.D34_14.toString());
		addPropertyToSet("assembly.configurable[0].choicePresetWindowWidth", ChoicePresetWindowWidth.OTHER.toString());
		addPropertyToSet("assembly.configurable[0].sectionInput[0].sectionWidth", Units.inch(20));
		addPropertyToSet("assembly.configurable[0].choicePresetWindowWidth.remove_Override", "");
		addPropertyToSet("assembly.configurable[0].dimension.width", Units.inch(80d));
		addPropertyToSet("assembly.configurable[0].dimension.width", Units.inch(66d));

		addPropertyToSet("assembly.configurable[0].nbSections", 3);
		addPropertyToSet("assembly.configurable[0].choicePresetWindowWidth", ChoicePresetWindowWidth.D14_12_14.toString());
		addPropertyToSet("assembly.configurable[0].choicePresetWindowWidth", ChoicePresetWindowWidth.OTHER.toString());
		addPropertyToSet("assembly.configurable[0].sectionInput[0].sectionWidth", Units.inch(20));
		addPropertyToSet("assembly.configurable[0].sectionInput[1].sectionWidth", Units.inch(12));
		addPropertyToSet("assembly.configurable[0].choicePresetWindowWidth.remove_Override", "");
		addPropertyToSet("assembly.configurable[0].dimension.width", Units.inch(80d));
		addPropertyToSet("assembly.configurable[0].dimension.width", Units.inch(66d));

		addPropertyToSet("assembly.configurable[0].nbSections", 4);
		addPropertyToSet("assembly.configurable[0].choicePresetWindowWidth", ChoicePresetWindowWidth.OTHER.toString());
		addPropertyToSet("assembly.configurable[0].sectionInput[3].sectionWidth", Units.inch(15));
		addPropertyToSet("assembly.configurable[0].sectionInput[1].sectionWidth", Units.inch(15));
		addPropertyToSet("assembly.configurable[0].choicePresetWindowWidth.remove_Override", "");
		addPropertyToSet("assembly.configurable[0].dimension.width", Units.inch(80d));
		addPropertyToSet("assembly.configurable[0].dimension.width", Units.inch(66d));

		execute(testMode);
	}

	@Test
	public void CasementVisionHybrid_Grilles()
	{
		initialize("CVH");
		setVerbose(verbose);

		addPropertyToSet("assembly.configurable[0].dimension.width", Units.inch(66d));
		addPropertyToSet("assembly.configurable[0].dimension.height", Units.inch(66d));
		addPropertyToSet("assembly.configurable[0].nbSections", 1);
		addPropertyToSet("assembly.configurable[0].sash[0].sashSection.insulatedGlass[0].choiceAdvancedGrilles", ChoiceAdvancedGrilles.REGULAR);
		addPropertyToSet("assembly.configurable[0].sash[0].sashSection.insulatedGlass[0].choiceAdvancedGrilles", ChoiceAdvancedGrilles.CONTOUR);
		addPropertyToSet("assembly.configurable[0].sash[0].sashSection.insulatedGlass[0].choiceAdvancedGrilles", ChoiceAdvancedGrilles.PARTIALREGULAR);
		addPropertyToSet("assembly.configurable[0].sash[0].sashSection.insulatedGlass[0].choiceAdvancedGrilles", ChoiceAdvancedGrilles.HUBSANDSPOKES);
		addPropertyToSet("assembly.configurable[0].sash[0].sashSection.insulatedGlass[0].choiceAdvancedGrilles", ChoiceAdvancedGrilles.PERIMETER);
		addPropertyToSet("assembly.configurable[0].sash[0].sashSection.insulatedGlass[0].choiceAdvancedGrilles", ChoiceAdvancedGrilles.LADDER);
		addPropertyToSet("assembly.configurable[0].sash[0].sashSection.insulatedGlass[0].choiceAdvancedGrilles", ChoiceAdvancedGrilles.EMPRESSPATTERN);
		addPropertyToSet("assembly.configurable[0].sash[0].sashSection.insulatedGlass[0].choiceAdvancedGrilles.remove_Override", "");

		execute(testMode);
	}

	@Test
	public void CasementVisionHybrid_TDL()
	{
		initialize("CVH");
		setVerbose(verbose);

		addPropertyToSet("assembly.configurable[0].dimension.width", Units.inch(66d));
		addPropertyToSet("assembly.configurable[0].dimension.height", Units.inch(66d));
		addPropertyToSet("assembly.configurable[0].nbSections", 1);
		addPropertyToSet("assembly.configurable[0].sash[0].sashSection.choiceAdvancedGrilles", ChoiceAdvancedGrilles.MUNTIN);
		addPropertyToSet("assembly.configurable[0].sash[0].sashSection.advancedSectionGrilles.exteriorColor.choiceColor.code.EQUAL", "VA");
		addPropertyToSet("assembly.configurable[0].sash[0].sashSection.advancedSectionGrilles.nbSdlHorizBars", 0);
		addPropertyToSet("assembly.configurable[0].sash[0].sashSection.advancedSectionGrilles.nbSdlVertiBars", 0);
		addPropertyToSet("assembly.configurable[0].sash[0].sashSection.advancedSectionGrilles.nbTdlHorizBars", 1);
		addPropertyToSet("assembly.configurable[0].sash[0].sashSection.advancedSectionGrilles.nbTdlVertiBars", 1);
		addPropertyToSet("assembly.configurable[0].sash[0].sashSection.advancedSectionGrilles.nbTdlHorizBars", 0);
		addPropertyToSet("assembly.configurable[0].sash[0].sashSection.advancedSectionGrilles.exteriorColor.choiceColor.RESET_DE_FILTER", "");
		addPropertyToSet("assembly.configurable[0].sash[0].sashSection.advancedSectionGrilles.exteriorColor.choiceColor.code.EQUAL", "BC");
		addPropertyToSet("assembly.configurable[0].sash[0].sashSection.advancedSectionGrilles.exteriorColor.choiceColor.RESTORE_DE_FILTER", "");
		addPropertyToSet("assembly.configurable[0].sash[0].sashSection.choiceAdvancedGrilles.remove_Override", "");

		execute(testMode);
	}

}