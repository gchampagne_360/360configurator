package com.netappsid.test;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.client360.configuration.wad.CasementVisionPvc;
import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.netappsid.commonutils.math.Units;
import com.netappsid.erp.configurator.configmanagement.ConfigurationManager;
import com.netappsid.erp.configurator.gui.Configurator;
import com.netappsid.test.common.Common;

/**
 * SectionInput tests.
 * 
 * @author sboule
 */
public class SectionInputTest
{
	private static CasementVisionPvc configurable;
	private static Double width = 100d;
	private static Double height = 55d;
	private static ProductData productData = new ProductData();

	@BeforeClass
	public static void init() throws Exception
	{
		System.out.println("*********************************************** Starting tests");
		configurable = (CasementVisionPvc) Configurator.getConfigurableByName("CVP_WOA", null);
	}

	@AfterClass
	public static void tearDown()
	{
		System.out.println("*********************************************** Tests ended");
	}

	@Test
	/**
	 *  Single section test.
	 *  SectionInput must be hidden.
	 */
	public void singleSection()
	{
		Common.initialSetup(configurable, 1, width, height);

		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToGet.put("configurable.sectionInput.isVisible", "");
		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));
		Assert.assertFalse((Boolean) propertiesToGet.get("configurable.sectionInput.isVisible"));
	}

	@Test
	/**
	 *  Double section test with a symmetric window and sash.
	 *  SectionInput must be shown.
	 *  Both visible glass sides are equal.
	 */
	public void doubleSectionSymmetricEqual()
	{
		setProductDataSymmetric();
		Common.initialSetup(configurable, 1, width, height);
		Common.initialSetup(configurable, 2, width, height);

		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToGet.put("configurable.sectionInput.isVisible", "");

		for (int i = 0; i < 2; i++)
		{
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth", "");
			propertiesToGet.put("configurable.sectionInput[" + i + "].isReadOnly", "");
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth.isReadOnly", "");
		} /* end for */

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));
		Assert.assertTrue((Boolean) propertiesToGet.get("configurable.sectionInput.isVisible"));

		for (int i = 0; i < 2; i++)
		{
			Assert.assertEquals(50d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + i + "].sectionWidth")).doubleValue(Units.INCH),
					Common.DELTA);
			Assert.assertTrue((Boolean) propertiesToGet.get("configurable.sectionInput[" + i + "].isReadOnly"));
			Assert.assertFalse((Boolean) propertiesToGet.get("configurable.sectionInput[" + i + "].sectionWidth.isReadOnly"));
		} /* end for */

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
	}

	@Test
	/**
	 *  Double section test with a symmetric window and sash.
	 *  SectionInput must be shown.
	 *  Left visible glass side is larger.
	 */
	public void doubleSectionSymmetricUnequal1()
	{
		setProductDataSymmetric();
		Common.initialSetup(configurable, 1, width, height);
		Common.initialSetup(configurable, 2, width, height);

		Map<String, String> propertiesToSet = new LinkedHashMap<String, String>();
		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToSet.put("configurable.sectionInput[0].sectionWidth", "40 in");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		propertiesToGet.put("configurable.sectionInput.isVisible", "");

		for (int i = 0; i < 2; i++)
		{
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth", "");
			propertiesToGet.put("configurable.sectionInput[" + i + "].isReadOnly", "");
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth.isReadOnly", "");
		} /* end for */

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		// Section 0
		Assert.assertEquals(40d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH), Common.DELTA);
		Assert.assertTrue((Boolean) propertiesToGet.get("configurable.sectionInput[0].isReadOnly"));
		Assert.assertFalse((Boolean) propertiesToGet.get("configurable.sectionInput[0].sectionWidth.isReadOnly"));

		// Section 1
		Assert.assertEquals(60d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH), Common.DELTA);
		Assert.assertTrue((Boolean) propertiesToGet.get("configurable.sectionInput[1].isReadOnly"));
		Assert.assertTrue((Boolean) propertiesToGet.get("configurable.sectionInput[1].sectionWidth.isReadOnly"));

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
	}

	@Test
	/**
	 *  Double section test with a symmetric window and sash.
	 *  SectionInput must be shown.
	 *  Right visible glass side is larger.
	 */
	public void doubleSectionSymmetricUnequal2()
	{
		setProductDataSymmetric();
		Common.initialSetup(configurable, 1, width, height);
		Common.initialSetup(configurable, 2, width, height);

		Map<String, String> propertiesToSet = new LinkedHashMap<String, String>();
		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToSet.put("configurable.sectionInput[1].sectionWidth", "45 in");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		propertiesToGet.put("configurable.sectionInput.isVisible", "");

		for (int i = 0; i < 2; i++)
		{
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth", "");
			propertiesToGet.put("configurable.sectionInput[" + i + "].isReadOnly", "");
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth.isReadOnly", "");
		} /* end for */

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		// Section 0
		Assert.assertEquals(55d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH), Common.DELTA);
		Assert.assertTrue((Boolean) propertiesToGet.get("configurable.sectionInput[0].isReadOnly"));
		Assert.assertTrue((Boolean) propertiesToGet.get("configurable.sectionInput[0].sectionWidth.isReadOnly"));

		// Section 1
		Assert.assertEquals(45d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH), Common.DELTA);
		Assert.assertTrue((Boolean) propertiesToGet.get("configurable.sectionInput[1].isReadOnly"));
		Assert.assertFalse((Boolean) propertiesToGet.get("configurable.sectionInput[1].sectionWidth.isReadOnly"));

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
	}

	@Test
	/**
	 *  The mullions are aligned on the visible glass.
	 *  Double section test with an asymmetric window and sash.
	 *  Left frame and sash is larger than right side.
	 *  SectionInput must be shown.
	 *  Both visible glass sides are equal.
	 */
	public void doubleSectionAsymmetric1_Equal()
	{
		// Set the asymmetric frame and sash. Left side larger.
		setProductDataLargerLeft();

		double visibleWidth = width
				- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[3].doubleValue(Units.INCH)
				- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[3].doubleValue(Units.INCH)
				- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[1].doubleValue(Units.INCH)
				- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[1].doubleValue(Units.INCH)
				- ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.MULLION_WIDTH).doubleValue(Units.INCH);

		double leftSectionWidth = visibleWidth / 2d
				+ ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[3].doubleValue(Units.INCH)
				+ ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[3].doubleValue(Units.INCH)
				+ ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.MULLION_WIDTH).doubleValue(Units.INCH) / 2d;

		double rightSectionWidth = visibleWidth / 2d + ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.MULLION_WIDTH).doubleValue(Units.INCH)
				/ 2d + ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[1].doubleValue(Units.INCH)
				+ ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[1].doubleValue(Units.INCH);

		Common.initialSetup(configurable, 1, width, height);
		Common.initialSetup(configurable, 2, width, height);

		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToGet.put("configurable.sectionInput.isVisible", "");

		for (int i = 0; i < 2; i++)
		{
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth", "");
			propertiesToGet.put("configurable.sectionInput[" + i + "].isReadOnly", "");
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth.isReadOnly", "");
		} /* end for */

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));
		Assert.assertTrue((Boolean) propertiesToGet.get("configurable.sectionInput.isVisible"));

		for (int i = 0; i < 2; i++)
		{
			if (i == 0)
			{
				Assert.assertEquals(leftSectionWidth,
						((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + i + "].sectionWidth")).doubleValue(Units.INCH), Common.DELTA);
			} /* end if */
			else
			{
				Assert.assertEquals(rightSectionWidth,
						((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + i + "].sectionWidth")).doubleValue(Units.INCH), Common.DELTA);
			} /* end else */
			Assert.assertTrue((Boolean) propertiesToGet.get("configurable.sectionInput[" + i + "].isReadOnly"));
			Assert.assertFalse((Boolean) propertiesToGet.get("configurable.sectionInput[" + i + "].sectionWidth.isReadOnly"));
		} /* end for */

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
	}

	@Test
	/**
	 *  The mullions are aligned on the visible glass.
	 *  Double section test with an asymmetric window and sash.
	 *  Left frame and sash is larger than right side.
	 *  SectionInput must be shown.
	 *  Left visible glass side is larger.
	 */
	public void doubleSectionAsymmetric1_Unequal1()
	{
		// Set the asymmetric frame and sash. Left side larger.
		setProductDataLargerLeft();

		Common.initialSetup(configurable, 1, width, height);
		Common.initialSetup(configurable, 2, width, height);

		Map<String, String> propertiesToSet = new LinkedHashMap<String, String>();
		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToSet.put("configurable.sectionInput[0].sectionWidth", "40 in");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		propertiesToGet.put("configurable.sectionInput.isVisible", "");

		for (int i = 0; i < 2; i++)
		{
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth", "");
			propertiesToGet.put("configurable.sectionInput[" + i + "].isReadOnly", "");
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth.isReadOnly", "");
		} /* end for */

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));
		Assert.assertTrue((Boolean) propertiesToGet.get("configurable.sectionInput.isVisible"));

		Assert.assertEquals(40d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH), Common.DELTA);
		Assert.assertTrue((Boolean) propertiesToGet.get("configurable.sectionInput[0].isReadOnly"));
		Assert.assertFalse((Boolean) propertiesToGet.get("configurable.sectionInput[0].sectionWidth.isReadOnly"));

		Assert.assertEquals(60d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH), Common.DELTA);
		Assert.assertTrue((Boolean) propertiesToGet.get("configurable.sectionInput[1].isReadOnly"));
		Assert.assertTrue((Boolean) propertiesToGet.get("configurable.sectionInput[1].sectionWidth.isReadOnly"));

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);

		// Set back to default and test.
		double visibleWidth = width
				- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[3].doubleValue(Units.INCH)
				- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[3].doubleValue(Units.INCH)
				- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[1].doubleValue(Units.INCH)
				- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[1].doubleValue(Units.INCH)
				- ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.MULLION_WIDTH).doubleValue(Units.INCH);

		double leftSectionWidth = visibleWidth / 2d
				+ ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[3].doubleValue(Units.INCH)
				+ ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[3].doubleValue(Units.INCH)
				+ ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.MULLION_WIDTH).doubleValue(Units.INCH) / 2d;

		double rightSectionWidth = visibleWidth / 2d + ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.MULLION_WIDTH).doubleValue(Units.INCH)
				/ 2d + ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[1].doubleValue(Units.INCH)
				+ ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[1].doubleValue(Units.INCH);

		propertiesToSet = new LinkedHashMap<String, String>();
		propertiesToSet.put("configurable.sectionInput[0].sectionWidth.REMOVE_OVERRIDE", "");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		Assert.assertEquals(leftSectionWidth, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(rightSectionWidth, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
	}

	@Test
	/**
	 *  The mullions are aligned on the visible glass.
	 *  Double section test with an asymmetric window and sash.
	 *  Left frame and sash is larger than right side.
	 *  SectionInput must be shown.
	 *  Right visible glass side is larger.
	 */
	public void doubleSectionAsymmetric1_Unequal2()
	{
		// Set the asymmetric frame and sash. Left side larger.
		setProductDataLargerLeft();

		Common.initialSetup(configurable, 1, width, height);
		Common.initialSetup(configurable, 2, width, height);

		Map<String, String> propertiesToSet = new LinkedHashMap<String, String>();
		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToSet.put("configurable.sectionInput[1].sectionWidth", "40 in");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		propertiesToGet.put("configurable.sectionInput.isVisible", "");

		for (int i = 0; i < 2; i++)
		{
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth", "");
			propertiesToGet.put("configurable.sectionInput[" + i + "].isReadOnly", "");
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth.isReadOnly", "");
		} /* end for */

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));
		Assert.assertTrue((Boolean) propertiesToGet.get("configurable.sectionInput.isVisible"));

		Assert.assertEquals(60d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH), Common.DELTA);
		Assert.assertTrue((Boolean) propertiesToGet.get("configurable.sectionInput[0].isReadOnly"));
		Assert.assertTrue((Boolean) propertiesToGet.get("configurable.sectionInput[0].sectionWidth.isReadOnly"));

		Assert.assertEquals(40d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH), Common.DELTA);
		Assert.assertTrue((Boolean) propertiesToGet.get("configurable.sectionInput[1].isReadOnly"));
		Assert.assertFalse((Boolean) propertiesToGet.get("configurable.sectionInput[1].sectionWidth.isReadOnly"));

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);

		// Set back to default and test.
		double visibleWidth = width
				- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[3].doubleValue(Units.INCH)
				- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[3].doubleValue(Units.INCH)
				- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[1].doubleValue(Units.INCH)
				- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[1].doubleValue(Units.INCH)
				- ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.MULLION_WIDTH).doubleValue(Units.INCH);

		double leftSectionWidth = visibleWidth / 2d
				+ ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[3].doubleValue(Units.INCH)
				+ ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[3].doubleValue(Units.INCH)
				+ ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.MULLION_WIDTH).doubleValue(Units.INCH) / 2d;

		double rightSectionWidth = visibleWidth / 2d + ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.MULLION_WIDTH).doubleValue(Units.INCH)
				/ 2d + ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[1].doubleValue(Units.INCH)
				+ ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[1].doubleValue(Units.INCH);

		propertiesToSet = new LinkedHashMap<String, String>();
		propertiesToSet.put("configurable.sectionInput[1].sectionWidth.REMOVE_OVERRIDE", "");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		Assert.assertEquals(leftSectionWidth, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(rightSectionWidth, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
	}

	@Test
	/**
	 *  The mullions are aligned on the visible glass.
	 *  Double section test with an asymmetric window and sash.
	 *  Right frame and sash is larger than left side.
	 *  SectionInput must be shown.
	 *  Both visible glass sides are equal.
	 */
	public void doubleSectionAsymmetric2_Equal()
	{
		// Set the asymmetric frame and sash. Left side larger.
		setProductDataLargerRight();

		double visibleWidth = width
				- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[3].doubleValue(Units.INCH)
				- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[3].doubleValue(Units.INCH)
				- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[1].doubleValue(Units.INCH)
				- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[1].doubleValue(Units.INCH)
				- ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.MULLION_WIDTH).doubleValue(Units.INCH);

		double leftSectionWidth = visibleWidth / 2d
				+ ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[3].doubleValue(Units.INCH)
				+ ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[3].doubleValue(Units.INCH)
				+ ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.MULLION_WIDTH).doubleValue(Units.INCH) / 2d;

		double rightSectionWidth = visibleWidth / 2d + ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.MULLION_WIDTH).doubleValue(Units.INCH)
				/ 2d + ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[1].doubleValue(Units.INCH)
				+ ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[1].doubleValue(Units.INCH);

		Common.initialSetup(configurable, 1, width, height);
		Common.initialSetup(configurable, 2, width, height);

		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToGet.put("configurable.sectionInput.isVisible", "");

		for (int i = 0; i < 2; i++)
		{
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth", "");
			propertiesToGet.put("configurable.sectionInput[" + i + "].isReadOnly", "");
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth.isReadOnly", "");
		} /* end for */

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));
		Assert.assertTrue((Boolean) propertiesToGet.get("configurable.sectionInput.isVisible"));

		for (int i = 0; i < 2; i++)
		{
			if (i == 0)
			{
				Assert.assertEquals(leftSectionWidth,
						((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + i + "].sectionWidth")).doubleValue(Units.INCH), Common.DELTA);
			} /* end if */
			else
			{
				Assert.assertEquals(rightSectionWidth,
						((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + i + "].sectionWidth")).doubleValue(Units.INCH), Common.DELTA);
			} /* end else */
			Assert.assertTrue((Boolean) propertiesToGet.get("configurable.sectionInput[" + i + "].isReadOnly"));
			Assert.assertFalse((Boolean) propertiesToGet.get("configurable.sectionInput[" + i + "].sectionWidth.isReadOnly"));
		} /* end for */

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
	}

	@Test
	/**
	 *  The mullions are aligned on the visible glass.
	 *  Double section test with an asymmetric window and sash.
	 *  Right frame and sash is larger than left side.
	 *  SectionInput must be shown.
	 *  Left visible glass side is larger.
	 */
	public void doubleSectionAsymmetric2_Unequal1()
	{
		// Set the asymmetric frame and sash. Left side larger.
		setProductDataLargerRight();

		Common.initialSetup(configurable, 1, width, height);
		Common.initialSetup(configurable, 2, width, height);

		Map<String, String> propertiesToSet = new LinkedHashMap<String, String>();
		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToSet.put("configurable.sectionInput[0].sectionWidth", "40 in");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		propertiesToGet.put("configurable.sectionInput.isVisible", "");

		for (int i = 0; i < 2; i++)
		{
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth", "");
			propertiesToGet.put("configurable.sectionInput[" + i + "].isReadOnly", "");
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth.isReadOnly", "");
		} /* end for */

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));
		Assert.assertTrue((Boolean) propertiesToGet.get("configurable.sectionInput.isVisible"));

		Assert.assertEquals(40d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH), Common.DELTA);
		Assert.assertTrue((Boolean) propertiesToGet.get("configurable.sectionInput[0].isReadOnly"));
		Assert.assertFalse((Boolean) propertiesToGet.get("configurable.sectionInput[0].sectionWidth.isReadOnly"));

		Assert.assertEquals(60d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH), Common.DELTA);
		Assert.assertTrue((Boolean) propertiesToGet.get("configurable.sectionInput[1].isReadOnly"));
		Assert.assertTrue((Boolean) propertiesToGet.get("configurable.sectionInput[1].sectionWidth.isReadOnly"));

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);

		// Set back to default and test.
		propertiesToSet = new LinkedHashMap<String, String>();
		propertiesToSet.put("configurable.sectionInput[0].sectionWidth.REMOVE_OVERRIDE", "");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		double visibleWidth = width
				- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[3].doubleValue(Units.INCH)
				- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[3].doubleValue(Units.INCH)
				- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[1].doubleValue(Units.INCH)
				- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[1].doubleValue(Units.INCH)
				- ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.MULLION_WIDTH).doubleValue(Units.INCH);

		double leftSectionWidth = visibleWidth / 2d
				+ ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[3].doubleValue(Units.INCH)
				+ ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[3].doubleValue(Units.INCH)
				+ ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.MULLION_WIDTH).doubleValue(Units.INCH) / 2d;

		double rightSectionWidth = visibleWidth / 2d + ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.MULLION_WIDTH).doubleValue(Units.INCH)
				/ 2d + ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[1].doubleValue(Units.INCH)
				+ ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[1].doubleValue(Units.INCH);

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));
		Assert.assertEquals(leftSectionWidth, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(rightSectionWidth, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
	}

	@Test
	/**
	 *  The mullions are aligned on the visible glass.
	 *  Double section test with an asymmetric window and sash.
	 *  Right frame and sash is larger than left side.
	 *  SectionInput must be shown.
	 *  Right visible glass side is larger.
	 */
	public void doubleSectionAsymmetric2_Unequal2()
	{
		// Set the asymmetric frame and sash. Left side larger.
		setProductDataLargerRight();

		Common.initialSetup(configurable, 1, width, height);
		Common.initialSetup(configurable, 2, width, height);

		Map<String, String> propertiesToSet = new LinkedHashMap<String, String>();
		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToSet.put("configurable.sectionInput[1].sectionWidth", "40 in");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		propertiesToGet.put("configurable.sectionInput.isVisible", "");

		for (int i = 0; i < 2; i++)
		{
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth", "");
			propertiesToGet.put("configurable.sectionInput[" + i + "].isReadOnly", "");
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth.isReadOnly", "");
		} /* end for */

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));
		Assert.assertTrue((Boolean) propertiesToGet.get("configurable.sectionInput.isVisible"));

		Assert.assertEquals(60d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH), Common.DELTA);
		Assert.assertTrue((Boolean) propertiesToGet.get("configurable.sectionInput[0].isReadOnly"));
		Assert.assertTrue((Boolean) propertiesToGet.get("configurable.sectionInput[0].sectionWidth.isReadOnly"));

		Assert.assertEquals(40d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH), Common.DELTA);
		Assert.assertTrue((Boolean) propertiesToGet.get("configurable.sectionInput[1].isReadOnly"));
		Assert.assertFalse((Boolean) propertiesToGet.get("configurable.sectionInput[1].sectionWidth.isReadOnly"));

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);

		// Set back to default and test.
		propertiesToSet = new LinkedHashMap<String, String>();
		propertiesToSet.put("configurable.sectionInput[1].sectionWidth.REMOVE_OVERRIDE", "");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		double visibleWidth = width
				- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[3].doubleValue(Units.INCH)
				- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[3].doubleValue(Units.INCH)
				- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[1].doubleValue(Units.INCH)
				- ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[1].doubleValue(Units.INCH)
				- ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.MULLION_WIDTH).doubleValue(Units.INCH);

		double leftSectionWidth = visibleWidth / 2d
				+ ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[3].doubleValue(Units.INCH)
				+ ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[3].doubleValue(Units.INCH)
				+ ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.MULLION_WIDTH).doubleValue(Units.INCH) / 2d;

		double rightSectionWidth = visibleWidth / 2d + ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.MULLION_WIDTH).doubleValue(Units.INCH)
				/ 2d + ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS)[1].doubleValue(Units.INCH)
				+ ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID)[1].doubleValue(Units.INCH);

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));
		Assert.assertEquals(leftSectionWidth, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(rightSectionWidth, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
	}

	@Test
	/**
	 *  Triple section test with a symmetric window and sash.
	 *  All visible glass sides are equal.
	 */
	public void tripleSectionSymmetricEqual()
	{
		setProductDataSymmetric();
		Common.initialSetup(configurable, 1, width, height);
		Common.initialSetup(configurable, 3, width, height);

		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		for (int i = 0; i < 3; i++)
		{
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth", "");
		} /* end for */

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		Assert.assertEquals(33.5604d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 0 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(32.8793d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 1 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(33.5604d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 2 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[2].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
	}

	@Test
	/**
	 *  Triple section test with a symmetric window and sash.
	 *  Left SectionInput is changed.
	 *  Left and right visible glass sides are larger and center is smaller. 
	 */
	public void tripleSectionSymmetricUnequal1()
	{
		setProductDataSymmetric();
		Common.initialSetup(configurable, 1, width, height);
		Common.initialSetup(configurable, 3, width, height);

		Map<String, String> propertiesToSet = new LinkedHashMap<String, String>();
		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToSet.put("configurable.sectionInput[0].sectionWidth", "40 in");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		for (int i = 0; i < 3; i++)
		{
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth", "");
		} /* end for */

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		// Section 0
		Assert.assertEquals(40d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);

		// Section 1
		Assert.assertEquals(20d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);

		// Section 2
		Assert.assertEquals(40d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[2].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[2].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);

		// Set back to default and retest.
		propertiesToSet = new LinkedHashMap<String, String>();
		propertiesToSet.put("configurable.sectionInput[0].sectionWidth.REMOVE_OVERRIDE", "");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		Assert.assertEquals(33.5604d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 0 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(32.8793d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 1 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(33.5604d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 2 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[2].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
	}

	@Test
	/**
	 *  Triple section test with a symmetric window and sash.
	 *  Right SectionInput is changed.
	 *  Left and right visible glass sides are larger and center is smaller. 
	 */
	public void tripleSectionSymmetricUnequal2()
	{
		setProductDataSymmetric();
		Common.initialSetup(configurable, 1, width, height);
		Common.initialSetup(configurable, 3, width, height);

		Map<String, String> propertiesToSet = new LinkedHashMap<String, String>();
		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToSet.put("configurable.sectionInput[2].sectionWidth", "40 in");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		for (int i = 0; i < 3; i++)
		{
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth", "");
		} /* end for */

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		// Section 0
		Assert.assertEquals(40d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);

		// Section 1
		Assert.assertEquals(20d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);

		// Section 2
		Assert.assertEquals(40d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[2].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[2].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);

		// Set back to default and retest.
		propertiesToSet = new LinkedHashMap<String, String>();
		propertiesToSet.put("configurable.sectionInput[2].sectionWidth.REMOVE_OVERRIDE", "");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		Assert.assertEquals(33.5604d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 0 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(32.8793d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 1 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(33.5604d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 2 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[2].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
	}

	@Test
	/**
	 *  Triple section test with a symmetric window and sash.
	 *  Center SectionInput is changed.
	 *  Left and right visible glass sides are larger and center is smaller. 
	 */
	public void tripleSectionSymmetricUnequal3()
	{
		setProductDataSymmetric();
		Common.initialSetup(configurable, 1, width, height);
		Common.initialSetup(configurable, 3, width, height);

		Map<String, String> propertiesToSet = new LinkedHashMap<String, String>();
		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToSet.put("configurable.sectionInput[1].sectionWidth", "20 in");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		for (int i = 0; i < 3; i++)
		{
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth", "");
		} /* end for */

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		// Section 0
		Assert.assertEquals(40d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);

		// Section 1
		Assert.assertEquals(20d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);

		// Section 2
		Assert.assertEquals(40d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[2].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[2].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);

		// Set back to default and retest.
		propertiesToSet = new LinkedHashMap<String, String>();
		propertiesToSet.put("configurable.sectionInput[1].sectionWidth.REMOVE_OVERRIDE", "");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		Assert.assertEquals(33.5604d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 0 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(32.8793d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 1 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(33.5604d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 2 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[2].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
	}

	@Test
	/**
	 *  Triple section test with a symmetric window and sash.
	 *  Left and center SectionInput are changed.
	 *  All sections are changed.
	 *  Right section is read only. 
	 */
	public void tripleSectionSymmetricUnequal4()
	{
		setProductDataSymmetric();
		Common.initialSetup(configurable, 1, width, height);
		Common.initialSetup(configurable, 3, width, height);

		Map<String, String> propertiesToSet = new LinkedHashMap<String, String>();
		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToSet.put("configurable.sectionInput[0].sectionWidth", "40 in");
		propertiesToSet.put("configurable.sectionInput[1].sectionWidth", "30 in");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		for (int i = 0; i < 3; i++)
		{
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth", "");
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth.isReadOnly", "");
		} /* end for */

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		// Section 0
		Assert.assertEquals(40d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
		Assert.assertFalse((Boolean) propertiesToGet.get("configurable.sectionInput[0].sectionWidth.isReadOnly"));

		// Section 1
		Assert.assertEquals(30d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
		Assert.assertFalse((Boolean) propertiesToGet.get("configurable.sectionInput[1].sectionWidth.isReadOnly"));

		// Section 2
		Assert.assertEquals(30d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[2].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
		Assert.assertTrue((Boolean) propertiesToGet.get("configurable.sectionInput[2].sectionWidth.isReadOnly"));

		// Set back to default and retest.
		propertiesToSet = new LinkedHashMap<String, String>();
		propertiesToSet.put("configurable.sectionInput[0].sectionWidth.REMOVE_OVERRIDE", "");
		propertiesToSet.put("configurable.sectionInput[1].sectionWidth.REMOVE_OVERRIDE", "");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[2].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
		Assert.assertFalse((Boolean) propertiesToGet.get("configurable.sectionInput[0].sectionWidth.isReadOnly"));
		Assert.assertFalse((Boolean) propertiesToGet.get("configurable.sectionInput[1].sectionWidth.isReadOnly"));
		Assert.assertFalse((Boolean) propertiesToGet.get("configurable.sectionInput[2].sectionWidth.isReadOnly"));
	}

	@Test
	/**
	 *  Quadruple section test with a symmetric window and sash.
	 *  All visible glass sides are equal.
	 */
	public void quadSectionSymmetricEqual()
	{
		setProductDataSymmetric();
		Common.initialSetup(configurable, 1, width, height);
		Common.initialSetup(configurable, 4, width, height);

		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		for (int i = 0; i < 4; i++)
		{
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth", "");
		} /* end for */

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		Assert.assertEquals(25.3406d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 0 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(24.6594d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 1 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(24.6594d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 2 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(25.3406d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 3 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[2].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[3].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
	}

	@Test
	/**
	 *  Quadruple section test with a symmetric window and sash.
	 *  Left SectionInput is changed.
	 *  Left and right visible glass sides are larger, sections 2 and 3 are smaller. 
	 */
	public void quadSectionSymmetricUnequal1()
	{
		setProductDataSymmetric();
		Common.initialSetup(configurable, 1, width, height);
		Common.initialSetup(configurable, 4, width, height);

		Map<String, String> propertiesToSet = new LinkedHashMap<String, String>();
		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToSet.put("configurable.sectionInput[0].sectionWidth", "30 in");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		for (int i = 0; i < 4; i++)
		{
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth", "");
		} /* end for */

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		// Section 0
		Assert.assertEquals(30d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);

		// Section 1
		Assert.assertEquals(20d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);

		// Section 2
		Assert.assertEquals(20d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[2].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);

		// Section 3
		Assert.assertEquals(30d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[3].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[2].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[3].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);

		// Set back to default and retest.
		propertiesToSet = new LinkedHashMap<String, String>();
		propertiesToSet.put("configurable.sectionInput[0].sectionWidth.REMOVE_OVERRIDE", "");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		Assert.assertEquals(25.3406d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 0 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(24.6594d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 1 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(24.6594d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 2 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(25.3406d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 3 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[2].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[3].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
	}

	@Test
	/**
	 *  Quadruple section test with a symmetric window and sash.
	 *  Right SectionInput is changed.
	 *  Left and right visible glass sides are larger, sections 2 and 3 are smaller. 
	 */
	public void quadSectionSymmetricUnequal2()
	{
		setProductDataSymmetric();
		Common.initialSetup(configurable, 1, width, height);
		Common.initialSetup(configurable, 4, width, height);

		Map<String, String> propertiesToSet = new LinkedHashMap<String, String>();
		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToSet.put("configurable.sectionInput[3].sectionWidth", "30 in");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		for (int i = 0; i < 4; i++)
		{
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth", "");
		} /* end for */

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		// Section 0
		Assert.assertEquals(30d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);

		// Section 1
		Assert.assertEquals(20d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);

		// Section 2
		Assert.assertEquals(20d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[2].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);

		// Section 3
		Assert.assertEquals(30d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[3].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[2].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[3].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);

		// Set back to default and retest.
		propertiesToSet = new LinkedHashMap<String, String>();
		propertiesToSet.put("configurable.sectionInput[3].sectionWidth.REMOVE_OVERRIDE", "");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		Assert.assertEquals(25.3406d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 0 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(24.6594d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 1 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(24.6594d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 2 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(25.3406d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 3 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[2].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[3].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
	}

	@Test
	/**
	 *  Quadruple section test with a symmetric window and sash.
	 *  SectionInput 2 is changed (second).
	 *  Left and right visible glass sides stay the same, sections 2 is larger and section 3 is smaller. 
	 */
	public void quadSectionSymmetricUnequal3()
	{
		setProductDataSymmetric();
		Common.initialSetup(configurable, 1, width, height);
		Common.initialSetup(configurable, 4, width, height);

		Map<String, String> propertiesToSet = new LinkedHashMap<String, String>();
		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToSet.put("configurable.sectionInput[1].sectionWidth", "30 in");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		for (int i = 0; i < 4; i++)
		{
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth", "");
		} /* end for */

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		Assert.assertEquals(25.3406d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 0 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(30d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 1 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(19.3189d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 2 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(25.3406d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 3 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[2].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[3].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);

		// Set back to default and retest.
		propertiesToSet = new LinkedHashMap<String, String>();
		propertiesToSet.put("configurable.sectionInput[1].sectionWidth.REMOVE_OVERRIDE", "");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		Assert.assertEquals(25.3406d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 0 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(24.6594d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 1 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(24.6594d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 2 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(25.3406d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 3 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[2].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[3].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
	}

	@Test
	/**
	 *  Quadruple section test with a symmetric window and sash.
	 *  SectionInput 3 is changed (third).
	 *  Left and right visible glass sides stay the same, sections 2 is smaller and section 3 is larger. 
	 */
	public void quadSectionSymmetricUnequal4()
	{
		setProductDataSymmetric();
		Common.initialSetup(configurable, 1, width, height);
		Common.initialSetup(configurable, 4, width, height);

		Map<String, String> propertiesToSet = new LinkedHashMap<String, String>();
		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToSet.put("configurable.sectionInput[2].sectionWidth", "30 in");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		for (int i = 0; i < 4; i++)
		{
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth", "");
		} /* end for */

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		Assert.assertEquals(25.3406d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 0 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(19.3189d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 1 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(30d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 2 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(25.3406d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 3 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[2].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[3].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);

		// Set back to default and retest.
		propertiesToSet = new LinkedHashMap<String, String>();
		propertiesToSet.put("configurable.sectionInput[2].sectionWidth.REMOVE_OVERRIDE", "");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		Assert.assertEquals(25.3406d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 0 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(24.6594d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 1 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(24.6594d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 2 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);
		Assert.assertEquals(25.3406d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[" + 3 + "].sectionWidth")).doubleValue(Units.INCH),
				Common.DELTA);

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[2].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[3].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
	}

	@Test
	/**
	 *  Quadruple section test with a symmetric window and sash.
	 *  SectionInput 1, 2 and 3 are changed.
	 *  All sections are changed. 
	 */
	public void quadSectionSymmetricUnequal5()
	{
		setProductDataSymmetric();
		Common.initialSetup(configurable, 1, width, height);
		Common.initialSetup(configurable, 4, width, height);

		Map<String, String> propertiesToSet = new LinkedHashMap<String, String>();
		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToSet.put("configurable.sectionInput[0].sectionWidth", "20 in");
		propertiesToSet.put("configurable.sectionInput[1].sectionWidth", "20 in");
		propertiesToSet.put("configurable.sectionInput[2].sectionWidth", "20 in");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		for (int i = 0; i < 4; i++)
		{
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth", "");
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth.isReadOnly", "");
		} /* end for */

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		// Section 0
		Assert.assertEquals(20d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
		Assert.assertFalse((Boolean) propertiesToGet.get("configurable.sectionInput[0].sectionWidth.isReadOnly"));

		// Section 1
		Assert.assertEquals(20d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
		Assert.assertFalse((Boolean) propertiesToGet.get("configurable.sectionInput[1].sectionWidth.isReadOnly"));

		// Section 2
		Assert.assertEquals(20d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[2].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
		Assert.assertFalse((Boolean) propertiesToGet.get("configurable.sectionInput[2].sectionWidth.isReadOnly"));

		// Section 3
		Assert.assertEquals(40d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[3].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
		Assert.assertTrue((Boolean) propertiesToGet.get("configurable.sectionInput[3].sectionWidth.isReadOnly"));

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[2].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[3].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);

		// Set back to default and retest.
		propertiesToSet = new LinkedHashMap<String, String>();
		propertiesToSet.put("configurable.sectionInput[0].sectionWidth.REMOVE_OVERRIDE", "");
		propertiesToSet.put("configurable.sectionInput[1].sectionWidth.REMOVE_OVERRIDE", "");
		propertiesToSet.put("configurable.sectionInput[2].sectionWidth.REMOVE_OVERRIDE", "");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		for (int i = 0; i < 4; i++)
		{
			Assert.assertFalse((Boolean) propertiesToGet.get("configurable.sectionInput[" + i + "].sectionWidth.isReadOnly"));
		} /* end for */

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[2].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[3].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
	}

	@Test
	/**
	 *  Quadruple section test with a symmetric window and sash.
	 *  SectionInput 4, 3 and 2 are changed.
	 *  All sections are changed. 
	 */
	public void quadSectionSymmetricUnequal6()
	{
		setProductDataSymmetric();
		Common.initialSetup(configurable, 1, width, height);
		Common.initialSetup(configurable, 4, width, height);

		Map<String, String> propertiesToSet = new LinkedHashMap<String, String>();
		Map<String, Object> propertiesToGet = new LinkedHashMap<String, Object>();

		propertiesToSet.put("configurable.sectionInput[3].sectionWidth", "20 in");
		propertiesToSet.put("configurable.sectionInput[2].sectionWidth", "20 in");
		propertiesToSet.put("configurable.sectionInput[1].sectionWidth", "20 in");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		for (int i = 0; i < 4; i++)
		{
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth", "");
			propertiesToGet.put("configurable.sectionInput[" + i + "].sectionWidth.isReadOnly", "");
		} /* end for */

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		// Section 0
		Assert.assertEquals(40d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
		Assert.assertTrue((Boolean) propertiesToGet.get("configurable.sectionInput[0].sectionWidth.isReadOnly"));

		// Section 1
		Assert.assertEquals(20d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
		Assert.assertFalse((Boolean) propertiesToGet.get("configurable.sectionInput[1].sectionWidth.isReadOnly"));

		// Section 2
		Assert.assertEquals(20d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[2].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
		Assert.assertFalse((Boolean) propertiesToGet.get("configurable.sectionInput[2].sectionWidth.isReadOnly"));

		// Section 3
		Assert.assertEquals(20d, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[3].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
		Assert.assertFalse((Boolean) propertiesToGet.get("configurable.sectionInput[3].sectionWidth.isReadOnly"));

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[2].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[3].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);

		// Set back to default and retest.
		propertiesToSet = new LinkedHashMap<String, String>();
		propertiesToSet.put("configurable.sectionInput[1].sectionWidth.REMOVE_OVERRIDE", "");
		propertiesToSet.put("configurable.sectionInput[2].sectionWidth.REMOVE_OVERRIDE", "");
		propertiesToSet.put("configurable.sectionInput[3].sectionWidth.REMOVE_OVERRIDE", "");
		Assert.assertTrue(ConfigurationManager.insertConfigurableProperties(configurable, propertiesToSet));

		Assert.assertTrue(ConfigurationManager.getConfigurableProperties(configurable, propertiesToGet));

		for (int i = 0; i < 4; i++)
		{
			Assert.assertFalse((Boolean) propertiesToGet.get("configurable.sectionInput[" + i + "].sectionWidth.isReadOnly"));
		} /* end for */

		Assert.assertEquals(width, ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[0].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[1].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[2].sectionWidth")).doubleValue(Units.INCH)
				+ ((Measure<Length>) propertiesToGet.get("configurable.sectionInput[3].sectionWidth")).doubleValue(Units.INCH), Common.DELTA3);
	}

	/**
	 * Set the product data width symmetric on both sides.
	 */
	private void setProductDataSymmetric()
	{
		productData.addSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_BORDER_SPACERS, new Double[] { 64 / 25.4, 64 / 25.4, 64 / 25.4, 64 / 25.4,
				64 / 25.4, 64 / 25.4 });
		productData.addSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID, new Double[] { 30 / 25.4, 30 / 25.4, 30 / 25.4,
				30 / 25.4, 30 / 25.4, 30 / 25.4 });
		productData.addSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_GLASS_OFFSET_SPACERS, new Double[] { 2d, 2d, 2d, 2d, 2d, 2d, 2d, 2d });
	}

	/**
	 * Set the product data width larger on the left side.
	 */
	private void setProductDataLargerLeft()
	{
		productData.addSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_BORDER_SPACERS, new Double[] { 64 / 25.4, 140.2 / 25.4, 64 / 25.4, 64 / 25.4,
				64 / 25.4, 64 / 25.4 });
		productData.addSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID, new Double[] { 30 / 25.4, 102.6 / 25.4, 30 / 25.4,
				30 / 25.4, 30 / 25.4, 30 / 25.4 });
		productData.addSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_GLASS_OFFSET_SPACERS, new Double[] { 2d, 5d, 2d, 2d, 2d, 2d, 2d, 2d });
	}

	/**
	 * Set the product data width larger on the right side.
	 */
	private void setProductDataLargerRight()
	{
		productData.addSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_BORDER_SPACERS, new Double[] { 64 / 25.4, 140.2 / 25.4, 64 / 25.4, 64 / 25.4,
				64 / 25.4, 64 / 25.4 });
		productData.addSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID, new Double[] { 30 / 25.4, 102.6 / 25.4, 30 / 25.4,
				30 / 25.4, 30 / 25.4, 30 / 25.4 });
		productData.addSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_GLASS_OFFSET_SPACERS, new Double[] { 2d, 5d, 2d, 2d, 2d, 2d, 2d, 2d });
	}
}

/*
 * configurable.getSectionInput()[0].setSectionWidth(sectionWidth)
 */
