package com.netappsid.test;

import org.junit.Assert;
import org.junit.Test;

public class BayBow3SectionsTest extends BayBow3SectionsBase
{

	@Test
	public void mixedSpecialCase1()
	{

		setAutoCadVar1();
		setConstant();
		setSide();
		setWidth();
		setCenter();
		calculationCore();

		setAutoCadVar2();
		setSide();
		setAngle();
		setCenter();

		calculationCore();

	}

	// The next method are auto-generate to cover all input possibilities -------------------------------

	@Test
	public void SideIntProjCenter()
	{
		setAutoCadVar1();
		setConstant();
		setSide();
		setInteriorProjection();
		setCenter();
		calculationCore();

	}

	@Test
	public void SideWidthCenter()
	{

		setAutoCadVar1();
		setConstant();
		setSide();
		setWidth();
		setCenter();
		calculationCore();

	}

	@Test
	public void SideAngleCenter()
	{

		setAutoCadVar1();
		setConstant();
		setSide();
		setAngle();
		setCenter();
		calculationCore();
	}

	@Test
	public void SideCenter()
	{
		try
		{

			setAutoCadVar1();
			setConstant();

			setSide();

			setCenter();
			calculationCore();
		}
		catch (Exception e)
		{
			Assert.assertTrue(true);
		}

	}

	@Test
	public void SideWidthIntProj()
	{

		setAutoCadVar1();
		setConstant();

		setSide();

		setWidth();

		setInteriorProjection();
		calculationCore();
	}

	@Test
	public void SideAngleIntProj()
	{
		try
		{

			setAutoCadVar1();
			setConstant();

			setSide();

			setAngle();

			setInteriorProjection();
			calculationCore();

		}
		catch (Exception e)
		{
			Assert.assertTrue(true);
		}
	}

	@Test
	public void SideIntProj()
	{
		try
		{

			setAutoCadVar1();
			setConstant();

			setSide();

			setInteriorProjection();
			calculationCore();
		}
		catch (Exception e)
		{
			Assert.assertTrue(true);
		}

	}

	@Test
	public void SideAngleWidth()
	{

		setAutoCadVar1();
		setConstant();

		setSide();

		setAngle();

		setWidth();
		calculationCore();
	}

	@Test
	public void SideWidth()
	{
		try
		{

			setAutoCadVar1();
			setConstant();

			setSide();

			setWidth();
			calculationCore();
		}
		catch (Exception e)
		{
			Assert.assertTrue(true);
		}
	}

	@Test
	public void SideAngle()
	{
		try
		{

			setAutoCadVar1();
			setConstant();

			setSide();

			setAngle();
			calculationCore();
		}
		catch (Exception e)
		{
			Assert.assertTrue(true);
		}

	}

	@Test
	public void Side()
	{
		try
		{

			setAutoCadVar1();
			setConstant();

			setSide();
			calculationCore();
		}
		catch (Exception e)
		{
			Assert.assertTrue(true);
		}

	}

	@Test
	public void WidthIntProjCenter()
	{

		setAutoCadVar1();
		setConstant();

		setWidth();

		setInteriorProjection();

		setCenter();
		calculationCore();
	}

	@Test
	public void AngleIntProjCenter()
	{

		setAutoCadVar1();
		setConstant();

		setAngle();

		setInteriorProjection();

		setCenter();
		calculationCore();
	}

	@Test
	public void IntProjCenter()
	{
		try
		{

			setAutoCadVar1();
			setConstant();

			setInteriorProjection();

			setCenter();
			calculationCore();
		}
		catch (Exception e)
		{
			Assert.assertTrue(true);
		}

	}

	@Test
	public void AngleWidthCenter()
	{

		setAutoCadVar1();
		setConstant();

		setAngle();

		setWidth();

		setCenter();
		calculationCore();
	}

	@Test
	public void WidthCenter()
	{
		try
		{

			setAutoCadVar1();
			setConstant();

			setWidth();

			setCenter();
			calculationCore();
		}
		catch (Exception e)
		{
			Assert.assertTrue(true);
		}
	}

	@Test
	public void AngleCenter()
	{
		try
		{

			setAutoCadVar1();
			setConstant();

			setAngle();

			setCenter();
			calculationCore();
		}
		catch (Exception e)
		{
			Assert.assertTrue(true);
		}
	}

	@Test
	public void Center()
	{
		try
		{

			setAutoCadVar1();
			setConstant();

			setCenter();
			calculationCore();
		}
		catch (Exception e)
		{
			Assert.assertTrue(true);
		}
	}

	@Test
	public void AngleWidthIntProj()
	{

		setAutoCadVar1();
		setConstant();

		setAngle();

		setWidth();

		setInteriorProjection();
		calculationCore();
	}

	@Test
	public void WidthIntProj()
	{
		try
		{
			setAutoCadVar1();
			setConstant();
			setWidth();
			setInteriorProjection();
			calculationCore();
		}
		catch (Exception e)
		{
			Assert.assertTrue(true);
		}
	}

	@Test
	public void AngleIntProj()
	{
		try
		{

			setAutoCadVar1();
			setConstant();

			setAngle();

			setInteriorProjection();
			calculationCore();
		}
		catch (Exception e)
		{
			Assert.assertTrue(true);
		}
	}

	@Test
	public void IntProj()
	{
		try
		{

			setAutoCadVar1();
			setConstant();

			setInteriorProjection();
			calculationCore();
		}
		catch (Exception e)
		{
			Assert.assertTrue(true);
		}
	}

	@Test
	public void AngleWidth()
	{
		try
		{

			setAutoCadVar1();
			setConstant();

			setAngle();

			setWidth();
			calculationCore();
		}
		catch (Exception e)
		{
			Assert.assertTrue(true);
		}
	}

	@Test
	public void Width()
	{
		try
		{

			setAutoCadVar1();
			setConstant();

			setWidth();
			calculationCore();
		}
		catch (Exception e)
		{
			Assert.assertTrue(true);
		}
	}

	@Test
	public void Angle()
	{
		try
		{

			setAutoCadVar1();
			setConstant();

			setAngle();
			calculationCore();
		}
		catch (Exception e)
		{
			Assert.assertTrue(true);
		}
	}

	// Setter ---------------------------------------------------

	// Validation list ---------------------------------------------------

	// Validation ---------------------------------------------------

	// public void exteriorCenterCalc()
	// {
	// Double result;
	//
	// result = getExteriorCenter();
	//
	// Assert.assertEquals(MathFunction.round(exteriorCenter, precision), MathFunction.round(result, precision));
	// }
	// public void interiorCenterCalc()
	// {
	// Double result;
	//
	// result = getInteriorCenter();
	//
	// Assert.assertEquals(MathFunction.round(interiorCenter, precision), MathFunction.round(result, precision));
	// }
	// public void exteriorSideCalc()
	// {
	// Double result;
	//
	// result = getExteriorSide();
	//
	// Assert.assertEquals(MathFunction.round(exteriorSide, precision), MathFunction.round(result, precision));
	// }
	// public void interiorSideCalc()
	// {
	// Double result;
	//
	// result = getInteriorSide();
	//
	// Assert.assertEquals(MathFunction.round(interiorSide, precision), MathFunction.round(result, precision));
	// }

}