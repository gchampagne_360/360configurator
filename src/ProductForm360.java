import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Locale;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;

import com.client360.configuration.common.productForm.ProductForm;
import com.netappsid.erp.configurator.gui.Configurator;

public class ProductForm360 extends JPanel implements PropertyChangeListener
{
	private final JTabbedPane tabbedPane;
	private final ProductForm advance;
	private Configurator configurator;

	public ProductForm360()
	{
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		advance = new ProductForm();
		advance.addPropertyChangeListener(this);
		advance.setTest(true);
		tabbedPane.add("360 Configurator", advance);
		setLayout(new BorderLayout(2, 2));
		add(tabbedPane, BorderLayout.CENTER);
	}

	public String getValue()
	{
		if (advance.getProductQuickLibraryCode() != null)
		{
			return advance.getProductQuickLibraryCode();
		}
		else
		{
			return advance.getProductCode();
		}
	}

	private static void setDefaultUI()
	{
		try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			// UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
			// LookAndFeelFactory.installJideExtension(LookAndFeelFactory.XERTO_STYLE);
			// LookAndFeelFactory.installJideExtension(LookAndFeelFactory.OFFICE2003_STYLE);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt)
	{}

	public Object getConfiguration()
	{
		return configurator.getDefaultConfigurable();
	}

	public String getProductCode()
	{
		return configurator.getDefaultConfigurable().getName();
	}

	public String getProductNomenclatureCode()
	{
		return null;
	}

	public String getProductQuickLibraryCode()
	{
		return null;
	}

	public static void main(String[] args)
	{
		setDefaultUI();
		Locale.setDefault(Locale.CANADA);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

		JFrame frame = new JFrame();

		int w = frame.getSize().width;
		int h = frame.getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
		frame.setSize(950, 720);
		frame.setLocation(x - frame.getWidth() / 2, y - frame.getHeight() / 2);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		ProductForm360 searchForm = new ProductForm360();

		frame.getContentPane().add(searchForm);
		frame.setVisible(true);
	}
}
