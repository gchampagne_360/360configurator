import com.client360.configuration.wad.Combination;
import com.client360.configuration.wad.ProductGroup;
import com.netappsid.configurator.IConfigurator.ResultCode;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.erp.configurator.ConfiguratorImpl;
import com.netappsid.erp.configurator.model.ProductMatrix;
import com.netappsid.wadconfigurator.Assembly;
import com.netappsid.wadconfigurator.enums.ChoiceForm;
import com.netappsid.wadconfigurator.utils.ConfigurationStarter;

/**
 * @author nhabti
 * @author NetAppsID inc.
 * 
 * @version $Revision: 1.29 $
 */
public class MainClient extends ConfigurationStarter
{

	public static void main(String[] args) throws Exception
	{
		mainLoadProductFromProductCode(CVH());
	}

	protected static void mainLoadProductFromProductCode(Object configurable)
	{
		ConfiguratorImpl configuratorImpl = new ConfiguratorImpl();

		try
		{
			configuratorImpl.build(configurable);
			showConfigurator(configuratorImpl);

			System.exit(0);
		}
		catch (Exception exc)
		{
			exc.printStackTrace();
		}
	}

	/**
	 * @param configuratorImpl
	 * @param outFileName
	 */
	private static void showConfigurator(ConfiguratorImpl configuratorImpl)
	{
		ConfiguratorImpl localConfigurator = configuratorImpl;

		boolean exitLoop = false;
		while (!exitLoop)
		{
			ResultCode out = localConfigurator.show();

			exitLoop = out.equals(ResultCode.CANCEL);
			if (!exitLoop)
			{
				System.out.print("Serialization");
				byte[] serialization = localConfigurator.serialize();

				System.out.println(localConfigurator.getValidationResult());

				System.out.println(" and Deserialization");
				// printConfigurator(configuratorImpl);

				localConfigurator = new ConfiguratorImpl();
				localConfigurator.deserialize(serialization);
			}

		}
	}

	// Casement Vision Hybrid
	static Assembly CVH()
	{
		Assembly assembly = createAssembly();

		ProductGroup prodGroup = new ProductGroup(assembly);
		prodGroup.setForm(ChoiceForm.F02_RECTANGLE);
		prodGroup.loadCasementHybrid();
		addConfigurable(assembly, prodGroup);
		return assembly;
	}

	static Assembly createAssembly()
	{
		Assembly assembly = new Combination(null);
		assembly.setProductMatrix(new ProductMatrix());
		return assembly;
	}

	static Assembly addConfigurable(Assembly assembly, Configurable configurable)
	{
		assembly.getProductMatrix().setDefaultConfigurable(configurable);
		return assembly;
	}
}