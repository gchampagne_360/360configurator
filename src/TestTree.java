import java.util.LinkedList;
import java.util.List;

import com.netappsid.erp.configurator.ConfigProperty;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.erp.configurator.Configurable.ActionOnProperty;
import com.netappsid.erp.configurator.gui.Configurator;

public class TestTree
{

	public static void main(String[] args)
	{
		Configurable conf = Configurator.getConfigurableByName("F_CHIBOOKI", null);

		conf.updateState();

		List<String> addresses = getTree(conf);

		for (String address : addresses)
		{
			System.out.println(address);
		}

		System.exit(0);
	}

	private static List<String> getTree(Configurable conf)
	{
		final List<String> addresses = new LinkedList<String>();

		ActionOnProperty action = new ActionOnProperty()
			{

				@Override
				protected boolean doAction(ConfigProperty configProperty, String address)
				{
					addresses.add(address);
					return true;
				}
			};

		conf.doActionOnModel(true, false, action, null);

		return addresses;
	}
}
