// $Id: HungMainMeetingRail.java,v 1.2 2011-04-27 12:09:13 jddaigle Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.netappsid.erp.configurator.Configurable;

@SuppressWarnings("serial")
public class HungMainMeetingRail extends com.client360.configuration.wad.HungMainMeetingRailBase
{
	public HungMainMeetingRail(Configurable parent)
	{
		super(parent);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Measure<Length> getSashPositionNextRailOffset()
	{
		return ProductData.getValue(ProductType.HUNG_PVC, Data.MEETINGRAIL_POSITION_OFFSET);
	}

	@Override
	public Measure<Length> getSashPositionPreviousRailOffset()
	{
		return ProductData.getValue(ProductType.HUNG_PVC, Data.MEETINGRAIL_POSITION_OFFSET);
	}

	public Measure<Length>[] getGlazingBeadThickness()
	{
		return null;
	}
}
