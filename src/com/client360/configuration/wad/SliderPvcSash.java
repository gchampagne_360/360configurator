// $Id: SliderPvcSash.java,v 1.7 2011-09-07 19:04:33 sboule Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.cad.CadSashSlider;
import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.netappsid.commonutils.geo.CornerType;
import com.netappsid.commonutils.math.Units;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.rendering.common.advanced.Drawing;
import com.netappsid.wadconfigurator.SashSection;

public class SliderPvcSash extends com.client360.configuration.wad.SliderPvcSashBase
{

	public SliderPvcSash(Configurable parent)
	{
		super(parent);
	}

	private CadSashSlider sashCad = new CadSashSlider();

	// Default constructor

	public boolean isInstalledVisible()
	{
		return false;
	}

	@Override
	public void activateListener()
	{
		super.activateListener();
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
	}

	// ---------------------------------------------------------------------------------------------------
	// ----------------------------------C O R N E R T Y P E----------------------------------------------
	// ---------------------------------------------------------------------------------------------------
	@Override
	public CornerType[] getCornerType()
	{
		return ProductData.getCornerType(ProductType.SLIDER_PVC, Data.SASH_CORNERTYPE);
	}

	// ------------------------------------------------------------------------------------------------
	// ----------------------------------B O R D E R S P A C E R S-----------------------------------
	// ------------------------------------------------------------------------------------------------
	@Override
	public Measure<Length>[] getSashFrameThickness()
	{
		Measure[] sashFrameThickness = { Measure.valueOf(5d, Units.MM), Measure.valueOf(2d, Units.INCH), Measure.valueOf(5d, Units.MM),
				Measure.valueOf(2d, Units.INCH) };
		return sashFrameThickness;
	}

	@Override
	public Measure<Length>[] getGlazingBeadOffset()
	{
		return ProductData.getSpacer(ProductType.SLIDER_PVC, Data.INT_SASH9722_TO_GLAZINGBEAD_OFFSET_SPACERS);
	}

	@Override
	public Measure<Length>[] getScreenOffset()
	{
		return ProductData.getSpacer(ProductType.SLIDER_PVC, Data.INT_SASH9722_TO_SCREEN_OFFSET_SPACERS);
	}

	@Override
	public Measure<Length>[] getGlassOffset()
	{
		return ProductData.getSpacer(ProductType.SLIDER_PVC, Data.INT_SASH9722_TO_GLASS_OFFSET_SPACERS);
	}

	@Override
	public Drawing getCad()
	{
		sashCad.hideAllLayers();

		if (getIndex() == 0)
		{
			sashCad.showLayer(CadSashSlider.LAYER_LEFT);

		}
		else if (getIndex() == ((SliderMain) getParent()).getNbSections() - 1)
		{
			sashCad.showLayer(CadSashSlider.LAYER_RIGHT);
		}
		else
		{
			sashCad.showLayer(CadSashSlider.LAYER_BOTTOM);
		}

		return sashCad;
	}

	@Override
	public Measure<Length>[] getGlazingBeadThickness()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length>[] getVisibleGlassOffsetSpacer()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SashSection sashSectionClass()
	{
		return new SliderPvcSashSection(this);
	}
}
