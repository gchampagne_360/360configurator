package com.client360.configuration.wad.profiles;

import com.netappsid.rendering.common.advanced.Profile;

public class ProfileMoulurePanneauA extends Profile
{
	private static final long serialVersionUID = -558343333856045362L;

	public void load()
	{
		addGradientSegment(10, -50, -30);
		addLine(-30);
		addGradientSegment(8, -30, 20);
		addGradientSegment(16, 20);
		addGradientSegment(8, 20, -30);
		addLine(-30);
		addGradientSegment(10, -30, -50);
	}
}
