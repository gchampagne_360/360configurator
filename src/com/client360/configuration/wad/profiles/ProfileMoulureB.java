package com.client360.configuration.wad.profiles;

import com.netappsid.rendering.common.advanced.Profile;

public class ProfileMoulureB extends Profile
{
	private static final long serialVersionUID = 55822093063344906L;

	@Override
	public void load()
	{
		/*
		 * addGradientSegment(10, -50, -30); addLine(-30); addGradientSegment(8, -30, 20); addGradientSegment(16, 20); addGradientSegment(8, 20, -30);
		 * addLine(-30); addGradientSegment(10, -30, -50);
		 */

		addGradientSegment(10, -10, -40);
		addLine(-100);
		addGradientSegment(40, 0);
		addLine(-100);
		addGradientSegment(10, -40, -10);

	}

}
