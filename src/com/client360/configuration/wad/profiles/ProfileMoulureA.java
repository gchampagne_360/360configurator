package com.client360.configuration.wad.profiles;

import com.netappsid.rendering.common.advanced.Profile;

public class ProfileMoulureA extends Profile
{
	private static final long serialVersionUID = -2738972820152443316L;

	@Override
	public void load()
	{
		addGradientSegment(10, -40, -10);
		addLine(-100);
		addGradientSegment(40, 0);
		addLine(-100);
		addGradientSegment(10, -10, -40);
	}

}
