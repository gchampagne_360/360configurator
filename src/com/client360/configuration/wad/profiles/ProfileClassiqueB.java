package com.client360.configuration.wad.profiles;

import com.netappsid.rendering.common.advanced.Profile;

public class ProfileClassiqueB extends Profile
{
	private static final long serialVersionUID = 3703641639603228765L;

	@Override
	public void load()
	{
		addGradientSegment(2, -30, 0);
		addLine(-60);
		addGradientSegment(36, 0, -30);
		addLine(-60);
		addGradientSegment(10, 0);
		addGradientSegment(1.5, 0, 80);
		addGradientSegment(0.5, 80, 0);
		addLine(-60);
		addGradientSegment(1.5, 0, 80);
		addGradientSegment(0.5, 80, 20);
	}
}