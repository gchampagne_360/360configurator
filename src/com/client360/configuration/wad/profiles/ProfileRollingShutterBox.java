package com.client360.configuration.wad.profiles;

import com.netappsid.rendering.common.advanced.Profile;

public class ProfileRollingShutterBox extends Profile
{
	@Override
	protected void load()
	{
		addGradientSegment(1, -50, 0);
		addGradientSegment(1, 0, 35);
		addGradientSegment(4, 35, 0);
		addGradientSegment(4, 0, -50);
	}
}
