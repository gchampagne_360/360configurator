package com.client360.configuration.wad.profiles;

import com.netappsid.rendering.common.advanced.Profile;

public class ProfileClassiqueA extends Profile
{
	@Override
	public void load()
	{
		addGradientSegment(0.5, 20, 80);
		addGradientSegment(1.5, 80, 0);
		addLine(-60);
		addGradientSegment(36, 0, 30);
		addLine(-60);
		addGradientSegment(10, -20);
		addGradientSegment(2, -20, -50);
		addLine(-60);
		addGradientSegment(2, 0, -50);
	}
}