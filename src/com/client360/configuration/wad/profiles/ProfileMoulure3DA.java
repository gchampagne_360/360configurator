package com.client360.configuration.wad.profiles;

import com.netappsid.rendering.common.advanced.Profile;

public class ProfileMoulure3DA extends Profile
{

	private static final long serialVersionUID = -5189201142302050894L;

	public void load()
	{
		addGradientSegment(10, 50);
		addLine(-80);
		addGradientSegment(10, 50);
		addLine(-80);
		addGradientSegment(20, 30, -20);
		addLine(-80);
		addGradientSegment(10, -20, -30);
		addLine(-80);
		addGradientSegment(10, -30, -50);
	}
}
