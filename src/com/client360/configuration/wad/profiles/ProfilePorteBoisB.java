package com.client360.configuration.wad.profiles;

import com.netappsid.rendering.common.advanced.Profile;

public class ProfilePorteBoisB extends Profile
{
	@Override
	public void load()
	{
		addGradientSegment(1.5, 0, 80);
		addGradientSegment(0.5, 80, 20);
		addLine(-60);
		addGradientSegment(10, 0);
		addGradientSegment(1.5, 0, 80);
		addGradientSegment(0.5, 80, 0);
		addLine(-60);
		addGradientSegment(36, 0, -30);
		addLine(-60);
		addGradientSegment(2, -30, 0);
	}
}