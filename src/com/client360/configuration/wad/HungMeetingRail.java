// $Id: HungMeetingRail.java,v 1.5 2011-04-27 12:09:13 jddaigle Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;

public class HungMeetingRail extends com.client360.configuration.wad.HungMeetingRailBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public HungMeetingRail(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	@Override
	public Measure<Length> getSashPositionNextRailOffset()
	{
		return ProductData.getValue(ProductType.HUNG_PVC, Data.MEETINGRAIL_POSITION_OFFSET);
	}

	@Override
	public Measure<Length> getSashPositionPreviousRailOffset()
	{
		return ProductData.getValue(ProductType.HUNG_PVC, Data.MEETINGRAIL_POSITION_OFFSET);
	}
}
