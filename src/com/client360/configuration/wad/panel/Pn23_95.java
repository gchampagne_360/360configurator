package com.client360.configuration.wad.panel;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.netappsid.cad.profile.SteelDoorProfileA;
import com.netappsid.cad.profile.SteelDoorProfileB;
import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class Pn23_95 extends ParametricModel
{

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.INCH;
	}

	@Override
	protected void load()
	{
		addGaps(60d + 1d / 8d, 23d);
		addLineDownOffset(3, patternTop, 10d);

		addFormRectangle(patternLeft, patternRight, patternTop, 3);

		addElementSpace("stainedGlass", left, right, top, sill);

		addSpacer(3, 3, 3, 3);

		addProfile(new SteelDoorProfileA(), new SteelDoorProfileB());

	}

}
