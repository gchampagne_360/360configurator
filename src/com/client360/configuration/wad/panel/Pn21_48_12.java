package com.client360.configuration.wad.panel;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.netappsid.cad.profile.SteelDoorProfileA;
import com.netappsid.cad.profile.SteelDoorProfileB;
import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class Pn21_48_12 extends ParametricModel
{

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.INCH;
	}

	@Override
	protected void load()
	{
		addGaps(5d + 1d / 8d, 21d);
		addLineDownOffset(1, patternTop, 49d);
		addLineDownOffset(2, 1, 3d);
		addLineDownOffset(3, 2, 12d);

		addFormRectangle(patternLeft, patternRight, patternTop, 1);
		addFormRectangle(patternLeft, patternRight, 2, 3);

		addElementSpace("stainedGlass", left, right, top, sill);

		addSpacer(3, 3, 3, 3);
		addSpacer(3, 3, 3, 3);

		addProfile(new SteelDoorProfileA(), new SteelDoorProfileB());
		addProfile(new SteelDoorProfileA(), new SteelDoorProfileB());

	}

}
