// $Id: ModelListenerSpecialQuestionary.java,v 1.2 2011-05-31 17:25:25 sboule Exp $

package com.client360.configuration.wad.modelListeners;

import java.util.List;

import org.apache.log4j.Logger;

import com.netappsid.erp.configurator.Configurable;
import com.netappsid.erp.configurator.renderers.RenderingComponent;

public class ModelListenerSpecialQuestionary extends com.netappsid.wadconfigurator.modelListeners.ModelListenerAdvancedSectionGrilles
{
	private static final Logger logger = Logger.getLogger(ModelListenerSpecialQuestionary.class);

	// Default constructor
	public ModelListenerSpecialQuestionary(Configurable configurable)
	{
		super(configurable);
		logger.trace("Create ModelListener for Special Questionary");
	}

	public ModelListenerSpecialQuestionary(Configurable configurable, List<RenderingComponent> renderingComponents)
	{
		super(configurable, renderingComponents);
		logger.trace("Create ModelListener for Special Questionary");
	}

	// Add custom overrides here
}
