// $Id: ModelListenerHungMain.java,v 1.1 2010-06-16 20:13:04 msanschagrin Exp $

package com.client360.configuration.wad.modelListeners;

import java.util.List;

import com.client360.configuration.wad.HungMain;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.erp.configurator.renderers.RenderingComponent;

public class ModelListenerHungMain extends com.netappsid.wadconfigurator.modelListeners.ModelListenerHung
{

	public ModelListenerHungMain(HungMain configurable)
	{
		super(configurable);
	}

	public ModelListenerHungMain(Configurable configurable, List<RenderingComponent> renderingComponents)
	{
		super(configurable, renderingComponents);
	}

}
