// $Id: $

package com.client360.configuration.wad.modelListeners;

import java.util.List;

import org.apache.log4j.Logger;

import com.client360.configuration.wad.ProductTemplate;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.erp.configurator.renderers.RenderingComponent;
import com.netappsid.rendering.productGroup.ShapeForm;

public class ModelListenerProductTemplate extends com.netappsid.wadconfigurator.modelListeners.ModelListenerProductTemplate
{
	private static final Logger logger = Logger.getLogger(ModelListenerProductTemplate.class);
	protected ShapeForm renderingShape;

	// Default constructor
	public ModelListenerProductTemplate(ProductTemplate configurable)
	{
		super(configurable);
		logger.trace("Create ModelListener");
	}

	public ModelListenerProductTemplate(Configurable configurable, List<RenderingComponent> renderingComponents)
	{
		super(configurable, renderingComponents);
		logger.trace("Create ModelListener");
	}
}
