// $Id: ModelListenerCasementVisionPvc.java,v 1.4 2010-12-10 12:46:55 jddaigle Exp $

package com.client360.configuration.wad.modelListeners;

import java.util.List;

import com.client360.configuration.wad.CasementVisionPvc;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.erp.configurator.renderers.RenderingComponent;
import com.netappsid.wadconfigurator.modelListeners.ModelListenerCasement;

public class ModelListenerCasementVisionPvc extends ModelListenerCasement
{

	public ModelListenerCasementVisionPvc(CasementVisionPvc configurable)
	{
		super(configurable);
	}

	public ModelListenerCasementVisionPvc(Configurable configurable, List<RenderingComponent> renderingComponents)
	{
		super(configurable, renderingComponents);
	}

	// public void propertyChange(PropertyChangeEvent evt)
	// {
	//
	// super.propertyChange(evt);
	//
	// String name = evt.getPropertyName();
	// Object value = evt.getNewValue();
	// Object oldValue = evt.getOldValue();
	// Object source = evt.getSource();
	//
	//
	// if(name == CasementVisionPvcSash.PROPERTYNAME_CHOICESASHTYPE)
	// {
	// if(source instanceof Sash)
	// {
	// setConstantViewOnTop(getRenderingComponent((Configurable)source), (Sash)source);
	// }
	// }
	//
	// }
}
