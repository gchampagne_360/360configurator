// $Id: ModelListenerPortail.java,v 1.1 2010-09-28 17:43:29 jddaigle Exp $

package com.client360.configuration.wad.modelListeners;

import com.client360.configuration.wad.Portail;
import com.netappsid.wadconfigurator.modelListeners.ModelListenerEntranceDoor;

public class ModelListenerPortail extends ModelListenerEntranceDoor
{
	public ModelListenerPortail(Portail configurable)
	{
		super(configurable);
	}
}
