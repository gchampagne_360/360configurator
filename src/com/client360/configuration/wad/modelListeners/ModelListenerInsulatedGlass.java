// $Id: ModelListenerInsulatedGlass.java,v 1.1 2010-05-18 13:41:33 jddaigle Exp $

package com.client360.configuration.wad.modelListeners;

import java.util.List;

import org.apache.log4j.Logger;

import com.client360.configuration.wad.InsulatedGlass;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.erp.configurator.renderers.RenderingComponent;

public class ModelListenerInsulatedGlass extends com.netappsid.wadconfigurator.modelListeners.ModelListenerInsulatedGlass
{
	private static final Logger logger = Logger.getLogger(ModelListenerInsulatedGlass.class);

	// Default constructor
	public ModelListenerInsulatedGlass(InsulatedGlass configurable)
	{
		super(configurable);
	}

	public ModelListenerInsulatedGlass(Configurable configurable, List<RenderingComponent> renderingComponents)
	{
		super(configurable, renderingComponents);
	}

	// Add custom overrides here
}
