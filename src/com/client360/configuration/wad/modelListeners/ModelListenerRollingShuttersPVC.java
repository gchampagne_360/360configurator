// $Id: ModelListenerCasementVisionPvc.java,v 1.4 2010-12-10 12:46:55 jddaigle Exp $

package com.client360.configuration.wad.modelListeners;

import java.util.List;

import com.client360.configuration.wad.RollingShuttersPVC;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.erp.configurator.renderers.RenderingComponent;
import com.netappsid.wadconfigurator.modelListeners.ModelListenerRollingShutter;

public class ModelListenerRollingShuttersPVC extends ModelListenerRollingShutter
{

	public ModelListenerRollingShuttersPVC(RollingShuttersPVC configurable)
	{
		super(configurable);
	}

	public ModelListenerRollingShuttersPVC(Configurable configurable, List<RenderingComponent> renderingComponents)
	{
		super(configurable, renderingComponents);
	}

}
