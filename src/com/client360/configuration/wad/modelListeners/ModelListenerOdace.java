// $Id: ModelListenerCasementVisionPvc.java,v 1.4 2010-12-10 12:46:55 jddaigle Exp $

package com.client360.configuration.wad.modelListeners;

import java.util.List;

import com.client360.configuration.wad.Odace;
import com.netappsid.erp.configurator.renderers.RenderingComponent;
import com.netappsid.wadconfigurator.modelListeners.ModelListenerCasement;

public class ModelListenerOdace extends ModelListenerCasement
{

	public ModelListenerOdace(Odace configurable)
	{
		super(configurable);
	}

	public ModelListenerOdace(Odace configurable, List<RenderingComponent> renderingComponents)
	{
		super(configurable, renderingComponents);
	}

}
