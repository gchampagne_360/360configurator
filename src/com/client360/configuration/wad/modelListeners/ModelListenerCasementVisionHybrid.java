// $Id: ModelListenerCasementVisionHybrid.java,v 1.1 2010-10-25 15:47:55 jddaigle Exp $

package com.client360.configuration.wad.modelListeners;

import java.util.List;

import com.client360.configuration.wad.CasementVisionHybrid;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.erp.configurator.renderers.RenderingComponent;
import com.netappsid.wadconfigurator.modelListeners.ModelListenerCasement;

public class ModelListenerCasementVisionHybrid extends ModelListenerCasement
{

	public ModelListenerCasementVisionHybrid(CasementVisionHybrid configurable)
	{
		super(configurable);
	}

	public ModelListenerCasementVisionHybrid(Configurable configurable, List<RenderingComponent> renderingComponents)
	{
		super(configurable, renderingComponents);
	}

	// public void propertyChange(PropertyChangeEvent evt)
	// {
	//
	// super.propertyChange(evt);
	//
	// String name = evt.getPropertyName();
	// Object value = evt.getNewValue();
	// Object oldValue = evt.getOldValue();
	// Object source = evt.getSource();
	//
	//
	// if(name == CasementVisionPvcSash.PROPERTYNAME_CHOICESASHTYPE)
	// {
	// if(source instanceof Sash)
	// {
	// setConstantViewOnTop(getRenderingComponent((Configurable)source), (Sash)source);
	// }
	// }
	//
	// }
}
