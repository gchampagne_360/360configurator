package com.client360.configuration.wad.modelListeners;

import java.util.List;

import org.apache.log4j.Logger;

import com.netappsid.erp.configurator.Configurable;
import com.netappsid.erp.configurator.renderers.RenderingComponent;

public class ModelListenerSpecialMuntinQuestionary extends com.netappsid.wadconfigurator.modelListeners.ModelListenerAdvancedSectionGrilles
{
	private static final Logger logger = Logger.getLogger(ModelListenerSpecialMuntinQuestionary.class);

	// Default constructor
	public ModelListenerSpecialMuntinQuestionary(Configurable configurable)
	{
		super(configurable);
		logger.trace("Create ModelListener for Special Muntin Questionary");
	}

	public ModelListenerSpecialMuntinQuestionary(Configurable configurable, List<RenderingComponent> renderingComponents)
	{
		super(configurable, renderingComponents);
		logger.trace("Create ModelListener for Special Muntin Questionary");
	}

	// Add custom overrides here
}
