package com.client360.configuration.wad.modelListeners;

import java.util.List;

import org.apache.log4j.Logger;

import com.netappsid.erp.configurator.Configurable;
import com.netappsid.erp.configurator.renderers.RenderingComponent;

public class ModelListenerMuntinQuestionary extends com.netappsid.wadconfigurator.modelListeners.ModelListenerMuntinQuestionary
{
	private static final Logger logger = Logger.getLogger(ModelListenerMuntinQuestionary.class);

	// Default constructor
	public ModelListenerMuntinQuestionary(Configurable configurable)
	{
		super(configurable);
		logger.trace("Create ModelListener for Muntin Questionary");
	}

	public ModelListenerMuntinQuestionary(Configurable configurable, List<RenderingComponent> renderingComponents)
	{
		super(configurable, renderingComponents);
		logger.trace("Create ModelListener for Muntin Questionary");
	}

	// Add custom overrides here
}
