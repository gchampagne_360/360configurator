// $Id: ModelListenerHungMain.java,v 1.1 2010-06-16 20:13:04 msanschagrin Exp $

package com.client360.configuration.wad.modelListeners;

import java.util.List;

import com.client360.configuration.wad.Hung;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.erp.configurator.renderers.RenderingComponent;

public class ModelListenerHung extends com.netappsid.wadconfigurator.modelListeners.ModelListenerHung
{

	public ModelListenerHung(Hung configurable)
	{
		super(configurable);
	}

	public ModelListenerHung(Configurable configurable, List<RenderingComponent> renderingComponents)
	{
		super(configurable, renderingComponents);
	}

}
