// $Id: $

package com.client360.configuration.wad.modelListeners;

import java.util.List;

import org.apache.log4j.Logger;

import com.client360.configuration.wad.ProductGroup;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.erp.configurator.renderers.RenderingComponent;
import com.netappsid.rendering.productGroup.ShapeForm;

public class ModelListenerProductGroup extends com.netappsid.wadconfigurator.modelListeners.ModelListenerProductGroup
{
	private static final Logger logger = Logger.getLogger(ModelListenerProductGroup.class);
	protected ShapeForm renderingShape;

	// Default constructor
	public ModelListenerProductGroup(ProductGroup configurable)
	{
		super(configurable);
		logger.trace("Create ModelListener");
	}

	public ModelListenerProductGroup(Configurable configurable, List<RenderingComponent> renderingComponents)
	{
		super(configurable, renderingComponents);
		logger.trace("Create ModelListener");
	}
}
