// $Id: ModelListenerQuestionnairePetitBoisEnfourche.java,v 1.2 2011-09-06 13:22:20 sboule Exp $

package com.client360.configuration.wad.modelListeners;

import java.util.List;

import org.apache.log4j.Logger;

import com.netappsid.erp.configurator.Configurable;
import com.netappsid.erp.configurator.renderers.RenderingComponent;

public class ModelListenerQuestionnairePetitBoisEnfourche extends com.netappsid.wadconfigurator.modelListeners.ModelListenerAdvancedSectionGrilles
{
	private static final Logger logger = Logger.getLogger(ModelListenerQuestionnairePetitBoisEnfourche.class);

	// Default constructor
	public ModelListenerQuestionnairePetitBoisEnfourche(Configurable configurable)
	{
		super(configurable);
		logger.trace("Create ModelListener for Petit Bois Enfourch�");
	}

	public ModelListenerQuestionnairePetitBoisEnfourche(Configurable configurable, List<RenderingComponent> renderingComponents)
	{
		super(configurable, renderingComponents);
		logger.trace("Create ModelListener for Petit Bois Enfourch�");
	}
}
