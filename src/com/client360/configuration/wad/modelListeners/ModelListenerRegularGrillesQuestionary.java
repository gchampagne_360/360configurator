package com.client360.configuration.wad.modelListeners;

import java.util.List;

import org.apache.log4j.Logger;

import com.netappsid.erp.configurator.Configurable;
import com.netappsid.erp.configurator.renderers.RenderingComponent;

public class ModelListenerRegularGrillesQuestionary extends com.netappsid.wadconfigurator.modelListeners.ModelListenerRegularGrillesQuestionary
{
	private static final Logger logger = Logger.getLogger(ModelListenerRegularGrillesQuestionary.class);

	// Default constructor
	public ModelListenerRegularGrillesQuestionary(Configurable configurable)
	{
		super(configurable);
		logger.trace("Create ModelListener for Regular Grilles Questionary");
	}

	public ModelListenerRegularGrillesQuestionary(Configurable configurable, List<RenderingComponent> renderingComponents)
	{
		super(configurable, renderingComponents);
		logger.trace("Create ModelListener for Regular Grilles Questionary");
	}
}
