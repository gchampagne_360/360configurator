// $Id: ModelListenerSteelDoor.java,v 1.1 2010-05-18 13:41:33 jddaigle Exp $

package com.client360.configuration.wad.modelListeners;

import com.client360.configuration.wad.SteelDoor;
import com.netappsid.erp.configurator.renderers.RenderingComponent;
import com.netappsid.wadconfigurator.modelListeners.ModelListenerEntranceDoor;

public class ModelListenerSteelDoor extends ModelListenerEntranceDoor
{
	public ModelListenerSteelDoor(SteelDoor configurable)
	{
		super(configurable);
	}
	
	public ModelListenerSteelDoor(com.netappsid.erp.configurator.Configurable configurable, java.util.List<RenderingComponent> list)
	{
		super(configurable, list);
	}
}
