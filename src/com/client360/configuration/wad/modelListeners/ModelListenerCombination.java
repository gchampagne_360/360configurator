// $Id: ModelListenerCombination.java,v 1.1 2010-05-18 13:41:32 jddaigle Exp $

package com.client360.configuration.wad.modelListeners;

import java.util.List;

import org.apache.log4j.Logger;

import com.client360.configuration.wad.Combination;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.erp.configurator.renderers.RenderingComponent;

public class ModelListenerCombination extends com.netappsid.wadconfigurator.modelListeners.ModelListenerCombination
{
	private static final Logger logger = Logger.getLogger(ModelListenerCombination.class);

	// Default constructor
	// Default constructor
	public ModelListenerCombination(Combination configurable)
	{
		super(configurable);
	}

	public ModelListenerCombination(Configurable configurable, List<RenderingComponent> renderingComponents)
	{
		super(configurable, renderingComponents);
	}
}
