// $Id: ModelListenerSliderPvc.java,v 1.1 2010-05-18 13:41:33 jddaigle Exp $

package com.client360.configuration.wad.modelListeners;

import java.util.List;

import org.apache.log4j.Logger;

import com.client360.configuration.wad.SliderPvc;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.erp.configurator.renderers.RenderingComponent;
import com.netappsid.wadconfigurator.modelListeners.ModelListenerSlider;

public class ModelListenerSliderPvc extends ModelListenerSlider
{
	private static final Logger logger = Logger.getLogger(ModelListenerSliderPvc.class);

	// Default constructor
	public ModelListenerSliderPvc(SliderPvc configurable)
	{
		super(configurable);
	}

	public ModelListenerSliderPvc(Configurable configurable, List<RenderingComponent> renderingComponents)
	{
		super(configurable, renderingComponents);
	}

	// Add custom overrides here
}
