// $Id: ModelListenerSliderMain.java,v 1.1 2010-05-18 13:41:32 jddaigle Exp $

package com.client360.configuration.wad.modelListeners;

import java.util.List;

import org.apache.log4j.Logger;

import com.client360.configuration.wad.SliderMain;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.erp.configurator.renderers.RenderingComponent;

public class ModelListenerSliderMain extends com.netappsid.wadconfigurator.modelListeners.ModelListenerSlider
{
	private static final Logger logger = Logger.getLogger(ModelListenerSliderMain.class);

	// Default constructor
	public ModelListenerSliderMain(SliderMain configurable)
	{
		super(configurable);
		logger.trace("Create ModelListener");
	}

	public ModelListenerSliderMain(Configurable configurable, List<RenderingComponent> renderingComponents)
	{
		super(configurable, renderingComponents);
		logger.trace("Create ModelListener");
	}

}
