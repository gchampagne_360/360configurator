/**
 * 
 */
package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.netappsid.commonutils.math.Units;
import com.netappsid.erp.configurator.Configurable;

/**
 * Muntin used to test the AdvancedSectionGrilles
 * 
 * @author sboule
 */
public class InternalMuntin extends InternalMuntinBase
{
	public InternalMuntin(Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
		setDefaultThickness(ProductData.getValue(ProductType.INTERNAL_MUNTIN, Data.INTERNAL_MUNTIN_THICKNESS));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.netappsid.wadconfigurator.InternalMuntin#getInternalMuntinToGrillesOffset()
	 */
	@Override
	public Measure<Length> getInternalMuntinToGrillesOffset()
	{
		return Measure.valueOf(
				getThickness().doubleValue(Units.INCH)
						/ 2d
						- (ProductData.getValue(ProductType.INTERNAL_MUNTIN, Data.INTERNAL_MUNTIN_THICKNESS).doubleValue(Units.INCH) / 2d - ProductData
								.getValue(ProductType.INTERNAL_MUNTIN, Data.INTERNAL_MUNTIN_TO_GRILLES).doubleValue(Units.INCH)), Units.INCH);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.netappsid.wadconfigurator.InternalMuntin#getInternalMuntinToGlassOffset()
	 */
	@Override
	public Measure<Length> getInternalMuntinToGlassOffset()
	{
		return ProductData.getValue(ProductType.INTERNAL_MUNTIN, Data.INTERNAL_MUNTIN_TO_GLASS_OFFSET);
		// return Measure.valueOf(getThickness().doubleValue(Units.INCH)/2d -
		// (ProductData.getValue(ProductType.INTERNAL_MUNTIN, Data.INTERNAL_MUNTIN_THICKNESS).doubleValue(Units.INCH) / 2d -
		// ProductData.getValue(ProductType.INTERNAL_MUNTIN, Data.INTERNAL_MUNTIN_TO_GLASS_OFFSET).doubleValue(Units.INCH)),
		// Units.INCH);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.netappsid.wadconfigurator.InternalMuntin#getInternalMuntinToVisibleGlassOffset()
	 */
	@Override
	public Measure<Length> getInternalMuntinToVisibleGlassOffset()
	{
		return Measure.valueOf(getThickness().doubleValue(Units.INCH) / 2d, Units.INCH);
	}

}
