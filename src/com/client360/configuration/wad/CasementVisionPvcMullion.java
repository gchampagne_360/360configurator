// $Id: CasementVisionPvcMullion.java,v 1.10 2011-04-27 12:09:13 jddaigle Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.netappsid.commonutils.math.Units;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.rendering.common.advanced.Drawing;

public class CasementVisionPvcMullion extends com.client360.configuration.wad.CasementVisionPvcMullionBase
{
	public CasementVisionPvcMullion(Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
	}

	@Override
	public Drawing getCad()
	{
		return null;
		// return ProductData.getDrawing(ProductType.CASEMENT_VISION_PVC_IN, Data.DRAWING_MULLION);
	}

	@Override
	public Measure<Length> getWidthOS()
	{
		if (isAstragal() == null || (isAstragal() != null && !isAstragal()))
		{
			return ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.MULLION_WIDTH_OS);
		}
		else
		{
			return ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.ASTRAGAL_WIDTH_OS);
		}
	}

	@Override
	public Measure<Length> getWidth()
	{
		if (isAstragal() == null || (isAstragal() != null && !isAstragal()))
		{
			return ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.MULLION_WIDTH);
		}
		else
		{
			return ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.ASTRAGAL_WIDTH);
		}
	}

	@Override
	public Measure<Length> getLeftGlassPositionOffset()
	{
		Measure<Length> result = Units.mm(0);
		
		if (isAstragal() == null || !isAstragal())
		{
			result = ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.LEFT_MULLION_TO_SASH_PVC);
		}
		else
		{
			result = Units.inch(0d);
		}
		return result;
	}

	@Override
	public Measure<Length> getRightGlassPositionOffset()
	{
		Measure<Length> result = Units.mm(0);
		if (isAstragal() == null || !isAstragal())
		{
			result = ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.RIGHT_MULLION_TO_SASH_PVC);
		}
		else
		{
			result = Units.inch(0d);
		}
		return result;
	}

	@Override
	public Measure<Length> getLeftSashPositionOffset()
	{
		Measure<Length> result = Units.mm(0);
		if (isAstragal() == null || !isAstragal())
		{
			result = ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.LEFT_MULLION_TO_SASH_PVC);
		}
		else
		{
			Boolean isAstragalLeft = isAstragalLeft(); 
			
			if (isAstragalLeft != null && isAstragalLeft)
			{
				result = ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.ASTRAGAL_TO_OWNER_SASH);
			}
			else
			{
				result = ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.ASTRAGAL_TO_OTHER_SASH);
			}
		}
		return result;
	}

	@Override
	public Measure<Length> getRightSashPositionOffset()
	{
		Measure<Length> result = Units.mm(0);
		if (isAstragal() == null || !isAstragal())
		{
			result = ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.RIGHT_MULLION_TO_SASH_PVC);
		}
		else
		{
			Boolean isAstragalRight = isAstragalRight(); 
			
			if (isAstragalRight != null && isAstragalRight)
			{
				result = ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.ASTRAGAL_TO_OWNER_SASH);
			}
			else
			{
				result = ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.ASTRAGAL_TO_OTHER_SASH);
			}
		}
		return result;
	}
}
