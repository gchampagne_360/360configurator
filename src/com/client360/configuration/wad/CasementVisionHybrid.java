// $Id: CasementVisionHybrid.java,v 1.8 2011-06-20 12:09:36 sboule Exp $

package com.client360.configuration.wad;

import static com.client360.configuration.wad.utils.FenergicUtils.PVCColorChart;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.client360.configuration.wad.enums.ChoiceFrameDepth;
import com.netappsid.commonutils.geo.CornerType;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.rendering.common.advanced.Drawing;
import com.netappsid.wadconfigurator.ProductGroup;
import com.netappsid.wadconfigurator.Sash;
import com.netappsid.wadconfigurator.WindowsMullion;

public class CasementVisionHybrid extends com.client360.configuration.wad.CasementVisionHybridBase
{
	public CasementVisionHybrid(Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
		setChoiceFrameDepth(ChoiceFrameDepth.F412);

		setDefaultProfiles(getClientProfiles());

		addPropertyChangeListener(PROPERTYNAME_CASEMENTVISIONHYBRIDSASH, "testListener");

		if (getParent(ProductGroup.class) != null)
		{
			setDefaultNbSections(1);
		}
	}

	@Override
	public void afterSashChanged(Sash[] sash)
	{

		for (Sash item : sash)
		{
			// Set le CasementVisionHybrid comme master - replication de toutes les properties de item
			item.setReplicationMaster(getCasementVisionHybridSash());

			// Set le Sash comme master - replication uniquement de CasementVisionHybridSash.PROPERTYNAME_CHOICESASHTYPE);
			// item.setReplicationMaster( getCasementVisionHybridSash(),
			// CasementVisionHybridSash.PROPERTYNAME_CHOICESASHTYPE);

			// Retire de la liste des properties a repliquer la properties en parametre des properties de item.
			item.addPropertiesToReplicationExclusions(CasementVisionHybridSash.PROPERTYNAME_CHOICESASHTYPE);

			// item.getSashSection()[0].setReplicationMaster( getCasementVisionHybridSash().getSashSection()[0] );

		}

		super.afterSashChanged(sash);
	}

	@Override
	public WindowsMullion[] mullionMakeArray(int nbSections)
	{
		return makeConfigurableArray(CasementVisionHybridMullion.class, nbSections, this);
	}

	@Override
	public Sash[] sashMakeArray(int nbSections)
	{
		return makeConfigurableArray(CasementVisionHybridSash.class, nbSections, this);
	}

	@Override
	public Measure<Length>[] getSashOffset()
	{
		return ProductData.getSpacer(ProductType.CASEMENT, Data.FRAME_TO_SASH_OFFSET_SPACERS);
	}

	@Override
	public Measure<Length>[] getMullionOffset()
	{
		return ProductData.getSpacer(ProductType.CASEMENT, Data.FRAME_BORDER_SPACERS);
	}

	@Override
	public Measure<Length>[] getFrameBorder()
	{
		return ProductData.getSpacer(ProductType.CASEMENT, Data.FRAME_BORDER_SPACERS);
	}

	@Override
	public Measure<Length>[] getGlassOffset()
	{
		return ProductData.getSpacer(ProductType.CASEMENT, Data.SASH_TO_GLASS);
	}

	@Override
	public CornerType[] getCornerType()
	{
		return ProductData.getCornerType(ProductType.CASEMENT, Data.FRAME_CORNERTYPE);
	}

	@Override
	public void afterChoiceFrameDepthChanged(ChoiceFrameDepth choiceFrameDepth)
	{
		super.afterChoiceFrameDepthChanged(choiceFrameDepth);
		setFrameDepth(choiceFrameDepth.thickness);
	}

	@Override
	public String colorChartNameExterior()
	{
		return PVCColorChart;
	}

	@Override
	public String colorChartNameInterior()
	{
		return PVCColorChart;
	}

	@Override
	public int getNbSash()
	{
		return 0;
	}

	@Override
	public String productCode()
	{
		return null;
	}

	@Override
	public void setDivisionDimension()
	{}

	@Override
	public boolean useAutomaticGrilleAlignement()
	{
		return false;
	}

	@Override
	public String getName()
	{
		return clientTraductionBundle.getString("CasementVisionMV");
	}

	@Override
	public String toString()
	{
		com.netappsid.commonutils.geo.Form form = getDimension().getForm();

		String formDesc = null;
		if (form != null)
		{
			// form.identifyByGeo(true, true);
			// formDesc = form.getDescription();
		}

		StringBuilder s = new StringBuilder();

		if (getParent() instanceof ProductTemplate)
		{
			if (getParent().getParent() instanceof ProductTemplate)
			{
				s.append(getParent().getParent().getIndex() + 1);
			}
			else
			{
				s.append(getParent().getIndex() + 1);
			}
			s.append("-");
		} /* end if */

		s.append(clientTraductionBundle.getString("CasementVisionMV"));

		if (formDesc != null)
		{
			s.append("(");
			s.append(formDesc);
			s.append(")");
		}

		return s.toString();
	}

	public boolean isSectionVisible()
	{
		return false;
	}

	public boolean isDivisionVisible()
	{
		return false;
	}

	public boolean isInteriorColorColorChartEnabled()
	{
		return false;
	}

	public boolean isInteriorColorNonStandardColorEnabled()
	{
		return false;
	}

	@Override
	public void afterFrameDepthChanged(Measure<Length> frameDepth)
	{
		super.afterFrameDepthChanged(frameDepth);
		splitSection();
	}

	public boolean isHybridVisible()
	{
		return true;
	}

	public boolean isChoicePresetWindowWidthVisible()
	{
		return isSectionInputVisible();
	}

	@Override
	public Drawing getCad()
	{
		return null;
	}

	@Override
	public void init(Object[] params)
	{
		// TODO Auto-generated method stub

	}
}
