// $Id: SliderPvcSashSection.java,v 1.3 2011-04-27 12:09:13 jddaigle Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.commonutils.math.Units;
import com.netappsid.erp.configurator.Configurable;

public class SliderPvcSashSection extends com.client360.configuration.wad.SliderPvcSashSectionBase
{

	public SliderPvcSashSection(Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
		setDefaultWithInsulatedGlass(true);
	}

	// Add custom overrides here
	@Override
	public Measure<Length>[] getGlassOffset()
	{

		Measure[] glassOffset = new Measure[] { Measure.valueOf(-0.79375, Units.MM), Measure.valueOf(2.43125, Units.MM), Measure.valueOf(-0.79375, Units.MM),
				Measure.valueOf(2.43125, Units.MM) };

		return glassOffset;
	}

	public boolean isGlassEnabled()
	{
		return false;
	}
}
