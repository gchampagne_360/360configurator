package com.client360.configuration.wad.tools;

import com.netappsid.rendering.common.advanced.Tool;


public class ToolB1 extends Tool
{

	@Override
	protected void load()
	{
		setSvgFileName("ToolB1.svg");
		setName("Test pour jabage - gauche");
		setOriginX(0);
		setOriginY(0);
	}
}
