package com.client360.configuration.wad.tools;

import com.netappsid.rendering.common.advanced.Tool;


public class ToolA2 extends Tool
{

	@Override
	protected void load()
	{
		setSvgFileName("ToolA2.svg");
		setName("PIECE D'APPUI: PROFIL DESSUS  poste 7");
		setOriginX(62.57);
		setOriginY(0);
	}
}
