package com.client360.configuration.wad.tools;

import com.netappsid.rendering.common.advanced.Tool;


public class ToolD2 extends Tool
{

	@Override
	protected void load()
	{
		setSvgFileName("ToolD2.svg");
		setName("test 2");
		setOriginX(0);
		setOriginY(0);
	}
}
