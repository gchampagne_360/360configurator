package com.client360.configuration.wad.tools;

import com.netappsid.rendering.common.advanced.Tool;


public class ToolD1 extends Tool
{

	@Override
	protected void load()
	{
		setSvgFileName("ToolD1.svg");
		setName("Test");
		setOriginX(14.491);
		setOriginY(0);
	}
}
