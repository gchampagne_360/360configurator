package com.client360.configuration.wad.tools;

import com.netappsid.rendering.common.advanced.Tool;


public class ToolA3 extends Tool
{

	@Override
	protected void load()
	{
		setSvgFileName("ToolA3.svg");
		setName("PIECE D'APPUI: PROFIL DESSOUS  poste 8");
		setOriginX(0);
		setOriginY(18.93);
	}
}
