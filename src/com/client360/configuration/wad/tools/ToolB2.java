package com.client360.configuration.wad.tools;

import com.netappsid.rendering.common.advanced.Tool;


public class ToolB2 extends Tool
{

	@Override
	protected void load()
	{
		setSvgFileName("ToolB2.svg");
		setName("Test pour jambage - droit");
		setOriginX(38.25);
		setOriginY(0);
	}
}
