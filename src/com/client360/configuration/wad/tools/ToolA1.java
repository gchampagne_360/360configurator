package com.client360.configuration.wad.tools;

import com.netappsid.rendering.common.advanced.Tool;


public class ToolA1 extends Tool
{

	@Override
	protected void load()
	{
		setSvgFileName("ToolA1.svg");
		setName("PIECE D'APPUI: PROFIL DESSUS  poste 6");
		setOriginX(0);
		setOriginY(0);
	}
}
