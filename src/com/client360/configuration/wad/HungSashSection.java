// $Id: HungSashSection.java,v 1.5 2011-04-27 12:09:13 jddaigle Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;

public class HungSashSection extends com.client360.configuration.wad.HungSashSectionBase
{
	public HungSashSection(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	@Override
	public Measure<Length>[] getGlassOffset()
	{
		Measure<Length>[] offset;
		if (isFixNoSash())
		{
			offset = ProductData.getSpacer(ProductType.HUNG_PVC, Data.FRAME_TO_GLASS_OFFSET_SPACERS);
		}
		else
		{
			offset = ProductData.getSpacer(ProductType.HUNG_PVC, Data.SASH_TO_GLASS_OFFSET_SPACERS);
		}
		return offset;
	}

	@Override
	protected Measure<Length> getMullionToGlassOffset()
	{
		return ProductData.getValue(ProductType.HUNG_PVC, Data.MEETINGRAIL_POSITION_OFFSET);
	}
}
