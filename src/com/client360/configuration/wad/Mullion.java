// $Id: $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.commonutils.math.Units;

public class Mullion extends com.client360.configuration.wad.MullionBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public Mullion(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	public Measure<Length> getLeftThickness()
	{
		return Units.inch(1.5d);
	}

	public Measure<Length> getRightThickness()
	{
		return Units.inch(1.5d);
	}

	@Override
	public Measure<Length> getMullionToMullionLeftOffset()
	{
		return Units.inch(0.75d);
	}

	@Override
	public Measure<Length> getMullionToMullionRightOffset()
	{
		return Units.inch(0.75d);
	}
}
