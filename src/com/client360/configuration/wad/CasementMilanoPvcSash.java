// $Id: CasementMilanoPvcSash.java,v 1.2 2011-09-07 19:04:34 sboule Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.commonutils.geo.CornerType;
import com.netappsid.rendering.common.advanced.Drawing;
import com.netappsid.wadconfigurator.SashSection;

public class CasementMilanoPvcSash extends com.client360.configuration.wad.CasementMilanoPvcSashBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public CasementMilanoPvcSash(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	@Override
	public CornerType[] getCornerType()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length> getSectionMaxWidth()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length> getSectionMinWidth()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Drawing getCad()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length>[] getVisibleGlassOffsetSpacer()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length>[] getGlazingBeadThickness()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SashSection sashSectionClass()
	{
		// TODO Auto-generated method stub
		return null;
	}

	// Add custom overrides here
}
