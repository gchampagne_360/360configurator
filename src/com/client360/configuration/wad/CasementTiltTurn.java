// $Id: CasementTiltTurn.java,v 1.1 2011-04-27 12:09:14 jddaigle Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.commonutils.geo.CornerType;
import com.netappsid.rendering.common.advanced.Drawing;
import com.netappsid.wadconfigurator.Sash;
import com.netappsid.wadconfigurator.WindowsMullion;

public class CasementTiltTurn extends com.client360.configuration.wad.CasementTiltTurnBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public CasementTiltTurn(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	@Override
	public WindowsMullion[] mullionMakeArray(int nbMullion)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Sash[] sashMakeArray(int nbSections)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDivisionDimension()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public Measure<Length>[] getGlassOffset()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length>[] getSashOffset()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length>[] getFrameBorder()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length>[] getMullionOffset()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CornerType[] getCornerType()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Drawing getCad()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String productCode()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String colorChartNameExterior()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String colorChartNameInterior()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean useAutomaticGrilleAlignement()
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Integer getRenderingIndex()
	{
		return getIndex();
	}

	@Override
	public void init(Object[] params)
	{
		// TODO Auto-generated method stub

	}
}
