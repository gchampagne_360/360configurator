// $Id: $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.netappsid.commonutils.geo.CornerType;
import com.netappsid.commonutils.math.MathFunction;
import com.netappsid.rendering.common.advanced.Drawing;
import com.netappsid.wadconfigurator.SashSection;

public class OdaceVantail extends com.client360.configuration.wad.OdaceVantailBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public OdaceVantail(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	@Override
	public CornerType[] getCornerType()
	{
		return ProductData.getCornerType(ProductType.ODACE, Data.SASH_CORNERTYPE);
	}

	@Override
	public Measure<Length> getSectionMaxWidth()
	{
		return null;
	}

	@Override
	public Measure<Length> getSectionMinWidth()
	{
		return null;
	}

	@Override
	public Drawing getCad()
	{
		return null;
	}

	@Override
	public SashSection sashSectionClass()
	{
		return new OdaceSectionVantail(this);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Measure<Length>[] getVisibleGlassOffsetSpacer()
	{
		return (Measure<Length>[]) MathFunction.MeasureArrayAddition(getSashFrameThickness(), getGlazingBeadThickness());
	}

	@Override
	public Measure<Length>[] getSashFrameThickness()
	{
		return ProductData.getSpacer(ProductType.ODACE, Data.SASH_BORDER_SPACERS);
	}

	@Override
	public Measure<Length>[] getGlazingBeadThickness()
	{
		return ProductData.getSpacer(ProductType.ODACE, Data.GLAZING_BEAD_THICKNESS);
	}

	// Add custom overrides here
}
