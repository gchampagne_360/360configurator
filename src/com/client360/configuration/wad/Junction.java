// $Id: Junction.java,v 1.6 2011-06-30 11:02:07 jpdaigle Exp $

package com.client360.configuration.wad;

@SuppressWarnings("serial")
public class Junction extends com.client360.configuration.wad.JunctionBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public Junction(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	public boolean isControlBarCollapsed()
	{
		return false;
	}
}
