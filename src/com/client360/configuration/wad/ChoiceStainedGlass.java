// $Id: ChoiceStainedGlass.java,v 1.2 2011-04-27 12:09:13 jddaigle Exp $

package com.client360.configuration.wad;

import com.netappsid.erp.configurator.Configurable;

public class ChoiceStainedGlass extends ChoiceStainedGlassBase
{

	public ChoiceStainedGlass(Configurable parent)
	{
		super(parent);
	}

	@Override
	public String toString()
	{
		return getCode();
	}
}