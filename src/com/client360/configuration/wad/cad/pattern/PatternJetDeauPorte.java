package com.client360.configuration.wad.cad.pattern;

import com.client360.configuration.wad.cad.profile.ProfileJetDeauPorte;
import com.netappsid.commonutils.color.Color;
import com.netappsid.rendering.common.advanced.Graphic;
import com.netappsid.rendering.common.advanced.Pattern;

public class PatternJetDeauPorte extends Pattern
{

	@Override
	protected void load()
	{
		addSegment("JetDeau", 45, new Graphic(Color.Tan, null, null, new ProfileJetDeauPorte()));
	}

}
