package com.client360.configuration.wad.cad;

import com.client360.configuration.wad.cad.patterns.PatternFrameSlider;
import com.netappsid.rendering.common.advanced.Drawing;

public class CadSliderFrame extends Drawing
{

	public static final String LAYER_FRAME = "FrameSlider";

	@Override
	public String getName()
	{
		return "FrameSlider";
	}

	@Override
	public String getPath()
	{
		return "/images/cad/";
	}

	@Override
	public void load()
	{
		addLayer(LAYER_FRAME, "", "SliderFrame.svg", 0.0, 0.0);
		addPattern(LAYER_FRAME, new PatternFrameSlider());
		addLeftSnapPoint(LAYER_FRAME, Drawing.RAIL0, 0, 13);
		addLeftSnapPoint(LAYER_FRAME, Drawing.RAIL1, 0, 35);
		addLeftSnapPoint(LAYER_FRAME, Drawing.RAIL2, 0, 92);
		addLeftSnapPoint(LAYER_FRAME, Drawing.RAIL3, 0, 114);
	}

}
