package com.client360.configuration.wad.cad.profiles;

import com.netappsid.rendering.common.advanced.Profile;

public class ProfileGradient2 extends Profile
{
	@Override
	public void load()
	{
		addGradientSegment(1, 50, 0);
		addGradientSegment(1, -20, 0);
		addGradientSegment(1, 50, 0);
		addGradientSegment(1.5, 0, -50);
		addGradientSegment(0.5, -70, -30);
		addGradientSegment(1, -40, -100);

	}
}
