package com.client360.configuration.wad.cad.profiles;

import com.netappsid.rendering.common.advanced.Profile;

public class ProfileDormant extends Profile
{

	@Override
	public void load()
	{
		addGradientSegment(7, -20);
		addLine(-100);
		addGradientSegment(8, -40);
		addLine(-100);
		addGradientSegment(5.2, -20);
		addGradientSegment(6.2, -80, -30);
		addGradientSegment(7.4, -30);
		addGradientSegment(4.3, -80, -30);
		addGradientSegment(6.8, -30);
		addGradientSegment(5.6, -20, 80);
		addLine(-100);
		addGradientSegment(4.5, -30);
		addGradientSegment(23, -20);
		addGradientSegment(5, -20, 80);
		addLine(-100);
		addGradientSegment(37, -20);
		addGradientSegment(5, -20, 80);
	}

}
