package com.client360.configuration.wad.cad.profiles;

import com.netappsid.rendering.common.advanced.Profile;

public class ProfileFixFrame extends Profile
{

	@Override
	public void load()
	{

		addGradientSegment(2, -50, 0);
		addGradientSegment(47, 0);
		addGradientSegment(2, 0, 80);
		addGradientSegment(1, 80, 0);
		addGradientSegment(2.5, 0, 20);
		addGradientSegment(91, 20);
		addGradientSegment(2, 20, 80);
		addGradientSegment(1, 80, 0);
	}

}
