package com.client360.configuration.wad.cad.profiles;

import com.netappsid.rendering.common.advanced.Profile;

public class ProfileExtMouldA extends Profile
{

	@Override
	public void load()
	{
		addGradientSegment(0.25, 0, 60);
		addGradientSegment(0.25, 60, 0);
		addGradientSegment(4, 0);
		addGradientSegment(1, 0, -30);
		addLine(-60);
		addGradientSegment(0.5, -30, 60);
		addGradientSegment(0.5, 60, 0);
		addGradientSegment(1, 0, -30);
		addGradientSegment(2, -30, -20);
		addGradientSegment(1, -20, -10);
		addLine(-60);
		addGradientSegment(0.25, 0, 60);
		addGradientSegment(0.25, 60, 0);
		addGradientSegment(0.5, 0, -60);
		addLine(-60);
		addGradientSegment(1, -20, -60);
	}
}
