package com.client360.configuration.wad.cad.profiles;

import com.netappsid.rendering.common.advanced.Profile;

public class ProfileBF extends Profile
{

	@Override
	public void load()
	{
		addGradientSegment(2.9, 80, 0);
		addGradientSegment(1.1, 0);
		addLine(-80);
		addGradientSegment(1, 80, 0);
		addGradientSegment(45, 0);
		addGradientSegment(1, 0, -30);
		addLine(-80);
		addGradientSegment(1.1, 0, -30);
		addGradientSegment(2.9, 0, -50);
		addLine(-80);
		addGradientSegment(21, -10);
		addGradientSegment(2, -30, -80);

	}

}
