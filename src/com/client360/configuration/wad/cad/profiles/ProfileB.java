package com.client360.configuration.wad.cad.profiles;

import com.netappsid.rendering.common.advanced.Profile;

public class ProfileB extends Profile
{

	@Override
	public void load()
	{
		addSpace(28);
		addGradientSegment(2, 80, 0);
		addGradientSegment(26, 0);
		addGradientSegment(2, 0, -80);
	}
}
