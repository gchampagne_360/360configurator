package com.client360.configuration.wad.cad.profiles;

import com.netappsid.rendering.common.advanced.Profile;

public class ProfileRail extends Profile
{

	@Override
	public void load()
	{
		addGradientSegment(1.5, 60, 30);
		addGradientSegment(0.125, 30, 100);
		addGradientSegment(0.375, 100, 30);
		addGradientSegment(1.5, 30, -30);
	}

}
