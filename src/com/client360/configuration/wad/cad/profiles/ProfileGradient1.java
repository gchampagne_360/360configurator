package com.client360.configuration.wad.cad.profiles;

import com.netappsid.rendering.common.advanced.Profile;

public class ProfileGradient1 extends Profile
{
	@Override
	public void load()
	{
		addGradientSegment(1, 0);
		addGradientSegment(1, -20);
		addGradientSegment(1, 0);
		addGradientSegment(1.5, 0, 50);
		addLine(-50);
		addGradientSegment(0.5, 70, 30);
		addGradientSegment(1, 40, 100);

	}
}
