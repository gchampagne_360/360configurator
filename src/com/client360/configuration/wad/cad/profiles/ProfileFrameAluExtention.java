package com.client360.configuration.wad.cad.profiles;

import com.netappsid.rendering.common.advanced.Profile;

public class ProfileFrameAluExtention extends Profile
{

	@Override
	public void load()
	{
		addGradientSegment(42.5, -20);
		addLine(-80);
		addGradientSegment(46, -20, 50);
		addGradientSegment(1, 100);
	}

}
