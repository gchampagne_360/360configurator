package com.client360.configuration.wad.cad.profiles;

import com.netappsid.rendering.common.advanced.Profile;

public class ProfileFlat extends Profile
{
	@Override
	public void load()
	{
		addGradientSegment(1, 60, 0);
		addSpace(4);
		addGradientSegment(1, 0, -60);
	}
}
