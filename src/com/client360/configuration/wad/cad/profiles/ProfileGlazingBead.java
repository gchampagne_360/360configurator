package com.client360.configuration.wad.cad.profiles;

import com.netappsid.rendering.common.advanced.Profile;

public class ProfileGlazingBead extends Profile
{

	@Override
	public void load()
	{
		addGradientSegment(7, -40, -10);
		addGradientSegment(1, -10, 0);
		addSpace(16);
	}

}
