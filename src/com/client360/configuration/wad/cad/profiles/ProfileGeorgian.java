package com.client360.configuration.wad.cad.profiles;

import com.netappsid.rendering.common.advanced.Profile;

public class ProfileGeorgian extends Profile
{

	@Override
	public void load()
	{
		addGradientSegment(1, 60, 0);
		addSpace(2);
		addGradientSegment(1, 0, 60);
		addGradientSegment(1, 60, 0);
		addSpace(2);
		addGradientSegment(1, 0, -60);
		addGradientSegment(1, -60, 0);
		addSpace(2);
		addGradientSegment(1, 0, -60);
	}

}
