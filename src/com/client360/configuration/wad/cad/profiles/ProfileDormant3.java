package com.client360.configuration.wad.cad.profiles;

import com.netappsid.rendering.common.advanced.Profile;

public class ProfileDormant3 extends Profile
{

	@Override
	public void load()
	{
		addGradientSegment(2, 80, 0);
		addGradientSegment(41.7, 0);
		addGradientSegment(2, 0, -80);
	}

}
