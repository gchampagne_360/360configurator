package com.client360.configuration.wad.cad.profiles;

import com.netappsid.rendering.common.advanced.Profile;

public class ProfileBigDecoMouldA extends Profile
{

	@Override
	public void load()
	{
		// addLine(-60);
		addGradientSegment(2, -30, 0);
		addGradientSegment(2, 0);
		addLine(-60);
		addGradientSegment(1, -20);
		addLine(-60);
		addGradientSegment(2, -30, 0);
		addLine(-60);
		addGradientSegment(1, -20);
		addLine(-60);
		addGradientSegment(6, -30, 0);
		addGradientSegment(4, 0, 40);
		addGradientSegment(2, 40, 60);
		// addLine(-60);

	}
}
