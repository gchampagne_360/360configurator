package com.client360.configuration.wad.cad.profiles;

import com.netappsid.rendering.common.advanced.Profile;

public class ProfileFrameEmpty extends Profile
{

	@Override
	public void load()
	{
		addLine(-100);
		addGradientSegment(127, 0, 0);
		addLine(-100);
	}
}
