package com.client360.configuration.wad.cad.profiles;

import com.netappsid.rendering.common.advanced.Profile;

public class ProfileTraverseVantail extends Profile
{

	@Override
	public void load()
	{
		addGradientSegment(3.5, -30, -10);
		addGradientSegment(8.2, -30, -10);
		addGradientSegment(2.3, -10, 80);
		addGradientSegment(7, -30);
		addLine(-80);
		addGradientSegment(5.5, -50);
		addLine(-80);
		addGradientSegment(16.7, -30);
		addLine(-80);
		addGradientSegment(2.3, -30, -10);
		addGradientSegment(8.2, -10, 80);
		addLine(-80);
		addGradientSegment(3.5, -10, 80);
		addLine(-10);
		addGradientSegment(7.1, 20);
		addGradientSegment(6.7, 20, 80);

	}

}
