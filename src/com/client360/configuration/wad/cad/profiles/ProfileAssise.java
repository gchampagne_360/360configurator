package com.client360.configuration.wad.cad.profiles;

import com.netappsid.rendering.common.advanced.Profile;

public class ProfileAssise extends Profile
{

	@Override
	public void load()
	{
		addGradientSegment(2, -100, 0);
		addGradientSegment(18.3, 0);
		addGradientSegment(3, 0, 80);
		addGradientSegment(3, 80, 20);
		addGradientSegment(6.5, 20);
		addLine(-80);
		addGradientSegment(5, 0);
		addGradientSegment(3, 0, 80);
		addGradientSegment(3, 80, 20);
		addGradientSegment(4.1, 20);
		addLine(-80);
		addGradientSegment(10.3, 80, -20);
	}
}
