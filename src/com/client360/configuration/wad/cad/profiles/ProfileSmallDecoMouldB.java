package com.client360.configuration.wad.cad.profiles;

import com.netappsid.rendering.common.advanced.Profile;

public class ProfileSmallDecoMouldB extends Profile
{

	@Override
	public void load()
	{
		// addLine(-60);
		addGradientSegment(0.5, 20, 60);
		addGradientSegment(1.5, 60, 0);
		addGradientSegment(2, 0);
		addLine(-60);
		addGradientSegment(1, 0);
		addLine(-60);
		addGradientSegment(0.5, 20, 60);
		addGradientSegment(1.5, 60, 0);
		addLine(-60);
		addGradientSegment(1, 0);
		// addLine(-60);
	}
}
