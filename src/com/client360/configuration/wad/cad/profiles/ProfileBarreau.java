package com.client360.configuration.wad.cad.profiles;

import com.netappsid.rendering.common.advanced.Profile;

public class ProfileBarreau extends Profile
{

	@Override
	public void load()
	{
		addGradientSegment(5, 0);
		addLine(-80);
		addGradientSegment(7, -20, 80);
		addGradientSegment(3, 80, 0);
		addGradientSegment(50, 0);
		addGradientSegment(10, 0, -60);
		addLine(-80);
		addGradientSegment(5, 0);
	}

}
