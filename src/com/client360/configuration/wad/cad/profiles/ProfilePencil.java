package com.client360.configuration.wad.cad.profiles;

import com.netappsid.rendering.common.advanced.Profile;

public class ProfilePencil extends Profile
{
	@Override
	public void load()
	{
		addGradientSegment(1, 60, 0);
		addSpace(1);
		addGradientSegment(1, 0, -60);

	}
}
