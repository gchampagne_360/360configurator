package com.client360.configuration.wad.cad.profiles;

import com.netappsid.rendering.common.advanced.Profile;

public class ProfileMullion extends Profile
{

	@Override
	public void load()
	{
		addGradientSegment(0.5, 50, 0);
		addGradientSegment(1, 0);
		addGradientSegment(2, -20);
		addGradientSegment(1, 0);
		addGradientSegment(0.5, 0, -50);

	}

}
