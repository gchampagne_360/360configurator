package com.client360.configuration.wad.cad.profiles;

import com.netappsid.rendering.common.advanced.Profile;

public class ProfileJetDeau extends Profile
{

	@Override
	public void load()
	{
		addSpace(28);
		addLine(-80);
		addGradientSegment(2, -100, -60);
		addGradientSegment(8, -60);
		addLine(-80);
		addGradientSegment(4.1, -20, 0);
		addGradientSegment(5.5, 0, 70);
		addGradientSegment(3.2, 70, 20);
		addGradientSegment(2.5, 20);
		addLine(-80);
		addGradientSegment(2.7, -20);
		addGradientSegment(2, -20, -40);

	}
}
