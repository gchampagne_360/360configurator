package com.client360.configuration.wad.cad.pieces;

import java.awt.Color;

import com.client360.configuration.wad.cad.outils.OutilD2BatD;
import com.client360.configuration.wad.cad.outils.OutilD2BatG;
import com.netappsid.rendering.common.advanced.Graphic;
import com.netappsid.rendering.common.advanced.Machining;
import com.netappsid.rendering.common.advanced.RawMaterialSnapPoint;
import com.netappsid.rendering.common.advanced.WoodPart;

/**
 * Dessine une pi�ce du dormant h�rite de Woodpart et de drawing
 * 
 */
public class PieceDormantSTD extends WoodPart
{
	/**
	 * Nom de la pi�ce "Dormant STD"
	 * 
	 * @return String "Dormant STD"
	 */
	@Override
	public String getName()
	{
		return "Dormant STD";
	}

	/**
	 * Chemin des images
	 * 
	 * @return String "/images/svg/"
	 */
	@Override
	public String getPath()
	{
		return "/images/svg/";
	}

	/**
	 * Chargement du dessin
	 * 
	 */
	@Override
	protected void load()
	{
		addRawMaterial(55.0, new Graphic(Color.yellow), Machining.LEFT_RIGHT);

		addTool(new OutilD2BatG(), 6.26, 10.33, RawMaterialSnapPoint.TOPLEFT);
		addTool(new OutilD2BatD(), 9.86, 10.33, RawMaterialSnapPoint.TOPRIGHT);
	}

}
