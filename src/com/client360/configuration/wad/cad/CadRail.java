package com.client360.configuration.wad.cad;

import com.client360.configuration.wad.cad.patterns.PatternRail;
import com.netappsid.rendering.common.advanced.Drawing;

public class CadRail extends Drawing
{

	public static final String SNAPPOINT_HINGE = "hinge";

	@Override
	public String getName()
	{
		return "Rail Drawing";
	}

	@Override
	public String getPath()
	{
		return "/images/cad/kane/";
	}

	@Override
	protected void load()
	{
		// Set the svg file and put the origin to (0.7304, 0.7304)
		addLayer("RAIL", "", "Post.svg", 0.7304 * 25.4, 0.7304 * 25.4);
		// Set the pattern
		addPattern("RAIL", new PatternRail());
		addLeftSnapPoint("RAIL", SNAPPOINT_HINGE, 18.0, -10.5);

	}

}
