package com.client360.configuration.wad.cad;

import com.client360.configuration.wad.cad.patterns.PatternSlabPB;
import com.netappsid.rendering.common.advanced.Drawing;

public class CadSlabPB extends Drawing
{
	public static final String LAYER_SLAB = "slab";

	@Override
	public void load()
	{
		addLayer(LAYER_SLAB, "", "8500.svg", 0.0, 0.0);
		addLeftSnapPoint(LAYER_SLAB, HINGE, -3.0, 51.8);
		addPattern(LAYER_SLAB, new PatternSlabPB());
	}

	@Override
	public String getName()
	{
		return "Slab";
	}

	@Override
	public String getPath()
	{
		return "/images/cad/baillargeon/";
	}

}
