package com.client360.configuration.wad.cad.parametric;

import java.awt.Graphics2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import com.netappsid.commonutils.geo.NAIDShape;
import com.netappsid.commonutils.geo.ShapeFunction;
import com.netappsid.rendering.common.advanced.ParametricGraphic;
import com.netappsid.rendering.common.utils.ShapeFilling;

public class ParametricFrameExtention extends ParametricGraphic
{

	@Override
	public void paint(Graphics2D g2d)
	{
		GeneralPath gp = new GeneralPath();

		Point2D p1 = new Point2D.Double(4, 0);
		Point2D p2 = new Point2D.Double(parameters[0], 0);
		Point2D p3 = new Point2D.Double(parameters[0], parameters[1]);
		Point2D p4 = new Point2D.Double(0, parameters[1]);
		Point2D p5 = new Point2D.Double(0, 20);
		Point2D p6 = new Point2D.Double(4, 20);
		NAIDShape shape = NAIDShape.create(ShapeFunction.buildPolygonFromPoints(p1, p2, p3, p4, p5, p6));

		ShapeFilling.fill(g2d, shape, graphic, 0, true);

		g2d.setColor(graphic.getBorderColor());
		g2d.draw(gp);
	}

	@Override
	public Rectangle2D getBounds()
	{
		return null;
	}

}
