package com.client360.configuration.wad.cad.patterns;

import com.netappsid.commonutils.color.Color;
import com.netappsid.rendering.common.advanced.Graphic;
import com.netappsid.rendering.common.advanced.Pattern;

public class PatternSliderSash extends Pattern
{

	@Override
	protected void load()
	{
		addSegment(8, new Graphic(Color.white));

		addSegment(4, new Graphic(Color.LightBlue));
		addSegment(8, new Graphic(Color.white));
	}

}
