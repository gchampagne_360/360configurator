package com.client360.configuration.wad.cad.patterns;

import com.client360.configuration.wad.cad.profiles.ProfileJetDeauPorte;
import com.netappsid.commonutils.color.Color;
import com.netappsid.rendering.common.advanced.Graphic;
import com.netappsid.rendering.common.advanced.Pattern;

public class PatternJetDeauPorte extends Pattern
{

	@Override
	protected void load()
	{
		addSegment("JetDeau", 114, new Graphic(Color.Violet, null, null, new ProfileJetDeauPorte()));
	}

}
