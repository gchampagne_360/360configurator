package com.client360.configuration.wad.cad.patterns;

import com.client360.configuration.wad.cad.profiles.ProfileFixFrame;
import com.netappsid.rendering.common.advanced.Graphic;
import com.netappsid.rendering.common.advanced.Pattern;

public class PatternFrameFix extends Pattern
{

	@Override
	protected void load()
	{
		addSegment(127, new Graphic(new ProfileFixFrame()));

	}

}
