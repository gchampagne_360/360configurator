package com.client360.configuration.wad.cad.patterns;

import com.client360.configuration.wad.cad.profiles.ProfileFrameAluExtention;
import com.netappsid.commonutils.color.Color;
import com.netappsid.rendering.common.advanced.Graphic;
import com.netappsid.rendering.common.advanced.Pattern;

public class PatternFrameAluCap extends Pattern
{
	@Override
	protected void load()
	{
		addSegment("1", 89, new Graphic(Color.PINK, null, null, new ProfileFrameAluExtention()));
	}
}
