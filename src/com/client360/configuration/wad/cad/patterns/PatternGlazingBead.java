package com.client360.configuration.wad.cad.patterns;

import com.client360.configuration.wad.cad.profiles.ProfileGlazingBead;
import com.netappsid.commonutils.color.Color;
import com.netappsid.rendering.common.advanced.Graphic;
import com.netappsid.rendering.common.advanced.Pattern;

public class PatternGlazingBead extends Pattern
{

	@Override
	protected void load()
	{
		addSegment(24.2, new Graphic(Color.White, null, null, new ProfileGlazingBead()));
	}

}
