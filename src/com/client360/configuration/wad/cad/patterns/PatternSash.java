package com.client360.configuration.wad.cad.patterns;

import com.netappsid.commonutils.color.Color;
import com.netappsid.rendering.common.advanced.Graphic;
import com.netappsid.rendering.common.advanced.Pattern;

public class PatternSash extends Pattern
{

	@Override
	protected void load()
	{
		addSegment(17.5, new Graphic(Color.LIGHT_GRAY));
		addSegment(13.8, new Graphic(Color.LIGHT_GRAY));
		addSegment(32.6, new Graphic(Color.LIGHT_GRAY));
		addSegment(1.4, new Graphic(Color.LIGHT_GRAY));
		addSegment(20.8, new Graphic(Color.LIGHT_GRAY));
	}

}
