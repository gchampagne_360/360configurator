package com.client360.configuration.wad.cad.patterns;

import java.awt.Color;

import com.netappsid.rendering.common.advanced.Graphic;
import com.netappsid.rendering.common.advanced.Pattern;

public class PatternFramePB extends Pattern
{

	@Override
	protected void load()
	{
		addSegment(0.25, new Graphic(Color.BLACK));
		addSegment("Ext�rieur", 127, new Graphic(Color.WHITE));
		addSegment(0.25, new Graphic(Color.BLACK));

	}

}
