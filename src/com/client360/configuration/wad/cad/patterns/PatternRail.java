package com.client360.configuration.wad.cad.patterns;

import java.awt.Color;

import com.client360.configuration.wad.cad.profiles.ProfileRail;
import com.netappsid.rendering.common.advanced.Filling;
import com.netappsid.rendering.common.advanced.Graphic;
import com.netappsid.rendering.common.advanced.Pattern;

public class PatternRail extends Pattern
{

	@Override
	protected void load()
	{
		addSegment("Rail", 115.29741736, new Graphic(new Filling(Color.LIGHT_GRAY), new ProfileRail()));
	}

}
