package com.client360.configuration.wad.cad.patterns;

import com.netappsid.commonutils.color.Color;
import com.netappsid.rendering.common.advanced.Filling;
import com.netappsid.rendering.common.advanced.Graphic;
import com.netappsid.rendering.common.advanced.Pattern;

public class PatternFrame5 extends Pattern
{

	@Override
	protected void load()
	{
		addSegment(21, new Graphic(new Filling(Color.Lavender)));
		addSegment(57, new Graphic(new Filling(Color.Lavender)));
		addSegment(50, new Graphic(new Filling(Color.Lavender)));
		addSegment(6, new Graphic(new Filling(Color.Lavender)));
	}

}
