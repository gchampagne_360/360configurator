package com.client360.configuration.wad.cad.patterns;

import java.awt.Color;

import com.client360.configuration.wad.cad.profiles.ProfileDormant;
import com.netappsid.rendering.common.advanced.Filling;
import com.netappsid.rendering.common.advanced.Graphic;
import com.netappsid.rendering.common.advanced.Pattern;

public class MotifDormant extends Pattern
{

	@Override
	protected void load()
	{
		addSegment("Ext�rieur", 125, new Graphic(new Filling(Color.LIGHT_GRAY), new ProfileDormant()));

	}

}
