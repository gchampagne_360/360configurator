package com.client360.configuration.wad.cad.patterns;

import com.netappsid.rendering.common.advanced.Pattern;


public class PatternFrameExtention extends Pattern
{

	public static String EMPTY = "Empty";
	public static String TEXTURED = "Textured";

	@Override
	protected void load()
	{
		addSegment(EMPTY, 0, null);
//		addSegment(TEXTURED, 0, new Graphic("/com/client360/configuration/wad/images/jpgWoods/Oak3r(30).jpg"));
	}

}
