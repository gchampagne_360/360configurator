package com.client360.configuration.wad.cad.patterns;

import java.awt.Color;

import com.client360.configuration.wad.cad.profiles.ProfileTraverseVantail;
import com.netappsid.rendering.common.advanced.Filling;
import com.netappsid.rendering.common.advanced.Graphic;
import com.netappsid.rendering.common.advanced.Pattern;

public class MotifVantail extends Pattern
{

	@Override
	protected void load()
	{
		addSegment("", 71, new Graphic(new Filling((Color)null, "/images/jpgWoods/Oak1r(30)_R.jpg", 0f, null), new ProfileTraverseVantail()));

	}

}
