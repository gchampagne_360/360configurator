package com.client360.configuration.wad.cad.patterns;

import com.netappsid.commonutils.color.Color;
import com.netappsid.rendering.common.advanced.Graphic;
import com.netappsid.rendering.common.advanced.Pattern;

public class PatternFrameSlider extends Pattern
{

	@Override
	protected void load()
	{
		// addSegment(12,new Graphic(Color.LIGHT_GRAY));
		// addSegment(8,new Graphic(Color.LightBlue));
		// addSegment(18,new Graphic(Color.LIGHT_GRAY));
		// addSegment(8,new Graphic(Color.LightBlue));
		// addSegment(40,new Graphic(Color.LIGHT_GRAY));
		// addSegment(8,new Graphic(Color.LightBlue));
		// addSegment(12,new Graphic(Color.LIGHT_GRAY));
		// addSegment(8,new Graphic(Color.LightBlue));

		addSegment(127, new Graphic(Color.LIGHT_GRAY));
	}

}
