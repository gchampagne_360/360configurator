package com.client360.configuration.wad.cad.patterns;

import com.client360.configuration.wad.cad.profiles.ProfileBarreau;
import com.netappsid.commonutils.color.Color;
import com.netappsid.rendering.common.advanced.Filling;
import com.netappsid.rendering.common.advanced.Graphic;
import com.netappsid.rendering.common.advanced.Pattern;

public class PatternBarreau extends Pattern
{

	@Override
	protected void load()
	{
		addSegment("Barreau", 80, new Graphic(new Filling(Color.RAL_7044_Silk_grey), new ProfileBarreau()));
	}

}
