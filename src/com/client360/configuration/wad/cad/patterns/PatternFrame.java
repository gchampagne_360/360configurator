package com.client360.configuration.wad.cad.patterns;

import com.netappsid.commonutils.color.Color;
import com.netappsid.rendering.common.advanced.Graphic;
import com.netappsid.rendering.common.advanced.Pattern;

public class PatternFrame extends Pattern
{

	@Override
	protected void load()
	{
		addSegment(21, new Graphic(Color.LemonChiffon));
		addSegment(57, new Graphic(Color.LemonChiffon));
		addSegment(38, new Graphic(Color.LemonChiffon));
		addSegment(6, new Graphic(Color.LemonChiffon));

	}

}
