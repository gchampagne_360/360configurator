package com.client360.configuration.wad.cad.patterns;

import com.netappsid.commonutils.color.Color;
import com.netappsid.rendering.common.advanced.Graphic;
import com.netappsid.rendering.common.advanced.Pattern;

public class PatternSlabPB extends Pattern
{

	@Override
	protected void load()
	{
		addSegment(0.43, new Graphic(Color.BLACK));
		addSegment(0.844, new Graphic(Color.WHITE));
		addSegment(0.43, new Graphic(new Color(196, 138, 149)));
		addSegment(2.128, new Graphic(Color.WHITE));
		addSegment(0.43, new Graphic(Color.BLACK));
		addSegment(35.391, new Graphic(Color.WHITE));
		addSegment(0.43, new Graphic(Color.BLACK));
		addSegment(2.128, new Graphic(Color.WHITE));
		addSegment(0.43, new Graphic(new Color(196, 138, 149)));
		addSegment(0.844, new Graphic(Color.WHITE));
		addSegment(0.43, new Graphic(Color.BLACK));
	}

}
