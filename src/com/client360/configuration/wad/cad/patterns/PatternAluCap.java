package com.client360.configuration.wad.cad.patterns;

import com.netappsid.commonutils.color.Color;
import com.netappsid.rendering.common.advanced.Filling;
import com.netappsid.rendering.common.advanced.Graphic;
import com.netappsid.rendering.common.advanced.Pattern;

public class PatternAluCap extends Pattern
{

	@Override
	protected void load()
	{
		addSegment(15.7, new Graphic(new Filling(Color.Brown)));
		addSegment(13.6, new Graphic(new Filling(Color.Brown)));
	}

}
