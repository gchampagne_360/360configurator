package com.client360.configuration.wad.cad;

import com.client360.configuration.wad.cad.patterns.MotifDormant;
import com.netappsid.rendering.common.advanced.Drawing;

public class CadDormant extends Drawing
{

	public static final String SNAPPOINT_HINGE = "hinge";

	@Override
	public String getName()
	{
		return "Dormant";
	}

	@Override
	public String getPath()
	{
		return "/images/cad/samic/";
	}

	@Override
	protected void load()
	{
		addLayer("DORMANT", "", "MD.svg", 0.0, 15.75);
		addPattern("DORMANT", new MotifDormant());
		addLeftSnapPoint("DORMANT", SNAPPOINT_HINGE, 18.0, -10.5);
	}

}
