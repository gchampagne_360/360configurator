package com.client360.configuration.wad.cad;

import com.client360.configuration.wad.cad.patterns.PatternFrame;
import com.client360.configuration.wad.parts.PartSill2;
import com.netappsid.rendering.common.advanced.Drawing;
import com.netappsid.rendering.common.advanced.Reflection;
import com.netappsid.rendering.common.advanced.Rotation;

@SuppressWarnings("unchecked")
public class CadFrameFix2 extends Drawing
{
	public static final String LAYER_FRAME = "Frame";
	public static final String LAYER_PARCLOSE = "Parclose";
	public static final String SNAPPOINT_PARCLOSE = "Parclose";

	public static final String GROUP_FRAME = "Frame";

	@Override
	public void load()
	{
		addWoodPartLayer(LAYER_FRAME, GROUP_FRAME, new PartSill2(), 0, 0, Rotation.ROTATE_90, Reflection.HORIZONTAL);
		addPattern(LAYER_FRAME, new PatternFrame());

	}

	@Override
	public String getName()
	{
		return "Frame";
	}

	@Override
	public String getPath()
	{
		return "/images/cad/";
	}
}
