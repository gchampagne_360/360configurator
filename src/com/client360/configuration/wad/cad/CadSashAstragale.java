package com.client360.configuration.wad.cad;

import com.client360.configuration.wad.cad.patterns.PatternSash;
import com.netappsid.rendering.common.advanced.Drawing;

public class CadSashAstragale extends Drawing
{
	public static final String LAYER_SASH = "sashAstragal";
	public static final String LAYER_ALU_CAP = "sash_alu_cap";
	public static final String LAYER_GLAZINGBEAD_78 = "glazingbead_78";
	public static final String LAYER_GLAZINGBEAD_1 = "glazingbead_1";

	public static final String SNAPPOINT_GLAZINGBEAD = "glazingbead";
	public static final String SNAPPOINT_ALUCAP = "alucap";

	public static final String GROUP_GLAZINGBEAD = "glazingbead";

	public static final String Path = "/images/cad/";

	@Override
	public void load()
	{
		addLayer(LAYER_SASH, "", "sash.svg", 0, 0, "astragal.svg", 0, 21.486);
		addLeftSnapPoint(LAYER_SASH, HINGE, 10.22, 67.66);
		addLeftSnapPoint(LAYER_SASH, SNAPPOINT_GLAZINGBEAD, 57.55, 16.15);
		addRightSnapPoint(LAYER_SASH, SNAPPOINT_GLAZINGBEAD, 65.78, 16.15);
		addLeftSnapPoint(LAYER_SASH, SNAPPOINT_ALUCAP, 7.76, 79.65);

		addPattern(LAYER_SASH, new PatternSash());

		addLayer(LAYER_ALU_CAP, "", "sash_alu_cap.svg", 0.0, 0.0, "Empty.svg", 0.0, 0.0);
		addLeftSnapPoint(LAYER_ALU_CAP, SNAPPOINT_ALUCAP, 7.76, 15.88);
		addPattern(LAYER_ALU_CAP, new PatternSash());

		addLayer(LAYER_GLAZINGBEAD_78, "", "glazingBead.svg", 0.0, 0.0);
		addLeftSnapPoint(LAYER_GLAZINGBEAD_78, SNAPPOINT_GLAZINGBEAD, 1.19, 0.0);

	}

	@Override
	public String getName()
	{
		return "Sash";
	}

	@Override
	public String getPath()
	{
		return Path;
	}

}
