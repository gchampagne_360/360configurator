package com.client360.configuration.wad.cad;

import com.netappsid.rendering.common.advanced.Drawing;

public class CadEmptyMullion extends Drawing
{
	public static final String MULLION = "mullion";

	public static final String Path = "/images/cad/";

	@Override
	protected void load()
	{
		addLayer(MULLION, "", "Empty.svg", 73.82, 3.56);
		addLeftSnapPoint(MULLION, Drawing.LEFTHINGE, -22.92, 85.72);
		addLeftSnapPoint(MULLION, Drawing.RIGHTHINGE, 22.92, 85.72);
	}

	@Override
	public String getName()
	{
		return "Mullion";
	}

	@Override
	public String getPath()
	{
		return Path;
	}
}
