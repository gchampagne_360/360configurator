package com.client360.configuration.wad.cad;

import com.client360.configuration.wad.cad.patterns.PatternFrame;
import com.client360.configuration.wad.parts.PartSill2;
import com.netappsid.rendering.common.advanced.Drawing;
import com.netappsid.rendering.common.advanced.Reflection;
import com.netappsid.rendering.common.advanced.Rotation;

@SuppressWarnings("unchecked")
public class CadFrameFix extends Drawing
{
	public static final String LAYER_FRAME = "Frame";
	public static final String LAYER_PARCLOSE = "Parclose";
	public static final String SNAPPOINT_PARCLOSE = "Parclose";

	public static final String GROUP_FRAME = "Frame";

	@Override
	public void load()
	{
		// addLayer(LAYER_FRAME, GROUP_FRAME, "FixedFrame.svg", 0.0, 0.0, Rotation.ROTATE_90, Reflection.HORIZONTAL);
		// addLayer(LAYER_FRAME, GROUP_FRAME, "FixedFrame.svg", 0.0, 0.0);
		// addWoodPartLayer(LAYER_FRAME, GROUP_FRAME, new PartSill2(), 0, 0, Rotation.ROTATE_90, Reflection.HORIZONTAL);

		// addWoodPartLayer(LAYER_FRAME, GROUP_FRAME, new PartSill2(), 0, 0, Rotation.ROTATE_0, Reflection.NONE, new PartSill2(), 0, 0, Rotation.ROTATE_0,
		// Reflection.HORIZONTAL);
		// addWoodPartLayer(LAYER_FRAME, GROUP_FRAME, new PartSill2(), 0, 0, Rotation.ROTATE_0, Reflection.VERTICAL, new PartSill2(), 0, 0, Rotation.ROTATE_0,
		// Reflection.BOTH);
		// addWoodPartLayer(LAYER_FRAME, GROUP_FRAME, new PartSill2(), 0, 0, Rotation.ROTATE_90, Reflection.NONE, new PartSill2(), 0, 0, Rotation.ROTATE_90,
		// Reflection.HORIZONTAL);
		// addWoodPartLayer(LAYER_FRAME, GROUP_FRAME, new PartSill2(), 0, 0, Rotation.ROTATE_90, Reflection.VERTICAL, new PartSill2(), 0, 0, Rotation.ROTATE_90,
		// Reflection.BOTH);
		// addWoodPartLayer(LAYER_FRAME, GROUP_FRAME, new PartSill2(), 0, 0, Rotation.ROTATE_180, Reflection.NONE, new PartSill2(), 0, 0, Rotation.ROTATE_180,
		// Reflection.HORIZONTAL);
		// addWoodPartLayer(LAYER_FRAME, GROUP_FRAME, new PartSill2(), 0, 0, Rotation.ROTATE_180, Reflection.VERTICAL, new PartSill2(), 0, 0,
		// Rotation.ROTATE_180, Reflection.BOTH);
		// addWoodPartLayer(LAYER_FRAME, GROUP_FRAME, new PartSill2(), 0, 0, Rotation.ROTATE_270, Reflection.NONE, new PartSill2(), 0, 0, Rotation.ROTATE_270,
		// Reflection.HORIZONTAL);
		addWoodPartLayer(LAYER_FRAME, GROUP_FRAME, new PartSill2(), 0, 0, Rotation.ROTATE_270, Reflection.VERTICAL, new PartSill2(), 0, 0, Rotation.ROTATE_270,
				Reflection.BOTH);

		// addLayer(LAYER_FRAME, GROUP_FRAME, new PartSill(), 0, 0, Rotation.ROTATE_0, Reflection.NONE);
		addPattern(LAYER_FRAME, new PatternFrame());
		// addLayer(LAYER_PARCLOSE, GROUP_FRAME, "glazingBead.svg", 5.5, 0.0, Rotation.ROTATE_90, Reflection.HORIZONTAL);
		// addLayer(LAYER_PARCLOSE, GROUP_FRAME, "glazingBead.svg", 5.5, 0.0);

		// addSnapPoint(LAYER_FRAME, SNAPPOINT_PARCLOSE, 25.16,50);
		// addLeftSnapPoint(LAYER_FRAME, SNAPPOINT_PARCLOSE, 118, 32);
		// addLeftSnapPoint(LAYER_PARCLOSE, SNAPPOINT_PARCLOSE, 0.0,0.0);

	}

	@Override
	public String getName()
	{
		return "Frame";
	}

	@Override
	public String getPath()
	{
		return "/images/cad/";
	}
}
