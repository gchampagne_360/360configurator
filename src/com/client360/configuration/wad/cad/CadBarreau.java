package com.client360.configuration.wad.cad;

import com.client360.configuration.wad.cad.patterns.PatternBarreau;
import com.netappsid.rendering.common.advanced.Drawing;

public class CadBarreau extends Drawing
{

	public static final String LAYER_BARREAU = "barreau";

	@Override
	public String getName()
	{
		return "Barreau";
	}

	@Override
	public String getPath()
	{
		return "/images/cad/";
	}

	@Override
	public void load()
	{
		addLayer(LAYER_BARREAU);
		addPattern(LAYER_BARREAU, new PatternBarreau());
	}

}