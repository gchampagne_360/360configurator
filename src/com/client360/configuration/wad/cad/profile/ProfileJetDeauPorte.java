package com.client360.configuration.wad.cad.profile;

import com.netappsid.rendering.common.advanced.Profile;

public class ProfileJetDeauPorte extends Profile
{

	@Override
	public void load()
	{
		addGradientSegment(15, 0, 80);
		addGradientSegment(12, 80, 40);
		addGradientSegment(11, 40, 0);
		addLine(-80);
		addGradientSegment(2, -40, -60);
		addLine(-80);
		addGradientSegment(71.5, 0);
		addGradientSegment(2, -40, -60);
	}

}
