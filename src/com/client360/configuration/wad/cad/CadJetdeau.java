package com.client360.configuration.wad.cad;

import com.client360.configuration.wad.cad.pattern.PatternJetDeauPorte;
import com.netappsid.rendering.common.advanced.Machining;
import com.netappsid.rendering.common.advanced.WoodPart;

public class CadJetdeau extends WoodPart
{

	public static final String LAYER_JETDEAU = "jetdeau";

	@Override
	public String getName()
	{
		return "jetdeau";
	}

	@Override
	public String getPath()
	{
		return "/images/cad/";
	}

	@Override
	public void load()
	{

		addRawMaterial(Machining.LEFT_RIGHT);
		addPattern(new PatternJetDeauPorte());
		copyingLeftToRight();
		addTools("JetDeau", "jetdeau_clip.svg", 0, 0);

	}

}