package com.client360.configuration.wad.cad;

import com.client360.configuration.wad.cad.patterns.MotifVantail;
import com.netappsid.rendering.common.advanced.Drawing;

public class CadVantail extends Drawing
{

	public static final String SNAPPOINT_HINGE = "hinge";

	@Override
	public String getName()
	{
		return "Vantail";
	}

	@Override
	public String getPath()
	{
		return "/images/cad/samic/";
	}

	@Override
	protected void load()
	{
		addLayer("Vantail", "", "BC.svg", 16.0, 0.0, "BCP.svg", 0.0, 50.75);
		addPattern("Vantail", new MotifVantail());
		addLeftSnapPoint("Vantail", SNAPPOINT_HINGE, -10.0, 7.2);
	}

}
