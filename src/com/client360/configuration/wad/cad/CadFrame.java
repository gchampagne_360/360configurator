package com.client360.configuration.wad.cad;

import com.client360.configuration.wad.cad.parametric.ParametricFrameExtention;
import com.client360.configuration.wad.cad.patterns.PatternFrame;
import com.client360.configuration.wad.cad.patterns.PatternFrame5;
import com.client360.configuration.wad.cad.patterns.PatternFrameAluCap;
import com.client360.configuration.wad.cad.patterns.PatternFrameExtention;
import com.netappsid.rendering.common.advanced.Drawing;

@SuppressWarnings("unchecked")
public class CadFrame extends Drawing
{
	public static final String LAYER_FRAME45 = "Frame";
	public static final String LAYER_FRAME5 = "Frame5";
	public static final String LAYER_FRAMEALUCAP = "frame_alu_cap";
	public static final Class LAYER_FRAMEEXTENTION = ParametricFrameExtention.class;

	public static final String SNAPPOINT_EXTENTION = "Extention";
	public static final String SNAPPOINT_WOODFRAMEEXTENTION = "WoodExtention";
	public static final String SNAPPOINT_HINGE = "hinge";

	public static final String GROUP_FRAME = "Frame";

	@Override
	public void load()
	{
		addLayer(LAYER_FRAME45, GROUP_FRAME, "Frame.svg", 0.0, 2.84);
		addPattern(LAYER_FRAME45, new PatternFrame());
		addLeftSnapPoint(LAYER_FRAME45, SNAPPOINT_EXTENTION, 10.04, 115.93);
		addLeftSnapPoint(LAYER_FRAME45, SNAPPOINT_HINGE, 37.846, 86.36);
		addLeftSnapPoint(LAYER_FRAME45, SNAPPOINT_WOODFRAMEEXTENTION, 3, 21);

		addLayer(LAYER_FRAME5, GROUP_FRAME, "Frame5.svg", 0.0, 2.84);
		addPattern(LAYER_FRAME5, new PatternFrame5());
		addLeftSnapPoint(LAYER_FRAME5, SNAPPOINT_EXTENTION, 10.04, 128.63);
		addLeftSnapPoint(LAYER_FRAME5, SNAPPOINT_HINGE, 37.846, 86.36);
		addLeftSnapPoint(LAYER_FRAME5, SNAPPOINT_WOODFRAMEEXTENTION, 3, 21);

		addParametricLayer(LAYER_FRAMEEXTENTION, "", 0, 0, Axis.BOTTOMLEFT);
		addLeftSnapPoint(LAYER_FRAMEEXTENTION, SNAPPOINT_WOODFRAMEEXTENTION, 3, 0);
		addPattern(LAYER_FRAMEEXTENTION, new PatternFrameExtention());

		addLayer(LAYER_FRAMEALUCAP, "", "frame_alu_cap.svg", 0.0, 0.0);
		addPattern(LAYER_FRAMEALUCAP, new PatternFrameAluCap());
		addLeftSnapPoint(LAYER_FRAMEALUCAP, SNAPPOINT_EXTENTION, 10.04, 39.35);
	}

	@Override
	public String getName()
	{
		return "Frame";
	}

	@Override
	public String getPath()
	{
		return "/images/cad/";
	}
}
