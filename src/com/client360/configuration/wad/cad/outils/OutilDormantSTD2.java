package com.client360.configuration.wad.cad.outils;

import com.netappsid.rendering.common.advanced.Tool;


public class OutilDormantSTD2 extends Tool
{

	@Override
	protected void load()
	{
		setSvgFileName("OutilDormant2.svg");
		setName("OutilDormant2");
		setOriginX(0);
		setOriginY(0);
	}
}