package com.client360.configuration.wad.cad.outils;

import com.netappsid.rendering.common.advanced.Tool;


public class OutilD2BatD extends Tool
{

	@Override
	protected void load()
	{
		setSvgFileName("D2_BAT_D.svg");
		setName("BATTANT STD 75x55");
		setOriginX(28.694);
		setOriginY(0);
	}
}