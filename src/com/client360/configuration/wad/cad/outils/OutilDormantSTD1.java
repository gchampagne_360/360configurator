package com.client360.configuration.wad.cad.outils;

import com.netappsid.rendering.common.advanced.Tool;


public class OutilDormantSTD1 extends Tool
{

	@Override
	protected void load()
	{
		setSvgFileName("OutilDormant1.svg");
		setName("OutilDormant1");
		setOriginX(0);
		setOriginY(0);
	}
}