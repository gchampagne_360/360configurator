package com.client360.configuration.wad.cad.outils;

import com.netappsid.rendering.common.advanced.Tool;


public class OutilD2BatG extends Tool
{

	@Override
	protected void load()
	{
		setSvgFileName("D2_BAT_G.svg");
		setName("BATTANT STD 75x55");
		setOriginX(0);
		setOriginY(0);
	}
}