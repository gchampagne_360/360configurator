package com.client360.configuration.wad.cad;

import com.client360.configuration.wad.cad.pieces.PieceDormantSTD;
import com.netappsid.rendering.common.advanced.Drawing;
import com.netappsid.rendering.common.advanced.Reflection;
import com.netappsid.rendering.common.advanced.Rotation;

/**
 * dessine un dormant
 * 
 * h�rite de com.netappsid.advancedrendering.Drawing
 */
public class CadDormantSTD extends Drawing
{
	public static final String LAYER_FRAME = "Frame";
	public static final String GROUP_FRAME = "Frame";

	/**
	 * Dessinne le Dormant Charge un dessin PieceDormantStd
	 */
	@Override
	public void load()
	{
		addWoodPartLayer(LAYER_FRAME, GROUP_FRAME, new PieceDormantSTD(), 0, 0, Rotation.ROTATE_270, Reflection.VERTICAL); // , new PieceDormantSTD(), 0, 0,
																															// Rotation.ROTATE_270,
																															// Reflection.BOTH);
	}

	/**
	 * Nom du dessin
	 * 
	 * @return String "Frame"
	 */
	@Override
	public String getName()
	{
		return "Frame";
	}

	/**
	 * Chemin des images
	 * 
	 * @return String "/images/svg/"
	 */
	@Override
	public String getPath()
	{
		return "/images/svg/";
	}
}