package com.client360.configuration.wad.cad;

import com.client360.configuration.wad.cad.patterns.PatternBarreau;
import com.netappsid.rendering.common.advanced.Drawing;

public class CadFramePB extends Drawing
{

	public static final String SNAPPOINT_HINGE = "hinge";

	@Override
	public String getName()
	{
		return "Dormant";
	}

	@Override
	public String getPath()
	{
		return "/images/cad/baillargeon/";
	}

	@Override
	protected void load()
	{
		addLayer("DORMANT", "", "frame.svg", 0.0, 0.0);
		addPattern("DORMANT", new PatternBarreau());
		addLeftSnapPoint("DORMANT", SNAPPOINT_HINGE, 64.22, 135.418);
	}

}
