package com.client360.configuration.wad.cad;

import com.client360.configuration.wad.cad.patterns.PatternSliderSash;
import com.netappsid.rendering.common.advanced.Drawing;
import com.netappsid.rendering.common.advanced.Reflection;
import com.netappsid.rendering.common.advanced.Rotation;

public class CadSashSlider extends Drawing
{

	public static final String LAYER_LEFT = "Layer_Left";
	public static final String LAYER_RIGHT = "Layer_Right";
	public static final String LAYER_BOTTOM = "Layer_Bottom";

	@Override
	public String getName()
	{
		return "Sash_Slider";
	}

	@Override
	public String getPath()
	{
		return "/images/cad/";
	}

	@Override
	public void load()
	{
		addLayer(LAYER_LEFT, "", "SliderSashHandle.svg", 7.85, 16.43, Rotation.ROTATE_0, Reflection.NONE, "MeetingRail.svg", 0, 0, Rotation.ROTATE_0,
				Reflection.NONE);

		// addLayer(LAYER_LEFT,"","SliderSashHandle.svg",7.85,16.43,"MeetingRail.svg",0,0);

		addPattern(LAYER_LEFT, new PatternSliderSash());
		addLeftSnapPoint(LAYER_LEFT, Drawing.RAIL, 0, 10);

		// addLayer(LAYER_LEFT, "", "cadSashSlider2 copy.svg", 0, 0, "cadSashSlider1.svg",0,0);
		// addPattern(LAYER_LEFT, new PatternSliderSash());
		// addSnapPoint(LAYER_LEFT, Drawing.RAIL, 0,12);

		addLayer(LAYER_RIGHT, "", "MeetingRail.svg", 0, 0, Rotation.ROTATE_0, Reflection.BOTH, "SliderSashHandle.svg", 7.85, 16.43, Rotation.ROTATE_0,
				Reflection.VERTICAL);
		addPattern(LAYER_RIGHT, new PatternSliderSash());
		addLeftSnapPoint(LAYER_RIGHT, Drawing.RAIL, 0, 10);
		//
		//
		// addLayer(LAYER_BOTTOM, "", "cadSashSlider1.svg", 0, 0);
		// addPattern(LAYER_BOTTOM, new PatternSliderSash());
		// addSnapPoint(LAYER_BOTTOM, Drawing.RAIL, 0,12 );

	}

}
