package com.client360.configuration.wad.utils;

import java.io.File;

import com.netappsid.commonutils.math.Utilities;

public class GetterUtility
{
	public static String getAppPath()
	{
		// Get the applications path.
		File appPath = new File(System.getProperty("user.dir"));
		return appPath.toString() + "\\";
	}

	public static Double convertStringToDouble(String str)
	{
		if (str != null)
		{
			str = str.replace(',', '.');
			if (!str.equals("") && Utilities.isNumeric(str))
			{
				// Set the same value for each of 4 sides
				return Double.parseDouble(str);
			}
		}
		return 0d;
	}
}
