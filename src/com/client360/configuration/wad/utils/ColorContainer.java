package com.client360.configuration.wad.utils;

public interface ColorContainer
{
	public void setExteriorColorChart(String colorChart);

	public void setInteriorColorChart(String colorChart);
}
