package com.client360.configuration.wad.utils;

import com.client360.configuration.wad.ChoiceSlab360;
import com.client360.configuration.wad.Door;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class CreationRenduPorte
{

	private static String packageBati = "com.client360.configuration.wad.doormodel.";
	private static String packageAccessoire = "com.client360.configuration.wad.doormodel.accessoire.";
	private static String pathGrille = "/images/grille_svg/";

	/**
	 * Met � jour le rendu de la porte
	 */
	public static void creationRendu(Door door)
	{
		String[] listAcc;
		String packageBatiGamme = packageBati;
		String packageAccessoireGamme = packageAccessoire;

		ChoiceSlab360 slab = door.getChoiceSlab360();

		door.deleteSubModel();

		if (slab != null && slab.getFrame() != null && !slab.getFrame().equals(""))
		{
			try
			{
				String gammePorte = slab.getType();
				packageBatiGamme += gammePorte + ".";
				packageAccessoireGamme += gammePorte + ".";

				// creation du bati
				door.manageSlabModel(packageBatiGamme + slab.getFrame());

				// slab.getDoorModel().addParametricMeasure("hauSbs", slab.getSoubassement());
				addParametricValues(door);

				// ajout des accessoires
				if (slab.getAcc() != null && !slab.getAcc().equals(""))
				{
					listAcc = slab.getAcc().split(";", -1);

					for (int i = 1; i < listAcc.length + 1; i++)
					{
						if (!listAcc[i - 1].trim().equals(""))
						{
							door.setSubModel(String.valueOf(i), (Class<? extends ParametricModel>) (Class.forName(packageAccessoireGamme + listAcc[i - 1])),
									String.valueOf(i));
						}
					}
				}

			}
			catch (Exception e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// createThermos(vantail);
		}
	}

	// TODO : supprimer cette m�thode
	// Remplacer cette ex�cution par l'ajout du ParametricModel PetitBois dans les sous-mod�les.
	// � venir des que JJ a le nouveau build du WAD
	/*
	 * public static void createThermos(ISlabSamic vantail) { if (vantail.getInsulatedGlasses() != null) { // TODO V�rifier l'exactitude de ceci. Regular
	 * petitBois = new Regular(vantail.getInsulatedGlasses()[0].getInsulatedGlass());
	 * 
	 * if (vantail.getModele().getRenduPbh() != null && !vantail.getModele().getRenduPbh().equals("")) {
	 * petitBois.setNbVerticalSquare(Integer.valueOf(vantail.getModele().getRenduPbh().trim()) + 1);
	 * petitBois.setNbPBHorizontaux(Integer.valueOf(vantail.getModele().getRenduPbh().trim()));
	 * 
	 * }
	 * 
	 * if (vantail.getModele().getRenduPbv() != null && !vantail.getModele().getRenduPbv().equals("")) {
	 * petitBois.setNbHorizontalSquare(Integer.valueOf(vantail.getModele().getRenduPbv().trim()) + 1);
	 * petitBois.setNbPBVerticaux(Integer.valueOf(vantail.getModele().getRenduPbv().trim())); }
	 * petitBois.setGrilleBarThickness(Units.measure(Integer.valueOf(vantail.getModele().getRenduLarPbe()), Units.MM));
	 * vantail.getInsulatedGlasses()[0].getInsulatedGlass().setSectionGrille(petitBois); } }
	 */

	/**
	 * Ajoute les valeurs qui seront partag�es de la configuration au modele
	 */
	private static void addParametricValues(Door door)
	{
		if (door.getChoiceSlab360().getImageGrille() != null && !door.getChoiceSlab360().getImageGrille().equals(""))
		{
			door.getDoorModel().addParametricObject("imageGrille", pathGrille + door.getChoiceSlab360().getImageGrille());
		}

		if (door.getChoiceSlab360().getRenduPbh() != null && !door.getChoiceSlab360().getRenduPbh().equals(""))
		{
			door.getDoorModel().addParametricObject("pbh", door.getChoiceSlab360().getRenduPbh());
		}

		if (door.getChoiceSlab360().getRenduPbv() != null && !door.getChoiceSlab360().getRenduPbv().equals(""))
		{
			door.getDoorModel().addParametricObject("pbv", door.getChoiceSlab360().getRenduPbv());
		}

		if (door.getChoiceSlab360().getRenduLarPbe() != null && !door.getChoiceSlab360().getRenduLarPbe().equals(""))
		{
			door.getDoorModel().addParametricObject("larPbe", door.getChoiceSlab360().getRenduLarPbe());
		}

		if (door.getChoiceSlab360().getRenduLarMoulure() != null && !door.getChoiceSlab360().getRenduLarMoulure().equals(""))
		{
			door.getDoorModel().addParametricObject("larSpacer", door.getChoiceSlab360().getRenduLarMoulure());
		}
	}

}
