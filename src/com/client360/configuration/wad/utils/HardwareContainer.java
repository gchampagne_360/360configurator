package com.client360.configuration.wad.utils;

import com.client360.configuration.wad.Hardware;

public interface HardwareContainer
{
	public Hardware getCasementHardware();
}
