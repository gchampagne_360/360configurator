// $Id: Door.java,v 1.12 2011-06-06 19:03:55 sboule Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.enums.ChoiceAcc1;
import com.client360.configuration.wad.enums.ChoiceAcc2;
import com.client360.configuration.wad.enums.GlassType;
import com.client360.configuration.wad.enums.OtherSlabModel;
import com.client360.configuration.wad.utils.CreationRenduPorte;
import com.netappsid.commonutils.math.Units;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.rendering.common.advanced.Drawing;
import com.netappsid.wadconfigurator.SlabElement;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;
import com.netappsid.wadconfigurator.enums.ChoiceHandlesetSide;
import com.netappsid.wadconfigurator.enums.ChoiceOpening;
import com.netappsid.wadconfigurator.enums.ChoiceSlabNAIDModel;

public class Door extends com.client360.configuration.wad.DoorBase
{

	StringBuffer sb = new StringBuffer();

	public Door(Configurable parent)
	{
		super(parent);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void activateListener()
	{
		super.activateListener();
		addAssociationPropertyChangeListener(PROPERTYNAME_HANDLESET, SlabElement.PROPERTYNAME_POSX, "updateHandleSet");
		addAssociationPropertyChangeListener(PROPERTYNAME_HANDLESET, SlabElement.PROPERTYNAME_POSY, "updateHandleSet");
		addAssociationPropertyChangeListener(PROPERTYNAME_INTERIORCOLOR, StandardColor.PROPERTYNAME_CHOICECOLOR, "changeSlabColor");
	}

	public void updateHandleSet()
	{
		ChoiceHandleSet choiceHandleSet = getChoiceHandleSet();

		createHandleSet(choiceHandleSet.getPath(), Units.inch(choiceHandleSet.getWidth()), Units.inch(choiceHandleSet.getHeight()),
				Units.inch(choiceHandleSet.getOrix()), Units.inch(choiceHandleSet.getOriy()), choiceHandleSet.getTr1form(), getHandleSet()[0].getPosx(),
				getSlabHeight().minus(getHandleSet()[0].getPosy()), Units.inch(choiceHandleSet.getTr1width()), Units.inch(choiceHandleSet.getTr1height()));

		addHole(choiceHandleSet.getTr2form(), Units.inch(choiceHandleSet.getTr2posx()), Units.inch(choiceHandleSet.getTr2posy()),
				Units.inch(choiceHandleSet.getTr2width()), Units.inch(choiceHandleSet.getTr2height()));
	}

	public void changeSlabColor()
	{
		if (getModel() != null)
		{
			getModel().addParametricObject("Couleur motif", getInteriorColor().getJavaColor());
			updateComponents();
		}
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
		setDefaultChoiceHandleSet(null);
		setDefaultChoiceEyeHole(null);

		makeInvisible(PROPERTYNAME_OTHERSLABMODEL);
		makeInvisible(PROPERTYNAME_THERMOS);
		makeInvisible(PROPERTYNAME_CHOICEACC1);
		makeInvisible(PROPERTYNAME_CHOICEACC2);
		makeInvisible(PROPERTYNAME_CHOICEDEADBOLT);
		makeInvisible(PROPERTYNAME_CHOICEEYEHOLE);
		makeInvisible(PROPERTYNAME_CHOICEHANDLESETSIDE);

		// makeInvisible(PROPERTYNAME_CHOICESLABMODEL);
		makeInvisible(PROPERTYNAME_CHOICESTAINEDGLASS);
		makeInvisible(PROPERTYNAME_CHOICEINSULATEDGLASS);
		makeInvisible(PROPERTYNAME_DIMENSION);
		makeInvisible(PROPERTYNAME_CHOICESLABNAIDMODEL);
		makeInvisible(PROPERTYNAME_CHOICEHANDLESET);
		makeInvisible(PROPERTYNAME_HANDLESET);

		makeInvisible(PROPERTYNAME_LETTERBOX);
		makeInvisible(PROPERTYNAME_KNOCKER);
		makeInvisible(PROPERTYNAME_EYEHOLE);
		makeInvisible(PROPERTYNAME_DEADBOLT);

		makeInvisible(PROPERTYNAME_CHOICELETTERBOX);
		makeInvisible(PROPERTYNAME_CHOICEKNOCKER);
		makeInvisible(PROPERTYNAME_CHOICEEYEHOLE);
		makeInvisible(PROPERTYNAME_CHOICEDEADBOLT);

		makeDisabled(PROPERTYNAME_CHOICESTAINEDGLASSPOSITION);

		makeInvisible(PROPERTYNAME_CHOICESLABTHERMOS);

	}

	@Override
	public void afterChoiceSlabNAIDModelChanged(ChoiceSlabNAIDModel choiceSlabNAIDModel)
	{
		// TODO Auto-generated method stub
		super.afterChoiceSlabNAIDModelChanged(choiceSlabNAIDModel);

	}

	@Override
	public Drawing getCad()
	{
		return null;
	}

	@Override
	public void afterChoiceEyeHoleChanged(ChoiceEyeHole choiceEyeHole)
	{
		if (choiceEyeHole == null)
		{
			deleteEyeHole();
		}
		else
		{
			setEyeHole(choiceEyeHole.getPath(), Units.inch(choiceEyeHole.getImageHeight()), Units.inch(choiceEyeHole.getImageWidth()), getDimension()
					.getWidth().divide(2d), Units.inch(18), choiceEyeHole.getForm(), Units.inch(choiceEyeHole.getFormWidth()), Units.inch(choiceEyeHole
					.getFormHeight()));
		}
	}

	@Override
	public void afterChoiceHandleSetChanged(ChoiceHandleSet choiceHandleSet)
	{

		if (choiceHandleSet == null)
		{
			deleteHandleSet();
		}
		else
		{
			createHandleSet(choiceHandleSet.getPath(), Units.inch(choiceHandleSet.getWidth()), Units.inch(choiceHandleSet.getHeight()),
					Units.inch(choiceHandleSet.getOrix()), Units.inch(choiceHandleSet.getOriy()), choiceHandleSet.getTr1form(),
					Units.inch(choiceHandleSet.getTr1posx()), Units.inch(choiceHandleSet.getTr1posy()), Units.inch(choiceHandleSet.getTr1width()),
					Units.inch(choiceHandleSet.getTr1height()));

			addHole(choiceHandleSet.getTr2form(), Units.inch(choiceHandleSet.getTr2posx()), Units.inch(choiceHandleSet.getTr2posy()),
					Units.inch(choiceHandleSet.getTr2width()), Units.inch(choiceHandleSet.getTr2height()));
		}
	}

	@Override
	public void afterThermosChanged(Boolean thermos)
	{
		super.afterThermosChanged(thermos);

		if (thermos)
		{
			makeVisible(PROPERTYNAME_INSULATEDGLASS);
		}
		else
		{
			makeInvisible(PROPERTYNAME_INSULATEDGLASS);
		}
	}

	@Override
	public void afterOtherSlabModelChanged(OtherSlabModel otherSlabModel)
	{
		// TODO Auto-generated method stub
		super.afterOtherSlabModelChanged(otherSlabModel);
		if (otherSlabModel == null)
		{
			deleteDoorModel();
		}
		else
		{
			deleteDoorModel();
			deleteSubModel();
			setDoorModel(otherSlabModel.slabClass);
			// setImgPath("images/porte2.PNG");

		}
	}

	@Override
	public void afterChoiceAcc1Changed(ChoiceAcc1 choiceAcc1)
	{
		super.afterChoiceAcc1Changed(choiceAcc1);
		if (choiceAcc1 == null)
		{

			deleteSubModel("ChoiceAcc1");
		}
		else
		{
			if (isElementExist("ChoiceAcc1"))
			{
				deleteSubModel("ChoiceAcc1");
			}
			// setSubModel("ChoiceAcc1",choiceAcc1.accClass,choiceAcc1.spaceName);
		}
	}

	@Override
	public void afterChoiceAcc2Changed(ChoiceAcc2 choiceAcc2)
	{
		super.afterChoiceAcc2Changed(choiceAcc2);

		if (choiceAcc2 == null)
		{
			deleteSubModel("ChoiceAcc2");
		}
		else
		{
			if (isElementExist("ChoiceAcc2"))
			{
				deleteSubModel("ChoiceAcc2");
			}
			// setSubModel("ChoiceAcc2",choiceAcc2.accClass,choiceAcc2.spaceName);
		}

	}

	public boolean isChoiceAcc1Enabled()
	{

		return getOtherSlabModel() != null && (getOtherSlabModel() == OtherSlabModel.SLAB1 || getOtherSlabModel() == OtherSlabModel.SLAB2);
	}

	public boolean isChoiceAcc2Enabled()
	{

		return getOtherSlabModel() != null && getOtherSlabModel() == OtherSlabModel.SLAB2;
	}

	/**
	 * If the slab is fixed we don't replicate the choice handle
	 * 
	 * @see com.netappsid.wadconfigurator.DoorBase#afterChoiceOpeningChanged(com.netappsid.wadconfigurator.enums.ChoiceOpening)
	 */
	@Override
	public void afterChoiceOpeningChanged(ChoiceOpening choiceOpening)
	{
		super.afterChoiceOpeningChanged(choiceOpening);

		if (getChoiceOpening() == ChoiceOpening.FIXED)
		{
			addPropertyToReplicationExclusions(PROPERTYNAME_CHOICEHANDLESET);
			setDefaultChoiceHandleSet(null);
			setDefaultChoiceHandlesetSide(ChoiceHandlesetSide.LEFT);
		}
		else if (getChoiceOpening().isRight)
		{
			removePropertyFromReplicationExclusions(PROPERTYNAME_CHOICEHANDLESET);
			setDefaultChoiceHandlesetSide(ChoiceHandlesetSide.LEFT);
		}
		else if (getChoiceOpening().isLeft)
		{
			removePropertyFromReplicationExclusions(PROPERTYNAME_CHOICEHANDLESET);
			setDefaultChoiceHandlesetSide(ChoiceHandlesetSide.RIGHT);
		}
	}

	@Override
	public void afterChoiceHandlesetSideChanged(ChoiceHandlesetSide choiceHandlesetSide)
	{
		// TODO Auto-generated method stub
		super.afterChoiceHandlesetSideChanged(choiceHandlesetSide);

		if (getChoiceHandleSet() != null)
		{
			afterChoiceHandleSetChanged(getChoiceHandleSet()); // recall the handleset creation method to update the left or right position
		}
	}

	// public Boolean isChoiceHandleSetSideVisible()
	// {
	// if(getHandleSet() != null)
	// {
	// return true;
	// }
	// return false;
	// }

	@Override
	public void afterChoiceKnockerChanged(ChoiceKnocker choiceKnocker)
	{
		if (choiceKnocker == null)
		{
			deleteKnocker();
		}
		else
		{

			createKnocker(choiceKnocker.getPath(), Units.inch(choiceKnocker.getWidth()), Units.inch(choiceKnocker.getHeight()),
					Units.inch(choiceKnocker.getOrix()), Units.inch(choiceKnocker.getOriy()), choiceKnocker.getTrform(), Units.inch(choiceKnocker.getTrposx()),
					Units.inch(choiceKnocker.getTrposy()), Units.inch(choiceKnocker.getTrwidth()), Units.inch(choiceKnocker.getTrheight()));

		}
	}

	/*
	 * (non-Javadoc) WARNING -- Do not change this values. They are used by the JUnit tests.
	 */
	@Override
	public Measure<Length> getSlabGap()
	{
		if (this.getChoiceOpening() != null)
		{
			if (this.getChoiceOpening() == ChoiceOpening.FIXED)
			{
				return Units.inch(0.125);
			}
			else
			{
				return Units.inch(0.25);
			}
		}
		return Units.inch(0.0);
	}

	@Override
	public void afterChoiceSlab360Changed(ChoiceSlab360 choiceSlab360)
	{
		if (choiceSlab360 != null)
		{
			if (!choiceSlab360.getDescription().equals("FLUSH"))
			{
				removeChoicePanel();
			}
			if (choiceSlab360.getParametricModelClass().equals(""))
			{
				CreationRenduPorte.creationRendu(this);
			}
			else
			{
				manageSlabModel(choiceSlab360.getParametricModelClass());
			}
		}
		else
		{
			manageSlabModel(null);
		}

	}

	@Override
	public void afterChoiceStainedGlassChanged(ChoiceStainedGlass choiceStainedGlass)
	{
		// TODO Auto-generated method stub
		super.afterChoiceStainedGlassChanged(choiceStainedGlass);

		if (choiceStainedGlass != null && !choiceStainedGlass.getAskStainedGlassPosition().equals("enum"))
		{
		}
		else
		{
			deleteStainedGlass();
		}

		if (choiceStainedGlass != null && choiceStainedGlass.getAskStainedGlassPosition().equals("enum"))
		{
			removeChoiceStainedGlassPosition();
			makeEnabled(PROPERTYNAME_CHOICESTAINEDGLASSPOSITION);
		}
		else
		{
			makeDisabled(PROPERTYNAME_CHOICESTAINEDGLASSPOSITION);
		}
	}

	@Override
	public void afterChoiceInsulatedGlassChanged(ChoiceInsulatedGlass choiceInsulatedGlass)
	{
		if (choiceInsulatedGlass != null && !choiceInsulatedGlass.getAskStainedGlassPosition().equals("enum"))
		{
			makeVisible(PROPERTYNAME_INSULATEDGLASS);
		}
		else
		{
			makeInvisible(PROPERTYNAME_INSULATEDGLASS);
		}

		if (choiceInsulatedGlass != null && choiceInsulatedGlass.getAskStainedGlassPosition().equals("enum"))
		{
			setChoiceStainedGlassPosition(null);
			makeEnabled(PROPERTYNAME_CHOICESTAINEDGLASSPOSITION);
		}
		else
		{
			makeDisabled(PROPERTYNAME_CHOICESTAINEDGLASSPOSITION);
		}
	}

	public void manageSlabModel(String choiceSlabModel)
	{
		removeChoiceStainedGlass();
		removeChoiceInsulatedGlass();

		Class<? extends ParametricModel> doorModel = setDoorModel(choiceSlabModel);
		removeGlassType();

		if (choiceSlabModel != null)
		{
			sb.setLength(0);
			sb.append("Select STAINEDGLASS.* from STAINEDGLASS ");
			sb.append("inner join AVAILABILITY_STAINEDGLASS on AVAILABILITY_STAINEDGLASS.code = STAINEDGLASS.dimFormCode");
			sb.append(" where UPPER(").append(doorModel.getSimpleName()).append(") = ").append("'X'");

			addDynamicEnumQuery(PROPERTYNAME_CHOICESTAINEDGLASS, sb.toString());

			sb.setLength(0);
			sb.append("Select THERMOS.* from THERMOS ");
			sb.append("inner join AVAILABILITY_STAINEDGLASS on AVAILABILITY_STAINEDGLASS.code = THERMOS.code");
			sb.append(" where UPPER(").append(doorModel.getSimpleName()).append(") = ").append("'X'");

			addDynamicEnumQuery(PROPERTYNAME_CHOICEINSULATEDGLASS, sb.toString());
		}
		else
		{
			addDynamicEnumQuery(PROPERTYNAME_CHOICESTAINEDGLASS, "");
			addDynamicEnumQuery(PROPERTYNAME_CHOICEINSULATEDGLASS, "");
		}
	}

	@Override
	public void afterChoicePanelChanged(ChoicePanel choicePanel)
	{
		super.afterChoicePanelChanged(choicePanel);

		if (choicePanel != null)
		{
			// FIXME Don't know what to do with this, it does not compile
			// ConfigurationManager.insertConfigurableProperties(this, "Configurable.choiceSlab360.code", "FLUSH");
			removeDynamicEnumDefaultValues(PROPERTYNAME_CHOICESLAB360);
			manageSlabModel(choicePanel.getParametricModelClass());
		}
		else
		{
			manageSlabModel(null);
		}
	}

	@Override
	public void afterGlassTypeChanged(GlassType glassType)
	{
		super.afterGlassTypeChanged(glassType);
		makeDisabled(PROPERTYNAME_CHOICESTAINEDGLASS);
		makeDisabled(PROPERTYNAME_CHOICEINSULATEDGLASS);

		if (glassType != null && glassType == GlassType.WITHSTAINEDGLASS)
		{
			makeEnabled(PROPERTYNAME_CHOICESTAINEDGLASS);
			makeVisible(PROPERTYNAME_CHOICESTAINEDGLASS);
		}
		else if (glassType != null && glassType == GlassType.WITHTHERMOS)
		{
			makeEnabled(PROPERTYNAME_CHOICEINSULATEDGLASS);
			makeVisible(PROPERTYNAME_CHOICEINSULATEDGLASS);
		}
	}

	/*
	 * (non-Javadoc) WARNING -- Do not change this values. They are used by the JUnit tests.
	 */
	public Measure<Length> getSlabHeadGap()
	{
		return Units.inch(0.375);
	}

	/*
	 * (non-Javadoc) WARNING -- Do not change this values. They are used by the JUnit tests.
	 */
	@Override
	public Measure<Length> getSillGap()
	{
		return Units.inch(0.25);
	}
}
