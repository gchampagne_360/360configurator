// $Id: $

package com.client360.configuration.wad;

import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.RollingShutterMainBlade;
import com.netappsid.wadconfigurator.StandardColor;


public class RollingShutterCurtain extends com.client360.configuration.wad.RollingShutterCurtainBase
{
    // Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
    public RollingShutterCurtain(com.netappsid.erp.configurator.Configurable parent)
    {
        super(parent);
    }
    
    @Override
    public void setDefaultValues()
    {
    	super.setDefaultValues();
    	
    	setDefaultBladesHeight(Units.inch(1d));
    }

	@Override
	protected Class<? extends StandardColor> getStandardColorClass()
	{
		return com.client360.configuration.wad.StandardColor.class;
	}

	@Override
	protected Class<? extends RollingShutterMainBlade> getRollingShutterMainBladeClass()
	{
		return com.client360.configuration.wad.RollingShutterMainBlade.class;
	}
}
