// $Id: PatioDoor.java,v 1.5 2011-04-27 12:09:14 jddaigle Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.commonutils.geo.CornerType;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.rendering.common.advanced.Drawing;
import com.netappsid.wadconfigurator.MeetingRail;
import com.netappsid.wadconfigurator.Sash;
import com.netappsid.wadconfigurator.SlidingProductMeetingRail;

@SuppressWarnings("serial")
public class PatioDoor extends com.client360.configuration.wad.PatioDoorBase
{
	public PatioDoor(Configurable parent)
	{
		super(parent);
	}

	@Override
	public CornerType[] getCornerType()
	{
		return null;
	}

	@Override
	public Measure<Length>[] getFrameBorder()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length>[] getMullionOffset()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MeetingRail[] meetingRailMakeArray(int nbSections)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Sash[] sashMakeArray(int nbSections)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String colorChartNameExterior()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String colorChartNameInterior()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String productCode()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean useAutomaticGrilleAlignement()
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Drawing getCad()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length>[] getSashOffset(int railNumber, String sashRole)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Measure<Length>[] getGlassOffset()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SlidingProductMeetingRail[] interiorMeetingRailMakeArray(int nbSections)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(Object[] params)
	{
		// TODO Auto-generated method stub

	}
}
