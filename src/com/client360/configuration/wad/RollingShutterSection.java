// $Id: $

package com.client360.configuration.wad;

import com.netappsid.wadconfigurator.RollingShutterCurtain;
import com.netappsid.wadconfigurator.RollingShutterGuideRail;


public class RollingShutterSection extends com.client360.configuration.wad.RollingShutterSectionBase
{
    // Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
    public RollingShutterSection(com.netappsid.erp.configurator.Configurable parent)
    {
        super(parent);
    }

	@Override
	protected Class<? extends RollingShutterCurtain> getRollingShutterCurtainClass()
	{
		return com.client360.configuration.wad.RollingShutterCurtain.class;
	}

	@Override
	protected Class<? extends RollingShutterGuideRail> getRollingShutterGuideRailClass()
	{
		return com.client360.configuration.wad.RollingShutterGuideRail.class;
	}
}
