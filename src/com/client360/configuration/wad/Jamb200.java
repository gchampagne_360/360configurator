// $Id: Jamb200.java,v 1.2 2011-04-27 12:09:13 jddaigle Exp $

package com.client360.configuration.wad;

import com.netappsid.erp.configurator.Configurable;

public class Jamb200 extends com.client360.configuration.wad.Jamb200Base
{
	public Jamb200(Configurable parent)
	{
		super(parent);
	}

	double coteB = 200;
	double coteC = 25;

	@Override
	public double getCoteA()
	{
		return coteB + coteC;
	}
}
