// $Id: HungMain.java,v 1.5 2011-06-10 14:23:19 sboule Exp $

package com.client360.configuration.wad;

import static com.client360.configuration.wad.utils.FenergicUtils.PVCColorChart;

import com.netappsid.erp.configurator.Configurable;
import com.netappsid.wadconfigurator.Division;

public class HungMain extends com.client360.configuration.wad.HungMainBase
{
	public HungMain(Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
	}

	@Override
	public Division[] divisionMakeArray(int nbDivisions)
	{
		return makeArray(HungMainDivision.class, nbDivisions, this);
	}

	@Override
	public String colorChartNameExterior()
	{
		return PVCColorChart;
	}

	@Override
	public String colorChartNameInterior()
	{
		return PVCColorChart;
	}

	@Override
	public String productCode()
	{
		return "HPVC";
	}

	@Override
	public boolean useAutomaticGrilleAlignement()
	{
		return false;
	}

	@Override
	public String toString()
	{
		return getRenderingNumber() + " - " + clientTraductionBundle.getString("HungPvc");
	}

	@Override
	public void init(Object[] params)
	{
		// TODO Auto-generated method stub

	}
}
