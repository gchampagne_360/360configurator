package com.client360.configuration.wad.exception;

public class ProductDataMissingException extends RuntimeException
{

	public ProductDataMissingException()
	{
		super();
	}

	public ProductDataMissingException(String message)
	{
		super(message);
	}
}
