// $Id: SteelDoorOptions.java,v 1.1 2011-04-27 12:09:14 jddaigle Exp $

package com.client360.configuration.wad;

public class SteelDoorOptions extends com.client360.configuration.wad.SteelDoorOptionsBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public SteelDoorOptions(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	// Add custom overrides here
}
