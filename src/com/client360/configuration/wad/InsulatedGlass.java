// $Id: InsulatedGlass.java,v 1.11 2011-08-19 19:26:11 sboule Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.client360.configuration.wad.enums.ChoiceAdvancedGrilles;
import com.client360.configuration.wad.enums.ChoiceParametricModel;
import com.netappsid.commonutils.math.Units;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.wadconfigurator.AdvancedSectionGrilles;

public class InsulatedGlass extends com.client360.configuration.wad.InsulatedGlassBase
{
	public InsulatedGlass(Configurable parent)
	{
		super(parent);
	}

	/**
	 * Filter the choices to Grilles types only (not muntins).
	 */
	public boolean isChoiceAdvancedGrillesEnabled(ChoiceAdvancedGrilles grille)
	{
		return grille.glass;
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		makeInvisible(PROPERTYNAME_EXTERIORCOLOR);
		makeInvisible(PROPERTYNAME_INTERIORCOLOR);

		// setDefaultInteriorColor(createRedirectedConfigurableInstance(StandardColor.class, this));
		// setDefaultExteriorColor(createRedirectedConfigurableInstance(StandardColor.class, this));
	}

	@Override
	public void activateListener()
	{
		super.activateListener();
		// addAssociationPropertyChangeListener(PROPERTYNAME_DIMENSION, Dimension.PROPERTYNAME_WIDTH, "showDimension");
		// addAssociationPropertyChangeListener(PROPERTYNAME_DIMENSION, Dimension.PROPERTYNAME_HEIGHT, "showDimension");

	}

	public void showDimension()
	{
		if (getDimension().getWidth() != null && getDimension().getHeight() != null)
		{
			System.out.println("[" + getIndex() + "]T-W:" + getDimension().getWidth().doubleValue(Units.MM));
			System.out.println("[" + getIndex() + "]T-H:" + getDimension().getHeight().doubleValue(Units.MM));
		}
	}

	@Override
	public Measure<Length> getGrilleOffset()
	{
		return ProductData.getValue(ProductType.INSULATED_GLASS, Data.GLASS_TO_GRILLS_OFFSET_SPACERS);
	}

	@Override
	/**
	 * Assign the right AdvancedSectionGrilles.
	 */
	public void afterChoiceAdvancedGrillesChanged(ChoiceAdvancedGrilles choiceAdvancedGrilles)
	{
		if (choiceAdvancedGrilles == null)
		{
			setAdvancedSectionGrilles((AdvancedSectionGrilles) null);
		}
		else
		{
			setAdvancedSectionGrilles(choiceAdvancedGrilles.configClass);
		}
	}

	@Override
	/**
	 * Return the selected AdvancedSectionGrilles class.
	 */
	public Class<? extends AdvancedSectionGrilles> getAdvancedGrillesClass()
	{
		if (getChoiceAdvancedGrilles() == null)
		{
			return null;
		}
		return getChoiceAdvancedGrilles().configClass;
	}

	@Override
	/**
	 * Create and set the selected parametric model for the kick panel.
	 * @param choiceParametricModel - Choice made by the user.
	 */
	public void afterChoiceParametricModelChanged(ChoiceParametricModel choiceParametricModel)
	{
		super.afterChoiceParametricModelChanged(choiceParametricModel);

		deleteKickPanelModel();

		if (choiceParametricModel != null)
		{
			setKickPanelModel(choiceParametricModel.configClass);
		}
	}

	@Override
	public boolean beforePropertyReplicationFrom(String propertyName, Configurable source)
	{
		// if (propertyName.equals(PROPERTYNAME_ADVANCEDSECTIONGRILLES))
		// {
		// return false;
		// } /* end if */

		return super.beforePropertyReplicationFrom(propertyName, source);
	}

	@Override
	public void init(Object[] params)
	{
		// TODO Auto-generated method stub

	}
}
