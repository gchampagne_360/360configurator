// $Id: Hardware.java,v 1.2 2011-04-27 12:09:14 jddaigle Exp $

package com.client360.configuration.wad;

import com.client360.configuration.wad.enums.ChoiceHinge;
import com.netappsid.erp.configurator.Configurable;

public class Hardware extends com.client360.configuration.wad.HardwareBase
{

	public Hardware(Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
		setChoiceHinge(ChoiceHinge.ELLIPSE);
	}

	/**
	 * @param ChoiceHinge
	 *            choiceHinge
	 * @return true / false
	 */
	public boolean isChoiceHingeEnabled(ChoiceHinge choiceHinge)
	{
		// boolean isChoiceHingeEnabled = false;
		// if(getParent() != null && getParent() instanceof CasementMilanoPvc)
		// {
		// isChoiceHingeEnabled = true;
		// }
		// else if(getParent() != null && getParent() instanceof CasementVisionPvc &&
		// choiceHinge == ChoiceHinge.ELLIPSE)
		// {
		// isChoiceHingeEnabled = true;
		// }
		return false;
	}

}
