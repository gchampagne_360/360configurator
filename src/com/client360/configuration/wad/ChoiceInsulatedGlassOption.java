// $Id: ChoiceInsulatedGlassOption.java,v 1.2 2011-04-27 12:09:14 jddaigle Exp $

package com.client360.configuration.wad;

import com.netappsid.erp.configurator.Configurable;

public class ChoiceInsulatedGlassOption extends com.client360.configuration.wad.ChoiceInsulatedGlassOptionBase
{

	public ChoiceInsulatedGlassOption(Configurable parent)
	{
		super(parent);
	}

	@Override
	public String toString()
	{
		return getGlassType() + " " + getGlassTreatment() + " " + getEnergeticGlassTreatment() + " " + getGlassThickness() + " " + getTint();
	}
}
