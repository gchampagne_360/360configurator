// $Id: Combination.java,v 1.4 2011-04-27 12:09:14 jddaigle Exp $

package com.client360.configuration.wad;

import com.netappsid.erp.configurator.Configurable;

public class Combination extends com.client360.configuration.wad.CombinationBase
{
	public Combination(Configurable parent)
	{
		super(parent);
	}

	private boolean isHybrid = false;

	public boolean isWoodenBoxVisible()
	{
		return false;
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		makeInvisible(PROPERTYNAME_CAPPINGEXTERIOR);
		makeInvisible(PROPERTYNAME_CAPPINGINTERIOR);
	}

	@Override
	public void afterConfigurableAdded(Configurable configurable)
	{
		super.afterConfigurableAdded(configurable);
		if (configurable.getClass().getSimpleName().toLowerCase().contains("hybrid"))
		{
			isHybrid = true;
		}
		else
		{
			isHybrid = false;
		}
	}

	public String colorChartNameExterior()
	{
		if (isHybrid)
		{
			return "ALU";
		}
		else
		{
			return "PVC";
		}
	}

	public boolean isJunctionEnabled()
	{
		return true;
	}

	@Override
	public Configurable getDefaultSelectedConfigurable()
	{
		return getConfigurables().get(0);
	}
}
