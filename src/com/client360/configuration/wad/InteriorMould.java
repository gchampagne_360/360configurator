// $Id: InteriorMould.java,v 1.2 2011-04-27 12:09:13 jddaigle Exp $

package com.client360.configuration.wad;

import com.netappsid.erp.configurator.Configurable;

public class InteriorMould extends com.client360.configuration.wad.InteriorMouldBase
{
	public InteriorMould(Configurable parent)
	{
		super(parent);
	}

	// Add custom overrides here
	@Override
	public boolean isBlowingEnabled()
	{
		return true;
	}
}
