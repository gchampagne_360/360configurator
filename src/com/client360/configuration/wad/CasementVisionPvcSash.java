// $Id: CasementVisionPvcSash.java,v 1.10 2011-09-07 19:04:34 sboule Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.netappsid.commonutils.geo.CornerType;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.rendering.common.advanced.Drawing;
import com.netappsid.wadconfigurator.SashSection;
import com.netappsid.wadconfigurator.enums.ChoiceSashType;
import com.netappsid.wadconfigurator.enums.ChoiceSwing;

public class CasementVisionPvcSash extends com.client360.configuration.wad.CasementVisionPvcSashBase
{
	private static final long serialVersionUID = 7811154307557476129L;

	public CasementVisionPvcSash(Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
		// setDefaultProfiles(getClientProfiles());
		// setDefaultGlazingBeadProfiles(getClientGlazingBeadProfiles());

		setDefaultInteriorColor(createRedirectedConfigurableInstance(StandardColor.class, this));
		setDefaultExteriorColor(createRedirectedConfigurableInstance(StandardColor.class, this));
	}

	@Override
	public Measure<Length> getSectionMaxWidth()
	{
		// The maximum width should not be larger than the casement - (min * nbSections).
		return ((CasementVisionPvc) getParent()).getDimension().getWidth().minus(getSectionMinWidth().times(((CasementVisionPvc) getParent()).getNbSections()));
	}

	@Override
	public Measure<Length> getSectionMinWidth()
	{
		// The minimum width should be at least the frame to glass offset.
		return ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_GLASS_OFFSET_SPACERS)[3].times(6);
	}

	@Override
	public Measure<Length> getSectionMaxHeight()
	{
		// The maximum width should not be larger than the casement - (min * nbSections).
		return ((CasementVisionPvc) getParent()).getDimension().getHeight()
				.minus(getSectionMinHeight().times(((CasementVisionPvc) getParent()).getNbSections()));
	}

	@Override
	public Measure<Length> getSectionMinHeight()
	{
		// The minimum width should be at least the frame to glass offset.
		return ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_GLASS_OFFSET_SPACERS)[3].times(6);
	}

	@Override
	public CornerType[] getCornerType()
	{
		ChoiceSwing choiceSwing = ((CasementVisionPvc) getParent()).getChoiceSwing();

		if (ChoiceSwing.INSWING.equals(choiceSwing))
		{
			return ProductData.getCornerType(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_CORNERTYPE);
		}
		else
		{
			return ProductData.getCornerType(ProductType.CASEMENT_VISION_PVC_OUT, Data.SASH_CORNERTYPE);
		}

	}

	/**
	 * @see com.client360.configuration.wad.CasementMainSash#getSashFrameThickness()
	 */
	@Override
	public Measure<Length>[] getSashFrameThickness()
	{
		return ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_BORDER_SPACERS);
	}

	@Override
	public Measure<Length>[] getVisibleGlassOffsetSpacer()
	{
		if (getChoiceSashType() == ChoiceSashType.FIXED_NOSASH)
		{
			return ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_VISIBLEGLASS);
		}
		else
		{
			return ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS);
		}
	}

	/**
	 * @see com.netappsid.wadconfigurator.Sash#getGlazingBeadThickness()
	 */
	@Override
	public Measure<Length>[] getGlazingBeadThickness()
	{
		return ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.GLAZINGBEAD_BORDER_SPACERS);
	}

	/**
	 * @see com.netappsid.wadconfigurator.Sash#getClientProfiles()
	 */
	// public Profile[] getClientProfiles()
	// {
	// if(getChoiceSashType() != null && getChoiceSashType() == ChoiceSashType.RIGHTOPERATING)
	// {
	// return ProductData.getProfiles(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_PROFILES_FC);
	// }
	// else
	// {
	// return ProductData.getProfiles(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_PROFILES);
	// }
	// }
	//
	// public Profile[] getClientGlazingBeadProfiles()
	// {
	// return ProductData.getProfiles(ProductType.CASEMENT_VISION_PVC_IN, Data.GLASINGBEAD_PROFILE);
	// }

	@Override
	public void afterChoiceSashTypeChanged(ChoiceSashType choiceSashType)
	{
		super.afterChoiceSashTypeChanged(choiceSashType);
		// setDefaultProfiles(getClientProfiles());
		updateRendering();

		CasementVisionPvc casement = getParent(CasementVisionPvc.class); 
		if (casement != null)
		{
			casement.setSashDimension();
		}
	}

	@Override
	public Drawing getCad()
	{
		// Drawing frame;
		//
		// if(getChoiceSashType() == ChoiceSashType.LEFTSEMIOPERATING || getChoiceSashType() == ChoiceSashType.RIGHTSEMIOPERATING)
		// {
		// frame = ProductData.getDrawing(ProductType.CASEMENT_VISION_PVC, Data.DRAWING_SASHASTRAGAL);
		// }
		// else
		// {
		// frame = ProductData.getDrawing(ProductType.CASEMENT_VISION_PVC, Data.DRAWING_SASH);
		// }

		// Drawing frame;
		//
		// if(getChoiceSashType() != null && getChoiceSashType() == ChoiceSashType.RIGHTOPERATING)
		// {
		// frame = new CadVantailFC();
		// }
		// else
		// {
		// frame = new CadVantail();
		// }
		//
		// return frame;

		// return new DefaultSashCad();

		return null;
	}

	@Override
	public SashSection sashSectionClass()
	{
		return new CasementVisionPvcSashSection(this);
	}
}
