// $Id: $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.netappsid.rendering.common.advanced.Drawing;

public class OdaceMeneau extends com.client360.configuration.wad.OdaceMeneauBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public OdaceMeneau(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	@Override
	public Drawing getCad()
	{
		return null;
	}

	@Override
	public Measure<Length> getLeftSashPositionOffset()
	{
		return ProductData.getValue(ProductType.ODACE, Data.LEFT_MULLION_TO_SASH);
	}

	@Override
	public Measure<Length> getRightSashPositionOffset()
	{
		return ProductData.getValue(ProductType.ODACE, Data.RIGHT_MULLION_TO_SASH);
	}

	@Override
	public Measure<Length> getLeftGlassPositionOffset()
	{
		return ProductData.getValue(ProductType.ODACE, Data.RIGHT_MULLION_TO_GLASS);
	}

	@Override
	public Measure<Length> getRightGlassPositionOffset()
	{
		return ProductData.getValue(ProductType.ODACE, Data.RIGHT_MULLION_TO_GLASS);
	}

	@Override
	public Measure<Length> getWidthOS()
	{
		return ProductData.getValue(ProductType.ODACE, Data.MULLION_WIDTH_OS);
	}

	@Override
	public Measure<Length> getWidth()
	{
		return ProductData.getValue(ProductType.ODACE, Data.MULLION_WIDTH);
	}

}
