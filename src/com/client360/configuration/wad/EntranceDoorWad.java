// $Id: $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.commonutils.geo.CornerType;
import com.netappsid.rendering.common.advanced.Drawing;
import com.netappsid.wadconfigurator.Door;
import com.netappsid.wadconfigurator.DoorMullionFix;
import com.netappsid.wadconfigurator.Opening;
import com.netappsid.wadconfigurator.SideLight;


public class EntranceDoorWad extends com.client360.configuration.wad.EntranceDoorWadBase
{
    // Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
    public EntranceDoorWad(com.netappsid.erp.configurator.Configurable parent)
    {
        super(parent);
    }

	@Override
	public DoorMullionFix[] makeArrayDoorMullionFix(int nbMullionFix)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SideLight[] makeArraySideLight(int nbSideLight)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Opening[] makeArrayOpening(int nbOpening)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Opening[] makeArraySideliteOpening(int nbOpening)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Door[] makeArrayDoor(int nbDoor)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CornerType[] getCornerType()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length> getTopSpacerOS()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length> getTopSpacer()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length> getJambWidthRightOS()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length> getJambWidthRight()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length> getJambWidthLeftOS()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length> getJambWidthLeft()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length> getMullionFixWidthOS()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length> getMullionFixWidth()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length> getZMullionFonctionalWidth()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length> getZMullionWidth()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length> getMullionMobileWidthOS()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length> getMullionMobileWidth()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length> getSillThickness()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Drawing getCad()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String productCode()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String colorChartNameExterior()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String colorChartNameInterior()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(Object[] params)
	{
		// TODO Auto-generated method stub

	}
    
    // Add custom overrides here
}
