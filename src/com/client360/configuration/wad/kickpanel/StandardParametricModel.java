package com.client360.configuration.wad.kickpanel;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.netappsid.cad.profile.SteelDoorProfileA;
import com.netappsid.cad.profile.SteelDoorProfileB;
import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

/**
 * Kick panel parametric model example.
 * 
 * @author sboule
 */
public class StandardParametricModel extends ParametricModel
{

	@Override
	/**
	 * Set the unit of measure for the model.
	 */
	protected Unit<Length> getUnit()
	{
		return Units.INCH;
	}

	@Override
	/**
	 * Setup of gaps and lines for the model.
	 */
	protected void load()
	{
		addGaps(5d + 1d / 8d, 23d);

		addLineDownOffset(1, top, 4d);
		addLineUpOffset(2, sill, 4d);

		addLineRightOffset(3, left, 6d);
		addLineLeftOffset(4, right, 6d);

		addLineRightOffset(5, 3, getVConstructionLine().get(right) / 4d);
		addLineLeftOffset(6, 4, getVConstructionLine().get(right) / 4d);

		addFormRectangle(3, 5, 1, 2); // Left,Right,Top,Bottom
		addFormRectangle(6, 4, 1, 2);

		addSpacer(2, 2, 2, 2);
		addSpacer(2, 2, 2, 2);

		addProfile(new SteelDoorProfileA(), new SteelDoorProfileB());
		addProfile(new SteelDoorProfileA(), new SteelDoorProfileB());
	}
}
