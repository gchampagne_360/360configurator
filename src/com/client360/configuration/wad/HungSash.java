// $Id: HungSash.java,v 1.5 2011-04-27 12:09:13 jddaigle Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.netappsid.commonutils.geo.CornerType;
import com.netappsid.wadconfigurator.SashSection;

public class HungSash extends com.client360.configuration.wad.HungSashBase
{
	public HungSash(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	@Override
	public SashSection sashSectionClass()
	{
		return new HungSashSection(this);
	}

	@Override
	public Measure<Length>[] getGlazingBeadThickness()
	{
		return ProductData.getSpacer(ProductType.HUNG_PVC, Data.FRAME_TO_EXT_GLAZINGBEAD_OFFSET_SPACERS);
	}

	@Override
	public Measure<Length>[] getSashFrameThickness()
	{
		if (getIndex() == 0)
		{
			return ProductData.getSpacer(ProductType.HUNG_PVC, Data.EXT_SASH_BORDER_SPACERS);
		}
		else
		{
			return ProductData.getSpacer(ProductType.HUNG_PVC, Data.INT_SASH_BORDER_SPACERS);
		}
	}

	@Override
	public CornerType[] getCornerType()
	{
		return ProductData.getCornerType(ProductType.HUNG_PVC, Data.SASH_CORNERTYPE);
	}

	@Override
	public Measure<Length>[] getVisibleGlassOffsetSpacer()
	{
		return ProductData.getSpacer(ProductType.HUNG_PVC, Data.VISIBLE_GLASS_OFFSET);
	}
}
