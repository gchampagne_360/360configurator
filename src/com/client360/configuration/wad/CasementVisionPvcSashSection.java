// $Id: CasementVisionPvcSashSection.java,v 1.12 2011-08-12 18:32:51 sboule Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.client360.configuration.wad.enums.ChoiceAdvancedGrilles;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.wadconfigurator.AdvancedSectionGrilles;
import com.netappsid.wadconfigurator.enums.ChoiceSashType;

public class CasementVisionPvcSashSection extends com.client360.configuration.wad.CasementVisionPvcSashSectionBase
{

	public CasementVisionPvcSashSection(Configurable parent)
	{
		super(parent);
	}

	@Override
	public Measure<Length>[] getGlassOffset()
	{
		if (getChoiceSashType() == ChoiceSashType.FIXED_NOSASH)
		{
			return ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_TO_GLASS);
		}
		else
		{
			return ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS);
		}
	}

	/**
	 * Filter the choices to Muntins types only (TDL and SDL).
	 */
	public boolean isChoiceAdvancedGrillesEnabled(ChoiceAdvancedGrilles grille)
	{
		return !grille.glass;
	}

	@Override
	public void afterChoiceAdvancedGrillesChanged(ChoiceAdvancedGrilles choiceAdvancedGrilles)
	{
		if (choiceAdvancedGrilles == null) // No selection
		{
			setAdvancedSectionGrilles((AdvancedSectionGrilles) null);
		}
		else
		{
			removeAdvancedSectionGrilles();
			setAdvancedSectionGrilles(choiceAdvancedGrilles.configClass);
		}
	}

	@Override
	public Measure<Length>[] getSplittingGrillesOffset()
	{
		return ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SPLITTING_GRILLES_OFFSET);
	}

	@Override
	public Measure<Length>[] getNoSplittingGrillesOffset()
	{
		return ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.NOSPLITTING_GRILLES_OFFSET);
	}
}
