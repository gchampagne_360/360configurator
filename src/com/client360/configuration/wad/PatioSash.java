// $Id: PatioSash.java,v 1.4 2011-04-27 12:09:13 jddaigle Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.commonutils.geo.CornerType;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.rendering.common.advanced.Drawing;
import com.netappsid.wadconfigurator.SashSection;

public class PatioSash extends com.client360.configuration.wad.PatioSashBase
{
	public PatioSash(Configurable parent)
	{
		super(parent);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected Measure<Length>[] getSashFrameThickness()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Measure<Length>[] getGlazingBeadThickness()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Drawing getCad()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SashSection sashSectionClass()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CornerType[] getCornerType()
	{
		// TODO Auto-generated method stub
		return null;
	}

}
