// $Id: StandardColor.java,v 1.4 2011-06-10 14:23:19 sboule Exp $

package com.client360.configuration.wad;

import java.awt.image.BufferedImage;

import com.client360.configuration.wad.enums.ChoiceColorType;
import com.client360.configuration.wad.enums.ChoiceWoodTone;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.rendering.common.advanced.Filling;

/**
 * Example
 * 
 */
public class StandardColor extends com.client360.configuration.wad.StandardColorBase
{
	public StandardColor(Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		setDefaultChoiceColorType(ChoiceColorType.STANDARD);

	}

	@Override
	public void afterChoiceColorTypeChanged(ChoiceColorType choiceColorType)
	{
		super.afterChoiceColorTypeChanged(choiceColorType);

		// Set the filter to show only the current chart colors.
		removeDynamicEnumDefaultValues(PROPERTYNAME_CHOICECOLOR);
		addDynamicEnumDefaultValue(PROPERTYNAME_CHOICECOLOR, ChoiceColor.PROPERTYNAME_COLORCHART, choiceColorType.toString());

		// Select the color chart.
		removeDynamicEnumDefaultSelections(PROPERTYNAME_CHOICECOLOR);
		addDynamicEnumDefaultSelection(PROPERTYNAME_CHOICECOLOR, ChoiceColor.PROPERTYNAME_COLORCHART, choiceColorType.toString());
	}

	/**
	 * Initialize the color chart attributes according to the selected standard color choice
	 * 
	 * @param ChoiceColor
	 *            - the selected standard color choice
	 */
	private void setColors(ChoiceColor choiceColor, ChoiceWoodTone choiceWoodTone)
	{
		if (choiceWoodTone != null && choiceColor != null && !choiceColor.getTexturePath().equals(""))
		{
			setRed(choiceWoodTone.red);
			setGreen(choiceWoodTone.green);
			setBlue(choiceWoodTone.blue);
		}
		else if (choiceColor != null && (choiceColor.getRed() != null || choiceColor.getGreen() != null || choiceColor.getBlue() != null))
		{
			setRed(choiceColor.getRed());
			setGreen(choiceColor.getGreen());
			setBlue(choiceColor.getBlue());
		}
		else
		{
			setRed(255);
			setGreen(255);
			setBlue(255);
		}
	}

	@Override
	public void beforeChoiceColorChanged(ChoiceColor choiceColor)
	{
		super.beforeChoiceColorChanged(choiceColor);
		setColors(choiceColor, getChoiceWoodTone());
	}

	@Override
	public void beforeChoiceWoodToneChanged(ChoiceWoodTone choiceWoodTone)
	{
		super.beforeChoiceWoodToneChanged(choiceWoodTone);
		setColors(getChoiceColor(), choiceWoodTone);
	}

	@Override
	public BufferedImage getTextureImage()
	{
		if (getChoiceColor() != null && getChoiceColor().getTexturePath() != null && getChoiceColor().getTexturePath() != "")
		{
			return Filling.loadTextureImage(getChoiceColor().getTexturePath(), this.getClass());
		}
		return null;
	}

	@Override
	public String toString()
	{
		if (getChoiceColor() != null)
			return getChoiceColor().getDescription();
		else
			return null;
	}

}
