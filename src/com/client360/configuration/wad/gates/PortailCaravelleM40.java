package com.client360.configuration.wad.gates;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.cad.CadBarreau;
import com.netappsid.commonutils.geo.CornerType;
import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class PortailCaravelleM40 extends ParametricModel
{

	public static final String FORME_GRILLAGE_HAUT = "Forme grillage du haut";
	public static final String ARRAY_GRILLAGE_HAUT = "Array grillage du haut";
	public static final String ARRAY_GRILLAGE_HAUT_POSITION = "Array grillage du haut pour les positions";
	public static final String CENTRE_BARRES_GAUCHE = "Centre des barres Gauches";
	public static final String CENTRE_BARRES_DROITE = "Centre des barres Droites";
	public static final String POSITION_BARRES_GAUCHE = "Position des barres Gauches";
	public static final String POSITION_BARRES_DROITE = "Position des barres Droites";
	public static final String GH1 = "Grille H1";
	public static final String GH2 = "Grille H2";
	public static final String PH1 = "Panneau H1";
	public static final String PH2 = "Panneau H2";
	public static final String PANNEAU = "Panneau Bas";
	public static final String POSITION_MENEAU = "Position meneau";
	public static final String PANNEAU_TEXTURE_GAUCHE = "Panneau Bas gauche pour texture";
	public static final String PANNEAU_TEXTURE_DROIT = "Panneau Bas droit pour texture";

	@Override
	protected void load()
	{
		addGaps(35, 80, 80);
		addLineDownOffset(GH2, patternTop, 528);
		addLineUpOffset(GH1, patternSill, 505);
		addLineRightOffset(CENTRE_BARRES_GAUCHE, left, 40);
		addLineLeftOffset(CENTRE_BARRES_DROITE, right, 40);
		addLineLeftOffset(POSITION_BARRES_GAUCHE, CENTRE_BARRES_GAUCHE, 40);
		addLineLeftOffset(POSITION_BARRES_DROITE, CENTRE_BARRES_DROITE, 40);
		addLineLeftOffset(POSITION_MENEAU, vPatternCenter, 40);

		addLineUpOffset(PH2, patternSill, 551);
		addLineDownOffset(PH1, patternTop, 478);

		addRightLineOffsetArray(ARRAY_GRILLAGE_HAUT, CENTRE_BARRES_GAUCHE, CENTRE_BARRES_DROITE, 125d);
		addRightLineOffsetArray(ARRAY_GRILLAGE_HAUT_POSITION, POSITION_BARRES_GAUCHE, POSITION_BARRES_DROITE, 125d);

		addFormRectangle(left, right, top, sill);
		addFormElongatedArc2(FORME_GRILLAGE_HAUT, patternLeft, patternRight, patternTop, GH1, GH2, true, false);
		addFormElongatedArc2(PANNEAU, patternLeft, patternRight, PH1, patternSill, PH2, false, true);
		addFormElongatedHalfArc2(PANNEAU_TEXTURE_GAUCHE, patternLeft, vPatternCenter, PH1, patternSill, PH2, null, true, false, true);
		addFormElongatedHalfArc2(PANNEAU_TEXTURE_DROIT, vPatternCenter, patternRight, PH1, patternSill, PH2, null, false, false, true);
		addDrawing(new CadBarreau(), POSITION_MENEAU, vPatternCenter, patternSill, PH2, Orientation.VERTICAL);

		addDrawingOnArray(CadBarreau.class, ARRAY_GRILLAGE_HAUT, ARRAY_GRILLAGE_HAUT_POSITION, FORME_GRILLAGE_HAUT);

		addSpacer(70, 80, 80, 80);
		addEmptySpacer();
		addSpacer(90, 20, 20, 20);
		addEmptySpacer();
		addEmptySpacer();

		addCornerType(CornerType.SHORT_CORNER, CornerType.HALF_CORNER, CornerType.HALF_CORNER, CornerType.LONG_CORNER);
		addDefaultCornerType();
		addCornerType(CornerType.SHORT_CORNER, CornerType.HALF_CORNER, CornerType.HALF_CORNER, CornerType.LONG_CORNER);
		addDefaultCornerType();
		addDefaultCornerType();

	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

}
