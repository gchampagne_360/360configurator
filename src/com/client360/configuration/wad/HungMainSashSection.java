// $Id: HungMainSashSection.java,v 1.5 2011-04-27 12:09:13 jddaigle Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.netappsid.erp.configurator.Configurable;

public class HungMainSashSection extends com.client360.configuration.wad.HungMainSashSectionBase
{

	public HungMainSashSection(Configurable parent)
	{
		super(parent);
	}

	@Override
	public Measure<Length>[] getGlassOffset()
	{
		return ProductData.getSpacer(ProductType.HUNG_PVC, Data.SASH_TO_GLASS_OFFSET_SPACERS);
	}

	@Override
	protected Measure<Length> getMullionToGlassOffset()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length>[] getSplittingGrillesOffset()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length>[] getNoSplittingGrillesOffset()
	{
		// TODO Auto-generated method stub
		return null;
	}
}
