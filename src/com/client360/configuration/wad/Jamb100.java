package com.client360.configuration.wad;

import com.netappsid.erp.configurator.Configurable;

public class Jamb100 extends com.client360.configuration.wad.Jamb100Base
{
	public Jamb100(Configurable parent)
	{
		super(parent);
	}

	double coteB = 110;
	double coteC = 10;

	@Override
	public double getCoteA()
	{
		return coteB - coteC;
	}
}
