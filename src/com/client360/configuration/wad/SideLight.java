// $Id: SideLight.java,v 1.4 2011-04-27 12:09:14 jddaigle Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.commonutils.math.Units;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.wadconfigurator.enums.ChoiceOpening;

public class SideLight extends com.client360.configuration.wad.SideLightBase
{

	public SideLight(Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
		makeInvisible(PROPERTYNAME_CHOICEHANDLESETSIDE);
		makeInvisible(PROPERTYNAME_HANDLESET);
	}

	// Add custom overrides here
	/*
	 * (non-Javadoc) WARNING -- Do not change this values. They are used by the JUnit tests.
	 */
	@Override
	public Measure<Length> getSlabGap()
	{
		if (this.getChoiceOpening() != null)
		{
			if (this.getChoiceOpening() == ChoiceOpening.FIXED)
			{
				return Units.inch(0.125);
			}
			else
			{
				return Units.inch(0.25);
			}
		}
		return Units.inch(0.0);
	}

	/*
	 * (non-Javadoc) WARNING -- Do not change this values. They are used by the JUnit tests.
	 */
	public Measure<Length> getSlabHeadGap()
	{
		return Units.inch(0.375);
	}

	/*
	 * (non-Javadoc) WARNING -- Do not change this values. They are used by the JUnit tests.
	 */
	@Override
	public Measure<Length> getSillGap()
	{
		return Units.inch(0.25);
	}
}
