// $Id: CasementMainSash.java,v 1.3 2011-09-07 19:04:33 sboule Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.netappsid.commonutils.geo.CornerType;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.rendering.common.advanced.Profile;

public abstract class CasementMainSash extends com.client360.configuration.wad.CasementMainSashBase
{

	public CasementMainSash(Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
		makeInvisible(PROPERTYNAME_INSTALLED);
		makeInvisible(PROPERTYNAME_SCREEN);

		setProfiles(getClientProfiles());
		setGlazingBeadProfiles(getClientGlazingBeadProfiles());
	}

	public final Profile[] getClientProfiles()
	{
		return ProductData.getProfiles(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_PROFILES);
	}

	public final Profile[] getClientGlazingBeadProfiles()
	{
		return ProductData.getProfiles(ProductType.CASEMENT_VISION_PVC_IN, Data.GLASINGBEAD_PROFILE);
	}

	@Override
	public void activateListener()
	{
		super.activateListener();
	}

	public abstract CornerType[] getCornerType();

	public Measure<Length>[] getGlazingOffset()
	{
		return null;
	}

	/**
	 * @see com.netappsid.wadconfigurator.Sash#getSashFrameThickness()
	 */
	@Override
	public Measure<Length>[] getSashFrameThickness()
	{
		return null;
	}

	public Measure<Length>[] getGlassOffset()
	{
		return null;
	}

	public abstract Measure<Length> getSectionMaxWidth();

	public abstract Measure<Length> getSectionMinWidth();
}
