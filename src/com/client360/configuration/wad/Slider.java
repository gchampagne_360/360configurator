// $Id: Slider.java,v 1.3 2011-05-31 19:41:12 sboule Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.rendering.common.advanced.Drawing;
import com.netappsid.wadconfigurator.SlidingProductMeetingRail;

@SuppressWarnings("serial")
public class Slider extends com.client360.configuration.wad.SliderBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public Slider(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	@Override
	public Measure<Length>[] getLMouldingOffset()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length>[] getLMouldingExteriorOffset()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length> getSashStopOffset()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length> getSashStopExteriorOffset()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Drawing getCad()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length>[] getSashOffset(int railNumber, String sashRole)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length>[] getGlassOffset()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SlidingProductMeetingRail[] interiorMeetingRailMakeArray(int nbSections)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(Object[] params)
	{
		// TODO Auto-generated method stub

	}
}
