package com.client360.configuration.wad.parts;

import java.awt.Color;

import com.client360.configuration.wad.tools.ToolD1;
import com.client360.configuration.wad.tools.ToolD2;
import com.netappsid.rendering.common.advanced.Graphic;
import com.netappsid.rendering.common.advanced.Machining;
import com.netappsid.rendering.common.advanced.RawMaterialSnapPoint;
import com.netappsid.rendering.common.advanced.WoodPart;

public class PartTestDoc extends WoodPart
{
	@Override
	protected void load()
	{
		addRawMaterial(33.4, new Graphic(Color.LIGHT_GRAY), Machining.LEFT_RIGHT);
		addTool(new ToolD2(), 5, 5, RawMaterialSnapPoint.TOPLEFT);
		addTool(new ToolD1(), 5, 5, RawMaterialSnapPoint.TOPRIGHT);
	}

	@Override
	public String getName()
	{
		return "Part123";
	}

	@Override
	public String getPath()
	{
		return "/images/cad/";
	}

}
