package com.client360.configuration.wad.parts;

import java.awt.Color;

import com.client360.configuration.wad.tools.ToolA1;
import com.client360.configuration.wad.tools.ToolA2;
import com.client360.configuration.wad.tools.ToolA3;
import com.netappsid.rendering.common.advanced.Graphic;
import com.netappsid.rendering.common.advanced.Machining;
import com.netappsid.rendering.common.advanced.RawMaterialSnapPoint;
import com.netappsid.rendering.common.advanced.WoodPart;

public class PartSill extends WoodPart
{
	@Override
	protected void load()
	{
		addRawMaterial(125, new Graphic(Color.LIGHT_GRAY), Machining.UP_DOWN);
		addTool(new ToolA1(), 10.11, 10, RawMaterialSnapPoint.TOPLEFT);
		addTool(new ToolA2(), 3.83, 10, RawMaterialSnapPoint.TOPRIGHT);
		addTool(new ToolA3(), 7.32, 10.95, RawMaterialSnapPoint.BOTTOMLEFT);
	}

	@Override
	public String getName()
	{
		return "Part123";
	}

	@Override
	public String getPath()
	{
		return "/images/cad/";
	}

}
