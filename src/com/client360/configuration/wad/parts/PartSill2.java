package com.client360.configuration.wad.parts;

import com.client360.configuration.wad.tools.ToolC1;
import com.client360.configuration.wad.tools.ToolC2;
import com.client360.configuration.wad.tools.ToolC3;
import com.client360.configuration.wad.tools.ToolC4;
import com.netappsid.commonutils.color.Color;
import com.netappsid.rendering.common.advanced.Graphic;
import com.netappsid.rendering.common.advanced.Machining;
import com.netappsid.rendering.common.advanced.RawMaterialSnapPoint;
import com.netappsid.rendering.common.advanced.WoodPart;

public class PartSill2 extends WoodPart
{

	@Override
	protected void load()
	{
		addRawMaterial(125, new Graphic(Color.SaddleBrown), Machining.UP_DOWN);
		addTool(new ToolC2(), 6.26, 10.33, RawMaterialSnapPoint.TOPLEFT);
		addTool(new ToolC1(), 9.86, 10.33, RawMaterialSnapPoint.TOPRIGHT);
		addTool(new ToolC3(), -71.04, 10.60, RawMaterialSnapPoint.TOPLEFT);
		addTool(new ToolC4(), -16.37, 19.93, RawMaterialSnapPoint.BOTTOMLEFT);
		addTool(new ToolC4(), -62.27, 19.93, RawMaterialSnapPoint.BOTTOMLEFT);
	}

	@Override
	public String getName()
	{
		return "Seuil #938A-Revision 2";
	}

	@Override
	public String getPath()
	{
		return "/images/cad/";
	}
}
