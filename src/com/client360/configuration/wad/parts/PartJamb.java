package com.client360.configuration.wad.parts;

import java.awt.Color;

import com.client360.configuration.wad.tools.ToolB1;
import com.client360.configuration.wad.tools.ToolB2;
import com.netappsid.rendering.common.advanced.Graphic;
import com.netappsid.rendering.common.advanced.Machining;
import com.netappsid.rendering.common.advanced.RawMaterialSnapPoint;
import com.netappsid.rendering.common.advanced.WoodPart;

public class PartJamb extends WoodPart
{
	@Override
	protected void load()
	{
		addRawMaterial(46.48, new Graphic(Color.LIGHT_GRAY), Machining.LEFT_RIGHT);
		addTool(new ToolB1(), 6.24, 9.2, RawMaterialSnapPoint.TOPLEFT);
		addTool(new ToolB2(), 6.25, 10, RawMaterialSnapPoint.TOPRIGHT);
	}

	@Override
	public String getName()
	{
		return "Part123";
	}

	@Override
	public String getPath()
	{
		return "/images/cad/";
	}

}
