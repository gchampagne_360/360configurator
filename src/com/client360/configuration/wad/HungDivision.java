// $Id: HungDivision.java,v 1.5 2011-04-27 12:09:13 jddaigle Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.netappsid.commonutils.geo.CornerType;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.rendering.common.advanced.Drawing;
import com.netappsid.wadconfigurator.MeetingRail;
import com.netappsid.wadconfigurator.Sash;

@SuppressWarnings("serial")
public class HungDivision extends com.client360.configuration.wad.HungDivisionBase
{

	public HungDivision(Configurable parent)
	{
		super(parent);
	}

	@Override
	public Drawing getCad()
	{
		return ProductData.getDrawing(ProductType.HUNG_PVC, Data.FRAME_DRAWING);
	}

	@Override
	public CornerType[] getCornerType()
	{
		return ProductData.getCornerType(ProductType.HUNG_PVC, Data.FRAME_CORNERTYPE);
	}

	@Override
	public Measure<Length>[] getFrameBorder()
	{
		return ProductData.getSpacer(ProductType.HUNG_PVC, Data.FRAME_BORDER_SPACERS);
	}

	@Override
	public Measure<Length>[] getSashOffset(int railNumber)
	{
		return ProductData.getSpacer(ProductType.HUNG_PVC, Data.FRAME_TO_SASH_OFFSET_SPACERS);
	}

	@Override
	public MeetingRail[] meetingRailMakeArray(int nbSections)
	{
		return makeArray(HungMeetingRail.class, nbSections, this);
	}

	@Override
	public Sash[] sashMakeArray(int nbSections)
	{
		return makeArray(HungSash.class, nbSections, this);
	}

	@Override
	public Measure<Length>[] getFixedSashOffset()
	{
		return ProductData.getSpacer(ProductType.HUNG_PVC, Data.FRAME_TO_SASH_OFFSET_SPACERS);
	}

	@Override
	public Measure<Length>[] getVisibleGlassOffsetSpacer()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(Object[] params)
	{
		// TODO Auto-generated method stub

	}
}
