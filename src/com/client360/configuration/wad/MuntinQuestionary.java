// $Id: MuntinQuestionary.java,v 1.2 2011-06-01 15:15:19 sboule Exp $

package com.client360.configuration.wad;

import java.util.Map;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.PartialDiamondGrillesQuestionary;
import com.netappsid.wadconfigurator.PartialPerimeterGrillesQuestionary;
import com.netappsid.wadconfigurator.PartialRegularGrillesQuestionary;
import com.netappsid.wadconfigurator.grilles.SeparableGrillesManager;

public class MuntinQuestionary extends com.client360.configuration.wad.MuntinQuestionaryBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public MuntinQuestionary(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		setDefaultSdlThickness(ProductData.getValue(ProductType.INTERNAL_MUNTIN, Data.INTERNAL_MUNTIN_SDL_THICKNESS));
		setDefaultTdlThickness(ProductData.getValue(ProductType.INTERNAL_MUNTIN, Data.INTERNAL_MUNTIN_THICKNESS));
	}

	@Override
	public void afterCalculate()
	{
		super.afterCalculate();
		// Map<String, Object> map = new HashMap<String, Object>();
		//
		// // Only horizontal SDL lines
		// if ((getSplittingBars() == null || getSplittingBars().length == 0) && (getNoSplittingBars() != null && getNoSplittingBars().length >= 1)
		// && (getNbSdlVertiBars() == null || getNbSdlVertiBars() == 0))
		// {
		// createPartialRegularGrille(map);
		// // createPartialPerimeterGrille(map);
		// // createPartialDiamondGrille(map);
		// } /* end if */
		//
		// // No SDL and one TDL
		// else if ((getSplittingBars() != null && getSplittingBars().length == 1) && (getNoSplittingBars() == null || getNoSplittingBars().length == 0))
		// {
		// map.put(ContourGrillesQuestionary.PROPERTYNAME_NBHORIZBARS, 4);
		// map.put(ContourGrillesQuestionary.PROPERTYNAME_NBVERTIBARS, 5);
		// map.put(ContourGrillesQuestionary.PROPERTYNAME_WIDTH, 1 / 4d);
		// map.put(ContourGrillesQuestionary.PROPERTYNAME_DISTANCE, 6d);
		//
		// createAutomaticGrilles(ContourGrillesQuestionary.class, map, 0);
		// } /* end if */
		//
		// // No SDL and TDL > 1
		// else if ((getSplittingBars() != null && getSplittingBars().length > 1) && (getNoSplittingBars() == null || getNoSplittingBars().length == 0))
		// {
		// map.put(RegularGrillesQuestionary.PROPERTYNAME_NBHORIZBARS, 2);
		// map.put(RegularGrillesQuestionary.PROPERTYNAME_NBVERTIBARS, 2);
		// map.put(RegularGrillesQuestionary.PROPERTYNAME_WIDTH, 1 / 4d);
		//
		// createAutomaticGrilles(RegularGrillesQuestionary.class, map, 0);
		// } /* end if */
		// else if (((Section) getParent()).getInsulatedGlass() != null)
		// {
		// for (InsulatedGlass glass : ((Section) getParent()).getInsulatedGlass())
		// {
		// glass.removeAutomaticGrilles();
		// } /* end for */
		// } /* end else */
	}

	private void createPartialRegularGrille(Map<String, Object> map)
	{
		map.put(PartialRegularGrillesQuestionary.PROPERTYNAME_NBHORIZBARS, 2);
		map.put(PartialRegularGrillesQuestionary.PROPERTYNAME_NBVERTIBARS, 3);
		map.put(PartialRegularGrillesQuestionary.PROPERTYNAME_WIDTH, 1 / 4d);
		map.put(PartialRegularGrillesQuestionary.PROPERTYNAME_PARTIALDISTANCE, getNoSplittingBars()[0].getLine().getY1());
		map.put(SeparableGrillesManager.PARAMETER_PARTIALBARTOVISIBLEGLASSOFFSET, getNoSplittingBars()[0].getThickness().doubleValue(Units.INCH) / 2d);
		createAutomaticGrilles(PartialRegularGrillesQuestionary.class, map, 0);
	}

	private void createPartialDiamondGrille(Map<String, Object> map)
	{
		map.put(PartialDiamondGrillesQuestionary.PROPERTYNAME_NBHORIZBARS, 1);
		map.put(PartialDiamondGrillesQuestionary.PROPERTYNAME_NBVERTIBARS, 3);
		map.put(PartialDiamondGrillesQuestionary.PROPERTYNAME_WIDTH, 1 / 4d);
		map.put(PartialDiamondGrillesQuestionary.PROPERTYNAME_PATTERNWIDTH, 3d);
		map.put(PartialDiamondGrillesQuestionary.PROPERTYNAME_PATTERNHEIGHT, 4d);
		map.put(PartialDiamondGrillesQuestionary.PROPERTYNAME_MINIMUMBARLENGTH, 0.5d);
		map.put(PartialDiamondGrillesQuestionary.PROPERTYNAME_PARTIALDISTANCE, getNoSplittingBars()[0].getLine().getY1());
		map.put(SeparableGrillesManager.PARAMETER_PARTIALBARTOVISIBLEGLASSOFFSET, getNoSplittingBars()[0].getThickness().doubleValue(Units.INCH) / 2d);
		createAutomaticGrilles(PartialDiamondGrillesQuestionary.class, map, 0);
	}

	private void createPartialPerimeterGrille(Map<String, Object> map)
	{
		map.put("type", "perimeter");
		map.put(PartialPerimeterGrillesQuestionary.PROPERTYNAME_NBVERTICALSPOKES, 3);
		map.put(PartialPerimeterGrillesQuestionary.PROPERTYNAME_NBHORIZONTALSPOKES, 2);
		map.put(PartialPerimeterGrillesQuestionary.PROPERTYNAME_NBDIAGONALSPOKES, 2);
		map.put(PartialPerimeterGrillesQuestionary.PROPERTYNAME_NBARCSPOKES, 2);
		map.put(PartialPerimeterGrillesQuestionary.PROPERTYNAME_WIDTH, 1 / 4d);
		map.put(PartialPerimeterGrillesQuestionary.PROPERTYNAME_LEFT, true);
		map.put(PartialPerimeterGrillesQuestionary.PROPERTYNAME_RIGHT, true);
		map.put(PartialPerimeterGrillesQuestionary.PROPERTYNAME_TOP, true);
		map.put(PartialPerimeterGrillesQuestionary.PROPERTYNAME_BOTTOM, true);
		map.put(PartialPerimeterGrillesQuestionary.PROPERTYNAME_DIAGONAL, true);
		map.put(PartialPerimeterGrillesQuestionary.PROPERTYNAME_ARC, true);
		map.put(PartialPerimeterGrillesQuestionary.PROPERTYNAME_HDISTANCEFROMVISIBLEGLASS, true);
		map.put(PartialPerimeterGrillesQuestionary.PROPERTYNAME_VDISTANCEFROMVISIBLEGLASS, true);
		map.put(PartialPerimeterGrillesQuestionary.PROPERTYNAME_DISTANCE, 1d);
		map.put(PartialPerimeterGrillesQuestionary.PROPERTYNAME_PARTIALDISTANCE, getNoSplittingBars()[0].getLine().getY1());
		map.put(SeparableGrillesManager.PARAMETER_PARTIALBARTOVISIBLEGLASSOFFSET, getNoSplittingBars()[0].getThickness().doubleValue(Units.INCH) / 2d);
		createAutomaticGrilles(PartialPerimeterGrillesQuestionary.class, map, 0);
	}
	
	@Override
	protected Class<? extends InternalMuntin> getMuntinClass()
	{
		return InternalMuntin.class;
	}
}
