package com.client360.configuration.wad.doormodel.accessoire.tandem;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class JetDeauTandem extends ParametricModel
{

	@Override
	protected void load()
	{
		addForm(rectangle(left, right, top, sill));
		addSpacer(1, 1, 1, 1);
		addEmptyProfile();
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

}
