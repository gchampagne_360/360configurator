package com.client360.configuration.wad.doormodel.accessoire.tandem;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.profiles.ProfileMoulure3DA;
import com.client360.configuration.wad.profiles.ProfileMoulure3DB;
import com.client360.configuration.wad.profiles.ProfilePorteBoisA;
import com.client360.configuration.wad.profiles.ProfilePorteBoisB;
import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class Pan1Rectangle extends ParametricModel
{
	private double spacerExt = 56.5d;
	private double spacerEmbosse = 35.5d;

	protected String embosse1Gauche = "embosse1Gauche";
	protected String embosse1Droit = "embosse1Droit";
	protected String embosse1Haut = "embosse1Haut";
	protected String embosse1Bas = "embosse1Bas";

	@Override
	protected void load()
	{
		// add frame
		addForm(rectangle(left, right, top, sill));
		addSpacer(spacerExt, spacerExt, spacerExt, spacerExt);
		addProfile(new ProfileMoulure3DA(), new ProfileMoulure3DB());

		// add embosse
		addLineRightOffset(embosse1Gauche, left, spacerExt);
		addLineDownOffset(embosse1Haut, top, spacerExt);
		addLineUpOffset(embosse1Bas, sill, spacerExt);
		addLineLeftOffset(embosse1Droit, right, spacerExt);

		addForm(rectangle(embosse1Gauche, embosse1Droit, embosse1Haut, embosse1Bas));
		addSpacer(spacerEmbosse, spacerEmbosse, spacerEmbosse, spacerEmbosse);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

}
