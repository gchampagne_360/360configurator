package com.client360.configuration.wad.doormodel.accessoire.tandem;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.profiles.ProfilePorteBoisA;
import com.client360.configuration.wad.profiles.ProfilePorteBoisB;
import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class OculusTriangleHautGauche extends ParametricModel
{
	@Override
	protected void load()
	{
		// add thermos
		addForm(triangleRectangle(left, right, top, sill), true, true, false, false);
		addInsulatedGlass(true);
		addEmptyProfile();
		addEmptySpacer();

		// add frame
		addForm(triangleRectangle(left, right, top, sill), true, true, false, false);
		addInsulatedGlass(false);
		addSpacer(45d, 45d, 45d);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}
}
