package com.client360.configuration.wad.doormodel.accessoire.tandem;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.profiles.ProfileMoulure3DA;
import com.client360.configuration.wad.profiles.ProfileMoulure3DB;
import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class Pan2VitrageRectangle extends ParametricModel
{
	private double spacerCreux = 56d;
	private double espaceEntreAcc = 61d;

	protected String panneau1Droit = "panneau1Droit";
	protected String panneau2Gauche = "panneau2Gauche";

	protected String vitrage1Gauche = "vitrage1Gauche";
	protected String vitrage1Droit = "vitrage1Droit";
	protected String vitrage1Haut = "vitrage1Haut";
	protected String vitrage1Bas = "vitrage1Bas";

	protected String vitrage2Gauche = "vitrage2Gauche";
	protected String vitrage2Droit = "vitrage2Droit";
	protected String vitrage2Haut = "vitrage2Haut";
	protected String vitrage2Bas = "vitrage2Bas";

	@Override
	protected void load()
	{
		double largeurPanneau = (vDist(left, right) - espaceEntreAcc) / 2;

		addLineRightOffset(panneau1Droit, left, largeurPanneau);
		addLineLeftOffset(panneau2Gauche, right, largeurPanneau);

		// vitrage gauche
		addForm(rectangle(left, panneau1Droit, top, sill));
		addInsulatedGlass(true);
		addEmptyProfile();
		addEmptySpacer();

		// vitrage droit
		addForm(rectangle(panneau2Gauche, right, top, sill));
		addInsulatedGlass(true);
		addEmptyProfile();
		addEmptySpacer();

		// frame gauche
		addForm(rectangle(left, panneau1Droit, top, sill));
		addSpacer(spacerCreux, spacerCreux, spacerCreux, spacerCreux);
		addProfile(new ProfileMoulure3DA(), new ProfileMoulure3DB());
		addInsulatedGlass(false);

		// frame droit
		addForm(rectangle(panneau2Gauche, right, top, sill));
		addSpacer(spacerCreux, spacerCreux, spacerCreux, spacerCreux);
		addProfile(new ProfileMoulure3DA(), new ProfileMoulure3DB());
		addInsulatedGlass(false);

		addLineRightOffset(vitrage1Gauche, left, spacerCreux);
		addLineDownOffset(vitrage1Haut, top, spacerCreux);
		addLineUpOffset(vitrage1Bas, sill, spacerCreux);
		addLineRightOffset(vitrage1Droit, vitrage1Gauche, largeurPanneau - (2 * spacerCreux));

		addLineLeftOffset(vitrage2Droit, right, spacerCreux);
		addLineDownOffset(vitrage2Haut, top, spacerCreux);
		addLineUpOffset(vitrage2Bas, sill, spacerCreux);
		addLineLeftOffset(vitrage2Gauche, vitrage2Droit, largeurPanneau - (2 * spacerCreux));
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

}
