package com.client360.configuration.wad.doormodel.accessoire.tandem;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.profiles.ProfileMoulure3DA;
import com.client360.configuration.wad.profiles.ProfileMoulure3DB;
import com.netappsid.commonutils.math.Units;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class PanVitrageDemiLune extends ParametricModel
{
	private double spacerCreux = 56d;

	protected String vitrageGauche = "vitrageGauche";
	protected String vitrageDroit = "vitrageDroit";
	protected String vitrageHaut = "vitrageHaut";
	protected String vitrageBas = "vitrageBas";

	@Override
	protected void load()
	{
		// add thermos
		String imgPath = (String) getParametricObject("imageGrille");
		addForm(halfCircle(left, right, top), imgPath, ImageScale.FULL);
		addInsulatedGlass(true);
		addEmptyProfile();
		addEmptySpacer();

		// add frame
		addForm(halfCircle(left, right, top));
		addSpacer(spacerCreux, spacerCreux, spacerCreux, spacerCreux);
		addProfile(new ProfileMoulure3DA(), new ProfileMoulure3DB());
		addInsulatedGlass(false);

		addLineRightOffset(vitrageGauche, left, spacerCreux);
		addLineDownOffset(vitrageHaut, top, spacerCreux);
		addLineUpOffset(vitrageBas, sill, spacerCreux);
		addLineLeftOffset(vitrageDroit, right, spacerCreux);

		// addForm(arc(vitrageGauche, vitrageDroit, vitrageHaut, vitrageBas));
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

}
