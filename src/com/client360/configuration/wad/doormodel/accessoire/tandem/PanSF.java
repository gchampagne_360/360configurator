package com.client360.configuration.wad.doormodel.accessoire.tandem;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.profiles.ProfilePorteBoisA;
import com.client360.configuration.wad.profiles.ProfilePorteBoisB;
import com.netappsid.commonutils.math.Units;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class PanSF extends ParametricModel
{
	protected double largeurCadre = 21.75;

	public final String accHaut = "accHaut";
	public final String accBas = "accBas";
	public final String accGauche = "accGauche";
	public final String accDroit = "accDroit";

	@Override
	protected void load()
	{
		addLineDownOffset(accHaut, top, largeurCadre);
		addLineUpOffset(accBas, sill, largeurCadre);
		addLineRightOffset(accGauche, left, largeurCadre);
		addLineLeftOffset(accDroit, right, largeurCadre);

		// add filling
		String imgPath = (String) getParametricObject("imageGrille");
		addForm(rectangle(accGauche, accDroit, accHaut, accBas), imgPath, ImageAlign.CENTER);
		addEmptyProfile();
		addEmptySpacer();

		// add frame
		addForm(rectangle(left, right, top, sill));
		addSpacer(largeurCadre, largeurCadre, largeurCadre, largeurCadre);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}
}
