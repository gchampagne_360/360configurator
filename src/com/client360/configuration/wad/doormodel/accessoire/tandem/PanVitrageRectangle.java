package com.client360.configuration.wad.doormodel.accessoire.tandem;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.profiles.ProfileMoulure3DA;
import com.client360.configuration.wad.profiles.ProfileMoulure3DB;
import com.netappsid.commonutils.math.Units;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class PanVitrageRectangle extends ParametricModel
{
	private double spacerCreux = 56d;
	private double espaceVitrage = 3;

	protected String vitrageGauche = "vitrageGauche";
	protected String vitrageDroit = "vitrageDroit";
	protected String vitrageHaut = "vitrageHaut";
	protected String vitrageBas = "vitrageBas";

	@Override
	protected void load()
	{
		addLineRightOffset(vitrageGauche, left, spacerCreux - espaceVitrage);
		addLineDownOffset(vitrageHaut, top, spacerCreux - espaceVitrage);
		addLineUpOffset(vitrageBas, sill, spacerCreux - espaceVitrage);
		addLineLeftOffset(vitrageDroit, right, spacerCreux - espaceVitrage);

		// add thermos
		String imgPath = (String) getParametricObject("imageGrille");
		addForm(rectangle(vitrageGauche, vitrageDroit, vitrageHaut, vitrageBas), imgPath, ImageScale.FULL);
		addInsulatedGlass(true);
		addEmptyProfile();
		addEmptySpacer();

		// add frame
		addForm(rectangle(left, right, top, sill));
		addSpacer(spacerCreux, spacerCreux, spacerCreux, spacerCreux);
		addProfile(new ProfileMoulure3DA(), new ProfileMoulure3DB());
		addInsulatedGlass(false);
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

}
