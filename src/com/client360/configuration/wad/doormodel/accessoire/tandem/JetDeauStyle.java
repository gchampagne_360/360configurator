package com.client360.configuration.wad.doormodel.accessoire.tandem;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.cad.CadJetdeau;
import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class JetDeauStyle extends ParametricModel
{
	@Override
	protected void load()
	{
		addDrawing(new CadJetdeau(), left, right, top, sill, Orientation.HORIZONTAL);
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}
}