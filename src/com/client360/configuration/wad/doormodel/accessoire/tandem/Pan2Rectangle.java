package com.client360.configuration.wad.doormodel.accessoire.tandem;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.profiles.ProfileMoulure3DA;
import com.client360.configuration.wad.profiles.ProfileMoulure3DB;
import com.client360.configuration.wad.profiles.ProfilePorteBoisA;
import com.client360.configuration.wad.profiles.ProfilePorteBoisB;
import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class Pan2Rectangle extends ParametricModel
{
	private double spacerCreux = 56d;
	private double spacerEmbosse = 43.2d;
	private double espaceEntreAcc = 61d;

	protected String panneau1Droit = "panneau1Droit";
	protected String panneau2Gauche = "panneau2Gauche";

	protected String embosse1Gauche = "embosse1Gauche";
	protected String embosse1Droit = "embosse1Droit";
	protected String embosse1Haut = "embosse1Haut";
	protected String embosse1Bas = "embosse1Bas";

	protected String embosse2Gauche = "embosse2Gauche";
	protected String embosse2Droit = "embosse2Droit";
	protected String embosse2Haut = "embosse2Haut";
	protected String embosse2Bas = "embosse2Bas";

	@Override
	protected void load()
	{
		double largeurPanneau = (vDist(left, right) - espaceEntreAcc) / 2;

		addLineRightOffset(panneau1Droit, left, largeurPanneau);
		addLineLeftOffset(panneau2Gauche, right, largeurPanneau);

		addForm(rectangle(left, panneau1Droit, top, sill));
		addSpacer(spacerCreux, spacerCreux, spacerCreux, spacerCreux);
		addProfile(new ProfileMoulure3DA(), new ProfileMoulure3DB());

		addForm(rectangle(panneau2Gauche, right, top, sill));
		addSpacer(spacerCreux, spacerCreux, spacerCreux, spacerCreux);
		addProfile(new ProfileMoulure3DA(), new ProfileMoulure3DB());

		addLineRightOffset(embosse1Gauche, left, spacerCreux);
		addLineDownOffset(embosse1Haut, top, spacerCreux);
		addLineUpOffset(embosse1Bas, sill, spacerCreux);
		addLineRightOffset(embosse1Droit, embosse1Gauche, largeurPanneau - (2 * spacerCreux));

		addLineLeftOffset(embosse2Droit, right, spacerCreux);
		addLineDownOffset(embosse2Haut, top, spacerCreux);
		addLineUpOffset(embosse2Bas, sill, spacerCreux);
		addLineLeftOffset(embosse2Gauche, embosse2Droit, largeurPanneau - (2 * spacerCreux));

		addForm(rectangle(embosse1Gauche, embosse1Droit, embosse1Haut, embosse1Bas));
		addSpacer(spacerEmbosse, spacerEmbosse, spacerEmbosse, spacerEmbosse);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());

		addForm(rectangle(embosse2Gauche, embosse2Droit, embosse2Haut, embosse2Bas));
		addSpacer(spacerEmbosse, spacerEmbosse, spacerEmbosse, spacerEmbosse);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

}
