package com.client360.configuration.wad.doormodel.accessoire.steel;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.profiles.ProfilePorteBoisA;
import com.client360.configuration.wad.profiles.ProfilePorteBoisB;
import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class Pan2Rectangle extends ParametricModel
{
	protected final double espaceEntrePanneau = 120;
	protected final double spacerPanneau = 50;

	public final String pan1Droite = "pan1Droite";
	public final String pan2Gauche = "pan2Gauche";

	@Override
	protected void load()
	{
		double largeurPanneau = (vDist(left, right) - espaceEntrePanneau) / 2;
		addLineRightOffset(pan1Droite, left, largeurPanneau);
		addLineLeftOffset(pan2Gauche, right, largeurPanneau);

		addForm(rectangle(left, pan1Droite, top, sill));
		addSpacer(spacerPanneau, spacerPanneau, spacerPanneau, spacerPanneau);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());

		addForm(rectangle(pan2Gauche, right, top, sill));
		addSpacer(spacerPanneau, spacerPanneau, spacerPanneau, spacerPanneau);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

}
