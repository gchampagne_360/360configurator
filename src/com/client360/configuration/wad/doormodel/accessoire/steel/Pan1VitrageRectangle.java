package com.client360.configuration.wad.doormodel.accessoire.steel;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.doormodel.accessoire.PetitBois;
import com.client360.configuration.wad.profiles.ProfilePorteBoisA;
import com.client360.configuration.wad.profiles.ProfilePorteBoisB;
import com.netappsid.commonutils.math.Units;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class Pan1VitrageRectangle extends ParametricModel
{
	protected final double spacerPanneau = 39.5;

	protected String vGau = "vGau";
	protected String vDro = "vDro";
	protected String vHau = "vHau";
	protected String vBas = "vBas";

	protected String pbGau = "pbGau";
	protected String pbDro = "pbDro";
	protected String pbHau = "pbHau";
	protected String pbBas = "pbBas";

	@Override
	protected void load()
	{
		addLineRightOffset(vGau, left, spacerPanneau - 7);
		addLineDownOffset(vHau, top, spacerPanneau - 7);
		addLineLeftOffset(vDro, right, spacerPanneau - 7);
		addLineUpOffset(vBas, sill, spacerPanneau - 7);

		String imgPath = (String) getParametricObject("imageGrille");
		addForm(rectangle(vGau, vDro, vHau, vBas), imgPath, ImageScale.FULL);
		addEmptySpacer();
		addEmptyProfile();
		addInsulatedGlass(true);

		addForm(rectangle(left, right, top, sill));
		addSpacer(spacerPanneau, spacerPanneau, spacerPanneau, spacerPanneau);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
		addInsulatedGlass(false);

		addLineRightOffset(pbGau, left, spacerPanneau);
		addLineDownOffset(pbHau, top, spacerPanneau);
		addLineLeftOffset(pbDro, right, spacerPanneau);
		addLineUpOffset(pbBas, sill, spacerPanneau);

		addElementSpace("pb", pbGau, pbDro, pbHau, pbBas);
		addSubModel(new PetitBois(), "pb");
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

}
