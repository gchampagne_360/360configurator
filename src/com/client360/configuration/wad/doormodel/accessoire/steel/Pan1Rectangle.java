package com.client360.configuration.wad.doormodel.accessoire.steel;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.profiles.ProfilePorteBoisA;
import com.client360.configuration.wad.profiles.ProfilePorteBoisB;
import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class Pan1Rectangle extends ParametricModel
{
	protected final double spacerPanneau = 50;

	@Override
	protected void load()
	{
		addForm(rectangle(left, right, top, sill));
		addSpacer(spacerPanneau, spacerPanneau, spacerPanneau, spacerPanneau);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

}
