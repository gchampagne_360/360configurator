package com.client360.configuration.wad.doormodel.accessoire.steel;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.profiles.ProfilePorteBoisA;
import com.client360.configuration.wad.profiles.ProfilePorteBoisB;
import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class Pan8Rectangle extends ParametricModel
{
	protected double espaceHorizontalEntrePanneau = 105;
	protected double espaceVerticalEntrePanneau = 113;
	protected double spacer = 50;

	public final String posPan1Droit = "posPan1Droit";
	public final String posPan2Gauche = "posPan2Gauche";
	public final String posPanHaut = "posPanHaut";
	public final String posPanBas = "posPanBas";

	@Override
	protected void load()
	{
		double largeurPanneau = (vDist(left, right) - espaceHorizontalEntrePanneau) / 2;
		double hauteurPanneau = (hDist(top, sill) - (3 * espaceVerticalEntrePanneau)) / 4;

		addLineRightOffset(posPan1Droit, left, largeurPanneau);
		addLineLeftOffset(posPan2Gauche, right, largeurPanneau);

		double offset = 0;
		for (int i = 1; i <= 4; i++)
		{
			offset += hauteurPanneau;
			addLineDownOffset(posPanBas + i, top, offset);

			offset += espaceVerticalEntrePanneau;
			addLineDownOffset(posPanHaut + (i + 1), top, offset);

			if (i == 1)
			{
				addForm(rectangle(left, posPan1Droit, top, posPanBas + i));
				addSpacer(spacer, spacer, spacer, spacer);
				addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());

				addForm(rectangle(posPan2Gauche, right, top, posPanBas + i));
				addSpacer(spacer, spacer, spacer, spacer);
				addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
			}
			else if (i == 4)
			{
				addForm(rectangle(left, posPan1Droit, posPanHaut + i, sill));
				addSpacer(spacer, spacer, spacer, spacer);
				addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());

				addForm(rectangle(posPan2Gauche, right, posPanHaut + i, sill));
				addSpacer(spacer, spacer, spacer, spacer);
				addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
			}
			else
			{
				addForm(rectangle(left, posPan1Droit, posPanHaut + i, posPanBas + i));
				addSpacer(spacer, spacer, spacer, spacer);
				addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());

				addForm(rectangle(posPan2Gauche, right, posPanHaut + i, posPanBas + i));
				addSpacer(spacer, spacer, spacer, spacer);
				addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
			}
		}

	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}
}
