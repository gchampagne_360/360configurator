package com.client360.configuration.wad.doormodel.accessoire.steel;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class OculusRond extends ParametricModel
{
	protected double espaceVitrage = 10;

	public final String vHaut = "vHaut";
	public final String vBas = "vBas";
	public final String vGauche = "vGauche";
	public final String vDroit = "vDroit";

	@Override
	protected void load()
	{
		addLineDownOffset(vHaut, top, espaceVitrage);
		addLineUpOffset(vBas, sill, espaceVitrage);
		addLineLeftOffset(vDroit, right, espaceVitrage);
		addLineRightOffset(vGauche, left, espaceVitrage);

		// add thermos
		addForm(circle(vGauche, vDroit, vHaut));
		addInsulatedGlass(true);
		addEmptyProfile();
		addEmptySpacer();

		// add frame
		addForm(circle(left, right, top));
		addInsulatedGlass(false);
		addSpacer(40d, 40d);
		addEmptyProfile();
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

}
