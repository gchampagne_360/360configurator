package com.client360.configuration.wad.doormodel.accessoire.steel;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class OculusRectangle extends ParametricModel
{
	protected double espaceVitrage = 25;

	public final String vHaut = "vHaut";
	public final String vBas = "vBas";
	public final String vGauche = "vGauche";
	public final String vDroit = "vDroit";

	@Override
	protected void load()
	{
		addLineDownOffset(vHaut, top, espaceVitrage);
		addLineUpOffset(vBas, sill, espaceVitrage);
		addLineLeftOffset(vDroit, right, espaceVitrage);
		addLineRightOffset(vGauche, left, espaceVitrage);

		// add thermos
		addForm(rectangle(vGauche, vDroit, vHaut, vBas));
		addInsulatedGlass(true);
		addEmptySpacer();
		addEmptyProfile();

		// add frame
		addForm(rectangle(left, right, top, sill));
		addSpacer(51d, 51d, 51d, 51d);
		addEmptyProfile();
		addInsulatedGlass(false);
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

}
