package com.client360.configuration.wad.doormodel.accessoire.steel;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.profiles.ProfilePorteBoisA;
import com.client360.configuration.wad.profiles.ProfilePorteBoisB;
import com.netappsid.commonutils.math.Units;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class Pan2VitrageRectangle extends ParametricModel
{
	protected final double espaceEntrePanneau = 91;
	protected final double spacerPanneau = 39.5;

	public final String pan1Droite = "pan1Droite";
	public final String pan2Gauche = "pan2Gauche";

	protected String v1Gau = "v1Gau";
	protected String v1Dro = "v1Dro";
	protected String v1Hau = "v1Hau";
	protected String v1Bas = "v1Bas";

	protected String v2Gau = "v2Gau";
	protected String v2Dro = "v2Dro";
	protected String v2Hau = "v2Hau";
	protected String v2Bas = "v2Bas";

	@Override
	protected void load()
	{
		String imgPath = (String) getParametricObject("imageGrille");
		double largeurPanneau = (vDist(left, right) - espaceEntrePanneau) / 2;
		addLineRightOffset(pan1Droite, left, largeurPanneau);
		addLineLeftOffset(pan2Gauche, right, largeurPanneau);

		addLineRightOffset(v1Gau, left, spacerPanneau - 7);
		addLineDownOffset(v1Hau, top, spacerPanneau - 7);
		addLineLeftOffset(v1Dro, pan1Droite, spacerPanneau - 7);
		addLineUpOffset(v1Bas, sill, spacerPanneau - 7);

		addForm(rectangle(v1Gau, v1Dro, v1Hau, v1Bas), imgPath, ImageScale.FULL);
		addEmptySpacer();
		addEmptyProfile();
		addInsulatedGlass(true);

		addLineRightOffset(v2Gau, pan2Gauche, spacerPanneau - 7);
		addLineDownOffset(v2Hau, top, spacerPanneau - 7);
		addLineLeftOffset(v2Dro, right, spacerPanneau - 7);
		addLineUpOffset(v2Bas, sill, spacerPanneau - 7);

		addForm(rectangle(v2Gau, v2Dro, v2Hau, v2Bas), imgPath, ImageScale.FULL);
		addEmptySpacer();
		addEmptyProfile();
		addInsulatedGlass(true);

		addForm(rectangle(left, pan1Droite, top, sill));
		addSpacer(spacerPanneau, spacerPanneau, spacerPanneau, spacerPanneau);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
		addInsulatedGlass(false);

		addForm(rectangle(pan2Gauche, right, top, sill));
		addSpacer(spacerPanneau, spacerPanneau, spacerPanneau, spacerPanneau);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
		addInsulatedGlass(false);
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

}
