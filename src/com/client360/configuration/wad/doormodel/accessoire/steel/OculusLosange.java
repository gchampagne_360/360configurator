package com.client360.configuration.wad.doormodel.accessoire.steel;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class OculusLosange extends ParametricModel
{
	protected double espaceVitrage = 17.5;

	public final String vHaut = "vHaut";
	public final String vBas = "vBas";
	public final String vGauche = "vGauche";
	public final String vDroit = "vDroit";

	@Override
	protected void load()
	{
		addLineDownOffset(vHaut, top, espaceVitrage);
		addLineUpOffset(vBas, sill, espaceVitrage);
		addLineLeftOffset(vDroit, right, espaceVitrage);
		addLineRightOffset(vGauche, left, espaceVitrage);

		// add thermos
		addForm(rhombus(vGauche, vDroit, vHaut, vBas));
		addInsulatedGlass(true);
		addEmptySpacer();
		addEmptyProfile();

		// add frame
		addForm(rhombus(left, right, top, sill));
		addSpacer(40d, 40d, 40d, 40d);
		addEmptyProfile();
		addInsulatedGlass(false);
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

}
