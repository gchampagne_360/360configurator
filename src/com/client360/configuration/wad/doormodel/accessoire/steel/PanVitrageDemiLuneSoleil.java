package com.client360.configuration.wad.doormodel.accessoire.steel;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.profiles.ProfileMoulurePanneauA;
import com.netappsid.commonutils.math.Units;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class PanVitrageDemiLuneSoleil extends ParametricModel
{
	private double spacer = 39.5;

	protected String vitrageGauche = "vitrageGauche";
	protected String vitrageDroit = "vitrageDroit";
	protected String vitrageHaut = "vitrageHaut";
	protected String vitrageBas = "vitrageBas";

	@Override
	protected void load()
	{
		addLineRightOffset(vitrageGauche, left, spacer - 7);
		addLineDownOffset(vitrageHaut, top, spacer - 7);
		addLineLeftOffset(vitrageDroit, right, spacer - 7);
		addLineUpOffset(vitrageBas, sill, spacer - 7);

		// add thermos
		addForm(halfCircle(vitrageGauche, vitrageDroit, vitrageHaut), "/images/grille_svg/Soleil2Rayon.svg", ImageScale.FULL);
		addInsulatedGlass(true);
		addEmptyProfile();
		addEmptySpacer();

		// add frame
		addForm(halfCircle(left, right, top));
		addSpacer(spacer, spacer, spacer, spacer);
		addProfile(new ProfileMoulurePanneauA(), new ProfileMoulurePanneauA());
		addInsulatedGlass(false);
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

}
