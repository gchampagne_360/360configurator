package com.client360.configuration.wad.doormodel.accessoire;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class AccessoireGenerique extends ParametricModel
{
	@Override
	protected void load()
	{
		// TODO Auto-generated method stub

	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

}
