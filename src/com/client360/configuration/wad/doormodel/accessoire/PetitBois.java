package com.client360.configuration.wad.doormodel.accessoire;

import java.util.List;

import com.client360.configuration.wad.profiles.ProfilePorteBoisA;
import com.client360.configuration.wad.profiles.ProfilePorteBoisB;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;
import com.netappsid.wadconfigurator.entranceDoor.ElementSpace;

public class PetitBois extends AccessoireGenerique
{
	public String petitBoisHUp = "petitBoisHUp";
	public String petitBoisHDown = "petitBoisHDown";

	public String petitBoisHLeft = "petitBoisHLeft";
	public String petitBoisHRight = "petitBoisHRight";

	public String petitBoisVUp = "petitBoisVUp";
	public String petitBoisVDown = "petitBoisVDown";

	public String petitBoisVLeft = "petitBoisVLeft";
	public String petitBoisVRight = "petitBoisVRight";

	@Override
	protected void load()
	{
		try
		{
			int pbh = getPbh();
			int pbv = getPbv();
			int larPbe = getLarPbe();
			int larSpacer = getLargeurSpacerPetitBois();

			double hauteurCarreau = (hDist(top, sill) - (pbh * larPbe)) / (pbh + 1);
			double largeurCarreau = (vDist(left, right) - (pbv * larPbe)) / (pbv + 1);

			// add horizontal lines
			addLineDownOffset(petitBoisVDown + 1, sill, larSpacer);
			double hTotal = 0;
			for (int i = 1; i <= pbh; i++)
			{
				hTotal += hauteurCarreau;
				addLineUpOffset(petitBoisHDown + i, sill, hTotal);

				hTotal += larSpacer;
				addLineUpOffset(petitBoisVUp + i, sill, hTotal);

				hTotal += larPbe - (2 * larSpacer);
				addLineUpOffset(petitBoisVDown + (i + 1), sill, hTotal);

				hTotal += larSpacer;
				addLineUpOffset(petitBoisHUp + i, sill, hTotal);
			}
			addLineUpOffset(petitBoisVUp + (pbh + 1), top, larSpacer);

			// add vertical lines
			addLineLeftOffset(petitBoisHLeft, left, larSpacer);
			addLineRightOffset(petitBoisHRight, right, larSpacer);

			int lTotal = 0;
			for (int j = 1; j <= pbv; j++)
			{
				lTotal += largeurCarreau;
				addLineRightOffset(petitBoisVLeft + j, left, lTotal);

				lTotal += larPbe;
				addLineRightOffset(petitBoisVRight + j, left, lTotal);
			}

			// add petit bois horizontal
			for (int i = 1; i <= pbh; i++)
			{
				addForm(rectangle(petitBoisHLeft, petitBoisHRight, petitBoisHUp + i, petitBoisHDown + i));
				addInsulatedGlass(false);
				addSpacer(larSpacer, -larSpacer, larSpacer, -larSpacer);
				addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
				addPart("petitBois", "petitBoisH" + i, petitBoisHLeft, petitBoisHRight, petitBoisHUp + i, petitBoisHDown + i);
			}

			// add petit bois vertical
			for (int i = 1; i <= (pbh + 1); i++)
			{
				for (int j = 1; j <= pbv; j++)
				{
					addForm(rectangle(petitBoisVLeft + j, petitBoisVRight + j, petitBoisVUp + i, petitBoisVDown + i));
					addInsulatedGlass(false);
					addSpacer(-larSpacer, larSpacer, -larSpacer, larSpacer);
					addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
					addPart("petitBois", "petitBoisV" + i, petitBoisVLeft + j, petitBoisVRight + j, petitBoisVUp + i, petitBoisVDown + i);
				}
			}

			// add parts
			final String group = "PanneauPetitBois";
			if (pbh != 0 || pbv != 0)
			{
				for (int i = 1; i <= (pbh + 1); i++)
				{
					for (int j = 1; j <= (pbv + 1); j++)
					{
						Object cleft = null;
						Object cright = null;
						Object ctop = null;
						Object cbottom = null;

						if (j == 1)
						{
							cleft = left;
						}
						else
						{
							cleft = petitBoisVRight + (j - 1);
						}
						if (j == pbv + 1)
						{
							cright = right;
						}
						else
						{
							cright = petitBoisVLeft + j;
						}

						if (i == 1)
						{
							cbottom = sill;
							if (pbh == 0)
							{
								ctop = top;
							}
							else
							{
								ctop = petitBoisHDown + i;
							}
						}
						else if (i == pbh + 1)
						{
							ctop = top;
							cbottom = petitBoisHUp + (i - 1);
						}
						else
						{
							ctop = petitBoisHDown + i;
							cbottom = petitBoisHUp + (i - 1);
						}
						addPart(group, "panneau" + i + j, cleft, cright, ctop, cbottom);
					}
				}
			}

			// add grilles between if any
			String imgPath = (String) getParametricObject("imageGrille");
			if (imgPath != null)
			{
				List<ElementSpace> carreaux = getPartsByGroup(group);
				for (ElementSpace space : carreaux)
				{
					addForm(rectangle(space.getLeft(), space.getRight(), space.getTop(), space.getBottom()), imgPath, ImageScale.FULL);
					addEmptySpacer();
					addEmptyProfile();
					addInsulatedGlass(false);
				}
			}
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
	}

	public int getLargeurSpacerPetitBois()
	{
		try
		{
			return Integer.parseInt((String) getParametricObject("larSpacer"));
		}
		catch (NumberFormatException e)
		{
			return 0;
		}
	}

	public int getPbh()
	{
		try
		{
			return Integer.parseInt((String) getParametricObject("pbh"));
		}
		catch (NumberFormatException e)
		{
			return 0;
		}
	}

	public int getPbv()
	{
		try
		{
			return Integer.parseInt((String) getParametricObject("pbv"));
		}
		catch (NumberFormatException e)
		{
			return 0;
		}
	}

	public int getLarPbe()
	{
		try
		{
			return Integer.parseInt((String) getParametricObject("larPbe"));
		}
		catch (NumberFormatException e)
		{
			return 0;
		}
	}
}
