package com.client360.configuration.wad.doormodel.accessoire;

import com.client360.configuration.wad.profiles.ProfilePorteBoisA;
import com.client360.configuration.wad.profiles.ProfilePorteBoisB;

public class OculusTriangleBasDroit extends AccessoireGenerique
{
	@Override
	protected void load()
	{
		// add thermos
		addForm(triangleRectangle(left, right, top, sill), false, false, false, false);
		setInsulatedGlass(true);
		addEmptyProfile();
		addEmptySpacer();

		// add frame
		addForm(triangleRectangle(left, right, top, sill), false, false, false, false);
		setInsulatedGlass(false);
		addSpacer(45d, 45d, 45d);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
	}
}
