package com.client360.configuration.wad.doormodel.accessoire;

public class OculusRond extends AccessoireGenerique
{

	@Override
	protected void load()
	{
		// add thermos
		addForm(circle(left, right, top));
		setInsulatedGlass(true);
		addEmptyProfile();
		addEmptySpacer();

		// add frame
		addForm(circle(left, right, top));
		setInsulatedGlass(false);
		addSpacer(45d, 45d, 45d, 45d);
		// there is a fraction bug here if we set a profile, dont know why
		// addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
		addEmptyProfile();

	}

}
