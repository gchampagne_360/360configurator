package com.client360.configuration.wad.doormodel.accessoire.wood;

import com.client360.configuration.wad.doormodel.wood.GestionLargeurStyle;
import com.client360.configuration.wad.profiles.ProfileMoulure3DA;
import com.client360.configuration.wad.profiles.ProfileMoulure3DB;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;

public class CfcgDyane extends GestionLargeurStyle
{
	@Override
	protected void load()
	{
		// add square around ellipse
		addForm(rectangle(left, right, top, sill));
		addSpacer(1, 1, 1, 1);
		addEmptyProfile();
		setInsulatedGlass(false);

		addLineDownOffset("sill3", sill, 25);
		addLineUpOffset("top2", top, 25);
		addLineLeftOffset("left2", left, 25);
		addLineRightOffset("right2", right, 25);

		addLineUpOffset("thermosSill", "sill3", larMntCfpSt);
		addLineDownOffset("thermosTop", "top2", larMntCfpSt);
		addLineRightOffset("thermosLeft", "left2", larMntCfpSt);
		addLineLeftOffset("thermosRight", "right2", larMntCfpSt);

		// add thermos
		String imgPath = (String) getParametricObject("imageGrille");
		addForm(ellipse("thermosLeft", "thermosRight", "thermosTop", "thermosSill"), imgPath, ImageScale.FULL);
		addEmptySpacer();
		addEmptyProfile();
		setInsulatedGlass(true);

		// add frame
		addForm(ellipse("left2", "right2", "top2", "sill3"));
		setInsulatedGlass(false);
		addSpacer(larMntCfpSt, larMntCfpSt, larMntCfpSt, larMntCfpSt, larMntCfpSt, larMntCfpSt);
		addProfile(new ProfileMoulure3DA(), new ProfileMoulure3DB());
	}
}