package com.client360.configuration.wad.doormodel.accessoire.wood;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.doormodel.accessoire.PetitBois;
import com.client360.configuration.wad.profiles.ProfileMoulure3DA;
import com.client360.configuration.wad.profiles.ProfileMoulure3DB;
import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class CfpSt1PanRecHiPb extends ParametricModel
{
	@Override
	protected void load()
	{
		addForm(rectangle(left, right, top, sill));
		addSpacer(56, 56, 56, 56);
		addProfile(new ProfileMoulure3DA(), new ProfileMoulure3DB());

		// add petit bois
		addLineDownOffset("pbTop", top, 56);
		addLineUpOffset("pbBottom", sill, 56);
		addLineRightOffset("pbLeft", left, 56);
		addLineLeftOffset("pbRight", right, 56);

		addElementSpace("pb", "pbLeft", "pbRight", "pbTop", "pbBottom");
		addSubModel(new PetitBois(), "pb");
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

}
