package com.client360.configuration.wad.doormodel.accessoire.wood;

import com.client360.configuration.wad.profiles.ProfileClassiqueA;
import com.client360.configuration.wad.profiles.ProfileClassiqueB;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;

public class PanneauMoucVrtViper extends PanneauMoucVrt
{
	protected double hauteurOculus = 542;
	protected double largeurOculus = 262;
	protected double spacerOculus = 45;
	protected double largeurParclose = 7;

	public final String ocHaut = "ocHaut";
	public final String ocBas = "ocBas";
	public final String ocGauche = "ocGauche";
	public final String ocDroit = "ocDroit";

	public final String vHaut = "vHaut";
	public final String vBas = "vBas";
	public final String vGauche = "vGauche";
	public final String vDroit = "vDroit";

	@Override
	protected void load()
	{
		super.load();

		addLineLeftOffset(ocGauche, vCenter, largeurOculus / 2);
		addLineRightOffset(ocDroit, vCenter, largeurOculus / 2);
		addLineDownOffset(ocHaut, top, 256);
		addLineDownOffset(ocBas, ocHaut, hauteurOculus);

		addForm(rectangle(ocGauche, ocDroit, ocHaut, ocBas));
		addSpacer(spacerOculus, spacerOculus, spacerOculus, spacerOculus);
		addProfile(new ProfileClassiqueA(), new ProfileClassiqueB());
		setInsulatedGlass(false);

		addLineDownOffset(vHaut, ocHaut, spacerOculus - largeurParclose);
		addLineUpOffset(vBas, ocBas, spacerOculus - largeurParclose);
		addLineLeftOffset(vDroit, ocDroit, spacerOculus - largeurParclose);
		addLineRightOffset(vGauche, ocGauche, spacerOculus - largeurParclose);

		addForm(rectangle(vGauche, vDroit, vHaut, vBas), "/images/grille_svg/Petit_ble.svg", ImageScale.FULL);
		addEmptySpacer();
		addEmptyProfile();
		setInsulatedGlass(true);
	}

}
