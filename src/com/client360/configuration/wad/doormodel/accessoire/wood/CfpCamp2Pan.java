package com.client360.configuration.wad.doormodel.accessoire.wood;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.doormodel.wood.GestionLargeurCampagne;
import com.client360.configuration.wad.profiles.ProfilePorteBoisA;
import com.client360.configuration.wad.profiles.ProfilePorteBoisB;
import com.netappsid.commonutils.math.Units;

public class CfpCamp2Pan extends GestionLargeurCampagne
{
	protected String posMnoGau = "posMnoGau";
	protected String posMnoDro = "posMnoDro";
	protected Double larMno = larMnoCfpCamp;

	protected String petitEmbosseGau = "petitEmbosseGau";
	protected String petitEmbosseDro = "petitEmbosseDro";
	protected String petitEmbosseHau = "petitEmbosseHau";
	protected String petitEmbosseBas = "petitEmbosseBas";

	@Override
	protected void load()
	{
		addLineLeftOffset(posMnoGau, vPatternCenter, larMno / 2);
		addLineRightOffset(posMnoDro, vPatternCenter, larMno / 2);

		// Lignes petit emboss�
		// TODO Ajuster rendu pour qu'il soit plus fid�le � la r�alit�...
		addLineRightOffset(petitEmbosseGau, patternLeft, larMntCfpCamp);
		addLineDownOffset(petitEmbosseHau, patternTop, larTraCfpCamp);
		addLineLeftOffset(petitEmbosseDro, patternRight, larMntCfpCamp);
		addLineUpOffset(petitEmbosseBas, patternSill, larTraCfpCamp);

		addForm(rectangle(left, right, top, sill));
		addSpacer(larMntCfpCamp, larTraCfpCamp, larMntCfpCamp, larTraCfpCamp);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());

		addForm(rectangle(petitEmbosseGau, posMnoGau, petitEmbosseHau, petitEmbosseBas));
		addSpacer(larPlateBandeVisible, larPlateBandeVisible, larPlateBandeVisible, larPlateBandeVisible);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());

		addForm(rectangle(posMnoDro, petitEmbosseDro, petitEmbosseHau, petitEmbosseBas));
		addSpacer(larPlateBandeVisible, larPlateBandeVisible, larPlateBandeVisible, larPlateBandeVisible);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}
}