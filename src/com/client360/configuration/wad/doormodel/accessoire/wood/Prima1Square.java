package com.client360.configuration.wad.doormodel.accessoire.wood;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class Prima1Square extends ParametricModel
{
	private final double oculusSpacer = 3.2d;

	public String oculusHaut = "oculusHaut";
	public String oculusBas = "oculusBas";
	public String oculusGauche = "oculusGauche";
	public String oculusDroit = "oculusDroit";

	@Override
	protected void load()
	{
		addForm(rectangle(left, right, top, sill));
		setInsulatedGlass(true);
		addEmptyProfile();
		addEmptySpacer();

		addForm(rectangle(left, right, top, sill));
		addSpacer(oculusSpacer, oculusSpacer, oculusSpacer, oculusSpacer);
		addEmptyProfile();
		setInsulatedGlass(false);

		addLineDownOffset(oculusHaut, top, oculusSpacer);
		addLineUpOffset(oculusBas, sill, oculusSpacer);
		addLineRightOffset(oculusGauche, left, oculusSpacer);
		addLineLeftOffset(oculusDroit, right, oculusSpacer);
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

}
