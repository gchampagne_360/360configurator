package com.client360.configuration.wad.doormodel.accessoire.wood;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.profiles.ProfileMoulure3DA;
import com.client360.configuration.wad.profiles.ProfileMoulure3DB;
import com.netappsid.commonutils.math.Units;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class CfcgStRecDemiSoleil extends ParametricModel
{
	private double spacer = 56d;

	protected String vitrageGauche = "vitrageGauche";
	protected String vitrageDroit = "vitrageDroit";
	protected String vitrageHaut = "vitrageHaut";
	protected String vitrageBas = "vitrageBas";

	@Override
	protected void load()
	{
		// add frame
		addForm(rectangle(left, right, top, sill));
		addSpacer(spacer, spacer, spacer, spacer);
		addProfile(new ProfileMoulure3DA(), new ProfileMoulure3DB());
		setInsulatedGlass(false);

		// add thermos
		addLineRightOffset(vitrageGauche, left, spacer + 6.3);
		addLineDownOffset(vitrageHaut, top, spacer + 6.3);
		addLineUpOffset(vitrageBas, sill, spacer + 6.3);
		addLineLeftOffset(vitrageDroit, right, spacer + 6.3);

		String imgPath = (String) getParametricObject("imageGrille");
		addForm(arc(vitrageGauche, vitrageDroit, vitrageHaut, vitrageBas), imgPath, ImageScale.FULL);
		setInsulatedGlass(true);
		addEmptyProfile();
		addEmptySpacer();
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}
}
