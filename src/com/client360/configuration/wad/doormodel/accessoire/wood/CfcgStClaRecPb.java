package com.client360.configuration.wad.doormodel.accessoire.wood;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.doormodel.accessoire.PetitBois;
import com.client360.configuration.wad.doormodel.wood.GestionLargeurClassique;
import com.client360.configuration.wad.profiles.ProfileMoulure3DA;
import com.client360.configuration.wad.profiles.ProfileMoulure3DB;
import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class CfcgStClaRecPb extends ParametricModel
{
	@Override
	protected void load()
	{
		addLineDownOffset("vTop", top, GestionLargeurClassique.larMntCfcgCl);
		addLineUpOffset("vBottom", sill, GestionLargeurClassique.larMntCfcgCl);
		addLineRightOffset("vLeft", left, GestionLargeurClassique.larMntCfcgCl);
		addLineLeftOffset("vRight", right, GestionLargeurClassique.larMntCfcgCl);

		// add vitrage
		addForm(rectangle("vLeft", "vRight", "vTop", "vBottom"));
		setInsulatedGlass(true);
		addEmptyProfile();
		addEmptySpacer();

		// add frame
		addForm(rectangle(left, right, top, sill));
		setInsulatedGlass(false);
		addSpacer(GestionLargeurClassique.larMntCfcgCl, GestionLargeurClassique.larTraCfcgCl, GestionLargeurClassique.larMntCfcgCl,
				GestionLargeurClassique.larTraCfcgCl);
		addProfile(new ProfileMoulure3DA(), new ProfileMoulure3DB());

		// add petit bois
		addElementSpace("pb", "vLeft", "vRight", "vTop", "vBottom");
		addSubModel(new PetitBois(), "pb");
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

}