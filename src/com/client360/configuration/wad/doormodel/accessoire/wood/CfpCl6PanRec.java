package com.client360.configuration.wad.doormodel.accessoire.wood;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.doormodel.wood.GestionLargeurClassique;
import com.client360.configuration.wad.profiles.ProfilePorteBoisA;
import com.client360.configuration.wad.profiles.ProfilePorteBoisB;
import com.netappsid.commonutils.math.Units;

/**
 * 
 * @author Gwenael Bonneau
 * @since 1.2 Migration addFormRectangle addForm(rectangle 2011/9/9
 */
public class CfpCl6PanRec extends GestionLargeurClassique
{

	protected String posMnoGau = "posMnoGau";
	protected String posMnoDro = "posMnoDro";
	protected Double larMno = larMnoCfpCl;
	protected Double larTraItr = larTraItrCfpCl;
	protected String petitEmbosseGau = "petitEmbosseGau";
	protected String petitEmbosseDro = "petitEmbosseDro";
	protected String petitEmbosseHau = "petitEmbosseHau";
	protected String petitEmbosseBas = "petitEmbosseBas";

	@Override
	protected void load()
	{

		int nbTraverse = 5;
		Double entreAxeTraverse = 0.0;

		entreAxeTraverse = (Math.ceil((hDist(patternTop, patternSill) - larTraCfpCl * 2) - (nbTraverse * larTraItr))) / (nbTraverse + 1);

		addLineRightOffset(petitEmbosseGau, patternLeft, larMntCfpCl);
		addLineLeftOffset(petitEmbosseDro, patternRight, larMntCfpCl);
		addLineDownOffset(petitEmbosseHau, patternTop, larTraCfpCl);
		addLineUpOffset(petitEmbosseBas, patternSill, larTraCfpCl);

		addLineDownOffset("posTra1Hau", patternTop, larTraCfpCl + entreAxeTraverse);
		addLineDownOffset("posTra1Bas", "posTra1Hau", larTraItrCfpCl);
		addLineDownOffset("posTra2Hau", "posTra1Bas", entreAxeTraverse);
		addLineDownOffset("posTra2Bas", "posTra2Hau", larTraItrCfpCl);
		addLineDownOffset("posTra3Hau", "posTra2Bas", entreAxeTraverse);
		addLineDownOffset("posTra3Bas", "posTra3Hau", larTraItrCfpCl);
		addLineDownOffset("posTra4Hau", "posTra3Bas", entreAxeTraverse);
		addLineDownOffset("posTra4Bas", "posTra4Hau", larTraItrCfpCl);
		addLineDownOffset("posTra5Hau", "posTra4Bas", entreAxeTraverse);
		addLineDownOffset("posTra5Bas", "posTra5Hau", larTraItrCfpCl);

		addForm(rectangle(petitEmbosseGau, petitEmbosseDro, petitEmbosseHau, "posTra1Hau"));
		addForm(rectangle(petitEmbosseGau, petitEmbosseDro, "posTra1Bas", "posTra2Hau"));
		addForm(rectangle(petitEmbosseGau, petitEmbosseDro, "posTra2Bas", "posTra3Hau"));
		addForm(rectangle(petitEmbosseGau, petitEmbosseDro, "posTra3Bas", "posTra4Hau"));
		addForm(rectangle(petitEmbosseGau, petitEmbosseDro, "posTra4Bas", "posTra5Hau"));
		addForm(rectangle(petitEmbosseGau, petitEmbosseDro, "posTra5Bas", petitEmbosseBas));

		for (int i = 0; i < 6; i++)
		{
			addSpacer(larPlateBandeVisible, larPlateBandeVisible, larPlateBandeVisible, larPlateBandeVisible);
		}

		addForm(rectangle(patternLeft, patternRight, patternTop, patternSill));
		addSpacer(larMntCfpCl, larTraCfpCl, larMntCfpCl, larTraCfpCl);

		for (int i = 0; i < 14; i++)
		{
			addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
		}

	}

	@Override
	protected Unit<Length> getUnit()
	{

		return Units.MM;
	}

}
