package com.client360.configuration.wad.doormodel.accessoire.wood;

import com.client360.configuration.wad.doormodel.wood.GestionLargeurStyle;
import com.client360.configuration.wad.profiles.ProfileMoulure3DA;
import com.client360.configuration.wad.profiles.ProfileMoulure3DB;
import com.client360.configuration.wad.profiles.ProfilePorteBoisA;
import com.client360.configuration.wad.profiles.ProfilePorteBoisB;

public class CfpSt2PanRec extends GestionLargeurStyle
{
	protected String posMnoGau = "posMnoGau";
	protected String posMnoDro = "posMnoDro";
	protected Double larMno = larMnoCfpSt;

	protected String petitEmbosseGau = "petitEmbosseGau";
	protected String petitEmbosseDro = "petitEmbosseDro";
	protected String petitEmbosseHau = "petitEmbosseHau";
	protected String petitEmbosseBas = "petitEmbosseBas";

	@Override
	protected void load()
	{
		addLineLeftOffset(posMnoGau, vPatternCenter, larMno / 2);
		addLineRightOffset(posMnoDro, vPatternCenter, larMno / 2);

		addLineRightOffset(petitEmbosseGau, patternLeft, larMntCfpSt);
		addLineDownOffset(petitEmbosseHau, patternTop, larTraCfpSt);
		addLineLeftOffset(petitEmbosseDro, patternRight, larMntCfpSt);
		addLineUpOffset(petitEmbosseBas, patternSill, larTraCfpSt);

		addForm(rectangle(petitEmbosseGau, posMnoGau, petitEmbosseHau, petitEmbosseBas));
		addSpacer(larPlateBandeVisible, larPlateBandeVisible, larPlateBandeVisible, larPlateBandeVisible);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());

		addForm(rectangle(posMnoDro, petitEmbosseDro, petitEmbosseHau, petitEmbosseBas));
		addSpacer(larPlateBandeVisible, larPlateBandeVisible, larPlateBandeVisible, larPlateBandeVisible);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());

		addForm(rectangle(patternLeft, patternRight, patternTop, patternSill));
		addSpacer(larMntCfpSt, larTraCfpSt, larMntCfpSt, larTraCfpSt);
		addProfile(new ProfileMoulure3DA(), new ProfileMoulure3DB());
	}
}
