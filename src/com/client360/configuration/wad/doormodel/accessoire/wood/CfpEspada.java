package com.client360.configuration.wad.doormodel.accessoire.wood;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.doormodel.accessoire.PetitBois;
import com.client360.configuration.wad.profiles.ProfileClassiqueA;
import com.client360.configuration.wad.profiles.ProfileClassiqueB;
import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class CfpEspada extends ParametricModel
{
	private final double largeurPetitBois = 52d;

	@Override
	protected void load()
	{
		// add insulated glass
		// addForm(rectangle(petitBoisHLeft, petitBoisHRight, "petitBoisVUp6", "petitBoisVDown1"));
		addForm(rectangle(left, right, top, sill));
		setInsulatedGlass(true);
		addEmptyProfile();
		addEmptySpacer();

		// add frame
		addForm(rectangle(left, right, top, sill));
		setInsulatedGlass(false);
		addSpacer(largeurPetitBois, largeurPetitBois, largeurPetitBois, largeurPetitBois);
		addProfile(new ProfileClassiqueA(), new ProfileClassiqueB());

		// add petit bois
		addLineDownOffset("pbTop", top, largeurPetitBois);
		addLineUpOffset("pbBottom", sill, largeurPetitBois);
		addLineRightOffset("pbLeft", left, largeurPetitBois);
		addLineLeftOffset("pbRight", right, largeurPetitBois);

		addElementSpace("pb", "pbLeft", "pbRight", "pbTop", "pbBottom");
		addSubModel(new PetitBois(), "pb");
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

}
