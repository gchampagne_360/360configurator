package com.client360.configuration.wad.doormodel.accessoire.wood;

import com.client360.configuration.wad.doormodel.wood.Campagne1vtl;
import com.client360.configuration.wad.profiles.ProfilePorteBoisA;
import com.client360.configuration.wad.profiles.ProfilePorteBoisB;

public class CfcgCampAnsPan extends Campagne1vtl
{

	@Override
	protected void load()
	{
		addForm(rectangle(left, right, top, sill));
		addSpacer(larMntCfcgCamp, larTraCfcgCamp, larMntCfcgCamp, larTraCfcgCamp);
		setInsulatedGlass(false);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());

		// addLineDownOffset("posBasAnsPan", patternTop, 221.5);
		addLineDownOffset("top2", top, larTraCfcgCamp + 25);
		addLineUpOffset("sill3", sill, larTraCfcgCamp + 25);
		addLineLeftOffset("right2", right, larMntCfcgCamp + 25);
		addLineRightOffset("left2", left, larMntCfcgCamp + 25);

		addLineDownOffset("posBasAnsPan2", "top2", 100);
		addForm(elongatedEllipse("left2", "right2", "top2", "sill3", "posBasAnsPan2"));
		setInsulatedGlass(true);
		// addSpacer(larMntCfcgCamp, larTraCfcgCamp, 1, larTraCfcgCamp);
		addSpacer(5, 5, 5, 5, 5, 5);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
	}
}
