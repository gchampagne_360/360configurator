package com.client360.configuration.wad.doormodel.accessoire.wood;

import com.client360.configuration.wad.doormodel.wood.GestionLargeurStyle;
import com.client360.configuration.wad.profiles.ProfilePorteBoisA;
import com.client360.configuration.wad.profiles.ProfilePorteBoisB;

public class PanneauMassifPostD2 extends GestionLargeurStyle
{

	@Override
	protected void load()
	{

		addForm(rectangle(left, right, top, sill));
		addSpacer(50, 70, 50, 70);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());

	}
}
