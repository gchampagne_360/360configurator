package com.client360.configuration.wad.doormodel.accessoire.wood;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.doormodel.wood.GestionLargeurCampagne;
import com.client360.configuration.wad.profiles.ProfilePorteBoisA;
import com.client360.configuration.wad.profiles.ProfilePorteBoisB;
import com.netappsid.commonutils.math.Units;

public class CfpCamp1Pan extends GestionLargeurCampagne
{
	protected String petitEmbosseGauche = "petitEmbosseGauche";
	protected String petitEmbosseDroit = "petitEmbosseDroit";
	protected String petitEmbosseHaut = "petitEmbosseHaut";
	protected String petitEmbosseBas = "petitEmbosseBas";

	@Override
	protected void load()
	{
		addLineRightOffset(petitEmbosseGauche, patternLeft, larMntCfpCamp);
		addLineDownOffset(petitEmbosseHaut, patternTop, larTraCfpCamp);
		addLineLeftOffset(petitEmbosseDroit, patternRight, larMntCfpCamp);
		addLineUpOffset(petitEmbosseBas, patternSill, larTraCfpCamp);

		addForm(rectangle(left, right, top, sill));
		addSpacer(larTraCfpCamp, larMntCfpCamp, larTraCfpCamp, larMntCfpCamp);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());

		addForm(rectangle(petitEmbosseGauche, petitEmbosseDroit, petitEmbosseHaut, petitEmbosseBas));
		addSpacer(larPlateBandeVisible, larPlateBandeVisible, larPlateBandeVisible, larPlateBandeVisible);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}
}