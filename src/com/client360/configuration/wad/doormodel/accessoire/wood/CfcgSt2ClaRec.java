package com.client360.configuration.wad.doormodel.accessoire.wood;

import com.client360.configuration.wad.doormodel.accessoire.PetitBois;
import com.client360.configuration.wad.doormodel.wood.GestionLargeurStyle;
import com.client360.configuration.wad.profiles.ProfileMoulure3DA;
import com.client360.configuration.wad.profiles.ProfileMoulure3DB;

public class CfcgSt2ClaRec extends GestionLargeurStyle
{
	protected String posMnoGau = "posMnoGau";
	protected String posMnoDro = "posMnoDro";
	protected Double larMno = larMnoCfpSt;

	protected String petitEmbosseGau = "petitEmbosseGau";
	protected String petitEmbosseDro = "petitEmbosseDro";
	protected String petitEmbosseHau = "petitEmbosseHau";
	protected String petitEmbosseBas = "petitEmbosseBas";

	@Override
	protected void load()
	{
		addLineLeftOffset(posMnoGau, vCenter, larMno / 2);
		addLineRightOffset(posMnoDro, vCenter, larMno / 2);

		addLineRightOffset(petitEmbosseGau, left, larMntCfpSt);
		addLineDownOffset(petitEmbosseHau, top, larTraCfpSt);
		addLineLeftOffset(petitEmbosseDro, right, larMntCfpSt);
		addLineUpOffset(petitEmbosseBas, sill, larTraCfpSt);

		// vitarge gauche
		// String imgPath = (String) getParametricObject("imageGrille");
		addForm(rectangle(petitEmbosseGau, posMnoGau, petitEmbosseHau, petitEmbosseBas));// , imgPath, ImageScale.FULL);
		setInsulatedGlass(true);
		addEmptyProfile();
		addEmptySpacer();

		// vitrage droit
		addForm(rectangle(posMnoDro, petitEmbosseDro, petitEmbosseHau, petitEmbosseBas));// , imgPath, ImageScale.FULL);
		setInsulatedGlass(true);
		addEmptyProfile();
		addEmptySpacer();

		// cadre
		addForm(rectangle(left, right, top, sill));
		addSpacer(larMntCfpSt, larTraCfpSt, larMntCfpSt, larTraCfpSt);
		addProfile(new ProfileMoulure3DA(), new ProfileMoulure3DB());

		// petit bois
		addElementSpace("pb", petitEmbosseGau, petitEmbosseDro, petitEmbosseHau, petitEmbosseBas);
		addSubModel(new PetitBois(), "pb");
	}
}
