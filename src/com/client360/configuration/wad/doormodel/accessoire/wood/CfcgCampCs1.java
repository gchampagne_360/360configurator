package com.client360.configuration.wad.doormodel.accessoire.wood;

import com.client360.configuration.wad.doormodel.accessoire.PetitBois;
import com.client360.configuration.wad.doormodel.wood.Campagne1vtl;
import com.client360.configuration.wad.profiles.ProfilePorteBoisA;
import com.client360.configuration.wad.profiles.ProfilePorteBoisB;

public class CfcgCampCs1 extends Campagne1vtl
{
	private double spacerCadre = 40.5;
	private double spacerCadreIG = 11;

	public String cadreIntGauche = "cadreIntGauche";
	public String cadreIntDroit = "cadreIntDroit";
	public String cadreIntHaut = "cadreIntHaut";
	public String cadreIntBas = "cadreIntBas";

	public String cadreIGHaut = "cadreIGHaut";
	public String cadreIGH2 = "cadreIGH2";

	@Override
	protected void load()
	{
		addForm(rectangle(left, right, top, sill));
		addSpacer(spacerCadre, spacerCadre, spacerCadre, spacerCadre);
		setInsulatedGlass(false);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());

		addLineDownOffset(cadreIntHaut, top, spacerCadre);
		addLineUpOffset(cadreIntBas, sill, spacerCadre);
		addLineLeftOffset(cadreIntDroit, right, spacerCadre);
		addLineRightOffset(cadreIntGauche, left, spacerCadre);

		addLineDownOffset(cadreIGHaut, cadreIntHaut, 10);
		addLineDownOffset(cadreIGH2, cadreIGHaut, 51.5);

		// add thermos
		addForm(elongatedArc(cadreIntGauche, cadreIntDroit, cadreIGHaut, cadreIntBas, cadreIGH2));
		setInsulatedGlass(true);
		addEmptySpacer();
		addEmptyProfile();

		// add frame
		addForm(elongatedArc(cadreIntGauche, cadreIntDroit, cadreIGHaut, cadreIntBas, cadreIGH2));
		addSpacer(spacerCadreIG, spacerCadreIG, spacerCadreIG, spacerCadreIG, spacerCadreIG, spacerCadreIG);
		setInsulatedGlass(false);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());

		// add petit bois
		addLineDownOffset("pbTop", cadreIGHaut, spacerCadreIG);
		addLineUpOffset("pbBottom", cadreIntBas, spacerCadreIG);
		addLineRightOffset("pbLeft", cadreIntGauche, spacerCadreIG);
		addLineLeftOffset("pbRight", cadreIntDroit, spacerCadreIG);

		addElementSpace("pb", "pbLeft", "pbRight", "pbTop", "pbBottom");
		addSubModel(new PetitBois(), "pb");
	}
}