package com.client360.configuration.wad.doormodel.accessoire.wood;

import com.client360.configuration.wad.doormodel.wood.GestionLargeurClassique;
import com.client360.configuration.wad.profiles.ProfileMoulure3DA;
import com.client360.configuration.wad.profiles.ProfileMoulure3DB;

public class CfpClMoulure extends GestionLargeurClassique
{

	@Override
	protected void load()
	{
		addForm(rectangle(patternLeft, patternRight, patternTop, patternSill));
		addSpacer(larMntCfpCl, larMntCfpCl, larMntCfpCl, larMntCfpCl);
		addProfile(new ProfileMoulure3DA(), new ProfileMoulure3DB());
	}

}
