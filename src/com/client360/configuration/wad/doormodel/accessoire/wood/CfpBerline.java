package com.client360.configuration.wad.doormodel.accessoire.wood;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.doormodel.accessoire.PetitBois;
import com.client360.configuration.wad.profiles.ProfileClassiqueA;
import com.client360.configuration.wad.profiles.ProfileClassiqueB;
import com.client360.configuration.wad.profiles.ProfilePorteBoisA;
import com.client360.configuration.wad.profiles.ProfilePorteBoisB;
import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class CfpBerline extends ParametricModel
{
	public final String pan1Gauche = "pan1Gauche";
	public final String pan1Droite = "pan1Droite";
	public final String pan2Gauche = "pan2Gauche";
	public final String pan2Droite = "pan2Droite";
	public final String panHaut = "panHaut";
	public final String panBas = "panBas";

	public final String moulureHaut = "moulureHaut";
	public final String moulureBas = "moulureBas";
	public final String moulure1Gauche = "moulure1Gauche";
	public final String moulure1Droite = "moulure1Droite";
	public final String moulure2Gauche = "moulure2Gauche";
	public final String moulure2Droite = "moulure2Droite";

	public final String rec1Gauche = "rec1Gauche";
	public final String rec1Droite = "rec1Droite";
	public final String rec2Gauche = "rec2Gauche";
	public final String rec2Droite = "rec2Droite";
	public final String recHaut = "recHaut";
	public final String recBas = "recBas";

	private double cadre = 37;
	private double spacer = 15;
	private double rec = 26;

	@Override
	protected void load()
	{
		Integer larPbe = Integer.parseInt((String) getParametricObject("larPbe"));

		double largeurPanneauLateral = (vDist(left, right) - (2 * cadre) - (2 * larPbe)) / 4;

		addLineRightOffset(pan1Gauche, left, cadre);
		addLineRightOffset(pan1Droite, pan1Gauche, largeurPanneauLateral);

		addLineLeftOffset(pan2Droite, right, cadre);
		addLineLeftOffset(pan2Gauche, pan2Droite, largeurPanneauLateral);

		addLineDownOffset(panHaut, top, cadre);
		addLineUpOffset(panBas, sill, cadre);

		// add vitrage
		addForm(rectangle(pan1Droite, pan2Gauche, panHaut, panBas));
		setInsulatedGlass(true);
		addEmptyProfile();
		addEmptySpacer();

		// add frame
		addForm(rectangle(left, right, top, sill));
		setInsulatedGlass(false);
		addSpacer(cadre, cadre, cadre, cadre);
		addProfile(new ProfileClassiqueA(), new ProfileClassiqueB());

		// add panneau gauche
		addForm(rectangle(pan1Gauche, pan1Droite, panHaut, panBas));
		addSpacer(spacer, spacer, spacer, spacer);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
		setInsulatedGlass(false);

		addLineRightOffset(rec1Gauche, pan1Gauche, spacer);
		addLineLeftOffset(rec1Droite, pan1Droite, spacer);
		addLineUpOffset(recBas, panBas, spacer);
		addLineDownOffset(recHaut, panHaut, spacer);

		addForm(rectangle(rec1Gauche, rec1Droite, recHaut, recBas));
		addSpacer(rec, rec, rec, rec);
		addEmptyProfile();
		setInsulatedGlass(false);

		// add panneau droit
		addForm(rectangle(pan2Gauche, pan2Droite, panHaut, panBas));
		addSpacer(spacer, spacer, spacer, spacer);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
		setInsulatedGlass(false);

		addLineRightOffset(rec2Gauche, pan2Gauche, spacer);
		addLineLeftOffset(rec2Droite, pan2Droite, spacer);

		addForm(rectangle(rec2Gauche, rec2Droite, recHaut, recBas));
		addSpacer(rec, rec, rec, rec);
		addEmptyProfile();
		setInsulatedGlass(false);

		// add petit bois
		addLineLeftOffset(moulure1Gauche, pan1Droite, spacer);
		addLineRightOffset(moulure1Droite, moulure1Gauche, larPbe);

		addLineRightOffset(moulure2Droite, pan2Gauche, spacer);
		addLineLeftOffset(moulure2Gauche, moulure2Droite, larPbe);

		addLineDownOffset(moulureHaut, panHaut, 7);
		addLineUpOffset(moulureBas, panBas, 7);

		addForm(rectangle(moulure1Gauche, moulure1Droite, panHaut, panBas));
		setInsulatedGlass(false);
		addSpacer(-spacer, spacer, -spacer, spacer);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());

		addForm(rectangle(moulure2Gauche, moulure2Droite, panHaut, panBas));
		setInsulatedGlass(false);
		addSpacer(-spacer, spacer, -spacer, spacer);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());

		addElementSpace("pb", moulure1Droite, moulure2Gauche, moulureHaut, moulureBas);
		addSubModel(new PetitBois(), "pb");
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}
}
