package com.client360.configuration.wad.doormodel.accessoire.wood;

import com.client360.configuration.wad.doormodel.accessoire.PetitBois;
import com.client360.configuration.wad.doormodel.wood.GestionLargeurStyle;
import com.client360.configuration.wad.profiles.ProfileMoulurePanneauA;

/**
 * 
 * @author Gwenael Bonneau
 * @since 1.3 nettoyage commentaires : Migration addFormRectangle addForm(rectangle 2011/9/9
 */
public class CfpStPallas36 extends GestionLargeurStyle
{

	protected String posMnoGau = "posMnoGau";
	protected String posMnoDro = "posMnoDro";
	protected String posTraItrHau = "posTraItrHau";
	protected String posTraItrBas = "posTraItrBas";

	protected Double larMno = larMnoCfpPallas36;
	protected Double larTraItr = larTraItrCfpPallas36;

	protected String petitEmbosseGau = "petitEmbosseGau";
	protected String petitEmbosseDro = "petitEmbosseDro";
	protected String petitEmbosseHau = "petitEmbosseHau";
	protected String petitEmbosseBas = "petitEmbosseBas";

	@Override
	protected void load()
	{
		/*
		 * addLineUpOffset(posTraItrHau, hPatternCenter, larTraItr /2); addLineDownOffset(posTraItrBas, hPatternCenter, larTraItr /2);
		 * 
		 * addLineLeftOffset(posMnoGau, vPatternCenter, larMno/2); addLineRightOffset(posMnoDro, vPatternCenter, larMno/2);
		 * 
		 * addLineLeftOffset(petitEmbosseDro, patternRight, larMntCfpSt); addLineUpOffset(petitEmbosseBas, patternSill, larMntCfpSt);
		 * addLineDownOffset(petitEmbosseHau, patternTop, larMntCfpSt); addLineRightOffset(petitEmbosseGau, patternLeft, larMntCfpSt);
		 * 
		 * addForm(rectangle(petitEmbosseGau, posMnoGau, petitEmbosseHau, posTraItrHau)); addForm(rectangle(posMnoDro, petitEmbosseDro, petitEmbosseHau,
		 * posTraItrHau)); addForm(rectangle(petitEmbosseGau, posMnoGau, posTraItrBas, petitEmbosseBas)); addForm(rectangle(posMnoDro, petitEmbosseDro,
		 * posTraItrBas, petitEmbosseBas));
		 * 
		 * addSpacer(larPlateBandeVisible,larPlateBandeVisible,larPlateBandeVisible,larPlateBandeVisible);
		 * addSpacer(larPlateBandeVisible,larPlateBandeVisible,larPlateBandeVisible,larPlateBandeVisible);
		 * addSpacer(larPlateBandeVisible,larPlateBandeVisible,larPlateBandeVisible,larPlateBandeVisible);
		 * addSpacer(larPlateBandeVisible,larPlateBandeVisible,larPlateBandeVisible,larPlateBandeVisible);
		 * 
		 * addForm(rectangle(patternLeft,patternRight,patternTop,patternSill));
		 * addSpacer(larMntCfpPallas36,larTraCfpPallas36,larMntCfpPallas36,larTraCfpPallas36);
		 * 
		 * addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB()); addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB()); addProfile(new
		 * ProfilePorteBoisA(), new ProfilePorteBoisB()); addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB()); addProfile(new ProfilePorteBoisA(), new
		 * ProfilePorteBoisB()); addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
		 */

		addForm(rectangle(left, right, top, sill));
		addSpacer(larMntCfpPallas36, larTraCfpPallas36, larMntCfpPallas36, larTraCfpPallas36);
		addProfile(new ProfileMoulurePanneauA(), new ProfileMoulurePanneauA());

		addLineDownOffset(petitEmbosseHau, top, larTraCfpPallas36);
		addLineUpOffset(petitEmbosseBas, sill, larTraCfpPallas36);
		addLineRightOffset(petitEmbosseGau, left, larTraCfpPallas36);
		addLineLeftOffset(petitEmbosseDro, right, larTraCfpPallas36);

		// keep trace of current parametric objects
		Object oldLarPbe = getParametricObject("larPbe");
		Object oldlarSpacer = (String) getParametricObject("larSpacer");
		Object oldPbv = getParametricObject("pbv");
		Object oldPbh = getParametricObject("pbh");

		// add petit bois separating the panel in 2
		addParametricObject("larPbe", "96");
		addParametricObject("larSpacer", "25");
		addParametricObject("pbh", "1");
		addParametricObject("pbv", "1");
		addElementSpace("pb", petitEmbosseGau, petitEmbosseDro, petitEmbosseHau, petitEmbosseBas);
		addSubModel(new PetitBois(), "pb");

		// restore old parametric objects
		addParametricObject("larPbe", oldLarPbe);
		addParametricObject("larSpacer", oldlarSpacer);
		addParametricObject("pbh", oldPbv);
		addParametricObject("pbv", oldPbh);

	}

}
