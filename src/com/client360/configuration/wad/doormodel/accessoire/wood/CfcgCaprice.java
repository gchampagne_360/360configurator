package com.client360.configuration.wad.doormodel.accessoire.wood;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.doormodel.accessoire.PetitBois;
import com.client360.configuration.wad.doormodel.wood.GestionLargeurClassique;
import com.client360.configuration.wad.profiles.ProfileMoulure3DA;
import com.client360.configuration.wad.profiles.ProfileMoulure3DB;
import com.client360.configuration.wad.profiles.ProfilePorteBoisA;
import com.client360.configuration.wad.profiles.ProfilePorteBoisB;
import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class CfcgCaprice extends ParametricModel
{
	public String cadre1IntHaut = "cadre1IntHaut";
	public String cadre1IntBas = "cadre1IntBas";
	public String cadre1IntGauche = "cadre1IntGauche";
	public String cadre1IntDroit = "cadre1IntDroit";

	public String cadre2ExtHaut = "cadre2ExtHaut";
	public String cadre2ExtBas = "cadre2ExtBas";
	public String cadre2ExtGauche = "cadre2ExtGauche";
	public String cadre2ExtDroit = "cadre2ExtDroit";

	public String cadre2IntHaut = "cadre2IntHaut";
	public String cadre2IntBas = "cadre2IntBas";
	public String cadre2IntGauche = "cadre2IntGauche";
	public String cadre2IntDroit = "cadre12ntDroit";

	private double spacerInt = 11;

	@Override
	protected void load()
	{
		addLineDownOffset(cadre1IntHaut, top, GestionLargeurClassique.larMntCfcgCl);
		addLineUpOffset(cadre1IntBas, sill, GestionLargeurClassique.larMntCfcgCl);
		addLineRightOffset(cadre1IntGauche, left, GestionLargeurClassique.larMntCfcgCl);
		addLineLeftOffset(cadre1IntDroit, right, GestionLargeurClassique.larMntCfcgCl);

		// addLineDownOffset(cadre2ExtHaut, cadre1IntHaut, 16);
		// addLineUpOffset(cadre2ExtBas, cadre1IntBas, 16);
		// addLineRightOffset(cadre2ExtGauche, cadre1IntGauche, 16);
		// addLineLeftOffset(cadre2ExtDroit, cadre1IntDroit, 16);

		addLineDownOffset(cadre2IntHaut, cadre1IntHaut, spacerInt);
		addLineUpOffset(cadre2IntBas, cadre1IntBas, spacerInt);
		addLineRightOffset(cadre2IntGauche, cadre1IntGauche, spacerInt);
		addLineLeftOffset(cadre2IntDroit, cadre1IntDroit, spacerInt);

		// add vitrage
		addForm(rectangle(cadre1IntGauche, cadre1IntDroit, cadre1IntHaut, cadre1IntBas));
		setInsulatedGlass(true);
		addEmptyProfile();
		addEmptySpacer();

		// add exterior frame
		addForm(rectangle(left, right, top, sill));
		setInsulatedGlass(false);
		addSpacer(GestionLargeurClassique.larMntCfcgCl, GestionLargeurClassique.larTraCfcgCl, GestionLargeurClassique.larMntCfcgCl,
				GestionLargeurClassique.larTraCfcgCl);
		addProfile(new ProfileMoulure3DA(), new ProfileMoulure3DB());

		// add interior frame
		addForm(rectangle(cadre1IntGauche, cadre1IntDroit, cadre1IntHaut, cadre1IntBas));
		setInsulatedGlass(false);
		addSpacer(spacerInt, spacerInt, spacerInt, spacerInt);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());

		// add petit bois
		addElementSpace("pb", cadre2IntGauche, cadre2IntDroit, cadre2IntHaut, cadre2IntBas);
		addSubModel(new PetitBois(), "pb");
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

}