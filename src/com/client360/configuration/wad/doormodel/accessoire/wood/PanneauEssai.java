package com.client360.configuration.wad.doormodel.accessoire.wood;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.profiles.ProfilePorteBoisA;
import com.client360.configuration.wad.profiles.ProfilePorteBoisB;
import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

/**
 * Exemple de panneau simple ne d�pendent pas du mod�le de porte qui le contient. Cet accessoire devient ainsi utilisable dans tous les mod�les de porte sans
 * souci.
 * 
 * @author Marc-Andr� Bouchard
 * @author 360 Innovations
 * 
 */
public class PanneauEssai extends ParametricModel
{
	@Override
	protected void load()
	{
		addForm(rectangle(left, right, top, sill));
		addSpacer(20, 20, 20, 20);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}
}