package com.client360.configuration.wad.doormodel.accessoire.wood;

import com.client360.configuration.wad.doormodel.accessoire.PetitBois;

public class CfpStPanRecHiPb extends CfpStPanRecHi
{
	public String cadre1IntHaut = "cadre1IntHaut";
	public String cadre1IntBas = "cadre1IntBas";
	public String cadre1IntGauche = "cadre1IntGauche";
	public String cadre1IntDroit = "cadre1IntDroit";

	@Override
	protected void load()
	{
		super.load();

		addLineDownOffset(cadre1IntHaut, top, spacerCadre);
		addLineUpOffset(cadre1IntBas, sill, spacerCadre);
		addLineRightOffset(cadre1IntGauche, left, spacerCadre);
		addLineLeftOffset(cadre1IntDroit, right, spacerCadre);

		// keep trace of current parametric objects
		Object oldLarPbe = getParametricObject("larPbe");
		Object oldlarSpacer = (String) getParametricObject("larSpacer");
		Object oldPbv = getParametricObject("pbv");
		Object oldPbh = getParametricObject("pbh");

		// add petit bois separating the panel in 2
		addParametricObject("larPbe", "28");
		addParametricObject("larSpacer", "11");
		addParametricObject("pbh", "0");
		addParametricObject("pbv", "1");
		addElementSpace("pb", cadre1IntGauche, cadre1IntDroit, cadre1IntHaut, cadre1IntBas);
		addSubModel(new PetitBois(), "pb");

		// restore old parametric objects
		addParametricObject("larPbe", oldLarPbe);
		addParametricObject("larSpacer", oldlarSpacer);
		addParametricObject("pbh", oldPbv);
		addParametricObject("pbv", oldPbh);
	}
}
