package com.client360.configuration.wad.doormodel.accessoire.wood;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.cad.CadJetdeau;
import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class JetDeau extends ParametricModel
{
	@Override
	protected void load()
	{
		addDrawing(new CadJetdeau(), patternLeft, patternRight, patternTop, patternSill, Orientation.HORIZONTAL);
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}
}