package com.client360.configuration.wad.doormodel.accessoire.wood;

public class ParametreAccessoire
{

	// Chassis Campagne

	// Chassis � panneau campagne
	// public static Double larMntCfpCamp = 55.5;
	// public static Double larTraCfpCamp = 55.5;
	// public static Double larMnoCfpCamp = 56.0;

	// Chassis � carreaux campagne
	// public static Double larMntCfcgCamp = 51.5;
	// public static Double larTraCfcgCamp = 51.5;
	// public static Double larPbeCfcgCamp = 32.0;

	// Chassis Style

	// Chassis � panneau style
	public static Double larMntCfpSt = 55.5;
	public static Double larTraCfpSt = 55.5;
	public static Double larMnoCfpSt = 56.0;

	// Chassis � panneau pallas 36 mm
	public static Double larMntCfpPallas36 = 55.5;
	public static Double larTraCfpPallas36 = 55.5;
	public static Double larMnoCfpPallas36 = 96.0;
	public static Double larTraItrCfpPallas36 = 96.0;

	// Chassis � panneau pallas 47 mm
	public static Double larMntCfpPallas47 = 55.5;
	public static Double larTraCfpPallas47 = 55.5;
	public static Double larMnoCfpPallas47 = 96.0;
	public static Double larTraItrCfpPallas47 = 96.0;

	// Chassis � carreaux style
	public static Double larMntCfcgSt = 51.5;
	public static Double larTraCfcgSt = 51.5;
	public static Double larPbeCfcgSt = 32.0;

	// Chassis Classique

	// Chassis � panneau classique
	public static Double larMntCfpCla = 55.5;
	public static Double larTraCfpCla = 55.5;
	public static Double larMnoCfpCla = 56.0;

	// Chassis � carreaux classique
	public static Double larMntCfcgCla = 51.5;
	public static Double larTraCfcgCla = 51.5;
	public static Double larPbeCfcgCla = 32.0;

}
