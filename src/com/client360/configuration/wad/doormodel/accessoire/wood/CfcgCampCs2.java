package com.client360.configuration.wad.doormodel.accessoire.wood;

import com.client360.configuration.wad.doormodel.accessoire.PetitBois;
import com.client360.configuration.wad.doormodel.wood.Campagne1vtl;
import com.client360.configuration.wad.doormodel.wood.GestionLargeurCampagne;
import com.client360.configuration.wad.profiles.ProfilePorteBoisA;
import com.client360.configuration.wad.profiles.ProfilePorteBoisB;

public class CfcgCampCs2 extends Campagne1vtl
{
	private double spacerCadre = GestionLargeurCampagne.larMntCfcgCamp;

	public String frameH2 = "frameH2";

	public String cadreIntGauche = "cadreIntGauche";
	public String cadreIntDroit = "cadreIntDroit";
	public String cadreIntHaut = "cadreIntHaut";
	public String cadreIntBas = "cadreIntBas";
	public String cadreIntH2 = "cadreIntH2";

	@Override
	protected void load()
	{
		double fleche = 120;
		addLineDownOffset(frameH2, top, fleche);
		addLineDownOffset(cadreIntHaut, top, spacerCadre);
		addLineUpOffset(cadreIntBas, sill, spacerCadre);
		addLineLeftOffset(cadreIntDroit, right, spacerCadre);
		addLineRightOffset(cadreIntGauche, left, spacerCadre);

		// add thermos
		addForm(elongatedArc(left, right, top, sill, frameH2));
		setInsulatedGlass(true);
		addEmptySpacer();
		addEmptyProfile();

		// add frame
		addForm(elongatedArc(left, right, top, sill, frameH2));
		addSpacer(spacerCadre, spacerCadre, spacerCadre, spacerCadre, spacerCadre);
		setInsulatedGlass(false);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());

		// add petit bois
		addElementSpace("pb", cadreIntGauche, cadreIntDroit, cadreIntHaut, cadreIntBas);
		addSubModel(new PetitBois(), "pb");
	}
}