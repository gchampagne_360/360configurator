package com.client360.configuration.wad.doormodel.accessoire.wood;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.doormodel.wood.GestionLargeurClassique;
import com.client360.configuration.wad.profiles.ProfilePorteBoisA;
import com.client360.configuration.wad.profiles.ProfilePorteBoisB;
import com.netappsid.commonutils.math.Units;

/**
 * 
 * @author Gwenael Bonneau
 * @since 1.1 Migration addFormRectangle addForm(rectangle 2011/9/9
 */
public class CfpCl3PanRec extends GestionLargeurClassique
{

	protected Double larTraItr = larTraItrCfpCl;
	protected String petitEmbosseGau = "petitEmbosseGau";
	protected String petitEmbosseDro = "petitEmbosseDro";
	protected String petitEmbosseHau = "petitEmbosseHau";
	protected String petitEmbosseBas = "petitEmbosseBas";

	@Override
	protected void load()
	{

		addLineRightOffset(petitEmbosseGau, patternLeft, larMntCfpCl);
		addLineLeftOffset(petitEmbosseDro, patternRight, larMntCfpCl);
		addLineDownOffset(petitEmbosseHau, patternTop, larTraCfpCl);
		addLineUpOffset(petitEmbosseBas, patternSill, larTraCfpCl);

		addLineDownOffset("posTra1Hau", patternTop, 897);
		addLineDownOffset("posTra1Bas", "posTra1Hau", larTraItrCfpCl);
		addLineUpOffset("posTra2Hau", patternSill, 556);
		addLineDownOffset("posTra2Bas", "posTra2Hau", larTraItrCfpCl);

		addForm(rectangle(petitEmbosseGau, petitEmbosseDro, petitEmbosseHau, "posTra1Hau"));
		addForm(rectangle(petitEmbosseGau, petitEmbosseDro, "posTra1Bas", "posTra2Hau"));
		addForm(rectangle(petitEmbosseGau, petitEmbosseDro, "posTra2Bas", petitEmbosseBas));

		for (int i = 0; i < 3; i++)
		{
			addSpacer(larPlateBandeVisible, larPlateBandeVisible, larPlateBandeVisible, larPlateBandeVisible);
		}

		addForm(rectangle(patternLeft, patternRight, patternTop, patternSill));
		addSpacer(larMntCfpCl, larTraCfpCl, larMntCfpCl, larTraCfpCl);

		for (int i = 0; i < 8; i++)
		{
			addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
		}

	}

	@Override
	protected Unit<Length> getUnit()
	{

		return Units.MM;
	}

}
