package com.client360.configuration.wad.doormodel.accessoire.wood;

import com.client360.configuration.wad.doormodel.wood.GestionLargeurClassique;
import com.client360.configuration.wad.profiles.ProfileClassiqueA;
import com.client360.configuration.wad.profiles.ProfileClassiqueB;
import com.client360.configuration.wad.profiles.ProfilePorteBoisA;
import com.client360.configuration.wad.profiles.ProfilePorteBoisB;

public class CfpCl5PanRec extends GestionLargeurClassique
{

	protected Double larMno = larMnoCfpCl;
	protected double larSpacerPb = 15;
	protected double larSpacerPanneau = 30;
	protected double hauteurPb1 = 879;
	protected double hauteurPb2 = 537;

	protected String petitEmbosseGau = "petitEmbosseGau";
	protected String petitEmbosseDro = "petitEmbosseDro";
	protected String petitEmbosseHau = "petitEmbosseHau";
	protected String petitEmbosseBas = "petitEmbosseBas";

	protected String pbV1Haut = "pbV1Haut";
	protected String pbV1Bas = "pbV1Bas";
	protected String pbV2Haut = "pbV2Haut";
	protected String pbV2Bas = "pbV2Bas";
	protected String pbVGauche = "pbVGauche";
	protected String pbVDroit = "pbVDroit";

	protected String pbH1Haut = "pbH1Haut";
	protected String pbH1Bas = "pbH1Bas";
	protected String pbH2Haut = "pbH2Haut";
	protected String pbH2Bas = "pbH2Bas";
	protected String pbHGauche = "pbHGauche";
	protected String pbHDroit = "pbHDroit";

	@Override
	protected void load()
	{
		addLineLeftOffset(pbVGauche, vCenter, larMno / 2);
		addLineRightOffset(pbVDroit, vCenter, larMno / 2);

		// frame
		addForm(rectangle(left, right, top, sill));
		addSpacer(larMntCfpCl, larMntCfpCl, larMntCfpCl, larMntCfpCl);
		addProfile(new ProfileClassiqueA(), new ProfileClassiqueB());

		addLineRightOffset(petitEmbosseGau, left, larMntCfpCl);
		addLineLeftOffset(petitEmbosseDro, right, larMntCfpCl);
		addLineDownOffset(petitEmbosseHau, top, larTraCfpCl);
		addLineUpOffset(petitEmbosseBas, sill, larTraCfpCl);

		addLineLeftOffset(pbHGauche, petitEmbosseGau, larSpacerPb);
		addLineRightOffset(pbHDroit, petitEmbosseDro, larSpacerPb);

		addLineUpOffset(pbV1Haut, petitEmbosseHau, larSpacerPb);
		addLineDownOffset(pbV1Bas, pbV1Haut, hauteurPb1);

		addLineDownOffset(pbV2Bas, petitEmbosseBas, larSpacerPb);
		addLineUpOffset(pbV2Haut, pbV2Bas, hauteurPb2);

		addLineUpOffset(pbH1Haut, pbV1Bas, larSpacerPb);
		addLineDownOffset(pbH1Bas, pbH1Haut, larMno);

		addLineDownOffset(pbH2Bas, pbV2Haut, larSpacerPb);
		addLineUpOffset(pbH2Haut, pbH2Bas, larMno);

		// petit bois horizontal haut
		addForm(rectangle(pbHGauche, pbHDroit, pbH1Haut, pbH1Bas));
		addSpacer(larSpacerPb, -larSpacerPb, larSpacerPb, -larSpacerPb);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());

		// petit bois horizontal bas
		addForm(rectangle(pbHGauche, pbHDroit, pbH2Haut, pbH2Bas));
		addSpacer(larSpacerPb, -larSpacerPb, larSpacerPb, -larSpacerPb);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());

		// petit bois vertical haut
		addForm(rectangle(pbVGauche, pbVDroit, pbV1Haut, pbV1Bas));
		addSpacer(-larSpacerPb, larSpacerPb, -larSpacerPb, larSpacerPb);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());

		// petit bois vertical bas
		addForm(rectangle(pbVGauche, pbVDroit, pbV2Haut, pbV2Bas));
		addSpacer(-larSpacerPb, larSpacerPb, -larSpacerPb, larSpacerPb);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());

		// panneaux intérieurs
		addForm(rectangle(petitEmbosseGau, pbVGauche, petitEmbosseHau, pbH1Haut));
		addSpacer(larSpacerPanneau, larSpacerPanneau, larSpacerPanneau, larSpacerPanneau);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());

		addForm(rectangle(pbVDroit, petitEmbosseDro, petitEmbosseHau, pbH1Haut));
		addSpacer(larSpacerPanneau, larSpacerPanneau, larSpacerPanneau, larSpacerPanneau);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());

		addForm(rectangle(petitEmbosseGau, petitEmbosseDro, pbH1Bas, pbH2Haut));
		addSpacer(larSpacerPanneau, larSpacerPanneau, larSpacerPanneau, larSpacerPanneau);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());

		addForm(rectangle(petitEmbosseGau, pbVGauche, pbH2Bas, petitEmbosseBas));
		addSpacer(larSpacerPanneau, larSpacerPanneau, larSpacerPanneau, larSpacerPanneau);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());

		addForm(rectangle(pbVDroit, petitEmbosseDro, pbH2Bas, petitEmbosseBas));
		addSpacer(larSpacerPanneau, larSpacerPanneau, larSpacerPanneau, larSpacerPanneau);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
	}

}
