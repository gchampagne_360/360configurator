package com.client360.configuration.wad.doormodel.accessoire.wood;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.profiles.ProfileMoulure3DA;
import com.client360.configuration.wad.profiles.ProfileMoulure3DB;
import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class CfpStPanRecHi extends ParametricModel
{
	protected double spacerCadre = 56;

	@Override
	protected void load()
	{
		addForm(rectangle(left, right, top, sill));
		addSpacer(spacerCadre, spacerCadre, spacerCadre, spacerCadre);
		addProfile(new ProfileMoulure3DA(), new ProfileMoulure3DB());
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

}
