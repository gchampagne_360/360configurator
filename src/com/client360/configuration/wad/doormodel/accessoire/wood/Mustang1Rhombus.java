package com.client360.configuration.wad.doormodel.accessoire.wood;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class Mustang1Rhombus extends ParametricModel
{

	@Override
	protected void load()
	{
		addForm(rhombus(left, right, top, sill));
		setInsulatedGlass(true);
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

}
