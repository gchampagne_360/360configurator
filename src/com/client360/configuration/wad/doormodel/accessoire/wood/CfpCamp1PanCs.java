package com.client360.configuration.wad.doormodel.accessoire.wood;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.doormodel.wood.GestionLargeurCampagne;
import com.client360.configuration.wad.profiles.ProfileMoulurePanneauA;
import com.client360.configuration.wad.profiles.ProfilePorteBoisA;
import com.client360.configuration.wad.profiles.ProfilePorteBoisB;
import com.netappsid.commonutils.math.Units;

public class CfpCamp1PanCs extends GestionLargeurCampagne
{
	public String frameH2 = "frameH2";

	protected String petitEmbosseGauche = "petitEmbosseGauche";
	protected String petitEmbosseDroit = "petitEmbosseDroit";
	protected String petitEmbosseHaut = "petitEmbosseHaut";
	protected String petitEmbosseBas = "petitEmbosseBas";
	protected String petitEmbosseH2 = "petitEmbosseH2";

	@Override
	protected void load()
	{
		double fleche = 120;
		addLineDownOffset(frameH2, top, fleche);

		addLineRightOffset(petitEmbosseGauche, left, larMntCfpCamp);
		addLineDownOffset(petitEmbosseHaut, top, larTraCfpCamp);
		addLineLeftOffset(petitEmbosseDroit, right, larMntCfpCamp);
		addLineUpOffset(petitEmbosseBas, sill, larTraCfpCamp);

		double fleche2 = hDist(petitEmbosseHaut, petitEmbosseBas) / hDist(top, sill) * fleche;
		addLineDownOffset(petitEmbosseH2, petitEmbosseHaut, fleche2);

		// add frame
		addForm(elongatedArc(left, right, top, sill, frameH2));
		addSpacer(larTraCfpCamp, larMntCfpCamp, larTraCfpCamp, larMntCfpCamp, larMntCfpCamp);
		addProfile(new ProfileMoulurePanneauA(), new ProfileMoulurePanneauA());

		// add petit embosse
		addForm(elongatedArc(petitEmbosseGauche, petitEmbosseDroit, petitEmbosseHaut, petitEmbosseBas, petitEmbosseH2));
		addSpacer(larPlateBandeVisible, larPlateBandeVisible, larPlateBandeVisible, larPlateBandeVisible, larPlateBandeVisible);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}
}