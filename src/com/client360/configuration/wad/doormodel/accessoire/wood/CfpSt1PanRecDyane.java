package com.client360.configuration.wad.doormodel.accessoire.wood;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.doormodel.wood.GestionLargeurStyle;
import com.client360.configuration.wad.profiles.ProfileMoulure3DA;
import com.client360.configuration.wad.profiles.ProfileMoulure3DB;
import com.netappsid.commonutils.math.Units;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;

public class CfpSt1PanRecDyane extends GestionLargeurStyle
{
	protected String petitEmbosseGauche = "petitEmbosseGauche";
	protected String petitEmbosseDroit = "petitEmbosseDroit";
	protected String petitEmbosseHaut = "petitEmbosseHaut";
	protected String petitEmbosseBas = "petitEmbosseBas";

	@Override
	protected void load()
	{
		addLineRightOffset(petitEmbosseGauche, left, larMntCfpSt + 35);
		addLineDownOffset(petitEmbosseHaut, top, larTraCfpSt + 35);
		addLineLeftOffset(petitEmbosseDroit, right, larMntCfpSt + 35);
		addLineUpOffset(petitEmbosseBas, sill, larTraCfpSt + 35);

		addForm(rectangle(left, right, top, sill));
		addProfile(new ProfileMoulure3DA(), new ProfileMoulure3DB());
		addSpacer(larMntCfpSt, larTraCfpSt, larMntCfpSt, larTraCfpSt);

		addForm(rectangle(petitEmbosseGauche, petitEmbosseDroit, petitEmbosseHaut, petitEmbosseBas), "/images/grille_svg/DyaneBas.svg", ImageScale.FULL);
		addEmptyProfile();
		addSpacer(1, 1, 1, 1);
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}
}
