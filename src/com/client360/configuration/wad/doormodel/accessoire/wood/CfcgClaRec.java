package com.client360.configuration.wad.doormodel.accessoire.wood;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.doormodel.accessoire.PetitBois;
import com.client360.configuration.wad.doormodel.wood.GestionLargeurClassique;
import com.client360.configuration.wad.profiles.ProfileClassiqueA;
import com.client360.configuration.wad.profiles.ProfileClassiqueB;
import com.netappsid.commonutils.math.Units;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class CfcgClaRec extends ParametricModel
{
	private final double largeurParClose = 7;

	@Override
	protected void load()
	{
		addLineDownOffset("vTop", top, GestionLargeurClassique.larMntCfcgCl - largeurParClose);
		addLineUpOffset("vBottom", sill, GestionLargeurClassique.larMntCfcgCl - largeurParClose);
		addLineRightOffset("vLeft", left, GestionLargeurClassique.larMntCfcgCl - largeurParClose);
		addLineLeftOffset("vRight", right, GestionLargeurClassique.larMntCfcgCl - largeurParClose);

		// add vitrage
		String imgPath = (String) getParametricObject("imageGrille");
		addForm(rectangle("vLeft", "vRight", "vTop", "vBottom"), imgPath, ImageScale.FULL);
		setInsulatedGlass(true);
		addEmptyProfile();
		addEmptySpacer();

		// add frame
		addForm(rectangle(left, right, top, sill));
		setInsulatedGlass(false);
		addSpacer(GestionLargeurClassique.larMntCfcgCl, GestionLargeurClassique.larTraCfcgCl, GestionLargeurClassique.larMntCfcgCl,
				GestionLargeurClassique.larTraCfcgCl);
		addProfile(new ProfileClassiqueA(), new ProfileClassiqueB());

		// add petit bois
		addElementSpace("pb", "vLeft", "vRight", "vTop", "vBottom");
		addSubModel(new PetitBois(), "pb");
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

}