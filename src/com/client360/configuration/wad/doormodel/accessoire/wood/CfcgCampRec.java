package com.client360.configuration.wad.doormodel.accessoire.wood;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.doormodel.accessoire.PetitBois;
import com.client360.configuration.wad.doormodel.wood.GestionLargeurCampagne;
import com.client360.configuration.wad.profiles.ProfilePorteBoisA;
import com.client360.configuration.wad.profiles.ProfilePorteBoisB;
import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class CfcgCampRec extends ParametricModel
{
	@Override
	protected void load()
	{
		// add vitrage
		addForm(rectangle(left, right, top, sill));
		setInsulatedGlass(true);
		addEmptySpacer();
		addEmptyProfile();

		// add frame
		addForm(rectangle(left, right, top, sill));
		setInsulatedGlass(false);
		addSpacer(GestionLargeurCampagne.larMntCfcgCamp, GestionLargeurCampagne.larTraCfcgCamp, GestionLargeurCampagne.larMntCfcgCamp,
				GestionLargeurCampagne.larTraCfcgCamp);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());

		// add petit bois
		addLineDownOffset("pbTop", top, GestionLargeurCampagne.larMntCfcgCamp);
		addLineUpOffset("pbBottom", sill, GestionLargeurCampagne.larMntCfcgCamp);
		addLineRightOffset("pbLeft", left, GestionLargeurCampagne.larMntCfcgCamp);
		addLineLeftOffset("pbRight", right, GestionLargeurCampagne.larMntCfcgCamp);

		addElementSpace("pb", "pbLeft", "pbRight", "pbTop", "pbBottom");
		addSubModel(new PetitBois(), "pb");
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}
}