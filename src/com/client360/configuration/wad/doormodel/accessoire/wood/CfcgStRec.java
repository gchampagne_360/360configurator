package com.client360.configuration.wad.doormodel.accessoire.wood;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class CfcgStRec extends ParametricModel
{
	@Override
	protected void load()
	{
		addForm(rectangle(left, right, top, sill));
		setInsulatedGlass(true);
		// addSpacer(GestionLargeurStyle.larMntCfcgSt, GestionLargeurStyle.larTraCfcgSt, GestionLargeurStyle.larMntCfcgSt, GestionLargeurStyle.larTraCfcgSt);
		// addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}
}