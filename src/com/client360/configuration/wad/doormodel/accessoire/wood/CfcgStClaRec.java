package com.client360.configuration.wad.doormodel.accessoire.wood;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.doormodel.wood.GestionLargeurClassique;
import com.client360.configuration.wad.profiles.ProfileMoulure3DA;
import com.client360.configuration.wad.profiles.ProfileMoulure3DB;
import com.netappsid.commonutils.math.Units;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class CfcgStClaRec extends ParametricModel
{
	@Override
	protected void load()
	{
		addLineDownOffset("vTop", top, GestionLargeurClassique.larMntCfcgCl);
		addLineUpOffset("vBottom", sill, GestionLargeurClassique.larMntCfcgCl);
		addLineRightOffset("vLeft", left, GestionLargeurClassique.larMntCfcgCl);
		addLineLeftOffset("vRight", right, GestionLargeurClassique.larMntCfcgCl);

		// add vitrage
		String imgPath = (String) getParametricObject("imageGrille");
		addForm(rectangle("vLeft", "vRight", "vTop", "vBottom"), imgPath, ImageScale.FULL);
		setInsulatedGlass(true);
		addEmptyProfile();
		addEmptySpacer();

		// add frame
		addForm(rectangle(left, right, top, sill));
		setInsulatedGlass(false);
		addSpacer(GestionLargeurClassique.larMntCfcgCl, GestionLargeurClassique.larTraCfcgCl, GestionLargeurClassique.larMntCfcgCl,
				GestionLargeurClassique.larTraCfcgCl);
		addProfile(new ProfileMoulure3DA(), new ProfileMoulure3DB());
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

}