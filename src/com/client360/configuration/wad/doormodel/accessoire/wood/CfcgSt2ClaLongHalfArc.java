package com.client360.configuration.wad.doormodel.accessoire.wood;

import com.client360.configuration.wad.doormodel.accessoire.PetitBois;
import com.client360.configuration.wad.doormodel.wood.GestionLargeurStyle;
import com.client360.configuration.wad.profiles.ProfileMoulure3DA;
import com.client360.configuration.wad.profiles.ProfileMoulure3DB;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;

public class CfcgSt2ClaLongHalfArc extends GestionLargeurStyle
{
	protected String posMnoGau = "posMnoGau";
	protected String posMnoDro = "posMnoDro";
	protected String posMnoHaut = "posMnoHaut";
	protected String posMnoBas = "posMnoBas";
	protected String h2LineCadre = "h2LineCadre";
	// protected String h2LineIG = "h2LineIG";

	protected double larMno = larMnoCfpSt;
	protected double spacerCadreRectangle = 13.5;
	protected double spacerCadreDemiArcAllonge = 25;
	protected double spacerMno = 15;
	protected double espaceFenetre = 17;

	protected String petitEmbosseGau = "petitEmbosseGau";
	protected String petitEmbosseDro = "petitEmbosseDro";
	protected String petitEmbosseHau = "petitEmbosseHau";
	protected String petitEmbosseBas = "petitEmbosseBas";

	protected String vGau = "vGau";
	protected String vDro = "vDro";
	protected String vHau = "vHau";
	protected String vBas = "vBas";

	@Override
	protected void load()
	{
		addLineLeftOffset(posMnoGau, vCenter, larMno / 2);
		addLineRightOffset(posMnoDro, vCenter, larMno / 2);

		addLineRightOffset(petitEmbosseGau, left, spacerCadreRectangle + espaceFenetre);
		addLineDownOffset(petitEmbosseHau, top, spacerCadreRectangle + espaceFenetre);
		addLineLeftOffset(petitEmbosseDro, right, spacerCadreRectangle + espaceFenetre);
		addLineUpOffset(petitEmbosseBas, sill, spacerCadreRectangle + espaceFenetre);

		addLineRightOffset(vGau, petitEmbosseGau, spacerCadreDemiArcAllonge);
		addLineDownOffset(vHau, petitEmbosseHau, spacerCadreDemiArcAllonge);
		addLineLeftOffset(vDro, petitEmbosseDro, spacerCadreDemiArcAllonge);
		addLineUpOffset(vBas, petitEmbosseBas, spacerCadreDemiArcAllonge);

		// vitrage gauche
		String imgPath = (String) getParametricObject("imageGrille");
		addForm(elongatedHalfArc(vGau, posMnoGau, vHau, vBas, null, null), imgPath + "Left.svg", ImageScale.FULL);
		setInsulatedGlass(true);
		addEmptyProfile();
		addEmptySpacer();

		// vitrage droit
		addForm(elongatedHalfArc(posMnoDro, vDro, vHau, vBas, null, null), imgPath + "Right.svg", ImageAlign.NONE, ImageScale.FULL, true, false, false, false);
		setInsulatedGlass(true);
		addEmptyProfile();
		addEmptySpacer();

		// cadre vitrage
		addLineDownOffset(h2LineCadre, petitEmbosseHau, vDist(petitEmbosseGau, petitEmbosseDro) / 2);
		addForm(elongatedArc(petitEmbosseGau, petitEmbosseDro, petitEmbosseHau, petitEmbosseBas, h2LineCadre));
		setInsulatedGlass(false);
		addProfile(new ProfileMoulure3DA(), new ProfileMoulure3DB());
		addSpacer(spacerCadreDemiArcAllonge, spacerCadreDemiArcAllonge, spacerCadreDemiArcAllonge, spacerCadreDemiArcAllonge);

		// cadre rectangle
		addForm(rectangle(left, right, top, sill));
		addSpacer(spacerCadreRectangle, spacerCadreRectangle, spacerCadreRectangle, spacerCadreRectangle);
		addProfile(new ProfileMoulure3DA(), new ProfileMoulure3DB());
		setInsulatedGlass(false);

		// petit bois
		addParametricObject("imageGrille", null);
		addElementSpace("pb", vGau, vDro, vHau, vBas);
		addSubModel(new PetitBois(), "pb");
		addParametricObject("imageGrille", imgPath);
	}
}
