package com.client360.configuration.wad.doormodel.accessoire;

import com.client360.configuration.wad.profiles.ProfilePorteBoisA;
import com.client360.configuration.wad.profiles.ProfilePorteBoisB;

public class OculusTriangleHautDroit extends AccessoireGenerique
{
	@Override
	protected void load()
	{
		// add thermos
		addForm(triangleRectangle(left, right, top, sill), false, true, false, false);
		setInsulatedGlass(true);
		addEmptyProfile();
		addEmptySpacer();

		// add frame
		addForm(triangleRectangle(left, right, top, sill), false, true, false, false);
		setInsulatedGlass(false);
		addSpacer(45d, 45d, 45d);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
	}
}
