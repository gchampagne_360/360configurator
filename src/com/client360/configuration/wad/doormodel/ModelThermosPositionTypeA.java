package com.client360.configuration.wad.doormodel;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.cad.profiles.ProfileParecloseA;
import com.client360.configuration.wad.cad.profiles.ProfileParecloseB;
import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class ModelThermosPositionTypeA extends ParametricModel
{

	public ModelThermosPositionTypeA()
	{
		super();
	}

	@Override
	protected void load()
	{
		addGaps(130, 200, 100);

		// Construction lines definitions
		addLineLeftOffset(1, vPatternCenter, 100);
		addLineRightOffset(3, vPatternCenter, 100);

		addLineDownOffset(3, patternTop, (hDist(patternTop, patternSill) - 300) * 0.75);

		// Forms creation
		addFormRectangle(patternLeft, 1, patternTop, 3);
		addFormRectangle(3, patternRight, patternTop, 3);

		// Form Spacer definition
		addSpacer(12, 12, 12, 12);
		addSpacer(12, 12, 12, 12);

		// Form Profile definition
		addProfile(new ProfileParecloseA(), new ProfileParecloseB());
		addProfile(new ProfileParecloseA(), new ProfileParecloseB());

	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

}
