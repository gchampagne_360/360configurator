package com.client360.configuration.wad.doormodel;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.netappsid.cad.profile.SteelDoorProfileA;
import com.netappsid.cad.profile.SteelDoorProfileB;
import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class SubModel3 extends ParametricModel
{

	public SubModel3()
	{
		super();
		update(Units.inch(20), Units.inch(10));
	}

	@Override
	protected Unit<Length> getUnit()
	{

		return Units.INCH;
	}

	@Override
	protected void load()
	{

		addFormRectangle(left, right, top, sill);

		addSpacer(2, 2, 2, 2);
		addProfile(new SteelDoorProfileA(), new SteelDoorProfileB());
		addProfile(new SteelDoorProfileA(), new SteelDoorProfileB());

	}

}
