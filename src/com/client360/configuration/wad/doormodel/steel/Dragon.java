package com.client360.configuration.wad.doormodel.steel;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;


public class Dragon extends AcierContemporaine
{
	@Override
	protected void load()
	{
		super.load();
		addAccessoires();
		addForm(rectangle(left, right, top, sill), "/images/door_svg/steel/contemporaines/Dragon_motif.svg", ImageAlign.CENTER, ImageScale.FULL);
	}

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();
		double milieuVerticalPorte = hDist(top, sill) / 2;

		addLineUpOffset(thermosHaut, sill, milieuVerticalPorte + 400 + 461);
		addLineUpOffset(thermosBas, sill, milieuVerticalPorte + 400);
		addLineLeftOffset(thermosGauche, right, 115 + 271);
		addLineLeftOffset(thermosDroit, right, 115);

		addElementSpace("1", thermosGauche, thermosDroit, thermosHaut, thermosBas);
	}
}
