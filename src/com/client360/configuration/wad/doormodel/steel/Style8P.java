package com.client360.configuration.wad.doormodel.steel;

public class Style8P extends AcierStyle
{
	// accessoire haut
	public final String posAccHautHaut = "posAccHautHaut";
	public final String posAccHautBas = "posAccHautBas";
	public final String posAccHautGauche = "posAccHautGauche";
	public final String posAccHautDroite = "posAccHautDroite";

	protected double hauteurPanneauHaut = 1665;
	protected double largeurPanneauHaut = 589;

	protected double espacePorteHautMax = 192.5;

	@Override
	protected void load()
	{
		addAccessoires();
	}

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();
		espacePorteCoteHaut = calculEspacePorteCote();
		espacePorteHaut = calculEspacePorteHaut();

		addLineRightOffset(posAccHautGauche, left, espacePorteCoteHaut);
		addLineLeftOffset(posAccHautDroite, right, espacePorteCoteHaut);

		addLineDownOffset(posAccHautHaut, top, espacePorteHaut);
		addLineUpOffset(posAccHautBas, posAccHautHaut, hauteurPanneauHaut);

		addElementSpace("2", posAccHautGauche, posAccHautDroite, posAccHautHaut, posAccHautBas);

	}

	protected double calculEspacePorteCote()
	{
		return (vDist(left, right) - largeurPanneauHaut) / 2;
	}

	private double calculEspacePorteHaut()
	{
		return espacePorteHautMax - ((2150 - hDist(top, sill)) / 10);
	}
}
