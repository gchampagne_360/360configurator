package com.client360.configuration.wad.doormodel.steel;

import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;

public class Silex extends AcierContemporaine
{
	private final double largeurThermos = 424d;

	public String motifPositionRight = "motifPositionRight";

	@Override
	protected void load()
	{
		super.load();
		addLineLeftOffset(motifPositionRight, right, 52);

		addAccessoires();
		addForm(rectangle(left, motifPositionRight, top, sill), "/images/door_svg/steel/contemporaines/Silex_motif.svg", ImageAlign.TOPRIGHT, ImageScale.NONE);
	}

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();
		addLineDownOffset(thermosHaut, top, 283);
		addLineDownOffset(thermosBas, top, 283 + largeurThermos);
		addLineLeftOffset(thermosGauche, right, 50 + largeurThermos);
		addLineLeftOffset(thermosDroit, right, 50);

		addElementSpace("1", thermosGauche, thermosDroit, thermosHaut, thermosBas);
	}
}
