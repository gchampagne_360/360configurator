package com.client360.configuration.wad.doormodel.steel;

public class AcierServiceRond extends AcierService
{
	protected double largeurCercle = 310;

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();

		addLineDownOffset(accHaut, top, 245);
		addLineDownOffset(accBas, accHaut, largeurCercle);
		addLineLeftOffset(accGauche, vCenter, largeurCercle / 2);
		addLineRightOffset(accDroit, vCenter, largeurCercle / 2);

		addElementSpace("2", accGauche, accDroit, accHaut, accBas);
	}
}
