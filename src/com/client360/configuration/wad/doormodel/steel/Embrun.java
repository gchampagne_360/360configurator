package com.client360.configuration.wad.doormodel.steel;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;


public class Embrun extends AcierContemporaine
{

	@Override
	protected void load()
	{
		super.load();
		addAccessoires();
		addForm(rectangle(left, right, top, sill), "/images/door_svg/steel/contemporaines/Embrun_motif.svg", ImageAlign.BOTTOMCENTER, ImageScale.NONE);
	}

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();
		addLineDownOffset(thermosHaut, top, 370);
		addLineDownOffset(thermosBas, top, 370 + 290);
		addLineLeftOffset(thermosDroit, right, 114);
		addLineLeftOffset(thermosGauche, right, 114 + 290);

		addElementSpace("1", thermosGauche, thermosDroit, thermosHaut, thermosBas);
	}
}
