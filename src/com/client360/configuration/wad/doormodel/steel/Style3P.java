package com.client360.configuration.wad.doormodel.steel;

public class Style3P extends AcierStyle
{
	// accessoire haut
	public final String posAccHautHaut = "posAccHautHaut";
	public final String posAccHautBas = "posAccHautBas";
	public final String posAccHautGauche = "posAccHautGauche";
	public final String posAccHautDroite = "posAccHautDroite";

	// accessoire bas
	public final String posAccBasHaut = "posAccBasHaut";
	public final String posAccBasBas = "posAccBasBas";
	public final String posAccBasGauche = "posAccBasGauche";
	public final String posAccBasDroite = "posAccBasDroite";

	protected final double hauteurPanneauBas = 548;
	protected final double hauteurPanneauHaut = 970;
	protected final double largeurPanneauBas = 235;
	protected final double largeurPanneauHaut = 615;

	protected final double espacePorteHautMax = 203;

	@Override
	protected void load()
	{
		addAccessoires();
		espaceEntreAccH1 = 120;
	}

	public void _1()
	{
		espaceEntreAccV1 = 149.5;
	}

	public void _2()
	{
		espaceEntreAccV1 = 154.5;
	}

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();
		espacePorteCoteHaut = calculEspacePorteCoteAccHaut();
		espacePorteCoteBas = calculEspacePorteCoteAccBas();
		espacePorteHaut = calculEspacePorteHaut();
		espacePorteBas = calculEspacePorteBas();

		addLineRightOffset(posAccBasGauche, left, espacePorteCoteBas);
		addLineLeftOffset(posAccBasDroite, right, espacePorteCoteBas);

		addLineUpOffset(posAccBasBas, sill, espacePorteBas);
		addLineUpOffset(posAccBasHaut, posAccBasBas, hauteurPanneauBas);

		addLineRightOffset(posAccHautGauche, left, espacePorteCoteHaut);
		addLineLeftOffset(posAccHautDroite, right, espacePorteCoteHaut);

		addLineDownOffset(posAccHautHaut, top, espacePorteHaut);
		addLineUpOffset(posAccHautBas, posAccHautHaut, hauteurPanneauHaut);

		addElementSpace("2", posAccBasGauche, posAccBasDroite, posAccBasHaut, posAccBasBas);
		addElementSpace("3", posAccHautGauche, posAccHautDroite, posAccHautHaut, posAccHautBas);

	}

	private double calculEspacePorteCoteAccHaut()
	{
		return (vDist(left, right) - largeurPanneauHaut) / 2;
	}

	private double calculEspacePorteCoteAccBas()
	{
		return (vDist(left, right) - ((2 * largeurPanneauBas) + espaceEntreAccH1)) / 2;
	}

	private double calculEspacePorteHaut()
	{
		return espacePorteHautMax - (((2150 - hDist(top, sill)) / 10) * 5);
	}

	private double calculEspacePorteBas()
	{
		return hDist(top, sill) - calculEspacePorteHaut() - hauteurPanneauHaut - hauteurPanneauBas - espaceEntreAccV1;
	}
}
