package com.client360.configuration.wad.doormodel.steel;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;

public class Imperial extends AcierContemporaine
{
	@Override
	protected void load()
	{
		super.load();
		addAccessoires();
		addForm(rectangle(left, right, top, sill), "/images/door_svg/steel/contemporaines/Imperial_motif.svg", ImageAlign.NONE, ImageScale.FULL);
	}

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();
		addLineDownOffset(thermosHaut, top, 388.5);
		addLineDownOffset(thermosBas, top, 388.5 + 300);
		addLineRightOffset(thermosGauche, left, 140);
		addLineRightOffset(thermosDroit, left, 140 + 300);

		addElementSpace("1", thermosGauche, thermosDroit, thermosHaut, thermosBas);
	}
}
