package com.client360.configuration.wad.doormodel.steel;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class AcierService extends ParametricModel
{

	protected final double hauteurJetDeau = 17.63;
	protected final double espaceCoteJetDeau = 4;

	public final String posJetDeauHaut = "posJetDeauHaut";
	public final String posJetDeauGauche = "posJetDeauGauche";
	public final String posJetDeauDroit = "posJetDeauDroit";

	public final String accHaut = "accHaut";
	public final String accBas = "accBas";
	public final String accGauche = "accGauche";
	public final String accDroit = "accDroit";

	@Override
	protected void load()
	{
		addAccessoires();
	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxWidth(1000);
		addMaxHeight(2250);
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

	protected void addAccessoires()
	{
		addLineUpOffset(posJetDeauHaut, sill, hauteurJetDeau);
		addLineRightOffset(posJetDeauGauche, left, espaceCoteJetDeau);
		addLineLeftOffset(posJetDeauDroit, right, espaceCoteJetDeau);
		addElementSpace("1", posJetDeauGauche, posJetDeauDroit, posJetDeauHaut, sill);
	}

}
