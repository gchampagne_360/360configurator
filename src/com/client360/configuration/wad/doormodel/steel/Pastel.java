package com.client360.configuration.wad.doormodel.steel;

import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;

public class Pastel extends AcierContemporaine
{
	private final double coteThermos = 275.2d;

	@Override
	protected void load()
	{

		super.load();

		addEmptyColor();

		addGaps(50.0, 35.0, 75.0);

		addForm(rectangle(patternLeft, patternRight, patternTop, patternSill), "/images/door_svg/Pastel.svg", ImageAlign.CENTER, ImageScale.NONE);

		addSpacer(5.0, 5.0, 5.0, 5.0);

	}

}
