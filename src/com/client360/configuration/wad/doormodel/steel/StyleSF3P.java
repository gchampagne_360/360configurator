package com.client360.configuration.wad.doormodel.steel;

public class StyleSF3P extends Style6P
{
	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxWidth(400);
		addMaxHeight(2150);
	}

	@Override
	protected void calculEspacePorteCote()
	{
		espacePorteCoteHaut = (vDist(left, right) - largeurPanneauHaut) / 2;
	}
}
