package com.client360.configuration.wad.doormodel.steel;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class AcierStyle extends ParametricModel
{
	protected final double hauteurJetDeau = 17.63;
	protected final double espaceCoteJetDeau = 4;

	public String posJetDeauHaut = "posJetDeauHaut";
	public String posJetDeauGauche = "posJetDeauGauche";
	public String posJetDeauDroit = "posJetDeauDroit";

	protected double espacePorteHaut;
	protected double espacePorteBas;
	protected double espacePorteCoteHaut;
	protected double espacePorteCoteBas;

	protected double espaceEntreAccH1;
	protected double espaceEntreAccH2;
	protected double espaceEntreAccH3;

	protected double espaceEntreAccV1;
	protected double espaceEntreAccV2;
	protected double espaceEntreAccV3;

	@Override
	protected void load()
	{

	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxWidth(900);
		addMaxWidth(800);
		addMaxHeight(2150);
		addMaxHeight(2000);
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

	protected void addAccessoires()
	{
		addLineUpOffset(posJetDeauHaut, sill, hauteurJetDeau);
		addLineRightOffset(posJetDeauGauche, left, espaceCoteJetDeau);
		addLineLeftOffset(posJetDeauDroit, right, espaceCoteJetDeau);
		addElementSpace("1", posJetDeauGauche, posJetDeauDroit, posJetDeauHaut, sill);
	}

}
