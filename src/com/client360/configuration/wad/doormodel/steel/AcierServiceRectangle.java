package com.client360.configuration.wad.doormodel.steel;

public class AcierServiceRectangle extends AcierService
{
	protected double largeurRec = 511;
	protected double hauteurRec = 321.63;

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();

		addLineDownOffset(accHaut, top, 239.5);
		addLineDownOffset(accBas, accHaut, hauteurRec);
		addLineLeftOffset(accGauche, vCenter, largeurRec / 2);
		addLineRightOffset(accDroit, vCenter, largeurRec / 2);

		addElementSpace("2", accGauche, accDroit, accHaut, accBas);
	}
}
