package com.client360.configuration.wad.doormodel.steel;

import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;

public class Palladium extends AcierContemporaine
{
	private final double coteThermos = 300d;

	@Override
	protected void load()
	{
		super.load();
	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		// Small
		addMaxHeight(2100);

		// Big
		addMaxWidth(1000);
		addMaxHeight(2250);
	}

	// Small
	public void _1()
	{
		addAccessoires();
		addForm(rectangle(left, right, top, sill), "/images/door_svg/steel/contemporaines/Palladium_motif_small.svg", ImageAlign.BOTTOMCENTER, ImageScale.NONE);
	}

	// Big
	public void _2()
	{
		addAccessoires();
		addForm(rectangle(left, right, top, sill), "/images/door_svg/steel/contemporaines/Palladium_motif_big.svg", ImageAlign.BOTTOMCENTER, ImageScale.NONE);
	}

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();
		double distanceHaut = hDist(top, sill) / 12;
		double distanceHaut2 = distanceHaut + coteThermos;
		double distanceCote = (vDist(left, right) - coteThermos) / 2;
		double hauteurBasThermos;

		if (hDist(top, sill) >= 2100)
		{
			hauteurBasThermos = 1591;
		}
		else
		{
			hauteurBasThermos = 1471;
		}

		addLineUpOffset(thermosHaut, sill, hauteurBasThermos + coteThermos);
		addLineUpOffset(thermosBas, sill, hauteurBasThermos);
		addLineRightOffset(thermosGauche, left, distanceCote);
		addLineLeftOffset(thermosDroit, right, distanceCote);

		addElementSpace("1", thermosGauche, thermosDroit, thermosHaut, thermosBas);
	}

}
