package com.client360.configuration.wad.doormodel.steel;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;


public class Kubic extends AcierContemporaine
{
	public String distMotifTop = "distMotifTop";

	@Override
	protected void load()
	{
		super.load();
		addLineDownOffset(distMotifTop, top, 319);

		addAccessoires();
		addForm(rectangle(left, right, distMotifTop, sill), "/images/door_svg/steel/contemporaines/Kubic_motif.svg", ImageAlign.TOPCENTER, ImageScale.NONE);
	}

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();
		addLineDownOffset(thermosHaut, top, 185);
		addLineDownOffset(thermosBas, top, 185 + 300);

		double distanceCoteDroit = 115 - (((1000 - vDist(left, right)) / 100) * 20);
		addLineLeftOffset(thermosDroit, right, distanceCoteDroit);
		addLineLeftOffset(thermosGauche, right, distanceCoteDroit + 300);

		addElementSpace("1", thermosGauche, thermosDroit, thermosHaut, thermosBas);
	}
}
