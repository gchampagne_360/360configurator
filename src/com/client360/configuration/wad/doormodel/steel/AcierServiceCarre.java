package com.client360.configuration.wad.doormodel.steel;

public class AcierServiceCarre extends AcierService
{
	protected double largeurCarre = 300;

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();

		addLineDownOffset(accHaut, top, 250);
		addLineDownOffset(accBas, accHaut, largeurCarre);
		addLineLeftOffset(accGauche, vCenter, largeurCarre / 2);
		addLineRightOffset(accDroit, vCenter, largeurCarre / 2);

		addElementSpace("2", accGauche, accDroit, accHaut, accBas);
	}
}
