package com.client360.configuration.wad.doormodel.steel;

import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;


public class Active extends AcierContemporaine
{
	@Override
	protected void load()
	{
		super.load();
		addAccessoires();
		addForm(rectangle(left, right, top, sill), "/images/door_svg/steel/contemporaines/Active_motif.svg", ImageAlign.CENTER);
	}

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();
		double thermosRayon = 157.5;

		double distanceCote = (vDist(left, right) - (2 * thermosRayon)) / 2;
		double distanceFromTopToThermosCenter = 616 - (((2250 - hDist(top, sill)) / 10) * 5);

		addLineDownOffset(thermosHaut, top, distanceFromTopToThermosCenter - thermosRayon);
		addLineDownOffset(thermosBas, top, distanceFromTopToThermosCenter + thermosRayon);
		addLineRightOffset(thermosGauche, left, distanceCote);
		addLineLeftOffset(thermosDroit, right, distanceCote);

		addElementSpace("1", thermosGauche, thermosDroit, thermosHaut, thermosBas);
	}
}
