package com.client360.configuration.wad.doormodel.steel;

public class AcierServiceLosange extends AcierService
{
	protected double largeurLosange = 424.26;

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();

		addLineDownOffset(accHaut, top, 187.87);
		addLineDownOffset(accBas, accHaut, largeurLosange);
		addLineLeftOffset(accGauche, vCenter, largeurLosange / 2);
		addLineRightOffset(accDroit, vCenter, largeurLosange / 2);

		addElementSpace("2", accGauche, accDroit, accHaut, accBas);
	}
}
