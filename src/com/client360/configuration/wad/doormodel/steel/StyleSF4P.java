package com.client360.configuration.wad.doormodel.steel;

public class StyleSF4P extends Style8P
{
	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxWidth(400);
		addMaxHeight(2150);
	}

	@Override
	protected void load()
	{
		largeurPanneauHaut = 242;
		addAccessoires();
	}

}
