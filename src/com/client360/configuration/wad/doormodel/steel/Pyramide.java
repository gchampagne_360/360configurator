package com.client360.configuration.wad.doormodel.steel;

import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;

public class Pyramide extends AcierContemporaine
{
	@Override
	protected void load()
	{
		super.load();
		addAccessoires();
		addForm(rectangle(left, right, top, sill), "/images/door_svg/steel/contemporaines/Pyramide_motif.svg", ImageAlign.CENTER);
	}
}
