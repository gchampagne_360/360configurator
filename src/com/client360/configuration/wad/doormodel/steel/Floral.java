package com.client360.configuration.wad.doormodel.steel;

import java.awt.Color;

import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;

public class Floral extends AcierContemporaine
{
	public String distMotifTop = "distMotifTop";
	public String distMotifDroite = "distMotifDroite";

	@Override
	protected void load()
	{
		super.load();
		addLineDownOffset(distMotifTop, top, 100);
		addLineLeftOffset(distMotifDroite, right, 100);

		addAccessoires();
		addForm(rectangle(left, distMotifDroite, distMotifTop, sill), "/images/door_svg/steel/contemporaines/Floral_motif.svg", ImageAlign.TOPRIGHT,
				ImageScale.NONE);
		addColor((Color) getParametricObject("Couleur motif"));
	}

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();
		// addLineDownOffset(thermosHaut, top, 370);
		// addLineDownOffset(thermosBas, top, 370 + 290);
		// addLineRightOffset(thermosGauche, left, 114);
		// addLineRightOffset(thermosDroit, left, 114 + 290);
		//
		// addElementSpace("1", thermosGauche, thermosDroit, thermosHaut, thermosBas);
	}
}
