package com.client360.configuration.wad.doormodel.steel;

import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;

public class Kaki extends AcierContemporaine
{
	private final double coteThermos = 275.2d;

	@Override
	protected void load()
	{
		addEmptyColor();
		addLineDownOffset("Motif", top, 200);
		addLineDownOffset("Metal", top, 200);

		addLineRightOffset("MotifR", vCenter, 490 / 2);
		addLineLeftOffset("MotifL", vCenter, 490 / 2);
		addLineRightOffset("MetalR", vCenter, 490 / 2);
		addLineLeftOffset("MetalL", vCenter, 490 / 2);
		super.load();

	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		// Small
		addMaxHeight(2000);

		// Big
		addMaxHeight(3000);
	}

	// Small
	public void _1()
	{
		addAccessoires();
		addForm(rectangle("MotifL", "MotifR", "Motif", sill), "/images/door_svg/Kakismall.svg", ImageAlign.TOPLEFT, ImageScale.NONE);
	}

	// Big
	public void _2()
	{
		addAccessoires();
		addForm(rectangle("MotifL", "MotifR", "Motif", sill), "/images/door_svg/Kaki.svg", ImageAlign.TOPLEFT, ImageScale.NONE);
	}

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();
	}

}
