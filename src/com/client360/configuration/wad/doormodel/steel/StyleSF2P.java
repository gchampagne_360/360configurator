package com.client360.configuration.wad.doormodel.steel;

public class StyleSF2P extends Style4P
{
	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxWidth(400);
		addMaxHeight(2150);
	}

	@Override
	protected void calculEspacePorteCoteHaut()
	{
		espacePorteCoteHaut = (vDist(left, right) - largeurPanneauHaut) / 2;
	}

	@Override
	protected void calculEspacePorteCoteBas()
	{
		espacePorteCoteBas = (vDist(left, right) - largeurPanneauBas) / 2;
	}
}
