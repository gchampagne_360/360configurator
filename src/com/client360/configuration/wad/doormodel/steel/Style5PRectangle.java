package com.client360.configuration.wad.doormodel.steel;

public class Style5PRectangle extends AcierStyle
{
	// accessoire haut
	public final String posAccHautHaut = "posAccHautHaut";
	public final String posAccHautBas = "posAccHautBas";
	public final String posAccHautGauche = "posAccHautGauche";
	public final String posAccHautDroite = "posAccHautDroite";

	// accessoire bas
	public final String posAccMilieuHaut = "posAccMilieuHaut";
	public final String posAccMilieuBas = "posAccMilieuBas";
	public final String posAccMilieuGauche = "posAccMilieuGauche";
	public final String posAccMilieuDroite = "posAccMilieuDroite";

	// accessoire bas
	public final String posAccBasHaut = "posAccBasHaut";
	public final String posAccBasBas = "posAccBasBas";
	public final String posAccBasGauche = "posAccBasGauche";
	public final String posAccBasDroite = "posAccBasDroite";

	protected final double hauteurPanneauBas = 548;
	protected final double hauteurPanneauMilieu = 650;
	protected final double hauteurPanneauHaut = 304;
	protected final double largeurPanneauBas = 235;
	protected final double largeurPanneauMilieu = 235;
	protected final double largeurPanneauHaut = 607;

	protected final double espacePorteHautMax = 101;
	protected final double espaceEntreAccV2Max = 110.5;

	@Override
	protected void load()
	{
		addAccessoires();
	}

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();

		espaceEntreAccH1 = 120;
		espaceEntreAccH2 = 120;

		espaceEntreAccV1 = 162;
		espaceEntreAccV2 = calculEspaceAccV2();

		double espacePorteCoteBasMilieu = calculEspacePorteCoteBasMilieu();
		double espacePorteCoteHaut = calculEspacePorteCoteHaut();

		espacePorteHaut = calculEspacePorteHaut();

		addLineRightOffset(posAccBasGauche, left, espacePorteCoteBasMilieu);
		addLineLeftOffset(posAccBasDroite, right, espacePorteCoteBasMilieu);

		addLineRightOffset(posAccHautGauche, left, espacePorteCoteHaut);
		addLineLeftOffset(posAccHautDroite, right, espacePorteCoteHaut);

		addLineRightOffset(posAccMilieuGauche, left, espacePorteCoteBasMilieu);
		addLineLeftOffset(posAccMilieuDroite, right, espacePorteCoteBasMilieu);

		addLineDownOffset(posAccHautHaut, top, espacePorteHaut);
		addLineDownOffset(posAccHautBas, posAccHautHaut, hauteurPanneauHaut);

		addLineDownOffset(posAccMilieuHaut, posAccHautBas, espaceEntreAccV2);
		addLineDownOffset(posAccMilieuBas, posAccMilieuHaut, hauteurPanneauMilieu);

		addLineDownOffset(posAccBasHaut, posAccMilieuBas, espaceEntreAccV1);
		addLineDownOffset(posAccBasBas, posAccBasHaut, hauteurPanneauBas);

		addElementSpace("2", posAccBasGauche, posAccBasDroite, posAccBasHaut, posAccBasBas);
		addElementSpace("3", posAccMilieuGauche, posAccMilieuDroite, posAccMilieuHaut, posAccMilieuBas);
		addElementSpace("4", posAccHautGauche, posAccHautDroite, posAccHautHaut, posAccHautBas);

	}

	private double calculEspacePorteCoteBasMilieu()
	{
		return (vDist(left, right) - ((2 * largeurPanneauMilieu) + espaceEntreAccH1)) / 2;
	}

	private double calculEspacePorteCoteHaut()
	{
		return (vDist(left, right) - largeurPanneauHaut) / 2;
	}

	private double calculEspacePorteHaut()
	{
		return espacePorteHautMax - ((2150 - hDist(top, sill)) * 3 / 25);
	}

	private double calculEspaceAccV2()
	{
		return espaceEntreAccV2Max - ((2150 - hDist(top, sill)) * 2 / 25);
	}
}
