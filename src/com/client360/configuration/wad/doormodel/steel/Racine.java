package com.client360.configuration.wad.doormodel.steel;

import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;

public class Racine extends AcierContemporaine
{
	public String distMotifDroite = "distMotifDroite";

	@Override
	protected void load()
	{
		super.load();
		addLineLeftOffset(distMotifDroite, right, 148.17);

		addAccessoires();
		addForm(rectangle(left, distMotifDroite, top, sill), "/images/door_svg/steel/contemporaines/Racine_motif.svg", ImageAlign.TOPRIGHT, ImageScale.NONE);
	}

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();
		addLineDownOffset(thermosHaut, top, 388.5);
		addLineDownOffset(thermosBas, top, 388.5 + 300);
		addLineLeftOffset(thermosDroit, right, 134);
		addLineLeftOffset(thermosGauche, right, 134 + 300);

		addElementSpace("1", thermosGauche, thermosDroit, thermosHaut, thermosBas);
	}
}
