package com.client360.configuration.wad.doormodel.steel;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;


public class Octal extends AcierContemporaine
{
	private final double largeurThermos = 424d;

	@Override
	protected void load()
	{
		super.load();
		addAccessoires();
		addForm(rectangle(left, right, top, sill), "/images/door_svg/steel/contemporaines/Octal_motif.svg", ImageAlign.TOPCENTER, ImageScale.NONE);
	}

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();
		addLineDownOffset(thermosHaut, top, 284);
		addLineDownOffset(thermosBas, top, 284 + largeurThermos);

		double distanceCote = (vDist(left, right) - largeurThermos) / 2;
		addLineLeftOffset(thermosDroit, right, distanceCote);
		addLineRightOffset(thermosGauche, left, distanceCote);

		addElementSpace("1", thermosGauche, thermosDroit, thermosHaut, thermosBas);
	}
}
