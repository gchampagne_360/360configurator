package com.client360.configuration.wad.doormodel.steel;

import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;

public class Vitamine extends AcierContemporaine
{
	private final double coteThermos = 275.2d;

	@Override
	protected void load()
	{

		super.load();
	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		// Small
		addMaxHeight(1892);

		// Big
		addMaxWidth(1000);
		addMaxHeight(2242);
	}

	// Small
	public void _1()
	{
		addAccessoires();
		addForm(rectangle(left, right, top, sill), "/images/door_svg/steel/contemporaines/Vitamine_motif.svg", ImageAlign.BOTTOMRIGHT, ImageScale.NONE);
	}

	// Big
	public void _2()
	{
		addAccessoires();
		addForm(rectangle(left, right, top, sill), "/images/door_svg/steel/contemporaines/Vitamine_motif.svg", ImageAlign.BOTTOMRIGHT, ImageScale.NONE);
	}

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();
		addLineDownOffset(thermosHaut, top, 370);
		addLineDownOffset(thermosBas, top, 370 + 275);
		addLineRightOffset(thermosGauche, left, 121.4);
		addLineRightOffset(thermosDroit, left, 121.4 + 275);

		addElementSpace("1", thermosGauche, thermosDroit, thermosHaut, thermosBas);
	}

}
