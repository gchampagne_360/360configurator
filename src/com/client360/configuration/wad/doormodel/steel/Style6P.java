package com.client360.configuration.wad.doormodel.steel;

public class Style6P extends AcierStyle
{
	// accessoire haut
	public final String posAccHautHaut = "posAccHautHaut";
	public final String posAccHautBas = "posAccHautBas";
	public final String posAccHautGauche = "posAccHautGauche";
	public final String posAccHautDroite = "posAccHautDroite";

	// accessoire bas
	public final String posAccMilieuHaut = "posAccMilieuHaut";
	public final String posAccMilieuBas = "posAccMilieuBas";
	public final String posAccMilieuGauche = "posAccMilieuGauche";
	public final String posAccMilieuDroite = "posAccMilieuDroite";

	// accessoire bas
	public final String posAccBasHaut = "posAccBasHaut";
	public final String posAccBasBas = "posAccBasBas";
	public final String posAccBasGauche = "posAccBasGauche";
	public final String posAccBasDroite = "posAccBasDroite";

	protected double hauteurPanneauBas = 548;
	protected double hauteurPanneauMilieu = 650;
	protected double hauteurPanneauHaut = 185;
	protected double largeurPanneauBas = 235;
	protected double largeurPanneauMilieu = 235;
	protected double largeurPanneauHaut = 235;

	protected final double espacePorteHautMax = 220.5;

	@Override
	protected void load()
	{
		addAccessoires();

		espaceEntreAccH1 = 120;
		espaceEntreAccH2 = 120;
		espaceEntreAccH3 = 120;

		espaceEntreAccV1 = 162;
		espaceEntreAccV2 = 110;
	}

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();
		calculEspacePorteCote();
		espacePorteHaut = calculEspacePorteHaut();
		espacePorteBas = calculEspacePorteBas();

		addLineRightOffset(posAccBasGauche, left, espacePorteCoteHaut);
		addLineLeftOffset(posAccBasDroite, right, espacePorteCoteHaut);

		addLineRightOffset(posAccHautGauche, left, espacePorteCoteHaut);
		addLineLeftOffset(posAccHautDroite, right, espacePorteCoteHaut);

		addLineRightOffset(posAccMilieuGauche, left, espacePorteCoteHaut);
		addLineLeftOffset(posAccMilieuDroite, right, espacePorteCoteHaut);

		addLineDownOffset(posAccHautHaut, top, espacePorteHaut);
		addLineDownOffset(posAccHautBas, posAccHautHaut, hauteurPanneauHaut);

		addLineDownOffset(posAccMilieuHaut, posAccHautBas, espaceEntreAccV2);
		addLineDownOffset(posAccMilieuBas, posAccMilieuHaut, hauteurPanneauMilieu);

		addLineDownOffset(posAccBasHaut, posAccMilieuBas, espaceEntreAccV1);
		addLineDownOffset(posAccBasBas, posAccBasHaut, hauteurPanneauBas);

		addElementSpace("2", posAccBasGauche, posAccBasDroite, posAccBasHaut, posAccBasBas);
		addElementSpace("3", posAccMilieuGauche, posAccMilieuDroite, posAccMilieuHaut, posAccMilieuBas);
		addElementSpace("4", posAccHautGauche, posAccHautDroite, posAccHautHaut, posAccHautBas);

	}

	protected void calculEspacePorteCote()
	{
		espacePorteCoteHaut = (vDist(left, right) - ((2 * largeurPanneauHaut) + espaceEntreAccH1)) / 2;
	}

	protected double calculEspacePorteHaut()
	{
		return espacePorteHautMax - ((2150 - hDist(top, sill)) * 2 / 3);
	}

	protected double calculEspacePorteBas()
	{
		return hDist(top, sill) - calculEspacePorteHaut() - hauteurPanneauHaut - hauteurPanneauBas - espaceEntreAccV1;
	}
}
