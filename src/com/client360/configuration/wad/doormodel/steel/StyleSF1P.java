package com.client360.configuration.wad.doormodel.steel;

public class StyleSF1P extends StyleSF4P
{
	@Override
	protected void load()
	{
		largeurPanneauHaut = 235;
		hauteurPanneauHaut = 1682;
		addAccessoires();
	}
}
