package com.client360.configuration.wad.doormodel.steel;

import java.awt.Color;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public abstract class AcierContemporaine extends ParametricModel
{
	// Lignes accessoires
	public String thermosHaut = "thermosHaut";
	public String thermosBas = "thermosBas";
	public String thermosGauche = "thermosGauche";
	public String thermosDroit = "thermosDroit";

	@Override
	protected void load()
	{
		addColor((Color) getParametricObject("Couleur motif"));
	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxWidth(1000);
		addMaxHeight(2250);
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

	protected void addAccessoires()
	{

	}

}
