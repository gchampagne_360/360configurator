package com.client360.configuration.wad.doormodel;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class Slab2SubModel extends ParametricModel
{
	@Override
	protected Unit<Length> getUnit()
	{

		return Units.INCH;
	}

	@Override
	protected void load()
	{

		addGaps(5d + 1d / 8d, 23d);
		addLineDownOffset(1, patternTop, 11d + 1d / 2d);
		addLineDownOffset(2, 1, 25d + 1d / 2d);
		addLineDownOffset(3, patternTop, 53d + 7 / 16d);
		addLineDownOffset(4, 3, 15 + 1d / 2d);
		addLineDownOffset(7, 1, 30);
		addLineUpOffset(8, 3, 3);

		addLineRightOffset(5, patternLeft, 0d);
		addLineRightOffset(6, patternRight, 0d);

		addElementSpace("ACC2", 5, 6, 1, 2);
		addElementSpace("ACC3", 5, 6, 8, 4);
		addElementSpace("ACC1", 5, 6, 3, 4);
	}

}
