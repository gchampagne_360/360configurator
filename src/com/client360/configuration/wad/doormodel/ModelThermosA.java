package com.client360.configuration.wad.doormodel;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.cad.profiles.ProfileParecloseA;
import com.client360.configuration.wad.cad.profiles.ProfileParecloseB;
import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class ModelThermosA extends ParametricModel
{

	public ModelThermosA()
	{
		super();
	}

	@Override
	protected void load()
	{
		addGaps(130, 200, 100);

		// Construction lines definitions
		addLineDownOffset(1, patternTop, (hDist(patternTop, patternSill) - 300) * 0.75);

		// Forms creation
		addFormRectangle(patternLeft, patternRight, patternTop, 1);

		// Form Spacer definition
		addSpacer(40, 40, 40, 40);

		// Form Profile definition
		addProfile(new ProfileParecloseA(), new ProfileParecloseB());

	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

}
