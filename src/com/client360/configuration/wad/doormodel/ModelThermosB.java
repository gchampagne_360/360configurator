package com.client360.configuration.wad.doormodel;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class ModelThermosB extends ParametricModel
{

	public ModelThermosB()
	{
		super();
	}

	@Override
	protected void load()
	{
		addGaps(130, 200, 100);

		// Construction lines definitions
		addLineLeftOffset(1, vPatternCenter, 25);
		addLineLeftOffset(2, 1, 30);
		addLineRightOffset(3, vPatternCenter, 25);
		addLineRightOffset(4, 3, 30);
		addLineRightOffset(5, patternLeft, 30);
		addLineLeftOffset(6, patternRight, 30);

		addLineDownOffset(1, patternTop, 30);
		addLineDownOffset(2, 1, 100);
		addLineDownOffset(3, patternTop, (hDist(patternTop, patternSill) - 300) * 0.75);
		addLineUpOffset(4, 3, 30);
		addLineDownOffset(5, 3, 100);
		addLineDownOffset(6, 5, 100);
		addLineDownOffset(7, 6, 100);
		addLineDownOffset(8, patternSill, 30);
		addLineDownOffset(9, 8, 114);

		// Forms creation
		addFormElongatedHalfArc(5, 2, 1, 4, 2, null, false, false);
		addFormElongatedHalfArc(4, 6, 1, 4, 2, null, true, false);

		// Form Spacer definition
		addSpacer(25, 25, 25, 25);
		addSpacer(25, 25, 25, 25);

		// Form Profile definition
		// addProfile(new ProfileBigDecoMouldA(), new ProfileBigDecoMouldB());
		// addProfile(new ProfileBigDecoMouldA(), new ProfileBigDecoMouldB());
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

}
