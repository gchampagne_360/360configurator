package com.client360.configuration.wad.doormodel;

import com.client360.configuration.wad.doormodel.accessoire.wood.PanneauMassifPostD2;
import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.DoorModelTesting;

public class TestingDoorModel
{
	public static void main(String[] args)
	{
		new DoorModelTesting(PanneauMassifPostD2.class, Units.mm(900), Units.mm(2000));
	}
}