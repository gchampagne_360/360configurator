package com.client360.configuration.wad.doormodel.wood;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.doormodel.accessoire.wood.PanneauEssai;
import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

/**
 * Exemple de simulacre rapide du mod�le Targa. NE PAS MODIFIER.
 * 
 * Cette classe d�montre l'utilisation des ElementSpace pour d�finir la grandeur d'accessoires, permettant l'usage d'accessoires g�n�riques qui ne d�pendent pas
 * du mod�le de porte parent.
 * 
 * Il est donc possible de faire tr�s peu d'accessoires qui deviennent ensuite adaptables � toutes les sauces.
 * 
 * @author Marc-Andr� Bouchard
 * @author 360 Innovations
 * 
 */
public class TargaEssai extends ParametricModel
{
	public static String centreGauche = "centreGauche";
	public static String centreDroite = "centreDroite";

	public static String coteGauche = "coteGauche";
	public static String coteDroite = "coteDroite";

	public static String extremeHaut = "extremeHaut";
	public static String milieuHaut = "milieuHaut";
	public static String centreHaut = "centreHaut";
	public static String centreHaut2 = "centreHaut2";

	public static String extremeBas = "extremeBas";
	public static String milieuBas = "milieuBas";
	public static String centreBas = "centreBas";
	public static String centreBas2 = "centreBas2";

	@Override
	protected void load()
	{
		addLineLeftOffset(centreGauche, vPatternCenter, 30);
		addLineRightOffset(centreDroite, vPatternCenter, 30);

		addLineLeftOffset(coteDroite, patternRight, 30);
		addLineRightOffset(coteGauche, patternLeft, 30);

		addLineDownOffset(extremeHaut, patternTop, 50);
		addLineDownOffset(milieuHaut, patternTop, hDist(extremeHaut, hPatternCenter) / 3);
		addLineDownOffset(centreHaut, patternTop, hDist(extremeHaut, hPatternCenter) / 3 * 2);
		addLineUpOffset(centreHaut2, hPatternCenter, 50);

		addLineUpOffset(extremeBas, patternSill, 50);
		addLineUpOffset(milieuBas, patternSill, hDist(extremeBas, hPatternCenter) / 3);
		addLineUpOffset(centreBas, patternSill, hDist(extremeBas, hPatternCenter) / 3 * 2);
		addLineDownOffset(centreBas2, hPatternCenter, 50);

		addElementSpace("a1", coteGauche, centreGauche, extremeHaut, milieuHaut);
		addElementSpace("b1", coteGauche, centreGauche, milieuHaut, centreHaut);
		addElementSpace("c1", coteGauche, centreGauche, centreHaut, centreHaut2);
		addElementSpace("d1", coteGauche, centreGauche, centreBas2, centreBas);
		addElementSpace("e1", coteGauche, centreGauche, centreBas, milieuBas);
		addElementSpace("f1", coteGauche, centreGauche, milieuBas, extremeBas);

		addElementSpace("a2", centreDroite, coteDroite, extremeHaut, milieuHaut);
		addElementSpace("b2", centreDroite, coteDroite, milieuHaut, centreHaut);
		addElementSpace("c2", centreDroite, coteDroite, centreHaut, centreHaut2);
		addElementSpace("d2", centreDroite, coteDroite, centreBas2, centreBas);
		addElementSpace("e2", centreDroite, coteDroite, centreBas, milieuBas);
		addElementSpace("f2", centreDroite, coteDroite, milieuBas, extremeBas);

		addSubModel(new PanneauEssai(), "a1");
		addSubModel(new PanneauEssai(), "b1");
		addSubModel(new PanneauEssai(), "c1");
		addSubModel(new PanneauEssai(), "d1");
		addSubModel(new PanneauEssai(), "e1");
		addSubModel(new PanneauEssai(), "f1");
		addSubModel(new PanneauEssai(), "a2");
		addSubModel(new PanneauEssai(), "b2");
		addSubModel(new PanneauEssai(), "c2");
		addSubModel(new PanneauEssai(), "d2");
		addSubModel(new PanneauEssai(), "e2");
		addSubModel(new PanneauEssai(), "f2");
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

}
