package com.client360.configuration.wad.doormodel.wood;

/**
 * @author jjagod Programme qui permet de calculer la position des traverses suivant la hauteur du vantail
 */

public class ClassiqueFlavia extends GestionLargeur
{
	private Double maxIntHau4 = 2242.0;
	private Double maxIntHau3 = 2142.0;

	private Double larJde = 45.0;

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxHeight(2242); // s'applique avec la r�gle "_2"
		addMaxHeight(2142); // s'applique avec la r�gle "_1"

	}

	@Override
	protected void addBasicStuff()
	{
		super.addBasicStuff();

		getVConstructionLine().remove(posAccGau);
		getVConstructionLine().remove(posAccDro);
		addLineLeftOffset(posAccGau, posBatGau, 6);
		addLineRightOffset(posAccDro, posBatDro, 6);

	}

	// ------------Fabrication des lignes

	// si la hauteur du vantail est inf�rieure � 2142
	public void _1()
	{
		double hauteurVantail = hDist(top, sill);

		addLineDownOffset(basLarTra1, top, 105);
		addLineDownOffset(sill2, sill, (maxIntHau3 - hauteurVantail));
		addLineUpOffset(hauLarTra8, sill2, 105);
		addLineUpOffset(hauLarTra7, hauLarTra8, 59);
		addLineUpOffset(hauLarTra6, hauLarTra7, 105);
		addLineUpOffset(hauLarTra5, hauLarTra6, 105);
		addLineUpOffset(hauLarTra4, hauLarTra5, 105);
		addLineUpOffset(hauLarTra3, hauLarTra4, 105);
		addLineUpOffset(hauLarTra2, hauLarTra3, 105);

		addLineUpOffset(hauLarEmpBas, sill2, 635);

		addPart("traverse", "traverse1", posBatGau, posBatDro, top, basLarTra1);
		addPart("traverse", "traverse2", posBatGau, posBatDro, hauLarTra2, hauLarTra3);
		addPart("traverse", "traverse3", posBatGau, posBatDro, hauLarTra3, hauLarTra4);
		addPart("traverse", "traverse4", posBatGau, posBatDro, hauLarTra4, hauLarTra5);
		addPart("traverse", "traverse5", posBatGau, posBatDro, hauLarTra5, hauLarTra4);
		addPart("traverse", "traverse6", posBatGau, posBatDro, hauLarTra6, hauLarTra5);
		addPart("traverse", "traverse7", posBatGau, posBatDro, hauLarTra7, hauLarTra6);
		addPart("traverse", "traverse8", posBatGau, posBatDro, hauLarTra8, sill2);

		addLineUpOffset(hauAccHau, basLarTra1, 11);
		addLineDownOffset(basAccHau, hauLarEmpBas, 11);

		addElementSpace("3", posAccGau, posAccDro, hauAccHau, basAccHau);

		calculHauteurPlinthe();

	}

	// si la hauteur du vantail est inf�rieure � 2242
	public void _2()
	{
		double hauteurVantail = hDist(top, sill);

		addLineDownOffset(basLarTra1, top, 105);
		addLineDownOffset(sill2, sill, (maxIntHau3 - hauteurVantail));
		addLineUpOffset(hauLarTra9, sill2, 105);
		addLineUpOffset(hauLarTra8, hauLarTra9, 63);
		addLineUpOffset(hauLarTra7, hauLarTra8, 105);
		addLineUpOffset(hauLarTra6, hauLarTra7, 105);
		addLineUpOffset(hauLarTra5, hauLarTra6, 105);
		addLineUpOffset(hauLarTra4, hauLarTra5, 105);
		addLineUpOffset(hauLarTra3, hauLarTra4, 105);
		addLineUpOffset(hauLarTra2, hauLarTra3, 105);

		addLineUpOffset(hauLarEmpBas, sill2, 735);

		addPart("traverse", "traverse1", posBatGau, posBatDro, top, basLarTra1);
		addPart("traverse", "traverse2", posBatGau, posBatDro, hauLarTra2, hauLarTra3);
		addPart("traverse", "traverse3", posBatGau, posBatDro, hauLarTra3, hauLarTra4);
		addPart("traverse", "traverse4", posBatGau, posBatDro, hauLarTra4, hauLarTra5);
		addPart("traverse", "traverse5", posBatGau, posBatDro, hauLarTra5, hauLarTra4);
		addPart("traverse", "traverse6", posBatGau, posBatDro, hauLarTra6, hauLarTra5);
		addPart("traverse", "traverse7", posBatGau, posBatDro, hauLarTra7, hauLarTra6);
		addPart("traverse", "traverse8", posBatGau, posBatDro, hauLarTra8, hauLarTra7);
		addPart("traverse", "traverse9", posBatGau, posBatDro, hauLarTra9, sill2);

		addLineUpOffset(hauAccHau, basLarTra1, 11);
		addLineDownOffset(basAccHau, hauLarEmpBas, 11);

		addElementSpace("3", posAccGau, posAccDro, hauAccHau, basAccHau);

		calculHauteurPlinthe();

	}

	private void calculHauteurPlinthe()
	{
		if ((hDist(hauLarEmpBas, sill)) >= 201)
		{
			addLineUpOffset(pliBas, sill, 9);
			addLineUpOffset(pliHau, pliBas, 93);
			addLineUpOffset(jdeHau, pliHau, larJde);
		}

		else if ((hDist(hauLarEmpBas, sill)) < 201)
		{
			addLineUpOffset(pliBas, sill, 9);
			addLineUpOffset(pliHau, pliBas, 45);
			addLineUpOffset(jdeHau, pliHau, larJde);
		}

		addElementSpace("1", posPliGau, posPliDro, pliHau, pliBas);
		addElementSpace("2", posJdeGau, posJdeDro, jdeHau, pliHau);
	}
}