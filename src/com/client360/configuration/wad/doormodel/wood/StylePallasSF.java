package com.client360.configuration.wad.doormodel.wood;

/**
 * @author jjagod Programme qui permet de calculer la largeur des battants pour un vantail semi-fixe
 */
public class StylePallasSF extends StylePallas
{
	private Double minIntLar1 = 366.0;
	private Double minIntLar2 = 466.0;
	private Double minIntLar3 = 554.0;
	private Double minIntLar4 = 713.0;
	private Double minIntLar5 = 780.0;
	private Double minIntLar6 = 880.0;

	@Override
	public void matrixDefinition()
	{
		super.matrixDefinition();

		addMaxWidth(1000); // s'applique avec la r�gle "_f"
		addMaxWidth(879); // s'applique avec la r�gle "_e"
		addMaxWidth(779); // s'applique avec la r�gle "_d"
		addMaxWidth(712); // s'applique avec la r�gle "_c"
		addMaxWidth(553); // s'applique avec la r�gle "_b"
		addMaxWidth(453); // s'applique avec la r�gle "_a"

	}

	// -------------Fabrication des colonnes
	// si la largeur du vantail est inf�rieure � 453
	public void aOverride()
	{

		double largeurVantail = vDist(patternRight, patternLeft);
		double largeurBattant = 0.0;

		if (largeurVantail >= minIntLar1)

		{
			// Formule pour calculer la largeur des battants
			largeurBattant = (largeurVantail - minIntLar1) / 2 + minBattantSf;

			// Position du trait int�rieur du battant gauche
			addLineRightOffset(posBatGau, patternLeft, largeurBattant);
			// Position du trait int�rieur du battant droit
			addLineLeftOffset(posBatDro, patternRight, largeurBattant);
			// D�finition de la pi�ce battant gauche
			addPart("battant", "battant1", patternLeft, posBatGau, top, sill);
			// D�finition de la pi�ce battant droit
			addPart("battant", "battant2", patternRight, posBatDro, top, sill);

			addLineLeftOffset(posAccGau, posBatGau, 13.5);
			addLineRightOffset(posAccDro, posBatDro, 13.5);

		}
		calculLargeurPlinthe();

	}

	// si la largeur du vantail est inf�rieure � 553
	public void bOverride()
	{
		double largeurVantail = vDist(patternRight, patternLeft);
		double largeurBattant = 0.0;
		if (largeurVantail >= minIntLar2)

		{
			largeurBattant = (largeurVantail - minIntLar2) / 2 + minBattantSf;

			addLineRightOffset(posBatGau, patternLeft, largeurBattant);
			addLineLeftOffset(posBatDro, patternRight, largeurBattant);

			addPart("battant", "battant1", patternLeft, posBatGau, top, sill);
			addPart("battant", "battant2", patternRight, posBatDro, top, sill);

			addLineLeftOffset(posAccGau, posBatGau, 13.5);
			addLineRightOffset(posAccDro, posBatDro, 13.5);

		}
		calculLargeurPlinthe();
	}

	// si la largeur du vantail est inf�rieure � 712
	public void cOverride()
	{
		double largeurVantail = vDist(patternRight, patternLeft);
		double largeurBattant = 0.0;
		if (largeurVantail >= minIntLar3)

		{
			largeurBattant = (largeurVantail - minIntLar3) / 2 + minBattant;

			addLineRightOffset(posBatGau, patternLeft, largeurBattant);
			addLineLeftOffset(posBatDro, patternRight, largeurBattant);

			addPart("battant", "battant1", patternLeft, posBatGau, top, sill);
			addPart("battant", "battant2", patternRight, posBatDro, top, sill);

			addLineLeftOffset(posAccGau, posBatGau, 13.5);
			addLineRightOffset(posAccDro, posBatDro, 13.5);

		}
		calculLargeurPlinthe();
	}

	// si la largeur du vantail est inf�rieure � 779
	public void dOverride()
	{
		double largeurVantail = vDist(patternRight, patternLeft);
		double largeurBattant = 0.0;
		if (largeurVantail >= minIntLar4)

		{
			largeurBattant = (largeurVantail - minIntLar4) / 2 + minBattantSf;

			addLineRightOffset(posBatGau, patternLeft, largeurBattant);
			addLineLeftOffset(posBatDro, patternRight, largeurBattant);

			addPart("battant", "battant1", patternLeft, posBatGau, top, sill);
			addPart("battant", "battant2", patternRight, posBatDro, top, sill);

			addLineLeftOffset(posAccGau, posBatGau, 13.5);
			addLineRightOffset(posAccDro, posBatDro, 13.5);

		}
		calculLargeurPlinthe();
	}

	// si la largeur du vantail est inf�rieure � 879
	public void eOverride()
	{
		double largeurVantail = vDist(patternRight, patternLeft);
		double largeurBattant = 0.0;
		if (largeurVantail >= minIntLar5)

		{
			largeurBattant = (largeurVantail - minIntLar5) / 2 + minBattant;

			addLineRightOffset(posBatGau, patternLeft, largeurBattant);
			addLineLeftOffset(posBatDro, patternRight, largeurBattant);

			addPart("battant", "battant1", patternLeft, posBatGau, top, sill);
			addPart("battant", "battant2", patternRight, posBatDro, top, sill);

			addLineLeftOffset(posAccGau, posBatGau, 13.5);
			addLineRightOffset(posAccDro, posBatDro, 13.5);

		}
		calculLargeurPlinthe();

	}

	// si la largeur du vantail est inf�rieure � 1000
	public void fOverride()
	{
		double largeurVantail = vDist(patternRight, patternLeft);
		double largeurBattant = 0.0;
		if (largeurVantail >= minIntLar6)

		{
			largeurBattant = (largeurVantail - minIntLar6) / 2 + minBattant;

			addLineRightOffset(posBatGau, patternLeft, largeurBattant);
			addLineLeftOffset(posBatDro, patternRight, largeurBattant);

			addPart("battant", "battant1", patternLeft, posBatGau, top, sill);
			addPart("battant", "battant2", patternRight, posBatDro, top, sill);

			addLineLeftOffset(posAccGau, posBatGau, 13.5);
			addLineRightOffset(posAccDro, posBatDro, 13.5);

		}
		calculLargeurPlinthe();
	}
}
