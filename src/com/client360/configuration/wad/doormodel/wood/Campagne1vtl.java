package com.client360.configuration.wad.doormodel.wood;

/**
 * @author jjagod Programme qui permet de calculer la position des traverses suivant la hauteur du vantail
 */
public class Campagne1vtl extends GestionLargeurCampagne
{
	private Double maxIntHau3 = 2242.0;
	private Double maxIntHau2 = 2142.0;
	private Double maxIntHau1 = 2032.0;
	private Double minIntHau1 = 1867.0;

	private Double maxSbs2242 = 1165.0;
	private Double maxSbs2142 = 1065.0;
	private Double maxSbs2032 = 955.0;
	private Double minSbsHau1 = 790.0;
	private Double larJde = 45.0;

	private Double RecHauVolMobAcc = 33.0;

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxHeight(2242); // s'applique avec la r�gle "_4"
		addMaxHeight(2142); // s'applique avec la r�gle "_3"
		addMaxHeight(2032); // s'applique avec la r�gle "_2"
		addMaxHeight(1867); // s'applique avec la r�gle "_1"
	}

	// ------------Fabrication des lignes
	// si la hauteur du vantail est inf�rieure � 1867
	public void _1()
	{
		double hauteurVantail = hDist(top, sill);
		double hauSbs = 0.0;

		if (getParametricMeasure("hauSbs") != null && getParametricMeasure("hauSbs") != 0)
		{
			hauSbs = getParametricMeasure("hauSbs");
			System.out.println(hauSbs + " if");
		}
		else
		{
			hauSbs = minSbsHau1 - (minIntHau1 - hauteurVantail) / 2;
			System.out.println(hauSbs + " else");
		}

		addLineDownOffset(basLarTra1, top, 105);
		addLineDownOffset(hauLarTra2, top, hauteurVantail - hauSbs);
		addLineDownOffset(basLarTra2, hauLarTra2, 105);
		addLineDownOffset(sill2, sill, 165);
		addLineUpOffset(hauLarTra6, sill2, 105);
		addLineDownOffset(basLarTra5, hauLarTra6, 9);
		addLineUpOffset(hauLarTra5, basLarTra5, 59);
		addLineDownOffset(basLarTra4, hauLarTra5, 9);
		addLineUpOffset(hauLarTra4, basLarTra4, 79);
		addLineDownOffset(basLarTra3, hauLarTra4, 9);
		addLineUpOffset(hauLarTra3, basLarTra3, 105);
		addLineUpOffset(hauLarEmpBas, sill2, 321);

		addPart("traverse", "traverse1", posBatGau, posBatDro, top, basLarTra1);
		addPart("traverse", "traverse2", posBatGau, posBatDro, hauLarTra2, basLarTra2);
		addPart("traverse", "traverse3", posBatGau, posBatDro, hauLarTra3, basLarTra3);
		addPart("traverse", "traverse4", posBatGau, posBatDro, hauLarTra4, basLarTra4);
		addPart("traverse", "traverse5", posBatGau, posBatDro, hauLarTra5, basLarTra5);
		addPart("traverse", "traverse6", posBatGau, posBatDro, hauLarTra6, sill2);

		addLineUpOffset(hauAccHau, basLarTra1, 13.5);
		addLineDownOffset(basAccHau, hauLarTra2, 13.5);
		addLineUpOffset(hauAccBas, basLarTra2, 13.5);
		addLineDownOffset(basAccBas, hauLarEmpBas, 13.5);
		addLineUpOffset(volMobHau, hauAccHau, RecHauVolMobAcc);
		addLineDownOffset(volMobBas, basAccHau, RecHauVolMobAcc);

		addElementSpace("4", posAccGau, posAccDro, hauAccHau, basAccHau);
		addElementSpace("3", posAccGau, posAccDro, hauAccBas, basAccBas);
		System.out.println("Accessoire haut " + (hDist(hauAccHau, basAccHau) + "*" + vDist(posAccGau, posAccDro)));
		System.out.println("Accessoire bas " + (hDist(hauAccBas, basAccBas) + "*" + vDist(posAccGau, posAccDro)));

		calculHauteurPlinthe();
	}

	// si la hauteur du vantail est inf�rieure � 2032
	public void _2()
	{
		double hauteurVantail = hDist(top, sill);
		double hauSbs;
		if (getParametricMeasure("hauSbs") != null && getParametricMeasure("hauSbs") != 0)
		{
			hauSbs = getParametricMeasure("hauSbs");
		}
		else
		{
			hauSbs = maxSbs2032 - (maxIntHau1 - hauteurVantail);
		}

		addLineDownOffset(basLarTra1, top, 105);
		addLineDownOffset(hauLarTra2, top, hauteurVantail - hauSbs);
		addLineDownOffset(basLarTra2, hauLarTra2, 105);
		addLineDownOffset(sill2, sill, (maxIntHau1 - hauteurVantail));
		addLineUpOffset(hauLarTra6, sill2, 105);
		addLineDownOffset(basLarTra5, hauLarTra6, 9);
		addLineUpOffset(hauLarTra5, basLarTra5, 59);
		addLineDownOffset(basLarTra4, hauLarTra5, 9);
		addLineUpOffset(hauLarTra4, basLarTra4, 79);
		addLineDownOffset(basLarTra3, hauLarTra4, 9);
		addLineUpOffset(hauLarTra3, basLarTra3, 105);
		addLineUpOffset(hauLarEmpBas, sill2, 321);

		addPart("traverse", "traverse1", posBatGau, posBatDro, top, basLarTra1);
		addPart("traverse", "traverse2", posBatGau, posBatDro, hauLarTra2, basLarTra2);
		addPart("traverse", "traverse3", posBatGau, posBatDro, hauLarTra3, basLarTra3);
		addPart("traverse", "traverse4", posBatGau, posBatDro, hauLarTra4, basLarTra4);
		addPart("traverse", "traverse5", posBatGau, posBatDro, hauLarTra5, basLarTra5);
		addPart("traverse", "traverse6", posBatGau, posBatDro, hauLarTra6, sill2);

		addLineUpOffset(hauAccHau, basLarTra1, 13.5);
		addLineDownOffset(basAccHau, hauLarTra2, 13.5);
		addLineUpOffset(hauAccBas, basLarTra2, 13.5);
		addLineDownOffset(basAccBas, hauLarEmpBas, 13.5);
		addLineUpOffset(volMobHau, hauAccHau, RecHauVolMobAcc);
		addLineDownOffset(volMobBas, basAccHau, RecHauVolMobAcc);

		addElementSpace("4", posAccGau, posAccDro, hauAccHau, basAccHau);
		addElementSpace("3", posAccGau, posAccDro, hauAccBas, basAccBas);
		System.out.println("Accessoire haut " + (hDist(hauAccHau, basAccHau) + "*" + vDist(posAccGau, posAccDro)));
		System.out.println("Accessoire bas " + (hDist(hauAccBas, basAccBas) + "*" + vDist(posAccGau, posAccDro)));

		calculHauteurPlinthe();
	}

	// si la hauteur du vantail est inf�rieure � 2142
	public void _3()
	{
		double hauteurVantail = hDist(top, sill);
		double hauSbs;
		if (getParametricMeasure("hauSbs") != null && getParametricMeasure("hauSbs") != 0)
		{
			hauSbs = getParametricMeasure("hauSbs");
		}
		else
		{
			hauSbs = maxSbs2142 - (maxIntHau2 - hauteurVantail);
		}

		addLineDownOffset(basLarTra1, top, 105);
		addLineDownOffset(hauLarTra2, top, hauteurVantail - hauSbs);
		addLineDownOffset(basLarTra2, hauLarTra2, 105);
		addLineDownOffset(sill2, sill, (maxIntHau2 - hauteurVantail));
		addLineUpOffset(hauLarTra6, sill2, 105);
		addLineDownOffset(basLarTra5, hauLarTra6, 9);
		addLineUpOffset(hauLarTra5, basLarTra5, 59);
		addLineDownOffset(basLarTra4, hauLarTra5, 9);
		addLineUpOffset(hauLarTra4, basLarTra4, 79);
		addLineDownOffset(basLarTra3, hauLarTra4, 9);
		addLineUpOffset(hauLarTra3, basLarTra3, 105);
		addLineUpOffset(hauLarEmpBas, sill2, 321);

		addPart("traverse", "traverse1", posBatGau, posBatDro, top, basLarTra1);
		addPart("traverse", "traverse2", posBatGau, posBatDro, hauLarTra2, basLarTra2);
		addPart("traverse", "traverse3", posBatGau, posBatDro, hauLarTra3, basLarTra3);
		addPart("traverse", "traverse4", posBatGau, posBatDro, hauLarTra4, basLarTra4);
		addPart("traverse", "traverse5", posBatGau, posBatDro, hauLarTra5, basLarTra5);
		addPart("traverse", "traverse6", posBatGau, posBatDro, hauLarTra6, sill2);

		addLineUpOffset(hauAccHau, basLarTra1, 13.5);
		addLineDownOffset(basAccHau, hauLarTra2, 13.5);
		addLineUpOffset(hauAccBas, basLarTra2, 13.5);
		addLineDownOffset(basAccBas, hauLarEmpBas, 13.5);
		addLineUpOffset(volMobHau, hauAccHau, RecHauVolMobAcc);
		addLineDownOffset(volMobBas, basAccHau, RecHauVolMobAcc);

		addElementSpace("4", posAccGau, posAccDro, hauAccHau, basAccHau);
		addElementSpace("3", posAccGau, posAccDro, hauAccBas, basAccBas);

		System.out.println("Accessoire haut " + (hDist(hauAccHau, basAccHau) + "*" + vDist(posAccGau, posAccDro)));
		System.out.println("Accessoire bas " + (hDist(hauAccBas, basAccBas) + "*" + vDist(posAccGau, posAccDro)));

		calculHauteurPlinthe();
	}

	// si la hauteur du vantail est inf�rieure � 2242
	public void _4()
	{
		double hauteurVantail = hDist(top, sill);
		double hauSbs;
		if (getParametricMeasure("hauSbs") != null && getParametricMeasure("hauSbs") != 0)
		{
			hauSbs = getParametricMeasure("hauSbs");
		}
		else
		{
			hauSbs = maxSbs2242 - (maxIntHau3 - hauteurVantail);
		}

		addLineDownOffset(basLarTra1, top, 105);
		addLineDownOffset(hauLarTra2, top, hauteurVantail - hauSbs);
		addLineDownOffset(hauLarTra3, hauLarTra2, 105);
		addLineDownOffset(basLarTra3, hauLarTra3, 105);
		addLineDownOffset(sill2, sill, (maxIntHau3 - hauteurVantail));
		addLineUpOffset(hauLarTra7, sill2, 105);
		addLineUpOffset(hauLarTra6, hauLarTra7, 63);
		addLineUpOffset(hauLarTra5, hauLarTra6, 79);
		addLineUpOffset(hauLarTra4, hauLarTra5, 105);
		addLineUpOffset(hauLarEmpBas, sill2, 325);

		addPart("traverse", "traverse1", posBatGau, posBatDro, top, basLarTra1);
		addPart("traverse", "traverse2", posBatGau, posBatDro, hauLarTra2, hauLarTra3);
		addPart("traverse", "traverse3", posBatGau, posBatDro, hauLarTra3, basLarTra3);
		addPart("traverse", "traverse4", posBatGau, posBatDro, hauLarTra4, hauLarTra5);
		addPart("traverse", "traverse5", posBatGau, posBatDro, hauLarTra5, hauLarTra6);
		addPart("traverse", "traverse6", posBatGau, posBatDro, hauLarTra6, hauLarTra7);
		addPart("traverse", "traverse7", posBatGau, posBatDro, hauLarTra7, sill2);

		addLineUpOffset(hauAccHau, basLarTra1, 13.5);
		addLineDownOffset(basAccHau, hauLarTra2, 13.5);
		addLineUpOffset(hauAccBas, basLarTra3, 22.5);
		addLineDownOffset(basAccBas, hauLarEmpBas, 13.5);
		addLineUpOffset(volMobHau, hauAccHau, RecHauVolMobAcc);
		addLineDownOffset(volMobBas, basAccHau, RecHauVolMobAcc);

		addElementSpace("4", posAccGau, posAccDro, hauAccHau, basAccHau);
		addElementSpace("3", posAccGau, posAccDro, hauAccBas, basAccBas);

		System.out.println("Accessoire haut " + (hDist(hauAccHau, basAccHau) + "*" + vDist(posAccGau, posAccDro)));
		System.out.println("Accessoire bas " + (hDist(hauAccBas, basAccBas) + "*" + vDist(posAccGau, posAccDro)));

		calculHauteurPlinthe();
	}

	private void calculHauteurPlinthe()
	{
		if ((hDist(hauLarEmpBas, sill)) >= 201)
		{
			addLineUpOffset(pliBas, sill, 9);
			addLineUpOffset(pliHau, pliBas, 93);
			addLineUpOffset(jdeHau, pliHau, larJde);

		}

		else if ((hDist(hauLarEmpBas, sill)) < 201)
		{
			addLineUpOffset(pliBas, sill, 9);
			addLineUpOffset(pliHau, pliBas, 45);
			addLineUpOffset(jdeHau, pliHau, larJde);

		}

		addElementSpace("1", posPliGau, posPliDro, pliHau, pliBas);
		addElementSpace("2", posJdeGau, posJdeDro, jdeHau, pliHau);
	}

	@Override
	public Double getHauteurVoletMobile()
	{
		return (hDist(volMobBas, volMobHau));
	}

	@Override
	public Double getLargeurVoletMobile()
	{
		return (vDist(posVolMobDro, posVolMobGau));
	}

}
