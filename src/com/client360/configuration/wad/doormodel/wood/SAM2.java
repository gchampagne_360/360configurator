package com.client360.configuration.wad.doormodel.wood;

import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;

public class SAM2 extends HIContemporaineSF
{
	@Override
	public void load()
	{
		super.addAccessoires();
		addForm(rectangle(left, right, top, posJetDeauHaut), "/images/door_svg/wood/contemporaines/SAM2.svg", ImageAlign.BOTTOMLEFT);
	}
}
