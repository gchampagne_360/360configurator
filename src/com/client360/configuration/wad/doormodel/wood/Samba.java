package com.client360.configuration.wad.doormodel.wood;

import com.netappsid.commonutils.math.MathFunction;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;

public class Samba extends HIContemporaine
{
	// hauteurs
	private final double height1 = 1940;
	private final double height2 = 2090;
	private final double height3 = 2230;
	private final double height4 = 2250;

	// largeurs
	private final double width1 = 800;
	private final double width2 = 900;
	private final double width3 = 1000;

	protected final double largeurDroiteOculus = 121d;
	protected final double largeurBaseGaucheOculus = 325d;
	protected final double hauteurBaseHautOculus = 243d;

	protected final double hauteurOculus = 600d;
	protected final double largeurOculus = 500d;
	protected final double largeur2Oculus = largeurOculus / 2;

	// lignes accessoires
	public String oculusHaut = "oculusHaut";
	public String oculusBas = "oculusBas";
	public String oculusGauche = "oculusGauche";
	public String oculusDroit = "oculusDroit";
	public String oculusH2 = "oculusH2";

	@Override
	protected void load()
	{
		addAccessoires();
		addForm(rectangle(left, right, top, posJetDeauHaut), "/images/door_svg/wood/contemporaines/Samba_motif.svg", ImageAlign.BOTTOMCENTER);
	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxHeight(height4); // s'applique avec la r�gle "_4"
		addMaxHeight(height3); // s'applique avec la r�gle "_3"
		addMaxHeight(height2); // s'applique avec la r�gle "_2"
		addMaxHeight(height1); // s'applique avec la r�gle "_1"

		addMaxWidth(width3); // s'applique avec la r�gle "_c"
		addMaxWidth(width2); // s'applique avec la r�gle "_b"
		addMaxWidth(width1); // s'applique avec la r�gle "_a"
	}

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();

		addLineDownOffset(oculusHaut, top, getDistanceOculusHaut());
		addLineDownOffset(oculusBas, top, getDistanceOculusHaut() + hauteurOculus);
		addLineRightOffset(oculusGauche, left, getDistanceOculusGauche());
		addLineLeftOffset(oculusDroit, right, largeurDroiteOculus);
		// addLineLeftOffset(oculusH2, right, largeurDroiteOculus + largeur2Oculus);

		addElementSpace("3", oculusGauche, oculusDroit, oculusHaut, oculusBas);
		// addSubModel(new SambaOculus(), "3");
	}

	private double getDistanceOculusHaut()
	{
		double heightDifference = height4 - hDist(top, sill);
		double variableOffset = MathFunction.round(heightDifference / 10, 0) * 2.5;
		return hauteurBaseHautOculus - variableOffset;
	}

	private double getDistanceOculusGauche()
	{
		double variableOffset = width3 - vDist(left, right);
		return largeurBaseGaucheOculus - variableOffset;
	}
}
