package com.client360.configuration.wad.doormodel.wood;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class StyleHI extends ParametricModel
{
	protected final double hauteurPlinthe = 83.7d;
	protected final double hauteurJetDeau = 9.4d;
	protected final double hauteurBasPlinthe = 4d;

	protected final double espaceHaut = 96.5d;
	protected final double espaceCote = 96.5d;
	public final double espaceBas = 83.9d;

	// lines
	public String posPlintheHaut = "posPlintheHaut";
	public String posPlintheBas = "posPlintheBas";

	public String posJetDeauHaut = "posJetDeauHaut";

	public String posPanHautHaut = "posPanHautHaut";
	public String posPanHautBas = "posPanHautBas";
	public String posPanHautGauche = "posPanHautGauche";
	public String posPanHautDroit = "posPanHautDroit";

	public String posPanMilieuHaut = "posPanMilieuHaut";
	public String posPanMilieuBas = "posPanMilieuBas";
	public String posPanMilieuGauche = "posPanMilieuGauche";
	public String posPanMilieuDroit = "posPanMilieuDroit";

	public String posPanBasHaut = "posPanBasHaut";
	public String posPanBasBas = "posPanBasBas";
	public String posPanBasGauche = "posPanBasGauche";
	public String posPanBasDroit = "posPanBasDroit";

	@Override
	protected void load()
	{
		// TODO Auto-generated method stub

	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

	protected void addAccessoires()
	{
		addLineUpOffset(posPlintheBas, sill, hauteurBasPlinthe);
		addLineUpOffset(posPlintheHaut, posPlintheBas, hauteurPlinthe);

		addLineUpOffset(posJetDeauHaut, posPlintheHaut, hauteurJetDeau);

		addElementSpace("1", left, right, posPlintheHaut, posPlintheBas);
		addElementSpace("2", left, right, posJetDeauHaut, posPlintheHaut);
	}

}
