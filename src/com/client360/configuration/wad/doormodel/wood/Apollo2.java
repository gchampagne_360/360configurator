package com.client360.configuration.wad.doormodel.wood;

import com.netappsid.commonutils.math.MathFunction;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;

public class Apollo2 extends HIContemporaine
{
	// hauteur de base entre le top et la premi�re ligne du motif
	private Double hautLigneBase1 = 72.25;
	private Double hautLigneBase2 = 82.25;
	private Double hautLigneBase3 = 69.25;
	private Double hautLigneBase4 = 79.75;

	// hauteur de base entre le sill et la derni�re ligne du motif
	private Double basLigneBase1 = 84.66;
	private Double basLigneBase2 = 94.66;
	private Double basLigneBase3 = 82.16;
	private Double basLigneBase4 = 92.16;

	// hauteurs
	private final double height1 = 1940;
	private final double height2 = 2090;
	private final double height3 = 2230;
	private final double height4 = 2250;

	// largeurs
	private final double width1 = 779;
	private final double width2 = 879;
	private final double width3 = 1000;

	@Override
	protected void load()
	{
		addForm(rectangle(left, right, posMotifHaut, posMotifBas), "/images/door_svg/wood/contemporaines/Apollo2_motif.svg", ImageAlign.TOPCENTER);
	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxHeight(height4); // s'applique avec la r�gle "_4"
		addMaxHeight(height3); // s'applique avec la r�gle "_3"
		addMaxHeight(height2); // s'applique avec la r�gle "_2"
		addMaxHeight(height1); // s'applique avec la r�gle "_1"

		addMaxWidth(width3); // s'applique avec la r�gle "_c"
		addMaxWidth(width2); // s'applique avec la r�gle "_b"
		addMaxWidth(width1); // s'applique avec la r�gle "_a"
	}

	public void _1()
	{
		addAccessoires();
		setPosMotifHaut(height1, hautLigneBase1);
		setPosMotifBas(height1, basLigneBase1);
	}

	public void _2()
	{
		addAccessoires();
		setPosMotifHaut(height2, hautLigneBase2);
		setPosMotifBas(height2, basLigneBase2);
	}

	public void _3()
	{
		addAccessoires();
		setPosMotifHaut(height3, hautLigneBase3);
		setPosMotifBas(height3, basLigneBase3);
	}

	public void _4()
	{
		addAccessoires();
		setPosMotifHaut(height4, hautLigneBase4);
		setPosMotifBas(height4, basLigneBase4);
	}

	public void a()
	{

	}

	public void b()
	{

	}

	public void c()
	{

	}

	private void setPosMotifHaut(double baseHeight, double baseOffset)
	{
		double height = hDist(top, sill);
		double baseDifference = baseHeight - height;
		double variableOffset = MathFunction.round(baseDifference / 10, 0) * -5;
		double offset = baseOffset + variableOffset;
		addLineDownOffset(posMotifHaut, top, offset);
	}

	private void setPosMotifBas(double baseHeight, double baseOffset)
	{
		double height = hDist(top, sill);
		double baseDifference = baseHeight - height;
		double variableOffset = MathFunction.round(baseDifference / 10, 0) * -5;
		double offset = baseOffset + variableOffset;
		addLineUpOffset(posMotifBas, posJetDeauHaut, offset);
	}

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();
		super.addOculus();
	}
}
