package com.client360.configuration.wad.doormodel.wood;

public class HIContemporaineSF extends HIContemporaine
{
	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxWidth(500);
		addMaxWidth(400);

		addMaxHeight(2250);
	}
}
