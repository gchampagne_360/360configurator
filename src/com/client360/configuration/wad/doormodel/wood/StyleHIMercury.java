package com.client360.configuration.wad.doormodel.wood;

public class StyleHIMercury extends StyleHI
{
	private final double hauteurPanHaut = 991d;
	private final double hauteurPanMilieu = 224d;
	private final double espaceEntreAcc = 54.5d;

	@Override
	protected void load()
	{
		addAccessoires();
	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxHeight(2250);
		addMaxWidth(1000);
	}

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();

		addLineDownOffset(posPanHautHaut, top, espaceHaut);
		addLineRightOffset(posPanHautGauche, left, espaceCote);
		addLineLeftOffset(posPanHautDroit, right, espaceCote);
		addLineDownOffset(posPanHautBas, posPanHautHaut, hauteurPanHaut);

		addLineDownOffset(posPanMilieuHaut, posPanHautBas, espaceEntreAcc);
		addLineRightOffset(posPanMilieuGauche, left, espaceCote);
		addLineLeftOffset(posPanMilieuDroit, right, espaceCote);
		addLineDownOffset(posPanMilieuBas, posPanMilieuHaut, hauteurPanMilieu);

		addLineDownOffset(posPanBasHaut, posPanMilieuBas, espaceEntreAcc);
		addLineUpOffset(posPanBasBas, posJetDeauHaut, espaceBas);
		addLineRightOffset(posPanBasGauche, left, espaceCote);
		addLineLeftOffset(posPanBasDroit, right, espaceCote);

		addElementSpace("3", posPanBasGauche, posPanBasDroit, posPanBasHaut, posPanBasBas);
		addElementSpace("4", posPanMilieuGauche, posPanMilieuDroit, posPanMilieuHaut, posPanMilieuBas);
		addElementSpace("5", posPanHautGauche, posPanHautDroit, posPanHautHaut, posPanHautBas);
	}
}
