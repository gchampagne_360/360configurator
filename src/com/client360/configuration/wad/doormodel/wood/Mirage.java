package com.client360.configuration.wad.doormodel.wood;

import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;

public class Mirage extends HIContemporaine
{
	@Override
	protected void load()
	{
		addAccessoires();
		addForm(rectangle(left, right, top, posJetDeauHaut), "/images/door_svg/wood/contemporaines/Mirage_motif.svg", ImageAlign.BOTTOMRIGHT);
	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();
		addMaxWidth(1000);
		addMaxHeight(2250);
	}

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();
		super.addOculus();
	}
}
