package com.client360.configuration.wad.doormodel.wood;

/**
 * @author jjagod Programme qui permet de calculer la largeur des battants pour un vantail principal
 * 
 */
public class GestionLargeurStyleFloride extends GestionLargeur
{
	protected String posFouGau = "posFouGau";
	protected String posFouDro = "posFouDro";

	public static Double minIntLar1 = 850.0;
	public static Double minIntLar2 = 950.0;

	private Double larFou = 50.0;

	@Override
	public void matrixDefinition()
	{
		addMaxWidth(1000); // s'applique avec la r�gle "_b"
		addMaxWidth(949); // s'applique avec la r�gle "_a"

	}

	// -------------Fabrication des colonnes
	// Si la largeur du vantail est inf�rieure � 950
	@Override
	public void a()
	{

		double largeurVantail = vDist(patternRight, patternLeft);
		double largeurBattant = 0.0;

		if (largeurVantail >= minIntLar1)
		{
			largeurBattant = (largeurVantail - minIntLar1) / 2 + minBattant;

			addLineRightOffset(posBatGau, patternLeft, largeurBattant);
			addLineLeftOffset(posBatDro, patternRight, largeurBattant);
			addBasicStuff();
			calculLargeurPlinthe();
		}
	}

	public void b()
	{
		double largeurVantail = vDist(patternRight, patternLeft);
		double largeurBattant = 0.0;
		if (largeurVantail >= minIntLar2)
		{
			largeurBattant = (largeurVantail - minIntLar2) / 2 + minBattant;

			addLineRightOffset(posBatGau, patternLeft, largeurBattant);
			addLineLeftOffset(posBatDro, patternRight, largeurBattant);
			addLineRightOffset(posFouGau, posBatGau, larFou);
			addLineLeftOffset(posFouDro, posBatDro, larFou);

			addPart("battant", "battant1", patternLeft, posBatGau, top, sill);
			addPart("battant", "battant2", patternRight, posBatDro, top, sill);

			addLineLeftOffset(posAccGau, posFouGau, 13.5);
			addLineRightOffset(posAccDro, posFouDro, 13.5);

			calculLargeurPlinthe();
		}
	}
}
