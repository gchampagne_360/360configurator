package com.client360.configuration.wad.doormodel.wood;

import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;


public class Apollo3 extends HIContemporaine
{
	// hauteurs
	private final double height1 = 1890;
	private final double height2 = 2020;
	private final double height3 = 2150;
	private final double height4 = 2250;

	// largeurs
	private final double width1 = 779;
	private final double width2 = 879;
	private final double width3 = 1000;

	// hauteur de base entre le top et la premi�re ligne du motif
	private Double hauteurTrapezeHaut1 = 145d;
	private Double hauteurTrapezeHaut2 = 105d;

	// hauteur de base entre le sill et la derni�re ligne du motif
	private Double hauteurTrapezeBas1 = 156d;
	private Double hauteurTrapezeBas2 = 114d;

	@Override
	protected void load()
	{

	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxHeight(height4); // s'applique avec la r�gle "_4"
		addMaxHeight(height3); // s'applique avec la r�gle "_3"
		addMaxHeight(height2); // s'applique avec la r�gle "_2"
		addMaxHeight(height1); // s'applique avec la r�gle "_1"

		addMaxWidth(width3); // s'applique avec la r�gle "_c"
		addMaxWidth(width2); // s'applique avec la r�gle "_b"
		addMaxWidth(width1); // s'applique avec la r�gle "_a"
	}

	public void _1()
	{
		addCommonStuff();
		addForm(rectangle(left, right, posMotifHaut, posMotifBas), "/images/door_svg/wood/contemporaines/Apollo3_motif_1.svg", ImageScale.FULL);
	}

	public void _2()
	{
		addCommonStuff();
		addForm(rectangle(left, right, posMotifHaut, posMotifBas), "/images/door_svg/wood/contemporaines/Apollo3_motif_2.svg", ImageScale.FULL);
	}

	public void _3()
	{
		addCommonStuff();
		addForm(rectangle(left, right, posMotifHaut, posMotifBas), "/images/door_svg/wood/contemporaines/Apollo3_motif_3.svg", ImageScale.FULL);
	}

	public void _4()
	{
		addCommonStuff();
		addForm(rectangle(left, right, posMotifHaut, posMotifBas), "/images/door_svg/wood/contemporaines/Apollo3_motif_4.svg", ImageScale.FULL);
	}

	public void a()
	{

	}

	public void b()
	{

	}

	public void c()
	{

	}

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();
		super.addOculus();
	}

	private void addCommonStuff()
	{
		addAccessoires();

		addLineDownOffset(posMotifHaut, top, hauteurTrapezeHaut2);
		addLineUpOffset(posMotifBas, posJetDeauHaut, hauteurTrapezeBas2);

		// addForm(rectangle(left, right, top, posMotifHaut));
		// addForm(rectangle(left, right, posMotifBas, posJetDeauHaut));
	}

}
