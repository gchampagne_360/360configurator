package com.client360.configuration.wad.doormodel.wood;

/**
 * @author jjagod Programme qui permet de calculer la position des traverses suivant la hauteur du vantail
 */

public class ClassiqueViva extends GestionLargeur
{
	private Double maxIntHau4 = 2242.0;
	private Double maxIntHau3 = 2142.0;
	private Double maxIntHau2 = 1977.0;

	private Double larJde = 45.0;

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxHeight(2242); // s'applique avec la r�gle "_3"
		addMaxHeight(2142); // s'applique avec la r�gle "_2"
		addMaxHeight(1977); // s'applique avec la r�gle "_1"
	}

	@Override
	protected void addBasicStuff()
	{
		super.addBasicStuff();

		getVConstructionLine().remove(posAccGau);
		getVConstructionLine().remove(posAccDro);
		addLineLeftOffset(posAccGau, posBatGau, 0);
		addLineRightOffset(posAccDro, posBatDro, 0);

	}

	// ------------Fabrication des lignes

	// si la hauteur du vantail est inf�rieure � 1977
	public void _1()
	{
		double hauteurVantail = hDist(top, sill);

		addLineDownOffset(basLarTra1, top, 105);
		addLineDownOffset(sill2, sill, 165);
		addLineUpOffset(hauLarTra5, sill2, 105);
		addLineDownOffset(basLarTra4, hauLarTra5, 9);
		addLineUpOffset(hauLarTra4, basLarTra4, 59);
		addLineDownOffset(basLarTra3, hauLarTra4, 9);
		addLineUpOffset(hauLarTra3, basLarTra3, 79);
		addLineDownOffset(basLarTra2, hauLarTra3, 9);
		addLineUpOffset(hauLarTra2, basLarTra2, 105);
		addLineUpOffset(hauLarEmpBas, sill2, 321);

		addPart("traverse", "traverse1", posBatGau, posBatDro, top, basLarTra1);
		addPart("traverse", "traverse2", posBatGau, posBatDro, hauLarTra2, basLarTra2);
		addPart("traverse", "traverse3", posBatGau, posBatDro, hauLarTra3, basLarTra3);
		addPart("traverse", "traverse4", posBatGau, posBatDro, hauLarTra4, basLarTra4);
		addPart("traverse", "traverse6", posBatGau, posBatDro, hauLarTra5, sill2);

		addLineUpOffset(hauAccHau, basLarTra1, 0);
		addLineDownOffset(basAccHau, hauLarEmpBas, 0);

		addElementSpace("3", posAccGau, posAccDro, hauAccHau, basAccHau);
		// Accessoire CfpMoulure pour viva d�cor et viper
		addElementSpace("4", posAccGau, posAccDro, hauAccHau, basAccHau);

		calculHauteurPlinthe();

	}

	// si la hauteur du vantail est inf�rieure � 2142
	public void _2()
	{
		double hauteurVantail = hDist(top, sill);

		addLineDownOffset(basLarTra1, top, 105);
		addLineDownOffset(sill2, sill, (maxIntHau3 - hauteurVantail));
		addLineUpOffset(hauLarTra5, sill2, 105);
		addLineDownOffset(basLarTra4, hauLarTra5, 9);
		addLineUpOffset(hauLarTra4, basLarTra4, 59);
		addLineDownOffset(basLarTra3, hauLarTra4, 9);
		addLineUpOffset(hauLarTra3, basLarTra3, 79);
		addLineDownOffset(basLarTra2, hauLarTra3, 9);
		addLineUpOffset(hauLarTra2, basLarTra2, 105);
		addLineUpOffset(hauLarEmpBas, sill2, 321);

		addPart("traverse", "traverse1", posBatGau, posBatDro, top, basLarTra1);
		addPart("traverse", "traverse2", posBatGau, posBatDro, hauLarTra2, basLarTra2);
		addPart("traverse", "traverse3", posBatGau, posBatDro, hauLarTra3, basLarTra3);
		addPart("traverse", "traverse4", posBatGau, posBatDro, hauLarTra4, basLarTra4);
		addPart("traverse", "traverse5", posBatGau, posBatDro, hauLarTra5, sill2);

		addLineUpOffset(hauAccHau, basLarTra1, 0);
		addLineDownOffset(basAccHau, hauLarEmpBas, 0);

		addElementSpace("3", posAccGau, posAccDro, hauAccHau, basAccHau);
		// Accessoire CfpMoulure pour viva d�cor et viper
		addElementSpace("4", posAccGau, posAccDro, hauAccHau, basAccHau);

		calculHauteurPlinthe();

	}

	// si la hauteur du vantail est inf�rieure � 2242
	public void _3()
	{
		double hauteurVantail = hDist(top, sill);

		addLineDownOffset(basLarTra1, top, 105);
		addLineDownOffset(sill2, sill, (maxIntHau4 - hauteurVantail));
		addLineUpOffset(hauLarTra6, sill2, 105);
		addLineDownOffset(basLarTra5, hauLarTra6, 9);
		addLineUpOffset(hauLarTra5, basLarTra5, 63);
		addLineDownOffset(basLarTra4, hauLarTra5, 9);
		addLineUpOffset(hauLarTra4, basLarTra4, 79);
		addLineDownOffset(basLarTra3, hauLarTra4, 9);
		addLineUpOffset(hauLarTra3, basLarTra3, 105);
		addLineDownOffset(basLarTra2, hauLarTra3, 9);
		addLineUpOffset(hauLarTra2, basLarTra2, 105);
		addLineUpOffset(hauLarEmpBas, sill2, 421);

		addPart("traverse", "traverse1", posBatGau, posBatDro, top, basLarTra1);
		addPart("traverse", "traverse2", posBatGau, posBatDro, hauLarTra2, basLarTra2);
		addPart("traverse", "traverse3", posBatGau, posBatDro, hauLarTra3, basLarTra3);
		addPart("traverse", "traverse4", posBatGau, posBatDro, hauLarTra4, basLarTra4);
		addPart("traverse", "traverse5", posBatGau, posBatDro, hauLarTra5, basLarTra5);
		addPart("traverse", "traverse6", posBatGau, posBatDro, hauLarTra6, sill2);

		addLineUpOffset(hauAccHau, basLarTra1, 0);
		addLineDownOffset(basAccHau, hauLarEmpBas, 0);

		addElementSpace("3", posAccGau, posAccDro, hauAccHau, basAccHau);
		// Accessoire CfpMoulure pour viva d�cor et viper
		addElementSpace("4", posAccGau, posAccDro, hauAccHau, basAccHau);

		calculHauteurPlinthe();

	}

	private void calculHauteurPlinthe()
	{
		if ((hDist(hauLarEmpBas, sill)) >= 201)
		{
			addLineUpOffset(pliBas, sill, 9);
			addLineUpOffset(pliHau, pliBas, 93);
			addLineUpOffset(jdeHau, pliHau, larJde);
		}

		else if ((hDist(hauLarEmpBas, sill)) < 201)
		{
			addLineUpOffset(pliBas, sill, 9);
			addLineUpOffset(pliHau, pliBas, 45);
			addLineUpOffset(jdeHau, pliHau, larJde);
		}

		addElementSpace("1", posPliGau, posPliDro, pliHau, pliBas);
		addElementSpace("2", posJdeGau, posJdeDro, jdeHau, pliHau);
	}
}