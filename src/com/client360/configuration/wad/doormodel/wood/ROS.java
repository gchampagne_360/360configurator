package com.client360.configuration.wad.doormodel.wood;

import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;

public class ROS extends HIContemporaineSF
{
	public void a()
	{
		super.addAccessoires();
		addForm(rectangle(left, right, top, posJetDeauHaut), "/images/door_svg/wood/contemporaines/ROS_small_left.svg", ImageAlign.TOPLEFT);
	}

	public void b()
	{
		super.addAccessoires();
		addForm(rectangle(left, right, top, posJetDeauHaut), "/images/door_svg/wood/contemporaines/ROS_big_left.svg", ImageAlign.TOPLEFT);
	}
}
