package com.client360.configuration.wad.doormodel.wood;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

/**
 * @author jjagod Programme qui permet de calculer la largeur des battants pour un vantail principal
 * 
 */

public class GestionLargeur extends ParametricModel
{
	// Panneau plate bande
	public static Double larPlateBandeVisible = 32.0;

	public static Double minIntLar1 = 780.0;
	public static Double minIntLar2 = 880.0;
	public static Double dedVtlPli = 38.0;
	public static Double dedVtlJde = 30.0;

	public static String posBatGau = "posBatGau";
	public static String posBatDro = "posBatDro";
	public static String posAccGau = "posAccGau";
	public static String posAccDro = "posAccDro";
	public static String posPliGau = "posPliGau";
	public static String posPliDro = "posPliDro";
	public static String posJdeGau = "posJdeGau";
	public static String posJdeDro = "posJdeDro";
	public static String posVolMobGau = "posVolMobGau";
	public static String posVolMobDro = "posVolMobDro";
	public static String posMnoGau2pxBas = "posMnoGau2pxBas";
	public static String posMnoDro2pxBas = "posMnoDro2pxBas";
	public static String posAccGauMno = "posAccGauMno";
	public static String posAccDroMno = "posAccDroMno";

	// ===============================================================================
	// PARAMETRES VOLET

	public static String basLarTra1 = "basLarTra1";
	public static String hauLarTra2 = "hauLarTra2";
	public static String basLarTra2 = "basLarTra2";
	public static String hauLarTra3 = "hauLarTra3";
	public static String basLarTra3 = "basLarTra3";
	public static String hauLarTra4 = "hauLarTra4";
	public static String basLarTra4 = "basLarTra4";
	public static String hauLarTra5 = "hauLarTra5";
	public static String basLarTra5 = "basLarTra5";
	public static String hauLarTra6 = "hauLarTra6";
	public static String basLarTra6 = "basLarTra6";
	public static String hauLarTra7 = "hauLarTra7";
	public static String basLarTra7 = "basLarTra7";
	public static String hauLarTra8 = "hauLarTra8";
	public static String basLarTra8 = "basLarTra8";
	public static String hauLarTra9 = "hauLarTra9";
	public static String basLarTra9 = "basLarTra9";

	public static String hauAccHau = "hauAccHau";
	public static String basAccHau = "basAccHau";
	public static String hauAccBas = "hauAccBas";
	public static String basAccBas = "basAccBas";

	public static String pliHau = "pliHau";
	public static String pliBas = "pliBas";

	public static String jdeHau = "jdeHau";

	public static String volMobHau = "volMobHau";
	public static String volMobBas = "volMobBas";

	public static String hauLarEmpBas = "hauLarEmpBas";

	public static String sill2 = "sill2";

	// PARAMETRES VOLET
	// ===============================================================================

	public static Double minBattant = 94.5;
	public static Double minBattantSf = 61.0;
	public static Double stdBattant = 104.5;
	public static Double larMno2pxBas = 95.0;

	public static Double RecLarVolMobAcc = 16.0;

	@Override
	protected void load()
	{

	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

	@Override
	public void matrixDefinition()
	{
		addMaxWidth(1000); // s'applique avec la r�gle "_c"
		addMaxWidth(879); // s'applique avec la r�gle "_b"
		addMaxWidth(779); // s'applique avec la r�gle "_a"
	}

	// -------------Fabrication des colonnes
	protected void addBasicStuff()
	{
		// Ajout des lignes de base pour la porte.
		addPart("battant", "battant1", patternLeft, posBatGau, top, sill);
		addPart("battant", "battant2", patternRight, posBatDro, top, sill);

		addLineLeftOffset(posAccGau, posBatGau, 13.5);
		addLineRightOffset(posAccDro, posBatDro, 13.5);

		addLineLeftOffset(posVolMobGau, posAccGau, RecLarVolMobAcc);
		addLineRightOffset(posVolMobDro, posAccDro, RecLarVolMobAcc);

		addLineLeftOffset(posMnoGau2pxBas, vPatternCenter, (larMno2pxBas / 2));
		addLineRightOffset(posMnoDro2pxBas, posMnoGau2pxBas, larMno2pxBas);

		addLineRightOffset(posAccGauMno, posMnoGau2pxBas, 13.5);
		addLineLeftOffset(posAccDroMno, posMnoDro2pxBas, 13.5);
	}

	protected void calculLargeurPlinthe()
	{
		addLineRightOffset(posPliGau, patternLeft, dedVtlPli);
		addLineLeftOffset(posPliDro, patternRight, dedVtlPli);
		addLineRightOffset(posJdeGau, patternLeft, dedVtlJde);
		addLineLeftOffset(posJdeDro, patternRight, dedVtlJde);
	}

	// Si la largeur du vantail est inf�rieure � 779
	public void a()
	{
		addLineRightOffset(posBatGau, patternLeft, stdBattant);
		addLineLeftOffset(posBatDro, patternRight, stdBattant);
		addBasicStuff();
		calculLargeurPlinthe();
	}

	// Si la largeur du vantail est inf�rieure � 879
	public void b()
	{
		double largeurVantail = vDist(patternRight, patternLeft);
		double largeurBattant = 0.0;

		if (largeurVantail >= minIntLar1)
		{
			largeurBattant = (largeurVantail - minIntLar1) / 2 + minBattant;

			addLineRightOffset(posBatGau, patternLeft, largeurBattant);
			addLineLeftOffset(posBatDro, patternRight, largeurBattant);
			addBasicStuff();
			calculLargeurPlinthe();
		}
	}

	// Si la largeur du vantail est inf�rieure � 1000
	public void c()
	{
		double largeurVantail = vDist(patternRight, patternLeft);
		double largeurBattant = 0.0;
		if (largeurVantail >= minIntLar2)
		{
			largeurBattant = (largeurVantail - minIntLar2) / 2 + minBattant;

			addLineRightOffset(posBatGau, patternLeft, largeurBattant);
			addLineLeftOffset(posBatDro, patternRight, largeurBattant);
			addBasicStuff();
			calculLargeurPlinthe();
		}
	}

	public Double getHauteurVoletMobile()
	{
		return 0.0;
	}

	public Double getLargeurVoletMobile()
	{
		return 0.0;
	}
}
