package com.client360.configuration.wad.doormodel.wood;

import com.netappsid.commonutils.math.MathFunction;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;

public class Cooper extends HIContemporaine
{
	// hauteurs
	private final double height1 = 2110;
	private final double height2 = 2250;

	// largeurs
	private final double width1 = 800;
	private final double width2 = 900;
	private final double width3 = 1000;

	// valeurs fixes
	protected final double largeurDroiteOculus = 117d;
	protected final double hauteurOculus = 1842d;
	protected final double largeurOculus = 300d;

	// valeurs de bases pour variables
	protected final double hauteurBaseOculusHaut1 = 98d;
	protected final double hauteurBaseOculusHaut2 = 173d;
	protected final double hauteurBaseOculusBas1 = 26.9d;
	protected final double hauteurBaseOculusBas2 = 91.9d;

	protected final double hauteurBaseLignesHautA1 = 378d;
	protected final double hauteurBaseLignesHautB1 = 278d;
	protected final double hauteurBaseLignesHautC1 = 178d;
	protected final double hauteurBaseLignesHautA2 = 457d;
	protected final double hauteurBaseLignesHautB2 = 357d;
	protected final double hauteurBaseLignesHautC2 = 257d;

	protected final double hauteurBaseLignesBasA1 = 296.2d;
	protected final double hauteurBaseLignesBasB1 = 196.2d;
	protected final double hauteurBaseLignesBasC1 = 96.2d;
	protected final double hauteurBaseLignesBasA2 = 366.2d;
	protected final double hauteurBaseLignesBasB2 = 266.2d;
	protected final double hauteurBaseLignesBasC2 = 166.2d;

	// lignes accessoires
	public String oculusHaut = "oculusHaut";
	public String oculusBas = "oculusBas";
	public String oculusGauche = "oculusGauche";
	public String oculusDroit = "oculusDroit";
	public String lignesHaut = "lignesHaut";
	public String lignesBas = "lignesBas";

	@Override
	protected void load()
	{
		addLineLeftOffset(oculusDroit, right, largeurDroiteOculus);
		addLineLeftOffset(oculusGauche, right, largeurDroiteOculus + largeurOculus);

		addForm(rectangle(oculusGauche, oculusDroit, oculusHaut, oculusBas), "/images/door_svg/wood/contemporaines/Cooper_window.svg", ImageAlign.TOPLEFT);
		addEmptySpacer();
		addEmptyProfile();

		addForm(rectangle(left, oculusGauche, lignesHaut, lignesBas), "/images/door_svg/wood/contemporaines/Cooper_lines.svg", ImageScale.FULL);
		addEmptySpacer();
		addEmptyProfile();
	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxHeight(height2); // s'applique avec la r�gle "_2"
		addMaxHeight(height1); // s'applique avec la r�gle "_1"

		addMaxWidth(width3); // s'applique avec la r�gle "_c"
		addMaxWidth(width2); // s'applique avec la r�gle "_b"
		addMaxWidth(width1); // s'applique avec la r�gle "_a"
	}

	public void a1Override()
	{
		addAccessoires();
		addLinesForOculusMotif(height1, hauteurBaseOculusHaut1, hauteurBaseOculusBas1);
		addLinesForLignesMotif(height1, hauteurBaseLignesHautA1, hauteurBaseLignesBasA1);
	}

	public void a2Override()
	{
		addAccessoires();
		addLinesForOculusMotif(height2, hauteurBaseOculusHaut2, hauteurBaseOculusBas2);
		addLinesForLignesMotif(height2, hauteurBaseLignesHautA2, hauteurBaseLignesBasA2);
	}

	public void b1Override()
	{
		addAccessoires();
		addLinesForOculusMotif(height1, hauteurBaseOculusHaut1, hauteurBaseOculusBas1);
		addLinesForLignesMotif(height1, hauteurBaseLignesHautB1, hauteurBaseLignesBasB1);
	}

	public void b2Override()
	{
		addAccessoires();
		addLinesForOculusMotif(height2, hauteurBaseOculusHaut2, hauteurBaseOculusBas2);
		addLinesForLignesMotif(height2, hauteurBaseLignesHautB2, hauteurBaseLignesBasB2);
	}

	public void c1Override()
	{
		addAccessoires();
		addLinesForOculusMotif(height1, hauteurBaseOculusHaut1, hauteurBaseOculusBas1);
		addLinesForLignesMotif(height1, hauteurBaseLignesHautC1, hauteurBaseLignesBasC1);
	}

	public void c2Override()
	{
		addAccessoires();
		addLinesForOculusMotif(height2, hauteurBaseOculusHaut2, hauteurBaseOculusBas2);
		addLinesForLignesMotif(height2, hauteurBaseLignesHautC2, hauteurBaseLignesBasC2);
	}

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();
	}

	private void addLinesForLignesMotif(double baseHeight, double baseHeightHaut, double baseHeightBas)
	{
		double height = hDist(top, sill);
		double baseDifference = baseHeight - height;

		// lignes
		double variableOffsetLines = MathFunction.round(baseDifference / 10, 0) * -5;
		double offsetLinesH = baseHeightHaut + variableOffsetLines;
		double offsetLinesB = baseHeightBas + variableOffsetLines;
		addLineDownOffset(lignesHaut, top, offsetLinesH);
		addLineUpOffset(lignesBas, posJetDeauHaut, offsetLinesB);
	}

	private void addLinesForOculusMotif(double baseHeight, double baseHeightHaut, double baseHeightBas)
	{
		double height = hDist(top, sill);
		double baseDifference = baseHeight - height;

		double variableOffset = 0;
		if (baseHeight == height1)
		{
			variableOffset = MathFunction.round(baseDifference / 10, 0) * -10;
			double offset = hauteurBaseOculusHaut1 + variableOffset;
			addLineDownOffset(oculusHaut, top, offset);
			addLineUpOffset(oculusBas, posJetDeauHaut, baseHeightBas);
		}
		else
		{
			variableOffset = MathFunction.round(baseDifference / 10, 0) * -5;
			double offsetLinesH = baseHeightHaut + variableOffset;
			double offsetLinesB = baseHeightBas + variableOffset;
			addLineDownOffset(oculusHaut, top, offsetLinesH);
			addLineUpOffset(oculusBas, posJetDeauHaut, offsetLinesB);
		}
	}
}
