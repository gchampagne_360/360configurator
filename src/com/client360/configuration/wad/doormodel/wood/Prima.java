package com.client360.configuration.wad.doormodel.wood;

import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;

public class Prima extends HIContemporaine
{
	private final double coteThermos = 403d;

	// lignes accessoires
	public String thermosHaut = "thermosHaut";
	public String thermosBas = "thermosBas";
	public String thermosGauche = "thermosGauche";
	public String thermosDroit = "thermosDroit";

	@Override
	protected void load()
	{
		addAccessoires();
		addForm(rectangle(left, right, top, posJetDeauHaut), "/images/door_svg/wood/contemporaines/Prima_motif.svg", ImageAlign.NONE, ImageScale.FULL);
	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();
		addMaxWidth(1000);
		addMaxHeight(2250);
	}

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();

		double distanceHaut = hDist(top, sill) / 12;
		double distanceHaut2 = distanceHaut + coteThermos;
		double distanceCote = (vDist(left, right) - coteThermos) / 2;

		addLineDownOffset(thermosHaut, top, distanceHaut);
		addLineDownOffset(thermosBas, top, distanceHaut2);
		addLineRightOffset(thermosGauche, left, distanceCote);
		addLineLeftOffset(thermosDroit, right, distanceCote);

		addElementSpace("3", thermosGauche, thermosDroit, thermosHaut, thermosBas);
	}

}
