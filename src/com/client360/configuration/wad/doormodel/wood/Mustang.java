package com.client360.configuration.wad.doormodel.wood;

import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;

public class Mustang extends HIContemporaine
{
	public String thermosTop1 = "thermosTop1";
	public String thermosBottom1 = "thermosBottom1";
	public String thermosLeft1 = "thermosLeft1";
	public String thermosRight1 = "thermosRight1";

	public String thermosTop2 = "thermosTop2";
	public String thermosBottom2 = "thermosBottom2";
	public String thermosLeft2 = "thermosLeft2";
	public String thermosRight2 = "thermosRight2";

	public String thermosTop3 = "thermosTop3";
	public String thermosBottom3 = "thermosBottom3";
	public String thermosLeft3 = "thermosLeft3";
	public String thermosRight3 = "thermosRight3";

	private final double largeurThermos = 240.4d;

	private final double largeurDroiteThermos1 = 121.5d;
	private final double largeurDroiteThermos2 = 369d;
	private final double largeurDroiteThermos3 = 121.5d;

	private final double hauteurHautThermos1 = 1018.7d;
	private final double hauteurHautThermos2 = 1266.2d;
	private final double hauteurHautThermos3 = 1513.7d;

	@Override
	protected void load()
	{
		addAccessoires();
		addForm(rectangle(left, right, top, posJetDeauHaut), "/images/door_svg/wood/contemporaines/Mustang_motif.svg", ImageAlign.BOTTOMRIGHT);
	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();
		addMaxWidth(1000);
		addMaxHeight(2250);
	}

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();

		// lower thermos
		addLineUpOffset(thermosTop1, posJetDeauHaut, hauteurHautThermos1 + largeurThermos);
		addLineUpOffset(thermosBottom1, posJetDeauHaut, hauteurHautThermos1);
		addLineLeftOffset(thermosRight1, right, largeurDroiteThermos1);
		addLineLeftOffset(thermosLeft1, right, largeurDroiteThermos1 + largeurThermos);
		addElementSpace("3", thermosLeft1, thermosRight1, thermosTop1, thermosBottom1);

		// middle thermos
		addLineUpOffset(thermosTop2, posJetDeauHaut, hauteurHautThermos2 + largeurThermos);
		addLineUpOffset(thermosBottom2, posJetDeauHaut, hauteurHautThermos2);
		addLineLeftOffset(thermosRight2, right, largeurDroiteThermos2);
		addLineLeftOffset(thermosLeft2, right, largeurDroiteThermos2 + largeurThermos);
		addElementSpace("4", thermosLeft2, thermosRight2, thermosTop2, thermosBottom2);

		// higher thermos
		addLineUpOffset(thermosTop3, posJetDeauHaut, hauteurHautThermos3 + largeurThermos);
		addLineUpOffset(thermosBottom3, posJetDeauHaut, hauteurHautThermos3);
		addLineLeftOffset(thermosRight3, right, largeurDroiteThermos3);
		addLineLeftOffset(thermosLeft3, right, largeurDroiteThermos3 + largeurThermos);
		addElementSpace("5", thermosLeft3, thermosRight3, thermosTop3, thermosBottom3);
	}
}
