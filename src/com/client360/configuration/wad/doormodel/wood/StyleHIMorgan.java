package com.client360.configuration.wad.doormodel.wood;

public class StyleHIMorgan extends StyleHI
{
	private final double hauteurPanHaut = 424.5d;
	private final double espaceEntreAcc = 78d;

	@Override
	protected void load()
	{
		addAccessoires();
	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxHeight(2250);
		addMaxWidth(1000);
	}

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();

		addLineDownOffset(posPanHautHaut, top, espaceHaut);
		addLineRightOffset(posPanHautGauche, left, espaceCote);
		addLineLeftOffset(posPanHautDroit, right, espaceCote);
		addLineDownOffset(posPanHautBas, posPanHautHaut, hauteurPanHaut);

		addLineDownOffset(posPanBasHaut, posPanHautBas, espaceEntreAcc);
		addLineUpOffset(posPanBasBas, posJetDeauHaut, espaceBas);
		addLineRightOffset(posPanBasGauche, left, espaceCote);
		addLineLeftOffset(posPanBasDroit, right, espaceCote);

		addElementSpace("3", posPanBasGauche, posPanBasDroit, posPanBasHaut, posPanBasBas);
		addElementSpace("4", posPanHautGauche, posPanHautDroit, posPanHautHaut, posPanHautBas);

		addParametricObject("larPbe", "95");
		addParametricObject("larSpacer", "15");
		addParametricObject("pbh", "1");
		addParametricObject("pbv", "1");
	}

}
