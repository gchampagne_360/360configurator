package com.client360.configuration.wad.doormodel.wood;

/**
 * @author jjagod Programme qui permet de calculer la position des traverses suivant la hauteur du vantail
 */
public class ClassiqueApal extends GestionLargeur
{
	private Double maxIntHau4 = 2242.0;
	private Double maxIntHau3 = 2142.0;
	private Double maxIntHau2 = 1977.0;

	private Double maxSbs2242 = 1752.0;
	private Double maxSbs2142 = 1652.0;
	private Double minSbsHau1 = 1552.0;

	private Double larJde = 45.0;

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxHeight(2242); // s'applique avec la r�gle "_3"
		addMaxHeight(2142); // s'applique avec la r�gle "_2"
		addMaxHeight(1977); // s'applique avec la r�gle "_1"
	}

	@Override
	protected void load()
	{}

	// ------------Fabrication des lignes

	// si la hauteur du vantail est inf�rieure � 1977
	public void _1()
	{

		double hauteurVantail = hDist(top, sill);
		double hauSbs = minSbsHau1 - (2042 - hauteurVantail);

		addLineDownOffset(basLarTra1, top, 105);
		addLineDownOffset(hauLarTra2, top, hauteurVantail - hauSbs);
		addLineDownOffset(basLarTra2, hauLarTra2, 105);
		addLineDownOffset(sill2, sill, 165);
		addLineUpOffset(hauLarTra6, sill2, 105);
		addLineDownOffset(basLarTra5, hauLarTra6, 9);
		addLineUpOffset(hauLarTra5, basLarTra5, 59);
		addLineDownOffset(basLarTra4, hauLarTra5, 9);
		addLineUpOffset(hauLarTra4, basLarTra4, 79);
		addLineDownOffset(basLarTra3, hauLarTra4, 9);
		addLineUpOffset(hauLarTra3, basLarTra3, 105);
		addLineUpOffset(hauLarEmpBas, sill2, 321);

		addPart("traverse", "traverse1", posBatGau, posBatDro, top, basLarTra1);
		addPart("traverse", "traverse2", posBatGau, posBatDro, hauLarTra2, basLarTra2);
		addPart("traverse", "traverse3", posBatGau, posBatDro, hauLarTra3, basLarTra3);
		addPart("traverse", "traverse4", posBatGau, posBatDro, hauLarTra4, basLarTra4);
		addPart("traverse", "traverse5", posBatGau, posBatDro, hauLarTra5, basLarTra5);
		addPart("traverse", "traverse6", posBatGau, posBatDro, hauLarTra6, sill2);

		addLineUpOffset(hauAccHau, basLarTra1, 13.5);
		addLineDownOffset(basAccHau, hauLarTra2, 13.5);
		addLineUpOffset(hauAccBas, basLarTra2, 13.5);
		addLineDownOffset(basAccBas, hauLarEmpBas, 13.5);

		addElementSpace("4", posAccGau, posAccDro, hauAccHau, basAccHau);
		addElementSpace("3", posAccGau, posAccDro, hauAccBas, basAccBas);

		calculHauteurPlinthe();

	}

	// si la hauteur du vantail est inf�rieure � 2142
	public void _2()
	{
		double hauteurVantail = hDist(top, sill);
		double hauSbs = maxSbs2142 - (maxIntHau3 - hauteurVantail);

		addLineDownOffset(basLarTra1, top, 105);
		addLineDownOffset(hauLarTra2, top, hauteurVantail - hauSbs);
		addLineDownOffset(basLarTra2, hauLarTra2, 105);
		addLineDownOffset(sill2, sill, (maxIntHau3 - hauteurVantail));
		addLineUpOffset(hauLarTra6, sill2, 105);
		addLineDownOffset(basLarTra5, hauLarTra6, 9);
		addLineUpOffset(hauLarTra5, basLarTra5, 59);
		addLineDownOffset(basLarTra4, hauLarTra5, 9);
		addLineUpOffset(hauLarTra4, basLarTra4, 79);
		addLineDownOffset(basLarTra3, hauLarTra4, 9);
		addLineUpOffset(hauLarTra3, basLarTra3, 105);
		addLineUpOffset(hauLarEmpBas, sill2, 321);

		addPart("traverse", "traverse1", posBatGau, posBatDro, top, basLarTra1);
		addPart("traverse", "traverse2", posBatGau, posBatDro, hauLarTra2, basLarTra2);
		addPart("traverse", "traverse3", posBatGau, posBatDro, hauLarTra3, basLarTra3);
		addPart("traverse", "traverse4", posBatGau, posBatDro, hauLarTra4, basLarTra4);
		addPart("traverse", "traverse5", posBatGau, posBatDro, hauLarTra5, basLarTra5);
		addPart("traverse", "traverse6", posBatGau, posBatDro, hauLarTra6, sill2);

		addLineUpOffset(hauAccHau, basLarTra1, 13.5);
		addLineDownOffset(basAccHau, hauLarTra2, 13.5);
		addLineUpOffset(hauAccBas, basLarTra2, 13.5);
		addLineDownOffset(basAccBas, hauLarEmpBas, 13.5);

		addElementSpace("4", posAccGau, posAccDro, hauAccHau, basAccHau);
		addElementSpace("3", posAccGau, posAccDro, hauAccBas, basAccBas);

		calculHauteurPlinthe();

	}

	// si la hauteur du vantail est inf�rieure � 2242
	public void _3()
	{
		double hauteurVantail = hDist(top, sill);
		double hauSbs = maxSbs2242 - (maxIntHau4 - hauteurVantail);

		addLineDownOffset(basLarTra1, top, 105);
		addLineDownOffset(hauLarTra2, top, hauteurVantail - hauSbs);
		addLineDownOffset(basLarTra2, hauLarTra2, 105);
		addLineDownOffset(sill2, sill, (maxIntHau4 - hauteurVantail));
		addLineUpOffset(hauLarTra7, sill2, 105);
		addLineDownOffset(basLarTra6, hauLarTra7, 9);
		addLineUpOffset(hauLarTra6, basLarTra6, 63);
		addLineDownOffset(basLarTra5, hauLarTra6, 9);
		addLineUpOffset(hauLarTra5, basLarTra5, 79);
		addLineDownOffset(basLarTra4, hauLarTra5, 9);
		addLineUpOffset(hauLarTra4, basLarTra4, 105);
		addLineDownOffset(basLarTra3, hauLarTra4, 9);
		addLineUpOffset(hauLarTra3, basLarTra3, 105);
		addLineUpOffset(hauLarEmpBas, sill2, 421);

		addPart("traverse", "traverse1", posBatGau, posBatDro, top, basLarTra1);
		addPart("traverse", "traverse2", posBatGau, posBatDro, hauLarTra2, basLarTra2);
		addPart("traverse", "traverse3", posBatGau, posBatDro, hauLarTra3, basLarTra3);
		addPart("traverse", "traverse4", posBatGau, posBatDro, hauLarTra4, basLarTra4);
		addPart("traverse", "traverse5", posBatGau, posBatDro, hauLarTra5, basLarTra5);
		addPart("traverse", "traverse6", posBatGau, posBatDro, hauLarTra6, basLarTra6);
		addPart("traverse", "traverse7", posBatGau, posBatDro, hauLarTra7, sill2);

		addLineUpOffset(hauAccHau, basLarTra1, 13.5);
		addLineDownOffset(basAccHau, hauLarTra2, 13.5);
		addLineUpOffset(hauAccBas, basLarTra2, 13.5);
		addLineDownOffset(basAccBas, hauLarEmpBas, 13.5);

		addElementSpace("4", posAccGau, posAccDro, hauAccHau, basAccHau);
		addElementSpace("3", posAccGau, posAccDro, hauAccBas, basAccBas);

		calculHauteurPlinthe();

	}

	private void calculHauteurPlinthe()
	{
		if ((hDist(hauLarEmpBas, sill)) >= 201)
		{
			addLineUpOffset(pliBas, sill, 9);
			addLineUpOffset(pliHau, pliBas, 93);
			addLineUpOffset(jdeHau, pliHau, larJde);

		}

		else if ((hDist(hauLarEmpBas, sill)) < 201)
		{
			addLineUpOffset(pliBas, sill, 9);
			addLineUpOffset(pliHau, pliBas, 45);
			addLineUpOffset(jdeHau, pliHau, larJde);

		}

		addElementSpace("1", posPliGau, posPliDro, pliHau, pliBas);
		addElementSpace("2", posJdeGau, posJdeDro, jdeHau, pliHau);

	}

}
