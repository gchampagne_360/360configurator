package com.client360.configuration.wad.doormodel.wood;

/**
 * @author jjagod Programme qui permet de calculer la position des traverses suivant la hauteur du vantail
 */
public class StyleSilver extends GestionLargeur
{
	private Double maxIntHau3 = 2242.0;
	private Double maxIntHau2 = 2142.0;
	private Double maxIntHau1 = 2042.0;

	private Double maxSbs2242 = 1752.0;
	private Double maxSbs2142 = 1652.0;
	private Double maxSbs2042 = 1552.0;

	private Double larJde = 45.0;
	private Double larEmpMilieu2242 = 1016.0;
	private Double larEmpMilieu2142 = 1016.0;
	private Double larEmpMilieu2042 = 916.0;

	protected String hauAccItr = "hauAccItr";
	protected String basAccItr = "basAccItr";

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxHeight(2242); // s'applique avec la r�gle "_3"
		addMaxHeight(2142); // s'applique avec la r�gle "_2"
		addMaxHeight(2042); // s'applique avec la r�gle "_1"
	}

	// ------------Fabrication des lignes

	// si la hauteur du vantail est inf�rieure � 2042
	public void _1()
	{
		double hauteurVantail = hDist(top, sill);
		double hauSbs = maxSbs2042 - (maxIntHau1 - hauteurVantail);

		addLineDownOffset(basLarTra1, top, 105);
		addLineDownOffset(hauLarTra2, top, hauteurVantail - hauSbs);
		addLineDownOffset(basLarTra2, hauLarTra2, 79);
		addLineDownOffset(basLarTra3, hauLarTra2, larEmpMilieu2042);
		addLineUpOffset(hauLarTra3, basLarTra3, 79);
		addLineDownOffset(sill2, sill, (maxIntHau1 - hauteurVantail));
		addLineUpOffset(hauLarTra6, sill2, 105);
		addLineDownOffset(basLarTra5, hauLarTra6, 9);
		addLineUpOffset(hauLarTra5, basLarTra5, 59);
		addLineDownOffset(basLarTra4, hauLarTra5, 9);
		addLineUpOffset(hauLarTra4, basLarTra4, 105);
		addLineUpOffset(hauLarEmpBas, sill2, 251);

		addPart("traverse", "traverse1", posBatGau, posBatDro, top, basLarTra1);
		addPart("traverse", "traverse2", posBatGau, posBatDro, hauLarTra2, basLarTra2);
		addPart("traverse", "traverse3", posBatGau, posBatDro, hauLarTra3, basLarTra3);
		addPart("traverse", "traverse4", posBatGau, posBatDro, hauLarTra4, basLarTra4);
		addPart("traverse", "traverse5", posBatGau, posBatDro, hauLarTra5, basLarTra5);
		addPart("traverse", "traverse6", posBatGau, posBatDro, hauLarTra6, sill2);

		addLineUpOffset(hauAccHau, basLarTra1, 13.5);
		addLineDownOffset(basAccHau, hauLarTra2, 13.5);
		addLineUpOffset(hauAccItr, basLarTra2, 13.5);
		addLineDownOffset(basAccItr, hauLarTra3, 13.5);
		addLineUpOffset(hauAccBas, basLarTra3, 13.5);
		addLineDownOffset(basAccBas, hauLarEmpBas, 13.5);

		addElementSpace("6", posAccGau, posAccDro, hauAccHau, basAccHau);
		addElementSpace("5", posAccDroMno, posAccDro, hauAccItr, basAccItr);
		addElementSpace("4", posAccGau, posAccGauMno, hauAccItr, basAccItr);
		addElementSpace("3", posAccGau, posAccDro, hauAccBas, basAccBas);

		System.out.println("Accessoire haut " + (hDist(hauAccHau, basAccHau) + "*" + vDist(posAccGau, posAccDro)));
		System.out.println("Accessoire interm�diaire gauche " + (hDist(hauAccHau, basAccHau) + "*" + vDist(posAccGau, posAccGauMno)));
		System.out.println("Accessoire interm�diaire droite " + (hDist(hauAccHau, basAccHau) + "*" + vDist(posAccDroMno, posAccDroMno)));
		System.out.println("Accessoire bas " + (hDist(hauAccBas, basAccBas) + "*" + vDist(posAccGau, posAccDro)));

		calculHauteurPlinthe();

	}

	// si la hauteur du vantail est inf�rieure � 2142
	public void _2()
	{
		double hauteurVantail = hDist(top, sill);
		double hauSbs = maxSbs2142 - (maxIntHau2 - hauteurVantail);

		addLineDownOffset(basLarTra1, top, 105);
		addLineDownOffset(hauLarTra2, top, hauteurVantail - hauSbs);
		addLineDownOffset(basLarTra2, hauLarTra2, 79);
		addLineDownOffset(basLarTra3, hauLarTra2, larEmpMilieu2142);
		addLineUpOffset(hauLarTra3, basLarTra3, 79);
		addLineDownOffset(sill2, sill, (maxIntHau2 - hauteurVantail));
		addLineUpOffset(hauLarTra6, sill2, 105);
		addLineDownOffset(basLarTra5, hauLarTra6, 9);
		addLineUpOffset(hauLarTra5, basLarTra5, 59);
		addLineDownOffset(basLarTra4, hauLarTra5, 9);
		addLineUpOffset(hauLarTra4, basLarTra4, 105);
		addLineUpOffset(hauLarEmpBas, sill2, 251);

		addPart("traverse", "traverse1", posBatGau, posBatDro, top, basLarTra1);
		addPart("traverse", "traverse2", posBatGau, posBatDro, hauLarTra2, basLarTra2);
		addPart("traverse", "traverse3", posBatGau, posBatDro, hauLarTra3, basLarTra3);
		addPart("traverse", "traverse4", posBatGau, posBatDro, hauLarTra4, basLarTra4);
		addPart("traverse", "traverse5", posBatGau, posBatDro, hauLarTra5, basLarTra5);
		addPart("traverse", "traverse6", posBatGau, posBatDro, hauLarTra6, sill2);

		addLineUpOffset(hauAccHau, basLarTra1, 13.5);
		addLineDownOffset(basAccHau, hauLarTra2, 13.5);
		addLineUpOffset(hauAccItr, basLarTra2, 13.5);
		addLineDownOffset(basAccItr, hauLarTra3, 13.5);
		addLineUpOffset(hauAccBas, basLarTra3, 13.5);
		addLineDownOffset(basAccBas, hauLarEmpBas, 13.5);

		addElementSpace("6", posAccGau, posAccDro, hauAccHau, basAccHau);
		addElementSpace("5", posAccDroMno, posAccDro, hauAccItr, basAccItr);
		addElementSpace("4", posAccGau, posAccGauMno, hauAccItr, basAccItr);
		addElementSpace("3", posAccGau, posAccDro, hauAccBas, basAccBas);

		System.out.println("Accessoire haut " + (hDist(hauAccHau, basAccHau) + "*" + vDist(posAccGau, posAccDro)));
		System.out.println("Accessoire interm�diaire gauche " + (hDist(hauAccHau, basAccHau) + "*" + vDist(posAccGau, posAccGauMno)));
		System.out.println("Accessoire interm�diaire droite " + (hDist(hauAccHau, basAccHau) + "*" + vDist(posAccDroMno, posAccDroMno)));
		System.out.println("Accessoire bas " + (hDist(hauAccBas, basAccBas) + "*" + vDist(posAccGau, posAccDro)));

		calculHauteurPlinthe();

	}

	// si la hauteur du vantail est inf�rieure � 2242
	public void _3()
	{
		double hauteurVantail = hDist(top, sill);
		double hauSbs = maxSbs2242 - (maxIntHau3 - hauteurVantail);

		addLineDownOffset(basLarTra1, top, 105);
		addLineDownOffset(hauLarTra2, top, hauteurVantail - hauSbs);
		addLineDownOffset(basLarTra2, hauLarTra2, 79);
		addLineDownOffset(basLarTra3, hauLarTra2, larEmpMilieu2242);
		addLineUpOffset(hauLarTra3, basLarTra3, 79);
		addLineDownOffset(sill2, sill, (maxIntHau3 - hauteurVantail));
		addLineDownOffset(basLarTra6, hauLarTra7, 9);
		addLineUpOffset(hauLarTra6, basLarTra6, 63);
		addLineDownOffset(basLarTra5, hauLarTra6, 9);
		addLineUpOffset(hauLarTra5, basLarTra5, 105);
		addLineDownOffset(basLarTra4, hauLarTra5, 9);
		addLineUpOffset(hauLarTra4, basLarTra4, 105);
		addLineUpOffset(hauLarEmpBas, sill2, 351);

		addPart("traverse", "traverse1", posBatGau, posBatDro, top, basLarTra1);
		addPart("traverse", "traverse2", posBatGau, posBatDro, hauLarTra2, basLarTra2);
		addPart("traverse", "traverse3", posBatGau, posBatDro, hauLarTra3, basLarTra3);
		addPart("traverse", "traverse4", posBatGau, posBatDro, hauLarTra4, basLarTra4);
		addPart("traverse", "traverse5", posBatGau, posBatDro, hauLarTra5, basLarTra5);
		addPart("traverse", "traverse6", posBatGau, posBatDro, hauLarTra6, basLarTra6);
		addPart("traverse", "traverse7", posBatGau, posBatDro, hauLarTra7, sill2);

		addLineUpOffset(hauAccHau, basLarTra1, 13.5);
		addLineDownOffset(basAccHau, hauLarTra2, 13.5);
		addLineUpOffset(hauAccItr, basLarTra2, 13.5);
		addLineDownOffset(basAccItr, hauLarTra3, 13.5);
		addLineUpOffset(hauAccBas, basLarTra3, 13.5);
		addLineDownOffset(basAccBas, hauLarEmpBas, 13.5);

		addElementSpace("6", posAccGau, posAccDro, hauAccHau, basAccHau);
		addElementSpace("5", posAccDroMno, posAccDro, hauAccItr, basAccItr);
		addElementSpace("4", posAccGau, posAccGauMno, hauAccItr, basAccItr);
		addElementSpace("3", posAccGau, posAccDro, hauAccBas, basAccBas);

		System.out.println("Accessoire haut " + (hDist(hauAccHau, basAccHau) + "*" + vDist(posAccGau, posAccDro)));
		System.out.println("Accessoire interm�diaire gauche " + (hDist(hauAccHau, basAccHau) + "*" + vDist(posAccGau, posAccGauMno)));
		System.out.println("Accessoire interm�diaire droite " + (hDist(hauAccHau, basAccHau) + "*" + vDist(posAccDroMno, posAccDroMno)));
		System.out.println("Accessoire bas " + (hDist(hauAccBas, basAccBas) + "*" + vDist(posAccGau, posAccDro)));

		calculHauteurPlinthe();

	}

	private void calculHauteurPlinthe()
	{
		if ((hDist(hauLarEmpBas, sill)) >= 201)
		{
			addLineUpOffset(pliBas, sill, 9);
			addLineUpOffset(pliHau, pliBas, 93);
			addLineUpOffset(jdeHau, pliHau, larJde);
		}

		else if ((hDist(hauLarEmpBas, sill)) < 201)
		{
			addLineUpOffset(pliBas, sill, 9);
			addLineUpOffset(pliHau, pliBas, 45);
			addLineUpOffset(jdeHau, pliHau, larJde);
		}

		addElementSpace("1", posPliGau, posPliDro, pliHau, pliBas);
		addElementSpace("2", posJdeGau, posJdeDro, jdeHau, pliHau);
	}
}