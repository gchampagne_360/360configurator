package com.client360.configuration.wad.doormodel.wood;

public class GestionLargeurCampagne extends GestionLargeur
{
	// Chassis � panneau campagne
	public static Double larMntCfpCamp = 55.5;
	public static Double larTraCfpCamp = 55.5;
	public static Double larMnoCfpCamp = 56.0;

	// Chassis � carreaux campagne
	public static Double larMntCfcgCamp = 51.5;
	public static Double larTraCfcgCamp = 51.5;
	public static Double larPbeCfcgCamp = 32.0;
}
