package com.client360.configuration.wad.doormodel.wood;

import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;

public class Torpedo extends HIContemporaine
{
	// hauteurs
	private final double height1 = 1950;
	private final double height2 = 2050;
	private final double height3 = 2150;
	private final double height4 = 2250;

	// largeurs
	private final double width1 = 700;
	private final double width2 = 800;
	private final double width3 = 900;
	private final double width4 = 1000;

	// hauteur de base entre le jet d'eau et la derni�re ligne du motif
	private Double basLigneBase1 = 123.66;
	private Double basLigneBase2 = 127.66;
	private Double basLigneBase3 = 310.27;
	private Double basLigneBase4 = 317.67;

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxHeight(height4); // s'applique avec la r�gle "_4"
		addMaxHeight(height3); // s'applique avec la r�gle "_3"
		addMaxHeight(height2); // s'applique avec la r�gle "_2"
		addMaxHeight(height1); // s'applique avec la r�gle "_1"

		addMaxWidth(width4); // s'applique avec la r�gle "_d"
		addMaxWidth(width3); // s'applique avec la r�gle "_c"
		addMaxWidth(width2); // s'applique avec la r�gle "_b"
		addMaxWidth(width1); // s'applique avec la r�gle "_a"
	}

	@Override
	protected void load()
	{
		addForm(rectangle(left, right, top, posMotifBas), "/images/door_svg/wood/contemporaines/Torpedo_motif.svg", ImageAlign.BOTTOMLEFT,
				ImageScale.HORIZONTAL);
	}

	public void _1()
	{
		addAccessoires();
		addLineUpOffset(posMotifBas, posPlintheHaut, getHauteurMotifBas(vDist(left, right), hDist(top, sill)));
	}

	public void _2()
	{
		addAccessoires();
		addLineUpOffset(posMotifBas, posPlintheHaut, getHauteurMotifBas(vDist(left, right), hDist(top, sill)));
	}

	public void _3()
	{
		addAccessoires();
		addLineUpOffset(posMotifBas, posPlintheHaut, getHauteurMotifBas(vDist(left, right), hDist(top, sill)));
	}

	public void _4()
	{
		addAccessoires();
		addLineUpOffset(posMotifBas, posPlintheHaut, getHauteurMotifBas(vDist(left, right), hDist(top, sill)));
	}

	public void a()
	{

	}

	public void b()
	{

	}

	public void c()
	{

	}

	public void d()
	{

	}

	private double getHauteurMotifBas(double width, double height)
	{
		double A = height / 2.7;
		double D = A + width;
		double C = D - 142 - 100;
		double B = C / 5;
		return B;
	}
}
