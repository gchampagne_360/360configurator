package com.client360.configuration.wad.doormodel.wood;

import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;

public class Rosalie extends HIContemporaine
{
	@Override
	protected void load()
	{
		addAccessoires();
		addForm(rectangle(left, right, top, posJetDeauHaut), "/images/door_svg/wood/contemporaines/Rosalie_motif.svg", ImageAlign.CENTER);
	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();
		addMaxWidth(1000);
		addMaxHeight(2250);
	}

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();
	}
}
