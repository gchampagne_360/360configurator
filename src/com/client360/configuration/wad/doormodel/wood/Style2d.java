package com.client360.configuration.wad.doormodel.wood;

/**
 * @author jjagod Programme qui permet de calculer la position des traverses suivant la hauteur du vantail
 */
public class Style2d extends GestionLargeurStyle
{
	private Double maxIntHau4 = 2242.0;
	private Double maxIntHau3 = 2142.0;
	private Double maxIntHau2 = 2042.0;
	private Double maxIntHau1 = 1942.0;
	private Double minIntHau1 = 1902.0;

	private Double maxSbs2242 = 1165.0;
	private Double maxSbs2142 = 1065.0;
	private Double maxSbs2042 = 965.0;
	private Double maxSbs1942 = 865.0;
	private Double minSbsHau1 = 826.0;
	private Double larJde = 45.0;

	private String hauCim = "hauCim";
	private String basCim = "basCim";
	private Double positionMilieuCimaise = 0.0;

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxHeight(2242); // s'applique avec la r�gle "_5"
		addMaxHeight(2142); // s'applique avec la r�gle "_4"
		addMaxHeight(2042); // s'applique avec la r�gle "_3"
		addMaxHeight(1942); // s'applique avec la r�gle "_2"
		addMaxHeight(1902); // s'applique avec la r�gle "_1"

	}

	// ------------Fabrication des lignes
	// si la hauteur du vantail est inf�rieure � 1902
	public void _1()
	{

		double hauteurVantail = hDist(top, sill);

		double hauSbs;
		if (getParametricMeasure("hauSbs") != null && getParametricMeasure("hauSbs") != 0)
		{
			hauSbs = getParametricMeasure("hauSbs");
			System.out.println(hauSbs + " if");
		}
		else
		{
			hauSbs = minSbsHau1 - (minIntHau1 - hauteurVantail) / 2;
			System.out.println(hauSbs + " else");
		}

		addLineDownOffset(basLarTra1, top, 105);
		addLineDownOffset(hauLarTra2, top, hauteurVantail - hauSbs);
		addLineDownOffset(hauLarTra3, hauLarTra2, 105);
		addLineDownOffset(basLarTra3, hauLarTra3, 105);
		addLineDownOffset(sill2, sill, 39);
		addLineUpOffset(hauLarTra6, sill2, 105);
		addLineUpOffset(hauLarTra5, hauLarTra6, 79);
		addLineUpOffset(hauLarTra4, hauLarTra5, 59);
		addLineUpOffset(hauLarEmpBas, sill2, 225);

		addPart("traverse", "traverse1", posBatGau, posBatDro, top, basLarTra1);
		addPart("traverse", "traverse2", posBatGau, posBatDro, hauLarTra2, hauLarTra3);
		addPart("traverse", "traverse3", posBatGau, posBatDro, hauLarTra3, basLarTra3);
		addPart("traverse", "traverse4", posBatGau, posBatDro, hauLarTra4, hauLarTra5);
		addPart("traverse", "traverse5", posBatGau, posBatDro, hauLarTra5, hauLarTra6);
		addPart("traverse", "traverse6", posBatGau, posBatDro, hauLarTra6, sill2);

		addLineUpOffset(hauAccHau, basLarTra1, 13.5);
		addLineDownOffset(basAccHau, hauLarTra2, 13.5);
		addLineUpOffset(hauAccBas, basLarTra3, 22.5);
		addLineDownOffset(basAccBas, hauLarEmpBas, 13.5);

		addElementSpace("4", posAccGau, posAccDro, hauAccHau, basAccHau);
		addElementSpace("3", posAccGau, posAccDro, hauAccBas, basAccBas);

		System.out.println("Accessoire haut " + (hDist(hauAccHau, basAccHau) + "*" + vDist(posAccGau, posAccDro)));
		System.out.println("Accessoire bas " + (hDist(hauAccBas, basAccBas) + "*" + vDist(posAccGau, posAccDro)));

		calculHauteurPlinthe();

	}

	// si la hauteur du vantail est inf�rieure � 1942
	public void _2()
	{

		double hauteurVantail = hDist(top, sill);
		double hauSbs;

		if (getParametricMeasure("hauSbs") != null && getParametricMeasure("hauSbs") != 0)
		{
			hauSbs = getParametricMeasure("hauSbs");
		}
		else
		{
			hauSbs = maxSbs1942 - (maxIntHau1 - hauteurVantail);
		}

		addLineDownOffset(basLarTra1, top, 105);
		addLineDownOffset(hauLarTra2, top, hauteurVantail - hauSbs);
		addLineDownOffset(hauLarTra3, hauLarTra2, 105);
		addLineDownOffset(basLarTra3, hauLarTra3, 105);
		addLineDownOffset(sill2, sill, (maxIntHau1 - hauteurVantail));
		addLineUpOffset(hauLarTra6, sill2, 105);
		addLineUpOffset(hauLarTra5, hauLarTra6, 59);
		addLineUpOffset(hauLarTra4, hauLarTra5, 79);
		addLineUpOffset(hauLarEmpBas, sill2, 225);

		addPart("traverse", "traverse1", posBatGau, posBatDro, top, basLarTra1);
		addPart("traverse", "traverse2", posBatGau, posBatDro, hauLarTra2, hauLarTra3);
		addPart("traverse", "traverse3", posBatGau, posBatDro, hauLarTra3, basLarTra3);
		addPart("traverse", "traverse4", posBatGau, posBatDro, hauLarTra4, hauLarTra5);
		addPart("traverse", "traverse5", posBatGau, posBatDro, hauLarTra5, hauLarTra6);
		addPart("traverse", "traverse6", posBatGau, posBatDro, hauLarTra6, sill2);

		addLineUpOffset(hauAccHau, basLarTra1, 13.5);
		addLineDownOffset(basAccHau, hauLarTra2, 13.5);
		addLineUpOffset(hauAccBas, basLarTra3, 22.5);
		addLineDownOffset(basAccBas, hauLarEmpBas, 13.5);

		addElementSpace("4", posAccGau, posAccDro, hauAccHau, basAccHau);
		addElementSpace("3", posAccGau, posAccDro, hauAccBas, basAccBas);

		System.out.println("Accessoire haut " + (hDist(hauAccHau, basAccHau) + "*" + vDist(posAccGau, posAccDro)));
		System.out.println("Accessoire bas " + (hDist(hauAccBas, basAccBas) + "*" + vDist(posAccGau, posAccDro)));

		calculHauteurPlinthe();

	}

	// si la hauteur du vantail est inf�rieure � 2042
	public void _3()
	{
		double hauteurVantail = hDist(top, sill);
		double hauSbs;

		if (getParametricMeasure("hauSbs") != null && getParametricMeasure("hauSbs") != 0)
		{
			hauSbs = getParametricMeasure("hauSbs");
		}
		else
		{
			hauSbs = maxSbs2042 - (maxIntHau2 - hauteurVantail);
		}

		addLineDownOffset(basLarTra1, top, 105);
		addLineDownOffset(hauLarTra2, top, hauteurVantail - hauSbs);
		addLineDownOffset(hauLarTra3, hauLarTra2, 105);
		addLineDownOffset(basLarTra3, hauLarTra3, 105);
		addLineDownOffset(sill2, sill, (maxIntHau2 - hauteurVantail));
		addLineUpOffset(hauLarTra6, sill2, 105);
		addLineUpOffset(hauLarTra5, hauLarTra6, 59);
		addLineUpOffset(hauLarTra4, hauLarTra5, 105);
		addLineUpOffset(hauLarEmpBas, sill2, 251);

		addPart("traverse", "traverse1", posBatGau, posBatDro, top, basLarTra1);
		addPart("traverse", "traverse2", posBatGau, posBatDro, hauLarTra2, hauLarTra3);
		addPart("traverse", "traverse3", posBatGau, posBatDro, hauLarTra3, basLarTra3);
		addPart("traverse", "traverse4", posBatGau, posBatDro, hauLarTra4, hauLarTra5);
		addPart("traverse", "traverse5", posBatGau, posBatDro, hauLarTra5, hauLarTra6);
		addPart("traverse", "traverse6", posBatGau, posBatDro, hauLarTra6, sill2);

		addLineUpOffset(hauAccHau, basLarTra1, 13.5);
		addLineDownOffset(basAccHau, hauLarTra2, 13.5);
		addLineUpOffset(hauAccBas, basLarTra3, 22.5);
		addLineDownOffset(basAccBas, hauLarEmpBas, 13.5);

		addElementSpace("4", posAccGau, posAccDro, hauAccHau, basAccHau);
		addElementSpace("3", posAccGau, posAccDro, hauAccBas, basAccBas);

		System.out.println("Accessoire haut " + (hDist(hauAccHau, basAccHau) + "*" + vDist(posAccGau, posAccDro)));
		System.out.println("Accessoire bas " + (hDist(hauAccBas, basAccBas) + "*" + vDist(posAccGau, posAccDro)));

		calculHauteurPlinthe();
	}

	// si la hauteur du vantail est inf�rieure � 2142
	public void _4()
	{
		double hauteurVantail = hDist(top, sill);
		double hauSbs;

		if (getParametricMeasure("hauSbs") != null && getParametricMeasure("hauSbs") != 0)
		{
			hauSbs = getParametricMeasure("hauSbs");
		}
		else
		{
			hauSbs = maxSbs2142 - (maxIntHau3 - hauteurVantail);
		}

		addLineDownOffset(basLarTra1, top, 105);
		addLineDownOffset(hauLarTra2, top, hauteurVantail - hauSbs);
		addLineDownOffset(hauLarTra3, hauLarTra2, 105);
		addLineDownOffset(hauLarTra4, hauLarTra3, 59);
		addLineDownOffset(basLarTra4, hauLarTra4, 105);
		addLineDownOffset(sill2, sill, (maxIntHau3 - hauteurVantail));
		addLineUpOffset(hauLarTra7, sill2, 105);
		addLineUpOffset(hauLarTra6, hauLarTra7, 59);
		addLineUpOffset(hauLarTra5, hauLarTra6, 105);
		addLineUpOffset(hauLarEmpBas, sill2, 251);

		addPart("traverse", "traverse1", posBatGau, posBatDro, top, basLarTra1);
		addPart("traverse", "traverse2", posBatGau, posBatDro, hauLarTra2, hauLarTra3);
		addPart("traverse", "traverse3", posBatGau, posBatDro, hauLarTra3, hauLarTra4);
		addPart("traverse", "traverse4", posBatGau, posBatDro, hauLarTra4, basLarTra4);
		addPart("traverse", "traverse5", posBatGau, posBatDro, hauLarTra5, hauLarTra6);
		addPart("traverse", "traverse6", posBatGau, posBatDro, hauLarTra6, hauLarTra7);
		addPart("traverse", "traverse7", posBatGau, posBatDro, hauLarTra7, sill2);

		addLineUpOffset(hauAccHau, basLarTra1, 13.5);
		addLineDownOffset(basAccHau, hauLarTra2, 13.5);
		addLineUpOffset(hauAccBas, basLarTra4, 31.5);
		addLineDownOffset(basAccBas, hauLarEmpBas, 13.5);

		addElementSpace("4", posAccGau, posAccDro, hauAccHau, basAccHau);
		addElementSpace("3", posAccGau, posAccDro, hauAccBas, basAccBas);

		System.out.println("Accessoire haut " + (hDist(hauAccHau, basAccHau) + "*" + vDist(posAccGau, posAccDro)));
		System.out.println("Accessoire bas " + (hDist(hauAccBas, basAccBas) + "*" + vDist(posAccGau, posAccDro)));

		calculHauteurPlinthe();

	}

	// si la hauteur du vantail est inf�rieure � 2242
	public void _5()
	{
		double hauteurVantail = hDist(top, sill);
		double hauSbs;

		if (getParametricMeasure("hauSbs") != null && getParametricMeasure("hauSbs") != 0)
		{
			hauSbs = getParametricMeasure("hauSbs");
		}
		else
		{
			hauSbs = maxSbs2242 - (maxIntHau4 - hauteurVantail);
		}

		addLineDownOffset(basLarTra1, top, 105);
		addLineDownOffset(hauLarTra2, top, hauteurVantail - hauSbs);
		addLineDownOffset(hauLarTra3, hauLarTra2, 105);
		addLineDownOffset(hauLarTra4, hauLarTra3, 59);
		addLineDownOffset(basLarTra4, hauLarTra4, 105);
		addLineDownOffset(sill2, sill, (maxIntHau4 - hauteurVantail));
		addLineUpOffset(hauLarTra8, sill2, 105);
		addLineUpOffset(hauLarTra7, hauLarTra8, 63);
		addLineUpOffset(hauLarTra6, hauLarTra7, 105);
		addLineUpOffset(hauLarTra5, hauLarTra6, 105);
		addLineUpOffset(hauLarEmpBas, sill2, 351);

		addPart("traverse", "traverse1", posBatGau, posBatDro, top, basLarTra1);
		addPart("traverse", "traverse2", posBatGau, posBatDro, hauLarTra2, hauLarTra3);
		addPart("traverse", "traverse3", posBatGau, posBatDro, hauLarTra3, hauLarTra4);
		addPart("traverse", "traverse4", posBatGau, posBatDro, hauLarTra4, basLarTra4);
		addPart("traverse", "traverse5", posBatGau, posBatDro, hauLarTra5, hauLarTra6);
		addPart("traverse", "traverse6", posBatGau, posBatDro, hauLarTra6, hauLarTra7);
		addPart("traverse", "traverse7", posBatGau, posBatDro, hauLarTra7, hauLarTra8);
		addPart("traverse", "traverse8", posBatGau, posBatDro, hauLarTra8, sill2);

		addLineUpOffset(hauAccHau, basLarTra1, 13.5);
		addLineDownOffset(basAccHau, hauLarTra2, 13.5);
		addLineUpOffset(hauAccBas, basLarTra4, 31.5);
		addLineDownOffset(basAccBas, hauLarEmpBas, 13.5);

		addElementSpace("4", posAccGau, posAccDro, hauAccHau, basAccHau);
		addElementSpace("3", posAccGau, posAccDro, hauAccBas, basAccBas);

		System.out.println("Accessoire haut " + (hDist(hauAccHau, basAccHau) + "*" + vDist(posAccGau, posAccDro)));
		System.out.println("Accessoire bas " + (hDist(hauAccBas, basAccBas) + "*" + vDist(posAccGau, posAccDro)));

		calculHauteurPlinthe();

	}

	private void calculHauteurPlinthe()
	{
		if ((hDist(hauLarEmpBas, sill)) >= 201)
		{
			addLineUpOffset(pliBas, sill, 9);
			addLineUpOffset(pliHau, pliBas, 93);
			addLineUpOffset(jdeHau, pliHau, larJde);

		}

		else if ((hDist(hauLarEmpBas, sill)) < 201)
		{
			addLineUpOffset(pliBas, sill, 9);
			addLineUpOffset(pliHau, pliBas, 45);
			addLineUpOffset(jdeHau, pliHau, larJde);

		}

		positionMilieuCimaise = ((hDist(hauAccBas, basAccHau) / 2) - 50);

		addLineDownOffset(hauCim, basAccHau, positionMilieuCimaise);
		addLineDownOffset(basCim, hauCim, 100);

		addElementSpace("1", posPliGau, posPliDro, pliHau, pliBas);
		addElementSpace("2", posJdeGau, posJdeDro, jdeHau, pliHau);
		addElementSpace("5", posAccGau, posAccDro, hauCim, basCim);

	}

}
