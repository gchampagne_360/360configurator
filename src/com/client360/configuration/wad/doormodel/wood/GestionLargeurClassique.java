package com.client360.configuration.wad.doormodel.wood;

/**
 * @author jjagod Programme qui permet de calculer la largeur des battants pour un vantail principal
 * 
 */
public class GestionLargeurClassique extends GestionLargeur
{
	// Chassis � panneau classique
	public static Double larMntCfpCl = 55.5;
	public static Double larTraCfpCl = 55.5;
	public static Double larMnoCfpCl = 56.0;

	// Chassis � carreaux classique
	public static Double larMntCfcgCl = 55.5;
	public static Double larTraCfcgCl = 55.5;
	public static Double larTraItrCfpCl = 56.0;

	protected String posFouGau = "posFouGau";
	protected String posFouDro = "posFouDro";

	private Double larFou = 50.0;

	// -------------Fabrication des colonnes
	// Si la largeur du vantail est inf�rieure � 1000
	@Override
	public void c()
	{
		super.c();
		double largeurVantail = vDist(patternRight, patternLeft);

		if (largeurVantail >= minIntLar2)
		{
			addLineRightOffset(posFouGau, posBatGau, larFou);
			addLineLeftOffset(posFouDro, posBatDro, larFou);

			// TODO Pourrait-on faire mieux?...
			getVConstructionLine().remove(posAccGau);
			addLineLeftOffset(posAccGau, posFouGau, 13.5);
			getVConstructionLine().remove(posAccDro);
			addLineRightOffset(posAccDro, posFouDro, 13.5);
		}
	}
}
