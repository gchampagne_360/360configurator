package com.client360.configuration.wad.doormodel.wood;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.doormodel.accessoire.OculusRond;
import com.client360.configuration.wad.doormodel.accessoire.OculusTriangleBasDroit;
import com.client360.configuration.wad.doormodel.accessoire.OculusTriangleBasGauche;
import com.client360.configuration.wad.doormodel.accessoire.OculusTriangleHautDroit;
import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class HIContemporaine extends ParametricModel
{
	protected final double hauteurPlinthe = 84;
	protected final double hauteurJetDeau = 9;

	// horizontal lines
	public String posPlintheHaut = "posPlintheHaut";
	public String posJetDeauHaut = "posJetDeauHaut";
	public String posMotifHaut = "posMotifHaut";
	public String posMotifBas = "posMotifBas";

	@Override
	protected void load()
	{

	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

	protected void addAccessoires()
	{
		addLineUpOffset(posPlintheHaut, sill, hauteurPlinthe);
		addLineUpOffset(posJetDeauHaut, posPlintheHaut, hauteurJetDeau);
		addElementSpace("1", left, right, posPlintheHaut, sill);
		addElementSpace("2", left, right, posJetDeauHaut, posPlintheHaut);
	}

	protected void addOculus()
	{
		// un oculus rond bois/inox
		double oculusRondLargeur = 390;
		double espaceOcRond = (vDist(left, right) - oculusRondLargeur) / 2;
		addLineDownOffset("ocRondHaut", top, espaceOcRond);
		addLineDownOffset("ocRondBas", top, espaceOcRond + oculusRondLargeur);
		addLineRightOffset("ocRondGauche", left, espaceOcRond);
		addLineLeftOffset("ocRondDroit", right, espaceOcRond);

		addElementSpace(OculusRond.class.toString(), "ocRondGauche", "ocRondDroit", "ocRondHaut", "ocRondBas");
		// addElementSpace(Oculus.UNOCULUSRONDINOX.toString(), "ocRondGauche", "ocRondDroit", "ocRondHaut", "ocRondBas");

		// oculus triangle haut/bas
		double oculusTriangleLargeur = 642;
		double espaceOcTriangle = 90;
		addLineDownOffset("ocTrHautHaut", top, espaceOcTriangle);
		addLineDownOffset("ocTrHautBas", top, espaceOcTriangle + oculusTriangleLargeur);
		addLineLeftOffset("ocTrHautGauche", right, espaceOcTriangle + oculusTriangleLargeur);
		addLineLeftOffset("ocTrHautDroit", right, espaceOcTriangle);

		addLineUpOffset("ocTrBasDroitBas", posJetDeauHaut, espaceOcTriangle);
		addLineUpOffset("ocTrBasDroitHaut", posJetDeauHaut, espaceOcTriangle + oculusTriangleLargeur);
		addLineLeftOffset("ocTrBasDroitGauche", right, espaceOcTriangle + oculusTriangleLargeur);
		addLineLeftOffset("ocTrBasDroitDroit", right, espaceOcTriangle);

		addLineUpOffset("ocTrBasGaucheBas", posJetDeauHaut, espaceOcTriangle);
		addLineUpOffset("ocTrBasGaucheHaut", posJetDeauHaut, espaceOcTriangle + oculusTriangleLargeur);
		addLineRightOffset("ocTrBasGaucheGauche", left, espaceOcTriangle);
		addLineRightOffset("ocTrBasGaucheDroit", left, espaceOcTriangle + oculusTriangleLargeur);

		addElementSpace(OculusTriangleHautDroit.class.toString(), "ocTrHautGauche", "ocTrHautDroit", "ocTrHautHaut", "ocTrHautBas");
		addElementSpace(OculusTriangleBasDroit.class.toString(), "ocTrBasDroitGauche", "ocTrBasDroitDroit", "ocTrBasDroitHaut", "ocTrBasDroitBas");
		addElementSpace(OculusTriangleBasGauche.class.toString(), "ocTrBasGaucheGauche", "ocTrBasGaucheDroit", "ocTrBasGaucheHaut", "ocTrBasGaucheBas");
	}

}
