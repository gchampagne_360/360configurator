package com.client360.configuration.wad.doormodel.wood;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;


public class Samba1 extends HIContemporaine
{
	// hauteurs
	private final double height1 = 1940;
	private final double height2 = 2090;
	private final double height3 = 2230;
	private final double height4 = 2250;

	// largeurs
	private final double width1 = 800;
	private final double width2 = 900;
	private final double width3 = 1000;

	@Override
	protected void load()
	{
		addAccessoires();
		addForm(rectangle(left, right, top, posJetDeauHaut), "/images/door_svg/wood/contemporaines/Samba1_motif.svg", ImageAlign.CENTERRIGHT);
	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxHeight(height4); // s'applique avec la r�gle "_4"
		addMaxHeight(height3); // s'applique avec la r�gle "_3"
		addMaxHeight(height2); // s'applique avec la r�gle "_2"
		addMaxHeight(height1); // s'applique avec la r�gle "_1"

		addMaxWidth(width3); // s'applique avec la r�gle "_c"
		addMaxWidth(width2); // s'applique avec la r�gle "_b"
		addMaxWidth(width1); // s'applique avec la r�gle "_a"
	}

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();
	}
}
