package com.client360.configuration.wad.doormodel.wood;

import com.netappsid.commonutils.math.MathFunction;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;

public class Apollo1 extends HIContemporaine
{
	// hauteur de base entre le top et la premi�re ligne du motif
	private Double hautLigneBase1 = 111.25;
	private Double hautLigneBase2 = 115.25;
	private Double hautLigneBase3 = 114.25;
	private Double hautLigneBase4 = 53.25;

	// hauteur de base entre le sill et la derni�re ligne du motif
	private Double basLigneBase1 = 123.66;
	private Double basLigneBase2 = 127.66;
	private Double basLigneBase3 = 126.66;
	private Double basLigneBase4 = 65.66;

	// hauteurs
	private final double height1 = 1940;
	private final double height2 = 2090;
	private final double height3 = 2230;
	private final double height4 = 2250;

	// largeurs
	private final double width1 = 779;
	private final double width2 = 879;
	private final double width3 = 1000;

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxHeight(height4); // s'applique avec la r�gle "_4"
		addMaxHeight(height3); // s'applique avec la r�gle "_3"
		addMaxHeight(height2); // s'applique avec la r�gle "_2"
		addMaxHeight(height1); // s'applique avec la r�gle "_1"

		addMaxWidth(width3); // s'applique avec la r�gle "_c"
		addMaxWidth(width2); // s'applique avec la r�gle "_b"
		addMaxWidth(width1); // s'applique avec la r�gle "_a"
	}

	@Override
	protected void load()
	{
		addForm(rectangle(left, right, posMotifHaut, posMotifBas), "/images/door_svg/wood/contemporaines/Apollo1_motif.svg", ImageAlign.TOPCENTER);
	}

	public void _1()
	{
		addAccessoires();
		setPosMotifHaut(height1, hautLigneBase1);
		setPosMotifBas(height1, basLigneBase1);
	}

	public void _2()
	{
		addAccessoires();
		setPosMotifHaut(height2, hautLigneBase2);
		setPosMotifBas(height2, basLigneBase2);
	}

	public void _3()
	{
		addAccessoires();
		setPosMotifHaut(height3, hautLigneBase3);
		setPosMotifBas(height3, basLigneBase3);
	}

	public void _4()
	{
		addAccessoires();
		setPosMotifHaut(height4, hautLigneBase4);
		setPosMotifBas(height4, basLigneBase4);
	}

	public void a()
	{

	}

	public void b()
	{

	}

	public void c()
	{

	}

	private void setPosMotifHaut(double baseHeight, double baseOffset)
	{
		double height = hDist(top, sill);
		double baseDifference = baseHeight - height;
		double variableOffset = MathFunction.round(baseDifference / 10, 0) * -5;
		double offset = baseOffset + variableOffset;
		addLineDownOffset(posMotifHaut, top, offset);
	}

	private void setPosMotifBas(double baseHeight, double baseOffset)
	{
		double height = hDist(top, sill);
		double baseDifference = baseHeight - height;
		double variableOffset = MathFunction.round(baseDifference / 10, 0) * -5;
		double offset = baseOffset + variableOffset;
		addLineUpOffset(posMotifBas, posJetDeauHaut, offset);
	}

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();
		super.addOculus();

		/*
		 * double oculusRondLargeur = 390; double espace = (vDist(left, right) - 390) / 2; addLineDownOffset("ocHaut", top, espace); addLineDownOffset("ocBas",
		 * top, espace + oculusRondLargeur); addLineRightOffset("ocGauche", left, espace); addLineLeftOffset("ocDroit", right, espace);
		 * addElementSpace(Oculus.UNOCULUSROND.toString(), "ocGauche", "ocDroit", "ocHaut", "ocBas");
		 */
	}

}
