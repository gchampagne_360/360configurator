package com.client360.configuration.wad.doormodel;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class Slab1SubModel extends ParametricModel
{

	@Override
	protected Unit<Length> getUnit()
	{

		return Units.INCH;
	}

	@Override
	protected void load()
	{

		addGaps(5d + 1d / 8d, 23d);

		addLineDownOffset(1, patternTop, 43d + 7 / 16d);
		addLineDownOffset(2, 1, 21d + 1d / 2d);

		addLineRightOffset(3, patternLeft, 0d);
		addLineRightOffset(4, patternRight, 0d);

		addElementSpace("ACC1", 3, 4, 1, 2);

	}

}
