package com.client360.configuration.wad.doormodel.accessoires;

import com.client360.configuration.wad.profiles.ProfileMoulure3DA;
import com.client360.configuration.wad.profiles.ProfileMoulure3DB;
import com.client360.configuration.wad.profiles.ProfilePorteBoisA;
import com.client360.configuration.wad.profiles.ProfilePorteBoisB;

public class CfpSt1PanRec extends GestionLargeurStyle
{
	protected String petitEmbosseGauche = "petitEmbosseGauche";
	protected String petitEmbosseDroit = "petitEmbosseDroit";
	protected String petitEmbosseHaut = "petitEmbosseHaut";
	protected String petitEmbosseBas = "petitEmbosseBas";

	@Override
	protected void load()
	{
		addLineRightOffset(petitEmbosseGauche, left, larMntCfpSt);
		addLineDownOffset(petitEmbosseHaut, top, larTraCfpSt);
		addLineLeftOffset(petitEmbosseDroit, right, larMntCfpSt);
		addLineUpOffset(petitEmbosseBas, sill, larTraCfpSt);

		addForm(rectangle(left, right, top, sill));
		addSpacer(larMntCfpSt, larTraCfpSt, larMntCfpSt, larTraCfpSt);
		addProfile(new ProfileMoulure3DA(), new ProfileMoulure3DB());

		addForm(rectangle(petitEmbosseGauche, petitEmbosseDroit, petitEmbosseHaut, petitEmbosseBas));
		addSpacer(larPlateBandeVisible, larPlateBandeVisible, larPlateBandeVisible, larPlateBandeVisible);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
	}
}
