package com.client360.configuration.wad.doormodel.accessoires;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.profiles.ProfilePorteBoisA;
import com.client360.configuration.wad.profiles.ProfilePorteBoisB;
import com.netappsid.commonutils.math.Units;

/**
 * 
 * @author Gwenael Bonneau
 * @since 1.2 Migration addForm(rectangle addForm(rectangle 2011/9/9
 * 
 */
public class PanneauMoucHoz extends GestionLargeur
{

	private Double entreAxeMouchette = 160.0;
	private Double nbMouchette = 0.0;

	private int epaisseur = 2;

	@Override
	protected void load()
	{
		int vraiNbMouchettes = 0;
		nbMouchette = Math.ceil(hDist(patternTop, patternSill) / entreAxeMouchette);

		// Lignes communes: peu importe le nombre de mouchettes, j'aurai toujours ces lignes.
		addLineUpOffset("milieu", hPatternCenter, 0);
		addLineUpOffset("h1", "milieu", entreAxeMouchette);
		addLineDownOffset("b1", "milieu", entreAxeMouchette);

		// Lignes sp�cifiques. Plus de mouchettes = plus de divisions � faire.
		if (nbMouchette > 4)
		{
			addLineUpOffset("h2", "milieu", entreAxeMouchette * 2);
			addLineDownOffset("b2", "milieu", entreAxeMouchette * 2);
		}
		if (nbMouchette > 6)
		{
			addLineUpOffset("h3", "milieu", entreAxeMouchette * 3);
			addLineDownOffset("b3", "milieu", entreAxeMouchette * 3);
		}
		if (nbMouchette > 8)
		{
			addLineUpOffset("h4", "milieu", entreAxeMouchette * 4);
			addLineDownOffset("b4", "milieu", entreAxeMouchette * 4);
		}
		if (nbMouchette > 10)
		{
			addLineUpOffset("h5", "milieu", entreAxeMouchette * 5);
			addLineDownOffset("b5", "milieu", entreAxeMouchette * 5);
		}
		if (nbMouchette > 12)
		{
			addLineUpOffset("h6", "milieu", entreAxeMouchette * 6);
			addLineDownOffset("b6", "milieu", entreAxeMouchette * 6);
		}

		// Dessin actuel des rectangles. Propre � chaque nombre de mouchettes.
		// Une bouche aurait pu �tre faite � ce stade pour rendre le tout g�n�rique, mais ceci aurait r�duit la clart� du code.
		if (nbMouchette <= 4)
		{
			addForm(rectangle(patternLeft, patternRight, patternTop, "h1"));
			addForm(rectangle(patternLeft, patternRight, "h1", "milieu"));
			addForm(rectangle(patternLeft, patternRight, "milieu", "b1"));
			addForm(rectangle(patternLeft, patternRight, "b1", patternSill));
			vraiNbMouchettes = 4;
		}
		else if (nbMouchette > 4 && nbMouchette <= 6)
		{
			addForm(rectangle(patternLeft, patternRight, patternTop, "h2"));
			addForm(rectangle(patternLeft, patternRight, "h2", "h1"));
			addForm(rectangle(patternLeft, patternRight, "h1", "milieu"));
			addForm(rectangle(patternLeft, patternRight, "milieu", "b1"));
			addForm(rectangle(patternLeft, patternRight, "b1", "b2"));
			addForm(rectangle(patternLeft, patternRight, "b2", patternSill));
			vraiNbMouchettes = 6;
		}
		else if (nbMouchette > 6 && nbMouchette <= 8)
		{
			addForm(rectangle(patternLeft, patternRight, patternTop, "h3"));
			addForm(rectangle(patternLeft, patternRight, "h3", "h2"));
			addForm(rectangle(patternLeft, patternRight, "h2", "h1"));
			addForm(rectangle(patternLeft, patternRight, "h1", "milieu"));
			addForm(rectangle(patternLeft, patternRight, "milieu", "b1"));
			addForm(rectangle(patternLeft, patternRight, "b1", "b2"));
			addForm(rectangle(patternLeft, patternRight, "b2", "b3"));
			addForm(rectangle(patternLeft, patternRight, "b3", patternSill));
			vraiNbMouchettes = 8;
		}
		else if (nbMouchette > 8 && nbMouchette <= 10)
		{
			addForm(rectangle(patternLeft, patternRight, patternTop, "h4"));
			addForm(rectangle(patternLeft, patternRight, "h4", "h3"));
			addForm(rectangle(patternLeft, patternRight, "h3", "h2"));
			addForm(rectangle(patternLeft, patternRight, "h2", "h1"));
			addForm(rectangle(patternLeft, patternRight, "h1", "milieu"));
			addForm(rectangle(patternLeft, patternRight, "milieu", "b1"));
			addForm(rectangle(patternLeft, patternRight, "b1", "b2"));
			addForm(rectangle(patternLeft, patternRight, "b2", "b3"));
			addForm(rectangle(patternLeft, patternRight, "b3", "b4"));
			addForm(rectangle(patternLeft, patternRight, "b4", patternSill));
			vraiNbMouchettes = 10;
		}
		else if (nbMouchette > 10 && nbMouchette <= 12)
		{
			addForm(rectangle(patternLeft, patternRight, patternTop, "h5"));
			addForm(rectangle(patternLeft, patternRight, "h5", "h4"));
			addForm(rectangle(patternLeft, patternRight, "h4", "h3"));
			addForm(rectangle(patternLeft, patternRight, "h3", "h2"));
			addForm(rectangle(patternLeft, patternRight, "h2", "h1"));
			addForm(rectangle(patternLeft, patternRight, "h1", "milieu"));
			addForm(rectangle(patternLeft, patternRight, "milieu", "b1"));
			addForm(rectangle(patternLeft, patternRight, "b1", "b2"));
			addForm(rectangle(patternLeft, patternRight, "b2", "b3"));
			addForm(rectangle(patternLeft, patternRight, "b3", "b4"));
			addForm(rectangle(patternLeft, patternRight, "b4", "b5"));
			addForm(rectangle(patternLeft, patternRight, "b5", patternSill));
			vraiNbMouchettes = 12;
		}
		else if (nbMouchette > 12 && nbMouchette <= 14)
		{
			addForm(rectangle(patternLeft, patternRight, patternTop, "h6"));
			addForm(rectangle(patternLeft, patternRight, "h6", "h5"));
			addForm(rectangle(patternLeft, patternRight, "h5", "h4"));
			addForm(rectangle(patternLeft, patternRight, "h4", "h3"));
			addForm(rectangle(patternLeft, patternRight, "h3", "h2"));
			addForm(rectangle(patternLeft, patternRight, "h2", "h1"));
			addForm(rectangle(patternLeft, patternRight, "h1", "milieu"));
			addForm(rectangle(patternLeft, patternRight, "milieu", "b1"));
			addForm(rectangle(patternLeft, patternRight, "b1", "b2"));
			addForm(rectangle(patternLeft, patternRight, "b2", "b3"));
			addForm(rectangle(patternLeft, patternRight, "b3", "b4"));
			addForm(rectangle(patternLeft, patternRight, "b4", "b5"));
			addForm(rectangle(patternLeft, patternRight, "b5", "b6"));
			addForm(rectangle(patternLeft, patternRight, "b6", patternSill));
			vraiNbMouchettes = 12;
		}

		// Un spacer et une paire de profil�s pour chaque mouchette � dessiner.
		for (int i = 0; i < vraiNbMouchettes; i++)
		{
			addSpacer(epaisseur, epaisseur, epaisseur, epaisseur);
			addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
			addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
		}

		// Et enfin, le rectangle ext�rieur.
		addForm(rectangle(patternLeft, patternRight, patternTop, patternSill));
		addSpacer(1, 1, 1, 1);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}
}
