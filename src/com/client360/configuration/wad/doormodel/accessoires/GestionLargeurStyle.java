package com.client360.configuration.wad.doormodel.accessoires;

public class GestionLargeurStyle extends GestionLargeur
{
	// Chassis � panneau style
	public static Double larMntCfpSt = 55.5;
	public static Double larTraCfpSt = 55.5;
	public static Double larMnoCfpSt = 56.0;

	// Chassis � panneau pallas 36 mm
	public static Double larMntCfpPallas36 = 55.5;
	public static Double larTraCfpPallas36 = 55.5;
	public static Double larMnoCfpPallas36 = 96.0;
	public static Double larTraItrCfpPallas36 = 96.0;

	// Chassis � panneau pallas 47 mm
	public static Double larMntCfpPallas47 = 55.5;
	public static Double larTraCfpPallas47 = 55.5;
	public static Double larMnoCfpPallas47 = 96.0;
	public static Double larTraItrCfpPallas47 = 96.0;

	// Chassis � carreaux style
	public static Double larMntCfcgSt = 55.5;
	public static Double larTraCfcgSt = 55.5;
	public static Double larPbeCfcgSt = 32.0;

}
