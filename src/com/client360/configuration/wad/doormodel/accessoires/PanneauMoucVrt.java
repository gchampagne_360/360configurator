package com.client360.configuration.wad.doormodel.accessoires;

import com.client360.configuration.wad.profiles.ProfilePorteBoisA;
import com.client360.configuration.wad.profiles.ProfilePorteBoisB;

public class PanneauMoucVrt extends GestionLargeur
{

	private Double entreAxeMouchette = 80.0;
	private Double nbMouchette = 0.0;

	private int epaisseur = 2;

	@Override
	protected void load()
	{
		int vraiNbMouchettes = 0;
		nbMouchette = Math.ceil(vDist(patternLeft, patternRight) / entreAxeMouchette);

		// Lignes communes: peu importe le nombre de mouchettes, j'aurai toujours ces lignes.
		addLineLeftOffset("milieu", vPatternCenter, 0);
		addLineLeftOffset("g1", "milieu", entreAxeMouchette);
		addLineRightOffset("d1", "milieu", entreAxeMouchette);

		// Lignes sp�cifiques. Plus de mouchettes = plus de divisions � faire.
		if (nbMouchette > 4)
		{
			addLineLeftOffset("g2", "milieu", entreAxeMouchette * 2);
			addLineRightOffset("d2", "milieu", entreAxeMouchette * 2);
		}
		if (nbMouchette > 6)
		{
			addLineLeftOffset("g3", "milieu", entreAxeMouchette * 3);
			addLineRightOffset("d3", "milieu", entreAxeMouchette * 3);
		}
		if (nbMouchette > 8)
		{
			addLineLeftOffset("g4", "milieu", entreAxeMouchette * 4);
			addLineRightOffset("d4", "milieu", entreAxeMouchette * 4);
		}

		// Dessin actuel des rectangles. Propre � chaque nombre de mouchettes.
		// Une bouche aurait pu �tre faite � ce stade pour rendre le tout g�n�rique, mais ceci aurait r�duit la clart� du code.
		if (nbMouchette <= 4)
		{
			addForm(rectangle(patternLeft, "g1", patternTop, patternSill));
			addForm(rectangle("g1", "milieu", patternTop, patternSill));
			addForm(rectangle("milieu", "d1", patternTop, patternSill));
			addForm(rectangle("d1", patternRight, patternTop, patternSill));
			vraiNbMouchettes = 4;
		}
		else if (nbMouchette > 4 && nbMouchette <= 6)
		{
			addForm(rectangle(patternLeft, "g2", patternTop, patternSill));
			addForm(rectangle("g2", "g1", patternTop, patternSill));
			addForm(rectangle("g1", "milieu", patternTop, patternSill));
			addForm(rectangle("milieu", "d1", patternTop, patternSill));
			addForm(rectangle("d1", "d2", patternTop, patternSill));
			addForm(rectangle("d2", patternRight, patternTop, patternSill));
			vraiNbMouchettes = 6;
		}
		else if (nbMouchette > 6 && nbMouchette <= 8)
		{
			addForm(rectangle(patternLeft, "g3", patternTop, patternSill));
			addForm(rectangle("g3", "g2", patternTop, patternSill));
			addForm(rectangle("g2", "g1", patternTop, patternSill));
			addForm(rectangle("g1", "milieu", patternTop, patternSill));
			addForm(rectangle("milieu", "d1", patternTop, patternSill));
			addForm(rectangle("d1", "d2", patternTop, patternSill));
			addForm(rectangle("d2", "d3", patternTop, patternSill));
			addForm(rectangle("d3", patternRight, patternTop, patternSill));
			vraiNbMouchettes = 8;
		}
		else if (nbMouchette > 8 && nbMouchette <= 10)
		{
			addForm(rectangle(patternLeft, "g4", patternTop, patternSill));
			addForm(rectangle("g4", "g3", patternTop, patternSill));
			addForm(rectangle("g3", "g2", patternTop, patternSill));
			addForm(rectangle("g2", "g1", patternTop, patternSill));
			addForm(rectangle("g1", "milieu", patternTop, patternSill));
			addForm(rectangle("milieu", "d1", patternTop, patternSill));
			addForm(rectangle("d1", "d2", patternTop, patternSill));
			addForm(rectangle("d2", "d3", patternTop, patternSill));
			addForm(rectangle("d3", "d4", patternTop, patternSill));
			addForm(rectangle("d4", patternRight, patternTop, patternSill));
			vraiNbMouchettes = 10;
		}

		// Un spacer et une paire de profil�s pour chaque mouchette � dessiner.
		for (int i = 0; i < vraiNbMouchettes; i++)
		{
			addSpacer(epaisseur, epaisseur, epaisseur, epaisseur);
			addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
			setInsulatedGlass(false);
		}

		// Et enfin, le rectangle ext�rieur.
		addForm(rectangle(patternLeft, patternRight, patternTop, patternSill));
		addSpacer(1, 1, 1, 1);
		addProfile(new ProfilePorteBoisA(), new ProfilePorteBoisB());
		setInsulatedGlass(false);
	}
}
