package com.client360.configuration.wad.doormodel;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class WoodDoor extends ParametricModel
{
	@Override
	protected Unit<Length> getUnit()
	{

		return Units.MM;
	}

	@Override
	public void matrixDefinition()
	{

		addMaxWidth(915);
		addMaxHeight(1800);
		addMaxWidth(715);
		addMaxHeight(1600);
		addMaxWidth(815);
		addMaxHeight(1980);
	}

	@Override
	protected void load()
	{

		createDataSource();
		getDataSource();
		getParts();
		getPartsByGroup("traverse");
		getPartByName("traverse1");

	}

	// ------------------------Tourillon
	private void createDataSource()
	{
		addLineDownOffset("tour1", patternTop, 50);
		addDataSource("tour1");
		addLineDownOffset("tour2", patternTop, 100);
		addDataSource("tour2");
		addLineDownOffset("tour3", patternTop, 300);
		addDataSource("tour3");
		addLineDownOffset("tour4", patternTop, 400);
		addDataSource("tour4");

	}

	// ---------------------------------------------------
	public void b2Before()
	{
		addFormRectangle(left, right, top, sill);
		addSpacer(49, 49, 49, 49);
	}

	public void b2After()
	{
		addFormRectangle(10, 11, 1, 2);
		addSpacer(49, 49, 49, 49);
	}

	// public void b2Override()
	// {
	// addLineDownOffset(1, patternTop, 150);
	// addLineDownOffset(2, 1, hDist(top, sill) * 0.45);
	// addLineDownOffset(3, 2, 75);
	//
	// addLineUpOffset(5, sill, 75);
	// addLineUpOffset(4, 5, 75);
	// }

	// ------------------------colonne
	public void a()
	{
		addLineRightOffset(10, left, vDist(right, left) * 0.15);
		addLineLeftOffset(11, right, vDist(right, left) * 0.15);
	}

	public void b()
	{
		addLineRightOffset(10, left, 100);
		addLineLeftOffset(11, right, 100);
	}

	public void c()
	{
		addLineRightOffset(10, left, 150);
		addLineLeftOffset(11, right, 150);
	}

	// ------------------------------Ligne
	public void _1()
	{
		addLineDownOffset(1, patternTop, 150);
		addLineDownOffset(2, 1, hDist(top, sill) * 0.45);
		addLineDownOffset(3, 2, 75);

		addLineUpOffset(5, sill, 75);
		addLineUpOffset(4, 5, 75);

		addPart("traverse", "traverse1", 10, 11, patternTop, 1);
		addPart("traverse", "traverse2", 10, 11, 2, 3);
		addPart("traverse", "traverse3", 10, 11, 4, 5);
		addPart("traverse", "traverse4", 10, 11, 5, sill);

		addPart("montant", "montant1", patternLeft, 10, patternTop, sill);
		addPart("montant", "montant2", 11, patternRight, patternTop, sill);

	}

	public void _2()
	{
		addLineDownOffset(1, patternTop, 150);
		addLineDownOffset(2, 1, 650);
		addLineDownOffset(3, 2, 75);
		addLineDownOffset(4, 3, 75);

		addLineUpOffset(6, sill, 75);
		addLineUpOffset(5, 6, 75);

		addPart("traverse", "traverse1", 10, 11, patternTop, 1);
		addPart("traverse", "traverse2", 10, 11, 2, 3);
		addPart("traverse", "traverse3", 10, 11, 3, 4);
		addPart("traverse", "traverse4", 10, 11, 5, 6);
		addPart("traverse", "traverse5", 10, 11, 6, sill);

		addPart("montant", "montant1", patternLeft, 10, patternTop, sill);
		addPart("montant", "montant2", 11, patternRight, patternTop, sill);

	}

	public void _3()
	{
		addLineDownOffset(1, patternTop, 150);
		addLineDownOffset(2, 1, 840);
		addLineDownOffset(3, 2, 75);
		addLineDownOffset(4, 3, 75);

		addLineUpOffset(7, sill, 75);
		addLineUpOffset(6, 7, 75);
		addLineUpOffset(5, 6, 75);

		addPart("traverse", "traverse1", 10, 11, patternTop, 1);
		addPart("traverse", "traverse2", 10, 11, 2, 3);
		addPart("traverse", "traverse3", 10, 11, 3, 4);
		addPart("traverse", "traverse4", 10, 11, 5, 6);
		addPart("traverse", "traverse5", 10, 11, 6, 7);
		addPart("traverse", "traverse6", 10, 11, 7, sill);

		addPart("montant", "montant1", patternLeft, 10, patternTop, sill);
		addPart("montant", "montant2", 11, patternRight, patternTop, sill);

	}
}
