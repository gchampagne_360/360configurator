package com.client360.configuration.wad.doormodel.tandem;

public class Style4PSF extends Style4P
{
	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxHeight(2250);
		addMaxWidth(500);
	}

	/*
	 * @Override protected double calculHauteurAccHaut() { return 377; }
	 */
}
