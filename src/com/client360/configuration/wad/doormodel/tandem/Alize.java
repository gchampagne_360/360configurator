package com.client360.configuration.wad.doormodel.tandem;

import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;


public class Alize extends TandemArchitecturale
{
	@Override
	protected void load()
	{
		super.load();
		addAccessoires();

		addLineDownOffset(posMotifHaut, top, 30);
		addLineUpOffset(posMotifBas, sill, 45);
		addLineRightOffset(posMotifGauche, left, 30);
		addLineLeftOffset(posMotifDroit, right, 134);
		addForm(rectangle(posMotifGauche, posMotifDroit, posMotifHaut, posMotifBas), "/images/door_svg/tandem/architecturales/Alize.svg", ImageScale.FULL);
	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxWidth(900);
		addMaxHeight(2150);
	}
}
