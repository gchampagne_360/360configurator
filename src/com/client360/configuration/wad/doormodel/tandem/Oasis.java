package com.client360.configuration.wad.doormodel.tandem;

import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;

public class Oasis extends TandemContemporaine
{
	@Override
	protected void load()
	{
		super.load();
		super.addAccessoires();
		super.addOculus();
		addForm(rectangle(left, right, top, sill), "/images/door_svg/tandem/contemporaines/Oasis.svg", ImageAlign.CENTER, ImageScale.HORIZONTAL);
	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxWidth(1000);
		addMaxHeight(2250);
	}
}
