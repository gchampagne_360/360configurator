package com.client360.configuration.wad.doormodel.tandem;

public class Style3PSF extends Style3P
{
	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxHeight(2218);
		addMaxWidth(500);
	}
}
