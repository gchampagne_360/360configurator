package com.client360.configuration.wad.doormodel.tandem;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;


public class Eclipse extends TandemMillenium
{
	@Override
	protected void load()
	{
		super.load();
		addAccessoires();
	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxWidth(1002); // s'applique avec la r�gle "b"
		addMaxWidth(852); // s'applique avec la r�gle "a"
		addMaxHeight(2250);
	}

	public void a()
	{
		setLines();
		addForm(rectangle(posMotifGauche, posMotifDroit, posMotifHaut, sill), "/images/door_svg/tandem/millenium/Eclipse_small.svg", ImageAlign.TOPCENTER);
	}

	public void b()
	{
		setLines();
		addForm(rectangle(posMotifGauche, posMotifDroit, posMotifHaut, sill), "/images/door_svg/tandem/millenium/Eclipse_big.svg", ImageAlign.TOPCENTER);
	}

	private void setLines()
	{
		addLineDownOffset(posMotifHaut, top, 15);
		addLineRightOffset(posMotifGauche, left, 15);
		addLineLeftOffset(posMotifDroit, right, 15);
	}
}
