package com.client360.configuration.wad.doormodel.tandem;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;


public class Dune extends TandemContemporaine
{
	@Override
	protected void load()
	{
		super.load();
		super.addAccessoires();
		super.addOculus();
		addForm(rectangle(left, right, top, posJetDeauHaut), "/images/door_svg/tandem/contemporaines/Dune.svg", ImageAlign.CENTERLEFT);
	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxWidth(900);
		addMaxHeight(2150);
	}
}
