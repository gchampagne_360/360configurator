package com.client360.configuration.wad.doormodel.tandem;

import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;

public class Flore extends TandemArchitecturale
{
	@Override
	protected void load()
	{
		super.load();
		addAccessoires();
		addForm(rectangle(left, right, top, sill), "/images/door_svg/tandem/architecturales/Flore.svg", ImageScale.FULL);
	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxWidth(1000);
		addMaxHeight(2250);
	}
}
