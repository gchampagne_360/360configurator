package com.client360.configuration.wad.doormodel.tandem;

public class Style4P extends TandemStyle
{
	@Override
	protected void load()
	{
		addAccessoires();
	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxHeight(2250);
		addMaxWidth(900);
	}

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();

		addLineDownOffset(posPanHautHaut, top, espaceHaut);
		addLineRightOffset(posPanHautGauche, left, espaceCote);
		addLineLeftOffset(posPanHautDroit, right, espaceCote);
		double hauteurPanHaut = calculHauteurAccHaut();
		addLineDownOffset(posPanHautBas, posPanHautHaut, hauteurPanHaut);

		addLineDownOffset(posPanMilieuHaut, posPanHautBas, espaceEntreAcc);
		addLineRightOffset(posPanMilieuGauche, left, espaceCote);
		addLineLeftOffset(posPanMilieuDroit, right, espaceCote);
		double hauteurPanMilieu = calculHauteurAccMilieu();
		addLineDownOffset(posPanMilieuBas, posPanMilieuHaut, hauteurPanMilieu);

		addLineDownOffset(posPanBasHaut, posPanMilieuBas, espaceEntreAcc);
		addLineUpOffset(posPanBasBas, posJetDeauHaut, espaceEntreAcc);
		addLineRightOffset(posPanBasGauche, left, espaceCote);
		addLineLeftOffset(posPanBasDroit, right, espaceCote);

		addElementSpace("3", posPanBasGauche, posPanBasDroit, posPanBasHaut, posPanBasBas);
		addElementSpace("4", posPanMilieuGauche, posPanMilieuDroit, posPanMilieuHaut, posPanMilieuBas);
		addElementSpace("5", posPanHautGauche, posPanHautDroit, posPanHautHaut, posPanHautBas);
	}

	protected double calculHauteurAccHaut()
	{
		return 377;
	}

	protected double calculHauteurAccMilieu()
	{
		return (hDist(posPanMilieuHaut, posJetDeauHaut) - (2 * espaceEntreAcc)) / 2;
	}

}
