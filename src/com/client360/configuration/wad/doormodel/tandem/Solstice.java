package com.client360.configuration.wad.doormodel.tandem;

import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;

public class Solstice extends TandemMillenium
{
	@Override
	protected void load()
	{
		super.load();
		addAccessoires();
	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxWidth(1002);
		addMaxHeight(2250);
	}

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();

		// motif haut
		addLineDownOffset(posMotifHaut, top, 267.1);
		addLineRightOffset(posMotifGauche, left, 55.71);
		addLineLeftOffset(posMotifDroit, right, 15);
		// addLineDownOffset(posMotifBas, top, 982.64);

		addForm(rectangle(posMotifGauche, posMotifDroit, posMotifHaut, sill), "/images/door_svg/tandem/millenium/Solstice_up.svg", ImageAlign.TOPLEFT);

		// motif bas
		addLineLeftOffset(posMotifDroit + 2, right, 136.4);
		addLineRightOffset(posMotifGauche + 2, left, 15);
		addLineDownOffset(posMotifBas + 2, top, 1428);
		// addLineDownOffset(posMotifHaut + 2, top, 731.14);

		addForm(rectangle(posMotifGauche + 2, posMotifDroit + 2, top, posMotifBas + 2), "/images/door_svg/tandem/millenium/Solstice_down.svg",
				ImageAlign.BOTTOMRIGHT);
	}
}
