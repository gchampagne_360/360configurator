package com.client360.configuration.wad.doormodel.tandem;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;


public class Equinoxe extends TandemArchitecturale
{
	@Override
	protected void load()
	{
		super.load();
		super.addAccessoires();
	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxWidth(1000);
		addMaxWidth(880);
		addMaxHeight(2250);
	}

	public void a()
	{
		addForm(rectangle(left, right, top, sill), "/images/door_svg/tandem/architecturales/Equinoxe_small.svg", ImageAlign.CENTERRIGHT);
	}

	public void b()
	{
		addForm(rectangle(left, right, top, sill), "/images/door_svg/tandem/architecturales/Equinoxe_big.svg", ImageAlign.CENTERRIGHT);
	}
}
