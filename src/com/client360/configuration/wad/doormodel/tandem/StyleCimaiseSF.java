package com.client360.configuration.wad.doormodel.tandem;

public class StyleCimaiseSF extends StyleCimaise
{
	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxHeight(2250);
		addMaxWidth(500);
	}
}
