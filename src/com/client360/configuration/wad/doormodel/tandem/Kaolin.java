package com.client360.configuration.wad.doormodel.tandem;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;


public class Kaolin extends TandemMillenium
{
	@Override
	protected void load()
	{
		super.load();
		super.addAccessoires();
		addForm(rectangle(left, right, top, sill), "/images/door_svg/tandem/millenium/Kaolin.svg", ImageAlign.CENTERLEFT, ImageScale.HORIZONTAL);
	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxWidth(1002);
		addMaxHeight(2250);
	}
}
