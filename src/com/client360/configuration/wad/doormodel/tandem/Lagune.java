package com.client360.configuration.wad.doormodel.tandem;

import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;

public class Lagune extends TandemArchitecturale
{
	@Override
	protected void load()
	{
		super.load();
		addAccessoires();
		addForm(rectangle(left, right, top, sill), "/images/door_svg/tandem/architecturales/Lagune.svg", ImageScale.FULL);
	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxWidth(900);
		addMaxHeight(2150);
	}
}
