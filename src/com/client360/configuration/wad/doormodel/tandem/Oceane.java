package com.client360.configuration.wad.doormodel.tandem;

import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;

public class Oceane extends TandemContemporaine
{
	@Override
	protected void load()
	{
		super.load();
		super.addAccessoires();
		super.addOculus();
		addForm(rectangle(left, right, top, sill), "/images/door_svg/tandem/contemporaines/Oceane.svg", ImageScale.FULL);
	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxWidth(900);
		addMaxHeight(2150);
	}
}
