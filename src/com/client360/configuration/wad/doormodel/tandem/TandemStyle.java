package com.client360.configuration.wad.doormodel.tandem;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public abstract class TandemStyle extends ParametricModel
{
	protected final double hauteurPlinthe = 74d;
	protected final double hauteurJetDeau = 43d;
	protected final double largeurCotePlinthe = 58d;
	protected final double largeurCoteJetDeau = 50d;
	protected final double hauteurBasPlinthe = 5d;

	protected double espaceHaut = 102d;
	protected double espaceCote = 102d;
	protected double espaceEntreAcc = 61d;

	// lines
	public String posPlintheHaut = "posPlintheHaut";
	public String posPlintheBas = "posPlintheBas";
	public String posPlintheGauche = "posPlintheGauche";
	public String posPlintheDroite = "posPlintheDroite";

	public String posJetDeauHaut = "posJetDeauHaut";
	public String posJetDeauGauche = "posJetDeauGauche";
	public String posJetDeauDroite = "posJetDeauDroite";

	public String posPanHautHaut = "posPanHautHaut";
	public String posPanHautBas = "posPanHautBas";
	public String posPanHautGauche = "posPanHautGauche";
	public String posPanHautDroit = "posPanHautDroit";

	public String posPanMilieuHaut = "posPanMilieuHaut";
	public String posPanMilieuBas = "posPanMilieuBas";
	public String posPanMilieuGauche = "posPanMilieuGauche";
	public String posPanMilieuDroit = "posPanMilieuDroit";

	public String posPanBasHaut = "posPanBasHaut";
	public String posPanBasBas = "posPanBasBas";
	public String posPanBasGauche = "posPanBasGauche";
	public String posPanBasDroit = "posPanBasDroit";

	@Override
	protected void load()
	{
		// TODO Auto-generated method stub

	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

	protected void addAccessoires()
	{
		addLineUpOffset(posPlintheBas, sill, hauteurBasPlinthe);
		addLineUpOffset(posPlintheHaut, posPlintheBas, hauteurPlinthe);
		addLineRightOffset(posPlintheGauche, left, largeurCotePlinthe);
		addLineLeftOffset(posPlintheDroite, right, largeurCotePlinthe);

		addLineUpOffset(posJetDeauHaut, posPlintheHaut, hauteurJetDeau);
		addLineRightOffset(posJetDeauGauche, left, largeurCoteJetDeau);
		addLineLeftOffset(posJetDeauDroite, right, largeurCoteJetDeau);

		addElementSpace("1", posPlintheGauche, posPlintheDroite, posPlintheHaut, posPlintheBas);
		addElementSpace("2", posJetDeauGauche, posJetDeauDroite, posJetDeauHaut, posPlintheHaut);
	}

}
