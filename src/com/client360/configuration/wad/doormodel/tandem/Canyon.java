package com.client360.configuration.wad.doormodel.tandem;

import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;


public class Canyon extends Tandem
{
	@Override
	protected void load()
	{
		super.load();
		addAccessoires();
		/*
		 * addLineDownOffset(posMotifHaut, top, 30); addLineUpOffset(posMotifBas, sill, 45); addLineRightOffset(posMotifGauche, left, 30);
		 * addLineLeftOffset(posMotifDroit, right, 134); addForm(rectangle(posMotifGauche, posMotifDroit, posMotifHaut, posMotifBas),
		 * "/images/door_svg/tandem/Alize.svg", ImageScale.FULL);
		 */
		addForm(rectangle(left, right, top, sill), "/images/door_svg/tandem/architecturales/Canyon.svg", ImageScale.FULL);
	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxWidth(1000);
		addMaxHeight(2250);
	}
}
