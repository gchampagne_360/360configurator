package com.client360.configuration.wad.doormodel.tandem;

import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;

public class Litoral extends TandemArchitecturale
{
	@Override
	protected void load()
	{
		super.load();
		super.addAccessoires();
	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxHeight(2250); // s'applique avec la r�gle "_2"
		addMaxHeight(2150); // s'applique avec la r�gle "_1"

		addMaxWidth(1000); // s'applique avec la r�gle "_b"
		addMaxWidth(880); // s'applique avec la r�gle "_a"
	}

	public void a1Override()
	{
		addForm(rectangle(left, right, top, sill), "/images/door_svg/tandem/architecturales/Litoral_a1.svg", ImageAlign.CENTERLEFT);
	}

	public void a2Override()
	{
		addForm(rectangle(left, right, top, sill), "/images/door_svg/tandem/architecturales/Litoral_a2.svg", ImageAlign.CENTERLEFT);
	}

	public void b1Override()
	{
		addForm(rectangle(left, right, top, sill), "/images/door_svg/tandem/architecturales/Litoral_b1.svg", ImageAlign.CENTERLEFT);
	}

	public void b2Override()
	{
		addForm(rectangle(left, right, top, sill), "/images/door_svg/tandem/architecturales/Litoral_b2.svg", ImageAlign.CENTERLEFT);
	}
}
