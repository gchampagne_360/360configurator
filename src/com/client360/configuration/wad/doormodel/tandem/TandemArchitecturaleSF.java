package com.client360.configuration.wad.doormodel.tandem;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class TandemArchitecturaleSF extends ParametricModel
{
	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxHeight(2250);
		addMaxWidth(500);
	}

	@Override
	protected void load()
	{
		addElementSpace("1", left, right, top, sill);
	}

	@Override
	protected Unit<Length> getUnit()
	{
		return Units.MM;
	}

}
