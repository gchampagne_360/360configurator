package com.client360.configuration.wad.doormodel.tandem;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;


public class Ecume extends TandemContemporaine
{
	@Override
	protected void load()
	{
		super.load();
		addAccessoires();
		addForm(rectangle(left, right, top, posJetDeauHaut), "/images/door_svg/tandem/contemporaines/Ecume.svg", ImageAlign.CENTERLEFT);
		addOculus();
	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxWidth(900);
		addMaxHeight(2150);
	}

	@Override
	protected void addOculus()
	{
		// oculus triangle haut/bas
		double oculusTriangleLargeur = 600;
		double espaceOcTriangle = 127;
		addLineDownOffset("ocTrHautGaucheHaut", top, espaceOcTriangle);
		addLineDownOffset("ocTrHautGaucheBas", top, espaceOcTriangle + oculusTriangleLargeur);
		addLineRightOffset("ocTrHautGaucheGauche", left, espaceOcTriangle);
		addLineRightOffset("ocTrHautGaucheDroit", left, espaceOcTriangle + oculusTriangleLargeur);

		addLineUpOffset("ocTrBasGaucheBas", sill, espaceOcTriangle);
		addLineUpOffset("ocTrBasGaucheHaut", sill, espaceOcTriangle + oculusTriangleLargeur);
		addLineRightOffset("ocTrBasGaucheGauche", left, espaceOcTriangle);
		addLineRightOffset("ocTrBasGaucheDroit", left, espaceOcTriangle + oculusTriangleLargeur);

		addElementSpace("2", "ocTrHautGaucheGauche", "ocTrHautGaucheDroit", "ocTrHautGaucheHaut", "ocTrHautGaucheBas");
		addElementSpace("3", "ocTrBasGaucheGauche", "ocTrBasGaucheDroit", "ocTrBasGaucheHaut", "ocTrBasGaucheBas");
	}
}
