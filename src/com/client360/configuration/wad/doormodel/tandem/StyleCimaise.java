package com.client360.configuration.wad.doormodel.tandem;

public class StyleCimaise extends TandemStyle
{
	// private final double hauteurPanHaut = 943.5;
	private final double hauteurPanMilieu = 100;

	@Override
	protected void load()
	{
		addAccessoires();
	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxHeight(2250);
		addMaxWidth(1000);
	}

	@Override
	protected void addAccessoires()
	{
		super.addAccessoires();

		double hauteurPanHaut = ((hDist(top, sill) - 1298.5)) + 80;
		addLineDownOffset(posPanHautHaut, top, espaceHaut);
		addLineRightOffset(posPanHautGauche, left, espaceCote);
		addLineLeftOffset(posPanHautDroit, right, espaceCote);
		addLineDownOffset(posPanHautBas, posPanHautHaut, hauteurPanHaut);

		addLineDownOffset(posPanMilieuHaut, posPanHautBas, espaceEntreAcc);
		addLineRightOffset(posPanMilieuGauche, left, espaceCote);
		addLineLeftOffset(posPanMilieuDroit, right, espaceCote);
		addLineDownOffset(posPanMilieuBas, posPanMilieuHaut, hauteurPanMilieu);

		addLineDownOffset(posPanBasHaut, posPanMilieuBas, espaceEntreAcc);
		addLineUpOffset(posPanBasBas, posJetDeauHaut, espaceEntreAcc);
		addLineRightOffset(posPanBasGauche, left, espaceCote);
		addLineLeftOffset(posPanBasDroit, right, espaceCote);

		addElementSpace("3", posPanBasGauche, posPanBasDroit, posPanBasHaut, posPanBasBas);
		addElementSpace("4", posPanMilieuGauche, posPanMilieuDroit, posPanMilieuHaut, posPanMilieuBas);
		addElementSpace("5", posPanHautGauche, posPanHautDroit, posPanHautHaut, posPanHautBas);
	}
}
