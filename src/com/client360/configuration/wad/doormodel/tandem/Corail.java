package com.client360.configuration.wad.doormodel.tandem;

public class Corail extends StyleCimaise
{
	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxHeight(2150);
		addMaxWidth(900);
	}
}
