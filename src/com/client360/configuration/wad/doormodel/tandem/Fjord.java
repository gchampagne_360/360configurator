package com.client360.configuration.wad.doormodel.tandem;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;


public class Fjord extends TandemArchitecturale
{
	@Override
	protected void load()
	{
		super.load();
		addAccessoires();
		addElementSpace("1", posJetDeauGauche, posJetDeauDroit, posJetDeauHaut, posJetDeauBas);
	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxWidth(1000);
		addMaxHeight(2250);
		addMaxHeight(2120);
	}

	public void _1()
	{
		addForm(rectangle(left, right, top, sill), "/images/door_svg/tandem/architecturales/Fjord_small.svg", ImageAlign.CENTER);
	}

	public void _2()
	{
		addForm(rectangle(left, right, top, sill), "/images/door_svg/tandem/architecturales/Fjord_big.svg", ImageAlign.CENTER);
	}
}
