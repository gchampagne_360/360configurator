package com.client360.configuration.wad.doormodel.tandem;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;


public class Golfe extends TandemMillenium
{
	@Override
	protected void load()
	{
		super.load();
		addAccessoires();
		addForm(rectangle(left, right, top, sill), "/images/door_svg/tandem/millenium/Golfe.svg", ImageAlign.CENTERLEFT);
	}

	@Override
	public void matrixDefinition()
	{
		clearHeightsMax();
		clearWidthsMax();
		super.matrixDefinition();

		addMaxWidth(1002);
		addMaxHeight(2250);
	}
}
