// $Id: QuestionnairePetitBoisEnfourche.java,v 1.2 2011-09-06 13:22:20 sboule Exp $

package com.client360.configuration.wad;

import com.client360.configuration.wad.enums.LargeurPetitBoisEnfourche;
import com.netappsid.wadconfigurator.InsulatedGlass;
import com.netappsid.wadconfigurator.Section;
import com.netappsid.wadconfigurator.grilles.CalculationMuntin;
import com.netappsid.wadconfigurator.grilles.GrillesManager;

public class QuestionnairePetitBoisEnfourche extends com.client360.configuration.wad.QuestionnairePetitBoisEnfourcheBase
{
	boolean inhibateCalulation;

	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public QuestionnairePetitBoisEnfourche(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	@Override
	public Class<? extends GrillesManager> getGrillesManagerClass()
	{
		// Utilisation du calculation du WAD qui fait l'affaire dans ce cas.
		return CalculationMuntin.class;
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		inhibateCalulation = true;

		try
		{
			setDefaultNbTdlHorizBars(1);
			setDefaultNbTdlVertiBars(1);
			setDefaultLargeurPetitBoisEnfourche(LargeurPetitBoisEnfourche.EPAISSEUR30MM);
			setDefaultVerresEgaux(true);
			setDefaultLargeurHorsCote(false);
		}
		finally
		{
			inhibateCalulation = false;
		}

		// Supprimer les sous-bassements si l'utilisateur passe directement de sous-bassement a petit bois.
		InsulatedGlass[] insulatedGlass = ((Section) getParent()).getInsulatedGlass();
		if (insulatedGlass != null && insulatedGlass.length > 0)
		{
			for (int i = insulatedGlass.length - 1; i > 0; i--)
			{
				insulatedGlass[i].deleteKickPanelModel();
			} /* end for */
		} /* end if */

		makeExpandedCollection(PROPERTYNAME_SPLITTINGBARS);
		makeReadOnly(PROPERTYNAME_SPLITTINGBARS);
		// makeInvisible(PROPERTYNAME_CHOICEBARPOSITIONFROM);
	}

	@Override
	public void afterNbTdlHorizBarsChanged(Integer nbTdlHorizBars)
	{
		super.afterNbTdlHorizBarsChanged(nbTdlHorizBars);
		calculate();
	}

	@Override
	public void afterNbTdlVertiBarsChanged(Integer nbTdlVertiBars)
	{
		super.afterNbTdlVertiBarsChanged(nbTdlVertiBars);
		calculate();
	}

	@Override
	public void afterLargeurPetitBoisEnfourcheChanged(LargeurPetitBoisEnfourche largeurPetitBoisEnfourche)
	{
		super.afterLargeurPetitBoisEnfourcheChanged(largeurPetitBoisEnfourche);
		calculate();
	}

	@Override
	public void afterVerresEgauxChanged(Boolean verresEgaux)
	{
		super.afterVerresEgauxChanged(verresEgaux);

		hideQuestions();

		// if (((Section) getParent()).getAdvancedSectionGrilles() != null)
		// {
		// InternalMuntin[] muntins = ((Section) getParent()).getAdvancedSectionGrilles().getSplittingBars();
		//
		// if (muntins != null && muntins.length > 0)
		// {
		// // Montrer les barres et leur position dans l'arbre de saisie si pas verres egaux.
		// for (InternalMuntin internalMuntin : muntins)
		// {
		// if (getVerresEgaux())
		// {
		// internalMuntin.makeInvisible(InternalMuntin.PROPERTYNAME_USERPOSITION);
		// internalMuntin.removeUserPosition();
		// } /* end if */
		// else
		// {
		// internalMuntin.makeVisible(InternalMuntin.PROPERTYNAME_USERPOSITION);
		// } /* end else */
		// } /* end for */
		// } /* end if */
		// } /* end if */

		calculate();
	}

	@Override
	public void afterLargeurHorsCoteChanged(Boolean arg0)
	{
		super.afterLargeurHorsCoteChanged(arg0);

		hideQuestions();

		if (((Section) getParent()).getAdvancedSectionGrilles() != null)
		{
			InternalMuntin[] muntins = (InternalMuntin[]) ((Section) getParent()).getAdvancedSectionGrilles().getSplittingBars();

			if (muntins != null && muntins.length > 0)
			{
				// Montrer les barres et leur largeur dans l'arbre de saisie si largeur hors cote.
				for (InternalMuntin internalMuntin : muntins)
				{
					if (getLargeurHorsCote())
					{
						internalMuntin.makeVisible(InternalMuntin.PROPERTYNAME_THICKNESS);
					} /* end if */
					else
					{
						internalMuntin.makeInvisible(InternalMuntin.PROPERTYNAME_THICKNESS);
						internalMuntin.removeThickness();
					} /* end else */
				} /* end for */
			} /* end if */
		} /* end if */
	}

	/**
	 * When the form dimension is changed, make sure the user inputs still make sense.
	 */
	public void formDimensionChanged()
	{
		// ...
		calculate();
	}

	/**
	 * Hide or show the questionnary question based on the current status.
	 */
	private void hideQuestions()
	{
		if (getVerresEgaux() == null || getLargeurHorsCote() == null)
		{
			return;
		} /* end if */

		if (getVerresEgaux() && !getLargeurHorsCote())
		{
			makeVisible(PROPERTYNAME_NBTDLHORIZBARS);
			makeVisible(PROPERTYNAME_NBTDLVERTIBARS);
			makeVisible(PROPERTYNAME_LARGEURPETITBOISENFOURCHE);

			if (((Section) getParent()).getAdvancedSectionGrilles() != null)
			{
				((Section) getParent()).getAdvancedSectionGrilles().makeInvisible(PROPERTYNAME_SPLITTINGBARS);
			} /* end if */
		} /* end if */
		else
		{
			makeInvisible(PROPERTYNAME_NBTDLHORIZBARS);
			makeInvisible(PROPERTYNAME_NBTDLVERTIBARS);
			makeInvisible(PROPERTYNAME_LARGEURPETITBOISENFOURCHE);

			if (((Section) getParent()).getAdvancedSectionGrilles() != null)
			{
				((Section) getParent()).getAdvancedSectionGrilles().makeVisible(PROPERTYNAME_SPLITTINGBARS);
			} /* end if */
		} /* end else */

		// if (getVerresEgaux())
		// {
		// makeInvisible(PROPERTYNAME_CHOICEMUNTINPOSITIONFROM);
		// } /* end if */
		// else
		// {
		// makeVisible(PROPERTYNAME_CHOICEMUNTINPOSITIONFROM);
		// } /* end else */
	}
}
