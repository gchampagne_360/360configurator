// $Id: CasementVisionHybridSashSection.java,v 1.5 2011-04-27 12:09:13 jddaigle Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.client360.configuration.wad.enums.ChoiceAdvancedGrilles;
import com.netappsid.erp.configurator.Configurable;

public class CasementVisionHybridSashSection extends com.client360.configuration.wad.CasementVisionHybridSashSectionBase
{
	public CasementVisionHybridSashSection(Configurable parent)
	{
		super(parent);
	}

	@Override
	public Measure<Length>[] getGlassOffset()
	{
		return ProductData.getSpacer(ProductType.CASEMENT, Data.SASH_TO_GLASS);
	}

	public void afterChoiceAdvancedGrillesChanged(ChoiceAdvancedGrilles choiceAdvancedGrilles)
	{
		if (choiceAdvancedGrilles == null) // No selection
		{
			removeAdvancedSectionGrilles();
		}
		else
		{
			setAdvancedSectionGrilles(choiceAdvancedGrilles.configClass);
		}
	}

	@Override
	public Measure<Length>[] getSplittingGrillesOffset()
	{
		return ProductData.getSpacer(ProductType.CASEMENT, Data.SASH_TO_GLASS);
	}

	@Override
	public Measure<Length>[] getNoSplittingGrillesOffset()
	{
		return ProductData.getSpacer(ProductType.CASEMENT, Data.SASH_TO_GLASS);
	}

}
