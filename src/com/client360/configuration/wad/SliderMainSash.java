// $Id: SliderMainSash.java,v 1.3 2011-09-07 19:04:33 sboule Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.commonutils.geo.CornerType;
import com.netappsid.erp.configurator.Configurable;

public abstract class SliderMainSash extends com.client360.configuration.wad.SliderMainSashBase
{

	public SliderMainSash(Configurable parent)
	{
		super(parent);
	}

	@Override
	public CornerType[] getCornerType()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length>[] getSashFrameThickness()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length> getSectionMaxWidth()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length> getSectionMinWidth()
	{
		// TODO Auto-generated method stub
		return null;
	}

	// @Override
	// public SashSection[] sashSectionMakeArray(int nbSections)
	// {
	// // TODO Auto-generated method stub
	// return null;
	// }

	// Add custom overrides here
	public abstract Measure<Length>[] getGlazingBeadOffset();

	public abstract Measure<Length>[] getScreenOffset();

	public abstract Measure<Length>[] getGlassOffset();
}
