package com.client360.configuration.wad.constant;

public enum ProductType
{
	// Insulated Glass
	INSULATED_GLASS,

	// FRAME
	FRAME,

	// Grilles
	CONTOUR,

	// Grille Bars
	MUNTIN, FLAT, GEORGIAN, PENCIL,

	// Casement
	CASEMENT_VISION_HYBRID, CASEMENT_VISION_PVC_IN, CASEMENT_VISION_PVC_OUT,

	// Fixed
	FIXED_HYBRID,

	// Steel Door
	DOOR,

	// Toutes les fenetres
	ALLWINDOWS,

	// InternalMuntin
	INTERNAL_MUNTIN,

	STEEL_DOOR, STEEL_DOOR_MP,

	CASEMENT_MILANO, CASEMENT_TILT_TURN,

	// Slider
	SLIDER_HYBRID, SLIDER_PVC,

	// Fixed
	FixedMilanoPvc, FixedVisionHybrid, FixedVisionPvc,

	// Hung
	HUNG_HYBRID, HUNG_PVC,

	// Patio Door
	PATIO_DOOR, PATIO_DOOR_EVEREST, PATIO_DOOR_FUSION, PATIO_DOOR_VIVALDI, CASEMENT, Form, ODACE,

	// ProductGroup
	PRODUCT_GROUP, ;
}