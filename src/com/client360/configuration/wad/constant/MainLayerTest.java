package com.client360.configuration.wad.constant;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JDialog;
import javax.swing.JPanel;

import com.client360.configuration.wad.cad.CadFrameFix;
import com.netappsid.commonutils.color.Color;
import com.netappsid.rendering.common.advanced.Drawing;

public class MainLayerTest
{

	public static void main(String[] args) throws Exception
	{
		new ProductData();

		testLayers();

		System.exit(0);
	}

	public static void testLayers()
	{

		showDrawing(new CadFrameFix());
	}

	public static void showDrawing(Drawing drawing)
	{
		final Drawing cad = drawing;

		JDialog frame = new JDialog();

		JPanel panel = new JPanel()
			{
				@Override
				public void paint(Graphics g)
				{
					super.paint(g);
					Graphics2D g2d = (Graphics2D) g;
					g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
					g2d.scale(4.0, 4.0);

					g2d.translate(25.0, 25.0);

					g2d.setColor(Color.Red);

					cad.paintDrawing(g2d, false);

				}
			};

		frame.getContentPane().add(panel);

		frame.setSize(1200, 900);
		frame.setModal(true);
		frame.setVisible(true);
	}

}
