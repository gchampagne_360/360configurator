package com.client360.configuration.wad.constant;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

class LinkedHashMapTest
{
	public static void main(String[] args)
	{
		testLinkedHashMap();
		testHashMap();
		testLinkedHashMapValues();
	}

	private static void testLinkedHashMap()
	{
		System.out.println("------ testLinkedHashMap -------");
		LinkedHashMap map = new LinkedHashMap();

		// Add some elements
		map.put("A", "1");
		map.put("B", "2");
		map.put("C", "3");
		map.put("Z", "4");
		map.put("V", "5");
		map.put("R", "6");
		map.put("T", "7");
		map.put("Y", "8");
		map.put("W", "9");
		map.put("I", "10");
		map.put("S", "11");
		map.put("X", "12");

		// List the entries
		for (Iterator it = map.keySet().iterator(); it.hasNext();)
		{
			Object key = it.next();
			Object value = map.get(key);
			System.out.println("Key = " + key.toString() + " Value = " + value.toString());

		}
	}

	private static void testLinkedHashMapValues()
	{
		System.out.println("------ testLinkedHashMapValues -------");
		Map<String, String> map = new LinkedHashMap<String, String>();

		// Add some elements
		map.put("A", "1");
		map.put("B", "2");
		map.put("C", "3");
		map.put("Z", "4");
		map.put("V", "5");
		map.put("R", "6");
		map.put("T", "7");
		map.put("Y", "8");
		map.put("W", "9");
		map.put("I", "10");
		map.put("S", "11");
		map.put("X", "12");

		// List the entries
		for (String value : map.values())
		{
			System.out.println(" Value = " + value);
		}

	}

	private static void testHashMap()
	{
		System.out.println("------ testHashMap -------");
		Map<String, String> map = new HashMap<String, String>();

		// Add some elements
		map.put("A", "1");
		map.put("B", "2");
		map.put("C", "3");
		map.put("Z", "4");
		map.put("V", "5");
		map.put("R", "6");
		map.put("T", "7");
		map.put("Y", "8");
		map.put("W", "9");
		map.put("I", "10");
		map.put("S", "11");
		map.put("X", "12");

		// List the entries
		for (Iterator it = map.keySet().iterator(); it.hasNext();)
		{
			Object key = it.next();
			Object value = map.get(key);
			System.out.println("Key = " + key.toString() + " Value = " + value.toString());

		}
	}
}