package com.client360.configuration.wad.constant;

import java.util.HashMap;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import org.jscience.physics.measures.Measure;

/**
 * Overwrite WAD.ProductDataManagement allowing product data to be replaced for tests purposes.
 * 
 * @author sboule
 */
public abstract class ProductDataManagement extends com.netappsid.wadconfigurator.productdata.ProductDataManagement
{

	public ProductDataManagement()
	{
		super();
	}

	/**
	 * Add or replace an array of spacers.
	 */
	public void addSpacer(String productType, String data, Double[] doubleValue)
	{
		if (windowSpacerSpecification.containsKey(productType))
		{
			Measure<Length>[] value = new Measure[doubleValue.length];
			for (int i = 0; i < doubleValue.length; i++)
			{
				value[i] = Measure.valueOf(doubleValue[i], getUnit());
			}

			windowSpacerSpecification.get(productType).put(data, value);
		}
		else
		{
			super.addSpacer(productType, data, doubleValue);
		}

	}

	/**
	 * Add or replace an array of values.
	 * 
	 * @param productType
	 * @param data
	 * @param doubleValue
	 * @param unit
	 */
	public static void addValues(String productType, String data, Double[] doubleValue, Unit<Length> unit)
	{
		if (windowValuesSpecification.containsKey(productType))
		{
			Measure<Length>[] value = new Measure[doubleValue.length];
			for (int i = 0; i < doubleValue.length; i++)
			{
				value[i] = Measure.valueOf(doubleValue[i], unit);
			}

			windowValuesSpecification.get(productType).put(data, value);
		}
		else
		{
			windowValues = new HashMap<String, Measure<Length>[]>();

			Measure<Length>[] value = new Measure[doubleValue.length];
			for (int i = 0; i < doubleValue.length; i++)
			{
				value[i] = Measure.valueOf(doubleValue[i], unit);
			}
			windowValues.put(data, value);
			windowValuesSpecification.put(productType, windowValues);
		}
	}

	/**
	 * Add or replace a value.
	 */
	public void addValue(Enum productType, Enum data, Double doubleValue)
	{
		String stringPoductType = productType.toString();
		String stringData = data.toString();

		if (windowValueSpecification.containsKey(stringPoductType))
		{
			Measure<Length> value = Measure.valueOf(doubleValue, getUnit());
			windowValueSpecification.get(stringPoductType).put(stringData, value);
		}
		else
		{
			super.addValue(productType, data, doubleValue);
		}
	}
}