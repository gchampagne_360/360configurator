package com.client360.configuration.wad.constant;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import com.client360.configuration.wad.cad.profiles.ProfileAssise;
import com.client360.configuration.wad.cad.profiles.ProfileFixFrame;
import com.client360.configuration.wad.cad.profiles.ProfileFlat;
import com.client360.configuration.wad.cad.profiles.ProfileGeorgian;
import com.client360.configuration.wad.cad.profiles.ProfileMullion;
import com.client360.configuration.wad.cad.profiles.ProfilePencil;
import com.netappsid.commonutils.geo.CornerType;
import com.netappsid.commonutils.math.Units;
import com.netappsid.rendering.common.advanced.Profile;

public class ProductData extends ProductDataManagement
{
	@Override
	public void load()
	{
		addCornerType(ProductType.CASEMENT, Data.FRAME_CORNERTYPE, CornerType.HALF_CORNER, 8);
		addCornerType(ProductType.CASEMENT, Data.SASH_CORNERTYPE, new CornerType[] { CornerType.HALF_CORNER, CornerType.HALF_CORNER, CornerType.HALF_CORNER,
				CornerType.HALF_CORNER, CornerType.HALF_CORNER, CornerType.HALF_CORNER, CornerType.HALF_CORNER, CornerType.HALF_CORNER });

		addSpacer(ProductType.CASEMENT, Data.FRAME_BORDER_SPACERS, 5 / 8d, 8);
		addSpacer(ProductType.CASEMENT, Data.FRAME_TO_SASH_OFFSET_SPACERS, 7 / 8d, 8);
		addSpacer(ProductType.CASEMENT, Data.SASH_BORDER_SPACERS, 1 + 1 / 4d, 8);
		addSpacer(ProductType.CASEMENT, Data.GLAZING_BEAD_THICKNESS, 1d /* 9/16d */, 8);
		addSpacer(ProductType.CASEMENT, Data.SASH_TO_GLASS, 1 + 5 / 16d, 8);
		addValue(ProductType.CASEMENT, Data.GLASS_TO_GRILLE, 9 / 16d);
		addSpacer(ProductType.CASEMENT, Data.VISIBLEGLASS_TO_GRILLES, -0.196d, 6);
		addValue(ProductType.CASEMENT, Data.LAR_ENTRE_PANNEAUX, 2.95d);

		addValue(ProductType.CASEMENT, Data.MULLION_TO_SASH, 9 / 16d);
		addValue(ProductType.CASEMENT, Data.MULLION_WIDTH, 1 + 11 / 16d);
		addValue(ProductType.CASEMENT, Data.MULLION_WIDTH_OS, 5 / 8d);

		addValue(ProductType.INTERNAL_MUNTIN, Data.INTERNAL_MUNTIN_SDL_THICKNESS, 5 / 8d);
		addValue(ProductType.INTERNAL_MUNTIN, Data.INTERNAL_MUNTIN_THICKNESS, 1.96);
		addValue(ProductType.INTERNAL_MUNTIN, Data.INTERNAL_MUNTIN_TO_GRILLES, 0.787);
		addValue(ProductType.INTERNAL_MUNTIN, Data.SECTION_TO_INTERNAL_MUNTIN, 1.5 / 4d);

		addValue(ProductType.INTERNAL_MUNTIN, Data.SDL_MUNTIN_THICKNESS, 1.0);
		addValue(ProductType.INTERNAL_MUNTIN, Data.TDL_MUNTIN_THICKNESS, 2.0);
		addValue(ProductType.INTERNAL_MUNTIN, Data.TDL_MUNTIN_TO_GRILLES, 0.787);
		addValue(ProductType.INTERNAL_MUNTIN, Data.SECTION_TO_INTERNAL_MUNTIN_OFFSET, 0.196);
		addValue(ProductType.INTERNAL_MUNTIN, Data.INTERNAL_MUNTIN_TO_GLASS_OFFSET, 0.59);
		addValue(ProductType.INTERNAL_MUNTIN, Data.GRILLES_NUDGE_OFFSET, 0.196); // SDL visible glass to grille

		addValue(ProductType.ALLWINDOWS, Data.INTERNAL_MUNTIN_TO_GLASS_OFFSET, 0.118);

		addValue(ProductType.INSULATED_GLASS, Data.GLASS_TO_GRILLS_OFFSET_SPACERS, 0.125); // INTERNAL_MUNTIN_TO_GRILLES - INTERNAL_MUNTIN_TO_GLASS_OFFSET

		// G R I L L E S
		addValue(ProductType.CONTOUR, Data.CONTOUR_OFFSET_SPACER, 2.75);

		// G R I L L B A R S
		addProfile(ProductType.PENCIL, Data.GRILLEBAR_PROFILE, new ProfilePencil());
		addProfile(ProductType.GEORGIAN, Data.GRILLEBAR_PROFILE, new ProfileGeorgian());
		addProfile(ProductType.FLAT, Data.GRILLEBAR_PROFILE, new ProfileFlat());
		addProfile(ProductType.MUNTIN, Data.GRILLEBAR_PROFILE, new ProfileMullion());

		// ****************************************
		// casement vision PVC
		// ****************************************
		addCornerType(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_CORNERTYPE, CornerType.LONG_CORNER, CornerType.SHORT_CORNER, CornerType.LONG_CORNER,
				CornerType.SHORT_CORNER, CornerType.LONG_CORNER, CornerType.SHORT_CORNER);

		// Frame spacer
		// addSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_BORDER_SPACERS, new Double[] { 64 / 25.4, 140.2 / 25.4, 64 / 25.4, 64 / 25.4, 64 / 25.4, 64
		// / 25.4 });
		// addSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_BORDER_SPACERS, new Double[] { 64 / 25.4, 64 / 25.4, 64 / 25.4, 140.2 / 25.4, 64 / 25.4, 64
		// / 25.4 });
		addSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_BORDER_SPACERS, new Double[] { 29 / 25.4, 29 / 25.4, 29 / 25.4, 29 / 25.4, 29 / 25.4,
				29 / 25.4 });

		// Frame profile
		Profile p = new ProfileGeorgian();
		addProfiles(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_PROFILES,p,p,p,p,p,p,p,p,p);
		

		// Frame to sash
		// addSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID, new Double[] { 30 / 25.4, 102.6 / 25.4, 30 / 25.4, 30 / 25.4,
		// 30 / 25.4, 30 / 25.4 });
		// addSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID, new Double[] { 30 / 25.4, 30 / 25.4, 30 / 25.4, 102.6 / 25.4,
		// 30 / 25.4, 30 / 25.4 });
		addSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID, new Double[] { 30 / 25.4, 30 / 25.4, 30 / 25.4, 30 / 25.4,
				30 / 25.4, 30 / 25.4 });

		// Sash to Glass
		addSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_GLASS_OFFSET_SPACERS, 2d, 8);

		// Frame to Glass (virtualSash)
		// addSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_GLASS_OFFSET_SPACERS, new Double[] { 2d, 5d, 2d, 2d, 2d, 2d, 2d, 2d });
		// addSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_GLASS_OFFSET_SPACERS, new Double[] { 2d, 2d, 2d, 5d, 2d, 2d, 2d, 2d });
		addSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_GLASS_OFFSET_SPACERS, new Double[] { 0d, 0d, 0d, 0d, 0d, 0d, 0d, 0d });

		// sash cornerType
		addCornerType(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_CORNERTYPE, CornerType.SHORT_CORNER, CornerType.LONG_CORNER, CornerType.SHORT_CORNER,
				CornerType.LONG_CORNER, CornerType.SHORT_CORNER, CornerType.LONG_CORNER);
		addCornerType(ProductType.CASEMENT_VISION_PVC_OUT, Data.SASH_CORNERTYPE, CornerType.HALF_CORNER, CornerType.HALF_CORNER, CornerType.HALF_CORNER,
				CornerType.HALF_CORNER, CornerType.HALF_CORNER, CornerType.HALF_CORNER);

		// Sash profile
		p = new ProfileAssise();//ProfileGeorgian();
		addProfiles(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_PROFILES, p,p,p,p,p,p,p,p,p,p);
		
		p = new ProfileAssise();//ProfileGeorgian();
		addProfiles(ProductType.CASEMENT_VISION_PVC_IN, Data.GLASINGBEAD_PROFILE, p,p,p,p,p,p,p,p,p,p);

		// sash frame spacer
		addSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_BORDER_SPACERS, 65 / 25.4, 6);
		addSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.NO_SASH_BORDER_SPACERS, 0d, 6);
		addSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SASH_TO_VISIBLEGLASS, 75 / 25.4, 6);
		addSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_VISIBLEGLASS, 0.01 / 25.4, 6);
		addSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_TO_GLASS, 1 / 25.4, 6);

		addSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.VISIBLEGLASS_TO_GRILLES, -0.1 / 25.4, 6);
		addSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.GLAZINGBEAD_BORDER_SPACERS, 0.5, 6);
		addSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.NOSPLITTING_GRILLES_OFFSET, 1d, 6);
		addSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.SPLITTING_GRILLES_OFFSET, 1.5, 6);

		// Mullion width(thickness)
		addValue(ProductType.CASEMENT_VISION_PVC_IN, Data.MULLION_WIDTH, 1.25);
		addValue(ProductType.CASEMENT_VISION_PVC_IN, Data.ASTRAGAL_WIDTH, 2.0d);

		// Mullion width(thickness)
		addValue(ProductType.CASEMENT_VISION_PVC_IN, Data.MULLION_WIDTH_OS, 0.75);
		addValue(ProductType.CASEMENT_VISION_PVC_IN, Data.ASTRAGAL_WIDTH_OS, 0.5d);

		// Mullion profile
		// addProfile(ProductType.CASEMENT_VISION_PVC_IN, Data.MULLION_PROFILE, new ProfileMullion());

		// mullion position offset
		addSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_MULLION_OFFSET_SPACER, 1.0, 6);
		addValue(ProductType.CASEMENT_VISION_PVC_IN, Data.MULLION_POSITION_OFFSET_PVC, 0.25);
		addValue(ProductType.CASEMENT_VISION_PVC_IN, Data.MULLION_POSITION_OFFSET_HYBRID, 0.25);
		addValue(ProductType.CASEMENT_VISION_PVC_IN, Data.RIGHT_MULLION_TO_SASH_PVC, 0.5d);
		addValue(ProductType.CASEMENT_VISION_PVC_IN, Data.LEFT_MULLION_TO_SASH_PVC, 0.5d);

		addValue(ProductType.CASEMENT_VISION_PVC_IN, Data.ASTRAGAL_TO_OWNER_SASH, 0.25d); // ASTRAGAL_WIDTH / 2
		addValue(ProductType.CASEMENT_VISION_PVC_IN, Data.ASTRAGAL_TO_OTHER_SASH, 0.50d);

		// ----------------- View on top -----------------------------
		// Frame

//		addDrawing(ProductType.CASEMENT_VISION_PVC_IN, Data.DRAWING_LEFTFRAME, new CadFrame());
//		addDrawing(ProductType.CASEMENT_VISION_PVC_IN, Data.DRAWING_RIGHTFRAME, new CadFrame());
//		addDrawing(ProductType.CASEMENT_VISION_PVC_IN, Data.DRAWING_SASH, new CadSash());
//		addDrawing(ProductType.CASEMENT_VISION_PVC_IN, Data.DRAWING_SASHASTRAGAL, new CadSashAstragale());
//		addDrawing(ProductType.CASEMENT_VISION_PVC_IN, Data.DRAWING_MULLION, new CadMullion());

		// ****************************************
		// Fixed vision hybrid
		// ****************************************

		addCornerType(ProductType.FixedVisionHybrid, Data.FRAME_CORNERTYPE, CornerType.HALF_CORNER, CornerType.HALF_CORNER, CornerType.HALF_CORNER,
				CornerType.HALF_CORNER, CornerType.HALF_CORNER, CornerType.HALF_CORNER, CornerType.HALF_CORNER, CornerType.HALF_CORNER);
		addSpacer(ProductType.FixedVisionHybrid, Data.FRAME_BORDER_SPACERS, 1.5d, 8);
		addSpacer(ProductType.FixedVisionHybrid, Data.FRAME_TO_GLASS_OFFSET_SPACERS, 1.122, 8);
		addValue(ProductType.FixedVisionHybrid, Data.LIMITE_LARGEUR_HAUTEUR, 84.0d);
		addValue(ProductType.FixedVisionHybrid, Data.MULLION_WIDTH, 1d);
		addValue(ProductType.FixedVisionHybrid, Data.RIGHT_MULLION_TO_SASH_PVC, 8.64 / 25.4);
		addValue(ProductType.FixedVisionHybrid, Data.LEFT_MULLION_TO_SASH_PVC, 8.64 / 25.4);

		// ****************************************
		// Fixed vision PVC
		// ****************************************
		addCornerType(ProductType.FixedVisionPvc, Data.FRAME_CORNERTYPE, CornerType.HALF_CORNER, CornerType.HALF_CORNER, CornerType.HALF_CORNER,
				CornerType.HALF_CORNER, CornerType.HALF_CORNER, CornerType.HALF_CORNER, CornerType.HALF_CORNER, CornerType.HALF_CORNER);
		addSpacer(ProductType.FixedVisionPvc, Data.FRAME_BORDER_SPACERS, 1.897d, 8);
		addSpacer(ProductType.FixedVisionPvc, Data.FRAME_TO_GLASS_OFFSET_SPACERS, 1.122, 8);
		addSpacer(ProductType.FixedVisionPvc, Data.FRAME_TO_GLASS_OFFSET_SPACERS1, 1.200, 8);
		addSpacer(ProductType.FixedVisionPvc, Data.FRAME_TO_GLASS_OFFSET_SPACERS5, 1.318, 8);
		addValue(ProductType.FixedVisionPvc, Data.LIMITE_LARGEUR_HAUTEUR, 84.0d);
		addValue(ProductType.FixedVisionPvc, Data.MULLION_WIDTH, 1d);
		addValue(ProductType.FixedVisionPvc, Data.RIGHT_MULLION_TO_SASH_PVC, 8.64 / 25.4);
		addValue(ProductType.FixedVisionPvc, Data.LEFT_MULLION_TO_SASH_PVC, 8.64 / 25.4);

		addSpacer(ProductType.FixedVisionPvc, Data.FRAME_TO_MULLION_OFFSET_SPACER, 1.0, 8);

		addProfile(ProductType.FixedVisionPvc, Data.MULLION_PROFILE, new ProfileMullion());

		addProfiles(ProductType.FixedVisionPvc, Data.FRAME_PROFILES, new ProfileFixFrame(), new ProfileFixFrame(), new ProfileFixFrame(), new ProfileFixFrame());

		addSpacer(ProductType.Form, Data.FRAME_TO_GLASS_OFFSET_SPACERS, 1.5, 8);
		addSpacer(ProductType.Form, Data.FRAME_BORDER_SPACERS, 1.875, 8);
		addCornerType(ProductType.Form, Data.FRAME_CORNERTYPE, CornerType.HALF_CORNER, 8);

		// ----------------- View on top -----------------------------

		// addDrawing(ProductType.FixedVisionPvc, Data.DRAWING_FRAME, new CadFrameFix());
		// addDrawing(ProductType.FixedVisionPvc, Data.DRAWING_MULLION, new CadMullion());

		// ****************************************
		// SteelDoor
		// ****************************************
		addValue(ProductType.DOOR, Data.FRAME_STD_SILL_BORDER_SPACERS, 0.5d);
		addValue(ProductType.DOOR, Data.FRAME_JAMB_DEEPIN_STD, 1.75);
		addValue(ProductType.DOOR, Data.FRAME_JAMB_DEEPOUT_STD, 5.5);
		addCornerType(ProductType.DOOR, Data.FRAME_CORNERTYPE, CornerType.HALF_CORNER, CornerType.HALF_CORNER, CornerType.HALF_CORNER, CornerType.HALF_CORNER);
		// addSpacer(ProductType.DOOR, Data.FRAME_BORDER_SPACERS, new Double[] {2.5d, 2.5d, 0d, 2.5d});
		addSpacer(ProductType.DOOR, Data.FRAME_BORDER_SPACERS, new Double[] { 0.5d, 0.5d, 0d, 0.5d });

		addSpacer(ProductType.DOOR, Data.DECORATIVEMOULDLARGE, 4.0, 4);
		addSpacer(ProductType.DOOR, Data.DECORATIVEMOULDSMALL, 2.0, 6);

		// ****************************************
		// Hung PVC
		// ****************************************
		addCornerType(ProductType.HUNG_PVC, Data.SASH_CORNERTYPE, CornerType.HALF_CORNER, 4);
		addSpacer(ProductType.HUNG_PVC, Data.EXT_SASH_BORDER_SPACERS, 1.0, 4);
		addSpacer(ProductType.HUNG_PVC, Data.INT_SASH_BORDER_SPACERS, 1.25, 4);
		addSpacer(ProductType.HUNG_PVC, Data.FRAME_TO_EXT_GLAZINGBEAD_OFFSET_SPACERS, 0.5, 4);

		addSpacer(ProductType.HUNG_PVC, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID, 0.625, 4);
		addSpacer(ProductType.HUNG_PVC, Data.FRAME_TO_MULLION_OFFSET_SPACER, 0d, 4);
		addSpacer(ProductType.HUNG_PVC, Data.FRAME_BORDER_SPACERS, 0.750, 4);
		addSpacer(ProductType.HUNG_PVC, Data.FRAME_TO_GLASS_OFFSET_SPACERS, 1.25, 4);
		addSpacer(ProductType.HUNG_PVC, Data.SASH_TO_GLASS_OFFSET_SPACERS, 0.375, 8);
		addValue(ProductType.HUNG_PVC, Data.MEETINGRAIL_POSITION_OFFSET, 0.375);
		addCornerType(ProductType.HUNG_PVC, Data.FRAME_CORNERTYPE, CornerType.HALF_CORNER, 4);
		addSpacer(ProductType.HUNG_PVC, Data.FRAME_TO_SASH_OFFSET_SPACERS, 0.950d, 4);

		// Slider
		// FRAME

		Double[] spacer = new Double[] { 2.0, 2.0, 2.0, 2.0 };

		addCornerType(ProductType.SLIDER_PVC, Data.FRAME_CORNERTYPE, new CornerType[] { CornerType.HALF_CORNER, CornerType.HALF_CORNER, CornerType.HALF_CORNER,
				CornerType.HALF_CORNER });
		addSpacer(ProductType.SLIDER_PVC, Data.FRAME_BORDER_SPACERS, spacer);
		addSpacer(ProductType.SLIDER_PVC, Data.FRAME_TO_MULLION_OFFSET, spacer);
		addSpacer(ProductType.SLIDER_PVC, Data.FRAME_TO_INT_LMOULDING_OFFSET_SPACERS, spacer);
		addValue(ProductType.SLIDER_PVC, Data.FRAME_TO_INT_SASHSTOP_OFFSET, 1.0);

		addSpacer(ProductType.SLIDER_PVC, Data.FRAME_TO_INT_SASH_OFFSET_SPACERS, spacer);
		addSpacer(ProductType.SLIDER_PVC, Data.FRAME_TO_EXT_LMOULDING_OFFSET_SPACERS, spacer);
		addValue(ProductType.SLIDER_PVC, Data.FRAME_TO_EXT_SASHSTOP_OFFSET, 1.0);

		// SASH
		addCornerType(ProductType.SLIDER_PVC, Data.SASH_CORNERTYPE, new CornerType[] { CornerType.LONG_CORNER, CornerType.SHORT_CORNER, CornerType.LONG_CORNER,
				CornerType.SHORT_CORNER });
		addSpacer(ProductType.SLIDER_PVC, Data.SASH_BORDER_SPACERS, spacer);
		addSpacer(ProductType.SLIDER_PVC, Data.SASH_TO_SCREEN_OFFSET, spacer);
		addValue(ProductType.SLIDER_PVC, Data.SASH_TO_GLASS_OFFSET_SPACERS_MEETINGRAIL, 1.0);
		addSpacer(ProductType.SLIDER_PVC, Data.SASH_TO_GLASS_OFFSET_SPACERS, spacer);
		addValue(ProductType.SLIDER_PVC, Data.SASH_BORDER_SPACERS_MEETINGRAIL, 1.0);
		addValue(ProductType.SLIDER_PVC, Data.INT_SASH_TO_MEETINGRAIL_OFFSET, 1.0);
		addValue(ProductType.SLIDER_PVC, Data.EXT_SASH_TO_MEETINGRAIL_OFFSET, 1.0);
		addSpacer(ProductType.SLIDER_PVC, Data.INT_SASH9722_TO_GLAZINGBEAD_OFFSET_SPACERS, spacer);
		addSpacer(ProductType.SLIDER_PVC, Data.INT_SASH9722_TO_GLASS_OFFSET_SPACERS, spacer);
		addSpacer(ProductType.SLIDER_PVC, Data.INT_SASH9722_TO_SCREEN_OFFSET_SPACERS, spacer);

		// --------------------------------------------------------------
		// PRODUCT GROUP
		// --------------------------------------------------------------
		// Dimension tableau
		// Premier spacer est pour le haut de la shape
		// addSpacer(ProductType.PRODUCT_GROUP, Data.BRICK_MOULD_THICKNESS, new Double[] { 0d, 0d, 0d, 0d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d,
		// 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d });
		// addSpacer(ProductType.PRODUCT_GROUP, Data.BRICK_MOULD_TO_FRAME, new Double[] { -5d, -5d, 0d, -5d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d,
		// 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d });

		// Brick Mould with no sill (rectangle)
		// addSpacer(ProductType.PRODUCT_GROUP, Data.BRICK_MOULD_THICKNESS, new Double[] { 3.00d, 3.00d, 0.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d,
		// 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d });
		// addSpacer(ProductType.PRODUCT_GROUP, Data.BRICK_MOULD_TO_FRAME, new Double[] { 2.75d, 2.75d, 0.00d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d,
		// 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d });

		// Brick Mould all same
		// addSpacer(ProductType.PRODUCT_GROUP, Data.BRICK_MOULD_THICKNESS, new Double[] { 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d,
		// 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d, 3.00d });
		// addSpacer(ProductType.PRODUCT_GROUP, Data.BRICK_MOULD_TO_FRAME, new Double[] { 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d,
		// 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d, 2.75d });
		addSpacer(ProductType.PRODUCT_GROUP, Data.BRICK_MOULD_THICKNESS, 0d, 20);
		addSpacer(ProductType.PRODUCT_GROUP, Data.BRICK_MOULD_TO_FRAME, 0d, 20);

		addValue(ProductType.PRODUCT_GROUP, Data.SECTION_MIN_WIDTH, Units.inch(1d).doubleValue(Units.INCH));
		addValue(ProductType.PRODUCT_GROUP, Data.SECTION_MIN_HEIGHT, Units.inch(1d).doubleValue(Units.INCH));

		addValue(ProductType.PRODUCT_GROUP, Data.PRODUCT_DATA_HEIGHT, Units.inch(100d).doubleValue(Units.INCH));
		addValue(ProductType.PRODUCT_GROUP, Data.PRODUCT_DATA_WIDTH, Units.inch(80d).doubleValue(Units.INCH));
		addValue(ProductType.PRODUCT_GROUP, Data.PRODUCT_DATA_HEIGHT2, Units.inch(65d).doubleValue(Units.INCH));
		addValue(ProductType.PRODUCT_GROUP, Data.PRODUCT_DATA_WIDTH2, Units.inch(50d).doubleValue(Units.INCH));
		addValue(ProductType.PRODUCT_GROUP, Data.PRODUCT_DATA_HEIGHT3, Units.inch(50d).doubleValue(Units.INCH));
		addValue(ProductType.PRODUCT_GROUP, Data.PRODUCT_DATA_DIAMETER, Units.inch(50d).doubleValue(Units.INCH));
	}

	@Override
	public Unit<Length> getUnit()
	{
		return Units.INCH;
	}

	public static void activate()
	{
		System.out.println("ProductData Activated"); //$NON-NLS-1$
	}

}