package com.client360.configuration.wad.constant.product;

import com.client360.configuration.wad.utils.GetterUtility;
import com.netappsid.utils.file.LookupXD;

public class VisionMvGetters
{
	static String ProductDataFile = "productData/VISIONMV.tsv";
	static int COLUMN_NAME = 10;

	public enum ProductDataCode
	{
		PRODUCTDATACODE_A(10), PRODUCTDATACODE_B(20), PRODUCTDATACODE_C(30), PRODUCTDATACODE_D(40), PRODUCTDATACODE_E(50), PRODUCTDATACODE_F0(60), PRODUCTDATACODE_F1(
				70), PRODUCTDATACODE_F2(80), PRODUCTDATACODE_G0(90), PRODUCTDATACODE_G1(100), PRODUCTDATACODE_G2(110), PRODUCTDATACODE_H0(120), PRODUCTDATACODE_H1(
				130), PRODUCTDATACODE_H2(140), PRODUCTDATACODE_I0(150), PRODUCTDATACODE_I1(160), PRODUCTDATACODE_I2(170);

		public int id = 0;

		private ProductDataCode(int id)
		{
			this.id = id;
		}
	}

	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

	// Getter of FRAME_BORDER_SPACERS
	public static Double[] getFrameBorderSpacers()
	{
		Double[] doubles = new Double[] { 0d, 0d, 0d, 0d };
		doubles[0] = GetterUtility.convertStringToDouble(LookupXD.getValue(ProductDataFile, LookupXD.TAB_FILE, COLUMN_NAME,
				ProductDataCode.PRODUCTDATACODE_A.id));
		doubles[1] = doubles[0];
		doubles[2] = doubles[0];
		doubles[3] = doubles[0];
		return doubles;
	}

	// Getter of MULLION_WIDTH
	public static Double getMullionWidth()
	{
		Double dbl = new Double(0);
		dbl = GetterUtility.convertStringToDouble(LookupXD.getValue(ProductDataFile, LookupXD.TAB_FILE, COLUMN_NAME, ProductDataCode.PRODUCTDATACODE_B.id));
		return dbl;
	}

	// Getter of SECTION_MULLION_DEDUCTION
	public static Double[] getSectionMullionDeduction()
	{
		Double[] doubles = new Double[] { 0d, 0d, 0d, 0d };
		doubles[0] = GetterUtility.convertStringToDouble(LookupXD.getValue(ProductDataFile, LookupXD.TAB_FILE, COLUMN_NAME,
				ProductDataCode.PRODUCTDATACODE_C.id));
		doubles[1] = doubles[0];
		doubles[2] = doubles[0];
		doubles[3] = doubles[0];
		return doubles;
	}

	// Getter of SECTION_MULLION_OFFSET
	public static Double getSectionMullionOffset()
	{
		Double dbl = new Double(0);
		dbl = GetterUtility.convertStringToDouble(LookupXD.getValue(ProductDataFile, LookupXD.TAB_FILE, COLUMN_NAME, ProductDataCode.PRODUCTDATACODE_D.id));
		return dbl;
	}

	// Getter of SASH_BORDER_SPACERS
	public static Double[] getSashBorderSpacers()
	{
		Double[] doubles = new Double[] { 0d, 0d, 0d, 0d };
		doubles[0] = GetterUtility.convertStringToDouble(LookupXD.getValue(ProductDataFile, LookupXD.TAB_FILE, COLUMN_NAME,
				ProductDataCode.PRODUCTDATACODE_E.id));
		doubles[1] = doubles[0];
		doubles[2] = doubles[0];
		doubles[3] = doubles[0];
		return doubles;
	}

	// Getter of FRAME_TO_SASH_OFFSET_SPACERS
	public static Double[] getFrameToSashOffsetSpacers()
	{
		Double[] doubles = new Double[] { 0d, 0d, 0d, 0d };
		doubles[0] = null;
		doubles[0] = GetterUtility.convertStringToDouble(LookupXD.getValue(ProductDataFile, LookupXD.TAB_FILE, COLUMN_NAME,
				ProductDataCode.PRODUCTDATACODE_F0.id));
		doubles[1] = GetterUtility.convertStringToDouble(LookupXD.getValue(ProductDataFile, LookupXD.TAB_FILE, COLUMN_NAME,
				ProductDataCode.PRODUCTDATACODE_F1.id));
		doubles[2] = GetterUtility.convertStringToDouble(LookupXD.getValue(ProductDataFile, LookupXD.TAB_FILE, COLUMN_NAME,
				ProductDataCode.PRODUCTDATACODE_F2.id));
		doubles[3] = doubles[1];
		return doubles;
	}

	// Getter of FRAME_TO_SCREEN_OFFSET_SPACERS
	public static Double[] getFrameToScreenOffsetSpacers()
	{
		Double[] doubles = new Double[] { 0d, 0d, 0d, 0d };
		doubles[0] = GetterUtility.convertStringToDouble(LookupXD.getValue(ProductDataFile, LookupXD.TAB_FILE, COLUMN_NAME,
				ProductDataCode.PRODUCTDATACODE_G0.id));
		doubles[1] = GetterUtility.convertStringToDouble(LookupXD.getValue(ProductDataFile, LookupXD.TAB_FILE, COLUMN_NAME,
				ProductDataCode.PRODUCTDATACODE_G1.id));
		doubles[2] = GetterUtility.convertStringToDouble(LookupXD.getValue(ProductDataFile, LookupXD.TAB_FILE, COLUMN_NAME,
				ProductDataCode.PRODUCTDATACODE_G2.id));
		doubles[3] = doubles[1];
		return doubles;
	}

	// Getter of SASH_TO_GLASS_OFFSET_SPACERS
	public static Double[] getSashToGlassOffsetSpacers()
	{
		Double[] doubles = new Double[] { 0d, 0d, 0d, 0d };
		doubles[0] = GetterUtility.convertStringToDouble(LookupXD.getValue(ProductDataFile, LookupXD.TAB_FILE, COLUMN_NAME,
				ProductDataCode.PRODUCTDATACODE_H0.id));
		doubles[1] = GetterUtility.convertStringToDouble(LookupXD.getValue(ProductDataFile, LookupXD.TAB_FILE, COLUMN_NAME,
				ProductDataCode.PRODUCTDATACODE_H1.id));
		doubles[2] = GetterUtility.convertStringToDouble(LookupXD.getValue(ProductDataFile, LookupXD.TAB_FILE, COLUMN_NAME,
				ProductDataCode.PRODUCTDATACODE_H2.id));
		doubles[3] = doubles[1];
		return doubles;
	}

	// Getter of SASH_TO_GLAZINGBEAD_OFFSET_SPACERS
	public static Double[] getSashToGlazingBeadOffsetSpacers()
	{
		Double[] doubles = new Double[] { 0d, 0d, 0d, 0d };
		doubles[0] = GetterUtility.convertStringToDouble(LookupXD.getValue(ProductDataFile, LookupXD.TAB_FILE, COLUMN_NAME,
				ProductDataCode.PRODUCTDATACODE_I0.id));
		doubles[1] = GetterUtility.convertStringToDouble(LookupXD.getValue(ProductDataFile, LookupXD.TAB_FILE, COLUMN_NAME,
				ProductDataCode.PRODUCTDATACODE_I1.id));
		doubles[2] = GetterUtility.convertStringToDouble(LookupXD.getValue(ProductDataFile, LookupXD.TAB_FILE, COLUMN_NAME,
				ProductDataCode.PRODUCTDATACODE_I2.id));
		doubles[3] = doubles[1];
		return doubles;
	}

}
