// $Id: Dimension.java,v 1.10 2011-08-10 13:49:28 sboule Exp $

package com.client360.configuration.wad;

import com.netappsid.erp.configurator.Configurable;

public class Dimension extends com.client360.configuration.wad.DimensionBase
{

	public Dimension(Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

	}

	@Override
	public void afterFormChanged(com.netappsid.commonutils.geo.Form form)
	{
		super.afterFormChanged(form);
		if (form != null)
		{
			if (getParent() instanceof ProductTemplate)
			{
				// identifyFormByGeo(false, true);
			}
		}
	}
}
