// $Id: $

package com.client360.configuration.wad;

import com.client360.configuration.wad.profiles.ProfileRollingShutterBox;
import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.StandardColor;

public class RollingShutterBox extends com.client360.configuration.wad.RollingShutterBoxBase
{
	private static final long serialVersionUID = 7903932692418766619L;

	private static final String RESOURCE_MASONRY_BOX_JPG = "images/jpgWoods/Redwood1r(30).jpg";

	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public RollingShutterBox(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		setDefaultHeight(Units.inch(10d));
		setProfile(new ProfileRollingShutterBox());
	}

	@Override
	public void afterDisplayMasonryBoxChanged(Boolean displayMasonryBox)
	{
		super.afterDisplayMasonryBoxChanged(displayMasonryBox);

		setDefaultMasonryBox(getSafeBoolean(displayMasonryBox) ? RESOURCE_MASONRY_BOX_JPG : "");
	}

	@Override
	public void afterMasonryBoxChanged(String masonryBox)
	{
		super.afterMasonryBoxChanged(masonryBox);

		boolean visible = !getSafeString(masonryBox).isEmpty();

		setVisible(PROPERTYNAME_MASONRYBOXOFFSETLEFT, visible);
		setVisible(PROPERTYNAME_MASONRYBOXOFFSETRIGHT, visible);
		setVisible(PROPERTYNAME_MASONRYBOXOFFSETTOP, visible);
	}

	@Override
	protected Class<? extends StandardColor> getStandardColorClass()
	{
		return com.client360.configuration.wad.StandardColor.class;
	}
}
