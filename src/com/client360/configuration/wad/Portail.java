// $Id: Portail.java,v 1.5 2011-06-10 14:23:19 sboule Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.client360.configuration.wad.gates.PortailCaravelleM40;
import com.netappsid.commonutils.geo.CornerType;
import com.netappsid.commonutils.math.Units;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.rendering.common.advanced.Drawing;
import com.netappsid.wadconfigurator.Door;
import com.netappsid.wadconfigurator.DoorMullionFix;

public class Portail extends com.client360.configuration.wad.PortailBase
{
	public Portail(Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
		setDoor(makeArray(PortailDoor.class, 1, this));

		PortailCaravelleM40 test = new PortailCaravelleM40();
		getDoor()[0].setDoorModel(PortailCaravelleM40.class);

		getDimension().setForm(getDefaultForm());
	}

	@Override
	public DoorMullionFix[] makeArrayDoorMullionFix(int nbMullionFix)
	{
		return null;
	}

	@Override
	public Door[] makeArrayDoor(int nbDoor)
	{
		return null;
	}

	@Override
	public CornerType[] getCornerType()
	{
		return ProductData.getCornerType(ProductType.DOOR, Data.FRAME_CORNERTYPE);
	}

	@Override
	public Measure<Length> getTopSpacerOS()
	{
		return Units.mm(0);
	}

	@Override
	public Measure<Length> getTopSpacer()
	{
		return Units.mm(0);
	}

	@Override
	public Measure<Length> getJambWidthRightOS()
	{
		return Units.mm(0);
	}

	@Override
	public Measure<Length> getJambWidthRight()
	{
		return Units.mm(0);
	}

	@Override
	public Measure<Length> getJambWidthLeftOS()
	{
		return Units.mm(0);
	}

	@Override
	public Measure<Length> getJambWidthLeft()
	{
		return Units.mm(0);
	}

	@Override
	public Measure<Length> getMullionFixWidthOS()
	{
		return Units.mm(0);
	}

	@Override
	public Measure<Length> getMullionFixWidth()
	{
		return Units.mm(0);
	}

	@Override
	public Measure<Length> getMullionMobileWidthOS()
	{
		return Units.mm(0);
	}

	@Override
	public Measure<Length> getMullionMobileWidth()
	{
		return Units.mm(0);
	}

	@Override
	public Measure<Length> getSillThickness()
	{
		return Units.mm(0);
	}

	@Override
	public Drawing getCad()
	{
		return null;
	}

	@Override
	public String productCode()
	{
		return null;
	}

	@Override
	public String colorChartNameExterior()
	{
		return null;
	}

	@Override
	public String colorChartNameInterior()
	{
		return null;
	}

	// Add custom overrides here
}
