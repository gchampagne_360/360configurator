package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.netappsid.erp.configurator.Configurable;

public class ProductTemplate extends ProductTemplateBase
{
	private static final long serialVersionUID = -6470172116858960069L;

	public ProductTemplate(Configurable parent)
	{
		super(parent);
	}

	/**
	 * @return the base price for the object (This is a test)
	 */
	@Override
	public double getBasePrice()
	{
		return 1d;
	}

	@Override
	/**
	 * @return the minimum ProductTemplate height from the product data.
	 */
	public Measure<Length> getMinHeight()
	{
		return ProductData.getValue(ProductType.PRODUCT_GROUP, Data.SECTION_MIN_HEIGHT);
	}

	@Override
	/**
	 * @return the minimum ProductTemplate width from the product data.
	 */
	public Measure<Length> getMinWidth()
	{
		return ProductData.getValue(ProductType.PRODUCT_GROUP, Data.SECTION_MIN_WIDTH);
	}
}
