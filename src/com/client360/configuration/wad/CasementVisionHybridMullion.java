// $Id: CasementVisionHybridMullion.java,v 1.2 2011-04-27 12:09:13 jddaigle Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.rendering.common.advanced.Drawing;

public class CasementVisionHybridMullion extends com.client360.configuration.wad.CasementVisionHybridMullionBase
{

	public CasementVisionHybridMullion(Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
	}

	@Override
	public Measure<Length> getLeftSashPositionOffset()
	{
		return ProductData.getValue(ProductType.CASEMENT, Data.MULLION_TO_SASH);
	}

	@Override
	public Measure<Length> getRightSashPositionOffset()
	{
		return ProductData.getValue(ProductType.CASEMENT, Data.MULLION_TO_SASH);
	}

	@Override
	public Drawing getCad()
	{
		return ProductData.getDrawing(ProductType.CASEMENT_VISION_PVC_IN, Data.DRAWING_MULLION);
	}

	@Override
	public Measure<Length> getWidth()
	{
		return ProductData.getValue(ProductType.CASEMENT, Data.MULLION_WIDTH);
	}

	@Override
	public Measure<Length> getWidthOS()
	{
		return ProductData.getValue(ProductType.CASEMENT, Data.MULLION_WIDTH_OS);
	}

	@Override
	public Measure<Length> getLeftGlassPositionOffset()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length> getRightGlassPositionOffset()
	{
		// TODO Auto-generated method stub
		return null;
	}
}
