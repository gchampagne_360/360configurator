// $Id: CasementMain.java,v 1.4 2011-04-27 12:09:13 jddaigle Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.netappsid.commonutils.math.Units;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.rendering.common.advanced.Profile;

public abstract class CasementMain extends com.client360.configuration.wad.CasementMainBase
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// Default constructor
	public CasementMain(Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		makeInvisible(PROPERTYNAME_ENERGYSTAR);
		makeInvisible(PROPERTYNAME_VERTICAL);
		makeInvisible(PROPERTYNAME_RENDERINGNUMBER);
		makeVisible(PROPERTYNAME_DIMENSION);

		 setDefaultProfiles(getClientProfiles());
	}
	
	public final Profile[] getClientProfiles()
	{
		return ProductData.getProfiles(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_PROFILES);
	}

	@Override
	public Measure<Length>[] getGlazingBeadThickness()
	{
		Measure<Length>[] glazingBeadThickness = (Measure<Length>[]) new Measure[8];
		for (int i = 0; i < glazingBeadThickness.length; i++)
		{
			glazingBeadThickness[i] = Units.inch(0.5d);
		} /* end for */

		return glazingBeadThickness;
	}
}
