// $Id: RollingShuttersPVC.java,v 1.1 2011-04-27 12:09:14 jddaigle Exp $

package com.client360.configuration.wad;

import com.netappsid.wadconfigurator.Assembly;
import com.netappsid.wadconfigurator.RollingShutterBox;
import com.netappsid.wadconfigurator.RollingShutterSection;
import com.netappsid.wadconfigurator.StandardColor;

public class RollingShuttersPVC extends com.client360.configuration.wad.RollingShuttersPVCBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public RollingShuttersPVC(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		Combination assembly = ((Combination) getParent(Assembly.class));
		if (assembly != null)
		{
			assembly.makeInvisible(Combination.PROPERTYNAME_CAPPINGEXTERIOR);
			assembly.makeInvisible(Combination.PROPERTYNAME_CAPPINGINTERIOR);
			assembly.makeInvisible(Combination.PROPERTYNAME_EXTERIORCOLOR);
			assembly.makeInvisible(Combination.PROPERTYNAME_FRAME);
			assembly.makeInvisible(Combination.PROPERTYNAME_INTERIORCOLOR);
			assembly.makeInvisible(Combination.PROPERTYNAME_PINKGLASS);
		}
	}

	@Override
	public String toString()
	{
		return clientTraductionBundle.getString("RollingShutterPVC");
	}

	@Override
	protected Class<? extends RollingShutterBox> getRollingShutterBoxClass()
	{
		return com.client360.configuration.wad.RollingShutterBox.class;
	}

	@Override
	protected Class<? extends StandardColor> getStandardColorClass()
	{
		return com.client360.configuration.wad.StandardColor.class;
	}

	@Override
	protected Class<? extends RollingShutterSection> getRollingShutterSectionClass()
	{
		return com.client360.configuration.wad.RollingShutterSection.class;
	}
}
