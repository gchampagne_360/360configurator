// $Id: ChoiceHandleSet.java,v 1.2 2011-04-27 12:09:13 jddaigle Exp $

package com.client360.configuration.wad;

public class ChoiceHandleSet extends com.client360.configuration.wad.ChoiceHandleSetBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public ChoiceHandleSet(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	// Add custom overrides here
}
