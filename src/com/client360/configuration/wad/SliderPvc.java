// $Id: SliderPvc.java,v 1.12 2011-08-10 13:49:28 sboule Exp $

package com.client360.configuration.wad;

import static com.client360.configuration.wad.utils.FenergicUtils.PVCColorChart;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.cad.CadSliderFrame;
import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.netappsid.commonutils.geo.CornerType;
import com.netappsid.commonutils.math.Units;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.rendering.common.advanced.Drawing;
import com.netappsid.wadconfigurator.MeetingRail;
import com.netappsid.wadconfigurator.Sash;
import com.netappsid.wadconfigurator.SliderSash;
import com.netappsid.wadconfigurator.SlidingProductMeetingRail;

public class SliderPvc extends com.client360.configuration.wad.SliderPvcBase
{

	public SliderPvc(Configurable parent)
	{
		super(parent);
	}

	// Add custom overrides here

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
		makeVisible(PROPERTYNAME_INTERIORSASH);
		makeVisible(PROPERTYNAME_HALFEXTERIOR);
		makeVisible(PROPERTYNAME_HALFINTERIOR);

		setDefaultHalfExterior(false);
		setDefaultHalfInterior(false);
	}

	// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	@Override
	public int getNbSash()
	{
		return super.getNbSash();
	}

	@Override
	public boolean isExteriorColorVisible()
	{
		return false;
	}

	@Override
	public boolean isInteriorColorVisible()
	{
		return false;
	}

	public boolean isChoiceOpeningTypeEnabled()
	{
		return false;
	}

	public boolean isChoiceMosquitoEnabled()
	{
		return false;
	}

	@Override
	public String colorChartNameExterior()
	{
		return PVCColorChart;
	}

	@Override
	public String colorChartNameInterior()
	{
		return PVCColorChart;
	}

	public boolean isHalfExteriorEnabled()
	{
		return true;
	}

	public boolean isRenderingNumberEnabled()
	{
		return false;
	}

	public boolean isHalfInteriorEnabled()
	{
		return true;
	}

	public boolean isExteriorColorColorChartEnabled()
	{
		return false;
	}

	public boolean isExteriorColorNonStandardColorEnabled()
	{
		return false;
	}

	public boolean isInteriorColorColorChartEnabled()
	{
		return false;
	}

	public boolean isInteriorColorNonStandardColorEnabled()
	{
		return false;
	}

	private static Double[] spacer = new Double[] { 2.0, 2.0, 2.0, 2.0 };
	private static Double[] rail = new Double[] { 0.0, 2.0, 0.0 };

	private final CadSliderFrame frameCad = new CadSliderFrame();

	@Override
	public void activateListener()
	{
		super.activateListener();
	}

	@Override
	public Sash[] sashMakeArray(int nbSections)
	{
		Sash[] sashs = makeConfigurableArray(SliderPvcSash.class, nbSections, this);
		for (Sash sash : sashs)
		{
			((SliderPvcSash) sash).setRail(1);
		}
		return sashs;
	}

	@Override
	public SliderSash[] interiorSashMakeArray(int nbSections)
	{
		// SliderSash[] sashsINT = makeConfigurableArray(SliderPvcSash.class, nbSections, this);
		// for (SliderSash sliderSash : sashsINT)
		// {
		// sliderSash.setRail(0);
		// }
		// return sashsINT;
		return null;
	}

	@Override
	public MeetingRail[] meetingRailMakeArray(int nbSections)
	{
		return makeConfigurableArray(SliderPvcMeetingRail.class, nbSections, this);
	}

	@Override
	public String toString()
	{
		return getRenderingNumber() + " - " + clientTraductionBundle.getString("SliderPvc");
	}

	@Override
	public String getName()
	{
		return clientTraductionBundle.getString("SliderPvc");
	}

	// -------------------------------------------------------------------------------------------------
	// -----------------------------------C O R N E R T Y P E-----------------------------------------
	// -------------------------------------------------------------------------------------------------

	@Override
	public CornerType[] getCornerType()
	{
		return ProductData.getCornerType(ProductType.SLIDER_PVC, Data.FRAME_CORNERTYPE);

	}

	// ------------------------------------------------------------------------------------------------
	// ----------------------------------B O R D E R S P A C E R S-----------------------------------
	// ------------------------------------------------------------------------------------------------

	@Override
	public Measure<Length>[] getFrameBorder()
	{
		return ProductData.getSpacer(ProductType.SLIDER_PVC, Data.FRAME_BORDER_SPACERS);
	}

	// ---------------------------------------------------------------------------------------------------
	// ----------------------------------O F F S E T G E T T E R S--------------------------------------
	// ---------------------------------------------------------------------------------------------------

	@Override
	public Measure<Length>[] getSashOffset(int railNumber)
	{
		Measure<Length>[] FRAME_TO_INT_SASH_OFFSET_SPACERS_VALUES = null;

		Measure[] value = { Measure.valueOf(51.59375, Units.MM), Measure.valueOf(30d, Units.MM), Measure.valueOf(51.59375, Units.MM),
				Measure.valueOf(30d, Units.MM) };

		FRAME_TO_INT_SASH_OFFSET_SPACERS_VALUES = value;

		return FRAME_TO_INT_SASH_OFFSET_SPACERS_VALUES;
	}

	// @Override
	// public Measure<Length> getRightPocketOffset()
	// {
	// return Units.mm(500);
	// }
	//
	// @Override
	// public Measure<Length> getLeftPocketOffset()
	// {
	// return Units.mm(500);
	// }

	@Override
	public Measure<Length>[] getMullionOffset()
	{
		return null;// ProductData.getSpacer(ProductType.SLIDER_PVC, Data.FRAME_TO_MULLION_OFFSET);
	}

	@Override
	public Measure<Length>[] getLMouldingOffset()
	{
		return ProductData.getSpacer(ProductType.SLIDER_PVC, Data.FRAME_TO_INT_LMOULDING_OFFSET_SPACERS);
	}

	@Override
	public Measure<Length>[] getLMouldingExteriorOffset()
	{
		return ProductData.getSpacer(ProductType.SLIDER_PVC, Data.FRAME_TO_EXT_LMOULDING_OFFSET_SPACERS);
	}

	@Override
	public Measure<Length> getSashStopOffset()
	{
		return ProductData.getValue(ProductType.SLIDER_PVC, Data.FRAME_TO_INT_SASHSTOP_OFFSET);
	}

	@Override
	public Measure<Length> getSashStopExteriorOffset()
	{
		return ProductData.getValue(ProductType.SLIDER_PVC, Data.FRAME_TO_EXT_SASHSTOP_OFFSET);

	}

	public boolean isChoiceOpeningSideVisible()
	{
		return false;
	}

	@Override
	public Drawing getCad()
	{

		return null;// frameCad;
	}

	@Override
	public Measure<Length>[] getSashOffset(int railNumber, String sashRole)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length>[] getGlassOffset()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SlidingProductMeetingRail[] interiorMeetingRailMakeArray(int nbSections)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(Object[] params)
	{
		// TODO Auto-generated method stub

	}
}