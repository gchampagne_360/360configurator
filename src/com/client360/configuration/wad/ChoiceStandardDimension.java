// $Id: ChoiceStandardDimension.java,v 1.3 2011-04-27 12:09:13 jddaigle Exp $

package com.client360.configuration.wad;

import com.netappsid.erp.configurator.Configurable;

public class ChoiceStandardDimension extends com.client360.configuration.wad.ChoiceStandardDimensionBase
{

	public ChoiceStandardDimension(Configurable parent)
	{
		super(parent);
	}

	@Override
	public String toString()
	{
		return "P: " + getNbDoor() + " S: " + getNbSide();
	}

	public String deQuery()
	{
		return "select * from CHOICESTD where TOTAL_WIDTH > 39";
	}

	// Add custom overrides here
}
