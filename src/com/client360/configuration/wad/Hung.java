// $Id: Hung.java,v 1.5 2011-04-27 12:09:14 jddaigle Exp $

package com.client360.configuration.wad;

import static com.client360.configuration.wad.utils.FenergicUtils.PVCColorChart;

import com.netappsid.wadconfigurator.Division;

public class Hung extends com.client360.configuration.wad.HungBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public Hung(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	@Override
	public Division[] divisionMakeArray(int nbDivisions)
	{
		return makeArray(HungDivision.class, nbDivisions, this);
	}

	@Override
	public String colorChartNameExterior()
	{
		return PVCColorChart;
	}

	@Override
	public String colorChartNameInterior()
	{
		return PVCColorChart;
	}

	@Override
	public String productCode()
	{
		return "HPVC";
	}

	@Override
	public boolean useAutomaticGrilleAlignement()
	{
		return false;
	}

	@Override
	public String toString()
	{
		return getRenderingNumber() + " - " + clientTraductionBundle.getString("HungPvc");
	}
}
