// $Id: CasementVisionHybridSash.java,v 1.5 2011-09-07 19:04:33 sboule Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.netappsid.commonutils.geo.CornerType;
import com.netappsid.commonutils.math.MathFunction;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.rendering.common.advanced.Drawing;
import com.netappsid.wadconfigurator.SashSection;

public class CasementVisionHybridSash extends com.client360.configuration.wad.CasementVisionHybridSashBase
{
	public CasementVisionHybridSash(Configurable parent)
	{
		super(parent);
	}

	private static final long serialVersionUID = 7811154307557476129L;

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
	}

	@Override
	public Measure<Length> getSectionMaxWidth()
	{
		return null;
	}

	@Override
	public Measure<Length> getSectionMinWidth()
	{
		return null;
	}

	// @Override
	// public SashSection[] sashSectionMakeArray(int nbSections)
	// {
	// return makeConfigurableArray(CasementVisionPvcSashSection.class,nbSections, this);
	// }

	@Override
	public CornerType[] getCornerType()
	{
		return ProductData.getCornerType(ProductType.CASEMENT, Data.SASH_CORNERTYPE);
	}

	/**
	 * @see com.client360.configuration.wad.CasementMainSash#getSashFrameThickness()
	 */
	@Override
	public Measure<Length>[] getSashFrameThickness()
	{
		return ProductData.getSpacer(ProductType.CASEMENT, Data.SASH_BORDER_SPACERS);
	}

	/**
	 * @see com.netappsid.wadconfigurator.Sash#getGlazingBeadThickness()
	 */
	@Override
	public Measure<Length>[] getGlazingBeadThickness()
	{
		return ProductData.getSpacer(ProductType.CASEMENT, Data.GLAZING_BEAD_THICKNESS);
	}

	@Override
	public Drawing getCad()
	{
		return null;
	}

	@Override
	public Measure<Length>[] getVisibleGlassOffsetSpacer()
	{
		return (Measure<Length>[]) MathFunction.MeasureArrayAddition(getSashFrameThickness(), getGlazingBeadThickness());
	}

	@Override
	public SashSection sashSectionClass()
	{
		return new CasementVisionPvcSashSection(this);
	}

}
