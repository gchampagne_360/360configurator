// $Id: $

package com.client360.configuration.wad;

import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.StandardColor;

public class RollingShutterGuideRail extends com.client360.configuration.wad.RollingShutterGuideRailBase
{
	private static final long serialVersionUID = -8369936424568235907L;

	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
    public RollingShutterGuideRail(com.netappsid.erp.configurator.Configurable parent)
    {
        super(parent);
    }
    
    @Override
    public void setDefaultValues()
    {
    	super.setDefaultValues();
    	
    	setDefaultWidth(Units.inch(1d));
    }

	@Override
	protected Class<? extends StandardColor> getStandardColorClass()
	{
		return com.client360.configuration.wad.StandardColor.class;
	}
}
