// $Id: $

package com.client360.configuration.wad;

public class ChoiceColor extends com.client360.configuration.wad.ChoiceColorBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public ChoiceColor(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	@Override
	public String toString()
	{
		return getDescription();
	}
}
