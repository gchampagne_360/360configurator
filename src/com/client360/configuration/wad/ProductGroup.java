package com.client360.configuration.wad;

import java.util.List;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.client360.configuration.wad.enums.ChoiceHungFrame;
import com.netappsid.commonutils.geo.enums.Orientation;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.common.enums.ChoiceFlip;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.wadconfigurator.enums.ChoiceForm;
import com.netappsid.wadconfigurator.enums.ChoiceOpeningType;
import com.netappsid.wadconfigurator.enums.ChoicePresetWindowType;

public class ProductGroup extends ProductGroupBase
{
	private static final long serialVersionUID = -5099952442807724064L;

	public ProductGroup(Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		getDimension().setDefaultWidth(ProductData.getValue(ProductType.PRODUCT_GROUP, Data.PRODUCT_DATA_WIDTH));
		getDimension().setDefaultHeight(ProductData.getValue(ProductType.PRODUCT_GROUP, Data.PRODUCT_DATA_HEIGHT));
		getDimension().setDefaultWidth2(ProductData.getValue(ProductType.PRODUCT_GROUP, Data.PRODUCT_DATA_WIDTH2));
		getDimension().setDefaultHeight2(ProductData.getValue(ProductType.PRODUCT_GROUP, Data.PRODUCT_DATA_HEIGHT2));
		getDimension().setDefaultHeight3(ProductData.getValue(ProductType.PRODUCT_GROUP, Data.PRODUCT_DATA_HEIGHT2));
	}

	public void loadRocket()
	{
		// 1- Set the desired dimensions
		getDimension().setDefaultHeight(Units.inch(100d));
		getDimension().setDefaultWidth(Units.inch(120d));

		// 2- Split the form
		// Cursor position is based on the ProductGroup top left corner
		splitForm(Orientation.HORIZONTAL, Units.inch(60d), Units.inch(50d));
		splitForm(Orientation.VERTICAL, Units.inch(35d), Units.inch(20d));
		splitForm(Orientation.VERTICAL, Units.inch(85d), Units.inch(20d));
		splitForm(Orientation.HORIZONTAL, Units.inch(20d), Units.inch(25d));
		splitForm(Orientation.HORIZONTAL, Units.inch(100d), Units.inch(25d));

		// 3- Insert sub forms
		// Add an elongatedArc in the top central form
		ProductGroup pg2 = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, null);
		pg2.setForm("ARCHFORM_N_ELONGATEDARC");
		bindConfigurableToTemplate(1, pg2);

		// Add a trapeze in the top left and right forms
		ProductGroup pg3 = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, null);
		pg3.setForm("TRAPEZE_N_RECTANGLE");
		bindConfigurableToTemplate(3, pg3);

		ProductGroup pg4 = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, null);
		pg4.setForm("TRAPEZE_N_RECTANGLE");
		bindConfigurableToTemplate(4, pg4);

		List<? extends Configurable> templates = getConfigurables();
		// 4- Flip the trapeze on the top right corner
		// A sub form can be accessed thought the "template2" getter.
		((ProductTemplate) templates.get(4)).getTemplate2().getDimension().setChoiceFlip(ChoiceFlip.FLIPHORIZONTAL);

		// 5- Insert product in each form
		// Index starts to 0 in the top left corner and increment from left to right, top to bottom.
		bindConfigurableToTemplate(1, new CasementVisionHybrid(templates.get(1)));
		bindConfigurableToTemplate(3, new CasementVisionHybrid(templates.get(3)));
		bindConfigurableToTemplate(4, new CasementVisionHybrid(templates.get(4)));
		bindConfigurableToTemplate(5, new CasementVisionHybrid(templates.get(5)));

		// removeBrickMouldSill();
	}

	public void loadChibooki()
	{
		// 1- Set the desired dimensions
		getDimension().setDefaultHeight(Units.inch(100d));
		getDimension().setDefaultWidth(Units.inch(120d));

		// 2- Split the form
		// Cursor position is based on the ProductGroup top left corner
		splitForm(Orientation.HORIZONTAL, Units.inch(60d), Units.inch(30d));
		splitForm(Orientation.VERTICAL, Units.inch(30d), Units.inch(10d));
		splitForm(Orientation.VERTICAL, Units.inch(90d), Units.inch(10d));
		splitForm(Orientation.VERTICAL, Units.inch(30d), Units.inch(50d));
		splitForm(Orientation.VERTICAL, Units.inch(90d), Units.inch(50d));

		// 3- Insert sub forms
		// Add an elongatedArc in the top central form
		ProductGroup pg2 = Configurable.createRedirectedConfigurableInstance(ProductGroup.class, getConfigurables().get(1));
		pg2.setForm(ChoiceForm.F05_ELONGATEDARC);
		bindConfigurableToTemplate(1, pg2);

		// Adjust size
		((ProductTemplate) getConfigurables().get(1)).getTemplate2().getDimension().setDefaultHeight2(Units.inch(0d));

		// 5- Insert product in each form
		// Index starts to 0 in the top left corner and increment from left to right, top to bottom.
		bindConfigurableToTemplate(3, new CasementVisionHybrid(getConfigurables().get(3)));
		bindConfigurableToTemplate(4, new CasementVisionHybrid(getConfigurables().get(4)));
		bindConfigurableToTemplate(5, new CasementVisionHybrid(getConfigurables().get(5)));
		bindConfigurableToTemplate(1, new CasementVisionHybrid(getConfigurables().get(1)));

		removeBrickMouldSill();
	}

	public void loadElongatedArc6Windows()
	{
		// 1- Set the desired dimensions
		getDimension().setDefaultHeight(Units.inch(60d));
		getDimension().setDefaultWidth(Units.inch(72d));
		getDimension().setDefaultHeight2(Units.inch(50d));

		// 2- Split the form
		// Cursor position is based on the ProductGroup top left corner
		splitForm(Orientation.HORIZONTAL, Units.inch(30d), Units.inch(20d));
		splitForm(Orientation.VERTICAL, Units.inch(24d), Units.inch(10d));
		splitForm(Orientation.VERTICAL, Units.inch(48d), Units.inch(10d));
		splitForm(Orientation.VERTICAL, Units.inch(24d), Units.inch(50d));
		splitForm(Orientation.VERTICAL, Units.inch(48d), Units.inch(50d));

		// 5- Insert product in each form
		// Index starts to 0 in the top left corner and increment from left to right, top to bottom.
		for (int i = 0; i < 6; i++)
		{
			bindConfigurableToTemplate(i, new CasementVisionHybrid(getConfigurables().get(i)));
		}

		removeBrickMouldSill();
	}

	public void loadWallWindow()
	{
		// 1- Set the desired dimensions
		getDimension().setDefaultHeight(Units.inch(120d));
		getDimension().setDefaultWidth(Units.inch(120d));

		// 2- Split the form
		// Cursor position is based on the ProductGroup top left corner
		splitForm(Orientation.HORIZONTAL, Units.inch(50d), Units.inch(40d));
		splitForm(Orientation.HORIZONTAL, Units.inch(50d), Units.inch(80d));
		splitForm(Orientation.VERTICAL, Units.inch(40d), Units.inch(20d));
		splitForm(Orientation.VERTICAL, Units.inch(80d), Units.inch(20d));
		splitForm(Orientation.VERTICAL, Units.inch(40d), Units.inch(60d));
		splitForm(Orientation.VERTICAL, Units.inch(80d), Units.inch(60d));
		splitForm(Orientation.VERTICAL, Units.inch(40d), Units.inch(100d));
		splitForm(Orientation.VERTICAL, Units.inch(80d), Units.inch(100d));

		// 5- Insert product in each form
		// Index starts to 0 in the top left corner and increment from left to right, top to bottom.
		for (int i = 0; i < 9; i++)
		{
			bindConfigurableToTemplate(i, new CasementVisionHybrid(getConfigurables().get(i)));
		}
	}

	public void loadRollingShutterPVC()
	{
		// 1- Set the desired dimensions
		getDimension().setDefaultHeight(Units.inch(60d));
		getDimension().setDefaultWidth(Units.inch(40d));

		// 2- Split the form
		// Cursor position is based on the ProductGroup top left corner
		splitForm(Orientation.HORIZONTAL, Units.inch(20d), Units.inch(10d));

		// 5- Insert product in each form
		// Index starts to 0 in the top left corner and increment from left to right, top to bottom.
		bindConfigurableToTemplate(0, new RollingShuttersPVC(getConfigurables().get(0)));
	}

	public void loadCasementPVC()
	{
		getDimension().setDefaultHeight(Units.inch(48d));
		getDimension().setDefaultWidth(Units.inch(48d));

		CasementVisionPvc casement = new CasementVisionPvc(getConfigurables().get(0));
		casement.setNbSections(1);
		casement.setDefaultChoicePresetWindowType(ChoicePresetWindowType.CASEMENT);
		bindConfigurableToTemplate(0, casement);
	}

	public void loadCasementHybrid()
	{
		getDimension().setDefaultHeight(Units.inch(48d));
		getDimension().setDefaultWidth(Units.inch(48d));

		CasementVisionHybrid casement = new CasementVisionHybrid(getConfigurables().get(0));
		casement.setNbSections(1);
		casement.setDefaultChoicePresetWindowType(ChoicePresetWindowType.CASEMENT);
		bindConfigurableToTemplate(0, casement);
	}

	public void loadSliderPVC()
	{
		getDimension().setDefaultHeight(Units.inch(36d));
		getDimension().setDefaultWidth(Units.inch(60d));

		SliderPvc slider = new SliderPvc(getConfigurables().get(0));
		slider.setNbSections(2);
		bindConfigurableToTemplate(0, slider);
	}

	public void loadHungPVC()
	{
		getDimension().setDefaultHeight(Units.inch(60d));
		getDimension().setDefaultWidth(Units.inch(36d));

		Hung hung = new Hung(getConfigurables().get(0));
		hung.setDefaultNbSections(1);
		hung.setChoiceHungFrame(ChoiceHungFrame.F_8006);
		hung.setChoiceOpeningType(ChoiceOpeningType.SIMPLE);
		bindConfigurableToTemplate(0, hung);
	}

	public void loadSteelDoor()
	{
		getDimension().setDefaultHeight(Units.inch(80d));
		getDimension().setDefaultWidth(Units.inch(42d));

		SteelDoor steelDoor = new SteelDoor(getConfigurables().get(0));
		bindConfigurableToTemplate(0, steelDoor);
		// steelDoor.setDimReadOnly(false);
	}

	@Override
	public Measure<Length>[] getDefaultOverallToFrameOffsets()
	{
		return ProductData.getSpacer(ProductType.PRODUCT_GROUP, Data.BRICK_MOULD_TO_FRAME);
	}

	@Override
	public Class<? extends Module> getModulesClass()
	{
		return Module.class;
	}

	@Override
	public Class<? extends Mullion> getMullionsClass()
	{
		return Mullion.class;
	}

	@Override
	public Measure<Length>[] getDefaultOverallTotalOffsets()
	{
		return ProductData.getSpacer(ProductType.PRODUCT_GROUP, Data.BRICK_MOULD_THICKNESS);
	}

	private void removeBrickMouldSill()
	{
		// No sill brick mould
		Integer sill = getOverallSillIndex();

		if (sill != null)
		{
			getOverallOffsets()[sill].setOverallToFrame(Units.inch(0d));
			getOverallOffsets()[sill].setOverallTotalOffset(Units.inch(0d));
		}
	}

	@Override
	public boolean showDebugMarks()
	{
		return false;
	}

	@Override
	public void init(Object[] params)
	{
		// TODO Auto-generated method stub

	}
}
