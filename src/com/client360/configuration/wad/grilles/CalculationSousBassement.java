package com.client360.configuration.wad.grilles;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.netappsid.commonutils.geo.Form;
import com.netappsid.commonutils.geo.NAIDEdge;
import com.netappsid.commonutils.math.IntersectionFunction;
import com.netappsid.commonutils.math.Units;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.wadconfigurator.Bar;
import com.netappsid.wadconfigurator.InternalMuntin;
import com.netappsid.wadconfigurator.grilles.GrillesManager;

/**
 * @author sboule
 * 
 *         Calculate a kick panel sash One horizontal TDL + varied TDL vertical bars in the bottom section.
 */
public class CalculationSousBassement extends GrillesManager
{
	public CalculationSousBassement(Configurable parent)
	{
		super(parent);
	}

	@Override
	/**
	 * Figure out the position of the bars as specified by the user.
	 * The order of the bar is used to paint them; i.e. first bars appear underneath later bars.
	 * TDL and SDL must be processed separately.
	 * @param param - Map containing the users specifications.
	 * @return the list of bars. 
	 */
	public void initializeBars()
	{
		// Interior Form is the visible glass and the exterior form is the glass
		Form visibleGlass;

		Form tdlForm;
		Form topGrilles;
		Form bottomGrilles;

		visibleGlass = getVisibleGlassForm();
		tdlForm = getSplittingGrillesForm();
		topGrilles = tdlForm.clone();
		bottomGrilles = tdlForm.clone();
		double originX = tdlForm.getBounds().getMinX();
		double originY = tdlForm.getBounds().getMinY();

		Map<String, Object> param = getParameters();
		
		// Get user parameters.
		Integer nbTdlVertiBar = (Integer) param.get("nbPanneaux") - 1;
		Boolean traItr = (Boolean) param.get("traItr");
		Double hauteurPanneau = getMeasureParam(param, "hauteurPanneau");
		Double widthHorizTdl = getMeasureParam(param, "largeurTraverseInter");
		Double widthVertiTdl = getMeasureParam(param, "largeurEntrePanneaux");

		// Null checks; apply default if null (not specified).
		if ((nbTdlVertiBar == null || nbTdlVertiBar == 0) && (hauteurPanneau == null || hauteurPanneau == 0.0))
		{
			return; // No bars to create.
		} /* end if */

		// Create infinite muntins
		List<Bar> muntins = new ArrayList<Bar>();

		if (!traItr) // Pas de traverse intermediaire
		{
			// Return an empty array of muntins.
			InternalMuntin[] array = new InternalMuntin[0];
			setSplittingBars(new InternalMuntin[0]);
			return;
		} /* end if */

		Line2D line1;
		Double xPos;
		Double yPos;

		Double halfTdlVertiThick = widthVertiTdl / 2;
		Double halfTdlHorizThick = widthHorizTdl / 2;
		Double horizTdlPosition = visibleGlass.getHeight() - hauteurPanneau - halfTdlHorizThick; // Center of bar from top

		Double muntinGrilleToVisibleGlass =
		// Muntin to visible glass is half the muntin thickness
		ProductData.getValue(ProductType.INTERNAL_MUNTIN, Data.TDL_MUNTIN_THICKNESS).doubleValue(Units.INCH) / 2d
				- ProductData.getValue(ProductType.INTERNAL_MUNTIN, Data.TDL_MUNTIN_TO_GRILLES).doubleValue(Units.INCH);

		// Create a top and bottom form to help us create the bars.
		Double bottomSpacer = tdlForm.getHeight() - horizTdlPosition /*- getTopOverallGrillesToVisibleGlassTranslation()*/
				+ (halfTdlHorizThick - muntinGrilleToVisibleGlass); // Muntin to grille
		Double topSpacer = horizTdlPosition/* + getTopOverallGrillesToVisibleGlassTranslation() */+ (halfTdlHorizThick - muntinGrilleToVisibleGlass); // Muntin
																																						// to
																																						// grille

		topGrilles.setSpacer(new Double[] { 0d, tdlForm.getWidth() / 2 + (halfTdlVertiThick - muntinGrilleToVisibleGlass), bottomSpacer, 0d });
		bottomGrilles.setSpacer(new Double[] { topSpacer, 0d, 0d, 0d });

		// Determine the position and length of the vertical bars
		Double vertiBarDistance = (visibleGlass.getWidth() - (nbTdlVertiBar * widthVertiTdl)) / (nbTdlVertiBar + 1);

		for (int i = 1; i < nbTdlVertiBar + 1; i++)
		{
			// Determine the position of the vertical bars
			line1 = new Line2D.Double();
			xPos = originX + (vertiBarDistance * i) + (widthVertiTdl * (i - 1)) + halfTdlVertiThick /* + getLeftOverallGrillesToVisibleGlassTranslation() */;
			line1.setLine(xPos, topSpacer, xPos, topSpacer + 1);
			setLineLength(bottomGrilles.getInteriorTranslatedForm(), line1);
			// line1.setLine(line1.getP1().getX(), line1.getP1().getY() + topSpacer, line1.getP2().getX(), line1.getP2().getY() + topSpacer);
			muntins.add(createMuntin(line1, widthVertiTdl));
		} /* end for */

		// Determine the position of the horizontal bar
		line1 = new Line2D.Double();
		yPos = originY + horizTdlPosition /* + getTopOverallGrillesToVisibleGlassTranslation() */;
		line1.setLine(originX, yPos, originX + 1, yPos);
		setLineLength(tdlForm, line1);
		muntins.add(createMuntin(line1, widthHorizTdl));

		// Return an array of muntins.
		InternalMuntin[] array = new InternalMuntin[muntins.size()];
		setSplittingBars(muntins.toArray(array));
	}

	/**
	 * Create a muntin.
	 * 
	 * @param line
	 *            - Line to use.
	 * @param lineThickness
	 *            - Thickness to use (inches).
	 * @return the created muntin.
	 */
	private InternalMuntin createMuntin(Line2D line, Double lineThickness)
	{
		InternalMuntin muntin;
		muntin = Configurable.createRedirectedConfigurableInstance(InternalMuntin.class, parent);
		muntin.setLine(line);
		muntin.setThickness(Units.inch(lineThickness));

		return muntin;
	}

	/**
	 * Verify whether the specified key is found in the specified map.
	 * 
	 * @param param
	 *            - Parameters map to look into.
	 * @param key
	 *            - Key to find.
	 * @return the converted measure if found (inches), null otherwise.
	 */
	@SuppressWarnings("unchecked")
	private Double getMeasureParam(Map<String, Object> param, String key)
	{
		if (param.get(key) == null)
		{
			return null;
		} /* end if */

		return ((Measure<Length>) param.get(key)).doubleValue(Units.INCH);
	}

	/**
	 * Set the specified line length to intersect with the specified form.
	 * 
	 * @param grilleForm
	 *            - Form to intersect.
	 * @param line
	 *            - Line for which to set the length.
	 */
	private void setLineLength(Form grilleForm, Line2D line)
	{
		List<Point2D> points = new ArrayList<Point2D>();

		for (NAIDEdge edge : grilleForm.getEdges())
		{
			List<Point2D> foundPoints = IntersectionFunction.getIntersection(line, edge.getShape());

			if (foundPoints != null)
			{
				points.addAll(foundPoints);
			} /* end if */
		} /* end for */

		line.setLine(points.get(0), points.get(1));
	}

	@Override
	public Bar[] updateNoSplittingBars()
	{
		return null;
	}
}
