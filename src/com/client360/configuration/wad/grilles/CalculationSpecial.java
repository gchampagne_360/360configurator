package com.client360.configuration.wad.grilles;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.netappsid.commonutils.geo.Form;
import com.netappsid.commonutils.geo.NAIDEdge;
import com.netappsid.commonutils.math.IntersectionFunction;
import com.netappsid.commonutils.math.Units;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.wadconfigurator.Bar;
import com.netappsid.wadconfigurator.InternalMuntin;
import com.netappsid.wadconfigurator.grilles.GrillesManager;

/**
 * @author sboule
 * 
 *         Calculate a special arrangement of SDL and TDL according to the visible glass and the top of the window position. One vertical TDL, one horizontal
 *         TDL + varied SDL bars on both top glasses.
 */
public class CalculationSpecial extends GrillesManager
{
	public CalculationSpecial(Configurable parent)
	{
		super(parent);
	}

	@Override
	/**
	 * Figure out the position of the bars as specified by the user.
	 * The order of the bar is used to paint them; i.e. first bars appear underneath later bars.
	 * TDL and SDL must be processed separately.
	 * @param param - Map containing the users specifications.
	 * @return the list of bars. 
	 */
	public void initializeBars()
	{
		// Interior Form is the visible glass and the exterior form is the glass
		final Form visibleGlass = getVisibleGlassForm();

		final Form splittingGrillesForm = getSplittingGrillesForm();
		final Form topGrilles = splittingGrillesForm.clone();
		final Form bottomGrilles = splittingGrillesForm.clone();
		Form framedGrille;

		// Get user parameters.
		Map<String, Object> param = getParameters();
		Integer nbSdlHorizBar = (Integer) param.get("nbSdlHorizBars");
		Integer nbSdlVertiBar = (Integer) param.get("nbSdlVertiBars");
		Boolean isSdlFrame = (Boolean) param.get("isSdlFrame");

		Double widthSdl = getMeasureParam(param, "widthSdl");
		Double horizTdlPosition = getMeasureParam(param, "horizTdlPosition");
		Double widthVertiTdl = getMeasureParam(param, "widthVertiTdl");
		Double widthVertiTdlKick = getMeasureParam(param, "widthVertiTdlKickPanel");
		Double widthHorizTdl = getMeasureParam(param, "widthHorizTdl");

		Boolean splittingBars = param.get("barsType").equals("TDL"); // False if "SDL" or null

		// Null checks; apply default if null (not specified).
		if (!splittingBars && nbSdlHorizBar == 0 && nbSdlVertiBar == 0)
		{
			return; // No bars to create.
		} /* end if */

		// Create infinite muntins
		List<Bar> muntins = new ArrayList<Bar>();
		List<Bar> bars = new ArrayList<Bar>();

		Line2D line1;
		Line2D line2;
		Double xPos;
		Double yPos;

		Double halfTdlVertiThick = widthVertiTdl / 2;
		Double halfTdlHorizThick = widthHorizTdl / 2;

		Double muntinGrilleToVisibleGlass =
		// Muntin to visible glass is half the muntin thickness
		ProductData.getValue(ProductType.INTERNAL_MUNTIN, Data.TDL_MUNTIN_THICKNESS).doubleValue(Units.INCH) / 2d
				- ProductData.getValue(ProductType.INTERNAL_MUNTIN, Data.TDL_MUNTIN_TO_GRILLES).doubleValue(Units.INCH);

		// Create a top and bottom form to help us create the bars.
		Double bottomSpacer = splittingGrillesForm.getHeight() - horizTdlPosition - getLeftOverallGrillesToVisibleGlassTranslation(getSplittingGrillesForm())
				+ (halfTdlHorizThick - muntinGrilleToVisibleGlass); // Muntin to grille
		Double topSpacer = horizTdlPosition + getLeftOverallGrillesToVisibleGlassTranslation(getSplittingGrillesForm())
				+ (halfTdlHorizThick - muntinGrilleToVisibleGlass); // Muntin to
		// grille

		topGrilles.setSpacer(new Double[] { 0d, splittingGrillesForm.getWidth() / 2 + (halfTdlVertiThick - muntinGrilleToVisibleGlass), bottomSpacer, 0d });
		bottomGrilles.setSpacer(new Double[] { topSpacer, 0d, 0d, 0d });

		if (splittingBars) // TDL
		{
			// Determine the position of the vertical bars: Always in the center
			line1 = new Line2D.Double();
			xPos = visibleGlass.getWidth() / 2d + getLeftOverallGrillesToVisibleGlassTranslation(getSplittingGrillesForm());
			line1.setLine(xPos, 0, xPos, 1);

			// Top vertical bar
			setLineLength(topGrilles.getInteriorTranslatedForm(), line1);
			muntins.add(createMuntin(line1, widthVertiTdl));

			// Bottom vertical bar (kick panels)
			line2 = (Line2D) line1.clone();
			line2.setLine(xPos, topSpacer, xPos, topSpacer + 1);
			setLineLength(bottomGrilles.getInteriorTranslatedForm(), line2);
			line2.setLine(line2.getP1().getX(), line2.getP1().getY() + topSpacer, line2.getP2().getX(), line2.getP2().getY() + topSpacer);
			muntins.add(createMuntin(line2, widthVertiTdlKick));

			// Determine the position of the horizontal bar
			line1 = new Line2D.Double();
			yPos = horizTdlPosition + getTopOverallGrillesToVisibleGlassTranslation(getSplittingGrillesForm());
			line1.setLine(0, yPos, 1, yPos);
			setLineLength(splittingGrillesForm, line1);
			muntins.add(createMuntin(line1, widthHorizTdl));

			// Return an array of muntins.
			InternalMuntin[] array = new InternalMuntin[muntins.size()];
			setSplittingBars(muntins.toArray(array));
			return;
		} /* end if */

		else
		// SDL
		{
			Double halfThick = widthSdl / 2d;

			Double grillesToVisibleGlass = ProductData.getValue(ProductType.INTERNAL_MUNTIN, Data.GRILLES_NUDGE_OFFSET).doubleValue(Units.INCH);

			Double horizVisibleGlassWidth = horizTdlPosition - halfTdlHorizThick - (nbSdlHorizBar * widthSdl);
			Double vertiVisibleGlassWidth = visibleGlass.getWidth() / 2d - halfTdlVertiThick - (nbSdlVertiBar * widthSdl);

			Double rightGrilleOffset = visibleGlass.getWidth() / 2d + halfTdlVertiThick - muntinGrilleToVisibleGlass;

			Double sdlFrameOffset = 0d;

			framedGrille = topGrilles.getInteriorTranslatedForm();

			// Build the SDL frame if requested.
			if (isSdlFrame)
			{
				// LEFT
				line1 = new Line2D.Double();
				xPos = widthSdl / 2;
				line1.setLine(xPos, 0, xPos, 1);
				setLineLength(framedGrille, line1);
				bars.add(createBar(line1, widthSdl));

				line2 = new Line2D.Double();
				line2.setLine(xPos + rightGrilleOffset, 0, xPos + rightGrilleOffset, 1);
				setLineLength(framedGrille, line2);
				bars.add(createBar(line2, widthSdl));

				// RIGHT
				line1 = new Line2D.Double();
				xPos = visibleGlass.getWidth() / 2d - widthSdl / 2 - (halfTdlVertiThick - muntinGrilleToVisibleGlass);
				line1.setLine(xPos, 0, xPos, 1);
				setLineLength(framedGrille, line1);
				bars.add(createBar(line1, widthSdl));

				line2 = new Line2D.Double();
				line2.setLine(xPos + rightGrilleOffset, 0, xPos + rightGrilleOffset, 1);
				setLineLength(framedGrille, line2);
				bars.add(createBar(line2, widthSdl));

				// TOP
				line1 = new Line2D.Double();
				yPos = widthSdl / 2;
				line1.setLine(0, yPos, 1, yPos);
				setLineLength(framedGrille, line1);
				bars.add(createBar(line1, widthSdl));

				line2 = new Line2D.Double();
				line2.setLine(0, yPos, 1, yPos);
				setLineLength(framedGrille, line2, rightGrilleOffset, 0d);
				bars.add(createBar(line2, widthSdl));

				// BOTTOM
				line1 = new Line2D.Double();
				yPos = horizTdlPosition - widthSdl / 2 - (halfTdlHorizThick - muntinGrilleToVisibleGlass);
				line1.setLine(0, yPos, 1, yPos);
				setLineLength(framedGrille, line1);
				bars.add(createBar(line1, widthSdl));

				line2 = new Line2D.Double();
				line2.setLine(0, yPos, 1, yPos);
				setLineLength(framedGrille, line2, rightGrilleOffset, 0d);
				bars.add(createBar(line2, widthSdl));

				horizVisibleGlassWidth -= 2 * (widthSdl - grillesToVisibleGlass);
				vertiVisibleGlassWidth -= 2 * (widthSdl - grillesToVisibleGlass);

				sdlFrameOffset = widthSdl - grillesToVisibleGlass;

				framedGrille.setSpacer(new Double[] { sdlFrameOffset, sdlFrameOffset, sdlFrameOffset, sdlFrameOffset });
			} /* end if */
			else
			{
				framedGrille.setSpacer(new Double[] { 0d, 0d, 0d, 0d });
			} /* end else */

			Double horizBarDistance = horizVisibleGlassWidth / (nbSdlHorizBar + 1);
			Double vertiBarDistance = vertiVisibleGlassWidth / (nbSdlVertiBar + 1);

			// Determine the position and length of the vertical bars
			for (int i = 1; i < nbSdlVertiBar + 1; i++)
			{
				line1 = new Line2D.Double();
				xPos = (vertiBarDistance * i) + (widthSdl * (i - 1)) + halfThick + getLeftOverallGrillesToVisibleGlassTranslation(getNoSplittingGrillesForm())
						+ sdlFrameOffset;
				line1.setLine(xPos, 0, xPos, 1);
				setLineLength(framedGrille.getInteriorTranslatedForm(), line1, 0d, sdlFrameOffset);
				bars.add(createBar(line1, widthSdl));

				line2 = new Line2D.Double();
				line2.setLine(xPos + rightGrilleOffset, 0, xPos + rightGrilleOffset, 1);
				setLineLength(framedGrille.getInteriorTranslatedForm(), line2, 0d, sdlFrameOffset);
				bars.add(createBar(line2, widthSdl));
			} /* end for */

			// Determine the position and length of the horizontal bars
			for (int i = 1; i < nbSdlHorizBar + 1; i++)
			{
				line1 = new Line2D.Double();
				yPos = (horizBarDistance * i) + (widthSdl * (i - 1)) + halfThick + getTopOverallGrillesToVisibleGlassTranslation(getNoSplittingGrillesForm())
						+ sdlFrameOffset;
				line1.setLine(0, yPos, 1, yPos);
				setLineLength(framedGrille.getInteriorTranslatedForm(), line1, sdlFrameOffset, 0d);
				bars.add(createBar(line1, widthSdl));

				line2 = new Line2D.Double();
				line2.setLine(0, yPos, 1, yPos);
				setLineLength(framedGrille.getInteriorTranslatedForm(), line2, sdlFrameOffset + rightGrilleOffset, 0d);
				bars.add(createBar(line2, widthSdl));
			} /* end for */

			Bar[] array = new Bar[bars.size()];
			setNoSplittingBars(bars.toArray(array));
			return;
		} /* end else */
	}

	protected Double getTopOverallGrillesToVisibleGlassTranslation(Form grillesForm)
	{
		final Form visibleGlassForm = getVisibleGlassForm();
		return visibleGlassForm.getOrigin().getY() - grillesForm.getOrigin().getY();
	}

	protected Double getLeftOverallGrillesToVisibleGlassTranslation(Form grillesForm)
	{
		return getVisibleGlassForm().getOrigin().getX() - grillesForm.getOrigin().getX();
	}
     
	/**
	 * Create a muntin.
	 * 
	 * @param line
	 *            - Line to use.
	 * @param lineThickness
	 *            - Thickness to use (inches).
	 * @return the created muntin.
	 */
	private InternalMuntin createMuntin(Line2D line, Double lineThickness)
	{
		InternalMuntin muntin;
		muntin = Configurable.createRedirectedConfigurableInstance(InternalMuntin.class, parent);
		muntin.setLine(line);
		muntin.setThickness(Units.inch(lineThickness));

		return muntin;
	}

	/**
	 * Create a bar.
	 * 
	 * @param line
	 *            - Line to use.
	 * @param lineThickness
	 *            - Thickness to use (inches).
	 * @return the created muntin.
	 */
	private Bar createBar(Line2D line, Double lineThickness)
	{
		Bar bar;
		bar = Configurable.createRedirectedConfigurableInstance(InternalMuntin.class, parent);
		bar.setLine(line);
		bar.setThickness(Units.inch(lineThickness));

		return bar;
	}

	/**
	 * Verify whether the specified key is found in the specified map.
	 * 
	 * @param param
	 *            - Parameters map to look into.
	 * @param key
	 *            - Key to find.
	 * @return the converted measure if found (inches), null otherwise.
	 */
	private Double getMeasureParam(Map<String, Object> param, String key)
	{
		if (param.get(key) == null)
		{
			return null;
		} /* end if */

		return ((Measure<Length>) param.get(key)).doubleValue(Units.INCH);
	}

	/**
	 * Set the specified line length to intersect with the specified form.
	 * 
	 * @param grilleForm
	 *            - Form to intersect.
	 * @param line
	 *            - Line for which to set the length.
	 */
	private void setLineLength(Form grilleForm, Line2D line)
	{
		List<Point2D> points = new ArrayList<Point2D>();

		for (NAIDEdge edge : grilleForm.getEdges())
		{
			List<Point2D> foundPoints = IntersectionFunction.getIntersection(line, edge.getShape());

			if (foundPoints != null)
			{
				points.addAll(foundPoints);
			} /* end if */
		} /* end for */

		line.setLine(points.get(0), points.get(1));
	}

	/**
	 * Set the specified line length to intersect with the specified form.
	 * 
	 * @param grilleForm
	 *            - Form to intersect.
	 * @param line
	 *            - Line for which to set the length.
	 * @param offsetX
	 *            - Horizontal offset to add.
	 * @param offsetY
	 *            - Vertical offset to add.
	 */
	private void setLineLength(Form grilleForm, Line2D line, Double offsetX, Double offsetY)
	{
		setLineLength(grilleForm, line);
		line.setLine(line.getP1().getX() + offsetX, line.getP1().getY() + offsetY, line.getP2().getX() + offsetX, line.getP2().getY() + offsetY);
	}

	@Override
	public Bar[] updateNoSplittingBars()
	{
		return null;
	}
}
