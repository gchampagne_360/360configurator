package com.client360.configuration.wad.grilles;

import com.netappsid.erp.configurator.Configurable;
import com.netappsid.wadconfigurator.Bar;
import com.netappsid.wadconfigurator.grilles.GrillesManager;

/**
 * Specific example of a mix of SDL and TDL.
 * 
 * @author sboule
 */
public class CalculationSpecialMuntin extends GrillesManager
{

	public CalculationSpecialMuntin(Configurable parent)
	{
		super(parent);
	}

	@Override
	public void initializeBars()
	{
		// //Interior Form is the visible glass and the exterior form is the overall dimensions of the grilles
		//
		// Form visibleGlass;
		// Form overAllGrilles;
		// visibleGlass = getVisibleGlassForm();
		// overAllGrilles = getOverallGrilleForm();
		//
		// Measure<Length> posTopBar = (Measure<Length>) param.get("posTopBar");
		// Measure<Length> posBotBar = (Measure<Length>) param.get("posBotBar");
		// Double thickness = (Double) param.get("thickness");
		// Double halfThick = thickness/2;
		//
		// //Create infinite bars
		// List<Bar> bars = new ArrayList<Bar>();
		// List<Line2D> lines = new ArrayList<Line2D>();
		//
		// Double horizBarDistance = 0d;
		//
		// if (posTopBar != null)
		// {
		// horizBarDistance = posTopBar.doubleValue(Units.INCH);
		// }
		//
		// if (posBotBar != null)
		// {
		// horizBarDistance = posTopBar.doubleValue(Units.INCH);
		// }
		//
		// Line2D line;
		// Double yPos;
		//
		// line = new Line2D.Double();
		// yPos = horizBarDistance + halfThick - getTopOverallGrillesToVisibleGlassTranslation();
		// line.setLine(0, yPos, 1, yPos);
		// lines.add(line);
		//
		// //Determine the length
		// for (Line2D currentLine : lines)
		// {
		// List<Point2D> points = new ArrayList<Point2D>();
		// for (Shape edge : overAllGrilles.getEdge())
		// {
		// List<Point2D> foundPoints = IntersectionFunction.getIntersection(currentLine, edge);
		//
		// if(foundPoints != null)
		// {
		// points.addAll(foundPoints);
		// }
		// }
		// currentLine.setLine(points.get(0), points.get(1));
		// }
		//
		//
		// Bar bar;
		// for (Line2D currentLine : lines)
		// {
		// bar = new Bar(this.parent);
		// bar.setLine(currentLine);
		// bar.setThickness(Units.inch(thickness));
		// bars.add(bar);
		// }
		//
		// Bar[] array = new Bar[bars.size()];
		//
		// return bars.toArray(array);
	}

	@Override
	public Bar[] updateNoSplittingBars()
	{
		// TODO Auto-generated method stub
		return null;
	}
}
