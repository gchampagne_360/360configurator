package com.client360.configuration.wad;

import com.netappsid.erp.configurator.Configurable;

public abstract class CasementMainSashSection extends com.client360.configuration.wad.CasementMainSashSectionBase
{

	public CasementMainSashSection(Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
	}

}
