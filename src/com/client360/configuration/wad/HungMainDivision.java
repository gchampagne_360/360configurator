// $Id: HungMainDivision.java,v 1.7 2011-06-20 12:09:36 sboule Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.netappsid.commonutils.geo.CornerType;
import com.netappsid.commonutils.math.Units;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.rendering.common.advanced.Drawing;
import com.netappsid.rendering.common.advanced.Profile;
import com.netappsid.wadconfigurator.MeetingRail;
import com.netappsid.wadconfigurator.Sash;
import com.netappsid.wadconfigurator.SectionInput;
import com.netappsid.wadconfigurator.enums.ChoiceOpeningType;

public abstract class HungMainDivision extends com.client360.configuration.wad.HungMainDivisionBase
{

	public HungMainDivision(Configurable parent)
	{
		super(parent);
	}

	/**
	 * 
	 * @see com.netappsid.wadconfigurator.HungDivision#activateListener()
	 */

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
		makeInvisible(PROPERTYNAME_INSTALLED);
		makeInvisible(PROPERTYNAME_SHOWMEETINGRAIL);

		setDefaultProfiles(getClientProfiles());
	}

	@Override
	public Sash[] sashMakeArray(int nbSections)
	{
		return makeConfigurableArray(HungMainSash.class, nbSections, this);
	}

	@Override
	public Measure<Length>[] getSashOffset(int railNumber)
	{
		return ProductData.getSpacer(ProductType.HUNG_PVC, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID);
	}

	// @Override
	// public Measure<Length>[] getMullionOffset()
	// {
	// return ProductData.getSpacer(ProductType.HUNG_PVC, Data.FRAME_TO_MULLION_OFFSET_SPACER);
	// }

	@Override
	public Measure<Length>[] getFrameBorder()
	{
		return ProductData.getSpacer(ProductType.HUNG_PVC, Data.FRAME_BORDER_SPACERS);
	}

	@Override
	public CornerType[] getCornerType()
	{
		return ProductData.getCornerType(ProductType.HUNG_PVC, Data.FRAME_CORNERTYPE);
	}

	public Profile[] getClientProfiles()
	{
		return ProductData.getProfiles(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_PROFILES);
	}

	// @Override
	// public void insertionInParentCompleted()
	// {
	// super.insertionInParentCompleted();
	//
	// ChoicePresetWindowWidth c = null;
	// if(getComponent() != null)
	// {
	// c = ((HungMainMod)getComponent()).getChoicePresetWindowWidth();
	// }
	//
	// if(c == ChoicePresetWindowWidth.D14_12_14 ||
	// c == ChoicePresetWindowWidth.D14_12_14SON)
	// {
	// setDefaultIsFixed(true);
	// }
	// else
	// {
	// setDefaultIsFixed(false);
	// }
	// }

	@Override
	public void activateListener()
	{
		super.activateListener();
		addAssociationPropertyChangeListener(PROPERTYNAME_SECTIONINPUT, SectionInput.PROPERTYNAME_SECTIONWIDTH, "splitSection");
	}

	/**
	 * @return
	 * @see com.netappsid.wadconfigurator.Window#getMaxNbSection()
	 */
	@Override
	public Integer getMaxNbSection()
	{
		return 2;
	}

	/**
	 * @return
	 * @see com.netappsid.wadconfigurator.Window#getMinNbSection()
	 */
	@Override
	public Integer getMinNbSection()
	{
		return 1;
	}

	/**
	 * @return the number of sash
	 * @see com.netappsid.wadconfigurator.Window#getNbSash()
	 */
	@Override
	public int getNbSash()
	{
		return 2 * getNbSections();
	}

	// Add custom overrides here

	/**
	 * @param nbSections
	 * @return
	 * @see com.netappsid.wadconfigurator.Window#meetingRailMakeArray(int)
	 */
	@Override
	public MeetingRail[] meetingRailMakeArray(int nbSections)
	{
		return makeConfigurableArray(HungMainMeetingRail.class, nbSections, this);
	}

	@Override
	public Measure<Length> getSectionMaxWidth()
	{
		return Units.inch(100d);
	}

	@Override
	public Measure<Length> getSectionMinWidth()
	{
		return Units.inch(10d);
	}

	public boolean isDivisionEnabled()
	{
		return false;
	}

	// ---------------------------------------------------------------------------------------------------
	// ---------------------------------------------------------------------------------------------------
	// ---------------------------------------------------------------------------------------------------

	@Override
	public void afterNbSectionsChanged(Integer nbSections)
	{
		// TODO Auto-generated method stub
		super.afterNbSectionsChanged(nbSections);
		if (((HungMain) getParent()).getChoiceOpeningType() == ChoiceOpeningType.SIMPLE)
		{
			if (getSash() != null)
			{
				getSash()[0].isFix = true;
				getSash()[1].isFix = false;
			}
		}
		else
		{
			if (getSash() != null)
			{
				getSash()[0].isFix = false;
				getSash()[1].isFix = false;
			}
		}
	}

	// ------------------------------------------------
	// ------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------------
	// ------------------------------------------------------------
	// ------------------------------------

	public InsulatedGlass getInsulatedGlass()
	{
		for (Sash s : getSash())
		{
			if (s.isFix)
			{
				return ((InsulatedGlass) s.getSashSection().getInsulatedGlass()[0]);
			}
		}

		return null;
	}

	@Override
	public Drawing getCad()
	{
		// TODO Auto-generated method stub
		return null;
	}

	public final Measure<Length>[] getGlazingBeadThickness()
	{
		return null;
	}

	public final Measure<Length> getMullionThickness()
	{
		return null;
	}

	public final Measure<Length> getMullionCenterCorrection()
	{
		return null;
	}
}
