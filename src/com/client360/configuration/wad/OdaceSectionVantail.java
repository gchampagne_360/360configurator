// $Id: $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.netappsid.wadconfigurator.enums.ChoiceSashType;

public class OdaceSectionVantail extends com.client360.configuration.wad.OdaceSectionVantailBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public OdaceSectionVantail(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	@Override
	public Measure<Length>[] getGlassOffset()
	{
		if (getChoiceSashType() == ChoiceSashType.FIXED_NOSASH)
		{
			return ProductData.getSpacer(ProductType.ODACE, Data.FRAME_TO_SASH_TO_GLASS);
		}
		else
		{
			return ProductData.getSpacer(ProductType.ODACE, Data.SASH_TO_GLASS_OFFSET_SPACERS);
		}
	}

	@Override
	public Measure<Length>[] getSplittingGrillesOffset()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length>[] getNoSplittingGrillesOffset()
	{
		// TODO Auto-generated method stub
		return null;
	}

}
