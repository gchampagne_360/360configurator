// $Id: ChoiceStandardDimensions.java,v 1.3 2011-08-19 19:35:48 sboule Exp $

package com.client360.configuration.wad;

public class ChoiceStandardDimensions extends com.client360.configuration.wad.ChoiceStandardDimensionsBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public ChoiceStandardDimensions(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	// Add custom overrides here
}
