// $Id: PatioMeetingRail.java,v 1.4 2011-05-31 17:25:25 sboule Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.erp.configurator.Configurable;

@SuppressWarnings("serial")
public class PatioMeetingRail extends com.client360.configuration.wad.PatioMeetingRailBase
{

	public PatioMeetingRail(Configurable parent)
	{
		super(parent);
	}

	@Override
	public Measure<Length> getSameRailLeftGlassOffset()
	{
		return null;
	}

	@Override
	public Measure<Length> getSameRailRightGlassOffset()
	{
		return null;
	}

	@Override
	public Measure<Length> getLeftGlassOffset()
	{
		return null;
	}

	@Override
	public Measure<Length> getRightGlassOffset()
	{
		return null;
	}

	public final Measure<Length> getMullionThickness()
	{
		return null;
	}

	public final Measure<Length> getMullionCenterCorrection()
	{
		return null;
	}
}
