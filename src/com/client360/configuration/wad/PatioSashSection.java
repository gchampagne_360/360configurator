// $Id: PatioSashSection.java,v 1.3 2011-04-27 12:09:13 jddaigle Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.erp.configurator.Configurable;

public class PatioSashSection extends com.client360.configuration.wad.PatioSashSectionBase
{
	public PatioSashSection(Configurable parent)
	{
		super(parent);
	}

	@Override
	public Measure<Length>[] getSplittingGrillesOffset()
	{
		return null;
	}

	@Override
	public Measure<Length>[] getNoSplittingGrillesOffset()
	{
		return null;
	}

	@Override
	protected Measure<Length>[] getGlassOffset()
	{
		return null;
	}
}
