// $Id: Jamb.java,v 1.2 2011-04-27 12:09:14 jddaigle Exp $

package com.client360.configuration.wad;

import com.netappsid.erp.configurator.Configurable;

public abstract class Jamb extends com.client360.configuration.wad.JambBase
{
	public Jamb(Configurable parent)
	{
		super(parent);
	}

	abstract public double getCoteA();

	// Add custom overrides here
}
