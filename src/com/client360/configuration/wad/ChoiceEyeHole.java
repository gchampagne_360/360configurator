// $Id: ChoiceEyeHole.java,v 1.3 2011-04-27 12:09:13 jddaigle Exp $

package com.client360.configuration.wad;

import com.netappsid.erp.configurator.Configurable;

public class ChoiceEyeHole extends com.client360.configuration.wad.ChoiceEyeHoleBase
{

	public ChoiceEyeHole(Configurable parent)
	{
		super(parent);
	}

	@Override
	public String toString()
	{
		return getDescription();
	}
}
