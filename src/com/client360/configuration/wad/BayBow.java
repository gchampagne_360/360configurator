// $Id: $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.commonutils.math.Units;
import com.netappsid.rendering.common.advanced.Drawing;
import com.netappsid.wadconfigurator.Window;
import com.netappsid.wadconfigurator.enums.ChoiceDimensionWidth;

public class BayBow extends com.client360.configuration.wad.BayBowBase
{

	public BayBow(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		setDefaultNbSections(3);
		setDefaultFrameDepth(Units.measure(6, Units.INCH));

		makeVisible(PROPERTYNAME_FRAMEDEPTH);
		makeVisible(PROPERTYNAME_OPTION);

		makeInvisible(PROPERTYNAME_DIVISION);

		overrideLabel(PROPERTYNAME_OPTION, "Installation");
		overrideLabel(PROPERTYNAME_FRAMEDEPTH, traductionBundle.getString("frameDepth"));

		// FIXME advancedBayBowCalculator.setConstant
		// advancedBayBowCalculator.setConstant(0.0, 4.5, 0.0, 0.0);
	}

	@Override
	public Window[] windowMakeArray(int nbSections)
	{
		return makeConfigurableArray(CasementVisionPvc.class, nbSections, this);
	}

	@Override
	public Measure<Length> getFrameThickness()
	{
		return getFrameDepth();
	}

	@Override
	public int getNbSash()
	{
		return 0;
	}

	@Override
	public Measure<Length>[] getMullionOffset()
	{
		return null;
	}

	@Override
	public Drawing getCad()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String productCode()
	{
		return "BBG";
	}

	@Override
	public String colorChartNameExterior()
	{
		return "STD";
	}

	@Override
	public String colorChartNameInterior()
	{
		return "STD";
	}

	@Override
	public boolean useAutomaticGrilleAlignement()
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean splitSection()
	{
		return true;
	}

	@Override
	public String toString()
	{
		return "BayBow";
	}

	@Override
	public String getName()
	{
		return "BayBow";
	}

	@Override
	public void activateListener()
	{
		super.activateListener();

		//		addAssociationPropertyChangeListener(PROPERTYNAME_DIMENSION, DimensionBase.PROPERTYNAME_HEIGHT, "removeDefaultValueOptionThermos"); //$NON-NLS-1$
		//		addAssociationPropertyChangeListener(PROPERTYNAME_DIMENSION, DimensionBase.PROPERTYNAME_WIDTH, "removeDefaultValueOptionThermos"); //$NON-NLS-1$
		addAssociationPropertyChangeListener(PROPERTYNAME_DIMENSION, DimensionBase.PROPERTYNAME_HEIGHT, "setDefaultDimensions"); //$NON-NLS-1$
		addAssociationPropertyChangeListener(PROPERTYNAME_DIMENSION, DimensionBase.PROPERTYNAME_WIDTH, "setDefaultDimensions"); //$NON-NLS-1$
	}

	public void setDefaultDimensions()
	{
		if (getDimension().getHeight() != null && getDimension().getWidth() != null)
		{
			if (getNbSections() > 3)
			{
				getBayBowSpecs().setChoiceDimensionWidth(ChoiceDimensionWidth.D1_4_1_2_1_4);
			}
			else
			{
				getBayBowSpecs().setChoiceDimensionWidth(ChoiceDimensionWidth.D1_3);
			}
		}
	}

	@Override
	public void init(Object[] params)
	{
		// TODO Auto-generated method stub

	}
}
