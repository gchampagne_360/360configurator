package com.client360.configuration.wad;
//// $Id: RegularQuestionary.java,v 1.6 2011-05-31 17:25:25 sboule Exp $
//
//package com.client360.configuration.wad;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import javax.measure.quantities.Length;
//
//import org.jscience.physics.measures.Measure;
//
//import com.client360.configuration.wad.constant.Data;
//import com.client360.configuration.wad.constant.ProductData;
//import com.client360.configuration.wad.constant.ProductType;
//import com.client360.configuration.wad.grilles.CalculationRegular;
//import com.netappsid.common.math.MathFunction;
//import com.netappsid.common.math.Units;
//import com.netappsid.erp.configurator.Configurable;
//import com.netappsid.erp.configurator.utils.MeasureConverter;
//import com.netappsid.wadconfigurator.Bar;
//import com.netappsid.wadconfigurator.grilles.GrillesManager;
//import com.netappsid.wadconfigurator.utils.MeasureLengthUtils;
//
//
///**
// * @author Jddaigle
// *
// * Client Custom questionnaire for the regular grille type
// */
//public class RegularQuestionary extends com.client360.configuration.wad.RegularQuestionaryBase
//{
//    public RegularQuestionary(Configurable parent)
//	{
//		super(parent);
//	}
//
//	@Override
//    public void afterNbHorizSquareChanged(Integer nbHorizSquare)
//    {
//    	super.afterNbHorizSquareChanged(nbHorizSquare);
//    	calculate();
//    }
//    
//    @Override
//    public void afterNbVertiSquareChanged(Integer nbVertiSquare)
//    {
//    	super.afterNbVertiSquareChanged(nbVertiSquare);
//    	calculate();
//    }
//    
//    /**
//     * Method to call when we need to update the bars
//     */
//    @Override
//	public void calculate()
//    {
//    	if(getNbHorizSquare() != null && getNbVertiSquare() != null && getDimension() != null && getDimension().getForm() != null)
//    	{
//    		Map<String, Object> map = new HashMap<String, Object>();
//    		map.put("nbHorizBar", getNbHorizSquare());
//    		map.put("nbVertiBar", getNbVertiSquare());
//    		map.put("thickness", 5/8d);
//    		
//    		
//    		//Convert the visible glass form (getDimension().getForm()) to the grilles form with the VISIBLEGLASS_TO_GRILLES and load the grilles
//    		//TODO move complex code to WAD
//    		Bar[] bars = getGrillesManager().load(map);
//    		
//    		setNoSplittingBars(bars);
//    	}
//    }
//    
//    @Override
//	public Class<? extends GrillesManager> getGrillesManagerClass()
//    {
//    	return CalculationRegular.class;
//    }
//
// }
