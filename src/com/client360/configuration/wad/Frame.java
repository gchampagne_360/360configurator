// $Id: Frame.java,v 1.4 2011-08-19 19:35:48 sboule Exp $

package com.client360.configuration.wad;

import com.netappsid.erp.configurator.Configurable;

public class Frame extends com.client360.configuration.wad.FrameBase
{
	public Frame(Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		makeInvisible(PROPERTYNAME_SUFFLAGEDEPTH);
		makeInvisible(PROPERTYNAME_CASING);
		makeInvisible(PROPERTYNAME_SILLMOULD);
	}

	public boolean isWoodenBoxVisible()
	{
		return false;
	}

	public boolean isWoodFrameVisible()
	{
		return false;
	}

}
