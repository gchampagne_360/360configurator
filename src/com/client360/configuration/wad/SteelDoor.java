// $Id: SteelDoor.java,v 1.9 2011-06-10 14:23:19 sboule Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.netappsid.commonutils.geo.CornerType;
import com.netappsid.commonutils.math.Units;
import com.netappsid.rendering.common.advanced.Drawing;
import com.netappsid.wadconfigurator.Door;
import com.netappsid.wadconfigurator.DoorMullionFix;
import com.netappsid.wadconfigurator.enums.ChoiceOpening;
import com.netappsid.wadconfigurator.enums.ChoiceSideLightSide;
import com.netappsid.wadconfigurator.enums.ChoiceSwing;

public class SteelDoor extends com.client360.configuration.wad.SteelDoorBase
{

	public SteelDoor(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		makeInvisible(PROPERTYNAME_PROFILESOS);
		makeInvisible(PROPERTYNAME_PROFILESNOS);
		makeInvisible(PROPERTYNAME_SILLPROFILES);
		makeInvisible(PROPERTYNAME_RENDERINGNUMBER);
		makeInvisible(PROPERTYNAME_DOORFRAME);
		makeInvisible(PROPERTYNAME_CHOICEBUILDTYPE);
		makeInvisible(PROPERTYNAME_CHOICEILLUSIONOPENING);
		makeInvisible(PROPERTYNAME_CHOICESLABDOORWIDTH);
		makeInvisible(PROPERTYNAME_CHOICESLABSIDELIGHTWIDTH);

		makeInvisible(PROPERTYNAME_STEELDOOROPTIONS);
		makeInvisible(PROPERTYNAME_OPENING);

		makeDisabled(PROPERTYNAME_CHOICEPAINTINGSIDE);
		makeDisabled(PROPERTYNAME_CHOICEBOXTYPE);
		setDefaultChoiceSwing(ChoiceSwing.INSWING);
		setDefaultChoiceSideLiteSwing(ChoiceSwing.INSWING);
		setDefaultChoiceSideLightSide(ChoiceSideLightSide.LEFTSIDE);
		overrideLabel(PROPERTYNAME_INTERIORCOLOR, "Couleur");

		makeReadOnly(PROPERTYNAME_EXTERIORCOLOR);
		makeReadOnly(PROPERTYNAME_INTERIORCOLOR);

		getDimension().setForm(getDefaultForm());
	}

	@Override
	public CornerType[] getCornerType()
	{
		return ProductData.getCornerType(ProductType.DOOR, Data.FRAME_CORNERTYPE);
	}

	@Override
	public DoorMullionFix[] makeArrayDoorMullionFix(int nbMullionFix)
	{
		return makeArray(DoorMullionFix.class, nbMullionFix, this);
	}

	@Override
	public String toString()
	{
		return getRenderingNumber() + " - " + clientTraductionBundle.getString("SteelDoor");
	}

	@Override
	public String getName()
	{
		return clientTraductionBundle.getString("SteelDoor");
	}

	@Override
	public String colorChartNameExterior()
	{
		return "";
	}

	@Override
	public String colorChartNameInterior()
	{
		return "";
	}

	@Override
	public String productCode()
	{
		return "STD";
	}

	/*
	 * (non-Javadoc) WARNING -- Do not change this values. They are used by the JUnit tests.
	 */
	@Override
	public Measure<Length> getTopSpacerOS()
	{
		return Units.inch(0.5);
	}

	/*
	 * (non-Javadoc) WARNING -- Do not change this values. They are used by the JUnit tests.
	 */
	@Override
	public Measure<Length> getTopSpacer()
	{
		return Units.inch(1.25);
	}

	/*
	 * (non-Javadoc) WARNING -- Do not change this values. They are used by the JUnit tests.
	 */
	@Override
	public Measure<Length> getJambWidthRightOS()
	{
		return Units.inch(1.25);
	}

	/*
	 * (non-Javadoc) WARNING -- Do not change this values. They are used by the JUnit tests.
	 */
	@Override
	public Measure<Length> getJambWidthRight()
	{
		return Units.inch(2.5);
	}

	/*
	 * (non-Javadoc) WARNING -- Do not change this values. They are used by the JUnit tests.
	 */
	@Override
	public Measure<Length> getJambWidthLeftOS()
	{
		return Units.inch(1.25);
	}

	/*
	 * (non-Javadoc) WARNING -- Do not change this values. They are used by the JUnit tests.
	 */
	@Override
	public Measure<Length> getJambWidthLeft()
	{
		return Units.inch(2.5);
	}

	/*
	 * (non-Javadoc) WARNING -- Do not change this values. They are used by the JUnit tests.
	 */
	@Override
	public Measure<Length> getMullionFixWidthOS()
	{
		return Units.inch(0.5);
	}

	/*
	 * (non-Javadoc) WARNING -- Do not change this values. They are used by the JUnit tests.
	 */
	@Override
	public Measure<Length> getMullionFixWidth()
	{
		return Units.inch(2.0);
	}

	/*
	 * (non-Javadoc) WARNING -- Do not change this values. They are used by the JUnit tests.
	 */
	@Override
	public Measure<Length> getZMullionFonctionalWidth()
	{
		return Units.inch(0d);
	}

	@Override
	public Measure<Length> getZMullionWidth()
	{
		return Units.inch(1.0);
	}

	/*
	 * (non-Javadoc) WARNING -- Do not change this values. They are used by the JUnit tests.
	 */
	@Override
	public Measure<Length> getMullionMobileWidthOS()
	{
		return Units.inch(0.25);
	}

	/*
	 * (non-Javadoc) WARNING -- Do not change this values. They are used by the JUnit tests.
	 */
	@Override
	public Measure<Length> getMullionMobileWidth()
	{
		return Units.inch(1.5);
	}

	/*
	 * (non-Javadoc) WARNING -- Do not change this values. They are used by the JUnit tests.
	 */
	public Measure<Length> getOpeningSlabGap()
	{
		return Units.inch(0.25);
	}

	/*
	 * (non-Javadoc) WARNING -- Do not change this values. They are used by the JUnit tests.
	 */
	public Measure<Length> getFixedSlabGap()
	{
		return Units.inch(0.125);
	}

	/*
	 * (non-Javadoc) WARNING -- Do not change this values. They are used by the JUnit tests.
	 */
	public Measure<Length> getSlabHeadGap()
	{
		return Units.inch(0.375);
	}

	/*
	 * (non-Javadoc) WARNING -- Do not change this values. They are used by the JUnit tests.
	 */
	@Override
	public Measure<Length> getSillThickness()
	{
		return Units.inch(0.25);
	}

	@Override
	public com.netappsid.wadconfigurator.Opening[] makeArrayOpening(int nbOpening)
	{
		return makeArray(getRedirectedClassIfPresent(com.netappsid.wadconfigurator.Opening.class), nbOpening, this);
	}

	@Override
	public com.netappsid.wadconfigurator.Opening[] makeArraySideliteOpening(int nbSideliteOpening)
	{
		return makeArray(getRedirectedClassIfPresent(com.netappsid.wadconfigurator.Opening.class), nbSideliteOpening, this);
	}

	@Override
	public SideLight[] makeArraySideLight(int nbSideLight)
	{
		return makeArray(SideLight.class, nbSideLight, this);
	}

	@Override
	public Drawing getCad()
	{
		return ProductData.getDrawing(ProductType.CASEMENT_VISION_PVC_IN, Data.DRAWING_MULLION);
	}

	@Override
	public void afterChoiceStandardDimensionChanged(ChoiceStandardDimension choiceStandardDimension)
	{

		getDimension().removeWidth();
		setNbDoors(choiceStandardDimension.getNbDoor());
		setNbSideLight(choiceStandardDimension.getNbSide());

		getDimension().setHeight(Units.inch(82.5));

		if (choiceStandardDimension.getDoorsWidth() != 0.0)
		{
			setSlabDoorWidth(Units.mm(choiceStandardDimension.getDoorsWidth()));
		}
		else
		{
			removeSlabDoorWidth();
		}

		if (choiceStandardDimension.getSidesWidth() != 0.0)
		{
			setSlabSidelightWidth(Units.mm(choiceStandardDimension.getSidesWidth()));
		}
		else
		{
			removeSlabSidelightWidth();
		}

		removeChoiceSideLightSide();

		if (!choiceStandardDimension.getSideLightSide().equals(""))
		{
			setChoiceSideLightSide(ChoiceSideLightSide.valueOf(choiceStandardDimension.getSideLightSide()));
		}

		String[] choiceOpenings = choiceStandardDimension.getOperatingSide().split(";");
		ChoiceOpening choiceOpening;

		try
		{
			for (int i = 0; i < getDoor().length; i++)
			{
				choiceOpening = ChoiceOpening.valueOf(choiceOpenings[i]);
				getDoor()[i].setChoiceOpening(choiceOpening);
			}
		}

		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}

		updateWidth();
	}

	@Override
	public Door[] makeArrayDoor(int nbDoor)
	{
		return makeConfigurableArray(com.client360.configuration.wad.Door.class, nbDoor, this);
	}

	// @Override
	// public EnumConverter getInteriorColorFKConverter(PropertyDescriptor pd, @SuppressWarnings("rawtypes") Class clazz)
	// {
	// List<Object> choices = new ArrayList<Object>();
	// List<String> labels = new ArrayList<String>();
	// Object defaultChoice = null;
	// Object tmpObject = null;
	//
	// if (isInteriorColorStandardColorEnabled())
	// {
	// tmpObject = getRedirectedClassIfPresent(com.netappsid.wadconfigurator.StandardColor.class);
	// choices.add(tmpObject);
	// if (Locale.getDefault().getLanguage().compareTo("fr") == 0)
	// {
	// labels.add("");
	// }
	// else
	// {
	// labels.add("");
	// }
	// }
	//
	// String[] labelArray = new String[labels.size()];
	// int i = 0;
	//
	// for (String s : labels)
	// {
	// labelArray[i++] = s;
	// }
	//
	// if (defaultChoice == null)
	// {
	// return new NAIDEnumConverter(clazz.getName() + "." + pd.getName() + this.hashCode(), clazz, choices.toArray(), labelArray);
	// }
	// return null;
	// }
	//
	// @Override
	// public EnumConverter getExteriorColorFKConverter(PropertyDescriptor pd, Class clazz)
	// {
	// List<Object> choices = new ArrayList<Object>();
	// List<String> labels = new ArrayList<String>();
	// Object tmpObject = null;
	//
	// if (isExteriorColorStandardColorEnabled())
	// {
	// tmpObject = getRedirectedClassIfPresent(com.netappsid.wadconfigurator.StandardColor.class);
	// choices.add(tmpObject);
	// if (Locale.getDefault().getLanguage().compareTo("fr") == 0)
	// {
	// labels.add("");
	// }
	// else
	// {
	// labels.add("");
	// }
	// }
	//
	// String[] labelArray = new String[labels.size()];
	// int i = 0;
	//
	// for (String s : labels)
	// {
	// labelArray[i++] = s;
	// }
	//
	// return new NAIDEnumConverter(clazz.getName() + "." + pd.getName() + this.hashCode(), clazz, choices.toArray(), labelArray);
	// }

	// -------PRICE------------

	@Override
	public double getBasePrice()
	{
		return 1000.0;
	}

	@Override
	public void init(Object[] params)
	{
		// TODO Auto-generated method stub

	}

}
