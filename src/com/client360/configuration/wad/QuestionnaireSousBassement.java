// $Id: QuestionnaireSousBassement.java,v 1.3 2011-09-06 14:52:55 jjgodard Exp $

package com.client360.configuration.wad;

import java.util.Map;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.client360.configuration.wad.enums.Soubassement;
import com.client360.configuration.wad.enums.TypeDePanneaux;
import com.client360.configuration.wad.enums.TypeDeTraverseIntermediaire;
import com.client360.configuration.wad.grilles.CalculationSousBassement;
import com.jgoodies.validation.ValidationResult;
import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.Section;
import com.netappsid.wadconfigurator.grilles.GrillesManager;

@SuppressWarnings("serial")
public class QuestionnaireSousBassement extends com.client360.configuration.wad.QuestionnaireSousBassementBase
{
	boolean inhibateCalulation;

	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public QuestionnaireSousBassement(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	@Override
	public Class<? extends GrillesManager> getGrillesManagerClass()
	{
		return CalculationSousBassement.class;
	}

	@Override
	public void activateListener()
	{
		super.activateListener();
		addPropertyChangeListener(PROPERTYNAME_NOSPLITTINGGRILLESFORM, "formDimensionChanged");
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		inhibateCalulation = true;

		try
		{
			// Ajuster les d�fauts
			setDefaultNbPanneaux(2);
			setDefaultSoubassement(Soubassement.SOUBASSEMNT400);
			setDefaultLarTraItr(getSoubassement().traverseBasse);
			setDefaultHauSbs(getSoubassement().hauSbs);
			setDefaultTypeDePanneaux(TypeDePanneaux.MASSIF);
			setDefaultTypeDeTraverseIntermediaire(TypeDeTraverseIntermediaire.TRAVERSE75);
			// makeInvisible(PROPERTYNAME_CHOICEMUNTINPOSITIONFROM);
		}
		finally
		{
			inhibateCalulation = false;
		}

		// La valeur de ces champs sont contr�l�s par des enum�rations.
		makeInvisible(PROPERTYNAME_LARTRAITR);
		makeInvisible(PROPERTYNAME_HAUSBS);
	}

	@Override
	protected void feedCalculationParameters(Map<String, Object> calculationParameters)
	{
		super.feedCalculationParameters(calculationParameters);
		// Ici, il est assum� que la traverse basse est g�r�e par l'enum Soubassement
		Measure<Length> bottomOffset = /* Units.inch(sash.getTopRelativePosition()).plus(sash.getDimension().getHeight()).plus( */getSoubassement().traverseBasse
		/* ) */.plus(ProductData.getSpacer(ProductType.CASEMENT, Data.VISIBLEGLASS_TO_GRILLES)[2]);
		Measure<Length> hauPanneau = getHauSbs().minus(getLarTraItr()).minus(bottomOffset);

		calculationParameters.put("traItr", getSoubassement().traItr); // Traverse Intermediaire
		calculationParameters.put("nbPanneaux", getNbPanneaux());
		calculationParameters.put("hauteurPanneau", hauPanneau);
		calculationParameters.put("largeurTraverseInter", getLarTraItr());
		calculationParameters.put("largeurEntrePanneaux", ProductData.getValue(ProductType.CASEMENT, Data.LAR_ENTRE_PANNEAUX));
	}

	@Override
	protected void updateSimpleValidations(ValidationResult validationResult)
	{
		super.updateSimpleValidations(validationResult);
		if (getSplittingGrillesForm() == null)
		{
			validationResult.addError(traductionBundle.getString("SplittingGrillesFormIsNull")); //$NON-NLS-1$
		}
	}

	@Override
	public void afterHauSbsChanged(Measure<Length> hauSbs)
	{
		// TODO Ajouter de la validation. La hauteur doit �tre entre 2 valeures fixes.
		super.afterHauSbsChanged(hauSbs);
		calculate();
	}

	@Override
	public void afterNbPanneauxChanged(Integer nbPanneaux)
	{
		// TODO Ajouter de la validation. Le nombre de panneaux ne peut pas �tre infini.
		super.afterNbPanneauxChanged(nbPanneaux);
		calculate();
	}

	@Override
	public void afterLarTraItrChanged(Measure<Length> larTraItr)
	{
		super.afterLarTraItrChanged(larTraItr);
		calculate();
	}

	@Override
	public void afterSoubassementChanged(Soubassement soubassement)
	{
		super.afterSoubassementChanged(soubassement);

		// Set fields driven by the enum.
		if (!getSoubassement().hauSbs.equals(Measure.valueOf(0d, Units.MM)))
		{
			// Standard values
			setHauSbs(getSoubassement().hauSbs);
			makeInvisible(PROPERTYNAME_HAUSBS);
		} /* end if */
		else
		{
			// Manual entry
			removePropertyOverride(PROPERTYNAME_HAUSBS);
			makeVisible(PROPERTYNAME_HAUSBS);
			calculate();
		} /* end else */

		// Cette facon de faire le lien entre les 2 enums est discutable.
		// Il serait preferable de mettre TypeDeTraverse dans l'enum TypeDeSoubassement a la place d'un Measure
		// et assigner directement la valeur de un a l'autre.
		// switch (new Double(getSoubassement().traverseBasse.doubleValue(Units.MM)).intValue())
		// {
		// case 111:
		// ((CasementSash) getParent().getParent()).setTypeDeTraverseBasse(TypeDeTraverseBasse.TRAVERSE111);
		// break;
		//
		// case 151:
		// ((CasementSash) getParent().getParent()).setTypeDeTraverseBasse(TypeDeTraverseBasse.TRAVERSE151);
		// break;
		//
		// case 196:
		// ((CasementSash) getParent().getParent()).setTypeDeTraverseBasse(TypeDeTraverseBasse.TRAVERSE196);
		// break;
		//
		// case 75:
		// ((CasementSash) getParent().getParent()).setTypeDeTraverseBasse(TypeDeTraverseBasse.TRAVERSE75);
		// break;
		//
		// case 91:
		// ((CasementSash) getParent().getParent()).setTypeDeTraverseBasse(TypeDeTraverseBasse.TRAVERSE91);
		// break;
		//
		// default:
		// ((CasementSash) getParent().getParent()).setTypeDeTraverseBasse(TypeDeTraverseBasse.TRAVERSE75);
		// break;
		// } /* end case */
	}

	@Override
	public void afterTypeDeTraverseIntermediaireChanged(TypeDeTraverseIntermediaire typeDeTraverseIntermediaire)
	{
		super.afterTypeDeTraverseIntermediaireChanged(typeDeTraverseIntermediaire);

		setLarTraItr(getTypeDeTraverseIntermediaire().larTraItr);
		calculate();
	}

	@Override
	public void afterTypeDePanneauxChanged(TypeDePanneaux typeDePanneaux)
	{
		super.afterTypeDePanneauxChanged(typeDePanneaux);
		afterCalculate();
	}

	/**
	 * When the form dimension is changed, make sure the user inputs still make sense.
	 */
	public void formDimensionChanged()
	{
		// TODO Validation apr�s le changement de dimension du vantail.
	}

	@Override
	/**
	 * After the glasses have been created, activate the kick panel in all bottom glasses.
	 * Set the panel type specified in the enum "TypeDePanneaux"
	 * Set a glass if panneaux = false
	 */
	public void afterCalculate()
	{
		super.afterCalculate();

		com.netappsid.wadconfigurator.InsulatedGlass[] insulatedGlass = ((Section) getParent()).getInsulatedGlass();
		if (insulatedGlass != null && insulatedGlass.length > 0)
		{
			for (int i = insulatedGlass.length - 1; i > 0; i--)
			{
				if (!getSoubassement().panneaux)
				{
					insulatedGlass[i].deleteKickPanelModel();
				} /* end if */
				else if (insulatedGlass[i].getKickPanelModel() == null || ((InsulatedGlass) insulatedGlass[i]).getTypeDePanneaux() != getTypeDePanneaux())
				{
					insulatedGlass[i].deleteKickPanelModel();
					((InsulatedGlass) insulatedGlass[i]).setKickPanelModel(getTypeDePanneaux().configClass);

					// Enlever la r�plication du verre transparent pour conserver nos sous-bassements
					// ((InsulatedGlass) ((Section) getParent()).getInsulatedGlass()[i]).setReplicationExclusions(InsulatedGlass.PROPERTYNAME_SLABELEMENT);
				} /* end if */
			} /* end for */

		} /* end if */
	}
}
