// $Id: SliderMainMeetingRail.java,v 1.5 2011-06-10 19:02:15 sboule Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.commonutils.math.Units;
import com.netappsid.erp.configurator.Configurable;

public class SliderMainMeetingRail extends com.client360.configuration.wad.SliderMainMeetingRailBase
{

	public SliderMainMeetingRail(Configurable parent)
	{
		super(parent);
	}

	@Override
	public Measure<Length> getSameRailLeftSashOffset()
	{
		return Measure.valueOf(1d, Units.INCH);
	}

	@Override
	public Measure<Length> getSameRailRightSashOffset()
	{
		return Measure.valueOf(1d, Units.INCH);
	}

	@Override
	public Measure<Length> getLeftSashOffset()
	{
		return Measure.valueOf(1d, Units.INCH);
	}

	@Override
	public Measure<Length> getRightSashOffset()
	{
		return Measure.valueOf(1d, Units.INCH);
	}

	@Override
	public Measure<Length> getSameRailLeftGlassOffset()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length> getSameRailRightGlassOffset()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length> getLeftGlassOffset()
	{
		return Measure.valueOf(1d, Units.INCH);
	}

	@Override
	public Measure<Length> getRightGlassOffset()
	{
		return Measure.valueOf(1d, Units.INCH);
	}

	@Override
	public Measure<Length> getMullionThickness()
	{
		return Units.inch(3.5d);
	}

	@Override
	public Measure<Length> getMullionCenterCorrection()
	{
		return Units.inch(0d);
	}
}
