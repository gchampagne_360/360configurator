// $Id: CasementTiltTurnSashSection.java,v 1.1 2011-04-27 12:09:13 jddaigle Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

public class CasementTiltTurnSashSection extends com.client360.configuration.wad.CasementTiltTurnSashSectionBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public CasementTiltTurnSashSection(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	@Override
	public Measure<Length>[] getGlassOffset()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length>[] getSplittingGrillesOffset()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length>[] getNoSplittingGrillesOffset()
	{
		// TODO Auto-generated method stub
		return null;
	}

}
