// $Id: HungMainSash.java,v 1.5 2011-09-07 19:04:33 sboule Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.netappsid.commonutils.geo.CornerType;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.rendering.common.advanced.Drawing;
import com.netappsid.rendering.common.advanced.Profile;
import com.netappsid.wadconfigurator.SashSection;

public class HungMainSash extends com.client360.configuration.wad.HungMainSashBase
{

	public HungMainSash(Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		setDefaultProfiles(getClientProfiles());
		setDefaultGlazingBeadProfiles(getClientGlazingBeadProfiles());
	}

	/**
	 * @see com.netappsid.wadconfigurator.Sash#getClientProfiles()
	 */
	public Profile[] getClientProfiles()
	{
		return ProductData.getProfiles(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_PROFILES);
	}

	public Profile[] getClientGlazingBeadProfiles()
	{
		return ProductData.getProfiles(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_PROFILES);

	}

	@Override
	public Measure<Length> getSectionMaxWidth()
	{
		return null;
	}

	@Override
	public Measure<Length> getSectionMinWidth()
	{
		return null;
	}

	// Add custom overrides here

	/**
	 * @return
	 * @see com.netappsid.wadconfigurator.Sash#getCornerType()
	 */
	@Override
	public CornerType[] getCornerType()
	{
		return ProductData.getCornerType(ProductType.HUNG_PVC, Data.SASH_CORNERTYPE);
	}

	// /**
	// * @param nbSections
	// * @return
	// * @see com.netappsid.wadconfigurator.Sash#sashSectionMakeArray(int)
	// */
	// @Override
	// public SashSection[] sashSectionMakeArray(int nbSections)
	// {
	// return makeConfigurableArray(HungMainSashSection.class, nbSections, this);
	// }

	/**
	 * @return
	 * @see com.netappsid.wadconfigurator.Sash#getSashFrameThickness()
	 */
	@Override
	public Measure<Length>[] getSashFrameThickness()
	{
		if (getIndex() == 0)
		{
			return ProductData.getSpacer(ProductType.HUNG_PVC, Data.EXT_SASH_BORDER_SPACERS);
		}
		else
		{
			return ProductData.getSpacer(ProductType.HUNG_PVC, Data.INT_SASH_BORDER_SPACERS);
		}
	}

	@Override
	public Drawing getCad()
	{
		return null;
	}

	@Override
	public Measure<Length>[] getGlazingBeadThickness()
	{
		return ProductData.getSpacer(ProductType.HUNG_PVC, Data.FRAME_TO_EXT_GLAZINGBEAD_OFFSET_SPACERS);
	}

	@Override
	public Measure<Length>[] getVisibleGlassOffsetSpacer()
	{
		return null;
	}

	@Override
	public SashSection sashSectionClass()
	{
		return new HungMainSashSection(this);
	}
}
