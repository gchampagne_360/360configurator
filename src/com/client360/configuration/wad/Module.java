// $Id: $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.commonutils.geo.CornerType;
import com.netappsid.commonutils.geo.Form;
import com.netappsid.commonutils.math.Units;

public class Module extends com.client360.configuration.wad.ModuleBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public Module(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	public Measure<Length>[] getFrameBorderSpacer(Form moduleForm)
	{
		Measure<Length> m = Units.inch(1.25d);
		return new Measure[] { m, m, m, m, m, m, m, m, m, m, m, m, m };
	}

	public CornerType[] getFrameCornerType(Form moduleForm)
	{
		return new CornerType[] { CornerType.HALF_CORNER, CornerType.HALF_CORNER, CornerType.HALF_CORNER, CornerType.HALF_CORNER, CornerType.HALF_CORNER,
				CornerType.HALF_CORNER, CornerType.HALF_CORNER, CornerType.HALF_CORNER, CornerType.HALF_CORNER, CornerType.HALF_CORNER, CornerType.HALF_CORNER,
				CornerType.HALF_CORNER };
	}

	@Override
	public Measure<Length>[] getFrameToMullionOffset()
	{
		Measure<Length> m = Units.inch(0.875d);
		return new Measure[] { m, m, m, m, m, m, m, m, m, m, m, m, m };
	}
	
	@Override
	protected boolean isLegacyFramesModeEnabled()
	{
		return false;
	}
}
