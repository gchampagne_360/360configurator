// $Id: JambOther.java,v 1.3 2011-04-27 12:09:13 jddaigle Exp $

package com.client360.configuration.wad;

import com.netappsid.erp.configurator.Configurable;

public class JambOther extends com.client360.configuration.wad.JambOtherBase
{
	public JambOther(Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
		setCustomDim(0.0);
	}

	@Override
	public double getCoteA()
	{
		return getCustomDim();
	}

	@Override
	public void afterCustomDimChanged(Double customDim)
	{
		super.afterCustomDimChanged(customDim);
		updateRendering();
	}
}
