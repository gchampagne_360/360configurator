// $Id: SliderSash.java,v 1.6 2011-09-07 19:04:34 sboule Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.erp.configurator.Configurable;
import com.netappsid.rendering.common.advanced.Drawing;
import com.netappsid.wadconfigurator.SashSection;

public class SliderSash extends com.client360.configuration.wad.SliderSashBase
{

	public SliderSash(Configurable parent)
	{
		super(parent);
	}

	@Override
	public Measure<Length>[] getGlazingBeadOffset()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length>[] getScreenOffset()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length>[] getGlassOffset()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Drawing getCad()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length>[] getVisibleGlassOffsetSpacer()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length>[] getGlazingBeadThickness()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SashSection sashSectionClass()
	{
		// TODO Auto-generated method stub
		return null;
	}
}
