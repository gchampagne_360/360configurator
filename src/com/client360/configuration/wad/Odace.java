// $Id: $

package com.client360.configuration.wad;

import static com.client360.configuration.wad.utils.FenergicUtils.ALUColorChart;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.netappsid.commonutils.geo.CornerType;
import com.netappsid.rendering.common.advanced.Drawing;
import com.netappsid.wadconfigurator.Sash;
import com.netappsid.wadconfigurator.WindowsMullion;
import com.netappsid.wadconfigurator.enums.ChoiceSwing;

public class Odace extends com.client360.configuration.wad.OdaceBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public Odace(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
		setChoiceSwing(ChoiceSwing.INSWING);
		setExteriorColor(new StandardColor(this));
		setInteriorColor(new StandardColor(this));
	}

	@Override
	public WindowsMullion[] mullionMakeArray(int nbMullion)
	{
		return makeArray(OdaceMeneau.class, nbMullion, this);
	}

	@Override
	public Sash[] sashMakeArray(int nbSections)
	{
		return makeArray(OdaceVantail.class, nbSections, this);
	}

	@Override
	public void setDivisionDimension()
	{}

	@Override
	public Measure<Length>[] getGlassOffset()
	{
		return ProductData.getSpacer(ProductType.ODACE, Data.FRAME_TO_GLASS_OFFSET_SPACERS);
	}

	@Override
	public Measure<Length>[] getSashOffset()
	{
		return ProductData.getSpacer(ProductType.ODACE, Data.FRAME_TO_SASH_OFFSET_SPACERS);
	}

	@Override
	public Measure<Length>[] getFrameBorder()
	{
		return ProductData.getSpacer(ProductType.ODACE, Data.FRAME_BORDER_SPACERS);
	}

	@Override
	public Measure<Length>[] getMullionOffset()
	{
		return ProductData.getSpacer(ProductType.ODACE, Data.FRAME_TO_MULLION_OFFSET_SPACER);
	}

	@Override
	public CornerType[] getCornerType()
	{
		return ProductData.getCornerType(ProductType.ODACE, Data.FRAME_CORNERTYPE);
	}

	@Override
	public Drawing getCad()
	{
		return null;
	}

	@Override
	public String productCode()
	{
		return null;
	}

	@Override
	public String colorChartNameExterior()
	{
		return ALUColorChart;
	}

	@Override
	public String colorChartNameInterior()
	{
		return ALUColorChart;
	}

	@Override
	public boolean useAutomaticGrilleAlignement()
	{
		return false;
	}

	@Override
	public void init(Object[] params)
	{
		// TODO Auto-generated method stub

	}
}
