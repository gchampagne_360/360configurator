// $Id: PortailDoor.java,v 1.4 2011-05-26 19:01:05 jcfortier Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.gates.PortailCaravelleM40;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.rendering.common.advanced.Drawing;
import com.netappsid.wadconfigurator.SlabElement;
import com.netappsid.wadconfigurator.SlabElementBackgroundContainer;

public class PortailDoor extends com.client360.configuration.wad.PortailDoorBase
{

	public PortailDoor(Configurable parent)
	{
		super(parent);
	}

	@Override
	public Drawing getCad()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateComponents()
	{
		super.updateComponents();

		if (getPanel() == null)
		{
			return;
		}
		SlabElement[] panel = getPanel();

		for (SlabElement slabElement : panel)
		{
			if (slabElement.getName().equals(PortailCaravelleM40.PANNEAU_TEXTURE_GAUCHE))
			{
				slabElement.setSlabElementBackground(new SlabElementBackgroundContainer("/images/lambris45.png"));
			}
		}

	}

	@Override
	public Measure<Length> getSillGap()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length> getSlabGap()
	{
		// TODO Auto-generated method stub
		return null;
	}
}
