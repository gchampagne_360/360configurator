// $Id: CasementVisionPvc.java,v 1.22 2011-06-22 19:37:09 sboule Exp $

package com.client360.configuration.wad;

import static com.client360.configuration.wad.utils.FenergicUtils.ALUColorChart;
import static com.client360.configuration.wad.utils.FenergicUtils.PVCColorChart;

import java.util.List;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.cad.CadFrame;
import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.client360.configuration.wad.enums.ChoiceAdvancedGrilles;
import com.client360.configuration.wad.enums.ChoiceFrameDepth;
import com.jgoodies.validation.Severity;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.message.SimpleValidationMessage;
import com.netappsid.commonutils.geo.CornerType;
import com.netappsid.commonutils.math.Units;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.rendering.common.advanced.Drawing;
import com.netappsid.wadconfigurator.AdvancedSectionGrilles;
import com.netappsid.wadconfigurator.Sash;
import com.netappsid.wadconfigurator.WindowsMullion;
import com.netappsid.wadconfigurator.enums.ChoiceSwing;

public class CasementVisionPvc extends com.client360.configuration.wad.CasementVisionPvcBase
{
	Drawing frame;

	public CasementVisionPvc(Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		new ProductData();

		setChoiceFrameDepth(ChoiceFrameDepth.F412);

		makeInvisible(PROPERTYNAME_CHOICEADVANCEDGRILLES);

		setDefaultHybrid(false);

		setChoiceSwing(ChoiceSwing.INSWING);

		frame = new CadFrame();

		// setMullionSplitVisibleGlass(false);

		// setVertical(true); // Vertical casement (Horizontal Mullions)

		// setExteriorRendering(false);
	}

	@Override
	public double getBasePrice()
	{
		if (getDimension() != null && getDimension().getHeight() != null && getDimension().getWidth() != null && getNbSections() != null)
		{
			return getDimension().getHeight().doubleValue(Units.FOOT) * getDimension().getWidth().doubleValue(Units.FOOT) * 250.0 + getNbSections() * 150;
		}
		return 0.0;
	}

	static boolean test = true;

	@Override
	public void activateListener()
	{
		super.activateListener();

		if (test)
		{
			publishPropertyInTopParent(PROPERTYNAME_NBSECTIONS, "TestNbSectionsXXX");
			publishPropertyInTopParent(PROPERTYNAME_INTERIORCOLOR, "TestColor");
			test = false;
		} /* end if */
		else
		{
			subscribe(PROPERTYNAME_NBSECTIONS, "TestNbSectionsXXX");
			subscribe(PROPERTYNAME_INTERIORCOLOR, "TestColor");
		} /* end else */
	}

	@Override
	public WindowsMullion[] mullionMakeArray(int nbSections)
	{
		return makeArray(CasementVisionPvcMullion.class, nbSections, this);
	}

	@Override
	public Sash[] sashMakeArray(int nbSections)
	{
		return makeArray(CasementVisionPvcSash.class, nbSections, this);
	}

	@Override
	public Measure<Length>[] getSashOffset()
	{
		return ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_SASH_OFFSET_SPACERS_HYBRID);
	}

	@Override
	public Measure<Length>[] getMullionOffset()
	{
		return ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_MULLION_OFFSET_SPACER);
	}

	@Override
	public Measure<Length>[] getFrameBorder()
	{
		return ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_BORDER_SPACERS);
	}

	@Override
	public Measure<Length>[] getGlassOffset()
	{
		return ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_GLASS_OFFSET_SPACERS);
	}

	@Override
	public CornerType[] getCornerType()
	{
		return ProductData.getCornerType(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_CORNERTYPE);
	}

	@Override
	public void afterChoiceFrameDepthChanged(ChoiceFrameDepth choiceFrameDepth)
	{
		super.afterChoiceFrameDepthChanged(choiceFrameDepth);
		setFrameDepth(choiceFrameDepth.thickness);
	}

	@Override
	public void afterHybridChanged(Boolean hybrid)
	{
		super.afterHybridChanged(hybrid);

		if (getHybrid() != null && getHybrid())
		{
			setChoiceFrameDepth(ChoiceFrameDepth.F412);
		}

		splitSection();
	}

	public Double[] getConstantViewOnTop()
	{
		Double depthIn = ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.DEPTH_IN).doubleValue(Units.MM);
		Double depthOut = Measure.valueOf(4.5, Units.INCH).doubleValue(Units.MM);
		Double widthIn = ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.WIDTH_IN).doubleValue(Units.MM);
		Double widthOut = ProductData.getValue(ProductType.CASEMENT_VISION_PVC_IN, Data.WIDTH_OUT).doubleValue(Units.MM);

		return new Double[] { widthIn, widthOut, depthIn, depthOut };
	}

	@Override
	public String colorChartNameExterior()
	{
		if (getHybrid() != null && getHybrid())
		{
			if (getExteriorColor() instanceof StandardColor)
			{
				return ALUColorChart;
			}
			else
			{
				return "";
			}
		}
		else
		{
			if (getExteriorColor() instanceof StandardColor)
			{
				return PVCColorChart;
			}
			else
			{
				return "";
			}
		}
	}

	@Override
	public String colorChartNameInterior()
	{
		if (getHybrid() != null && getHybrid())
		{
			if (getInteriorColor() instanceof StandardColor)
			{
				return PVCColorChart;
			}
			else
			{
				return "";
			}
		}
		else
		{
			return PVCColorChart;
		}
	}

	@Override
	public int getNbSash()
	{
		return 0;
	}

	@Override
	public String productCode()
	{
		return null;
	}

	@Override
	public void setDivisionDimension()
	{}

	@Override
	public boolean useAutomaticGrilleAlignement()
	{
		return false;
	}

	@Override
	public String getName()
	{
		return clientTraductionBundle.getString("CasementVisionPvc");
	}

	@Override
	public String toString()
	{
		if (getParent() instanceof ProductTemplate)
		{
			return getParent().getIndex() + 1 + "-" + clientTraductionBundle.getString("CasementVisionPvc");
		} /* end if */
		else
		{
			return clientTraductionBundle.getString("CasementVisionPvc");
		} /* end else */
	}

	public boolean isSectionVisible()
	{
		return false;
	}

	public boolean isDivisionVisible()
	{
		return false;
	}

	@Override
	public boolean isSectionInputVisible()
	{
		if (getDimension() != null && getDimension().getWidth() != null && getDimension().getHeight() != null
				&& getDimension().getWidth().isGreaterThan(Measure.valueOf(0, Units.INCH))
				&& getDimension().getHeight().isGreaterThan(Measure.valueOf(0, Units.INCH)))
		{
			return true;
		}
		else
		{
			return false;
		} /* end else */
	}

	public boolean isExteriorColorColorChartEnabled()
	{
		return true;
	}

	public boolean isExteriorColorNonStandardColorEnabled()
	{
		return true;
	}

	public boolean isInteriorColorColorChartEnabled()
	{
		return false;
	}

	public boolean isInteriorColorNonStandardColorEnabled()
	{
		return false;
	}

	@Override
	public void afterFrameDepthChanged(Measure<Length> frameDepth)
	{
		super.afterFrameDepthChanged(frameDepth);
		splitSection();
	}

	// public boolean isHybridEnabled()
	// {
	// return true;
	// }

	public boolean isHybridVisible()
	{
		return true;
	}

	// public boolean isChoiceFrameDepthVisible()
	// {
	// return getNbSections() != null;
	// }

	public boolean isChoicePresetWindowWidthVisible()
	{
		return isSectionInputVisible();
	}

	@Override
	public Drawing getCad()
	{
		// frame.hideAllLayers(CadFrame.GROUP_FRAME);
		//
		// if (getChoiceFrameDepth() == ChoiceFrameDepth.F5)
		// {
		// frame.showLayer(CadFrame.LAYER_FRAME5);
		// }
		// else
		// {
		// frame.showLayer(CadFrame.LAYER_FRAME45);
		// }
		//
		// // frame.showLayer(CadFrame.LAYER_FRAMEEXTENTION);
		// // frame.getLayer(CadFrame.LAYER_FRAMEEXTENTION).setGraphic(new Graphic("/images/jpgWoods/Oak1r(30).jpg"));
		// // frame.getLayer(CadFrame.LAYER_FRAMEEXTENTION).setParameters(0.75*25.4, 2.5*25.4);
		// // frame.getLayer(CadFrame.LAYER_FRAMEEXTENTION).getPattern().getSegment(PatternFrameExtention.EMPTY).setLength(20);
		// // frame.getLayer(CadFrame.LAYER_FRAMEEXTENTION).getPattern().getSegment(PatternFrameExtention.TEXTURED).setLength(2.5*25.4 - 20);
		//
		// if (getExteriorColor() == null)
		// {
		// return null;
		// }
		//
		// Filling filling = getExteriorColor().getFilling();
		// frame.getLayer(CadFrame.LAYER_FRAMEALUCAP).getPattern().getSegment("1").setFilling(filling);
		//
		//
		// if(hasViewOnTopVisibleEastComponents())
		// {
		// frame.getLayer(CadFrame.LAYER_FRAMEALUCAP).setRightVisible(false);
		// }
		// else
		// {
		// frame.getLayer(CadFrame.LAYER_FRAMEALUCAP).setRightVisible(true);
		// }
		//
		// if(hasViewOnTopVisibleWestComponents())
		// {
		// frame.getLayer(CadFrame.LAYER_FRAMEALUCAP).setLeftVisible(false);
		// }
		// else
		// {
		// frame.getLayer(CadFrame.LAYER_FRAMEALUCAP).setLeftVisible(true);
		// }
		// //
		// return frame;
		// return new CadDormant();
		// return new DefaultFrameCad();
		return null;
	}

	@Override
	public void afterChoiceAdvancedGrillesChanged(ChoiceAdvancedGrilles choiceAdvancedGrilles)
	{
		if (choiceAdvancedGrilles == null) // No selection
		{
			setAdvancedSectionGrilles((AdvancedSectionGrilles) null);
		}
		else
		{
			setAdvancedSectionGrilles(choiceAdvancedGrilles.configClass);
		}
	}

	// exemple de messages
	public List<ValidationMessage> getChoiceAdvancedGrillesValidation()
	{
		List<ValidationMessage> messages = createValidationMessageList();

		messages.add(new SimpleValidationMessage(clientTraductionBundle.getString("AssemblyFrameValue"), Severity.WARNING));
		return messages;
	}

	@Override
	public void afterNbSectionsChanged(Integer nbSections)
	{
		super.afterNbSectionsChanged(nbSections);
		// getDimension().setMaxWidth(nbSections * getSash()[0].getSectionMinWidth());
	}

	public String getNbSectionsInfo()
	{
		return "Attention de ne pas mettre trop de sections!";
	}

	@Override
	public void init(Object[] params)
	{
		// TODO Auto-generated method stub

	}
}
