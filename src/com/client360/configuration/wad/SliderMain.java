// $Id: SliderMain.java,v 1.6 2011-06-10 14:23:19 sboule Exp $

package com.client360.configuration.wad;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.commonutils.geo.CornerType;
import com.netappsid.commonutils.math.Units;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.wadconfigurator.MeetingRail;
import com.netappsid.wadconfigurator.Sash;
import com.netappsid.wadconfigurator.SliderSash;
import com.netappsid.wadconfigurator.enums.ChoiceOpeningSide;

public abstract class SliderMain extends com.client360.configuration.wad.SliderMainBase
{

	public SliderMain(Configurable parent)
	{
		super(parent);
	}

	public boolean isSectionVisible()
	{
		return false;
	}

	public boolean isInstalledVisible()
	{
		return false;
	}

	public boolean isSashVisible()
	{

		if (super.isSashVisible() && getNbSections() != null && getNbSections() > 1 && getDimension().getWidth().isGreaterThan(Measure.valueOf(0, Units.MM))
				&& getDimension().getHeight().isGreaterThan(Measure.valueOf(0, Units.MM)))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public boolean isInteriorSashVisible()
	{
		if (super.isSashVisible() && getNbSections() != null && getNbSections() > 1 && getDimension().getWidth().isGreaterThan(Measure.valueOf(0, Units.MM))
				&& getDimension().getHeight().isGreaterThan(Measure.valueOf(0, Units.MM)))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public void setDefaultValues()
	{
		// TODO Auto-generated method stub
		super.setDefaultValues();
		super.setDefaultValues();
		setDefaultWithInsulatedGlass(true);
		// makeInvisible(PROPERTYNAME_CHOICEOPENINGTYPE);
		getDimension().makeInvisible(DimensionBase.PROPERTYNAME_CHOICEFORM);
		makeInvisible(PROPERTYNAME_INTERIORSASH);
		makeInvisible(PROPERTYNAME_DIVISION);
		makeInvisible(PROPERTYNAME_WITHINSULATEDGLASS);
		setDefaultChoiceOpeningSide(ChoiceOpeningSide.LEFT);
		makeInvisible(PROPERTYNAME_ENERGYSTAR);
		makeInvisible(PROPERTYNAME_SAMEGRILLES);

		// TODO Add AdvancedSectionGrilles in the slider
		makeInvisible(PROPERTYNAME_CHOICEADVANCEDGRILLES); // ############
	}

	@Override
	public SliderSash[] interiorSashMakeArray(int nbSections)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CornerType[] getCornerType()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length>[] getGlassOffset()
	{
		Measure<Length>[] FRAME_TO_INT_SASH_OFFSET_SPACERS_VALUES = null;

		Measure[] value = { Measure.valueOf(51.59375, Units.MM), Measure.valueOf(30d, Units.MM), Measure.valueOf(51.59375, Units.MM),
				Measure.valueOf(30d, Units.MM) };

		FRAME_TO_INT_SASH_OFFSET_SPACERS_VALUES = value;

		return FRAME_TO_INT_SASH_OFFSET_SPACERS_VALUES;
	}

	@Override
	public Measure<Length>[] getFrameBorder()
	{
		return null;
	}

	@Override
	public Measure<Length>[] getMullionOffset()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measure<Length>[] getSashOffset(int railNumber)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MeetingRail[] meetingRailMakeArray(int nbSections)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Sash[] sashMakeArray(int nbSections)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String colorChartNameExterior()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String colorChartNameInterior()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String productCode()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean useAutomaticGrilleAlignement()
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Measure<Length>[] getGlazingBeadThickness()
	{
		return null;
	}

	// Add custom overrides here
	public abstract Measure<Length>[] getLMouldingOffset();

	public abstract Measure<Length>[] getLMouldingExteriorOffset();

	public abstract Measure<Length> getSashStopOffset();

	public abstract Measure<Length> getSashStopExteriorOffset();
}
