// $Id: SpecialMuntinQuestionary.java,v 1.3 2011-06-20 19:15:03 sboule Exp $

package com.client360.configuration.wad;

import com.client360.configuration.wad.grilles.CalculationSpecialMuntin;
import com.netappsid.wadconfigurator.grilles.GrillesManager;

public class SpecialMuntinQuestionary extends com.client360.configuration.wad.SpecialMuntinQuestionaryBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public SpecialMuntinQuestionary(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	@Override
	public Class<? extends GrillesManager> getGrillesManagerClass()
	{
		return CalculationSpecialMuntin.class;
	}
}
