// $Id: SpecialQuestionary.java,v 1.5 2011-06-22 19:37:09 sboule Exp $

package com.client360.configuration.wad;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.constant.Data;
import com.client360.configuration.wad.constant.ProductData;
import com.client360.configuration.wad.constant.ProductType;
import com.client360.configuration.wad.enums.ChoiceParametricModel;
import com.client360.configuration.wad.grilles.CalculationSpecial;
import com.jgoodies.validation.Severity;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.message.SimpleValidationMessage;
import com.netappsid.commonutils.math.Units;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.wadconfigurator.Section;
import com.netappsid.wadconfigurator.grilles.GrillesManager;

public class SpecialQuestionary extends com.client360.configuration.wad.SpecialQuestionaryBase
{
	private static final long serialVersionUID = 1L;
	private List<ValidationMessage> messagesHorizTdlPosition = null;
	private Double minPos = 0d;
	private boolean initial;

	public SpecialQuestionary(Configurable parent)
	{
		super(parent);
	}

	@Override
	public Class<? extends GrillesManager> getGrillesManagerClass()
	{
		return CalculationSpecial.class;
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		initial = true;

		// Set default values.
		setDefaultHorizTdlPosition(Units.inch(2d * ((Section) getParent()).getDimension().getHeight().doubleValue(Units.INCH) / 3d)); // Default: 2/3 from the
																																		// top.
		setDefaultNbSdlHorizBars(1);
		setDefaultNbSdlVertiBars(1);
		setDefaultWidthSdl(ProductData.getValue(ProductType.INTERNAL_MUNTIN, Data.INTERNAL_MUNTIN_SDL_THICKNESS));
		setDefaultWidthVertiTdl(ProductData.getValue(ProductType.INTERNAL_MUNTIN, Data.INTERNAL_MUNTIN_THICKNESS));
		setDefaultWidthHorizTdl(ProductData.getValue(ProductType.INTERNAL_MUNTIN, Data.INTERNAL_MUNTIN_THICKNESS));

		minPos = ProductData.getSpacer(ProductType.CASEMENT_VISION_PVC_IN, Data.FRAME_TO_GLASS_OFFSET_SPACERS)[0].doubleValue(Units.INCH);

		initial = false;
	}

	@Override
	public void activateListener()
	{
		super.activateListener();
		addPropertyChangeListener(PROPERTYNAME_SPLITTINGGRILLESFORM, "formDimensionChanged");
	}

	@Override
	public boolean beforeCalculate()
	{
		boolean valid = true;

		messagesHorizTdlPosition = null;

		if (getHorizTdlPosition() != null && getHorizTdlPosition().doubleValue(Units.INCH) < minPos)
		{
			messagesHorizTdlPosition = new ArrayList<ValidationMessage>();
			messagesHorizTdlPosition.add(new SimpleValidationMessage(traductionBundle.getString("Min_width", minPos), Severity.ERROR));
			valid = false;
		} /* end if */

		if (getHorizTdlPosition() != null && getHorizTdlPosition().doubleValue(Units.INCH) > Math.round(getSplittingGrillesForm().getHeight() - minPos))
		{
			messagesHorizTdlPosition = new ArrayList<ValidationMessage>();
			messagesHorizTdlPosition.add(new SimpleValidationMessage(traductionBundle.getString("Max_width",
					Math.round(getSplittingGrillesForm().getHeight() - minPos)), Severity.ERROR));
			valid = false;
		} /* end if */

		return valid;
	}

	@Override
	protected void feedCalculationParameters(Map<String, Object> calculationParameters)
	{
		super.feedCalculationParameters(calculationParameters);
		calculationParameters.put("nbSdlHorizBars", getNbSdlHorizBars());
		calculationParameters.put("nbSdlVertiBars", getNbSdlVertiBars());
		calculationParameters.put("widthSdl", getWidthSdl());
		calculationParameters.put("horizTdlPosition", getHorizTdlPosition());
		calculationParameters.put("widthVertiTdl", getWidthVertiTdl());
		calculationParameters.put("widthHorizTdl", getWidthHorizTdl());
	}

	@Override
	public void afterHorizTdlPositionChanged(Measure<Length> horizTdlPosition)
	{
		super.afterHorizTdlPositionChanged(horizTdlPosition);
		calculate();
	}

	@Override
	public void afterNbSdlHorizBarsChanged(Integer nbSdlHorizBars)
	{
		super.afterNbSdlHorizBarsChanged(nbSdlHorizBars);
		calculate();
	}

	@Override
	public void afterNbSdlVertiBarsChanged(Integer nbSdlVertiBars)
	{
		super.afterNbSdlVertiBarsChanged(nbSdlVertiBars);
		calculate();
	}

	@Override
	public void afterWidthHorizTdlChanged(Measure<Length> widthHorizTdl)
	{
		super.afterWidthHorizTdlChanged(widthHorizTdl);
		calculate();
	}

	@Override
	public void afterWidthSdlChanged(Measure<Length> widthSdl)
	{
		super.afterWidthSdlChanged(widthSdl);
		calculate();
	}

	@Override
	public void afterWidthVertiTdlChanged(Measure<Length> widthVertiTdl)
	{
		super.afterWidthVertiTdlChanged(widthVertiTdl);
		calculate();
	}

	/**
	 * Return the validation message to the user
	 * 
	 * @return The list of messages or an empty list if none.
	 */
	public List<ValidationMessage> getHorizTdlPositionValidation()
	{
		if (messagesHorizTdlPosition != null)
		{
			return messagesHorizTdlPosition;
		} /* end if */

		return createValidationMessageList();
	}

	/**
	 * When the form dimension is changed, make sure the user inputs still make sense.
	 */
	public void formDimensionChanged()
	{
		// Adjust the default value that depend on the dimension.
		setDefaultHorizTdlPosition(Units.inch(2d * ((Section) getParent()).getDimension().getHeight().doubleValue(Units.INCH) / 3d)); // Default: 2/3 from the
																																		// top.

		// If the height is too small for the specified horizTdlPosition, delete the override.
		if (getHorizTdlPosition() != null && getHorizTdlPosition().doubleValue(Units.INCH) > Math.round(getSplittingGrillesForm().getHeight() - minPos))
		{
			removePropertyOverride(PROPERTYNAME_HORIZTDLPOSITION);
			calculate();
		} /* end if */
	}

	@Override
	/**
	 * After the glasses have been created, activate the kick panel in both bottom glasses.
	 */
	public void after_updateState()
	{
		super.after_updateState();

		if (((Section) getParent()).getInsulatedGlass().length == 4)
		{
			if (((Section) getParent()).getInsulatedGlass()[2].getKickPanelModel() == null)
			{
				((InsulatedGlass) ((Section) getParent()).getInsulatedGlass()[2]).setChoiceParametricModel(ChoiceParametricModel.STDSLAB);
			} /* end if */

			if (((Section) getParent()).getInsulatedGlass()[3].getKickPanelModel() == null)
			{
				((InsulatedGlass) ((Section) getParent()).getInsulatedGlass()[3]).setChoiceParametricModel(ChoiceParametricModel.STDSLAB);
			} /* end if */
		} /* end if */
	}
}
