// $Id: $

package com.client360.configuration.blinds.vertical;

import com.client360.configuration.blinds.horizontal.colors.HorizontalColor;
import com.netappsid.configuration.common.Color;

public class VerticalRail extends com.client360.configuration.blinds.vertical.VerticalRailBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public VerticalRail(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	// Add custom overrides here
	@Override
	protected Class<? extends Color> getColorClass()
	{
		return HorizontalColor.class;
	}
}
