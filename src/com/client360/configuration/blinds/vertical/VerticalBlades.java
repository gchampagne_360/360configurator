// $Id: $

package com.client360.configuration.blinds.vertical;

import com.client360.configuration.blinds.vertical.color.VerticalColor;
import com.netappsid.configuration.blinds.Dimension;
import com.netappsid.configuration.blinds.verticalvenitian.VerticalBladesGroup;
import com.netappsid.configuration.common.Color;


public class VerticalBlades extends com.client360.configuration.blinds.vertical.VerticalBladesBase
{
    // Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
    public VerticalBlades(com.netappsid.erp.configurator.Configurable parent)
    {
        super(parent);
    }

	@Override
	protected Class<? extends VerticalBladesGroup> getVerticalBladesGroupClass()
	{
		return com.client360.configuration.blinds.vertical.VerticalBladesGroup.class;
	}

	@Override
	protected Class<? extends Dimension> getDimensionClass()
	{
		return com.client360.configuration.blinds.Dimension.class;
	}

	@Override
	protected Class<? extends Color> getColorsClass()
	{
		return VerticalColor.class;
	}
}
