// $Id: $

package com.client360.configuration.blinds.vertical;

import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.blinds.Dimension;
import com.netappsid.configuration.blinds.verticalvenitian.VerticalBlades;
import com.netappsid.configuration.blinds.verticalvenitian.VerticalRail;

public class Vertical extends com.client360.configuration.blinds.vertical.VerticalBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public Vertical(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		getDimension().setDefaultWidth(Units.mm(1000d));
		getDimension().setDefaultHeight(Units.mm(1000d));

		VerticalBlades vb = getVerticalBlades();
		vb.getVerticalBladesGroup()[0].setNbBlades(25);
		vb.setDefaultClipLength(Units.mm(20d));
		vb.setDefaultClipToBladeOffset(Units.mm(20d));
		vb.setDefaultBottomSewInsHeight(Units.mm(25d));
		vb.setDefaultTopOffset(Units.mm(10));
		vb.setDefaultLeftOffset(Units.mm(0d));
		vb.setDefaultRightOffset(Units.mm(0d));
		vb.setDefaultAngle(45d);
		vb.setDefaultThickness(Units.mm(40d));

		getRail().setHeight(Units.mm(20d));
	}

	@Override
	protected Class<? extends VerticalBlades> getVerticalBladesClass()
	{
		return com.client360.configuration.blinds.vertical.VerticalBlades.class;
	}

	@Override
	protected Class<? extends VerticalRail> getRailClass()
	{
		return com.client360.configuration.blinds.vertical.VerticalRail.class;
	}

	@Override
	protected Class<? extends com.netappsid.configuration.common.Dimension> getDimensionClass()
	{
		return Dimension.class;
	}
}
