package com.client360.configuration.blinds.vertical.modelListeners;

import java.util.List;

import com.client360.configuration.blinds.vertical.Vertical;
import com.netappsid.configuration.blinds.verticalvenitian.modelListeners.ModelListenerVerticalVenitian;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.erp.configurator.renderers.RenderingComponent;

public class ModelListenerVertical extends ModelListenerVerticalVenitian
{
	public ModelListenerVertical(Vertical configurable)
	{
		super(configurable);
	}

	public ModelListenerVertical(Configurable configurable, List<RenderingComponent> renderingComponents)
	{
		super(configurable, renderingComponents);
	}
}
