// $Id: $

package com.client360.configuration.blinds.vertical.color;

import java.awt.image.BufferedImage;

import com.netappsid.rendering.common.advanced.Filling;


public class VerticalColor extends com.client360.configuration.blinds.vertical.color.VerticalColorBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public VerticalColor(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}
	
	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
		makeDisabled(PROPERTYNAME_VERTICALCHOICECOLORTYPE);
	}

	@Override
	public void beforeVerticalChoiceColorChanged(VerticalChoiceColor verticalChoiceColor)
	{
		super.beforeVerticalChoiceColorChanged(verticalChoiceColor);
		setColors(verticalChoiceColor);
	}

	/**
	 * Initialize the color chart attributes according to the selected standard color choice
	 * 
	 * @param EnrDynChoixCouleur
	 *            - the selected standard color choice
	 */
	private void setColors(VerticalChoiceColor verticalChoiceColor)
	{
		if (verticalChoiceColor != null)
		{
			Integer red = verticalChoiceColor.getRed();
			Integer green = verticalChoiceColor.getGreen();
			Integer blue = verticalChoiceColor.getBlue();
			String texturePath = verticalChoiceColor.getTexturePath();
			if (red != null && green != null && blue != null)
			{
				setRed(red);
				setGreen(green);
				setBlue(blue);
			}
			if(texturePath != null)
			{
				setTexturePath(texturePath.replace("/com/mac/commun/store/images", "/com/client360/configuration/blinds/images"));
			}
			return;
		}
		
		setRed(255);
		setGreen(255);
		setBlue(255);
	}

	@Override
	public BufferedImage getTextureImage()
	{
		String texturePath = getTexturePath();
		if (texturePath != null)
		{
			return Filling.loadTextureImage(texturePath, this.getClass());
		}
		return null;
	}

	@Override
	public String toString()
	{
		VerticalChoiceColor verticalChoiceColor = getVerticalChoiceColor();
		if (verticalChoiceColor != null)
		{
			return verticalChoiceColor.getCode();
		}
		else
		{
			return null;
		}
	}
}
