// $Id: $

package com.client360.configuration.blinds;


public abstract class Color extends com.client360.configuration.blinds.ColorBase
{
    // Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
    public Color(com.netappsid.erp.configurator.Configurable parent)
    {
        super(parent);
    }
    
    @Override
    public void setDefaultValues()
    {
    	super.setDefaultValues();
    	makeInvisible(PROPERTYNAME_TEXTUREPATH);
    }
}
