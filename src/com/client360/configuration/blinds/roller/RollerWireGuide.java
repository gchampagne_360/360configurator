// $Id: $

package com.client360.configuration.blinds.roller;

import com.client360.configuration.blinds.roller.colors.RollerColor;
import com.netappsid.commonutils.geo.GeoFactory;
import com.netappsid.commonutils.geo.NAIDShape;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.common.VisualComposition;
import com.netappsid.configuration.common.utils.VisualCompositionUtils;


public class RollerWireGuide extends com.client360.configuration.blinds.roller.RollerWireGuideBase
{
    // Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
    public RollerWireGuide(com.netappsid.erp.configurator.Configurable parent)
    {
        super(parent);
    }
    
    // Add custom overrides here
    @Override
    public void setDefaultValues()
    {
    	super.setDefaultValues();

		setDefaultTopOffset(Units.mm(10));
		setDefaultLeftOffset(Units.mm(10));
		setDefaultRightOffset(Units.mm(10));
		setDefaultBottomOffset(Units.mm(100));

		setDefaultWiresWidth(Units.mm(8));
		setDefaultWiresTextureFileName("/com/client360/configuration/blinds/images/accessories/cable-sml.png");

		 makeVisible(PROPERTYNAME_BOTTOMOFFSET);
		 makeVisible(PROPERTYNAME_TOPOFFSET);
		 makeVisible(PROPERTYNAME_LEFTOFFSET);
		 makeVisible(PROPERTYNAME_RIGHTOFFSET);

		displayBrackets();
	}

	private void displayBrackets()
	{
		NAIDShape forme = NAIDShape.create(GeoFactory.rectangle(10, 10, false));
		VisualComposition equerre;

		equerre = VisualCompositionUtils.makeForm(forme, RollerColor.class, this);
		setDefaultLeftAnchorDisplay(equerre);

		equerre = VisualCompositionUtils.makeForm(forme, RollerColor.class, this);
		setDefaultRightAnchorDisplay(equerre);
	}
}
