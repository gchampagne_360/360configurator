// $Id: $

package com.client360.configuration.blinds.roller;

import com.client360.configuration.blinds.profiles.ProfileLame;
import com.client360.configuration.blinds.roller.colors.RollerColor;
import com.client360.configuration.wad.StandardColor;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.common.Color;
import com.netappsid.configuration.common.ImageCell;
import com.netappsid.configuration.common.VisualComposition;
import com.netappsid.configuration.common.utils.VisualCompositionUtils;

@SuppressWarnings("serial")
public class RollerBottomBar extends com.client360.configuration.blinds.roller.RollerBottomBarBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public RollerBottomBar(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		setColor(instanciateConfigurable(RollerColor.class, this));

		setDefaultHeight(Units.inch(7 / 8d));

		makeInvisible(PROPERTYNAME_MANTLING);
		makeInvisible(PROPERTYNAME_HASMANTLING);

		setDefaultLeftOffset(Units.inch(-0.25));
		setDefaultRightOffset(Units.inch(-0.25));

		setProfile(new ProfileLame());
	}

	@Override
	public void activateListener()
	{
		super.activateListener();

		setSewnInColorReplication();
	}

	@Override
	public void afterHasTipsChanged(Boolean hasTips)
	{
		super.afterHasTipsChanged(hasTips);

		VisualComposition leftTipDisplay = null;
		VisualComposition rightTipDisplay = null;

		if (hasTips)
		{
			leftTipDisplay = VisualCompositionUtils.makeImage("com/client360/configuration/blinds/images/accessories/windsor.png", Units.mm(35), Units.mm(25),
					this);
			((ImageCell) leftTipDisplay.getVisualCell()[0]).setDefaultTileEndH(-1d);
			rightTipDisplay = VisualCompositionUtils.makeImage("com/client360/configuration/blinds/images/accessories/windsor.png", Units.mm(35), Units.mm(25),
					this);
		}

		setLeftTipDisplay(leftTipDisplay);
		setRightTipDisplay(rightTipDisplay);
	}

	@Override
	public void afterSewnInsChanged(Boolean sewnIns)
	{
		super.afterSewnInsChanged(sewnIns);

		setSewnInColorReplication();
	}

	private void setSewnInColorReplication()
	{
		RollerColor rollerColor = (RollerColor) getColor();
		if (getSafeBoolean(getSewnIns()))
		{
			subscribe(PROPERTYNAME_COLOR, "Shade.color");
			makeInvisible(PROPERTYNAME_COLOR);
			rollerColor.removeRollerChoiceColor();
		}
		else
		{
			unsubscribe(PROPERTYNAME_COLOR);
			makeVisible(PROPERTYNAME_COLOR);
			if (rollerColor != null)
			{
				rollerColor.removeDefaultRollerChoiceColor();
			}
		}
	}

	@Override
	protected Class<? extends Color> getColorClass()
	{
		return StandardColor.class;
	}
}
