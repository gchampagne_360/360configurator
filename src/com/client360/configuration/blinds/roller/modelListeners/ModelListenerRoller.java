package com.client360.configuration.blinds.roller.modelListeners;

import java.util.List;

import com.client360.configuration.blinds.roller.Roller;
import com.netappsid.configuration.blinds.roller.modelListeners.ModelListenerRollerBlind;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.erp.configurator.renderers.RenderingComponent;

public class ModelListenerRoller extends ModelListenerRollerBlind
{
	public ModelListenerRoller(Roller configurable)
	{
		super(configurable);
	}

	public ModelListenerRoller(Configurable configurable, List<RenderingComponent> renderingComponents)
	{
		super(configurable, renderingComponents);
	}
}
