// $Id: $

package com.client360.configuration.blinds.roller.colors;

import java.awt.image.BufferedImage;

import com.netappsid.rendering.common.advanced.Filling;

public class RollerColor extends com.client360.configuration.blinds.roller.colors.RollerColorBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public RollerColor(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}
	
	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
		makeDisabled(PROPERTYNAME_ROLLERCHOICECOLORTYPE);
	}

	@Override
	public void beforeRollerChoiceColorChanged(RollerChoiceColor rollerChoiceColor)
	{
		super.beforeRollerChoiceColorChanged(rollerChoiceColor);
		setColors(rollerChoiceColor);
	}

	/**
	 * Initialize the color chart attributes according to the selected standard color choice
	 * 
	 * @param EnrDynChoixCouleur
	 *            - the selected standard color choice
	 */
	private void setColors(RollerChoiceColor rollerChoiceColor)
	{
		if (rollerChoiceColor != null)
		{
			Integer red = rollerChoiceColor.getRed();
			Integer green = rollerChoiceColor.getGreen();
			Integer blue = rollerChoiceColor.getBlue();
			String texturePath = rollerChoiceColor.getTexturePath();
			if (red != null && green != null && blue != null)
			{
				setRed(red);
				setGreen(green);
				setBlue(blue);
			}
			if(texturePath != null)
			{
				setTexturePath(texturePath.replace("/com/mac/commun/store/images", "/com/client360/configuration/blinds/images"));
			}
			return;
		}
		
		setRed(255);
		setGreen(255);
		setBlue(255);
	}

	@Override
	public BufferedImage getTextureImage()
	{
		String texturePath = getTexturePath();
		if (texturePath != null)
		{
			return Filling.loadTextureImage(texturePath, this.getClass());
		}
		return null;
	}

	@Override
	public String toString()
	{
		RollerChoiceColor rollerChoiceColor = getRollerChoiceColor();
		if (rollerChoiceColor != null)
		{
			return rollerChoiceColor.getCode();
		}
		else
		{
			return null;
		}
	}
}
