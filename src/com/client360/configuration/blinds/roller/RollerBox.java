// $Id: $

package com.client360.configuration.blinds.roller;

import com.client360.configuration.blinds.profiles.ProfileBoitier;
import com.client360.configuration.blinds.roller.colors.RollerColor;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.blinds.Dimension;
import com.netappsid.configuration.common.Color;

@SuppressWarnings("serial")
public class RollerBox extends com.client360.configuration.blinds.roller.RollerBoxBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public RollerBox(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		setDefaultHeight(Units.inch(2.5));

		setDefaultLeftOverflow(Units.inch(1));
		setDefaultRightOverflow(Units.inch(1));
		
		setDefaultProfile(new ProfileBoitier());
	}

	@Override
	protected Class<? extends Dimension> getDimensionClass()
	{
		return com.client360.configuration.blinds.Dimension.class;
	}

	@Override
	protected Class<? extends Color> getColorClass()
	{
		return RollerColor.class;
	}

	// Add custom overrides here
}
