// $Id: $

package com.client360.configuration.blinds.roller;

import com.client360.configuration.blinds.Dimension;
import com.client360.configuration.blinds.enums.ChoiceGuide;
import com.client360.configuration.blinds.roller.colors.RollerColor;
import com.netappsid.commonutils.geo.GeoFactory;
import com.netappsid.commonutils.geo.NAIDShape;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.blinds.Guide;
import com.netappsid.configuration.blinds.roller.RollerRoll;
import com.netappsid.configuration.common.FormCellBase;
import com.netappsid.configuration.common.VisualComposition;
import com.netappsid.configuration.common.utils.VisualCompositionUtils;

public class Roller extends com.client360.configuration.blinds.roller.RollerBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public Roller(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		setBracketsColor(instanciateConfigurable(RollerColor.class, this));
		displayBrackets();

		makeReadOnly(PROPERTYNAME_BRACKETSCOLOR);
		makeInvisible(PROPERTYNAME_BOX);

		getDimension().setDefaultWidth(Units.mm(1000d));
		getDimension().setDefaultHeight(Units.mm(1000d));

		setGuide();
	}

	@Override
	public void activateListener()
	{
		super.activateListener();

		publishProperty(PROPERTYNAME_BRACKETSCOLOR);
	}

	@Override
	protected Class<? extends RollerRoll> getRollClass()
	{
		return com.client360.configuration.blinds.roller.RollerRoll.class;
	}

	@Override
	protected Class<? extends com.netappsid.configuration.common.Dimension> getDimensionClass()
	{
		return Dimension.class;
	}

	private void displayBrackets()
	{
		NAIDShape forme = NAIDShape.create(GeoFactory.rectangle(10, 50, false));
		VisualComposition support;

		support = VisualCompositionUtils.makeForm(forme, RollerColor.class, this);
		support.getVisualCell()[0].subscribe(FormCellBase.PROPERTYNAME_COLOR, Roller.PROPERTYNAME_BRACKETSCOLOR);
		setDefaultLeftBracketDisplay(support);

		support = VisualCompositionUtils.makeForm(forme, RollerColor.class, this);
		support.getVisualCell()[0].subscribe(FormCellBase.PROPERTYNAME_COLOR, Roller.PROPERTYNAME_BRACKETSCOLOR);
		setDefaultRightBracketDisplay(support);
	}

	@Override
	public void afterHasBoxChanged(Boolean hasBox)
	{
		super.afterHasBoxChanged(hasBox);

		setBox(hasBox ? instanciateConfigurable(RollerBox.class, this) : null);
		setVisible(PROPERTYNAME_BOX, hasBox);
	}

	@Override
	public void afterChoiceGuideChanged(ChoiceGuide choiceGuide)
	{
		super.afterChoiceGuideChanged(choiceGuide);

		setGuide();
	}

	private void setGuide()
	{
		ChoiceGuide choiceGuide = getChoiceGuide();

		Guide guide = null;
		if (ChoiceGuide.SLIDER == choiceGuide)
		{
			guide = instanciateConfigurable(RollerSliderGuide.class, this);
			setHasBox(true);
			makeReadOnly(PROPERTYNAME_HASBOX);
		}
		else if (ChoiceGuide.WIRE == choiceGuide)
		{
			guide = instanciateConfigurable(RollerWireGuide.class, this);
			removeReadOnly(PROPERTYNAME_HASBOX);
		}

		setGuide(guide);

		setVisible(PROPERTYNAME_GUIDE, guide != null);
	}

}
