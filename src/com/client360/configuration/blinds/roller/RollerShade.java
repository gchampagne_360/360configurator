// $Id: $

package com.client360.configuration.blinds.roller;

import com.client360.configuration.blinds.roller.colors.RollerColor;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.blinds.Dimension;


@SuppressWarnings("serial")
public class RollerShade extends com.client360.configuration.blinds.roller.RollerShadeBase
{
    // Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
    public RollerShade(com.netappsid.erp.configurator.Configurable parent)
    {
        super(parent);
    }
    
    @Override
    public void setDefaultValues()
    {
    	super.setDefaultValues();
    	
    	makeVisible(PROPERTYNAME_LEFTOFFSET);
    	setDefaultLeftOffset(Units.inch(1));
    	
    	makeVisible(PROPERTYNAME_RIGHTOFFSET);
    	setDefaultRightOffset(Units.inch(1));
    	
    	setDefaultDiameter(Units.inch(1.6d));
    	makeInvisible(PROPERTYNAME_BOTTOMBAR);
		
    	setColor(instanciateConfigurable(RollerColor.class, this));
    }
    
    @Override
    public void activateListener()
    {
    	super.activateListener();
    	
    	publishProperty(PROPERTYNAME_COLOR, "Shade.color");
    }

	@Override
	protected Class<? extends Dimension> getDimensionClass()
	{
		return com.client360.configuration.blinds.Dimension.class;
	}
    
	@Override
	public void afterHasBottomBarChanged(Boolean hasBottomBar)
	{
		super.afterHasBottomBarChanged(hasBottomBar);
		
		setBottomBar(hasBottomBar ? instanciateConfigurable(RollerBottomBar.class, this) : null);
		setVisible(PROPERTYNAME_BOTTOMBAR, hasBottomBar);
	}
}
