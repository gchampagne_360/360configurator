// $Id: $

package com.client360.configuration.blinds.roller;

import com.client360.configuration.blinds.profiles.ProfileStorClip;
import com.client360.configuration.blinds.roller.colors.RollerColor;
import com.netappsid.commonutils.math.Units;


public class RollerSliderGuide extends com.client360.configuration.blinds.roller.RollerSliderGuideBase
{
    // Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
    public RollerSliderGuide(com.netappsid.erp.configurator.Configurable parent)
    {
        super(parent);
    }
    
    @Override
    public void setDefaultValues()
    {
    	super.setDefaultValues();
    	
    	setColor(instanciateConfigurable(RollerColor.class, this));
    	
    	ProfileStorClip profile = new ProfileStorClip();
    	
    	setDefaultLeftThickness(Units.inch(2d));
    	setLeftProfile(profile);
    	makeVisible(PROPERTYNAME_LEFTTHICKNESS);
    	
    	setDefaultRightThickness(Units.inch(2d));
    	setRightProfile(profile);
    	makeVisible(PROPERTYNAME_RIGHTTHICKNESS);
    	
    	setDefaultBottomThickness(Units.inch(2d));
    	makeVisible(PROPERTYNAME_BOTTOMTHICKNESS);
    	setBottomProfile(profile);
    	
    	
    	
    }
}
