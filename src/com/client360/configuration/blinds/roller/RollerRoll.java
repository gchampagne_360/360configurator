// $Id: $

package com.client360.configuration.blinds.roller;

import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.blinds.roller.RollerShade;


public class RollerRoll extends com.client360.configuration.blinds.roller.RollerRollBase
{
    // Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
    public RollerRoll(com.netappsid.erp.configurator.Configurable parent)
    {
        super(parent);
    }
    
    @Override
    public void setDefaultValues()
    {
    	super.setDefaultValues();
    	
    	makeVisible(PROPERTYNAME_AXISY);
    	setDefaultAxisY(Units.inch(1));
    	
    	makeVisible(PROPERTYNAME_LEFTOFFSET);
    	setDefaultLeftOffset(Units.inch(1));
    	
    	makeVisible(PROPERTYNAME_RIGHTOFFSET);
    	setDefaultRightOffset(Units.inch(1));
    	
    	setDefaultDiameter(Units.inch(1.5d));
    }

	@Override
	protected Class<? extends RollerShade> getShadeClass()
	{
		return com.client360.configuration.blinds.roller.RollerShade.class;
	}
    
    // Add custom overrides here
}
