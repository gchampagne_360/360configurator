// $Id: $

package com.client360.configuration.blinds.horizontal;

import com.client360.configuration.blinds.horizontal.colors.HorizontalColor;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.blinds.LadderPosition;
import com.netappsid.configuration.common.Color;


public class HorizontalLadders extends com.client360.configuration.blinds.horizontal.HorizontalLaddersBase
{
    // Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
    public HorizontalLadders(com.netappsid.erp.configurator.Configurable parent)
    {
        super(parent);
    }
    
    @Override
    public void setDefaultValues()
    {
    	super.setDefaultValues();
    	
    	setDefaultThickness(Units.mm(4));

    	setDefaultLastStep(Units.inch(0.25));
    	setDefaultStep(Units.inch(1.25));
    }

	@Override
	protected Class<? extends Color> getColorClass()
	{
		return HorizontalColor.class;
	}

	@Override
	protected Class<? extends LadderPosition> getLadderPositionClass()
	{
		return HorizontalLadderPosition.class;
	}
}
