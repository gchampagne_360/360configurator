// $Id: $

package com.client360.configuration.blinds.horizontal;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.blinds.horizontal.colors.HorizontalColor;
import com.client360.configuration.blinds.profiles.ProfileLame;
import com.netappsid.commonutils.geo.Form;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.blinds.Dimension;
import com.netappsid.configuration.blinds.Ladders;
import com.netappsid.configuration.blinds.horizontalvenitian.HorizontalFinalBlade;
import com.netappsid.configuration.blinds.horizontalvenitian.HorizontalLadders;
import com.netappsid.configuration.common.Color;
import com.netappsid.configuration.common.utils.DimensionChangeListener;

public class HorizontalBlades extends com.client360.configuration.blinds.horizontal.HorizontalBladesBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public HorizontalBlades(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	@Override
	public void activateListener()
	{
		super.activateListener();

		new DimensionChangeListener(this)
			{

				@Override
				protected void dimChanged()
				{
					resetDefaultSteps();
				}
			};

		addAddressedPropertyChangeListener(new PropertyChangeListener()
			{
				@Override
				public void propertyChange(PropertyChangeEvent evt)
				{
					String address = evt.getPropertyName();

					if ((address.startsWith(PROPERTYNAME_LADDERS + ".") && (address.endsWith(Ladders.PROPERTYNAME_LASTSTEP) || address
							.endsWith(Ladders.PROPERTYNAME_STEP)))
							|| address.equals(PROPERTYNAME_HORIZONTALFINALBLADE + "." + HorizontalFinalBlade.PROPERTYNAME_THICKNESS))
					{
						resetDefaultSteps();
					}
				}
			});
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		setDefaultAngle(60d);

		setDefaultLeftOffset(Units.inch(1d));
		setDefaultRightOffset(Units.inch(1d));
		setDefaultTopOffset(Units.inch(1d));

		setDefaultThickness(Units.inch(1.5d));
		
		setDefaultProfile(new ProfileLame());
	}

	private void resetDefaultSteps()
	{
		HorizontalLadders ladders = getLadders();
		HorizontalFinalBlade horizontalFinalBlade = getHorizontalFinalBlade();

		if (ladders == null || horizontalFinalBlade == null)
		{
			return;
		}

		Measure<Length> mStep = ladders.getStep();
		Measure<Length> mLastStep = ladders.getLastStep();
		Form form = getDimension().getForm();
		Measure<Length> mThickness = horizontalFinalBlade.getThickness();

		if (mThickness == null || mStep == null || mLastStep == null || !form.isValid())
		{
			return;
		}

		Double step = mStep.doubleValue(Dimension.DIMENSION_FORM_UNIT);
		Double lastStep = mLastStep.doubleValue(Dimension.DIMENSION_FORM_UNIT);
		Double height = form.getHeight() - mThickness.doubleValue(Dimension.DIMENSION_FORM_UNIT);

		double firstStep = (height - lastStep) % step;

		ladders.setDefaultFirstStep(Measure.valueOf(firstStep, Dimension.DIMENSION_FORM_UNIT));

	}

	@Override
	protected Class<? extends HorizontalFinalBlade> getHorizontalFinalBladeClass()
	{
		return com.client360.configuration.blinds.horizontal.HorizontalFinalBlade.class;
	}

	@Override
	protected Class<? extends HorizontalLadders> getLaddersClass()
	{
		return com.client360.configuration.blinds.horizontal.HorizontalLadders.class;
	}

	@Override
	protected Class<? extends Dimension> getDimensionClass()
	{
		return com.client360.configuration.blinds.Dimension.class;
	}

	@Override
	protected Class<? extends Color> getColorsClass()
	{
		return HorizontalColor.class;
	}
}
