// $Id: $

package com.client360.configuration.blinds.horizontal;

import com.client360.configuration.blinds.horizontal.colors.HorizontalColor;
import com.client360.configuration.blinds.profiles.ProfileBoitier;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.blinds.Dimension;
import com.netappsid.configuration.common.Color;

public class HorizontalBox extends com.client360.configuration.blinds.horizontal.HorizontalBoxBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public HorizontalBox(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	@Override
	protected Class<? extends Dimension> getDimensionClass()
	{
		return Dimension.class;
	}

	@Override
	protected Class<? extends Color> getColorClass()
	{
		return HorizontalColor.class;
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		setDefaultHeight(Units.inch(2.5));

		setDefaultLeftOverflow(Units.inch(1));
		setDefaultRightOverflow(Units.inch(1));
		
		setDefaultProfile(new ProfileBoitier());
	}
	
	
}
