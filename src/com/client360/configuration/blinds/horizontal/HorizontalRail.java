// $Id: $

package com.client360.configuration.blinds.horizontal;

import com.client360.configuration.blinds.horizontal.colors.HorizontalColor;
import com.client360.configuration.blinds.profiles.ProfileRail;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.common.Color;


public class HorizontalRail extends com.client360.configuration.blinds.horizontal.HorizontalRailBase
{
    // Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
    public HorizontalRail(com.netappsid.erp.configurator.Configurable parent)
    {
        super(parent);
    }
    
    @Override
    protected Class<? extends Color> getColorClass()
    {
    	return HorizontalColor.class;
    }
    
    @Override
    public void setDefaultValues()
    {
    	super.setDefaultValues();
    	
    	setDefaultHeight(Units.inch(1d));
		
		setDefaultProfile(new ProfileRail());
    }
}
