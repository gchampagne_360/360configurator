// $Id: $

package com.client360.configuration.blinds.horizontal.colors;

import java.awt.image.BufferedImage;

import com.netappsid.rendering.common.advanced.Filling;

public class HorizontalColor extends com.client360.configuration.blinds.horizontal.colors.HorizontalColorBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public HorizontalColor(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
		makeDisabled(PROPERTYNAME_HORIZONTALCHOICECOLORTYPE);
	}

	@Override
	public void beforeHorizontalChoiceColorChanged(HorizontalChoiceColor horizontalChoiceColor)
	{
		super.beforeHorizontalChoiceColorChanged(horizontalChoiceColor);
		setColors(horizontalChoiceColor);
	}

	/**
	 * Initialize the color chart attributes according to the selected standard color choice
	 * 
	 * @param EnrDynChoixCouleur
	 *            - the selected standard color choice
	 */
	private void setColors(HorizontalChoiceColor horizontalChoiceColor)
	{
		if (horizontalChoiceColor != null)
		{
			Integer red = horizontalChoiceColor.getRed();
			Integer green = horizontalChoiceColor.getGreen();
			Integer blue = horizontalChoiceColor.getBlue();
			String texturePath = horizontalChoiceColor.getTexture();
			if (red != null && green != null && blue != null)
			{
				setRed(red);
				setGreen(green);
				setBlue(blue);
			}
			if (texturePath != null)
			{
				setTexturePath(texturePath.replace("/com/mac/commun/store/images", "/com/client360/configuration/blinds/images"));
			}
			return;
		}

		setRed(255);
		setGreen(255);
		setBlue(255);
	}

	@Override
	public BufferedImage getTextureImage()
	{
		String texturePath = getTexturePath();
		if (texturePath != null)
		{
			return Filling.loadTextureImage(texturePath, this.getClass());
		}
		return null;
	}

	@Override
	public String toString()
	{
		HorizontalChoiceColor horizontalChoiceColor = getHorizontalChoiceColor();
		if (horizontalChoiceColor != null)
		{
			return horizontalChoiceColor.getCode();
		}
		else
		{
			return null;
		}
	}
}
