package com.client360.configuration.blinds.horizontal.modelListeners;

import java.util.List;

import com.client360.configuration.blinds.horizontal.Horizontal;
import com.netappsid.configuration.blinds.horizontalvenitian.modelListeners.ModelListenerHorizontalVenitian;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.erp.configurator.renderers.RenderingComponent;

public class ModelListenerHorizontal extends ModelListenerHorizontalVenitian
{
	public ModelListenerHorizontal(Horizontal configurable)
	{
		super(configurable);
	}

	public ModelListenerHorizontal(Configurable configurable, List<RenderingComponent> renderingComponents)
	{
		super(configurable, renderingComponents);
	}
}
