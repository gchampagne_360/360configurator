// $Id: $

package com.client360.configuration.blinds.horizontal;

import com.client360.configuration.blinds.enums.ChoiceGuide;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.blinds.Dimension;
import com.netappsid.configuration.blinds.Guide;
import com.netappsid.configuration.blinds.horizontalvenitian.HorizontalBlades;
import com.netappsid.configuration.blinds.horizontalvenitian.HorizontalRail;

public class Horizontal extends com.client360.configuration.blinds.horizontal.HorizontalBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public Horizontal(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		makeInvisible(PROPERTYNAME_BOX);
		getDimension().setDefaultWidth(Units.mm(1000d));
		getDimension().setDefaultHeight(Units.mm(1000d));
	}

	@Override
	protected Class<? extends HorizontalBlades> getHorizontalBladesClass()
	{
		return com.client360.configuration.blinds.horizontal.HorizontalBlades.class;
	}

	@Override
	protected Class<? extends HorizontalRail> getRailClass()
	{
		return com.client360.configuration.blinds.horizontal.HorizontalRail.class;
	}

	@Override
	protected Class<? extends com.netappsid.configuration.common.Dimension> getDimensionClass()
	{
		return Dimension.class;
	}

	@Override
	public void afterHasBoxChanged(Boolean hasBox)
	{
		super.afterHasBoxChanged(hasBox);

		setBox(hasBox ? instanciateConfigurable(HorizontalBox.class, this) : null);
		setVisible(PROPERTYNAME_BOX, hasBox);
	}

	@Override
	public void afterChoiceGuideChanged(ChoiceGuide choiceGuide)
	{
		super.afterChoiceGuideChanged(choiceGuide);

		setGuide();
	}

	private void setGuide()
	{
		ChoiceGuide choiceGuide = getChoiceGuide();

		Guide guide = null;
		if (ChoiceGuide.SLIDER == choiceGuide)
		{
			guide = instanciateConfigurable(HorizontalSliderGuide.class, this);
			setHasBox(true);
			makeReadOnly(PROPERTYNAME_HASBOX);
		}
		else if (ChoiceGuide.WIRE == choiceGuide)
		{
			guide = instanciateConfigurable(HorizontalWireGuide.class, this);
			removeReadOnly(PROPERTYNAME_HASBOX);
		}

		setGuide(guide);

		setVisible(PROPERTYNAME_GUIDE, guide != null);
	}
}
