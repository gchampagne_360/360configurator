// $Id: $

package com.client360.configuration.blinds.horizontal;

import com.client360.configuration.blinds.horizontal.colors.HorizontalColor;
import com.client360.configuration.blinds.profiles.ProfileBoitier;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.common.Color;


public class HorizontalFinalBlade extends com.client360.configuration.blinds.horizontal.HorizontalFinalBladeBase
{
    // Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
    public HorizontalFinalBlade(com.netappsid.erp.configurator.Configurable parent)
    {
        super(parent);
    }

	@Override
	protected Class<? extends Color> getColorClass()
	{
		return HorizontalColor.class;
	}
	
	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
		
		setDefaultLeftOffset(Units.inch(-0.5d));
		setDefaultRightOffset(Units.inch(-0.5d));
		
		setDefaultThickness(Units.inch(1.5d));
		
		setDefaultProfile(new ProfileBoitier());
	}
    
}
