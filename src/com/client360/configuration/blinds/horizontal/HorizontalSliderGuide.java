// $Id: $

package com.client360.configuration.blinds.horizontal;

import com.client360.configuration.blinds.horizontal.colors.HorizontalColor;
import com.client360.configuration.blinds.profiles.ProfileStorClip;
import com.netappsid.commonutils.math.Units;


public class HorizontalSliderGuide extends com.client360.configuration.blinds.horizontal.HorizontalSliderGuideBase
{
    // Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
    public HorizontalSliderGuide(com.netappsid.erp.configurator.Configurable parent)
    {
        super(parent);
    }
    
    @Override
    public void setDefaultValues()
    {
    	super.setDefaultValues();
    	
    	setColor(instanciateConfigurable(HorizontalColor.class, this));
    	
    	ProfileStorClip profile = new ProfileStorClip();
    	
    	setDefaultLeftThickness(Units.inch(2d));
    	setLeftProfile(profile);
    	makeVisible(PROPERTYNAME_LEFTTHICKNESS);
    	
    	setDefaultRightThickness(Units.inch(2d));
    	setRightProfile(profile);
    	makeVisible(PROPERTYNAME_RIGHTTHICKNESS);
    	
    	setDefaultBottomThickness(Units.inch(2d));
    	makeVisible(PROPERTYNAME_BOTTOMTHICKNESS);
    	setBottomProfile(profile);
    	
    	
    	
    }
}
