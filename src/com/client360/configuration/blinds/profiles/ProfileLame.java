package com.client360.configuration.blinds.profiles;

import com.netappsid.rendering.common.advanced.Profile;

@SuppressWarnings("serial")
public class ProfileLame extends Profile
{
	@Override
	public void load()
	{
		addGradientSegment(10, 40, 0);
		addGradientSegment(10, 0, -40);
	}
}