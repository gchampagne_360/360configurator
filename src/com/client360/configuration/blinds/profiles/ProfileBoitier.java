package com.client360.configuration.blinds.profiles;

import com.netappsid.rendering.common.advanced.Profile;

@SuppressWarnings("serial")
public class ProfileBoitier extends Profile
{
	@Override
	public void load()
	{
		addGradientSegment(10, 80, 0);
		addGradientSegment(40, 0);
		addGradientSegment(10, 0, -80);
	}
}