package com.client360.configuration.blinds.profiles;

import com.netappsid.rendering.common.advanced.Profile;

@SuppressWarnings("serial")
public class ProfileStorClip extends Profile
{
	@Override
	public void load()
	{
		addGradientSegment(2, -40, 0);
		addLine(-20);
		addSpace(3);
	}
}