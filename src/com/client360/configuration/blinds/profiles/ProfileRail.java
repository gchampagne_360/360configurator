package com.client360.configuration.blinds.profiles;

import com.netappsid.rendering.common.advanced.Profile;

@SuppressWarnings("serial")
public class ProfileRail extends Profile
{
	@Override
	public void load()
	{
		addSpace(1.5);
		addLine(-50);
		addGradientSegment(2, -20, -10);
		addLine(-50);
		addGradientSegment(3, 10, 0);
	}
}