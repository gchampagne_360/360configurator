package com.client360.configuration.blinds.constant;

import javax.measure.quantities.Length;

import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.common.productdata.ProductDataManagement;

public class ProductData extends ProductDataManagement
{
	@Override
	public void load()
	{
		// --------------------------------------------------------------
		// ENROULEUR
		// --------------------------------------------------------------
		// addValue(ProductType.ENROULEUR, Data.DEDUCTION_BDC_11x23, 0.0);
		// D�bit coulisses
		addValue(ProductType.ENROULEUR, Data.ENR_COUL_DEDUCTION_SMALL, 60d);
		addValue(ProductType.ENROULEUR, Data.ENR_COUL_DEDUCTION_MEDIUM, 80d);
		addValue(ProductType.ENROULEUR, Data.ENR_COUL_DEDUCTION_LARGE, 120d);
		// D�bit des autres �l�ments lorsqu'il y a des coulisses
		addValue(ProductType.ENROULEUR, Data.ENR_COUL_DEDUCTION_SMALL_GUIDE, 100d);
		addValue(ProductType.ENROULEUR, Data.ENR_COUL_DEDUCTION_MEDIUM_GUIDE, 100d);
		addValue(ProductType.ENROULEUR, Data.ENR_COUL_DEDUCTION_LARGE_GUIDE, 152d);
		// D�bit profil arri�re
		addValue(ProductType.ENROULEUR, Data.ENR_PROFIL_AR_DEDUCTION_SMALL, 9d);
		addValue(ProductType.ENROULEUR, Data.ENR_PROFIL_AR_DEDUCTION_MEDIUM, 9d);
		addValue(ProductType.ENROULEUR, Data.ENR_PROFIL_AR_DEDUCTION_LARGE, 14d);
		// D�bit profil avant
		addValue(ProductType.ENROULEUR, Data.ENR_PROFIL_AV_DEDUCTION_SMALL, 33d);
		addValue(ProductType.ENROULEUR, Data.ENR_PROFIL_AV_DEDUCTION_MEDIUM, 33d);
		addValue(ProductType.ENROULEUR, Data.ENR_PROFIL_AV_DEDUCTION_LARGE, 29d);
		addValue(ProductType.ENROULEUR, Data.ENR_PROFIL_AV_DEDUCTION_LARGE_MAN_230V, 14d);
		// D�bit cache facial
		addValue(ProductType.ENROULEUR, Data.ENR_PROFIL_CF_DEDUCTION_SMALL, -2d);
		addValue(ProductType.ENROULEUR, Data.ENR_PROFIL_CF_DEDUCTION_MEDIUM, -2d);
		addValue(ProductType.ENROULEUR, Data.ENR_PROFIL_CF_DEDUCTION_LARGE, 0d);
		// D�bit profil r�ception
		addValue(ProductType.ENROULEUR, Data.ENR_PROFIL_RC_DEDUCTION_LARGE, 140d);
		// D�bit suppl�mentaire pour tissus
		addValue(ProductType.ENROULEUR, Data.ENR_TISSU_DEDUCTION_SCREEN, 4d);
		// D�duction suppl�mentaire � appliquer lorsqu'il y a un guidage
		addValue(ProductType.ENROULEUR, Data.ENR_CABLE_DEDUCTION_SUPPL, 4d);
		// D�duction suppl�mentaire bdc dans fourreau
		addValue(ProductType.ENROULEUR, Data.ENR_BDC_DEDUCTION_SUPPL_10, 16d);
		addValue(ProductType.ENROULEUR, Data.ENR_BDC_DEDUCTION_SUPPL_15, 4d);

		// D�bit sp�cifique au STORCLIP
		addValue(ProductType.ENROULEUR, Data.ENR_PROFIL_RC_DEDUCTION_STORCLIP, 28d);
		addValue(ProductType.ENROULEUR, Data.ENR_COUL_DEDUCTION_STORCLIP, 55d);
		// Infos de per�ages pour STORCLIP
		addValue(ProductType.ENROULEUR, Data.ENR_PERCAGE_BAS_STORCLIP, 64d);
		addValue(ProductType.ENROULEUR, Data.ENR_PERCAGE_HAUT_STORCLIP, 38d);

		// D�bit sp�cifique au MULTISTOP
		addValue(ProductType.ENROULEUR, Data.ENR_PROFIL_RC_DEDUCTION_MULTISTOP, 35d);
		addValue(ProductType.ENROULEUR, Data.ENR_PROFIL_AV_DEDUCTION_MULTISTOP, 7d);

		// --------------------------------------------------------------
		// V�NITIEN HORIZONTAL
		// --------------------------------------------------------------
		addValue(ProductType.VENITIEN_HORIZONTAL, Data.VEN_STORCLIP_DEDUCTION_LAMES_GD, 11d);
		addValue(ProductType.VENITIEN_HORIZONTAL, Data.VEN_STORCLIP_DEDUCTION_LAMES_GD_TOIT, 10d);
		addValue(ProductType.VENITIEN_HORIZONTAL, Data.VEN_STORCLIP_DEDUCTION_LAMES_HB, 5d);
		addValue(ProductType.VENITIEN_HORIZONTAL, Data.VEN_STORCLIP_LARGEUR_PROFIL, 38d);
		addValue(ProductType.VENITIEN_HORIZONTAL, Data.VEN_STORCLIP_LARGEUR_PROFIL_DEBORD, 25d);
		addValue(ProductType.VENITIEN_HORIZONTAL, Data.VEN_LAMEFINALE_DEDUCTION_GUIDE_BLOQUEUR, 10d);
		addValue(ProductType.VENITIEN_HORIZONTAL, Data.VEN_LAMEFINALE_DEDUCTION_GUIDE_CABLE, 30d);

		addValue(ProductType.VENITIEN_HORIZONTAL, Data.VEN_BSO_DEDUCTION_LAMES_GD, 56d);
		addValue(ProductType.VENITIEN_HORIZONTAL, Data.VEN_BSO_DEDUCTION_LAME_FINALE, 4d);
	}

	@Override
	public javax.measure.units.Unit<Length> getUnit()
	{
		return Units.MM;
	}
}