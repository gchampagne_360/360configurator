// $Id: $

package com.client360.configuration.hingeshutters;

import java.util.List;

import com.netappsid.configuration.hingeshutters.ShutterBoard;


public class Provencal extends com.client360.configuration.hingeshutters.ProvencalBase
{
	private static final long serialVersionUID = -4044600005912856449L;
	private transient boolean drawInsideBoards;

    // Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
    public Provencal(com.netappsid.erp.configurator.Configurable parent)
    {
        super(parent);
    }
    
    // Add custom overrides here

	/**
	 * Build the boards for a provencal pattern
	 * 
	 * @return Boards
	 */
	@Override
	protected List<ShutterBoard> createBoards()
	{

		drawInsideBoards = false;

		// Draw the vertical boards
		setBoardHorizontal(false);
		List<ShutterBoard> result = super.createBoards();
		drawInsideBoards = true;

		// Draw the horizontal boards
		setBoardHorizontal(true);
		result.addAll(addBoards());

		return result;
	}

	/**
	 * Get the z order of the boards
	 * 
	 * @return Z order
	 */
	@Override
	protected int getBoardZIndex()
	{
		return super.getBoardZIndex() + (drawInsideBoards ? 1 : 0);
	}
}
