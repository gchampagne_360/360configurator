// $Id: $

package com.client360.configuration.hingeshutters;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.regex.Pattern;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.StandardColor;
import com.netappsid.commonutils.geo.ShapeFunction;
import com.netappsid.commonutils.math.MathFunction;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.common.Dimension;
import com.netappsid.configuration.common.ZoneVisualComposition;
import com.netappsid.configuration.common.utils.VisualCompositionUtils.ImageHorizontalAlignement;
import com.netappsid.configuration.common.utils.VisualCompositionUtils.ImageVerticalAlignement;
import com.netappsid.configuration.hingeshutters.Astragal;
import com.netappsid.configuration.hingeshutters.Panel;
import com.netappsid.configuration.hingeshutters.enums.ChoiceSide;


public class Panels extends com.client360.configuration.hingeshutters.PanelsBase
{
	private static final long serialVersionUID = -2137534605030239088L;
	private static final int NB_HINGES = 2;

	private static final String STAY_EDGE_FILENAME = "/images/hingeshutter/espagnoletteedge.png";
	private static final String STAY_HANDLE = "/images/hingeshutter/espagnolettehandle.png";
	private static final String STAY_HANDLE_REST = "/images/hingeshutter/espagnolettehandlerest.png";
	private static final String STAY_POLE_FILENAME = "/images/hingeshutter/espagnolettepole.png";
	private static final String PINTLEHINGE_FILENAME = "/images/hingeshutter/pintlehinge.png";
	// private static final String PINTLEHINGEBACK_FILENAME = "/images/hingeshutter/pintlehingeback.png";
	private static final String HINGE_FILENAME = "/images/hingeshutter/hinge2.png";
	
	private static final String PANEL_CHOICESIDE_REGEX = String.format("panel%s[0-9]+%s.%s", Pattern.quote("["), Pattern.quote("]"),
			Panel.PROPERTYNAME_CHOICESIDE);
	
	private final int FRONT_COMPOSITIONS_ZLAYER = com.netappsid.configuration.hingeshutters.Panel.MODELS_ZLAYER - 50;
	private final int BACK_COMPOSITIONS_ZLAYER = com.netappsid.configuration.hingeshutters.Panel.MODELS_ZLAYER + 50;
	
	private final int STAY_ZLAYER = BoardModel.DEFAULT_BOARD_Z_INDEX + 4;
	private final int STAY_HANDLE_ZLAYER = STAY_ZLAYER + 1;
	private final int STAY_POLE_ZLAYER = STAY_ZLAYER - 1;

    // Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public Panels(com.netappsid.erp.configurator.Configurable parent)
    {
        super(parent);
    }
    
	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
	}

	@Override
	public void activateListener()
	{
		super.activateListener();
		
		addAddressedPropertyChangeListener(new PropertyChangeListener()
			{
				@Override
				public void propertyChange(PropertyChangeEvent evt)
				{
					String propertyName = evt.getPropertyName();
					if (propertyName.equals(PROPERTYNAME_OFFSETFROMPRODUCT))
					{
						propagatePanelsWidth();
					}
					else if (propertyName.equals(PROPERTYNAME_DISPLAYSTAY))
					{
						addStayImage();
					}
					else if (propertyName.equals(PROPERTYNAME_DISPLAYHINGES))
					{
						addHingesImages();
					}
					else if (propertyName.matches(PANEL_CHOICESIDE_REGEX))
					{
						addStayImage();
						addHingesImages();
					}
				}
			});
	}

	// Add custom overrides here

	// -----==== Public Methods Override ====-----
	@Override
	protected Class<? extends Panel> getPanelClass()
	{
		return com.client360.configuration.hingeshutters.Panel.class;
	}

	@Override
	protected Class<? extends Astragal> getAstragalClass()
	{
		return com.client360.configuration.hingeshutters.Astragal.class;
	}

	/**
	 * Manage the replication of the panels
	 */
	@Override
	public void setupPanelsReplication()
	{
		super.setupPanelsReplication();

		if (getPanel() == null || getPanel().length == 0)
		{
			return;
		}

		Panel[] panel = getPanel();

		int iPanel = 0;
		for (Panel p : panel)
		{
			if (iPanel == 0)
			{
				p.publishProperty(Panel.PROPERTYNAME_GAPTOHELD, "HingeShutterPanel." + Panel.PROPERTYNAME_GAPTOHELD);
			}
			else
			{
				p.subscribe(Panel.PROPERTYNAME_GAPTOHELD, "HingeShutterPanel." + Panel.PROPERTYNAME_GAPTOHELD);
			}

			if (p.getPanelModel() != null)
			{
				p.getPanelModel().managePropertiesReplication(iPanel == 0, null);
			}

			iPanel++;
		}
	}

	/**
	 * Set the offset from product
	 */
	@Override
	public void beforeOffsetFromProductChanged(Measure<Length> offset)
	{
		super.beforeOffsetFromProductChanged(offset);
		if (MathFunction.equals(getSafeMeasure(offset).doubleValue(Units.MM), 0d, ShapeFunction.DELTA))
		{
			removeDefaultOffset();
		}
		else
		{
			refreshOffset(offset);
		}
		refreshForm();
	}

	@Override
	public Unit<?> getUnit()
	{
		return Units.MM;
	}

	// -----==== Protected Methods Override ====-----
	@Override
	protected Class<? extends com.netappsid.configuration.common.Color> getInteriorColorClass()
	{
		return StandardColor.class;
	}

	@Override
	protected Class<? extends com.netappsid.configuration.common.Color> getExteriorColorClass()
	{
		return StandardColor.class;
	}

	@Override
	protected Class<? extends Dimension> getDimensionClass()
	{
		return com.client360.configuration.wad.Dimension.class;
	}

	// -----==== Private methods ====-----
	/**
	 * Set the offset spacer according to the offset chosen by the user. For the concept proof, the values of the offsets will be only applied on the sides. In
	 * a client-side implementation, those values can be contained in an enumeration
	 * 
	 * @param offset
	 *            Offset to apply to each edge of the shape
	 */
	private void refreshOffset(Measure<Length> offset)
	{
		@SuppressWarnings("unchecked")
		Measure<Length>[] offsets = new Measure[getParent(com.netappsid.configuration.hingeshutters.HingeShutter.class).getDimension().getForm().getNbEdges()];

		for (int i = 0; i < offsets.length; ++i)
		{
			offsets[i] = offset;
		}

		setDefaultOffset(offsets);
	}

	/**
	 * Display the hinges in the hinge visual composition
	 */
	private void addHingesImages()
	{
		if (getPanel() == null || getPanel().length == 0)
		{
			return;
		}

		for (com.netappsid.configuration.hingeshutters.Panel p : getPanel())
		{
			boolean displayHinges = getSafeBoolean(getDisplayHinges());

			p.setHingesDisplay(null);

			if (displayHinges)
			{
				if (p.getHingesDisplay() == null)
				{
					p.setHingesDisplay(new ZoneVisualComposition(p));
				}
				// Check if the hinge to pintle image should be displayed
				if (panelOnPintle(p))
				{
					boolean left = p.getChoiceSide().equals(ChoiceSide.LEFT);

					p.getHingesDisplay().addImage(PINTLEHINGE_FILENAME, Units.mm(500d), Units.mm(50d), left ? 0d : 1d, left ? 1d : 0d, 0d, 1d,
							left ? ImageHorizontalAlignement.LEFT : ImageHorizontalAlignement.RIGHT, Units.mm(-15d), ImageVerticalAlignement.TOP,
							Units.mm(200), FRONT_COMPOSITIONS_ZLAYER);

					p.getHingesDisplay().addImage(PINTLEHINGE_FILENAME, Units.mm(500d), Units.mm(50d), left ? 0d : 1d, left ? 1d : 0d, 0d, 1d,
							left ? ImageHorizontalAlignement.LEFT : ImageHorizontalAlignement.RIGHT, Units.mm(-15d), ImageVerticalAlignement.BOTTOM,
							Units.mm(230), FRONT_COMPOSITIONS_ZLAYER);

					// Display the inside decorative pintle hinges
//					p.getHingesDisplay().addImage(PINTLEHINGEBACK_FILENAME, Units.mm(500d), Units.mm(50d), left ? 0d : 1d, left ? 1d : 0d, 0d, 1d,
//							left ? ImageHorizontalAlignement.LEFT : ImageHorizontalAlignement.RIGHT, Units.mm(-15d), ImageVerticalAlignement.TOP,
//							Units.mm(200), BACK_COMPOSITIONS_ZLAYER);

//					p.getHingesDisplay().addImage(PINTLEHINGEBACK_FILENAME, Units.mm(500d), Units.mm(50d), left ? 0d : 1d, left ? 1d : 0d, 0d, 1d,
//							left ? ImageHorizontalAlignement.LEFT : ImageHorizontalAlignement.RIGHT, Units.mm(-15d), ImageVerticalAlignement.BOTTOM,
//							Units.mm(230), BACK_COMPOSITIONS_ZLAYER);
				}
				else
				{
					// Display the hinges between two panels
					double height = getDimension().getHeight().doubleValue(Units.MM) / (NB_HINGES + 1);
					for (int i = 0; i < NB_HINGES; ++i)
					{
						p.getHingesDisplay().addImage(HINGE_FILENAME, Units.mm(25d), Units.mm(100d), 0d, 1d, 0d, 1d, ImageHorizontalAlignement.CENTER,
								Units.ZEROMM, ImageVerticalAlignement.TOP, Units.mm(height * (i + 1)), BACK_COMPOSITIONS_ZLAYER);
					}
				}
			}
		}
	}

	/**
	 * Display the stay
	 */
	private void addStayImage()
	{
		// Check if the panels' collection is valid
		if (getPanel() == null)
		{
			return;
		}
		
		for (com.netappsid.configuration.hingeshutters.Panel p : getPanel())
		{
			boolean displayStay = getSafeBoolean(getDisplayStay());

			p.setStayDisplay(null);

			// Check if the user wants to display the stay
			if (displayStay)
			{
				// Check if this is a center panel
				for (Integer iPanel : getCenterPanelsIndexes())
				{
					if (iPanel.equals(p.getIndex()) && p.getChoiceSide() != null)
					{
						if (p.getStayDisplay() == null)
						{
							p.setStayDisplay(new ZoneVisualComposition(p));
						}
						addStayImage((Panel) p);
					}
				}
			}
		}
	}

	/**
	 * Add the images for the stay
	 * 
	 * @param p
	 *            Panel where to add the stay
	 */
	private void addStayImage(Panel p)
	{
		boolean left = p.getChoiceSide().equals(ChoiceSide.LEFT);

		// Check if this is the left or lone panel
		if (left || getCenterPanelsIndexes().size() == 1)
		{
			// Add pole stay
			p.getStayDisplay().addImage(STAY_POLE_FILENAME, Units.mm(60d), Units.inch(p.getPanelModelForm().getHeight()), (left ? 0d : 1d), (left ? 1d : 0d),
					0d, 1d, (left ? ImageHorizontalAlignement.RIGHT : ImageHorizontalAlignement.LEFT), Units.ZEROIN, ImageVerticalAlignement.CENTER,
					Units.ZEROIN, STAY_POLE_ZLAYER);

			// Add top stay
			p.getStayDisplay().addImage(STAY_EDGE_FILENAME, Units.mm(125d), Units.mm(100d), (left ? 0d : 1d), (left ? 1d : 0d), 0d, 1d,
					(left ? ImageHorizontalAlignement.RIGHT : ImageHorizontalAlignement.LEFT), Units.ZEROIN, ImageVerticalAlignement.TOP, Units.ZEROIN,
					STAY_ZLAYER);

			// Add bottom stay
			p.getStayDisplay().addImage(STAY_EDGE_FILENAME, Units.mm(125d), Units.mm(100d), (left ? 0d : 1d), (left ? 1d : 0d), 1d, 0d,
					(left ? ImageHorizontalAlignement.RIGHT : ImageHorizontalAlignement.LEFT), Units.ZEROIN, ImageVerticalAlignement.BOTTOM, Units.ZEROIN,
					STAY_ZLAYER);

			// Add handle stay
			p.getStayDisplay().addImage(STAY_HANDLE, Units.mm(200d), Units.mm(100d), (left ? 0d : 1d), (left ? 1d : 0d), 0d, 1d,
					(left ? ImageHorizontalAlignement.RIGHT : ImageHorizontalAlignement.LEFT), Units.mm(10d), ImageVerticalAlignement.BOTTOM,
					Units.mm(getDimension().getHeight().doubleValue(Units.MM) * 0.3d), STAY_HANDLE_ZLAYER);
		}
		else
		{
			// Add handle rest
			p.getStayDisplay().addImage(STAY_HANDLE_REST, Units.mm(50d), Units.mm(100d), (left ? 0d : 1d), (left ? 1d : 0d), 0d, 1d,
					(left ? ImageHorizontalAlignement.RIGHT : ImageHorizontalAlignement.LEFT), Units.mm(50d), ImageVerticalAlignement.BOTTOM,
					Units.mm(getDimension().getHeight().doubleValue(Units.MM) * 0.3d), STAY_HANDLE_ZLAYER);
		}
	}

	/**
	 * Check if the panel contains a model
	 * 
	 * @param p
	 *            Panel to explore
	 * @param clazz
	 *            Model to find
	 * @return Model has been found
	 */
	private boolean panelContainsModel(Panel p, Class<? extends PanelModel> clazz)
	{
		boolean result = false;
		if (p.getPanelModel() != null)
		{
			return ((PanelModel) p.getPanelModel()).containsModel(clazz);
		}
		return result;
	}
}
