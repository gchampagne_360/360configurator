// $Id: $

package com.client360.configuration.hingeshutters;

import java.util.ArrayList;
import java.util.List;

import com.netappsid.commonutils.geo.Form;
import com.netappsid.commonutils.geo.ShapeFunction;
import com.netappsid.commonutils.geo.enums.Orientation;
import com.netappsid.commonutils.math.MathFunction;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.hingeshutters.ShutterBoard;

public class Louvered extends com.client360.configuration.hingeshutters.LouveredBase
{
	private static final long serialVersionUID = 8899492512392222379L;
	private static final String LOUVERED_REPLICATION_PREFIX = "Louvered";

	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
    public Louvered(com.netappsid.erp.configurator.Configurable parent)
    {
        super(parent);
    }
    
	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		setDefaultBoardHorizontal(true);
	}

    // Add custom overrides here

	/**
	 * Manage the replication of the louvered panel
	 * 
	 * @param publish
	 *            Indicate if the properties have to be published (false = listened)
	 * @param prefix
	 *            Prefix to add to the replication name
	 */
	@Override
	public void managePropertiesReplication(boolean publish, String prefix)
	{
		super.managePropertiesReplication(publish, prefix);

		String propertyPrefix = LOUVERED_REPLICATION_PREFIX + prefix == null ? "" : prefix;

		managePropertyReplication(publish, PROPERTYNAME_MULLION, propertyPrefix);
		managePropertyReplication(publish, PROPERTYNAME_MULLIONWIDTH, propertyPrefix);
	}

	/**
	 * Create the boards of the louvered panel
	 * 
	 * @return Boards
	 */
	@Override
	protected List<ShutterBoard> createBoards()
	{
		List<ShutterBoard> result = super.createBoards();

		// Check if a mullion has been selected
		if (getSafeBoolean(getMullion()))
		{
			ShutterBoard mullion = createMullion();

			// Check if the mullion is valid
			if (mullion != null)
			{
				result.add(mullion);
			}
		}
		return result;
	}

	/**
	 * Get the mullion z index
	 * 
	 * @return Z index
	 */
	protected int getMullionZIndex()
	{
		return getBoardZIndex();
	}

	/**
	 * Create the mullion
	 * 
	 * @return Mullion board (null if invalid)
	 */
	private ShutterBoard createMullion()
	{
		ShutterBoard result = null;

		double mullionWidth = getSafeMeasure(getMullionWidth()).doubleValue(Units.INCH);
		Form f = getFormOffset();
		// Check if the size of the mullion is valid

		if (f != null && MathFunction.greater(mullionWidth, 0d, ShapeFunction.DELTA))
		{
			double minX = f.getOrigin().getX();
			double width = f.getWidth() * 0.5d;
			List<Double> cuts = new ArrayList<Double>();
			cuts.add(width - mullionWidth * 0.5d + minX);
			cuts.add(width + mullionWidth * 0.5d + minX);
			
			Form mullion = split(f, Orientation.VERTICAL, cuts).get(1);

			result = new ShutterBoard(mullion, getPerimeterProfileInstance(), getPerimeterColor(), getMullionZIndex());
		}
		return result;
	}
}
