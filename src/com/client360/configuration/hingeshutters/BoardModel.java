// $Id: $

package com.client360.configuration.hingeshutters;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import com.client360.configuration.hingeshutters.enums.HingeShutterPanelProfiles;
import com.client360.configuration.wad.StandardColor;
import com.netappsid.commonutils.geo.Form;
import com.netappsid.commonutils.geo.ShapeFunction;
import com.netappsid.commonutils.geo.enums.Orientation;
import com.netappsid.commonutils.math.MathFunction;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.hingeshutters.ShutterBoard;
import com.netappsid.rendering.common.advanced.Profile;


public class BoardModel extends com.client360.configuration.hingeshutters.BoardModelBase
{
	public static final int DEFAULT_BOARD_Z_INDEX = DEFAULT_PERIMETER_Z_INDEX - 1;

	private static final long serialVersionUID = 8978973068399934262L;
	private static final String BOARD_MODEL_REPLICATION_PREFIX = "BoardModel";

	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
    public BoardModel(com.netappsid.erp.configurator.Configurable parent)
    {
        super(parent);
    }
    
	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		setBoardColor(new StandardColor(this));

		makeReadOnly(PROPERTYNAME_BOARDCOLOR);
		makeInvisible(PROPERTYNAME_BOARDHORIZONTAL);
	}

	@Override
	public void activateListener()
	{
		super.activateListener();

		addAddressedPropertyChangeListener(new PropertyChangeListener()
			{

				@Override
				public void propertyChange(PropertyChangeEvent evt)
				{
					String propertyName = evt.getPropertyName();
					if (propertyName.equals(PROPERTYNAME_BOARDPROFILE))
					{
						onBoardProfileChanged();
					}
				}
			});
	}

	// Add custom overrides here

	public boolean isZefiralBoardWidthVisible()
	{
		return getBoardProfile() != null && getBoardProfile().equals(HingeShutterPanelProfiles.ALUMINIUM);
	}

	/**
	 * Manage the replication of the properties of the board model
	 */
	@Override
	public void managePropertiesReplication(boolean publish, String prefix)
	{
		super.managePropertiesReplication(publish, prefix);
		
		String propertyPrefix = BOARD_MODEL_REPLICATION_PREFIX + (prefix == null ? "" : prefix);
		
		managePropertyReplication(publish, PROPERTYNAME_BOARDHORIZONTAL, propertyPrefix);
		managePropertyReplication(publish, PROPERTYNAME_BOARDWIDTH, propertyPrefix);
		managePropertyReplication(publish, PROPERTYNAME_BOARDSPACING, propertyPrefix);
		managePropertyReplication(publish, PROPERTYNAME_BOARDPROFILE, propertyPrefix);
		managePropertyReplication(publish, PROPERTYNAME_BOARDCOLOR, propertyPrefix);
		managePropertyReplication(publish, PROPERTYNAME_ZEFIRALBOARDWIDTH, propertyPrefix);
	}

	/**
	 * Create the boards of the panel section
	 * 
	 * @return Collection of boards
	 */
	@Override
	protected List<ShutterBoard> createBoards()
	{
		List<ShutterBoard> result = super.createBoards();
		result.addAll(addBoards());
		return result;
	}

	/**
	 * Get the wanted Z index for the boards
	 * 
	 * @return Board Z index
	 */
	protected int getBoardZIndex()
	{
		return DEFAULT_BOARD_Z_INDEX;
	}

	/**
	 * Create an instance of the board profile according to the choice of the user
	 * 
	 * @return Board profile
	 */
	protected Profile getBoardProfileInstance()
	{
		Profile result = null;

		if (getBoardProfile() != null)
		{
			final double zefiralBoardWidth = getSafeMeasure(getZefiralBoardWidth()).doubleValue(Units.INCH);
			// Check if the board profile is the special case Aluminium
			if (getBoardProfile().equals(HingeShutterPanelProfiles.ALUMINIUM) && MathFunction.greater(zefiralBoardWidth, 0d, ShapeFunction.DELTA))
			{
				final double maxPctWidth = (getSafeBoolean(getBoardHorizontal()) ? getFormOffset().getHeight() : getFormOffset().getWidth())
						/ zefiralBoardWidth;
				result = new Profile()
					{
						private static final long serialVersionUID = 5771245441736796404L;

						@Override
						protected void load()
						{
							Double accumulator = 0d;
							while (MathFunction.lower(accumulator, maxPctWidth, ShapeFunction.DELTA))
							{
								addLine(-45);

								accumulator += 0.05d;
								addGradientSegment(0.05d, 0);

								accumulator += 0.05d;
								addGradientSegment(0.05d, -45, 0);

								accumulator += 0.06d;
								addGradientSegment(0.06d, 25, 0);

								accumulator += 0.06d;
								addGradientSegment(0.06d, 0);

								addLine(25);

								accumulator += 0.06d;
								addGradientSegment(0.06d, 25, 0);

								accumulator += 0.67d;
								addGradientSegment(0.67d, 0);

								accumulator += 0.05d;
								addGradientSegment(0.05d, 0, -45);

							}
						}
					};
			}
			else
			{
				try
				{
					result = getBoardProfile().profileClass.newInstance();
				}
				catch (InstantiationException e)
				{
					logger.error(e, e);
				}
				catch (IllegalAccessException e)
				{
					logger.error(e, e);
				}
			}
		}
		return result;
	}

	/**
	 * Add the boards of the panel section
	 * 
	 * @return
	 */
	protected List<ShutterBoard> addBoards()
	{
		List<ShutterBoard> result = new ArrayList<ShutterBoard>();
		double boardWidth = getSafeMeasure(getBoardWidth()).doubleValue(Units.INCH);
		boolean horizontal = getSafeBoolean(getBoardHorizontal());

		Form masterForm = getParent(Panels.class).getDimension().getForm();
		Form f = getFormOffset();

		if (f != null && MathFunction.greater(boardWidth, 0d, ShapeFunction.DELTA))
		{
			double spaceWidth = getSafeMeasure(getBoardSpacing()).doubleValue(Units.INCH);
			boolean hasSpace = MathFunction.greater(spaceWidth, 0d, ShapeFunction.DELTA);

			List<Double> cuts = new ArrayList<Double>();

			// Start the iteration on the master form
			double accumulator = horizontal ? masterForm.getBounds().getMinY() : masterForm.getBounds().getMinX();

			// Determine the min cut on the panel section form
			double minCut = horizontal ? f.getBounds().getMinY() : f.getBounds().getMinX();

			// Determine the max cut on the panel section form
			double maxCut = horizontal ? f.getBounds().getMaxY() : f.getBounds().getMaxX();

			// Indicate if the first cut is a board
			boolean firstIsBoard = true;

			while (MathFunction.lower(accumulator, maxCut, ShapeFunction.DELTA))
			{
				accumulator += boardWidth;

				// Check if the cut is on the panel section form
				if (MathFunction.greater(accumulator, minCut, ShapeFunction.DELTA) && MathFunction.lower(accumulator, maxCut, ShapeFunction.DELTA))
				{
					cuts.add(accumulator);
				}

				// Check if there is space between each board
				if (hasSpace)
				{
					accumulator += spaceWidth;

					// Check if the cut is on the panel section form
					if (MathFunction.greater(accumulator, minCut, ShapeFunction.DELTA) && MathFunction.lower(accumulator, maxCut, ShapeFunction.DELTA))
					{
						// Check if this is the first cut
						if (cuts.size() == 0)
						{
							// Indicate that the first cut is not a board
							firstIsBoard = false;
						}
						cuts.add(accumulator);
					}
				}
			}

			// Get the panel section cuts
			List<Form> splittedForms = split(f, horizontal ? Orientation.HORIZONTAL : Orientation.VERTICAL, cuts);

			int i = 0;

			Profile boardProfile = getBoardProfileInstance();

			// Create the boards from the cuts
			for (Form splitForm : splittedForms)
			{
				if (splitForm != null && (!hasSpace || (i++ % 2) == (firstIsBoard ? 0 : 1)))
				{
					result.add(new ShutterBoard(splitForm, boardProfile, getBoardColor(), getBoardZIndex()));
				}
			}
		}
		return result;
	}

	private void onBoardProfileChanged()
	{
		setDefaultZefiralBoardWidth(getBoardWidth());
	}
}
