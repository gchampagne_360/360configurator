// $Id: $

package com.client360.configuration.hingeshutters;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class ShutterPanelSection extends com.client360.configuration.hingeshutters.ShutterPanelSectionBase
{
	private static final long serialVersionUID = 3895825580505309243L;

	private static final String PANEL_SECTION_REPLICATION_PREFIX = "ShutterPanelSection";

	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
    public ShutterPanelSection(com.netappsid.erp.configurator.Configurable parent)
    {
        super(parent);
    }
    
	@Override
    public void activateListener()
    {
    	super.activateListener();
    	
    	addAddressedPropertyChangeListener(new PropertyChangeListener()
			{
				
				@Override
				public void propertyChange(PropertyChangeEvent evt)
				{
					String propertyName = evt.getPropertyName();
					if(propertyName.equals(PROPERTYNAME_HINGESHUTTERPANELTYPES))
					{
						onHingeShutterPanelTypesChanged();
					}
				}
			});
    }

    // Add custom overrides here

	/**
	 * Setup the replication of a panel model
	 * 
	 * @param publish
	 *            Publish the properties
	 * @param prefix
	 *            Prefix of the published names
	 */
	public void managePropertiesReplication(boolean publish, String prefix)
	{
		// Build the prefix
		String propertyPrefix = String.format("%s%s", PANEL_SECTION_REPLICATION_PREFIX, prefix);

		managePropertyReplication(publish, PROPERTYNAME_HEIGHT, propertyPrefix);
		managePropertyReplication(publish, PROPERTYNAME_HINGESHUTTERPANELTYPES, propertyPrefix);
	}

	/**
	 * Redistribute the panel models if the type of a section changes
	 */
	private void onHingeShutterPanelTypesChanged()
	{
		getParent(MixedPanel.class).distributeSectionModel();
	}

	/**
	 * Manage the replication of a property
	 * 
	 * @param publish
	 *            Indicate if the property has to be publish (false = listen)
	 * @param propertyName
	 *            Name fo the property to replicate
	 * @param prefix
	 *            Prefix of the published property name
	 */
	private void managePropertyReplication(boolean publish, String propertyName, String prefix)
	{
		// Check if the property has to be published
		if (publish)
		{
			publishProperty(propertyName, prefix + "." + propertyName);
		}
		// Check if the property has to be listened
		else
		{
			subscribe(propertyName, prefix + "." + propertyName);
		}
	}
}
