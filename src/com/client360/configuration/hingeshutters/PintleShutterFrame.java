// $Id: $

package com.client360.configuration.hingeshutters;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.hingeshutters.enums.PintleShutterFrameTypes;
import com.client360.configuration.wad.StandardColor;
import com.netappsid.cad.profile.HingeShutterProfileA;
import com.netappsid.cad.profile.HingeShutterProfileB;
import com.netappsid.commonutils.geo.CornerType;
import com.netappsid.commonutils.geo.Form;
import com.netappsid.commonutils.geo.ShapeFunction;
import com.netappsid.commonutils.math.Units;
import com.netappsid.rendering.common.advanced.Profile;


public class PintleShutterFrame extends com.client360.configuration.hingeshutters.PintleShutterFrameBase
{
	private static final long serialVersionUID = 5671425195005214052L;

	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
    public PintleShutterFrame(com.netappsid.erp.configurator.Configurable parent)
    {
        super(parent);
    }

	@Override
    public void activateListener()
    {
    	super.activateListener();
    	
    	addAddressedPropertyChangeListener(new PropertyChangeListener()
			{
				
				@Override
				public void propertyChange(PropertyChangeEvent evt)
				{
					String propertyName = evt.getPropertyName();
					
					if (propertyName.equals(PROPERTYNAME_PINTLESHUTTERFRAMETYPES))
					{
						onPintleShutterFrameTypeChanged();
					}
					else if(propertyName.equals(PROPERTYNAME_FRAMESIDEOFFSET))
					{
						frameOffsetChanged();
					}
					else if(propertyName.equals(PROPERTYNAME_FRAMESIDESPACER))
					{
						frameSpacerChanged();
					}
				}
			});
    }
	
	@Override
	public Unit<?> getUnit()
	{
		return Units.MM;
	}

	@Override
	protected Class<? extends com.netappsid.wadconfigurator.Color> getColorClass()
	{
		return StandardColor.class;
	}

    // Add custom overrides here

	/**
	 * Manage the change of the profile of the frame
	 */
	private void onPintleShutterFrameTypeChanged()
	{
		PintleShutter parent = getParent(PintleShutter.class);
		if (parent == null || parent.getDimension() == null || parent.getDimension().getForm() == null)
		{
			return;
		}

		Form f = parent.getDimension().getForm();
		int nbEdges = f.getNbEdges();

		Profile[] profiles = new Profile[nbEdges];
		Profile profile = null;

		// Instantiate the chosen profile
		if(getPintleShutterFrameTypes() != null)
		{
			if (getPintleShutterFrameTypes().equals(PintleShutterFrameTypes.WITH_BEVEL))
			{
				removeCorners();
			}
			else
			{
				createFullVerticalCorners();
			}

			try
			{
				profile = getPintleShutterFrameTypes().equals(PintleShutterFrameTypes.WITH_BEVEL) ? HingeShutterProfileB.class.newInstance()
						: HingeShutterProfileA.class.newInstance();
			}
			catch (Exception e)
			{
				logger.error(e, e);
			}
		}

		for (int i = 0; i < nbEdges; ++i)
		{
			profiles[i] = profile;
		}

		// Distribute the profiles to the wad property
		setDefaultProfiles(profiles);
	}
	
	/**
	 * Manage the changed of the offset from the product
	 */
	@SuppressWarnings("unchecked")
	private void frameOffsetChanged()
	{
		PintleShutter parent = getParent(PintleShutter.class);
		if(parent == null || parent.getDimension() == null || parent.getDimension().getForm() == null)
		{
			return;
		}

		Form f = parent.getDimension().getForm();
		int nbEdges = f.getNbEdges();
		
		Measure<Length>[] offsets = null;
		
		int sillIndex = ShapeFunction.getSillIndex(f);

		if(getFrameSideOffset() != null)
		{
			offsets = new Measure[nbEdges];	
				
			for (int i = 0; i < nbEdges; ++i)
			{
				// Check if the current edge is the sill
				if (i == sillIndex)
				{
					offsets[i] = Units.ZEROMM;
				}
				else
				{
					offsets[i] = getFrameSideOffset();
				}
			}
		}
		
		// Distribute the offset to the wad property
		setDefaultOffset(offsets);
	}
	
	/**
	 * Manage a change of the spacer
	 */
	@SuppressWarnings("unchecked")
	private void frameSpacerChanged()
	{
		PintleShutter parent = getParent(PintleShutter.class);
		if (parent == null || parent.getDimension() == null || parent.getDimension().getForm() == null)
		{
			return;
		}

		Form f = parent.getDimension().getForm();
		int nbEdges = f.getNbEdges();
		
		int sillIndex = ShapeFunction.getSillIndex(f);
		
		Measure<Length>[] spacers = null;
		
		if(getFrameSideSpacer() != null)
		{
			spacers = new Measure[nbEdges];
			for (int i = 0; i < nbEdges; ++i)
			{
				// Check if the current edge is the sill
				if (i == sillIndex)
				{
					spacers[i] = Units.ZEROMM;
				}
				else
				{
					spacers[i] = getSafeMeasure(getFrameSideSpacer());
				}
			}
		}

		// Distribute the spacer to the wad property
		setDefaultBorderSpacer(spacers);
	}

	/**
	 * Set vertical corners to the pintle shutter frame
	 */
	private void createFullVerticalCorners()
	{

		Form f = getParent(PintleShutter.class).getDimension().getForm();
		int nbEdges = f.getNbEdges();

		int sillIndex = ShapeFunction.getSillIndex(f);
		int leftIndex = (sillIndex + 1) % nbEdges;
		int rightIndex = (sillIndex + nbEdges - 1) % nbEdges;
		int topIndex = (leftIndex + 1) % nbEdges;

		CornerType[] result = new CornerType[nbEdges];
		for (int i = 0; i < nbEdges; ++i)
		{
			if (i == sillIndex || i == topIndex)
			{
				result[i] = CornerType.SHORT_CORNER;
			}
			else if (i == leftIndex || i == rightIndex)
			{
				result[i] = CornerType.LONG_CORNER;
			}
			else
			{
				result[i] = CornerType.HALF_CORNER;
			}
		}

		setCorners(result);
	}
}
