// $Id: $

package com.client360.configuration.hingeshutters;

import javax.measure.units.Unit;

import com.netappsid.commonutils.geo.Form;
import com.netappsid.commonutils.math.Units;


public class HingeShutter extends com.client360.configuration.hingeshutters.HingeShutterBase
{
	private static final long serialVersionUID = 5686318634946868775L;

	private static final double DEFAULT_HEIGHT = 1400d;
	private static final double DEFAULT_WIDTH = 1200d;

    // Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
    public HingeShutter(com.netappsid.erp.configurator.Configurable parent)
    {
        super(parent);

		Form f = Form.create("RECTANGLE_N");
		getDimension().setForm(f);
		getDimension().setDefaultHeight(Units.mm(DEFAULT_HEIGHT));
		getDimension().setDefaultWidth(Units.mm(DEFAULT_WIDTH));
	}

    // Add custom overrides here

	@Override
	public Unit<?> getUnit()
	{
		return Units.MM;
	}

	@Override
	protected Class<? extends com.netappsid.configuration.common.Dimension> getDimensionClass()
	{
		return com.client360.configuration.wad.Dimension.class;
	}

	@Override
	protected Class<? extends com.netappsid.configuration.hingeshutters.Panels> getPanelsClass()
	{
		return Panels.class;
	}
}
