// $Id: $

package com.client360.configuration.hingeshutters;

import java.util.ArrayList;
import java.util.List;

import com.netappsid.commonutils.geo.CornerType;
import com.netappsid.commonutils.geo.Form;
import com.netappsid.commonutils.geo.ShapeFunction;
import com.netappsid.commonutils.math.MathFunction;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.hingeshutters.ShutterBoard;
import com.netappsid.rendering.common.part.ComplexForm;
import com.netappsid.rendering.common.part.edge.Edge;


public class OpeningPanel extends com.client360.configuration.hingeshutters.OpeningPanelBase
{
	private static final long serialVersionUID = -7747254611296565836L;
	private static final String HINGE_FILENAME = "/images/hingeshutter/penture.png";
	private static final String OPENING_PANEL_REPLICATION_PREFIX = "OpeningPanel";

	private static final int NB_EDGE_OPENING_PANEL = 4;

	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
    public OpeningPanel(com.netappsid.erp.configurator.Configurable parent)
    {
        super(parent);
    }
    
    // Add custom overrides here

	/**
	 * Create the boards of the opening panel
	 * 
	 * @return Boards
	 */
	@Override
	protected List<ShutterBoard> createBoards()
	{
		List<ShutterBoard> result = super.createBoards();

		// Append the opening panel boards
		result.addAll(createOpeningPanel());

		// Add the hinges to the custom visual composition of the PanelModel
		createHinges();

		return result;
	}

	/**
	 * Replicate the opening panel
	 * 
	 * @param publish
	 *            Indicate if the properties have to be published (false = listened)
	 * @param prefix
	 *            Prefix to add to the published name
	 */
	@Override
	public void managePropertiesReplication(boolean publish, String prefix)
	{
		super.managePropertiesReplication(publish, prefix);

		String propertyPrefix = OPENING_PANEL_REPLICATION_PREFIX + prefix == null ? "" : prefix;

		managePropertyReplication(publish, PROPERTYNAME_OPENINGLEFT, propertyPrefix);
		managePropertyReplication(publish, PROPERTYNAME_OPENINGRIGHT, propertyPrefix);
		managePropertyReplication(publish, PROPERTYNAME_OPENINGTOP, propertyPrefix);
		managePropertyReplication(publish, PROPERTYNAME_OPENINGSILL, propertyPrefix);

		managePropertyReplication(publish, PROPERTYNAME_OFFSETSIDEHINGE, propertyPrefix);
		managePropertyReplication(publish, PROPERTYNAME_OFFSETTOPHINGE, propertyPrefix);
		managePropertyReplication(publish, PROPERTYNAME_IMAGEHINGEWIDTH, propertyPrefix);
		managePropertyReplication(publish, PROPERTYNAME_IMAGEHINGEHEIGHT, propertyPrefix);
	}

	/**
	 * Support the offset of the opening panel if the offsetform
	 * 
	 * @return Offset form
	 */
	@Override
	protected Form getFormOffset()
	{
		Form result = getFormPerimeter();
		if (result != null)
		{
			Double[] spacers = calculateOffset(result, getMeasure(getOpeningLeft()), getMeasure(getOpeningRight()), getMeasure(getOpeningTop()),
					getMeasure(getOpeningSill()));
			try
			{
				result = ShapeFunction.getShapeOffset(result, spacers);
			}
			catch (IllegalArgumentException e)
			{
				e.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * Create the boards of the opening panel
	 * 
	 * @return
	 */
	private List<ShutterBoard> createOpeningPanel()
	{
		List<ShutterBoard> result = new ArrayList<ShutterBoard>();

		Form f = getFormPerimeter();
		if (f != null)
		{
			if(f.getNbEdges() != NB_EDGE_OPENING_PANEL)
			{
				throw new IllegalStateException("Opening Panel should have 4 edges");
			}
			else
			{
				Double[] perimeterOffset = calculateOffset(f, getMeasure(getOpeningLeft()), getMeasure(getOpeningRight()), getMeasure(getOpeningTop()),
						getMeasure(getOpeningSill()));
	
				CornerType[] corners = new CornerType[] { CornerType.SHORT_CORNER, CornerType.LONG_CORNER, CornerType.SHORT_CORNER, CornerType.LONG_CORNER };
	
				ComplexForm complexForm = new ComplexForm();
				complexForm.setForm(f);
				complexForm.setSpacer(perimeterOffset);
				complexForm.setCornerType(corners);
	
				Edge[] perimeterEdges = complexForm.getEdges();
	
				// Create the opening panel perimeter
				if (perimeterEdges != null)
				{
					for (Edge e : perimeterEdges)
					{
						if (e == null || e.getShape() == null)
							continue;
						result.add(new ShutterBoard(e.getShape(), getPerimeterProfileInstance(), getPerimeterColor(), getPerimeterZIndex()));
					}
				}
			}
		}
		return result;
	}

	/**
	 * Get the form of the section with the offset of the perimeter
	 * 
	 * @return Offset form
	 */
	private Form getFormPerimeter()
	{
		Form result;
		Form f = getForm();

		// Get the max offset of the form (offset and perimeter)
		double offsetLeft = Math.max(getMeasure(getOffsetLeft()), getMeasure(getPerimeterLeft()));
		double offsetRight = Math.max(getMeasure(getOffsetRight()), getMeasure(getPerimeterRight()));
		double offsetTop = Math.max(getMeasure(getOffsetTop()), getMeasure(getPerimeterTop()));
		double offsetSill = Math.max(getMeasure(getOffsetSill()), getMeasure(getPerimeterSill()));

		Double[] spacers = calculateOffset(f, offsetLeft, offsetRight, offsetTop, offsetSill);
		try
		{
			result = ShapeFunction.getShapeOffset(f, spacers);
		}
		catch (IllegalArgumentException e)
		{
			result = f;
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Add the images of the hinges to the parent mixed panel
	 */
	private void createHinges()
	{
		double hingeWidth = getMeasure(getImageHingeWidth());
		double hingeHeight = getMeasure(getImageHingeHeight());

		MixedPanel parent = getParent(MixedPanel.class);
		parent.clearImages();
		
		// Check if the hinges have a valid dimension
		if (MathFunction.greater(hingeWidth, 0d, ShapeFunction.DELTA)
				&& MathFunction.greater(hingeHeight, 0d, ShapeFunction.DELTA))
		{

			Form f = getForm();

			parent.addImage(this, HINGE_FILENAME, getImageHingeWidth(), getImageHingeHeight(), getOffsetSideHinge(), getOffsetTopHinge(), getHingeZIndex());

			parent.addImage(this, HINGE_FILENAME, getImageHingeWidth(), getImageHingeHeight(),
					Units.inch(f.getWidth() - getMeasure(getImageHingeWidth()) - getMeasure(getOffsetSideHinge())), getOffsetTopHinge(), getHingeZIndex());
		}
	}

	/**
	 * Get the z index of the hinges visual cell
	 * 
	 * @return Z index
	 */
	private int getHingeZIndex()
	{
		return getBoardZIndex() - 1;
	}
}
