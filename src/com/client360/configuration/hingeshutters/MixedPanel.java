// $Id: $

package com.client360.configuration.hingeshutters;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.measure.quantities.Length;
import javax.measure.units.Unit;

import org.jscience.physics.measures.Measure;

import com.netappsid.commonutils.geo.Form;
import com.netappsid.commonutils.geo.enums.Orientation;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.common.PointMeasure;
import com.netappsid.configuration.common.ZoneVisualComposition;
import com.netappsid.configuration.common.utils.VisualCompositionUtils.ImageHorizontalAlignement;
import com.netappsid.configuration.common.utils.VisualCompositionUtils.ImageVerticalAlignement;
import com.netappsid.configuration.hingeshutters.ShutterBoard;

public class MixedPanel extends com.client360.configuration.hingeshutters.MixedPanelBase
{
	private static final long serialVersionUID = 8536543491868199144L;
	private static final String SHUTTER_PANEL_SECTION_PATTERN = String.format("%s%s[0-9]+%s.%s", PROPERTYNAME_SHUTTERPANELSECTION, Pattern.quote("["),
			Pattern.quote("]"), ShutterPanelSection.PROPERTYNAME_HEIGHT);
	
	private static final String MIXED_PANEL_REPLICATION_PREFIX = "MixedPanel";

	private final double INCH_TO_MM = Units.inch(1d).doubleValue(Units.MM);

	private transient boolean creatingPanels;

    // Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
    public MixedPanel(com.netappsid.erp.configurator.Configurable parent)
    {
        super(parent);
    }
    
	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		putExpandedCollections(PROPERTYNAME_SHUTTERPANELSECTION, true);
		putExpandedCollections(PROPERTYNAME_PANELMODEL, true);

		makeReadOnly(PROPERTYNAME_SHUTTERPANELSECTION);
		makeReadOnly(PROPERTYNAME_PANELMODEL);

		makeInvisible(PROPERTYNAME_SHUTTERPANELSECTION);
		makeInvisible(PROPERTYNAME_PANELMODEL);
	}

	@Override
	public void activateListener()
	{
		super.activateListener();

		addAddressedPropertyChangeListener(new PropertyChangeListener()
			{
				@Override
				public void propertyChange(PropertyChangeEvent evt)
				{
					String propertyName = evt.getPropertyName();
					if (propertyName.equals(PROPERTYNAME_NBSUBPANELS))
					{
						onNbSubPanelsChanged();
					}
					else if (propertyName.equals(PROPERTYNAME_FORM) || propertyName.equals(PROPERTYNAME_OFFSETLEFT)
							|| propertyName.equals(PROPERTYNAME_OFFSETRIGHT) || propertyName.equals(PROPERTYNAME_OFFSETTOP)
							|| propertyName.equals(PROPERTYNAME_OFFSETSILL))
					{
						onFormChanged();
					}
					else if(propertyName.matches(SHUTTER_PANEL_SECTION_PATTERN))
					{
						if (!creatingPanels)
						{
							creatingPanels = true;
							refreshSectionsHeight();
							distributeSectionForm();
							creatingPanels = false;
						}
					}
				}
			});
	}

    // Add custom overrides here
	
	/**
	 * Manage the replication of the mixed panel properties
	 * 
	 * @param publish
	 *            Indicate if the properties has to be published
	 * @param prefix
	 *            Prefix to apply on the properties
	 */
	@Override
	public void managePropertiesReplication(boolean publish, String prefix)
	{
		super.managePropertiesReplication(publish, prefix);

		managePropertyReplication(publish, PROPERTYNAME_NBSUBPANELS, MIXED_PANEL_REPLICATION_PREFIX + ((prefix == null) ? "" : prefix));
	}

	@Override
	public Unit<?> getUnit()
	{
		return Units.MM;
	}

	/**
	 * Check if the panel contains a panel of the instance receive as a parameter
	 * 
	 * @param clazz
	 *            Class to find
	 * @return This panel or a subpanel is an instance of the class clazz
	 */
	@Override
	public boolean containsModel(Class<? extends PanelModel> clazz)
	{
		boolean result = super.containsModel(clazz);

		if (getPanelModel() != null)
		{
			for (PanelModel m : getPanelModel())
			{
				if (m != null && m.getClass().equals(clazz))
				{
					result = true;
					break;
				}
			}
		}
		return result;
	}

	/**
	 * Spread and instantiate the selected model types to the collection of panel sections
	 */
	public void distributeSectionModel()
	{
		int i = 0;

		// Create the empty collection of panel models
		PanelModel[] panels = new PanelModel[getNbSubPanels()];
		for (ShutterPanelSection s : getShutterPanelSection())
		{
			if (s.getHingeShutterPanelTypes() != null && !(getPanelModel()[i].getClass().equals(s.getHingeShutterPanelTypes().modelClass)))
			{
				panels[i] = (PanelModel) instanciateConfigurable(s.getHingeShutterPanelTypes().modelClass, this);
			}
			else
			{
				panels[i] = getPanelModel()[i];
			}
			++i;
		}
		setPanelModel(panels);
		distributeSectionForm();
	}

	/**
	 * Add a image to the PanelModel visual composition
	 * 
	 * @param child
	 *            Child panel model section
	 * @param path
	 *            Path of the image to create
	 * @param w
	 *            Width of the image
	 * @param h
	 *            Height of the image
	 * @param offsetX
	 *            Offset X of the image
	 * @param offsetY
	 *            Offset Y of the image
	 * @param zIndex
	 *            Z index of the image
	 */
	public void addImage(PanelModel child, String path, Measure<Length> w, Measure<Length> h,
			Measure<Length> offsetX, Measure<Length> offsetY, int zIndex)
	{
		ZoneVisualComposition zone = getCustomVisualComposition();

		// Check if the zone doesn't exist yet
		if (zone == null)
		{
			zone = new ZoneVisualComposition(this);
			setCustomVisualComposition(zone);
			zone.setWidth(Units.inch(getForm().getWidth()));
			zone.setHeight(Units.inch(getForm().getHeight()));
			zone.setPosition(PointMeasure.newAt(getForm().getOrigin().getX(), getForm().getOrigin().getY(), Units.INCH, zone));
		}

		double additionalOffsetX = child.getForm().getBounds().getMinX() - getForm().getBounds().getMinX();
		double additionalOffsetY = child.getForm().getBounds().getMinY() - getForm().getBounds().getMinY();

		// Add the image at the appropriate offset
		zone.addImage(path, w, h, 0d, 1d, 0d, 1d, ImageHorizontalAlignement.LEFT, getSafeMeasure(offsetX).plus(Units.inch(additionalOffsetX)),
				ImageVerticalAlignement.TOP, getSafeMeasure(offsetY).plus(Units.inch(additionalOffsetY)));

		// Set the z order to the newly added cell
		zone.getVisualCell()[zone.getVisualCell().length - 1].setZOrder(zIndex);
	}

	/**
	 * Clear the PanelModel visual composition
	 */
	public void clearImages()
	{
		ZoneVisualComposition zone = getCustomVisualComposition();
		if (zone != null)
		{
			zone.clearCells();
			removeCustomVisualComposition();
		}
	}

	/**
	 * Create the boards of the MixedPanel by iterating through its children
	 * 
	 * @return Boards of the panel model
	 */
	@Override
	protected List<ShutterBoard> createBoards()
	{
		// Build the MixedPanel boards
		List<ShutterBoard> result = super.createBoards();

		// Check if the boards has children
		if(getPanelModel() != null)
		{
			// Get the boards of the children
			for(PanelModel m : getPanelModel())
			{
				if (m.getForm() != null && m.getForm().isValid())
				{
					List<ShutterBoard> subModelBoards = m.createBoards();
					if (subModelBoards != null)
						result.addAll(subModelBoards);
				}
			}
		}
		
		return result;
	}

	/**
	 * Create the sub panels collection
	 */
	private void onNbSubPanelsChanged()
	{
		creatingPanels = true;

		// Check if the quantity of sub panels is invalid
		if (getSafeInteger(getNbSubPanels()) == 0)
		{
			// Remove the collection
			setShutterPanelSection(null);

			setPanelModel(null);

			makeInvisible(PROPERTYNAME_SHUTTERPANELSECTION);
			makeInvisible(PROPERTYNAME_PANELMODEL);
		}
		else
		{
			// Create the colection
			setShutterPanelSection(makeArray(ShutterPanelSection.class, getNbSubPanels(), this));
			setPanelModel(makeArray(PanelModel.class, getNbSubPanels(), this));

			makeVisible(PROPERTYNAME_SHUTTERPANELSECTION);
			makeVisible(PROPERTYNAME_PANELMODEL);

			distributeSectionHeight();
			distributeSectionForm();
		}
		creatingPanels = false;
	}

	/**
	 * Redistribute the dimension to the panel model children
	 */
	private void onFormChanged()
	{
		if (getSafeInteger(getNbSubPanels()) > 0)
		{
			distributeSectionHeight();
			distributeSectionForm();
		}
	}

	/**
	 * Distribute the height of the sections children
	 */
	private void distributeSectionHeight()
	{
		if (getParent(Panel.class).getIndex() == 0)
		{
			double h = getParent(Panels.class).getDimension().getForm().getHeight() * INCH_TO_MM;

			double sectionHeight = h / getNbSubPanels();

			for (ShutterPanelSection s : getShutterPanelSection())
			{
				s.setDefaultHeight(Units.mm(sectionHeight));
			}
		}
	}

	/**
	 * Distribute the forms to the sections children
	 */
	private void distributeSectionForm()
	{
		if (getShutterPanelSection() != null && getPanelModel() != null)
		{
			List<Double> cuts = new ArrayList<Double>();
			double minY = getParent(Panels.class).getDimension().getForm().getBounds().getMinY();
			double accumulator = 0;
			for(ShutterPanelSection p : getShutterPanelSection())
			{
				double sectionCut = getSafeMeasure(p.getHeight()).doubleValue(Units.INCH);
				accumulator += sectionCut;
				cuts.add(accumulator + minY);
			}
			List<Form> f = split(getFormOffset(), Orientation.HORIZONTAL, cuts);
			int cut = 0;
			boolean publish = getParent(Panel.class).getIndex() == 0;

			// Do not replicate if the nbPanelModel is different from the first panel
			int i = 0;
			for (PanelModel m : getPanelModel())
			{
				m.setForm(f.get(cut++));
				m.managePropertiesReplication(publish, String.format("%s", i++));
			}

			i = 0;
			for (ShutterPanelSection s : getShutterPanelSection())
			{
				s.managePropertiesReplication(publish, String.format("%s", i++));
			}
		}
	}

	/**
	 * The last section has to be calculated with the remains of the other sections
	 */
	private void refreshSectionsHeight()
	{
		if (getParent(Panels.class).getDimension().getForm() != null && getParent(Panel.class).getIndex() == 0)
		{
			double height = getParent(Panels.class).getDimension().getForm().getHeight();

			// Accumulator of the sections that have their default height
			List<ShutterPanelSection> defaultHeightSections = new ArrayList<ShutterPanelSection>();

			// TODO check if the first panel is overriden
			for (ShutterPanelSection p : getShutterPanelSection())
			{
				// Check if the height of the section is overriden
				if(p.isPropertyOverriden(ShutterPanelSection.PROPERTYNAME_HEIGHT))
				{
					// Remove the available section height
					height -= p.getHeight().doubleValue(Units.INCH);
				}
				else
				{
					// Register the section as a default height section
					defaultHeightSections.add(p);
				}
			}

			// Distribute the remaining height over the default height sections
			int nbDefaultHeightSections = defaultHeightSections.size();
			for (ShutterPanelSection p : defaultHeightSections)
			{
				p.setDefaultHeight(Units.inch(height / nbDefaultHeightSections));
			}

			distributeSectionForm();
		}
	}
}
