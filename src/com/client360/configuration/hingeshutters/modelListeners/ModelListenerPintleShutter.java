// $Id: $

package com.client360.configuration.hingeshutters.modelListeners;

import java.util.List;

import com.client360.configuration.hingeshutters.PintleShutter;
import com.netappsid.erp.configurator.renderers.RenderingComponent;

public class ModelListenerPintleShutter extends com.netappsid.configuration.hingeshutters.modelListeners.ModelListenerPintleShutter
{
    // Default constructor
	public ModelListenerPintleShutter(PintleShutter configurable)
    { 
        super(configurable); 
    }

	public ModelListenerPintleShutter(PintleShutter configurable, List<RenderingComponent> renderingComponents)
    { 
        super(configurable, renderingComponents); 
    }

    // Add custom overrides here
}
