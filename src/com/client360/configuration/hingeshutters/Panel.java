// $Id: $

package com.client360.configuration.hingeshutters;


public class Panel extends com.client360.configuration.hingeshutters.PanelBase
{
	private static final long serialVersionUID = -5153858399201855020L;
	
    // Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
    public Panel(com.netappsid.erp.configurator.Configurable parent)
    {
        super(parent);
    }

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
		setPanelModel(instanciateConfigurable(MixedPanel.class, this));
	}

	@Override
	public void activateListener()
	{
		super.activateListener();
	}

	// Add custom overrides here
}
