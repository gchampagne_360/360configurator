// $Id: $

package com.client360.configuration.hingeshutters;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.regex.Pattern;

import javax.measure.units.Unit;

import com.netappsid.commonutils.geo.Form;
import com.netappsid.commonutils.geo.NAIDEdge;
import com.netappsid.commonutils.geo.NAIDShape;
import com.netappsid.commonutils.geo.ShapeFunction;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.common.Dimension;
import com.netappsid.configuration.common.ZoneVisualComposition;
import com.netappsid.configuration.common.utils.VisualCompositionUtils.ImageHorizontalAlignement;
import com.netappsid.configuration.common.utils.VisualCompositionUtils.ImageVerticalAlignement;
import com.netappsid.configuration.hingeshutters.Panel;
import com.netappsid.configuration.hingeshutters.Panels;
import com.netappsid.configuration.hingeshutters.enums.ChoiceSide;

public class PintleShutter extends com.client360.configuration.hingeshutters.PintleShutterBase
{
	private static final long serialVersionUID = -642426343330825974L;

	private static final double INCH_TO_MM = Units.inch(1d).doubleValue(Units.MM);

	private static final double DEFAULT_HEIGHT = 1400d;
	private static final double DEFAULT_WIDTH = 1200d;

	private static final String IMAGE_PINTLE = "/images/hingeshutter/gond.png";
	private static final String IMAGE_STOPPER = "/images/hingeshutter/butee.png";
	private static final String IMAGE_HOLDBACK = "/images/hingeshutter/holdback.png";

	private static final String PANEL_CHOICESIDE_REGEX = String.format("%s.panel%s[0-9]+%s.%s", PROPERTYNAME_PANELS, Pattern.quote("["), Pattern.quote("]"),
			Panel.PROPERTYNAME_CHOICESIDE);

	private static final int DEFAULT_BACKCOMPOSITION_Z_INDEX = com.netappsid.configuration.hingeshutters.Panel.MODELS_ZLAYER + 75;
	private static final int DEFAULT_HOLDBACKS_Z_INDEX = com.netappsid.configuration.hingeshutters.Panel.MODELS_ZLAYER - 75;

	// private static final String IMAGE_HOLDBACK_HORIZONTAL = "/images/hingeshutter/holdback2.png";

	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public PintleShutter(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		// getDimension().setForm(createPentagon());
		getDimension().setForm(createArch());
		// getDimension().setForm(Form.create("RECTANGLE_N"));
		getDimension().setDefaultHeight(Units.mm(DEFAULT_HEIGHT));
		getDimension().setDefaultWidth(Units.mm(DEFAULT_WIDTH));
	}

	@Override
	public void activateListener()
	{
		super.activateListener();

		addAddressedPropertyChangeListener(new PropertyChangeListener()
			{

				@Override
				public void propertyChange(PropertyChangeEvent evt)
				{
					String propertyName = evt.getPropertyName();
					if (propertyName.equals(PROPERTYNAME_DISPLAYPINTLES))
					{
						addPintlesImage();
					}
					else if (propertyName.equals(PROPERTYNAME_DISPLAYSTOPPERS))
					{
						addStoppersImage();
					}
					else if (propertyName.equals(PROPERTYNAME_DISPLAYHOLDBACKS))
					{
						addHoldBacksImages();
					}
					else if (propertyName.matches(PANEL_CHOICESIDE_REGEX))
					{
						setLeftPintlesDisplay(null);
						setRightPintlesDisplay(null);
						addPintlesImage();

						setLeftHoldBacksDisplay(null);
						setRightHoldBacksDisplay(null);
						addHoldBacksImages();

						setStoppersDisplay(null);
						addStoppersImage();
					}
				}
			});
	}

	@Override
	protected Class<? extends Panels> getPanelsClass()
	{
		return com.client360.configuration.hingeshutters.Panels.class;
	}

	@Override
	protected Class<? extends Dimension> getDimensionClass()
	{
		return com.client360.configuration.wad.Dimension.class;
	}

	@Override
	protected Class<? extends PintleShutterFrame> getPintleShutterFrameClass()
	{
		return com.client360.configuration.hingeshutters.PintleShutterFrame.class;
	}

	// Add custom overrides here

	@Override
	public String toString()
	{
		return getTranslatedString("pintleShutter");
	}

	@Override
	public Unit<?> getUnit()
	{
		return Units.MM;
	}
	
	/**
	 * Create a debug pentagon form for test
	 * 
	 * @return Pentagon
	 */
	private Form createPentagon()
	{
		Form result = null;

		Point2D[] points = new Point2D[5];

		points[0] = (new Point2D.Double(0.0, DEFAULT_HEIGHT * 0.4));
		points[1] = (new Point2D.Double(DEFAULT_WIDTH / 2, 0.0));
		points[2] = (new Point2D.Double(DEFAULT_WIDTH, DEFAULT_HEIGHT * 0.4));
		points[3] = (new Point2D.Double(DEFAULT_WIDTH, DEFAULT_HEIGHT));
		points[4] = (new Point2D.Double(0.0, DEFAULT_HEIGHT));

		NAIDShape shape = NAIDShape.create(ShapeFunction.buildPolygonFromPoints(points));

		result = Form.create(shape.getEdges());
		result.scale(1 / INCH_TO_MM);
		result.identifyByGeo(true, true);
		return result;
	}

	/**
	 * Create a debug Arch form for test
	 * 
	 * @return Arch
	 */
	private Form createArch()
	{
		Form result = null;
		
		double minX = 0d;
		double midX = DEFAULT_WIDTH * 0.5d;
		double maxX = DEFAULT_WIDTH;

		double minY = 0d;
		double midY = DEFAULT_HEIGHT * 0.1d;
		double maxY = DEFAULT_HEIGHT;
		
		NAIDEdge left = new NAIDEdge.Line(new Line2D.Double(minX, maxY, minX, midY));
		NAIDEdge right = new NAIDEdge.Line(new Line2D.Double(maxX, midY, maxX, maxY));
		NAIDEdge bottom = new NAIDEdge.Line(new Line2D.Double(maxX, maxY, minX, maxY));
		Point2D topLeft = new Point2D.Double(minX, midY);
		Point2D topRight = new Point2D.Double(maxX, midY);
		Point2D topArc = new Point2D.Double(midX, minY);
		NAIDEdge arc = new NAIDEdge.Arc(topLeft, topRight, ShapeFunction.getCenterPoint(topLeft, topArc, topRight), true);

		result = Form.create(arc, right, bottom, left).scale(1 / INCH_TO_MM);
		result.identifyByGeo(true, true);

		return result;
	}

	/**
	 * Add the pintles to the product
	 */
	private void addPintlesImage()
	{
		addLeftPintlesImage();
		addRightPintlesImage();
	}

	private void addLeftPintlesImage()
	{
		if (getPanels() != null && getPanels().getPanel() != null && getPanels().getPanel().length > 0)
		{
			if (getLeftPintlesDisplay() == null)
			{
				setLeftPintlesDisplay(new ZoneVisualComposition(this));
			}

			// Indicate if the user wants to display the left pintles
			boolean display = getSafeBoolean(getDisplayPintles()) && getPanels().panelOnPintle(getPanels().getPanel()[0])
					&& getPanels().getPanel()[0].getChoiceSide().equals(ChoiceSide.LEFT);

			// Check if the left pintles are already dr awn
			if (getLeftPintlesDisplay().getVisualCell() != null && getLeftPintlesDisplay().getVisualCell().length > 0)
			{
				// Check if the user wants to hide the left pintles
				if (!display)
				{
					getLeftPintlesDisplay().clearCells();
				}
				return;
			}

			// Check if the user wants to show the pintles
			if (display)
			{
				// Add the top left pintle
				getLeftPintlesDisplay().addImage(IMAGE_PINTLE, Units.mm(25d), Units.mm(75d), 0d, 1d, 0d, 1d, ImageHorizontalAlignement.LEFT, Units.mm(-12d),
						ImageVerticalAlignement.TOP, Units.mm(200d), DEFAULT_BACKCOMPOSITION_Z_INDEX);

				// Add the bottom left pintle
				getLeftPintlesDisplay().addImage(IMAGE_PINTLE, Units.mm(25d), Units.mm(75d), 0d, 1d, 0d, 1d, ImageHorizontalAlignement.LEFT, Units.mm(-12d),
						ImageVerticalAlignement.BOTTOM, Units.mm(200d), DEFAULT_BACKCOMPOSITION_Z_INDEX);
			}
		}
	}

	/**
	 * Add the pintles at the right of the product
	 */
	private void addRightPintlesImage()
	{
		if (getPanels() != null && getPanels().getPanel() != null && getPanels().getPanel().length > 0)
		{
			// Check if the right pintles' zone is valid
			if (getRightPintlesDisplay() == null)
			{
				setRightPintlesDisplay(new ZoneVisualComposition(this));
			}
			// Indicate if the user wants to show the pintles
			boolean display = getSafeBoolean(getDisplayPintles()) && getPanels().panelOnPintle(getPanels().getPanel()[getPanels().getPanel().length - 1])
					&& getPanels().getPanel()[getPanels().getPanel().length - 1].getChoiceSide().equals(ChoiceSide.RIGHT);

			// Check if the right pintnles are already drawn
			if (getRightPintlesDisplay().getVisualCell() != null && getRightPintlesDisplay().getVisualCell().length > 0)
			{
				// Check if the user want to hide the pintles
				if (!display)
				{
					// Remove the pintles from the zone
					getRightPintlesDisplay().clearCells();
				}
				return;
			}

			// Check if the user wants to draw the pintles
			if (display)
			{

				// Draw the top pintle
				getRightPintlesDisplay().addImage(IMAGE_PINTLE, Units.mm(25d), Units.mm(75d), 0d, 1d, 0d, 1d, ImageHorizontalAlignement.RIGHT, Units.mm(-12d),
						ImageVerticalAlignement.TOP, Units.mm(200d), DEFAULT_BACKCOMPOSITION_Z_INDEX);

				// Draw the bottom pintle
				getRightPintlesDisplay().addImage(IMAGE_PINTLE, Units.mm(25d), Units.mm(75d), 0d, 1d, 0d, 1d, ImageHorizontalAlignement.RIGHT, Units.mm(-12d),
						ImageVerticalAlignement.BOTTOM, Units.mm(200d), DEFAULT_BACKCOMPOSITION_Z_INDEX);
			}
		}
	}

	/**
	 * Add the stoppers image to the product using the zone visual composition
	 */
	private void addStoppersImage()
	{
		if (getPanels() != null && getPanels().getPanel() != null && getPanels().getPanel().length > 0)
		{
			int nbPanels = getPanels().getPanel().length;
			int nbStoppers = (nbPanels - 1) + ((nbPanels <= 1 || getPanels().allPanelsHaveSide(getPanels().getPanel()[0].getChoiceSide())) ? 1 : 0);

			if (nbStoppers > 0)
			{
				if (getStoppersDisplay() == null)
				{
					setStoppersDisplay(makeArray(ZoneVisualComposition.class, nbStoppers, this));
				}
				else if (getStoppersDisplay().length != nbStoppers)
				{
					setStoppersDisplay(makeArray(ZoneVisualComposition.class, nbStoppers, this));
				}

				// Check if the stopper zone is valid
				if (getStoppersDisplay() != null && getStoppersDisplay().length > 0)
				{
					// Indicate if the user wants to display the stoppers
					boolean display = getSafeBoolean(getDisplayStoppers());

					// Iterate through the stoppers' zone visual compositions
					for (ZoneVisualComposition z : getStoppersDisplay())
					{
						// Check if the stoppers are already in the zone
						if (z.getVisualCell() != null && z.getVisualCell().length > 0)
						{
							// Check if the user wants to hide them
							if (!display)
							{
								// Remove the stoppers
								z.clearCells();
							}
							continue;
						}

						// Check if the user wants to display the stoppers
						if (display)
						{

							// Display the stopper at the top of the product
							z.addImage(IMAGE_STOPPER, Units.mm(30d), Units.mm(25), 0d, 1d, 1d, 0d, ImageHorizontalAlignement.CENTER, Units.ZEROMM,
									ImageVerticalAlignement.TOP, Units.ZEROMM, DEFAULT_BACKCOMPOSITION_Z_INDEX);

							// Display the stopper at the bottom of the product
							z.addImage(IMAGE_STOPPER, Units.mm(30d), Units.mm(25), 0d, 1d, 0d, 1d, ImageHorizontalAlignement.CENTER, Units.ZEROMM,
									ImageVerticalAlignement.BOTTOM, Units.ZEROMM, DEFAULT_BACKCOMPOSITION_Z_INDEX);
						}
					}
				}
			}
		}
	}

	/**
	 * Display the hold backs images
	 */
	private void addHoldBacksImages()
	{

		// Check if there is no panels
		if (getPanels().getPanel() == null)
		{
			return;
		}

		// Check if the zone of the left holbacks is valid
		ZoneVisualComposition zone = getLeftHoldBacksDisplay();
		if (zone == null)
		{
			zone = new ZoneVisualComposition(this);
			setLeftHoldBacksDisplay(zone);
		}
		boolean display = getSafeBoolean(getDisplayHoldBacks()) && getPanels().panelOnPintle(getPanels().getPanel()[0])
				&& getPanels().getPanel()[0].getChoiceSide().equals(ChoiceSide.LEFT);
		if (zone.getVisualCell() != null && zone.getVisualCell().length > 0)
		{
			if (!display)
			{
				zone.clearCells();
			}
		}

		if (display)
		{
			// Display the holdback under the shutter panel
			zone.addImage(IMAGE_HOLDBACK, Units.mm(50d), Units.mm(175d), 0d, 1d, 0d, 1d, ImageHorizontalAlignement.CENTER, Units.ZEROMM,
					ImageVerticalAlignement.BOTTOM, Units.mm(-75d), DEFAULT_HOLDBACKS_Z_INDEX);

			// Display the holdback on the side of the shutter panel
			// zone.addImage(IMAGE_HOLDBACK_HORIZONTAL, Units.mm(175d), Units.mm(50d), 0d, 1d, 1d, 0d, ImageHorizontalAlignement.LEFT, Units.mm(-75d),
			// ImageVerticalAlignement.CENTER, Units.ZEROMM);
		}

		// Check if the zone of the right holdbacks is valid
		zone = getRightHoldBacksDisplay();
		if (zone == null)
		{
			zone = new ZoneVisualComposition(this);
			setRightHoldBacksDisplay(zone);
		}
		display = getSafeBoolean(getDisplayHoldBacks()) && getPanels().panelOnPintle(getPanels().getPanel()[getPanels().getPanel().length - 1])
				&& getPanels().getPanel()[getPanels().getPanel().length - 1].getChoiceSide().equals(ChoiceSide.RIGHT);
		if (zone.getVisualCell() != null && zone.getVisualCell().length > 0)
		{
			if (!display)
			{
				zone.clearCells();
			}
		}

		if (display)
		{
			// Display the holdback under the shutter panel
			zone.addImage(IMAGE_HOLDBACK, Units.mm(50d), Units.mm(175d), 1d, 0d, 0d, 1d, ImageHorizontalAlignement.CENTER, Units.ZEROMM,
					ImageVerticalAlignement.BOTTOM, Units.mm(-75d), DEFAULT_HOLDBACKS_Z_INDEX);

			// Display the holdback on the side of the shutter panel
			// zone.addImage(IMAGE_HOLDBACK_HORIZONTAL, Units.mm(175d), Units.mm(50d), 1d, 0d, 1d, 0d, ImageHorizontalAlignement.RIGHT, Units.mm(-75d),
			// ImageVerticalAlignement.CENTER, Units.ZEROMM);
		}
	}
}
