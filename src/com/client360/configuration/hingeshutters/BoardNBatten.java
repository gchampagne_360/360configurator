// $Id: $

package com.client360.configuration.hingeshutters;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.client360.configuration.wad.StandardColor;
import com.netappsid.commonutils.geo.Form;
import com.netappsid.commonutils.geo.NAIDEdge;
import com.netappsid.commonutils.geo.NAIDShape;
import com.netappsid.commonutils.geo.ShapeFunction;
import com.netappsid.commonutils.geo.enums.Orientation;
import com.netappsid.commonutils.math.MathFunction;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.hingeshutters.ShutterBoard;
import com.netappsid.configuration.hingeshutters.enums.ChoiceSide;
import com.netappsid.rendering.common.advanced.Profile;


public class BoardNBatten extends com.client360.configuration.hingeshutters.BoardNBattenBase
{
	private static final long serialVersionUID = 3047118217664672451L;

	private static final int BATTEN_Z_INDEX = 4;
	private static final String BOARD_N_BATTEN_REPLICATION_PREFIX = "BoardNBatten";

	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
    public BoardNBatten(com.netappsid.erp.configurator.Configurable parent)
    {
        super(parent);
    }

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		setBattenColor(instanciateConfigurable(StandardColor.class, this));

		makeReadOnly(PROPERTYNAME_BATTENCOLOR);
	}
    
    // Add custom overrides here

	@Override
	public void managePropertiesReplication(boolean publish, String prefix)
	{
		super.managePropertiesReplication(publish, prefix);

		String propertyPrefix = BOARD_N_BATTEN_REPLICATION_PREFIX + prefix == null ? "" : prefix;

		managePropertyReplication(publish, PROPERTYNAME_NBBATTENS, propertyPrefix);
		managePropertyReplication(publish, PROPERTYNAME_ZBARS, propertyPrefix);
		managePropertyReplication(publish, PROPERTYNAME_ZBARWIDTH, propertyPrefix);
		managePropertyReplication(publish, PROPERTYNAME_ZBAROFFSETTOP, propertyPrefix);
		managePropertyReplication(publish, PROPERTYNAME_ZBAROFFSETSILL, propertyPrefix);
		managePropertyReplication(publish, PROPERTYNAME_RECESS, propertyPrefix);
		managePropertyReplication(publish, PROPERTYNAME_RECESSANGLE, propertyPrefix);
		managePropertyReplication(publish, PROPERTYNAME_RECESSLENGTH, propertyPrefix);
		managePropertyReplication(publish, PROPERTYNAME_BATTENSMIDOFFSET, propertyPrefix);
		managePropertyReplication(publish, PROPERTYNAME_BATTENWIDTH, propertyPrefix);
		managePropertyReplication(publish, PROPERTYNAME_BATTENGAPTOP, propertyPrefix);
		managePropertyReplication(publish, PROPERTYNAME_BATTENGAPLEFT, propertyPrefix);
		managePropertyReplication(publish, PROPERTYNAME_BATTENGAPRIGHT, propertyPrefix);
		managePropertyReplication(publish, PROPERTYNAME_BATTENGAPSILL, propertyPrefix);
		managePropertyReplication(publish, PROPERTYNAME_BATTENPROFILE, propertyPrefix);
		managePropertyReplication(publish, PROPERTYNAME_BATTENCOLOR, propertyPrefix);
	}

	@Override
	protected List<ShutterBoard> createBoards()
	{
		List<ShutterBoard> result = super.createBoards();

		result.addAll(createBattens());

		return result;
	}

	protected Profile getBattenProfileInstance()
	{
		Profile result = null;

		if (getBattenProfile() != null)
		{
			try
			{
				result = getBattenProfile().profileClass.newInstance();
			}
			catch (InstantiationException e)
			{
				logger.error(e, e);
			}
			catch (IllegalAccessException e)
			{
				logger.error(e, e);
			}
		}
		return result;
	}

	private int getBattenZIndex()
	{
		return getBoardZIndex() + BATTEN_Z_INDEX;
	}

	private List<ShutterBoard> createBattens()
	{
		List<ShutterBoard> result = new ArrayList<ShutterBoard>();
		double battenWidth = getSafeMeasure(getBattenWidth()).doubleValue(Units.INCH);

		if (getSafeInteger(getNbBattens()) > 0 && MathFunction.greater(battenWidth, 0d, ShapeFunction.DELTA))
		{
			Form f = getFormOffset();

			double battenOffsetTop = getSafeMeasure(getBattenGapTop()).doubleValue(Units.INCH);
			double battenOffsetSill = getSafeMeasure(getBattenGapSill()).doubleValue(Units.INCH);

			double battenOffsetLeft = getSafeMeasure(getBattenGapLeft()).doubleValue(Units.INCH);
			double battenOffsetRight = getSafeMeasure(getBattenGapRight()).doubleValue(Units.INCH);

			double minX = f.getOrigin().getX();
			double minY = f.getOrigin().getY();

			List<Double> cuts = new ArrayList<Double>();

			int shapeIndex = 0;

			if (MathFunction.greater(battenOffsetLeft, 0d, ShapeFunction.DELTA))
			{
				++shapeIndex;
				cuts.add(battenOffsetLeft + minX);
			}
			if (MathFunction.greater(battenOffsetRight, 0d, ShapeFunction.DELTA))
			{
				cuts.add(f.getWidth() - battenOffsetRight + minX);
			}

			Form cutForm = split(f, Orientation.VERTICAL, cuts).get(shapeIndex);

			cuts.clear();
			shapeIndex = 0;
			if (MathFunction.greater(battenOffsetTop, 0d, ShapeFunction.DELTA))
			{
				++shapeIndex;
				cuts.add(battenOffsetTop);
			}
			if (MathFunction.greater(battenOffsetSill, 0d, ShapeFunction.DELTA))
			{
				cuts.add(f.getHeight() - battenOffsetSill + minY);
			}

			Form battenWorkForm = split(cutForm, Orientation.HORIZONTAL, cuts).get(shapeIndex);

			result.addAll(addBattens(battenWorkForm));
		}

		return result;
	}

	private List<ShutterBoard> addBattens(Form battenWorkForm)
	{
		List<ShutterBoard> result = new ArrayList<ShutterBoard>();
		int nbBattens = getSafeInteger(getNbBattens());

		List<Double> cuts = new ArrayList<Double>();

		double battenFormHeight = battenWorkForm.getHeight();
		double battenWidth = getSafeMeasure(getBattenWidth()).doubleValue(Units.INCH);
		double battenSize = nbBattens * battenWidth;

		double battenMidOffset = getSafeMeasure(getBattensMidOffset()).doubleValue(Units.INCH);

		double remainingSize = battenFormHeight - battenSize;

		double minY = battenWorkForm.getOrigin().getY();
		double accumulator = minY;

		if (nbBattens == 1)
		{
			accumulator += remainingSize * 0.5d - battenMidOffset;
			cuts.add(accumulator);
			accumulator += battenSize;
			cuts.add(accumulator);
		}
		else
		{
			double spaceBetweenBatten = remainingSize / (nbBattens - 1);

			for (int i = 0; i < nbBattens - 1; ++i)
			{
				if (i == 0)
				{
					accumulator += battenWidth;
					cuts.add(accumulator);
					accumulator += spaceBetweenBatten - battenMidOffset;
					cuts.add(accumulator);
				}
				else if (i == nbBattens - 1)
				{
					accumulator += battenWidth;
					cuts.add(accumulator);
					accumulator += spaceBetweenBatten;
					cuts.add(accumulator);
				}
				else
				{
					accumulator += battenWidth;
					cuts.add(accumulator);
					accumulator += spaceBetweenBatten + battenMidOffset;
					cuts.add(accumulator);
				}
			}
		}

		List<Form> battenCuts = split(battenWorkForm, Orientation.HORIZONTAL, cuts);

		Profile battenProfile = getBattenProfileInstance();
		
		if(nbBattens == 1)
		{
			result.add(new ShutterBoard(battenCuts.get(1), battenProfile, getBattenColor(), getBattenZIndex()));
		}
		else
		{
			int i = 0;
			for (Form batten : battenCuts)
			{
				// Check if this is a batten
				if ((i++ % 2) == 0)
				{
					result.add(new ShutterBoard(batten, battenProfile, getBattenColor(), getBattenZIndex()));
				}
			}
		}

		if (getSafeBoolean(getZBars()) && result.size() > 1)
		{
			result.addAll(addZBars(result));
		}

		return result;
	}

	private List<ShutterBoard> addZBars(List<ShutterBoard> battens)
	{
		List<ShutterBoard> result = new ArrayList<ShutterBoard>();

		StandardColor zBarColor = (StandardColor) battens.get(0).getColor();
		Profile zBarProfile = battens.get(0).getProfile();
		int zIndex = battens.get(0).getZIndex();

		for(int i = 0; i < battens.size() - 1; ++i)
		{
			NAIDShape firstBatten = battens.get(i).getShape();
			NAIDShape secondBatten = battens.get(i + 1).getShape();

			boolean recess = getSafeBoolean(getRecess());
			boolean mirror = getParent(Panel.class).getChoiceSide().equals(ChoiceSide.RIGHT);

			double topY = firstBatten.getBounds().getMaxY();
			double bottomY = secondBatten.getBounds().getMinY();

			double zBarWidth = calculateZBarOffsetX(getSafeMeasure(getZbarWidth()).doubleValue(Units.INCH), firstBatten.getWidth(), bottomY - topY);

			List<Point2D> points = new ArrayList<Point2D>();
			if(mirror)
			{
				double leftX = firstBatten.getBounds().getMinX() + getSafeMeasure(getZBarOffsetTop()).doubleValue(Units.INCH);
				double rightX = secondBatten.getBounds().getMaxX() - getSafeMeasure(getZBarOffsetSill()).doubleValue(Units.INCH);
				points.add(new Point2D.Double(rightX - zBarWidth, bottomY));
				points.add(new Point2D.Double(leftX, topY));
				points.add(new Point2D.Double(leftX + zBarWidth, topY));
				points.add(new Point2D.Double(rightX, bottomY));

				if (recess)
				{
					addRecess(points, 3, 4, false, mirror);
					addRecess(points, 1, 2, true, mirror);
				}
			}
			else
			{
				double rightX = firstBatten.getBounds().getMaxX() - getSafeMeasure(getZBarOffsetTop()).doubleValue(Units.INCH);
				double leftX = secondBatten.getBounds().getMinX() + getSafeMeasure(getZBarOffsetSill()).doubleValue(Units.INCH);
				points.add(new Point2D.Double(rightX, topY));
				points.add(new Point2D.Double(leftX + zBarWidth, bottomY));
				points.add(new Point2D.Double(leftX, bottomY));
				points.add(new Point2D.Double(rightX - zBarWidth, topY));

				if (recess)
				{
					addRecess(points, 0, 4, false, mirror);
					addRecess(points, 2, 2, true, mirror);
				}
			}
			List<NAIDEdge> edges = ShapeFunction.buildPolygonFromPoints(Arrays.copyOf(points.toArray(), points.size(), Point2D[].class));
			Form zBar = Form.create(false, edges);
			
			result.add(new ShutterBoard(zBar, zBarProfile, zBarColor, zIndex));
		}
		return result;
	}

	/**
	 * Calculate the zBarOffsetX according to the board width.
	 * 
	 * @param width
	 * @return
	 */
	private double calculateZBarOffsetX(double zBarWidth, double width, double height)
	{
		width -= getSafeMeasure(getZBarOffsetSill()).doubleValue(Units.INCH) + getSafeMeasure(getZBarOffsetTop()).doubleValue(Units.INCH);
		double centerX = width * 0.5d;
		double centerY = height * 0.5d;

		double theta = Math.asin((zBarWidth * 0.5d) / Math.sqrt(centerX * centerX + centerY * centerY)) * 2d;

		double diagonaleX = (width - centerX) * Math.cos(theta) - centerY * Math.sin(theta) + centerX;
		double diagonaleY = (width - centerX) * -Math.sin(theta) - centerY * Math.cos(theta) + centerY;

		double slope = (diagonaleY - height) / diagonaleX;
		double firstY = -height;

		double distX = firstY / slope;
		return width - distX;
	}

	/**
	 * Add a point to the Z bar to draw the recess
	 * 
	 * @param points
	 *            List of the points of the Z bar
	 * @param reference
	 *            First point of the base of the triangle of the recess
	 * @param index
	 *            Index where to add the point for the recess
	 * @param top
	 *            Indicate if the third point should be added upper of the two other points
	 */
	private void addRecess(List<Point2D> points, int reference, int insert, boolean top, boolean mirror)
	{
		double recessLength = getSafeMeasure(getRecessLength()).doubleValue(Units.INCH);
		double recessAngle = getSafeDouble(getRecessAngle());
		// Check if the recess should not be added
		if (MathFunction.equals(recessLength, 0d, ShapeFunction.DELTA) || MathFunction.equals(recessAngle, 0d, ShapeFunction.DELTA))
		{
			return;
		}

		Point2D recess = new Point2D.Double();

		double radAngle = recessAngle * 2 * Math.PI / 360d;

		// Add the offset to the new point according to the length of the recess
		recess.setLocation((top ? 1d : -1d) * recessLength, 0);

		// Rotate the point into the batten to create the recess
		recess.setLocation(recess.getX() * Math.cos(radAngle) + (mirror ? 1d : -1d) * recess.getY() * Math.sin(radAngle), (mirror ? -1d : 1d) * recess.getX()
				* Math.sin(radAngle) + recess.getY() * Math.cos(radAngle));

		// Translate the rotated point to the reference point
		recess.setLocation(recess.getX() + points.get(reference).getX(), recess.getY() + points.get(reference).getY());

		points.add(insert, recess);
	}

}
