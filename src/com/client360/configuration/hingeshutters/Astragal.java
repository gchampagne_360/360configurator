// $Id: $

package com.client360.configuration.hingeshutters;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import com.client360.configuration.hingeshutters.enums.HingeShutterPanelProfiles;
import com.client360.configuration.wad.StandardColor;
import com.netappsid.wadconfigurator.Color;


public class Astragal extends com.client360.configuration.hingeshutters.AstragalBase
{
    // Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
    public Astragal(com.netappsid.erp.configurator.Configurable parent)
    {
        super(parent);
    }

	@Override
	protected Class<? extends Color> getColorClass()
	{
		return StandardColor.class;
	}

	@Override
	public void activateListener()
	{
		super.activateListener();

		addAddressedPropertyChangeListener(new PropertyChangeListener()
			{

				@Override
				public void propertyChange(PropertyChangeEvent evt)
				{
					String propertyName = evt.getPropertyName();
					if (propertyName.equals(PROPERTYNAME_HINGESHUTTERPANELPROFILES))
					{
						onChoiceProfileChanged();
					}
				}
			});
	}

	/**
	 * Adjust the visibility of the astragal properties according to the validity of its position
	 */
	@Override
	protected void onChoiceSideChanged()
	{
		super.onChoiceSideChanged();
		boolean visible = getChoiceSide() != null;
		setVisible(PROPERTYNAME_HINGESHUTTERPANELPROFILES, visible);
	}

	private void onChoiceProfileChanged()
	{
		HingeShutterPanelProfiles profile = getHingeShutterPanelProfiles();

		if (profile != null)
		{
			try
			{
				setProfile(profile.profileClass.newInstance());
			}
			catch (InstantiationException e)
			{
				removeProfile();
				logger.error(e, e);
			}
			catch (IllegalAccessException e)
			{
				removeProfile();
				logger.error(e, e);
			}
		}
		else
		{
			removeProfile();
		}
	}
}
