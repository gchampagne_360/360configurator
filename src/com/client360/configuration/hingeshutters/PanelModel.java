// $Id: $

package com.client360.configuration.hingeshutters;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.hingeshutters.enums.PanelModelCornerTypes;
import com.client360.configuration.wad.StandardColor;
import com.netappsid.commonutils.geo.CornerType;
import com.netappsid.commonutils.geo.Form;
import com.netappsid.commonutils.geo.NAIDEdge;
import com.netappsid.commonutils.geo.NAIDShape;
import com.netappsid.commonutils.geo.ShapeFunction;
import com.netappsid.commonutils.math.MathFunction;
import com.netappsid.commonutils.math.Units;
import com.netappsid.configuration.hingeshutters.ShutterBoard;
import com.netappsid.rendering.common.advanced.Profile;
import com.netappsid.rendering.common.part.ComplexForm;
import com.netappsid.rendering.common.part.edge.Edge;

public class PanelModel extends com.client360.configuration.hingeshutters.PanelModelBase
{
	protected static final int DEFAULT_PERIMETER_Z_INDEX = com.netappsid.configuration.hingeshutters.Panel.MODELS_ZLAYER + 25;
	private static final long serialVersionUID = -7489921227523925768L;
	private static final String PANEL_MODEL_REPLICATION_PREFIX = "PanelModel";

	private transient Form formOffset;

	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
    public PanelModel(com.netappsid.erp.configurator.Configurable parent)
    {
        super(parent);
    }

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		setPerimeterColor(new StandardColor(this));
		makeReadOnly(PROPERTYNAME_PERIMETERCOLOR);
	}

	@Override
	public void activateListener()
	{
		super.activateListener();
		addAddressedPropertyChangeListener(new PropertyChangeListener()
			{
				@Override
				public void propertyChange(PropertyChangeEvent evt)
				{
					String propertyName = evt.getPropertyName();
					if (propertyName.equals(PROPERTYNAME_FORM) || propertyName.equals(PROPERTYNAME_OFFSETLEFT) || propertyName.equals(PROPERTYNAME_OFFSETRIGHT)
							|| propertyName.equals(PROPERTYNAME_OFFSETTOP) || propertyName.equals(PROPERTYNAME_OFFSETSILL))
					{
						resetFormOffset();
					}
				}
			});
	}

	// Add custom overrides here

	/**
	 * Manage the replication of the panel model properties
	 * 
	 * @param publish
	 *            Indicate if the properties has to be published (or listened
	 * @param prefix
	 *            Prefix of the published property name
	 */
	@Override
	public void managePropertiesReplication(boolean publish, String prefix)
	{
		String replicationPrefix = PANEL_MODEL_REPLICATION_PREFIX + (prefix == null ? "" : prefix);
		managePropertyReplication(publish, PROPERTYNAME_OFFSETLEFT, replicationPrefix);
		managePropertyReplication(publish, PROPERTYNAME_OFFSETRIGHT, replicationPrefix);
		managePropertyReplication(publish, PROPERTYNAME_OFFSETTOP, replicationPrefix);
		managePropertyReplication(publish, PROPERTYNAME_OFFSETSILL, replicationPrefix);
		managePropertyReplication(publish, PROPERTYNAME_PERIMETERLEFT, replicationPrefix);
		managePropertyReplication(publish, PROPERTYNAME_PERIMETERRIGHT, replicationPrefix);
		managePropertyReplication(publish, PROPERTYNAME_PERIMETERTOP, replicationPrefix);
		managePropertyReplication(publish, PROPERTYNAME_PERIMETERSILL, replicationPrefix);
		managePropertyReplication(publish, PROPERTYNAME_BYZANCETOP, replicationPrefix);
		managePropertyReplication(publish, PROPERTYNAME_BYZANCESILL, replicationPrefix);
		managePropertyReplication(publish, PROPERTYNAME_PERIMETERPROFILE, replicationPrefix);
		managePropertyReplication(publish, PROPERTYNAME_PERIMETERCOLOR, replicationPrefix);
		managePropertyReplication(publish, PROPERTYNAME_PANELMODELCORNERTYPES, replicationPrefix);
	}

	/**
	 * Check if the panel model contains an instance of the model class receive as parameter
	 * 
	 * @param clazz
	 *            Class to verify
	 * @return Indicate if the panel is an instance of the class
	 */
	public boolean containsModel(Class<? extends PanelModel> clazz)
	{
		return getClass().equals(clazz);
	}

	/**
	 * Create the perimeter boards
	 * 
	 * @return Perimeter boards
	 */
	@Override
	protected List<ShutterBoard> createBoards()
	{
		List<ShutterBoard> result = createPerimeter();
		return result;
	}

	/**
	 * Get the section form with the spacer applied
	 * 
	 * @return Form
	 */
	protected Form getFormOffset()
	{
		// Check if the form offset is ready to be calculated
		if (formOffset == null && getForm() != null)
		{
			Form f = getForm();

			// Get the spacers to apply on the form
			Double[] spacers = calculateOffset(f, getMeasure(getOffsetLeft()), getMeasure(getOffsetRight()), getMeasure(getOffsetTop()),
					getMeasure(getOffsetSill()));
			try
			{
				formOffset = ShapeFunction.getShapeOffset(f, spacers);
			}
			catch (IllegalArgumentException e)
			{
				formOffset = f;
				e.printStackTrace();
			}
		}
		return formOffset;
	}

	/**
	 * Get perimeter Z index
	 * 
	 * @return Perimeter z index
	 */
	protected int getPerimeterZIndex()
	{
		return DEFAULT_PERIMETER_Z_INDEX;
	}

	/**
	 * Get the perimeter profile instance
	 * 
	 * @return Perimeter profile
	 */
	protected Profile getPerimeterProfileInstance()
	{
		Profile result = null;
		if (getPerimeterProfile() != null)
		{
			try
			{
				result = getPerimeterProfile().profileClass.newInstance();
			}
			catch (InstantiationException e)
			{
				logger.error(e, e);
			}
			catch (IllegalAccessException e)
			{
				logger.error(e, e);
			}
		}
		return result;
	}

	/**
	 * Create the perimeter edges
	 * 
	 * @return Perimeter edges
	 */
	protected List<ShutterBoard> createPerimeter()
	{
		List<ShutterBoard> result = new ArrayList<ShutterBoard>();
		Form f = getForm();

		// Check if the form is valid
		if (f != null && f.getNbEdges() != null)
		{
			double byzanceTop = getSafeMeasure(getByzanceTop()).doubleValue(Units.INCH);
			double byzanceSill = getSafeMeasure(getByzanceSill()).doubleValue(Units.INCH);

			// Check if this is a byzance perimeter
			if (MathFunction.greater(byzanceTop, 0d, ShapeFunction.DELTA) || MathFunction.greater(byzanceSill, 0d, ShapeFunction.DELTA))
			{
				result.addAll(createByzancePerimeter());
			}
			else
			{
				Double[] perimeterOffset = calculateOffset(f, getMeasure(getPerimeterLeft()), getMeasure(getPerimeterRight()),
						getMeasure(getPerimeterTop()), getMeasure(getPerimeterSill()));

				ComplexForm complexForm = new ComplexForm();
				complexForm.setForm(f);
				complexForm.setSpacer(perimeterOffset);
				complexForm.setCornerType(calculateCornerTypes(f));

				Edge[] perimeterEdges = complexForm.getEdges();

				if (perimeterEdges != null)
				{
					for (Edge e : perimeterEdges)
					{
						if (e == null || e.getShape() == null)
							continue;
						result.add(new ShutterBoard(e.getShape(), getPerimeterProfileInstance(), getPerimeterColor(), getPerimeterZIndex()));
					}
				}
			}
		}
		return result;
	}

	/**
	 * Calculate the spacer array according to the offset receive as parameter
	 * 
	 * @param f
	 *            Form to offset
	 * @param left
	 *            Wanted spacer to the left
	 * @param right
	 *            Wanted spacer to the right
	 * @param top
	 *            Wanted spacer to the top
	 * @param sill
	 *            Wanted spacer to the sill
	 * @return Array of spacers
	 */
	protected Double[] calculateOffset(NAIDShape f, double left, double right, double top, double sill)
	{
		int nbEdges = f.getNbEdges();
		Double[] result = new Double[nbEdges];
		
		// Retrieve the sill index
		int sillIndex = ShapeFunction.getSillIndex(f);

		// Calculate the left index
		int leftIndex = (sillIndex + 1) % nbEdges;

		// Calculate the right index
		int rightIndex = (sillIndex + nbEdges -1) % nbEdges;
		
		// Feed the spacer array
		for (int i = 0; i < nbEdges; ++i)
		{
			if(i == sillIndex)
			{
				result[i] = sill;
			}
			else if(i == leftIndex)
			{
				result[i] = left;
			}
			else if(i == rightIndex)
			{
				result[i] = right;
			}
			else
			{
				result[i] = top;
			}
		}
		return result;
	}

	protected double getMeasure(Measure<Length> m)
	{
		return getSafeMeasure(m).doubleValue(Units.INCH);
	}

	/**
	 * Create the perimeter handling the byzance edges Byzance is only valid with a rectangle panel section
	 * 
	 * @return Perimeter edges
	 */
	private List<ShutterBoard> createByzancePerimeter()
	{
		List<ShutterBoard> result = new ArrayList<ShutterBoard>();
		
		double byzanceTop = getSafeMeasure(getByzanceTop()).doubleValue(Units.INCH);
		double byzanceSill = getSafeMeasure(getByzanceSill()).doubleValue(Units.INCH);

		Rectangle2D bounds = getForm().getBounds();

		double left = bounds.getMinX();
		double perimeterLeft = left + getSafeMeasure(getPerimeterLeft()).doubleValue(Units.INCH);
		double top = bounds.getMinY();
		double perimeterTop = top + getSafeMeasure(getPerimeterTop()).doubleValue(Units.INCH);
		double right = bounds.getMaxX();
		double perimeterRight = right - getSafeMeasure(getPerimeterRight()).doubleValue(Units.INCH);
		double sill = bounds.getMaxY();
		double perimeterSill = sill - getSafeMeasure(getPerimeterSill()).doubleValue(Units.INCH);
		
		// Draw left side
		ShutterBoard edge = createRectangle(left, perimeterLeft, top, sill, false);
		if (edge != null)
		{
			result.add(edge);
		}
		
		// Draw top side
		if (MathFunction.greater(byzanceTop, 0d, ShapeFunction.DELTA) && MathFunction.lower(byzanceTop, perimeterTop - top, ShapeFunction.DELTA))
		{
			edge = createByzanceEdge(perimeterLeft, perimeterRight, top, perimeterTop, false, byzanceTop);
			if (edge != null)
			{
				result.add(edge);
			}
		}
		else
		{
			edge = createRectangle(perimeterLeft, perimeterRight, top, perimeterTop, true);
			if (edge != null)
			{
				result.add(edge);
			}
		}
		
		// Draw right side
		edge = createRectangle(perimeterRight, right, top, sill, false);
		if (edge != null)
		{
			result.add(edge);
		}

		// Draw bottom side
		if (MathFunction.greater(byzanceSill, 0d, ShapeFunction.DELTA) && MathFunction.lower(byzanceSill, sill - perimeterSill, ShapeFunction.DELTA))
		{
			edge = createByzanceEdge(perimeterLeft, perimeterRight, perimeterSill, sill, true, byzanceSill);
			if (edge != null)
			{
				result.add(edge);
			}
		}
		else
		{
			edge = createRectangle(perimeterLeft, perimeterRight, perimeterSill, sill, true);
			if (edge != null)
			{
				result.add(edge);
			}
		}
		return result;
	}

	/**
	 * Create a shutterboard rectangle plank
	 * 
	 * @param left
	 *            Min x
	 * @param right
	 *            Max x
	 * @param top
	 *            Min y
	 * @param sill
	 *            Max y
	 * @param horizontal
	 *            Orientation of the plank
	 * @return
	 */
	private ShutterBoard createRectangle(double left, double right, double top, double sill, boolean horizontal)
	{
		// Check if the plank has invalid dimensions
		if (MathFunction.equals(left, right, ShapeFunction.DELTA) || MathFunction.equals(top, sill, ShapeFunction.DELTA))
		{
			return null;
		}

		Point2D[] points = new Point2D[4];

		int i = 0;

		if (!horizontal)
		{
			++i;
		}
		points[i++] = new Point2D.Double(left, top);
		points[i++] = new Point2D.Double(right, top);
		points[i++] = new Point2D.Double(right, sill);
		points[i++ % points.length] = new Point2D.Double(left, sill);

		return new ShutterBoard(Form.create(false, ShapeFunction.buildPolygonFromPoints(points)), getPerimeterProfileInstance(), getPerimeterColor(),
				getPerimeterZIndex());
	}

	/**
	 * Create a byzance edge
	 * 
	 * @param left
	 *            Offset left
	 * @param right
	 *            Offset right
	 * @param top
	 *            Offset top
	 * @param sill
	 *            Offset sill
	 * @param arcTop
	 *            Offset top
	 * @param byzanceOffset
	 *            Top byzance offset
	 * @return
	 */
	private ShutterBoard createByzanceEdge(double left, double right, double top, double sill, boolean arcTop, double byzanceOffset)
	{
		if (MathFunction.equals(left, right, ShapeFunction.DELTA) || MathFunction.equals(top, sill, ShapeFunction.DELTA))
		{
			return null;
		}

		Line2D lineLeft = new Line2D.Double(left, sill, left, top);
		Line2D lineRight = new Line2D.Double(right, top, right, sill);
	
		NAIDEdge edgeLeft = new NAIDEdge.Line(lineLeft);
		NAIDEdge edgeRight = new NAIDEdge.Line(lineRight);
		
		Form byzance = null;
		List<NAIDEdge> edges = new ArrayList<NAIDEdge>();
	
		// Check if the arc is the top edge
		if(arcTop)
		{
			Point2D topLeft = new Point2D.Double(left, top);
			Point2D topArc = new Point2D.Double(left + (right - left) * 0.5d, top + byzanceOffset);
			Point2D topRight = new Point2D.Double(right, top);
			Line2D lineBottom = new Line2D.Double(right, sill, left, sill);

			edges.add(new NAIDEdge.Line(lineBottom));
			edges.add(edgeLeft);
			edges.add(new NAIDEdge.Arc(topLeft, topRight, ShapeFunction.getCenterPoint(topLeft, topArc, topRight), false));
			edges.add(edgeRight);
		}
		else
		{
			Line2D lineTop = new Line2D.Double(left, top, right, top);
			Point2D bottomLeft = new Point2D.Double(left, sill);
			Point2D bottomArc = new Point2D.Double(left + (right - left) * 0.5d, sill - byzanceOffset);
			Point2D bottomRight = new Point2D.Double(right, sill);

			edges.add(new NAIDEdge.Line(lineTop));
			edges.add(edgeRight);
			edges.add(new NAIDEdge.Arc(bottomRight, bottomLeft, ShapeFunction.getCenterPoint(bottomLeft, bottomArc, bottomRight), false));
			edges.add(edgeLeft);
		}
		byzance = Form.create(edges);
		return new ShutterBoard(byzance, getPerimeterProfileInstance(), getPerimeterColor(), getPerimeterZIndex());
	}

	/**
	 * Determine the corner of the perimeter
	 * 
	 * @param f
	 *            Form
	 * @return Corner array
	 */
	private CornerType[] calculateCornerTypes(Form f)
	{
		int nbEdges = f.getNbEdges();
		CornerType[] result = new CornerType[nbEdges];

		// Get side indexes from the sill
		int sillIndex = ShapeFunction.getSillIndex(f);
		int leftIndex = (sillIndex + 1) % nbEdges;
		int topIndex = (leftIndex + 1) % nbEdges;
		int rightIndex = (sillIndex + nbEdges - 1) % nbEdges;
		
		PanelModelCornerTypes corner = getPanelModelCornerTypes() == null ? PanelModelCornerTypes.HALF_CORNER : getPanelModelCornerTypes();
		boolean horizontal = corner.equals(PanelModelCornerTypes.FULL_HORIZONTAL);
		for(int i = 0; i < nbEdges; ++i)
		{
			if (!corner.equals(PanelModelCornerTypes.HALF_CORNER))
			{
				if (i == sillIndex || i == topIndex)
				{
					result[i] = horizontal ? CornerType.LONG_CORNER : CornerType.SHORT_CORNER;
					continue;
				}
				else if (i == leftIndex || i == rightIndex)
				{
					result[i] = horizontal ? CornerType.SHORT_CORNER : CornerType.LONG_CORNER;
					continue;
				}
			}
			result[i] = CornerType.HALF_CORNER;
		}
		
		return result;
	}

	private void resetFormOffset()
	{
		formOffset = null;

	}
}
