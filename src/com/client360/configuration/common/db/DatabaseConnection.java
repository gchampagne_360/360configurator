package com.client360.configuration.common.db;

import java.sql.SQLException;

import com.netappsid.datamanagement.database.connection.DerbyDatabaseConnectionManager;

public final class DatabaseConnection extends DerbyDatabaseConnectionManager
{
	public static final String CONFIG_DATABASE_PATH = "com/client360/configuration/common/database/files";

	public DatabaseConnection() throws SQLException
	{
		this(false);
	}

	public DatabaseConnection(boolean isImport) throws SQLException
	{
		super((isImport ? "resources/" : "") + CONFIG_DATABASE_PATH);
		setCreate(isImport);
		setGenerationDatabaseType(isImport ? DatabaseType.directory : DatabaseType.classpath);
		connect();
	}

	@Override
	public DatabaseType getDatabaseType()
	{
		return DatabaseType.classpath;
	}

	@Override
	public String getDatabaseName()
	{
		return CONFIG_DATABASE_PATH;
	}
}
