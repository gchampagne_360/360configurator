package com.client360.configuration.common.db;

import java.io.File;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.client360.configuration.blinds.global.queries.TableBDD;
import com.netappsid.datamanagement.file.ImportDatabase;
import com.netappsid.datamanagement.file.ImportDatabase.ImportType;

public class ImportExcel
{
	public static String FILENAME_STAINEDGLASS = "stainedGlass";
	public static String PATH_EXCEL = "excel";
	public static String PATH_DYNAMIC_ENUM = "dynamicEnums/";
	public static String PATH_BLINDS = "com/client360/configuration/blinds/";
	public static String EXCEL_EXTENSION = ".xls";
	public static String FILENAME_STANDARDDIM = "SD_Dimension_STD";
	public static String FILENAME_COLORS = "colors";
	public static String FILENAME_GLASS_OPTIONS = "InsulatedGlassOptions";

	private final ImportDatabase databaseTask;

	public ImportExcel() throws SQLException// BD en m�moire
	{
		long startTime = System.currentTimeMillis();
		DatabaseConnection database = new DatabaseConnection(true); // Cr�aton de la BD
		database.connect();

		databaseTask = new ImportDatabase();
		databaseTask.setConnection(database.getConnection()); // Instantiation du module d'importation

		System.out.println("Starting import excel");
		// Importation
		databaseTask.setType(ImportType.EXACT_EXCEL);

		load();

		System.out.println("End");

		database.shutdown();
		System.out.println("TEMPS TOTAL D'IMPORT : " + Math.round(System.currentTimeMillis() - startTime) + "ms");
	}

	private void load()
	{
		// Import DynamicEnum
		// databaseTask.setType(ImportType.DYNAMICENUM_EXCEL);
		// databaseTask.setWorkSheet("EXPORT_SHEET");

		importer(PATH_DYNAMIC_ENUM + "ChoiceStd", "ChoiceStd", "EXPORT_SHEET_FR");
		importer(PATH_DYNAMIC_ENUM + FILENAME_STAINEDGLASS, FILENAME_STAINEDGLASS, "EXPORT_SHEET");
		importer(PATH_DYNAMIC_ENUM + FILENAME_STAINEDGLASS, "AVAILABILITY_" + FILENAME_STAINEDGLASS, "AVAILABILITY_EXPORT_SHEET");
		importer(PATH_DYNAMIC_ENUM + FILENAME_STAINEDGLASS, "THERMOS", "THERMOS_EXPORT_SHEET");
		importer(PATH_DYNAMIC_ENUM + FILENAME_STAINEDGLASS, "PANELS", "PANELS_EXPORT_SHEET");
		importer(PATH_DYNAMIC_ENUM + FILENAME_STAINEDGLASS, "SLABS", "SLABS_EXPORT_SHEET");
		importer(PATH_DYNAMIC_ENUM + FILENAME_COLORS, FILENAME_COLORS, "colors_export_sheet");

		// BLINDS

		importer(PATH_BLINDS + "dynamicenum/coloris", TableBDD.COLORIS, "EXPORT");

		// ------------------------------------------------------------------------------------------//
		// ----------------------------------------ENROULEURS----------------------------------------//
		// ------------------------------------------------------------------------------------------//
		// Table des couleurs pour les enrouleurs
		importer(PATH_BLINDS + "dynamicenum/enrouleur/ENR_couleurs", TableBDD.ENR_COULEURS, "colors_export_sheet");
		// Enrouleur : Liste des toiles
		importer(PATH_BLINDS + "dynamicenum/enrouleur/ENR_tissus", TableBDD.ENR_TISSUS, "Tissus");
		// Enrouleur : Liste des tubes disponibles
		importer(PATH_BLINDS + "dynamicenum/enrouleur/ENR_tubes", TableBDD.ENR_TUBES, "Tubes");
		// Enrouleur : Liste des finitions hautes
		importer(PATH_BLINDS + "dynamicenum/enrouleur/ENR_finition_haute", TableBDD.ENR_FINITION_HAUTE, "FinitionHaute");
		// Enrouleur : Liste des finitions basses
		importer(PATH_BLINDS + "dynamicenum/enrouleur/ENR_finition_basse", TableBDD.ENR_FINITION_BASSE, "FinitionBasse");
		// Enrouleur : Liste des finitions des barres de charge
		importer(PATH_BLINDS + "dynamicenum/enrouleur/ENR_bdc", TableBDD.ENR_BDC, "Bdc");
		// Enrouleur : Liste des types de guidage
		importer(PATH_BLINDS + "dynamicenum/enrouleur/ENR_choix_guidage", TableBDD.ENR_CHOIX_GUIDAGE, "Guidage");
		// Enrouleur : Liste des coupes de profils
		importer(PATH_BLINDS + "tables/enrouleur/ENR_coupe_profils", TableBDD.ENR_COUPE_PROFILS, "CoupeProfils");

		// ------------------------------------------------------------------------------------------//
		// ----------------------------------------VENITIENS-----------------------------------------//
		// ------------------------------------------------------------------------------------------//

		Map<TableBDD, String> mapImportVen = new HashMap<TableBDD, String>();
		mapImportVen.put(TableBDD.VEN_lames, "Lames");
		mapImportVen.put(TableBDD.VEN_boitiers, "Boitier");
		mapImportVen.put(TableBDD.VEN_manoeuvres, "Manoeuvre");
		mapImportVen.put(TableBDD.VEN_couleurs, "Couleurs");
		mapImportVen.put(TableBDD.VEN_glands, "Glands");

		importMultiple(PATH_BLINDS + "dynamicenum/venitien/", mapImportVen);

		// Tables de mariage des couleurs de boitiers, echelles par rapport aux lames
		importer(PATH_BLINDS + "tables/venitien/VEN_mariage_couleurs", TableBDD.VEN_MARIAGE_COULEURS, "Couleurs");

		// Tables des propri�tes de lames
		importer(PATH_BLINDS + "tables/venitien/VEN_lames_proprietes", TableBDD.VEN_lames_proprietes, "Lames");

		// Table nbEchelles/nbPerforations/nbCordons
		importer(PATH_BLINDS + "tables/venitien/VEN_echelles", TableBDD.VEN_ECHELLES, "Echelles");

		// ------------------------------------------------------------------------------------------//
		// -----------------------------------------VERTICAUX----------------------------------------//
		// ------------------------------------------------------------------------------------------//
		// Couleurs pour les verticaux
		importer(PATH_BLINDS + "dynamicenum/vertical/VER_couleurs", TableBDD.VER_COULEURS, "colors_export_sheet");
		// Table des tissus pour les verticaux
		importer(PATH_BLINDS + "dynamicenum/vertical/VER_tissus_proprietes", TableBDD.VER_TISSUS_PROPRIETES, "Tissus");
	}

	private void importMultiple(String path, Map<TableBDD, String> mapNomFichierEtOnglet)
	{
		if (path != null && mapNomFichierEtOnglet != null)
		{
			for (TableBDD nomFichier : mapNomFichierEtOnglet.keySet())
			{
				if (nomFichier != null && mapNomFichierEtOnglet.get(nomFichier) != null)
				{
					importer(path + nomFichier.name(), nomFichier.name().toUpperCase(), mapNomFichierEtOnglet.get(nomFichier));
				}
			}
		}
	}

	private void importer(String nomFichier, String nomTable, String nomFeuille, boolean drop)
	{
		long startTime = System.currentTimeMillis();
		System.out.println("IMPORT DE " + nomFichier + ".xls");
		System.out.println("WORKSHEET >" + nomFeuille + "<" + " | TABLE NAME >" + nomTable + "<");

		databaseTask.load(nomFichier + ".xls", this);
		databaseTask.clearTableAndWorkSheet();
		databaseTask.addTableAndWorkSheet(nomTable, nomFeuille);
		databaseTask.setDropTable(drop);
		databaseTask.execute();

		System.out.println("Temps : " + Math.round(System.currentTimeMillis() - startTime) + " ms");
	}

	private void importer(String nomFichier, String nomTable, String nomFeuille)
	{
		importer(nomFichier, nomTable, nomFeuille, true);
	}

	private void importer(String nomFichier, TableBDD nomTable, String nomFeuille)
	{
		if (nomTable != null)
		{
			importer(nomFichier, nomTable.name(), nomFeuille);
		}
	}

	public static void delete()
	{
		if (deleteDir("resources/" + DatabaseConnection.CONFIG_DATABASE_PATH))
		{
			System.out.println("Old DB deleted");
		}
		else
		{
			System.out.println("Old DB delete failed");
		}

		if (deleteDir("target/classes/" + DatabaseConnection.CONFIG_DATABASE_PATH))
		{
			System.out.println("Old DB deleted");
		}
		else
		{
			System.out.println("Old DB delete failed");
		}
	}

	public static boolean deleteDir(File dir)
	{
		if (dir.isDirectory())
		{
			String[] children = dir.list();
			for (String element : children)
			{
				deleteDir(new File(dir, element));
			}
		}

		// The directory is now empty so delete it
		dir.delete();

		if (dir.exists())
		{
			return false;
		}

		return true;
	}

	public static boolean deleteDir(String pathDir)
	{
		System.out.println("Deleting : " + pathDir);
		File dir = new File(pathDir);
		return deleteDir(dir);
	}

	public static void main(String[] args) throws Exception // BD incluse dans le jar de Configuration
	{
		delete();
		new ImportExcel();
	}

}
