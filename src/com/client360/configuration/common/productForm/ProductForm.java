package com.client360.configuration.common.productForm;

import info.clearthought.layout.TableLayout;
import info.clearthought.layout.TableLayoutConstraints;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import net.miginfocom.swing.MigLayout;

import com.netappsid.configuration.common.productForm.JButton360;
import com.netappsid.configuration.common.productForm.ProductFormPanel;
import com.netappsid.rendering.common.productform.ProductFormRendering;

/**
 * @author Jean-Daniel Daigle
 */
public class ProductForm extends ProductFormPanel
{
	private JButton button7;
	private JButton jButtonBvSection3_noPG;
	public ProductForm()
	{
		initComponents();

	}

	private void initComponents()
	{
		// JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
		jLabelLogoA = new JLabel();
		jideTabbedPaneMain = new JTabbedPane();
		jtabbedWindows2 = new JPanel();
		jButtonBvSection1 = new JButton()
			{
				@Override
				public void paint(Graphics g)
				{
					super.paint(g);

					ProductFormRendering.drawImageOnButton(g, this.getBounds(), "com/client360/configuration/common/images/productform/battant.jpg");
					ProductFormRendering.drawStringOnButtonBottom(g, this.getBounds(), "Battant");
				}

				{
					addMouseListener(new MouseAdapter()
						{
							@Override
							public void mouseReleased(MouseEvent e)
							{
								addConfigurable("CVH");

								launchConfigurator();
							}

						});
				}
			};
		button7 = new JButton()
			{
				Integer nbSections = 1;

				String text = "Forme";

				@Override
				public void paint(Graphics g)
				{
					super.paint(g);

					ProductFormRendering.drawImageOnButton(g, this.getBounds(), "com/client360/configuration/common/images/forme.png");
					ProductFormRendering.drawStringOnButtonBottom(g, this.getBounds(), "Forme");
				}

				{
					addMouseListener(new MouseAdapter()
						{
							@Override
							public void mouseReleased(MouseEvent e)
							{

							}

						});
				}
			};
		jButtonBvSection2 = new JButton()
			{
				@Override
				public void paint(Graphics g)
				{
					super.paint(g);

					ProductFormRendering.drawImageOnButton(g, this.getBounds(), "com/client360/configuration/common/images/productform/battantadvancedrendering.png");
					ProductFormRendering.drawStringOnButtonBottom(g, this.getBounds(), "Battant");
				}

				{
					addMouseListener(new MouseAdapter()
						{
							@Override
							public void mouseReleased(MouseEvent e)
							{
								addConfigurable("CVP");
								launchConfigurator();
							}

						});
				}
			};
		jButtonBvSection3 = new JButton()
			{
				@Override
				public void paint(Graphics g)
				{
					super.paint(g);

					ProductFormRendering.drawImageOnButton(g, this.getBounds(), "com/client360/configuration/common/images/productform/VR.png");
					ProductFormRendering.drawStringOnButtonBottom(g, this.getBounds(), "Volet Roulant");
				}

				{
					addMouseListener(new MouseAdapter()
						{
							@Override
							public void mouseReleased(MouseEvent e)
							{
								addConfigurable("RSM");

								// ((ProductGroup) getCurrentProduct()).setDimension(new com.exemple.configurator.Dimension(getCurrentProduct()));
								// ((ProductGroup) getCurrentProduct()).getDimension().setForm(Form.create("RECTANGLE_N"));
								// ((ProductGroup) getCurrentProduct()).getDimension().setHeight(Units.mm(100.0));

								launchConfigurator();
							}

						});
				}
			};
		jButtonBvSection3_noPG = new JButton()
			{
				@Override
				public void paint(Graphics g)
				{
					super.paint(g);

					ProductFormRendering.drawImageOnButton(g, this.getBounds(), "com/client360/configuration/common/images/productform/VR.png");
					ProductFormRendering.drawStringOnButtonBottom(g, this.getBounds(), "Volet Roulant (sans PG)");
				}

				{
					addMouseListener(new MouseAdapter()
						{
							@Override
							public void mouseReleased(MouseEvent e)
							{
								addConfigurable("RSM_WOA");

								// ((ProductGroup) getCurrentProduct()).setDimension(new com.client360.configuration.wad.Dimension(getCurrentProduct()));
								// ((ProductGroup) getCurrentProduct()).getDimension().setForm(Form.create("RECTANGLE_N"));
								// ((ProductGroup) getCurrentProduct()).getDimension().setHeight(Units.mm(100.0));

								launchConfigurator();
							}

						});
				}
			};
		jButtonBvSection4 = new JButton()
			{
				@Override
				public void paint(Graphics g)
				{
					super.paint(g);

					ProductFormRendering.drawImageOnButton(g, this.getBounds(), "com/client360/configuration/common/images/productform/Slider.jpg");
					ProductFormRendering.drawStringOnButtonBottom(g, this.getBounds(), "Coulissant");
				}

				{
					addMouseListener(new MouseAdapter()
						{
							@Override
							public void mouseReleased(MouseEvent e)
							{
								addConfigurable("SLP");
								launchConfigurator();
							}

						});
				}
			};
		jButtonBvSection5 = new JButton()
			{
				@Override
				public void paint(Graphics g)
				{
					super.paint(g);

					ProductFormRendering.drawImageOnButton(g, this.getBounds(), "com/client360/configuration/common/images/productform/Hung.jpg");
					ProductFormRendering.drawStringOnButtonBottom(g, this.getBounds(), "Guillotine");
				}

				{
					addMouseListener(new MouseAdapter()
						{
							@Override
							public void mouseReleased(MouseEvent e)
							{
								addConfigurable("HUP");
								launchConfigurator();
							}

						});
				}
			};
		jButtonBvSection6 = new JButton()
			{
				@Override
				public void paint(Graphics g)
				{
					super.paint(g);
					ProductFormRendering.drawImageOnButton(g, this.getBounds(), "com/client360/configuration/common/images/productform/voletsBattants.jpg");
					ProductFormRendering.drawStringOnButtonBottom(g, this.getBounds(), "Volets Battants sur gonds");
				}

				{
					addMouseListener(new MouseAdapter()
						{
							@Override
							public void mouseReleased(MouseEvent e)
							{
								addConfigurable("PS");
								launchConfigurator();
							}
						});
				}
			};
		jtabbedDoors = new JPanel();
		button3 = new JButton()
			{
				Integer nbSections = 2;

				@Override
				public void paint(Graphics g)
				{
					super.paint(g);
					ProductFormRendering.drawImageOnButton(g, this.getBounds(), "com/client360/configuration/common/images/productform/porte.png");
					ProductFormRendering.drawStringOnButtonBottom(g, this.getBounds(), "Porte d'entr�e");
				}

				{
					addMouseListener(new MouseAdapter()
						{
							@Override
							public void mouseReleased(MouseEvent e)
							{
								addConfigurable("STD");
								launchConfigurator();
							}

						});
				}
			};

		blindsPanel = new JPanel();
		blindsPanel.setLayout(new MigLayout("", "[grow][grow][grow]", "[grow][grow]"));

		buttonBlindsHorizontal = new JButton360();
		buttonBlindsHorizontal.setPanel(this);
		buttonBlindsHorizontal.setIcon(new ImageIcon(ProductForm.class.getResource("/com/client360/configuration/common/images/productform/venitienh.png")));
		buttonBlindsHorizontal.setText(" ");
		buttonBlindsHorizontal.setQuickLib("HORIZONTAL");
		blindsPanel.add(buttonBlindsHorizontal, "cell 0 0,grow");

		buttonBlindsVertical = new JButton360();
		buttonBlindsVertical.setText(" ");
		buttonBlindsVertical.setPanel(this);
		buttonBlindsVertical.setQuickLib("VERTICAL");
		buttonBlindsVertical.setIcon(new ImageIcon(ProductForm.class.getResource("/com/client360/configuration/common/images/productform/venitienv.png")));
		blindsPanel.add(buttonBlindsVertical, "cell 1 0,grow");

		buttonBLindsRoller = new JButton360();
		buttonBLindsRoller.setPanel(this);
		buttonBLindsRoller.setIcon(new ImageIcon(ProductForm.class.getResource("/com/client360/configuration/common/images/productform/enrouleur.png")));
		buttonBLindsRoller.setText(" ");
		buttonBLindsRoller.setQuickLib("ROLLER");
		blindsPanel.add(buttonBLindsRoller, "cell 2 0,grow");
		// JFormDesigner - End of component initialization //GEN-END:initComponents

		// Ajout MPA pour Negoce - 17/07/2013
		buttonBlindsPleated = new JButton360();
		buttonBlindsPleated.setPanel(this);
		buttonBlindsPleated.setIcon(new ImageIcon(ProductForm.class.getResource("/com/client360/configuration/common/images/productform/plisses.png")));
		buttonBlindsPleated.setText(" ");
		buttonBlindsPleated.setQuickLib("PLEATED");
		blindsPanel.add(buttonBlindsPleated, "cell 0 1,grow");

		// Ajout MPA pour Negoce - 13/09/2013
		buttonBlindsRoman = new JButton360();
		buttonBlindsRoman.setPanel(this);
		buttonBlindsRoman.setIcon(new ImageIcon(ProductForm.class.getResource("/com/client360/configuration/common/images/productform/bateau.png")));
		buttonBlindsRoman.setText(" ");
		buttonBlindsRoman.setQuickLib("ROMAN");
		blindsPanel.add(buttonBlindsRoman, "cell 1 1,grow");

		// Ajout MPA pour Negoce - 13/09/2013
		buttonBlindsJapanese = new JButton360();
		buttonBlindsJapanese.setPanel(this);
		buttonBlindsJapanese.setIcon(new ImageIcon(ProductForm.class.getResource("/com/client360/configuration/common/images/productform/veranda.png")));
		buttonBlindsJapanese.setText(" ");
		buttonBlindsJapanese.setQuickLib("JAPANESE");
		blindsPanel.add(buttonBlindsJapanese, "cell 2 1,grow");

		jtabbedForms = new JPanel();
		// Icons are in the WAD resources.
		formsButtons[0] = createButton("/images/icoForms/ElongatedArc.png", "Arc allong�", "F_EA");
		formsButtons[1] = createButton("/images/icoForms/Cathedrale.png", "Cath�drale", "F_C");
		formsButtons[2] = createButton("/images/icoForms/DogHouse.png", "Niche � chien", "F_DH");
		formsButtons[3] = createButton("/images/icoForms/Rectangle.png", "Rectangle", "F_R");
		formsButtons[4] = createButton("/images/icoForms/Trapeze.png", "Trap�ze", "F_T");
		formsButtons[5] = createButton("/images/icoForms/IsoTriangle.png", "Triangle", "F_IT");
		formsButtons[6] = createButton("/images/icoForms/ElongatedHalfArc.png", "Demi arc allong�", "F_EHA");
		formsButtons[7] = createButton("/images/icoForms/QuaterCircle.png", "Quart de cercle", "F_QC");
		formsButtons[8] = createButton("/images/icoForms/Ellipse.png", "Ellipse", "F_ELL");
		formsButtons[9] = createButton("/images/icoForms/Hexagone.png", "Hexagone", "F_HEX");
		formsButtons[10] = createButton("/images/icoForms/HexagoneIrregular.png", "Hexagone irr�gulier", "F_IRRHEX");
		formsButtons[11] = createButton("/images/icoForms/Circle.png", "Cercle", "F_CIR");
		formsButtons[12] = createButton("/images/icoForms/Octagone.png", "Octogone r�gulier", "F_OCTREG");
		formsButtons[13] = createButton("/images/icoForms/OctagoneIrregular.png", "Octogone cot�s 45 �gaux", "F_OCT45EQU");
		formsButtons[14] = createButton("/images/icoForms/OctagoneIrregular.png", "Octogone cot�s 45", "F_OCT45");
		formsButtons[15] = createButton("/images/icoForms/OctagoneIrregular.png", "Octogone irr�gulier", "F_OCTIRR");
		formsButtons[16] = createButton("/images/icoForms/HalfEllipse.png", "Demi ellipse", "F_HELL");
		formsButtons[17] = createButton("/images/icoForms/TriangleRec.png", "Triangle rectangle", "F_TR");
		formsButtons[18] = createButton("/images/icoForms/RecCornerCut.png", "Rec coin coup�", "F_RCC");
		formsButtons[19] = createButton("/images/icoForms/RecDoubleCornerCut.png", "Rec 2 coins coup�", "F_RDCC");

		jtabbedAssembly = new JPanel();
		assemblyButtons[0] = createButton("", "Chibooki", "F_CHIBOOKI");
		assemblyButtons[1] = createButton("", "Fus�e", "F_ROCKET");
		assemblyButtons[2] = createButton("", "Arc Allong�", "F_ARC_01");
		assemblyButtons[3] = createButton("", "Mur", "F_WALL_01");

		// ======== this ========
		setLayout(new TableLayout(new double[][] { { 150, 1, TableLayout.PREFERRED, TableLayout.FILL }, { TableLayout.FILL } }));

		// ---- jLabelLogoA ----
		jLabelLogoA.setHorizontalAlignment(SwingConstants.CENTER);
		jLabelLogoA.setIcon(new ImageIcon(getClass().getResource("/com/client360/configuration/common/images/productform/product360.png")));
		jLabelLogoA.setBackground(Color.black);
		jLabelLogoA.setOpaque(true);
		jLabelLogoA.setBorder(LineBorder.createBlackLineBorder());
		add(jLabelLogoA, new TableLayoutConstraints(0, 0, 0, 0, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

		// ======== jideTabbedPaneMain ========
		{

			// ======== panel2: WINDOWS ========
			{
				jtabbedWindows2.setLayout(new TableLayout(new double[][] { { TableLayout.FILL, TableLayout.FILL, TableLayout.FILL },
						{ TableLayout.FILL, TableLayout.FILL, TableLayout.FILL } }));
				((TableLayout) jtabbedWindows2.getLayout()).setHGap(5);
				((TableLayout) jtabbedWindows2.getLayout()).setVGap(5);
				jtabbedWindows2.add(jButtonBvSection1, new TableLayoutConstraints(0, 0, 0, 0, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
				// jtabbedWindows2.add(button7, new TableLayoutConstraints(1, 0, 1, 0, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
				jtabbedWindows2.add(jButtonBvSection2, new TableLayoutConstraints(2, 0, 2, 0, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
				jtabbedWindows2.add(jButtonBvSection3, new TableLayoutConstraints(0, 1, 0, 1, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
				jtabbedWindows2.add(jButtonBvSection3_noPG, new TableLayoutConstraints(1, 1, 1, 1, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
				jtabbedWindows2.add(jButtonBvSection4, new TableLayoutConstraints(1, 0, 1, 0, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
				jtabbedWindows2.add(jButtonBvSection5, new TableLayoutConstraints(2, 1, 2, 1, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
				jtabbedWindows2.add(jButtonBvSection6, new TableLayoutConstraints(0, 2, 0, 2, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
			}
			jideTabbedPaneMain.addTab("Fen\u00eatres", jtabbedWindows2);

			// ======== panel3: DOORS ========
			{
				jtabbedDoors.setLayout(new TableLayout(new double[][] { { TableLayout.FILL, TableLayout.FILL }, { TableLayout.FILL, TableLayout.FILL } }));
				((TableLayout) jtabbedDoors.getLayout()).setHGap(5);
				((TableLayout) jtabbedDoors.getLayout()).setVGap(5);
				jtabbedDoors.add(button3, new TableLayoutConstraints(0, 0, 0, 0, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
			}
			jideTabbedPaneMain.addTab("Portes", jtabbedDoors);

			// ======== panel4: FORMS ========
			{
				jtabbedForms.setLayout(new TableLayout(new double[][] {
						{ TableLayout.FILL, TableLayout.FILL, TableLayout.FILL, TableLayout.FILL, TableLayout.FILL },
						{ TableLayout.FILL, TableLayout.FILL, TableLayout.FILL, TableLayout.FILL, TableLayout.FILL } }));
				((TableLayout) jtabbedForms.getLayout()).setHGap(5);
				((TableLayout) jtabbedForms.getLayout()).setVGap(5);

				jtabbedForms.add(formsButtons[0], new TableLayoutConstraints(0, 0, 0, 0, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
				jtabbedForms.add(formsButtons[1], new TableLayoutConstraints(1, 0, 1, 0, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
				jtabbedForms.add(formsButtons[2], new TableLayoutConstraints(2, 0, 2, 0, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
				jtabbedForms.add(formsButtons[3], new TableLayoutConstraints(0, 1, 0, 1, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
				jtabbedForms.add(formsButtons[4], new TableLayoutConstraints(1, 1, 1, 1, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
				jtabbedForms.add(formsButtons[5], new TableLayoutConstraints(2, 1, 2, 1, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
				jtabbedForms.add(formsButtons[6], new TableLayoutConstraints(0, 2, 0, 2, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
				jtabbedForms.add(formsButtons[7], new TableLayoutConstraints(1, 2, 1, 2, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
				jtabbedForms.add(formsButtons[8], new TableLayoutConstraints(2, 2, 2, 2, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
				jtabbedForms.add(formsButtons[9], new TableLayoutConstraints(3, 0, 3, 0, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
				jtabbedForms.add(formsButtons[10], new TableLayoutConstraints(3, 1, 3, 1, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
				jtabbedForms.add(formsButtons[11], new TableLayoutConstraints(3, 2, 3, 2, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
				jtabbedForms.add(formsButtons[12], new TableLayoutConstraints(0, 3, 0, 3, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
				jtabbedForms.add(formsButtons[13], new TableLayoutConstraints(1, 3, 1, 3, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
				jtabbedForms.add(formsButtons[14], new TableLayoutConstraints(2, 3, 2, 3, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
				jtabbedForms.add(formsButtons[15], new TableLayoutConstraints(3, 3, 3, 3, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
				jtabbedForms.add(formsButtons[16], new TableLayoutConstraints(4, 0, 4, 0, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
				jtabbedForms.add(formsButtons[17], new TableLayoutConstraints(4, 1, 4, 1, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
				jtabbedForms.add(formsButtons[18], new TableLayoutConstraints(4, 2, 4, 2, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
				jtabbedForms.add(formsButtons[19], new TableLayoutConstraints(4, 3, 4, 3, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
			}
			jideTabbedPaneMain.addTab("Formes", jtabbedForms);

			// ======== panel4: Assembly ========
			{
				jtabbedAssembly.setLayout(new TableLayout(new double[][] { { TableLayout.FILL, TableLayout.FILL, TableLayout.FILL },
						{ TableLayout.FILL, TableLayout.FILL, TableLayout.FILL } }));
				((TableLayout) jtabbedForms.getLayout()).setHGap(5);
				((TableLayout) jtabbedForms.getLayout()).setVGap(5);

				jtabbedAssembly.add(assemblyButtons[0], new TableLayoutConstraints(0, 0, 0, 0, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
				jtabbedAssembly.add(assemblyButtons[1], new TableLayoutConstraints(1, 0, 1, 0, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
				jtabbedAssembly.add(assemblyButtons[2], new TableLayoutConstraints(2, 0, 2, 0, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
				jtabbedAssembly.add(assemblyButtons[3], new TableLayoutConstraints(0, 1, 0, 1, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
			}
			jideTabbedPaneMain.addTab("Assemblages", jtabbedAssembly);

			// ======== panel4: Blinds ========
			jideTabbedPaneMain.addTab("Stores", blindsPanel);
		}
		add(jideTabbedPaneMain, new TableLayoutConstraints(3, 0, 3, 0, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
		
		garageDoorPanel = new JPanel();
		jideTabbedPaneMain.addTab("Portes de garage", null, garageDoorPanel, null);
		
		btnGarageDoor = new JButton(){
			@Override
			protected void paintComponent(Graphics g)
			{
				super.paintComponent(g);
				ProductFormRendering.drawImageOnButton(g, this.getBounds(), "com/client360/configuration/common/images/productform/garage.png");
				ProductFormRendering.drawStringOnButtonBottom(g, this.getBounds(), "Garage door");
			}
		};
		btnGarageDoor.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addConfigurable("PGM");
				launchConfigurator();
			}
		});
		garageDoorPanel.setLayout(new GridLayout(2, 2, 5, 5));
		garageDoorPanel.add(btnGarageDoor);
		
		label_1 = new JLabel("");
		garageDoorPanel.add(label_1);
		
		label = new JLabel("");
		garageDoorPanel.add(label);

	}

	private JButton360 createButton(String imageName, String text, String quicklib)
	{
		JButton360 button = new JButton360();
		Icon image = new ImageIcon(ProductForm.class.getResource(imageName));
		button.setIcon(image);
		button.setText(text);
		button.setQuickLib(quicklib);
		button.setPanel(this);

		return button;
	}

	private JPanel jtabbedForms;
	private final JButton[] formsButtons = new JButton[20];
	private JPanel jtabbedAssembly;
	private final JButton[] assemblyButtons = new JButton[20];

	// JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
	private JLabel jLabelLogoA;
	private JTabbedPane jideTabbedPaneMain;
	private JPanel jtabbedWindows2;
	private JButton jButtonBvSection1;
	private JButton jButtonBvSection2;
	private JButton jButtonBvSection3;
	private JButton jButtonBvSection4;
	private JButton jButtonBvSection5;
	private JButton jButtonBvSection6;
	private JPanel jtabbedDoors;
	private JButton button3;

	// Blinds
	private JPanel blindsPanel;
	private JButton360 buttonBlindsVertical;
	private JButton360 buttonBlindsHorizontal;
	private JButton360 buttonBLindsRoller;
	private JButton360 buttonBlindsPleated;
	private JButton360 buttonBlindsRoman;
	private JButton360 buttonBlindsJapanese;
	private JPanel garageDoorPanel;
	private JButton btnGarageDoor;
	private JLabel label;
	private JLabel label_1;
	// JFormDesigner - End of variables declaration //GEN-END:variables
}
