// $Id: $

package com.exemple.configurator;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.Dimension;
import com.client360.configuration.wad.StandardColor;
import com.netappsid.commonutils.geo.CornerType;
import com.netappsid.commonutils.geo.Form;
import com.netappsid.commonutils.geo.IdentifiableShape.Attribute;
import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.Color;
import com.netappsid.wadconfigurator.SlabElement;
import com.netappsid.wadconfigurator.enums.ChoiceGarageDoorType;

public class GarageDoor extends com.exemple.configurator.GarageDoorBase
{
	// Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
	public GarageDoor(com.netappsid.erp.configurator.Configurable parent)
	{
		super(parent);
	}

	@Override
	public void setDefaultValues()
	{
		setSharedProperty("DEBUGMODE", true);

		super.setDefaultValues();

		setDefaultNbSection(4);
		setDefaultChoiceGarageDoorType(ChoiceGarageDoorType.SECTIONAL_HORIZONTAL);

		getDimension().makeInvisible(Dimension.PROPERTYNAME_HEIGHTBRICKMOULD);
		getDimension().makeInvisible(Dimension.PROPERTYNAME_WIDTHBRICKMOULD);
		getDimension().makeInvisible(Dimension.PROPERTYNAME_FORM);

		getDimension().setHeight(Units.inch(84));
		getDimension().setWidth(Units.inch(200));
	}

	@Override
	public void afterGarageDoorSectionChanged(com.netappsid.wadconfigurator.GarageDoorSection[] garageDoorSection)
	{
		super.afterGarageDoorSectionChanged(garageDoorSection);

		Form sectionOverAllDimension = getSectionOverAllDimension(Units.MM);
		Measure<Length> width = Units.mm(sectionOverAllDimension.getWidth() / 4);
		Measure<Length> height = Units.mm(sectionOverAllDimension.getHeight() * 0.98);
		Measure<Length> posx = Units.mm(sectionOverAllDimension.getWidth() / 4);
		createPassDoor(width, height, posx);
	}

	@Override
	public void afterPassDoorChanged(SlabElement passDoor)
	{
		super.afterPassDoorChanged(passDoor);

		// String description = "Simple bronze";
		String path = "/com/client360/configuration/wad/images/handle/handle3.png";
		double width = 6.7;
		double height = 12.58;
		double orix = 0.91530055;
		double oriy = 6.66338798;
		Form handleForm = Form.create("RECTANGLE_N");
		handleForm.setValue(Attribute.WIDTH, 17.0);
		handleForm.setValue(Attribute.HEIGHT, 22.0);
		double tr1posx = 3;
		double tr1posy = 35;
		Form holeForm = Form.create("CIRCLE_N");
		holeForm.setValue(Attribute.WIDTH, 1.5);
		double tr2posx = 0;
		double tr2posy = 3;

		createHandleSet(path, Units.inch(width), Units.inch(height), Units.inch(orix), Units.inch(oriy), handleForm, Units.inch(tr1posx), Units.inch(tr1posy));

		addHole(holeForm, Units.inch(tr2posx), Units.inch(tr2posy));
	}

	@Override
	public void beforeHandleSetChanged(SlabElement[] handleSet)
	{
		super.beforeHandleSetChanged(handleSet);

		// XXX weird fix for instanciation problem
		for (SlabElement se : handleSet)
		{
			if (se.getInteriorColor() == null)
			{
				se.setInteriorColor(instanciateConfigurable(StandardColor.class, se));
			}
			if (se.getExteriorColor() == null)
			{
				se.setExteriorColor(instanciateConfigurable(StandardColor.class, se));
			}
		}
	}

	@Override
	public CornerType[] getCornerType()
	{
		return null;
	}

	@Override
	public Measure<Length> getTopSpacer()
	{
		return Units.ZEROMM;
	}

	@Override
	public Measure<Length> getJambWidthLeft()
	{
		return Units.ZEROMM;
	}

	@Override
	public Measure<Length> getJambWidthRight()
	{
		return Units.ZEROMM;
	}

	@Override
	public Measure<Length> getTopSectionOffset()
	{
		return Units.ZEROMM;
	}

	@Override
	public Measure<Length> getSillSectionOffset()
	{
		return Units.ZEROMM;
	}

	@Override
	public Measure<Length> getRightJambSectionOffset()
	{
		return Units.ZEROMM;
	}

	@Override
	public Measure<Length> getLeftJambSectionOffset()
	{
		return Units.ZEROMM;
	}

	@Override
	public Class<? extends GarageDoorSection> getSectionClass()
	{
		return GarageDoorSection.class;
	}

	@Override
	public Measure<Length> getSectionGap(int sectionNumber)
	{
		return Units.ZEROMM;
	}

	@Override
	public CornerType[] getPassDoorCornerType()
	{
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Measure<Length>[] getPassDoorSpacer()
	{
		return new Measure[] { Units.mm(30), Units.mm(30), Units.mm(30), Units.mm(30) };
	}

	@Override
	protected Class<? extends Color> getColorClass()
	{
		return StandardColor.class;
	}

	@Override
	public void init(Object[] params)
	{
		// TODO Auto-generated method stub

	}

	// Add custom overrides here
}
