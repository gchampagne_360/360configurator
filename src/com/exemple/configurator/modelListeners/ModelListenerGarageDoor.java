package com.exemple.configurator.modelListeners;

import java.util.List;

import com.exemple.configurator.GarageDoor;
import com.netappsid.erp.configurator.Configurable;
import com.netappsid.erp.configurator.renderers.RenderingComponent;

public class ModelListenerGarageDoor extends com.netappsid.wadconfigurator.modelListeners.ModelListenerGarageDoor
{

	public ModelListenerGarageDoor(GarageDoor configurable)
	{
		super(configurable);
	}

	public ModelListenerGarageDoor(Configurable configurable, List<RenderingComponent> renderingComponents)
	{
		super(configurable, renderingComponents);
	}

}
