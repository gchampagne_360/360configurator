package com.exemple.configurator.garagedoorModel;

import com.netappsid.advancedrendering.ProfileBoxVT;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageAlign;
import com.netappsid.rendering.common.advanced.SlabElementBackground.ImageScale;
import com.netappsid.wadconfigurator.entranceDoor.ParametricModel;

public class Rectangles extends ParametricModel
{
	@Override
	protected void load()
	{
		final double gapZone = 40/100.0;
		
		Double nbPanels = getParametricMeasure("nbSections");
		Double nbRectangles = getParametricMeasure("nbRectangles");
		if (nbPanels == null || nbRectangles == null)
		{
			throw new IllegalStateException("nbPanels or nbRectangles == null");
		}
		if (nbPanels <= 0 || nbRectangles <= 0)
		{
			throw new IllegalStateException("nbPanels or nbRectangles <= 0");
		}
		double dHeight = hDist(top, sill);
		double vGap = dHeight / nbPanels;
		double dWidth = vDist(right, left);
		double hGap = dWidth / nbRectangles;
		double topGap = (dHeight * gapZone ) / (nbPanels * 2);
		double leftGap = (dWidth * gapZone ) / (nbRectangles * 2);
		
		for (int i = 0; i < nbPanels*2; i+=2)
		{
			addLineDownOffset(i, top, topGap+vGap*(i/2));
			addLineDownOffset(i+1, top, vGap*(i/2 + 1)-topGap);
		}
		addLineDownOffset(nbPanels*2, sill, 0);
		for (int i = 0; i < nbRectangles*2; i+=2)
		{
			addLineRightOffset(i+10000, left, leftGap+hGap*(i/2));
			addLineRightOffset(i+1+10000, left, hGap*(i/2 + 1)-leftGap);
		}
		addLineRightOffset(nbRectangles*2, right, 0);
		
		for (int i = 0; i < nbPanels*2; i+=2)
		{
			for (int j = 10000; j < nbRectangles*2+10000; j+=2)
			{
				addForms("RECTANGLE_N", j, j+1, i, i+1, null, "/com/client360/configuration/wad/images/vitraux/novatech/beethoven/07-008-050-007.png", ImageScale.NONE, ImageAlign.CENTER, true, null);
				addSpacer(2, 2, 2, 2);
				addProfile(new ProfileBoxVT(), new ProfileBoxVT());
			}
		}
	}
}
