// $Id: $

package com.exemple.configurator;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.exemple.configurator.garagedoorModel.Rectangles;
import com.netappsid.commonutils.math.Units;
import com.netappsid.rendering.common.advanced.Drawing;
import com.netappsid.rendering.common.advanced.Profile;
import com.netappsid.wadconfigurator.SlabElement;


public class GarageDoorSection extends com.exemple.configurator.GarageDoorSectionBase
{
    // Default constructor - GENCODE d23c2140-4bfd-11e0-b8af-0800200c9a66
    public GarageDoorSection(com.netappsid.erp.configurator.Configurable parent)
    {
        super(parent);
    }
    
    @Override
    public void setDefaultValues()
    {
    	super.setDefaultValues();
    	
//    	setDoorModel(Empty.class);
		setDoorModel(Rectangles.class);
		getDoorModel().addParametricMeasure("nbRectangles", 4.0);
		getDoorModel().addParametricMeasure("nbSections", 1.0);
		updateComponents();
    }
    
    @Override
    public void afterBgImageChanged(Integer bgImage)
    {
    	super.afterBgImageChanged(bgImage);
    	
    	SlabElement[] panel = getPanel();
    	if (panel != null)
    	{
	    	String image = bgImage == null || bgImage == 0 ? "/com/client360/configuration/wad/images/vitraux/novatech/beethoven/07-008-050-007.png" : "/com/client360/configuration/wad/images/vitraux/novatech/belle_elegance/greygrill/07-024-050-004.png";
	    	
			for (SlabElement p : panel)
	    	{
				if (p.getSlabElementBackground() != null)
				{
					p.getSlabElementBackground().setImagePath(image);
				}
	    	}
    	}
    }
    
    private static final class A extends Profile
	{
		@Override
		protected void load()
		{
			addGradientSegment(10, -100, 0);
			addSpace(20);
		}
	};
    
    private static final class B extends Profile
	{
		@Override
		protected void load()
		{
			addSpace(20);
			addGradientSegment(10, 0, -100);
		}
	};
    
    @Override
    public void afterProfileChanged(Integer profile)
    {
    	super.afterProfileChanged(profile);
    	
    	SlabElement[] panel = getPanel();
    	if (panel != null)
    	{
	    	Profile[] profiles = profile == null || profile == 0 ? new Profile[]{new A(),new A()} : new Profile[]{new B(), new B()};
	    	
			for (SlabElement p : panel)
	    	{
	    		p.setProfiles(profiles);
	    	}
    	}
    }
    
    @Override
    public void afterPanelChanged(SlabElement[] panel)
    {
    	super.afterPanelChanged(panel);
    	
    	afterBgImageChanged(getBgImage());
    	afterProfileChanged(getProfile());
    }

	@Override
	public Measure<Length> getSillGap()
	{
		return Units.ZEROMM;
	}

	@Override
	public Measure<Length> getSlabGap()
	{
		return Units.ZEROMM;
	}

	@Override
	public Drawing getCad()
	{
		return null;
	}
    
    // Add custom overrides here
}
