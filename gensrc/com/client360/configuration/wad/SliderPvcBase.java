// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 


public abstract class SliderPvcBase extends com.client360.configuration.wad.SliderMain
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(SliderPvcBase.class);

    // attributes

    // Bound properties


    // Constructors

    public SliderPvcBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.SliderPvc"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations



    @Override
    protected String getSequences()
    {
        return "nbSections,dimension,choiceOpeningType,choiceOpeningSide,choicePresetWindowWidth,ExteriorColor,InteriorColor,withInsulatedGlass,choiceMosquito,halfExterior,halfInterior,sectionInput,interiorSash,frameDepth,energyStar,installed,qDQ,sameGrilles,showMeetingRail,showMullion,noFrame,windowsMullion,paintable,renderingNumber,section,sash,meetingRail,division";  
    }
}
