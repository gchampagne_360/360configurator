// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.client360.configuration.wad.enums.ChoiceFrameDepth;
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class CasementVisionHybridBase extends com.client360.configuration.wad.CasementMain
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(CasementVisionHybridBase.class);

    // attributes
    private ValueContainer<CasementVisionHybridSash> casementVisionHybridSash = new ValueContainer<CasementVisionHybridSash>(this);
    private ValueContainer<ChoiceFrameDepth> choiceFrameDepth = new ValueContainer<ChoiceFrameDepth>(this);
    private ValueContainer<Hardware> hardware = new ValueContainer<Hardware>(this);

    // Bound properties

    public static final String PROPERTYNAME_CASEMENTVISIONHYBRIDSASH = "casementVisionHybridSash";  
    public static final String PROPERTYNAME_CHOICEFRAMEDEPTH = "choiceFrameDepth";  
    public static final String PROPERTYNAME_HARDWARE = "hardware";  

    // Constructors

    public CasementVisionHybridBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.CasementVisionHybrid"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="fr", name="label", value="Master Sash")
    })
    public final CasementVisionHybridSash getCasementVisionHybridSash()
    {
         return this.casementVisionHybridSash.getValue();
    }

    public final void setCasementVisionHybridSash(CasementVisionHybridSash casementVisionHybridSash)
    {

        if (casementVisionHybridSash != null)
        {
            acquire(casementVisionHybridSash, "casementVisionHybridSash");
        }
        CasementVisionHybridSash oldValue = this.casementVisionHybridSash.getValue();
        this.casementVisionHybridSash.setValue(casementVisionHybridSash);

        CasementVisionHybridSash currentValue = this.casementVisionHybridSash.getValue();

        fireCasementVisionHybridSashChange(currentValue, oldValue);
    }
    public final CasementVisionHybridSash removeCasementVisionHybridSash()
    {
        CasementVisionHybridSash oldValue = this.casementVisionHybridSash.getValue();
        CasementVisionHybridSash removedValue = this.casementVisionHybridSash.removeValue();
        CasementVisionHybridSash currentValue = this.casementVisionHybridSash.getValue();

        fireCasementVisionHybridSashChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultCasementVisionHybridSash(CasementVisionHybridSash casementVisionHybridSash)
    {
        if (casementVisionHybridSash != null)
        {
            acquire(casementVisionHybridSash, "casementVisionHybridSash");
        }
        CasementVisionHybridSash oldValue = this.casementVisionHybridSash.getValue();
        this.casementVisionHybridSash.setDefaultValue(casementVisionHybridSash);

        CasementVisionHybridSash currentValue = this.casementVisionHybridSash.getValue();

        fireCasementVisionHybridSashChange(currentValue, oldValue);
    }
    
    public final CasementVisionHybridSash removeDefaultCasementVisionHybridSash()
    {
        CasementVisionHybridSash oldValue = this.casementVisionHybridSash.getValue();
        CasementVisionHybridSash removedValue = this.casementVisionHybridSash.removeDefaultValue();
        CasementVisionHybridSash currentValue = this.casementVisionHybridSash.getValue();

        fireCasementVisionHybridSashChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<CasementVisionHybridSash> getCasementVisionHybridSashValueContainer()
    {
        return this.casementVisionHybridSash;
    }
    public final void fireCasementVisionHybridSashChange(CasementVisionHybridSash currentValue, CasementVisionHybridSash oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeCasementVisionHybridSashChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CASEMENTVISIONHYBRIDSASH, oldValue, currentValue);
            afterCasementVisionHybridSashChanged(currentValue);
        }
    }

    public void beforeCasementVisionHybridSashChanged( CasementVisionHybridSash casementVisionHybridSash)
    { }
    public void afterCasementVisionHybridSashChanged( CasementVisionHybridSash casementVisionHybridSash)
    { }


    public boolean isCasementVisionHybridSashMandatory()
    {
        return true;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="desc", value="Frame")
        ,@LocalizedTag(language="fr", name="desc", value="Cadre")
        ,@LocalizedTag(language="en", name="label", value="Frame")
        ,@LocalizedTag(language="fr", name="label", value="Cadre")
    })
    public final ChoiceFrameDepth getChoiceFrameDepth()
    {
         return this.choiceFrameDepth.getValue();
    }

    public final void setChoiceFrameDepth(ChoiceFrameDepth choiceFrameDepth)
    {

        ChoiceFrameDepth oldValue = this.choiceFrameDepth.getValue();
        this.choiceFrameDepth.setValue(choiceFrameDepth);

        ChoiceFrameDepth currentValue = this.choiceFrameDepth.getValue();

        fireChoiceFrameDepthChange(currentValue, oldValue);
    }
    public final ChoiceFrameDepth removeChoiceFrameDepth()
    {
        ChoiceFrameDepth oldValue = this.choiceFrameDepth.getValue();
        ChoiceFrameDepth removedValue = this.choiceFrameDepth.removeValue();
        ChoiceFrameDepth currentValue = this.choiceFrameDepth.getValue();

        fireChoiceFrameDepthChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceFrameDepth(ChoiceFrameDepth choiceFrameDepth)
    {
        ChoiceFrameDepth oldValue = this.choiceFrameDepth.getValue();
        this.choiceFrameDepth.setDefaultValue(choiceFrameDepth);

        ChoiceFrameDepth currentValue = this.choiceFrameDepth.getValue();

        fireChoiceFrameDepthChange(currentValue, oldValue);
    }
    
    public final ChoiceFrameDepth removeDefaultChoiceFrameDepth()
    {
        ChoiceFrameDepth oldValue = this.choiceFrameDepth.getValue();
        ChoiceFrameDepth removedValue = this.choiceFrameDepth.removeDefaultValue();
        ChoiceFrameDepth currentValue = this.choiceFrameDepth.getValue();

        fireChoiceFrameDepthChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceFrameDepth> getChoiceFrameDepthValueContainer()
    {
        return this.choiceFrameDepth;
    }
    public final void fireChoiceFrameDepthChange(ChoiceFrameDepth currentValue, ChoiceFrameDepth oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceFrameDepthChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEFRAMEDEPTH, oldValue, currentValue);
            afterChoiceFrameDepthChanged(currentValue);
        }
    }

    public void beforeChoiceFrameDepthChanged( ChoiceFrameDepth choiceFrameDepth)
    { }
    public void afterChoiceFrameDepthChanged( ChoiceFrameDepth choiceFrameDepth)
    { }


    public boolean isChoiceFrameDepthMandatory()
    {
        return true;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Hardware")
        ,@LocalizedTag(language="fr", name="label", value="Quincaillerie")
    })
    public final Hardware getHardware()
    {
         return this.hardware.getValue();
    }

    public final void setHardware(Hardware hardware)
    {

        if (hardware != null)
        {
            acquire(hardware, "hardware");
        }
        Hardware oldValue = this.hardware.getValue();
        this.hardware.setValue(hardware);

        Hardware currentValue = this.hardware.getValue();

        fireHardwareChange(currentValue, oldValue);
    }
    public final Hardware removeHardware()
    {
        Hardware oldValue = this.hardware.getValue();
        Hardware removedValue = this.hardware.removeValue();
        Hardware currentValue = this.hardware.getValue();

        fireHardwareChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultHardware(Hardware hardware)
    {
        if (hardware != null)
        {
            acquire(hardware, "hardware");
        }
        Hardware oldValue = this.hardware.getValue();
        this.hardware.setDefaultValue(hardware);

        Hardware currentValue = this.hardware.getValue();

        fireHardwareChange(currentValue, oldValue);
    }
    
    public final Hardware removeDefaultHardware()
    {
        Hardware oldValue = this.hardware.getValue();
        Hardware removedValue = this.hardware.removeDefaultValue();
        Hardware currentValue = this.hardware.getValue();

        fireHardwareChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Hardware> getHardwareValueContainer()
    {
        return this.hardware;
    }
    public final void fireHardwareChange(Hardware currentValue, Hardware oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeHardwareChanged(currentValue);
            firePropertyChange(PROPERTYNAME_HARDWARE, oldValue, currentValue);
            afterHardwareChanged(currentValue);
        }
    }

    public void beforeHardwareChanged( Hardware hardware)
    { }
    public void afterHardwareChanged( Hardware hardware)
    { }


    public boolean isHardwareMandatory()
    {
        return true;
    }


    @Override
    protected String getSequences()
    {
        return "casementVisionHybridSash,choiceFrameDepth,nbSections,dimension,choicePresetWindowWidth,ExteriorColor,InteriorColor,awning,vertical,energyStar,installed,qDQ,sameGrilles,showMeetingRail,showMullion,frameDepth,noFrame,windowsMullion,paintable,renderingNumber,sectionInput,section,sash,meetingRail,not set,choiceSwing,profiles,option,hardware";  
    }
}
