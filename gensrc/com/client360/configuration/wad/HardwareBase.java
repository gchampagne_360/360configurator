// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.client360.configuration.wad.enums.ChoiceHinge;
import com.client360.configuration.wad.enums.ChoiceOperator;
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class HardwareBase extends com.netappsid.erp.configurator.Configurable
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(HardwareBase.class);

    // attributes
    private ValueContainer<ChoiceHinge> choiceHinge = new ValueContainer<ChoiceHinge>(this);
    private ValueContainer<ChoiceOperator> choiceOperator = new ValueContainer<ChoiceOperator>(this);

    // Bound properties

    public static final String PROPERTYNAME_CHOICEHINGE = "choiceHinge";  
    public static final String PROPERTYNAME_CHOICEOPERATOR = "choiceOperator";  

    // Constructors

    public HardwareBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.Hardware"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Hinge type")
        ,@LocalizedTag(language="fr", name="label", value="Type de penture")
    })
    public final ChoiceHinge getChoiceHinge()
    {
         return this.choiceHinge.getValue();
    }

    public final void setChoiceHinge(ChoiceHinge choiceHinge)
    {

        ChoiceHinge oldValue = this.choiceHinge.getValue();
        this.choiceHinge.setValue(choiceHinge);

        ChoiceHinge currentValue = this.choiceHinge.getValue();

        fireChoiceHingeChange(currentValue, oldValue);
    }
    public final ChoiceHinge removeChoiceHinge()
    {
        ChoiceHinge oldValue = this.choiceHinge.getValue();
        ChoiceHinge removedValue = this.choiceHinge.removeValue();
        ChoiceHinge currentValue = this.choiceHinge.getValue();

        fireChoiceHingeChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceHinge(ChoiceHinge choiceHinge)
    {
        ChoiceHinge oldValue = this.choiceHinge.getValue();
        this.choiceHinge.setDefaultValue(choiceHinge);

        ChoiceHinge currentValue = this.choiceHinge.getValue();

        fireChoiceHingeChange(currentValue, oldValue);
    }
    
    public final ChoiceHinge removeDefaultChoiceHinge()
    {
        ChoiceHinge oldValue = this.choiceHinge.getValue();
        ChoiceHinge removedValue = this.choiceHinge.removeDefaultValue();
        ChoiceHinge currentValue = this.choiceHinge.getValue();

        fireChoiceHingeChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceHinge> getChoiceHingeValueContainer()
    {
        return this.choiceHinge;
    }
    public final void fireChoiceHingeChange(ChoiceHinge currentValue, ChoiceHinge oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceHingeChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEHINGE, oldValue, currentValue);
            afterChoiceHingeChanged(currentValue);
        }
    }

    public void beforeChoiceHingeChanged( ChoiceHinge choiceHinge)
    { }
    public void afterChoiceHingeChanged( ChoiceHinge choiceHinge)
    { }


    public boolean isChoiceHingeMandatory()
    {
        return true;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Operator type")
        ,@LocalizedTag(language="fr", name="label", value="Type d\'opérateur")
    })
    public final ChoiceOperator getChoiceOperator()
    {
         return this.choiceOperator.getValue();
    }

    public final void setChoiceOperator(ChoiceOperator choiceOperator)
    {

        ChoiceOperator oldValue = this.choiceOperator.getValue();
        this.choiceOperator.setValue(choiceOperator);

        ChoiceOperator currentValue = this.choiceOperator.getValue();

        fireChoiceOperatorChange(currentValue, oldValue);
    }
    public final ChoiceOperator removeChoiceOperator()
    {
        ChoiceOperator oldValue = this.choiceOperator.getValue();
        ChoiceOperator removedValue = this.choiceOperator.removeValue();
        ChoiceOperator currentValue = this.choiceOperator.getValue();

        fireChoiceOperatorChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceOperator(ChoiceOperator choiceOperator)
    {
        ChoiceOperator oldValue = this.choiceOperator.getValue();
        this.choiceOperator.setDefaultValue(choiceOperator);

        ChoiceOperator currentValue = this.choiceOperator.getValue();

        fireChoiceOperatorChange(currentValue, oldValue);
    }
    
    public final ChoiceOperator removeDefaultChoiceOperator()
    {
        ChoiceOperator oldValue = this.choiceOperator.getValue();
        ChoiceOperator removedValue = this.choiceOperator.removeDefaultValue();
        ChoiceOperator currentValue = this.choiceOperator.getValue();

        fireChoiceOperatorChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceOperator> getChoiceOperatorValueContainer()
    {
        return this.choiceOperator;
    }
    public final void fireChoiceOperatorChange(ChoiceOperator currentValue, ChoiceOperator oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceOperatorChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEOPERATOR, oldValue, currentValue);
            afterChoiceOperatorChanged(currentValue);
        }
    }

    public void beforeChoiceOperatorChanged( ChoiceOperator choiceOperator)
    { }
    public void afterChoiceOperatorChanged( ChoiceOperator choiceOperator)
    { }


    public boolean isChoiceOperatorMandatory()
    {
        return true;
    }

}
