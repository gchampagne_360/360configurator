// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class SteelDoorOptionsBase extends com.netappsid.erp.configurator.Configurable
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(SteelDoorOptionsBase.class);

    // attributes
    private ValueContainer<Boolean> drilledForDeadLock = new ValueContainer<Boolean>(this);
    private ValueContainer<Boolean> extendedThreshold = new ValueContainer<Boolean>(this);
    private ValueContainer<Boolean> hasVinylCovering = new ValueContainer<Boolean>(this);
    private ValueContainer<Boolean> paintedWhite2Sides = new ValueContainer<Boolean>(this);
    private ValueContainer<Boolean> hasScreen = new ValueContainer<Boolean>(this);

    // Bound properties

    public static final String PROPERTYNAME_DRILLEDFORDEADLOCK = "drilledForDeadLock";  
    public static final String PROPERTYNAME_EXTENDEDTHRESHOLD = "extendedThreshold";  
    public static final String PROPERTYNAME_HASVINYLCOVERING = "hasVinylCovering";  
    public static final String PROPERTYNAME_PAINTEDWHITE2SIDES = "paintedWhite2Sides";  
    public static final String PROPERTYNAME_HASSCREEN = "hasScreen";  

    // Constructors

    public SteelDoorOptionsBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.SteelDoorOptions"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Drilled for deadlock")
        ,@LocalizedTag(language="fr", name="label", value="Perc� p�ne dormant")
    })
    public final Boolean getDrilledForDeadLock()
    {
         return this.drilledForDeadLock.getValue();
    }

    public final void setDrilledForDeadLock(Boolean drilledForDeadLock)
    {

        Boolean oldValue = this.drilledForDeadLock.getValue();
        this.drilledForDeadLock.setValue(drilledForDeadLock);

        Boolean currentValue = this.drilledForDeadLock.getValue();

        fireDrilledForDeadLockChange(currentValue, oldValue);
    }
    public final Boolean removeDrilledForDeadLock()
    {
        Boolean oldValue = this.drilledForDeadLock.getValue();
        Boolean removedValue = this.drilledForDeadLock.removeValue();
        Boolean currentValue = this.drilledForDeadLock.getValue();

        fireDrilledForDeadLockChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultDrilledForDeadLock(Boolean drilledForDeadLock)
    {
        Boolean oldValue = this.drilledForDeadLock.getValue();
        this.drilledForDeadLock.setDefaultValue(drilledForDeadLock);

        Boolean currentValue = this.drilledForDeadLock.getValue();

        fireDrilledForDeadLockChange(currentValue, oldValue);
    }
    
    public final Boolean removeDefaultDrilledForDeadLock()
    {
        Boolean oldValue = this.drilledForDeadLock.getValue();
        Boolean removedValue = this.drilledForDeadLock.removeDefaultValue();
        Boolean currentValue = this.drilledForDeadLock.getValue();

        fireDrilledForDeadLockChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Boolean> getDrilledForDeadLockValueContainer()
    {
        return this.drilledForDeadLock;
    }
    public final void fireDrilledForDeadLockChange(Boolean currentValue, Boolean oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeDrilledForDeadLockChanged(currentValue);
            firePropertyChange(PROPERTYNAME_DRILLEDFORDEADLOCK, oldValue, currentValue);
            afterDrilledForDeadLockChanged(currentValue);
        }
    }

    public void beforeDrilledForDeadLockChanged( Boolean drilledForDeadLock)
    { }
    public void afterDrilledForDeadLockChanged( Boolean drilledForDeadLock)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Threshold extension")
        ,@LocalizedTag(language="fr", name="label", value="Extension seuil")
    })
    public final Boolean getExtendedThreshold()
    {
         return this.extendedThreshold.getValue();
    }

    public final void setExtendedThreshold(Boolean extendedThreshold)
    {

        Boolean oldValue = this.extendedThreshold.getValue();
        this.extendedThreshold.setValue(extendedThreshold);

        Boolean currentValue = this.extendedThreshold.getValue();

        fireExtendedThresholdChange(currentValue, oldValue);
    }
    public final Boolean removeExtendedThreshold()
    {
        Boolean oldValue = this.extendedThreshold.getValue();
        Boolean removedValue = this.extendedThreshold.removeValue();
        Boolean currentValue = this.extendedThreshold.getValue();

        fireExtendedThresholdChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultExtendedThreshold(Boolean extendedThreshold)
    {
        Boolean oldValue = this.extendedThreshold.getValue();
        this.extendedThreshold.setDefaultValue(extendedThreshold);

        Boolean currentValue = this.extendedThreshold.getValue();

        fireExtendedThresholdChange(currentValue, oldValue);
    }
    
    public final Boolean removeDefaultExtendedThreshold()
    {
        Boolean oldValue = this.extendedThreshold.getValue();
        Boolean removedValue = this.extendedThreshold.removeDefaultValue();
        Boolean currentValue = this.extendedThreshold.getValue();

        fireExtendedThresholdChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Boolean> getExtendedThresholdValueContainer()
    {
        return this.extendedThreshold;
    }
    public final void fireExtendedThresholdChange(Boolean currentValue, Boolean oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeExtendedThresholdChanged(currentValue);
            firePropertyChange(PROPERTYNAME_EXTENDEDTHRESHOLD, oldValue, currentValue);
            afterExtendedThresholdChanged(currentValue);
        }
    }

    public void beforeExtendedThresholdChanged( Boolean extendedThreshold)
    { }
    public void afterExtendedThresholdChanged( Boolean extendedThreshold)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Vinyl covering")
        ,@LocalizedTag(language="fr", name="label", value="Recouvrement de vinyle")
    })
    public final Boolean getHasVinylCovering()
    {
         return this.hasVinylCovering.getValue();
    }

    public final void setHasVinylCovering(Boolean hasVinylCovering)
    {

        Boolean oldValue = this.hasVinylCovering.getValue();
        this.hasVinylCovering.setValue(hasVinylCovering);

        Boolean currentValue = this.hasVinylCovering.getValue();

        fireHasVinylCoveringChange(currentValue, oldValue);
    }
    public final Boolean removeHasVinylCovering()
    {
        Boolean oldValue = this.hasVinylCovering.getValue();
        Boolean removedValue = this.hasVinylCovering.removeValue();
        Boolean currentValue = this.hasVinylCovering.getValue();

        fireHasVinylCoveringChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultHasVinylCovering(Boolean hasVinylCovering)
    {
        Boolean oldValue = this.hasVinylCovering.getValue();
        this.hasVinylCovering.setDefaultValue(hasVinylCovering);

        Boolean currentValue = this.hasVinylCovering.getValue();

        fireHasVinylCoveringChange(currentValue, oldValue);
    }
    
    public final Boolean removeDefaultHasVinylCovering()
    {
        Boolean oldValue = this.hasVinylCovering.getValue();
        Boolean removedValue = this.hasVinylCovering.removeDefaultValue();
        Boolean currentValue = this.hasVinylCovering.getValue();

        fireHasVinylCoveringChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Boolean> getHasVinylCoveringValueContainer()
    {
        return this.hasVinylCovering;
    }
    public final void fireHasVinylCoveringChange(Boolean currentValue, Boolean oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeHasVinylCoveringChanged(currentValue);
            firePropertyChange(PROPERTYNAME_HASVINYLCOVERING, oldValue, currentValue);
            afterHasVinylCoveringChanged(currentValue);
        }
    }

    public void beforeHasVinylCoveringChanged( Boolean hasVinylCovering)
    { }
    public void afterHasVinylCoveringChanged( Boolean hasVinylCovering)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Painted WHITE 2 sides")
        ,@LocalizedTag(language="fr", name="label", value="Peintur� BLANC 2 c�t�s")
    })
    public final Boolean getPaintedWhite2Sides()
    {
         return this.paintedWhite2Sides.getValue();
    }

    public final void setPaintedWhite2Sides(Boolean paintedWhite2Sides)
    {

        Boolean oldValue = this.paintedWhite2Sides.getValue();
        this.paintedWhite2Sides.setValue(paintedWhite2Sides);

        Boolean currentValue = this.paintedWhite2Sides.getValue();

        firePaintedWhite2SidesChange(currentValue, oldValue);
    }
    public final Boolean removePaintedWhite2Sides()
    {
        Boolean oldValue = this.paintedWhite2Sides.getValue();
        Boolean removedValue = this.paintedWhite2Sides.removeValue();
        Boolean currentValue = this.paintedWhite2Sides.getValue();

        firePaintedWhite2SidesChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultPaintedWhite2Sides(Boolean paintedWhite2Sides)
    {
        Boolean oldValue = this.paintedWhite2Sides.getValue();
        this.paintedWhite2Sides.setDefaultValue(paintedWhite2Sides);

        Boolean currentValue = this.paintedWhite2Sides.getValue();

        firePaintedWhite2SidesChange(currentValue, oldValue);
    }
    
    public final Boolean removeDefaultPaintedWhite2Sides()
    {
        Boolean oldValue = this.paintedWhite2Sides.getValue();
        Boolean removedValue = this.paintedWhite2Sides.removeDefaultValue();
        Boolean currentValue = this.paintedWhite2Sides.getValue();

        firePaintedWhite2SidesChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Boolean> getPaintedWhite2SidesValueContainer()
    {
        return this.paintedWhite2Sides;
    }
    public final void firePaintedWhite2SidesChange(Boolean currentValue, Boolean oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforePaintedWhite2SidesChanged(currentValue);
            firePropertyChange(PROPERTYNAME_PAINTEDWHITE2SIDES, oldValue, currentValue);
            afterPaintedWhite2SidesChanged(currentValue);
        }
    }

    public void beforePaintedWhite2SidesChanged( Boolean paintedWhite2Sides)
    { }
    public void afterPaintedWhite2SidesChanged( Boolean paintedWhite2Sides)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="With screen")
        ,@LocalizedTag(language="fr", name="label", value="Avec Moustiquaire")
    })
    public final Boolean getHasScreen()
    {
         return this.hasScreen.getValue();
    }

    public final void setHasScreen(Boolean hasScreen)
    {

        Boolean oldValue = this.hasScreen.getValue();
        this.hasScreen.setValue(hasScreen);

        Boolean currentValue = this.hasScreen.getValue();

        fireHasScreenChange(currentValue, oldValue);
    }
    public final Boolean removeHasScreen()
    {
        Boolean oldValue = this.hasScreen.getValue();
        Boolean removedValue = this.hasScreen.removeValue();
        Boolean currentValue = this.hasScreen.getValue();

        fireHasScreenChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultHasScreen(Boolean hasScreen)
    {
        Boolean oldValue = this.hasScreen.getValue();
        this.hasScreen.setDefaultValue(hasScreen);

        Boolean currentValue = this.hasScreen.getValue();

        fireHasScreenChange(currentValue, oldValue);
    }
    
    public final Boolean removeDefaultHasScreen()
    {
        Boolean oldValue = this.hasScreen.getValue();
        Boolean removedValue = this.hasScreen.removeDefaultValue();
        Boolean currentValue = this.hasScreen.getValue();

        fireHasScreenChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Boolean> getHasScreenValueContainer()
    {
        return this.hasScreen;
    }
    public final void fireHasScreenChange(Boolean currentValue, Boolean oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeHasScreenChanged(currentValue);
            firePropertyChange(PROPERTYNAME_HASSCREEN, oldValue, currentValue);
            afterHasScreenChanged(currentValue);
        }
    }

    public void beforeHasScreenChanged( Boolean hasScreen)
    { }
    public void afterHasScreenChanged( Boolean hasScreen)
    { }



    @Override
    protected String getSequences()
    {
        return "hasVinylCovering,extendedThreshold,drilledForDeadLock,paintedWhite2Sides,hasScreen";  
    }
}
