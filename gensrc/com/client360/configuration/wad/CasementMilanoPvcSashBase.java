// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.client360.configuration.wad.enums.ChoiceMilanoSashStyle;
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class CasementMilanoPvcSashBase extends com.client360.configuration.wad.CasementMainSash
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(CasementMilanoPvcSashBase.class);

    // attributes
    private ValueContainer<ChoiceMilanoSashStyle> choiceMilanoSashStyle = new ValueContainer<ChoiceMilanoSashStyle>(this);

    // Bound properties

    public static final String PROPERTYNAME_CHOICEMILANOSASHSTYLE = "choiceMilanoSashStyle";  

    // Constructors

    public CasementMilanoPvcSashBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.CasementMilanoPvcSash"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Sash style")
        ,@LocalizedTag(language="fr", name="label", value="Style de volet")
    })
    public final ChoiceMilanoSashStyle getChoiceMilanoSashStyle()
    {
         return this.choiceMilanoSashStyle.getValue();
    }

    public final void setChoiceMilanoSashStyle(ChoiceMilanoSashStyle choiceMilanoSashStyle)
    {

        ChoiceMilanoSashStyle oldValue = this.choiceMilanoSashStyle.getValue();
        this.choiceMilanoSashStyle.setValue(choiceMilanoSashStyle);

        ChoiceMilanoSashStyle currentValue = this.choiceMilanoSashStyle.getValue();

        fireChoiceMilanoSashStyleChange(currentValue, oldValue);
    }
    public final ChoiceMilanoSashStyle removeChoiceMilanoSashStyle()
    {
        ChoiceMilanoSashStyle oldValue = this.choiceMilanoSashStyle.getValue();
        ChoiceMilanoSashStyle removedValue = this.choiceMilanoSashStyle.removeValue();
        ChoiceMilanoSashStyle currentValue = this.choiceMilanoSashStyle.getValue();

        fireChoiceMilanoSashStyleChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceMilanoSashStyle(ChoiceMilanoSashStyle choiceMilanoSashStyle)
    {
        ChoiceMilanoSashStyle oldValue = this.choiceMilanoSashStyle.getValue();
        this.choiceMilanoSashStyle.setDefaultValue(choiceMilanoSashStyle);

        ChoiceMilanoSashStyle currentValue = this.choiceMilanoSashStyle.getValue();

        fireChoiceMilanoSashStyleChange(currentValue, oldValue);
    }
    
    public final ChoiceMilanoSashStyle removeDefaultChoiceMilanoSashStyle()
    {
        ChoiceMilanoSashStyle oldValue = this.choiceMilanoSashStyle.getValue();
        ChoiceMilanoSashStyle removedValue = this.choiceMilanoSashStyle.removeDefaultValue();
        ChoiceMilanoSashStyle currentValue = this.choiceMilanoSashStyle.getValue();

        fireChoiceMilanoSashStyleChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceMilanoSashStyle> getChoiceMilanoSashStyleValueContainer()
    {
        return this.choiceMilanoSashStyle;
    }
    public final void fireChoiceMilanoSashStyleChange(ChoiceMilanoSashStyle currentValue, ChoiceMilanoSashStyle oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceMilanoSashStyleChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEMILANOSASHSTYLE, oldValue, currentValue);
            afterChoiceMilanoSashStyleChanged(currentValue);
        }
    }

    public void beforeChoiceMilanoSashStyleChanged( ChoiceMilanoSashStyle choiceMilanoSashStyle)
    { }
    public void afterChoiceMilanoSashStyleChanged( ChoiceMilanoSashStyle choiceMilanoSashStyle)
    { }


    public boolean isChoiceMilanoSashStyleMandatory()
    {
        return true;
    }


    @Override
    protected String getSequences()
    {
        return "choiceSashType,choiceMilanoSashStyle,installed,sashSection,screen,dimension,ExteriorColor,InteriorColor";  
    }
}
