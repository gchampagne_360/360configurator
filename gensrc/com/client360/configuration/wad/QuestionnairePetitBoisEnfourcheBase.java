// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.client360.configuration.wad.enums.LargeurPetitBoisEnfourche;
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class QuestionnairePetitBoisEnfourcheBase extends com.netappsid.wadconfigurator.AdvancedSectionGrilles
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(QuestionnairePetitBoisEnfourcheBase.class);

    // attributes
    private ValueContainer<Integer> nbTdlHorizBars = new ValueContainer<Integer>(this);
    private ValueContainer<Integer> nbTdlVertiBars = new ValueContainer<Integer>(this);
    private ValueContainer<Boolean> verresEgaux = new ValueContainer<Boolean>(this);
    private ValueContainer<Boolean> largeurHorsCote = new ValueContainer<Boolean>(this);
    private ValueContainer<LargeurPetitBoisEnfourche> largeurPetitBoisEnfourche = new ValueContainer<LargeurPetitBoisEnfourche>(this);

    // Bound properties

    public static final String PROPERTYNAME_NBTDLHORIZBARS = "nbTdlHorizBars";  
    public static final String PROPERTYNAME_NBTDLVERTIBARS = "nbTdlVertiBars";  
    public static final String PROPERTYNAME_VERRESEGAUX = "verresEgaux";  
    public static final String PROPERTYNAME_LARGEURHORSCOTE = "largeurHorsCote";  
    public static final String PROPERTYNAME_LARGEURPETITBOISENFOURCHE = "largeurPetitBoisEnfourche";  

    // Constructors

    public QuestionnairePetitBoisEnfourcheBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.QuestionnairePetitBoisEnfourche"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Horizontal bars qty")
        ,@LocalizedTag(language="fr", name="label", value="Nb barres horizontales")
    })
    public final Integer getNbTdlHorizBars()
    {
         return this.nbTdlHorizBars.getValue();
    }

    public final void setNbTdlHorizBars(Integer nbTdlHorizBars)
    {

        Integer oldValue = this.nbTdlHorizBars.getValue();
        this.nbTdlHorizBars.setValue(nbTdlHorizBars);

        Integer currentValue = this.nbTdlHorizBars.getValue();

        fireNbTdlHorizBarsChange(currentValue, oldValue);
    }
    public final Integer removeNbTdlHorizBars()
    {
        Integer oldValue = this.nbTdlHorizBars.getValue();
        Integer removedValue = this.nbTdlHorizBars.removeValue();
        Integer currentValue = this.nbTdlHorizBars.getValue();

        fireNbTdlHorizBarsChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultNbTdlHorizBars(Integer nbTdlHorizBars)
    {
        Integer oldValue = this.nbTdlHorizBars.getValue();
        this.nbTdlHorizBars.setDefaultValue(nbTdlHorizBars);

        Integer currentValue = this.nbTdlHorizBars.getValue();

        fireNbTdlHorizBarsChange(currentValue, oldValue);
    }
    
    public final Integer removeDefaultNbTdlHorizBars()
    {
        Integer oldValue = this.nbTdlHorizBars.getValue();
        Integer removedValue = this.nbTdlHorizBars.removeDefaultValue();
        Integer currentValue = this.nbTdlHorizBars.getValue();

        fireNbTdlHorizBarsChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Integer> getNbTdlHorizBarsValueContainer()
    {
        return this.nbTdlHorizBars;
    }
    public final void fireNbTdlHorizBarsChange(Integer currentValue, Integer oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeNbTdlHorizBarsChanged(currentValue);
            firePropertyChange(PROPERTYNAME_NBTDLHORIZBARS, oldValue, currentValue);
            afterNbTdlHorizBarsChanged(currentValue);
        }
    }

    public void beforeNbTdlHorizBarsChanged( Integer nbTdlHorizBars)
    { }
    public void afterNbTdlHorizBarsChanged( Integer nbTdlHorizBars)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Vertical bars qty")
        ,@LocalizedTag(language="fr", name="label", value="Nb barres verticales")
    })
    public final Integer getNbTdlVertiBars()
    {
         return this.nbTdlVertiBars.getValue();
    }

    public final void setNbTdlVertiBars(Integer nbTdlVertiBars)
    {

        Integer oldValue = this.nbTdlVertiBars.getValue();
        this.nbTdlVertiBars.setValue(nbTdlVertiBars);

        Integer currentValue = this.nbTdlVertiBars.getValue();

        fireNbTdlVertiBarsChange(currentValue, oldValue);
    }
    public final Integer removeNbTdlVertiBars()
    {
        Integer oldValue = this.nbTdlVertiBars.getValue();
        Integer removedValue = this.nbTdlVertiBars.removeValue();
        Integer currentValue = this.nbTdlVertiBars.getValue();

        fireNbTdlVertiBarsChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultNbTdlVertiBars(Integer nbTdlVertiBars)
    {
        Integer oldValue = this.nbTdlVertiBars.getValue();
        this.nbTdlVertiBars.setDefaultValue(nbTdlVertiBars);

        Integer currentValue = this.nbTdlVertiBars.getValue();

        fireNbTdlVertiBarsChange(currentValue, oldValue);
    }
    
    public final Integer removeDefaultNbTdlVertiBars()
    {
        Integer oldValue = this.nbTdlVertiBars.getValue();
        Integer removedValue = this.nbTdlVertiBars.removeDefaultValue();
        Integer currentValue = this.nbTdlVertiBars.getValue();

        fireNbTdlVertiBarsChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Integer> getNbTdlVertiBarsValueContainer()
    {
        return this.nbTdlVertiBars;
    }
    public final void fireNbTdlVertiBarsChange(Integer currentValue, Integer oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeNbTdlVertiBarsChanged(currentValue);
            firePropertyChange(PROPERTYNAME_NBTDLVERTIBARS, oldValue, currentValue);
            afterNbTdlVertiBarsChanged(currentValue);
        }
    }

    public void beforeNbTdlVertiBarsChanged( Integer nbTdlVertiBars)
    { }
    public void afterNbTdlVertiBarsChanged( Integer nbTdlVertiBars)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="fr", name="label", value="Verres �gaux")
    })
    public final Boolean getVerresEgaux()
    {
         return this.verresEgaux.getValue();
    }

    public final void setVerresEgaux(Boolean verresEgaux)
    {

        Boolean oldValue = this.verresEgaux.getValue();
        this.verresEgaux.setValue(verresEgaux);

        Boolean currentValue = this.verresEgaux.getValue();

        fireVerresEgauxChange(currentValue, oldValue);
    }
    public final Boolean removeVerresEgaux()
    {
        Boolean oldValue = this.verresEgaux.getValue();
        Boolean removedValue = this.verresEgaux.removeValue();
        Boolean currentValue = this.verresEgaux.getValue();

        fireVerresEgauxChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultVerresEgaux(Boolean verresEgaux)
    {
        Boolean oldValue = this.verresEgaux.getValue();
        this.verresEgaux.setDefaultValue(verresEgaux);

        Boolean currentValue = this.verresEgaux.getValue();

        fireVerresEgauxChange(currentValue, oldValue);
    }
    
    public final Boolean removeDefaultVerresEgaux()
    {
        Boolean oldValue = this.verresEgaux.getValue();
        Boolean removedValue = this.verresEgaux.removeDefaultValue();
        Boolean currentValue = this.verresEgaux.getValue();

        fireVerresEgauxChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Boolean> getVerresEgauxValueContainer()
    {
        return this.verresEgaux;
    }
    public final void fireVerresEgauxChange(Boolean currentValue, Boolean oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeVerresEgauxChanged(currentValue);
            firePropertyChange(PROPERTYNAME_VERRESEGAUX, oldValue, currentValue);
            afterVerresEgauxChanged(currentValue);
        }
    }

    public void beforeVerresEgauxChanged( Boolean verresEgaux)
    { }
    public void afterVerresEgauxChanged( Boolean verresEgaux)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="fr", name="label", value="Largeur hors c�te")
    })
    public final Boolean getLargeurHorsCote()
    {
         return this.largeurHorsCote.getValue();
    }

    public final void setLargeurHorsCote(Boolean largeurHorsCote)
    {

        Boolean oldValue = this.largeurHorsCote.getValue();
        this.largeurHorsCote.setValue(largeurHorsCote);

        Boolean currentValue = this.largeurHorsCote.getValue();

        fireLargeurHorsCoteChange(currentValue, oldValue);
    }
    public final Boolean removeLargeurHorsCote()
    {
        Boolean oldValue = this.largeurHorsCote.getValue();
        Boolean removedValue = this.largeurHorsCote.removeValue();
        Boolean currentValue = this.largeurHorsCote.getValue();

        fireLargeurHorsCoteChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultLargeurHorsCote(Boolean largeurHorsCote)
    {
        Boolean oldValue = this.largeurHorsCote.getValue();
        this.largeurHorsCote.setDefaultValue(largeurHorsCote);

        Boolean currentValue = this.largeurHorsCote.getValue();

        fireLargeurHorsCoteChange(currentValue, oldValue);
    }
    
    public final Boolean removeDefaultLargeurHorsCote()
    {
        Boolean oldValue = this.largeurHorsCote.getValue();
        Boolean removedValue = this.largeurHorsCote.removeDefaultValue();
        Boolean currentValue = this.largeurHorsCote.getValue();

        fireLargeurHorsCoteChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Boolean> getLargeurHorsCoteValueContainer()
    {
        return this.largeurHorsCote;
    }
    public final void fireLargeurHorsCoteChange(Boolean currentValue, Boolean oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeLargeurHorsCoteChanged(currentValue);
            firePropertyChange(PROPERTYNAME_LARGEURHORSCOTE, oldValue, currentValue);
            afterLargeurHorsCoteChanged(currentValue);
        }
    }

    public void beforeLargeurHorsCoteChanged( Boolean largeurHorsCote)
    { }
    public void afterLargeurHorsCoteChanged( Boolean largeurHorsCote)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="fr", name="label", value="Largeur")
    })
    public final LargeurPetitBoisEnfourche getLargeurPetitBoisEnfourche()
    {
         return this.largeurPetitBoisEnfourche.getValue();
    }

    public final void setLargeurPetitBoisEnfourche(LargeurPetitBoisEnfourche largeurPetitBoisEnfourche)
    {

        LargeurPetitBoisEnfourche oldValue = this.largeurPetitBoisEnfourche.getValue();
        this.largeurPetitBoisEnfourche.setValue(largeurPetitBoisEnfourche);

        LargeurPetitBoisEnfourche currentValue = this.largeurPetitBoisEnfourche.getValue();

        fireLargeurPetitBoisEnfourcheChange(currentValue, oldValue);
    }
    public final LargeurPetitBoisEnfourche removeLargeurPetitBoisEnfourche()
    {
        LargeurPetitBoisEnfourche oldValue = this.largeurPetitBoisEnfourche.getValue();
        LargeurPetitBoisEnfourche removedValue = this.largeurPetitBoisEnfourche.removeValue();
        LargeurPetitBoisEnfourche currentValue = this.largeurPetitBoisEnfourche.getValue();

        fireLargeurPetitBoisEnfourcheChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultLargeurPetitBoisEnfourche(LargeurPetitBoisEnfourche largeurPetitBoisEnfourche)
    {
        LargeurPetitBoisEnfourche oldValue = this.largeurPetitBoisEnfourche.getValue();
        this.largeurPetitBoisEnfourche.setDefaultValue(largeurPetitBoisEnfourche);

        LargeurPetitBoisEnfourche currentValue = this.largeurPetitBoisEnfourche.getValue();

        fireLargeurPetitBoisEnfourcheChange(currentValue, oldValue);
    }
    
    public final LargeurPetitBoisEnfourche removeDefaultLargeurPetitBoisEnfourche()
    {
        LargeurPetitBoisEnfourche oldValue = this.largeurPetitBoisEnfourche.getValue();
        LargeurPetitBoisEnfourche removedValue = this.largeurPetitBoisEnfourche.removeDefaultValue();
        LargeurPetitBoisEnfourche currentValue = this.largeurPetitBoisEnfourche.getValue();

        fireLargeurPetitBoisEnfourcheChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<LargeurPetitBoisEnfourche> getLargeurPetitBoisEnfourcheValueContainer()
    {
        return this.largeurPetitBoisEnfourche;
    }
    public final void fireLargeurPetitBoisEnfourcheChange(LargeurPetitBoisEnfourche currentValue, LargeurPetitBoisEnfourche oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeLargeurPetitBoisEnfourcheChanged(currentValue);
            firePropertyChange(PROPERTYNAME_LARGEURPETITBOISENFOURCHE, oldValue, currentValue);
            afterLargeurPetitBoisEnfourcheChanged(currentValue);
        }
    }

    public void beforeLargeurPetitBoisEnfourcheChanged( LargeurPetitBoisEnfourche largeurPetitBoisEnfourche)
    { }
    public void afterLargeurPetitBoisEnfourcheChanged( LargeurPetitBoisEnfourche largeurPetitBoisEnfourche)
    { }


    public boolean isLargeurPetitBoisEnfourcheMandatory()
    {
        return true;
    }


    @Override
    protected String getSequences()
    {
        return "verresEgaux,nbTdlHorizBars,nbTdlVertiBars,largeurPetitBoisEnfourche,dimension,NoSplittingBars,not set";  
    }
}
