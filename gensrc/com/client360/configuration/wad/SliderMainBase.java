// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.client360.configuration.wad.enums.ChoiceAdvancedGrilles;
import com.client360.configuration.wad.enums.ChoiceMosquito;
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class SliderMainBase extends com.netappsid.wadconfigurator.Slider
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(SliderMainBase.class);

    // attributes
    private ValueContainer<ChoiceMosquito> choiceMosquito = new ValueContainer<ChoiceMosquito>(this);
    private ValueContainer<ChoiceAdvancedGrilles> choiceAdvancedGrilles = new ValueContainer<ChoiceAdvancedGrilles>(this);

    // Bound properties

    public static final String PROPERTYNAME_CHOICEMOSQUITO = "choiceMosquito";  
    public static final String PROPERTYNAME_CHOICEADVANCEDGRILLES = "choiceAdvancedGrilles";  

    // Constructors

    public SliderMainBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.SliderMain"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
    })
    public final ChoiceMosquito getChoiceMosquito()
    {
         return this.choiceMosquito.getValue();
    }

    public final void setChoiceMosquito(ChoiceMosquito choiceMosquito)
    {

        ChoiceMosquito oldValue = this.choiceMosquito.getValue();
        this.choiceMosquito.setValue(choiceMosquito);

        ChoiceMosquito currentValue = this.choiceMosquito.getValue();

        fireChoiceMosquitoChange(currentValue, oldValue);
    }
    public final ChoiceMosquito removeChoiceMosquito()
    {
        ChoiceMosquito oldValue = this.choiceMosquito.getValue();
        ChoiceMosquito removedValue = this.choiceMosquito.removeValue();
        ChoiceMosquito currentValue = this.choiceMosquito.getValue();

        fireChoiceMosquitoChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceMosquito(ChoiceMosquito choiceMosquito)
    {
        ChoiceMosquito oldValue = this.choiceMosquito.getValue();
        this.choiceMosquito.setDefaultValue(choiceMosquito);

        ChoiceMosquito currentValue = this.choiceMosquito.getValue();

        fireChoiceMosquitoChange(currentValue, oldValue);
    }
    
    public final ChoiceMosquito removeDefaultChoiceMosquito()
    {
        ChoiceMosquito oldValue = this.choiceMosquito.getValue();
        ChoiceMosquito removedValue = this.choiceMosquito.removeDefaultValue();
        ChoiceMosquito currentValue = this.choiceMosquito.getValue();

        fireChoiceMosquitoChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceMosquito> getChoiceMosquitoValueContainer()
    {
        return this.choiceMosquito;
    }
    public final void fireChoiceMosquitoChange(ChoiceMosquito currentValue, ChoiceMosquito oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceMosquitoChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEMOSQUITO, oldValue, currentValue);
            afterChoiceMosquitoChanged(currentValue);
        }
    }

    public void beforeChoiceMosquitoChanged( ChoiceMosquito choiceMosquito)
    { }
    public void afterChoiceMosquitoChanged( ChoiceMosquito choiceMosquito)
    { }


    public boolean isChoiceMosquitoMandatory()
    {
        return true;
    }
    @LocalizedTags
    ({
    })
    public final ChoiceAdvancedGrilles getChoiceAdvancedGrilles()
    {
         return this.choiceAdvancedGrilles.getValue();
    }

    public final void setChoiceAdvancedGrilles(ChoiceAdvancedGrilles choiceAdvancedGrilles)
    {

        ChoiceAdvancedGrilles oldValue = this.choiceAdvancedGrilles.getValue();
        this.choiceAdvancedGrilles.setValue(choiceAdvancedGrilles);

        ChoiceAdvancedGrilles currentValue = this.choiceAdvancedGrilles.getValue();

        fireChoiceAdvancedGrillesChange(currentValue, oldValue);
    }
    public final ChoiceAdvancedGrilles removeChoiceAdvancedGrilles()
    {
        ChoiceAdvancedGrilles oldValue = this.choiceAdvancedGrilles.getValue();
        ChoiceAdvancedGrilles removedValue = this.choiceAdvancedGrilles.removeValue();
        ChoiceAdvancedGrilles currentValue = this.choiceAdvancedGrilles.getValue();

        fireChoiceAdvancedGrillesChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceAdvancedGrilles(ChoiceAdvancedGrilles choiceAdvancedGrilles)
    {
        ChoiceAdvancedGrilles oldValue = this.choiceAdvancedGrilles.getValue();
        this.choiceAdvancedGrilles.setDefaultValue(choiceAdvancedGrilles);

        ChoiceAdvancedGrilles currentValue = this.choiceAdvancedGrilles.getValue();

        fireChoiceAdvancedGrillesChange(currentValue, oldValue);
    }
    
    public final ChoiceAdvancedGrilles removeDefaultChoiceAdvancedGrilles()
    {
        ChoiceAdvancedGrilles oldValue = this.choiceAdvancedGrilles.getValue();
        ChoiceAdvancedGrilles removedValue = this.choiceAdvancedGrilles.removeDefaultValue();
        ChoiceAdvancedGrilles currentValue = this.choiceAdvancedGrilles.getValue();

        fireChoiceAdvancedGrillesChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceAdvancedGrilles> getChoiceAdvancedGrillesValueContainer()
    {
        return this.choiceAdvancedGrilles;
    }
    public final void fireChoiceAdvancedGrillesChange(ChoiceAdvancedGrilles currentValue, ChoiceAdvancedGrilles oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceAdvancedGrillesChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEADVANCEDGRILLES, oldValue, currentValue);
            afterChoiceAdvancedGrillesChanged(currentValue);
        }
    }

    public void beforeChoiceAdvancedGrillesChanged( ChoiceAdvancedGrilles choiceAdvancedGrilles)
    { }
    public void afterChoiceAdvancedGrillesChanged( ChoiceAdvancedGrilles choiceAdvancedGrilles)
    { }



    @Override
    protected String getSequences()
    {
        return "energyStar,noFrame,frameDepth,installed,nbSections,qDQ,sameGrilles,showMeetingRail,showMullion,profiles,InteriorColor,ExteriorColor,windowsMullion,advancedSectionGrilles,sash,paintable,renderingNumber,dimension,sectionInput,section,choicePresetWindowWidth,division,meetingRail,option";  
    }
}
