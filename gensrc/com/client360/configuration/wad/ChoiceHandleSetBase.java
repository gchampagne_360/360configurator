// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.DynamicEnum;
import com.netappsid.erp.configurator.annotations.Filtrable;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

@DynamicEnum(fileName = "dynamicEnums/handleset.tsv", locale = "en")
public abstract class ChoiceHandleSetBase extends com.netappsid.erp.configurator.Configurable
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ChoiceHandleSetBase.class);

    // attributes
    private ValueContainer<String> description = new ValueContainer<String>(this);
    private ValueContainer<String> path = new ValueContainer<String>(this);
    private ValueContainer<Double> width = new ValueContainer<Double>(this);
    private ValueContainer<Double> height = new ValueContainer<Double>(this);
    private ValueContainer<Double> orix = new ValueContainer<Double>(this);
    private ValueContainer<Double> oriy = new ValueContainer<Double>(this);
    private ValueContainer<String> tr1form = new ValueContainer<String>(this);
    private ValueContainer<Double> tr1posx = new ValueContainer<Double>(this);
    private ValueContainer<Double> tr1posy = new ValueContainer<Double>(this);
    private ValueContainer<Double> tr1width = new ValueContainer<Double>(this);
    private ValueContainer<Double> tr1height = new ValueContainer<Double>(this);
    private ValueContainer<String> tr2form = new ValueContainer<String>(this);
    private ValueContainer<Double> tr2posx = new ValueContainer<Double>(this);
    private ValueContainer<Double> tr2posy = new ValueContainer<Double>(this);
    private ValueContainer<Double> tr2width = new ValueContainer<Double>(this);
    private ValueContainer<Double> tr2height = new ValueContainer<Double>(this);

    // Bound properties

    public static final String PROPERTYNAME_DESCRIPTION = "description";  
    public static final String PROPERTYNAME_PATH = "path";  
    public static final String PROPERTYNAME_WIDTH = "width";  
    public static final String PROPERTYNAME_HEIGHT = "height";  
    public static final String PROPERTYNAME_ORIX = "orix";  
    public static final String PROPERTYNAME_ORIY = "oriy";  
    public static final String PROPERTYNAME_TR1FORM = "tr1form";  
    public static final String PROPERTYNAME_TR1POSX = "tr1posx";  
    public static final String PROPERTYNAME_TR1POSY = "tr1posy";  
    public static final String PROPERTYNAME_TR1WIDTH = "tr1width";  
    public static final String PROPERTYNAME_TR1HEIGHT = "tr1height";  
    public static final String PROPERTYNAME_TR2FORM = "tr2form";  
    public static final String PROPERTYNAME_TR2POSX = "tr2posx";  
    public static final String PROPERTYNAME_TR2POSY = "tr2posy";  
    public static final String PROPERTYNAME_TR2WIDTH = "tr2width";  
    public static final String PROPERTYNAME_TR2HEIGHT = "tr2height";  

    // Constructors

    public ChoiceHandleSetBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.ChoiceHandleSet"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @Filtrable(value = "true", defaultValue = "", defaultSelection = "")
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="0")
        ,@LocalizedTag(language="fr", name="column", value="0")
        ,@LocalizedTag(language="en", name="label", value="Description")
        ,@LocalizedTag(language="fr", name="label", value="Description")
    })
    public final String getDescription()
    {
         return this.description.getValue();
    }

    public final void setDescription(String description)
    {

        String oldValue = this.description.getValue();
        this.description.setValue(description);

        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
    }
    public final String removeDescription()
    {
        String oldValue = this.description.getValue();
        String removedValue = this.description.removeValue();
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultDescription(String description)
    {
        String oldValue = this.description.getValue();
        this.description.setDefaultValue(description);

        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
    }
    
    public final String removeDefaultDescription()
    {
        String oldValue = this.description.getValue();
        String removedValue = this.description.removeDefaultValue();
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getDescriptionValueContainer()
    {
        return this.description;
    }
    public final void fireDescriptionChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeDescriptionChanged(currentValue);
            firePropertyChange(PROPERTYNAME_DESCRIPTION, oldValue, currentValue);
            afterDescriptionChanged(currentValue);
        }
    }

    public void beforeDescriptionChanged( String description)
    { }
    public void afterDescriptionChanged( String description)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="1")
        ,@LocalizedTag(language="fr", name="column", value="1")
    })
    public final String getPath()
    {
         return this.path.getValue();
    }

    public final void setPath(String path)
    {

        String oldValue = this.path.getValue();
        this.path.setValue(path);

        String currentValue = this.path.getValue();

        firePathChange(currentValue, oldValue);
    }
    public final String removePath()
    {
        String oldValue = this.path.getValue();
        String removedValue = this.path.removeValue();
        String currentValue = this.path.getValue();

        firePathChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultPath(String path)
    {
        String oldValue = this.path.getValue();
        this.path.setDefaultValue(path);

        String currentValue = this.path.getValue();

        firePathChange(currentValue, oldValue);
    }
    
    public final String removeDefaultPath()
    {
        String oldValue = this.path.getValue();
        String removedValue = this.path.removeDefaultValue();
        String currentValue = this.path.getValue();

        firePathChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getPathValueContainer()
    {
        return this.path;
    }
    public final void firePathChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforePathChanged(currentValue);
            firePropertyChange(PROPERTYNAME_PATH, oldValue, currentValue);
            afterPathChanged(currentValue);
        }
    }

    public void beforePathChanged( String path)
    { }
    public void afterPathChanged( String path)
    { }


    public boolean isPathVisible()
    {
        return false;
    }
    @Filtrable(value = "true", defaultValue = "", defaultSelection = "")
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="2")
        ,@LocalizedTag(language="fr", name="column", value="2")
        ,@LocalizedTag(language="en", name="label", value="width")
        ,@LocalizedTag(language="fr", name="label", value="largeur")
    })
    public final Double getWidth()
    {
         return this.width.getValue();
    }

    public final void setWidth(Double width)
    {

        Double oldValue = this.width.getValue();
        this.width.setValue(width);

        Double currentValue = this.width.getValue();

        fireWidthChange(currentValue, oldValue);
    }
    public final Double removeWidth()
    {
        Double oldValue = this.width.getValue();
        Double removedValue = this.width.removeValue();
        Double currentValue = this.width.getValue();

        fireWidthChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultWidth(Double width)
    {
        Double oldValue = this.width.getValue();
        this.width.setDefaultValue(width);

        Double currentValue = this.width.getValue();

        fireWidthChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultWidth()
    {
        Double oldValue = this.width.getValue();
        Double removedValue = this.width.removeDefaultValue();
        Double currentValue = this.width.getValue();

        fireWidthChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getWidthValueContainer()
    {
        return this.width;
    }
    public final void fireWidthChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeWidthChanged(currentValue);
            firePropertyChange(PROPERTYNAME_WIDTH, oldValue, currentValue);
            afterWidthChanged(currentValue);
        }
    }

    public void beforeWidthChanged( Double width)
    { }
    public void afterWidthChanged( Double width)
    { }

    @Filtrable(value = "true", defaultValue = "", defaultSelection = "")
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="3")
        ,@LocalizedTag(language="fr", name="column", value="3")
        ,@LocalizedTag(language="en", name="label", value="height")
        ,@LocalizedTag(language="fr", name="label", value="hauteur")
    })
    public final Double getHeight()
    {
         return this.height.getValue();
    }

    public final void setHeight(Double height)
    {

        Double oldValue = this.height.getValue();
        this.height.setValue(height);

        Double currentValue = this.height.getValue();

        fireHeightChange(currentValue, oldValue);
    }
    public final Double removeHeight()
    {
        Double oldValue = this.height.getValue();
        Double removedValue = this.height.removeValue();
        Double currentValue = this.height.getValue();

        fireHeightChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultHeight(Double height)
    {
        Double oldValue = this.height.getValue();
        this.height.setDefaultValue(height);

        Double currentValue = this.height.getValue();

        fireHeightChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultHeight()
    {
        Double oldValue = this.height.getValue();
        Double removedValue = this.height.removeDefaultValue();
        Double currentValue = this.height.getValue();

        fireHeightChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getHeightValueContainer()
    {
        return this.height;
    }
    public final void fireHeightChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeHeightChanged(currentValue);
            firePropertyChange(PROPERTYNAME_HEIGHT, oldValue, currentValue);
            afterHeightChanged(currentValue);
        }
    }

    public void beforeHeightChanged( Double height)
    { }
    public void afterHeightChanged( Double height)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="4")
        ,@LocalizedTag(language="fr", name="column", value="4")
    })
    public final Double getOrix()
    {
         return this.orix.getValue();
    }

    public final void setOrix(Double orix)
    {

        Double oldValue = this.orix.getValue();
        this.orix.setValue(orix);

        Double currentValue = this.orix.getValue();

        fireOrixChange(currentValue, oldValue);
    }
    public final Double removeOrix()
    {
        Double oldValue = this.orix.getValue();
        Double removedValue = this.orix.removeValue();
        Double currentValue = this.orix.getValue();

        fireOrixChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultOrix(Double orix)
    {
        Double oldValue = this.orix.getValue();
        this.orix.setDefaultValue(orix);

        Double currentValue = this.orix.getValue();

        fireOrixChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultOrix()
    {
        Double oldValue = this.orix.getValue();
        Double removedValue = this.orix.removeDefaultValue();
        Double currentValue = this.orix.getValue();

        fireOrixChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getOrixValueContainer()
    {
        return this.orix;
    }
    public final void fireOrixChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeOrixChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ORIX, oldValue, currentValue);
            afterOrixChanged(currentValue);
        }
    }

    public void beforeOrixChanged( Double orix)
    { }
    public void afterOrixChanged( Double orix)
    { }


    public boolean isOrixVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="5")
        ,@LocalizedTag(language="fr", name="column", value="5")
    })
    public final Double getOriy()
    {
         return this.oriy.getValue();
    }

    public final void setOriy(Double oriy)
    {

        Double oldValue = this.oriy.getValue();
        this.oriy.setValue(oriy);

        Double currentValue = this.oriy.getValue();

        fireOriyChange(currentValue, oldValue);
    }
    public final Double removeOriy()
    {
        Double oldValue = this.oriy.getValue();
        Double removedValue = this.oriy.removeValue();
        Double currentValue = this.oriy.getValue();

        fireOriyChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultOriy(Double oriy)
    {
        Double oldValue = this.oriy.getValue();
        this.oriy.setDefaultValue(oriy);

        Double currentValue = this.oriy.getValue();

        fireOriyChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultOriy()
    {
        Double oldValue = this.oriy.getValue();
        Double removedValue = this.oriy.removeDefaultValue();
        Double currentValue = this.oriy.getValue();

        fireOriyChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getOriyValueContainer()
    {
        return this.oriy;
    }
    public final void fireOriyChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeOriyChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ORIY, oldValue, currentValue);
            afterOriyChanged(currentValue);
        }
    }

    public void beforeOriyChanged( Double oriy)
    { }
    public void afterOriyChanged( Double oriy)
    { }


    public boolean isOriyVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="6")
        ,@LocalizedTag(language="fr", name="column", value="6")
    })
    public final String getTr1form()
    {
         return this.tr1form.getValue();
    }

    public final void setTr1form(String tr1form)
    {

        String oldValue = this.tr1form.getValue();
        this.tr1form.setValue(tr1form);

        String currentValue = this.tr1form.getValue();

        fireTr1formChange(currentValue, oldValue);
    }
    public final String removeTr1form()
    {
        String oldValue = this.tr1form.getValue();
        String removedValue = this.tr1form.removeValue();
        String currentValue = this.tr1form.getValue();

        fireTr1formChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultTr1form(String tr1form)
    {
        String oldValue = this.tr1form.getValue();
        this.tr1form.setDefaultValue(tr1form);

        String currentValue = this.tr1form.getValue();

        fireTr1formChange(currentValue, oldValue);
    }
    
    public final String removeDefaultTr1form()
    {
        String oldValue = this.tr1form.getValue();
        String removedValue = this.tr1form.removeDefaultValue();
        String currentValue = this.tr1form.getValue();

        fireTr1formChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getTr1formValueContainer()
    {
        return this.tr1form;
    }
    public final void fireTr1formChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeTr1formChanged(currentValue);
            firePropertyChange(PROPERTYNAME_TR1FORM, oldValue, currentValue);
            afterTr1formChanged(currentValue);
        }
    }

    public void beforeTr1formChanged( String tr1form)
    { }
    public void afterTr1formChanged( String tr1form)
    { }


    public boolean isTr1formVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="7")
        ,@LocalizedTag(language="fr", name="column", value="7")
    })
    public final Double getTr1posx()
    {
         return this.tr1posx.getValue();
    }

    public final void setTr1posx(Double tr1posx)
    {

        Double oldValue = this.tr1posx.getValue();
        this.tr1posx.setValue(tr1posx);

        Double currentValue = this.tr1posx.getValue();

        fireTr1posxChange(currentValue, oldValue);
    }
    public final Double removeTr1posx()
    {
        Double oldValue = this.tr1posx.getValue();
        Double removedValue = this.tr1posx.removeValue();
        Double currentValue = this.tr1posx.getValue();

        fireTr1posxChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultTr1posx(Double tr1posx)
    {
        Double oldValue = this.tr1posx.getValue();
        this.tr1posx.setDefaultValue(tr1posx);

        Double currentValue = this.tr1posx.getValue();

        fireTr1posxChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultTr1posx()
    {
        Double oldValue = this.tr1posx.getValue();
        Double removedValue = this.tr1posx.removeDefaultValue();
        Double currentValue = this.tr1posx.getValue();

        fireTr1posxChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getTr1posxValueContainer()
    {
        return this.tr1posx;
    }
    public final void fireTr1posxChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeTr1posxChanged(currentValue);
            firePropertyChange(PROPERTYNAME_TR1POSX, oldValue, currentValue);
            afterTr1posxChanged(currentValue);
        }
    }

    public void beforeTr1posxChanged( Double tr1posx)
    { }
    public void afterTr1posxChanged( Double tr1posx)
    { }


    public boolean isTr1posxVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="8")
        ,@LocalizedTag(language="fr", name="column", value="8")
    })
    public final Double getTr1posy()
    {
         return this.tr1posy.getValue();
    }

    public final void setTr1posy(Double tr1posy)
    {

        Double oldValue = this.tr1posy.getValue();
        this.tr1posy.setValue(tr1posy);

        Double currentValue = this.tr1posy.getValue();

        fireTr1posyChange(currentValue, oldValue);
    }
    public final Double removeTr1posy()
    {
        Double oldValue = this.tr1posy.getValue();
        Double removedValue = this.tr1posy.removeValue();
        Double currentValue = this.tr1posy.getValue();

        fireTr1posyChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultTr1posy(Double tr1posy)
    {
        Double oldValue = this.tr1posy.getValue();
        this.tr1posy.setDefaultValue(tr1posy);

        Double currentValue = this.tr1posy.getValue();

        fireTr1posyChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultTr1posy()
    {
        Double oldValue = this.tr1posy.getValue();
        Double removedValue = this.tr1posy.removeDefaultValue();
        Double currentValue = this.tr1posy.getValue();

        fireTr1posyChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getTr1posyValueContainer()
    {
        return this.tr1posy;
    }
    public final void fireTr1posyChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeTr1posyChanged(currentValue);
            firePropertyChange(PROPERTYNAME_TR1POSY, oldValue, currentValue);
            afterTr1posyChanged(currentValue);
        }
    }

    public void beforeTr1posyChanged( Double tr1posy)
    { }
    public void afterTr1posyChanged( Double tr1posy)
    { }


    public boolean isTr1posyVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="9")
        ,@LocalizedTag(language="fr", name="column", value="9")
    })
    public final Double getTr1width()
    {
         return this.tr1width.getValue();
    }

    public final void setTr1width(Double tr1width)
    {

        Double oldValue = this.tr1width.getValue();
        this.tr1width.setValue(tr1width);

        Double currentValue = this.tr1width.getValue();

        fireTr1widthChange(currentValue, oldValue);
    }
    public final Double removeTr1width()
    {
        Double oldValue = this.tr1width.getValue();
        Double removedValue = this.tr1width.removeValue();
        Double currentValue = this.tr1width.getValue();

        fireTr1widthChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultTr1width(Double tr1width)
    {
        Double oldValue = this.tr1width.getValue();
        this.tr1width.setDefaultValue(tr1width);

        Double currentValue = this.tr1width.getValue();

        fireTr1widthChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultTr1width()
    {
        Double oldValue = this.tr1width.getValue();
        Double removedValue = this.tr1width.removeDefaultValue();
        Double currentValue = this.tr1width.getValue();

        fireTr1widthChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getTr1widthValueContainer()
    {
        return this.tr1width;
    }
    public final void fireTr1widthChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeTr1widthChanged(currentValue);
            firePropertyChange(PROPERTYNAME_TR1WIDTH, oldValue, currentValue);
            afterTr1widthChanged(currentValue);
        }
    }

    public void beforeTr1widthChanged( Double tr1width)
    { }
    public void afterTr1widthChanged( Double tr1width)
    { }


    public boolean isTr1widthVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="10")
        ,@LocalizedTag(language="fr", name="column", value="10")
    })
    public final Double getTr1height()
    {
         return this.tr1height.getValue();
    }

    public final void setTr1height(Double tr1height)
    {

        Double oldValue = this.tr1height.getValue();
        this.tr1height.setValue(tr1height);

        Double currentValue = this.tr1height.getValue();

        fireTr1heightChange(currentValue, oldValue);
    }
    public final Double removeTr1height()
    {
        Double oldValue = this.tr1height.getValue();
        Double removedValue = this.tr1height.removeValue();
        Double currentValue = this.tr1height.getValue();

        fireTr1heightChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultTr1height(Double tr1height)
    {
        Double oldValue = this.tr1height.getValue();
        this.tr1height.setDefaultValue(tr1height);

        Double currentValue = this.tr1height.getValue();

        fireTr1heightChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultTr1height()
    {
        Double oldValue = this.tr1height.getValue();
        Double removedValue = this.tr1height.removeDefaultValue();
        Double currentValue = this.tr1height.getValue();

        fireTr1heightChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getTr1heightValueContainer()
    {
        return this.tr1height;
    }
    public final void fireTr1heightChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeTr1heightChanged(currentValue);
            firePropertyChange(PROPERTYNAME_TR1HEIGHT, oldValue, currentValue);
            afterTr1heightChanged(currentValue);
        }
    }

    public void beforeTr1heightChanged( Double tr1height)
    { }
    public void afterTr1heightChanged( Double tr1height)
    { }


    public boolean isTr1heightVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="11")
        ,@LocalizedTag(language="fr", name="column", value="11")
    })
    public final String getTr2form()
    {
         return this.tr2form.getValue();
    }

    public final void setTr2form(String tr2form)
    {

        String oldValue = this.tr2form.getValue();
        this.tr2form.setValue(tr2form);

        String currentValue = this.tr2form.getValue();

        fireTr2formChange(currentValue, oldValue);
    }
    public final String removeTr2form()
    {
        String oldValue = this.tr2form.getValue();
        String removedValue = this.tr2form.removeValue();
        String currentValue = this.tr2form.getValue();

        fireTr2formChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultTr2form(String tr2form)
    {
        String oldValue = this.tr2form.getValue();
        this.tr2form.setDefaultValue(tr2form);

        String currentValue = this.tr2form.getValue();

        fireTr2formChange(currentValue, oldValue);
    }
    
    public final String removeDefaultTr2form()
    {
        String oldValue = this.tr2form.getValue();
        String removedValue = this.tr2form.removeDefaultValue();
        String currentValue = this.tr2form.getValue();

        fireTr2formChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getTr2formValueContainer()
    {
        return this.tr2form;
    }
    public final void fireTr2formChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeTr2formChanged(currentValue);
            firePropertyChange(PROPERTYNAME_TR2FORM, oldValue, currentValue);
            afterTr2formChanged(currentValue);
        }
    }

    public void beforeTr2formChanged( String tr2form)
    { }
    public void afterTr2formChanged( String tr2form)
    { }


    public boolean isTr2formVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="12")
        ,@LocalizedTag(language="fr", name="column", value="12")
    })
    public final Double getTr2posx()
    {
         return this.tr2posx.getValue();
    }

    public final void setTr2posx(Double tr2posx)
    {

        Double oldValue = this.tr2posx.getValue();
        this.tr2posx.setValue(tr2posx);

        Double currentValue = this.tr2posx.getValue();

        fireTr2posxChange(currentValue, oldValue);
    }
    public final Double removeTr2posx()
    {
        Double oldValue = this.tr2posx.getValue();
        Double removedValue = this.tr2posx.removeValue();
        Double currentValue = this.tr2posx.getValue();

        fireTr2posxChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultTr2posx(Double tr2posx)
    {
        Double oldValue = this.tr2posx.getValue();
        this.tr2posx.setDefaultValue(tr2posx);

        Double currentValue = this.tr2posx.getValue();

        fireTr2posxChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultTr2posx()
    {
        Double oldValue = this.tr2posx.getValue();
        Double removedValue = this.tr2posx.removeDefaultValue();
        Double currentValue = this.tr2posx.getValue();

        fireTr2posxChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getTr2posxValueContainer()
    {
        return this.tr2posx;
    }
    public final void fireTr2posxChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeTr2posxChanged(currentValue);
            firePropertyChange(PROPERTYNAME_TR2POSX, oldValue, currentValue);
            afterTr2posxChanged(currentValue);
        }
    }

    public void beforeTr2posxChanged( Double tr2posx)
    { }
    public void afterTr2posxChanged( Double tr2posx)
    { }


    public boolean isTr2posxVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="13")
        ,@LocalizedTag(language="fr", name="column", value="13")
    })
    public final Double getTr2posy()
    {
         return this.tr2posy.getValue();
    }

    public final void setTr2posy(Double tr2posy)
    {

        Double oldValue = this.tr2posy.getValue();
        this.tr2posy.setValue(tr2posy);

        Double currentValue = this.tr2posy.getValue();

        fireTr2posyChange(currentValue, oldValue);
    }
    public final Double removeTr2posy()
    {
        Double oldValue = this.tr2posy.getValue();
        Double removedValue = this.tr2posy.removeValue();
        Double currentValue = this.tr2posy.getValue();

        fireTr2posyChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultTr2posy(Double tr2posy)
    {
        Double oldValue = this.tr2posy.getValue();
        this.tr2posy.setDefaultValue(tr2posy);

        Double currentValue = this.tr2posy.getValue();

        fireTr2posyChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultTr2posy()
    {
        Double oldValue = this.tr2posy.getValue();
        Double removedValue = this.tr2posy.removeDefaultValue();
        Double currentValue = this.tr2posy.getValue();

        fireTr2posyChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getTr2posyValueContainer()
    {
        return this.tr2posy;
    }
    public final void fireTr2posyChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeTr2posyChanged(currentValue);
            firePropertyChange(PROPERTYNAME_TR2POSY, oldValue, currentValue);
            afterTr2posyChanged(currentValue);
        }
    }

    public void beforeTr2posyChanged( Double tr2posy)
    { }
    public void afterTr2posyChanged( Double tr2posy)
    { }


    public boolean isTr2posyVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="14")
        ,@LocalizedTag(language="fr", name="column", value="14")
    })
    public final Double getTr2width()
    {
         return this.tr2width.getValue();
    }

    public final void setTr2width(Double tr2width)
    {

        Double oldValue = this.tr2width.getValue();
        this.tr2width.setValue(tr2width);

        Double currentValue = this.tr2width.getValue();

        fireTr2widthChange(currentValue, oldValue);
    }
    public final Double removeTr2width()
    {
        Double oldValue = this.tr2width.getValue();
        Double removedValue = this.tr2width.removeValue();
        Double currentValue = this.tr2width.getValue();

        fireTr2widthChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultTr2width(Double tr2width)
    {
        Double oldValue = this.tr2width.getValue();
        this.tr2width.setDefaultValue(tr2width);

        Double currentValue = this.tr2width.getValue();

        fireTr2widthChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultTr2width()
    {
        Double oldValue = this.tr2width.getValue();
        Double removedValue = this.tr2width.removeDefaultValue();
        Double currentValue = this.tr2width.getValue();

        fireTr2widthChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getTr2widthValueContainer()
    {
        return this.tr2width;
    }
    public final void fireTr2widthChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeTr2widthChanged(currentValue);
            firePropertyChange(PROPERTYNAME_TR2WIDTH, oldValue, currentValue);
            afterTr2widthChanged(currentValue);
        }
    }

    public void beforeTr2widthChanged( Double tr2width)
    { }
    public void afterTr2widthChanged( Double tr2width)
    { }


    public boolean isTr2widthVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="15")
        ,@LocalizedTag(language="fr", name="column", value="15")
    })
    public final Double getTr2height()
    {
         return this.tr2height.getValue();
    }

    public final void setTr2height(Double tr2height)
    {

        Double oldValue = this.tr2height.getValue();
        this.tr2height.setValue(tr2height);

        Double currentValue = this.tr2height.getValue();

        fireTr2heightChange(currentValue, oldValue);
    }
    public final Double removeTr2height()
    {
        Double oldValue = this.tr2height.getValue();
        Double removedValue = this.tr2height.removeValue();
        Double currentValue = this.tr2height.getValue();

        fireTr2heightChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultTr2height(Double tr2height)
    {
        Double oldValue = this.tr2height.getValue();
        this.tr2height.setDefaultValue(tr2height);

        Double currentValue = this.tr2height.getValue();

        fireTr2heightChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultTr2height()
    {
        Double oldValue = this.tr2height.getValue();
        Double removedValue = this.tr2height.removeDefaultValue();
        Double currentValue = this.tr2height.getValue();

        fireTr2heightChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getTr2heightValueContainer()
    {
        return this.tr2height;
    }
    public final void fireTr2heightChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeTr2heightChanged(currentValue);
            firePropertyChange(PROPERTYNAME_TR2HEIGHT, oldValue, currentValue);
            afterTr2heightChanged(currentValue);
        }
    }

    public void beforeTr2heightChanged( Double tr2height)
    { }
    public void afterTr2heightChanged( Double tr2height)
    { }


    public boolean isTr2heightVisible()
    {
        return false;
    }

}
