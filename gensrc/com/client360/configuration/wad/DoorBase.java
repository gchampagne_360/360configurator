// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.client360.configuration.wad.enums.*; 

import com.netappsid.erp.configurator.*;
import com.netappsid.erp.configurator.annotations.*;

import org.apache.log4j.Logger;

import org.jscience.physics.measures.*;
import javax.measure.quantities.*;

import java.awt.geom.Line2D;
import java.awt.geom.Arc2D;
import java.awt.geom.Point2D;

import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.netappsid.rendering.common.advanced.Profile;

import com.netappsid.commonutils.geo.Form;
import com.netappsid.commonutils.geo.NAIDShape;
import com.netappsid.commonutils.geo.CornerType;

public abstract class DoorBase extends com.netappsid.wadconfigurator.Door
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(DoorBase.class);

    // attributes
    private ValueContainer<Boolean> thermos = new ValueContainer<Boolean>(this);
    private ValueContainer<ChoiceEyeHole> choiceEyeHole = new ValueContainer<ChoiceEyeHole>(this);
    private ValueContainer<ChoiceSlab360> choiceSlab360 = new ValueContainer<ChoiceSlab360>(this);
    private ValueContainer<ChoiceInsulatedGlass> choiceInsulatedGlass = new ValueContainer<ChoiceInsulatedGlass>(this);
    private ValueContainer<ChoiceAcc2> choiceAcc2 = new ValueContainer<ChoiceAcc2>(this);
    private ValueContainer<Handlesset> handlesset = new ValueContainer<Handlesset>(this);
    private ValueContainer<ChoiceSlabThermos> choiceSlabThermos = new ValueContainer<ChoiceSlabThermos>(this);
    private ValueContainer<OtherSlabModel> otherSlabModel = new ValueContainer<OtherSlabModel>(this);
    private ValueContainer<ChoiceLetterBox> choiceLetterBox = new ValueContainer<ChoiceLetterBox>(this);
    private ValueContainer<ChoiceStainedGlass> choiceStainedGlass = new ValueContainer<ChoiceStainedGlass>(this);
    private ValueContainer<ChoiceKnocker> choiceKnocker = new ValueContainer<ChoiceKnocker>(this);
    private ValueContainer<ChoiceHandleSet> choiceHandleSet = new ValueContainer<ChoiceHandleSet>(this);
    private ValueContainer<ChoiceAcc1> choiceAcc1 = new ValueContainer<ChoiceAcc1>(this);
    private ValueContainer<GlassType> glassType = new ValueContainer<GlassType>(this);
    private ValueContainer<ChoicePanel> choicePanel = new ValueContainer<ChoicePanel>(this);
    private ValueContainer<ChoiceDeadBolt> choiceDeadBolt = new ValueContainer<ChoiceDeadBolt>(this);

    // Bound properties

    public static final String PROPERTYNAME_THERMOS = "thermos";  
    public static final String PROPERTYNAME_CHOICEEYEHOLE = "choiceEyeHole";  
    public static final String PROPERTYNAME_CHOICESLAB360 = "choiceSlab360";  
    public static final String PROPERTYNAME_CHOICEINSULATEDGLASS = "choiceInsulatedGlass";  
    public static final String PROPERTYNAME_CHOICEACC2 = "choiceAcc2";  
    public static final String PROPERTYNAME_HANDLESSET = "handlesset";  
    public static final String PROPERTYNAME_CHOICESLABTHERMOS = "choiceSlabThermos";  
    public static final String PROPERTYNAME_OTHERSLABMODEL = "otherSlabModel";  
    public static final String PROPERTYNAME_CHOICELETTERBOX = "choiceLetterBox";  
    public static final String PROPERTYNAME_CHOICESTAINEDGLASS = "choiceStainedGlass";  
    public static final String PROPERTYNAME_CHOICEKNOCKER = "choiceKnocker";  
    public static final String PROPERTYNAME_CHOICEHANDLESET = "choiceHandleSet";  
    public static final String PROPERTYNAME_CHOICEACC1 = "choiceAcc1";  
    public static final String PROPERTYNAME_GLASSTYPE = "glassType";  
    public static final String PROPERTYNAME_CHOICEPANEL = "choicePanel";  
    public static final String PROPERTYNAME_CHOICEDEADBOLT = "choiceDeadBolt";  

    // Constructors

    public DoorBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.Door"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Thermos")
        ,@LocalizedTag(language="fr", name="label", value="Thermos")
    })
    public final Boolean getThermos()
    {
         return this.thermos.getValue();
    }

    public final void setThermos(Boolean thermos)
    {

        Boolean oldValue = this.thermos.getValue();
        this.thermos.setValue(thermos);

        Boolean currentValue = this.thermos.getValue();

        fireThermosChange(currentValue, oldValue);
    }
    public final Boolean removeThermos()
    {
        Boolean oldValue = this.thermos.getValue();
        Boolean removedValue = this.thermos.removeValue();
        Boolean currentValue = this.thermos.getValue();

        fireThermosChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultThermos(Boolean thermos)
    {
        Boolean oldValue = this.thermos.getValue();
        this.thermos.setDefaultValue(thermos);

        Boolean currentValue = this.thermos.getValue();

        fireThermosChange(currentValue, oldValue);
    }
    
    public final Boolean removeDefaultThermos()
    {
        Boolean oldValue = this.thermos.getValue();
        Boolean removedValue = this.thermos.removeDefaultValue();
        Boolean currentValue = this.thermos.getValue();

        fireThermosChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Boolean> getThermosValueContainer()
    {
        return this.thermos;
    }
    public final void fireThermosChange(Boolean currentValue, Boolean oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeThermosChanged(currentValue);
            firePropertyChange(PROPERTYNAME_THERMOS, oldValue, currentValue);
            afterThermosChanged(currentValue);
        }
    }

    public void beforeThermosChanged( Boolean thermos)
    { }
    public void afterThermosChanged( Boolean thermos)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="EYE HOLE")
        ,@LocalizedTag(language="fr", name="label", value="OEIL MAGIQUE")
        ,@LocalizedTag(language="fr", name="null", value="Sans")
        ,@LocalizedTag(language="en", name="null", value="Without")
        ,@LocalizedTag(language="fr", name="notnull", value="Avec")
        ,@LocalizedTag(language="en", name="notnull", value="With")
    })
    public final ChoiceEyeHole getChoiceEyeHole()
    {
         return this.choiceEyeHole.getValue();
    }

    public final void setChoiceEyeHole(ChoiceEyeHole choiceEyeHole)
    {

        if (choiceEyeHole != null)
        {
            acquire(choiceEyeHole, "choiceEyeHole");
        }
        ChoiceEyeHole oldValue = this.choiceEyeHole.getValue();
        this.choiceEyeHole.setValue(choiceEyeHole);

        ChoiceEyeHole currentValue = this.choiceEyeHole.getValue();

        fireChoiceEyeHoleChange(currentValue, oldValue);
    }
    public final ChoiceEyeHole removeChoiceEyeHole()
    {
        ChoiceEyeHole oldValue = this.choiceEyeHole.getValue();
        ChoiceEyeHole removedValue = this.choiceEyeHole.removeValue();
        ChoiceEyeHole currentValue = this.choiceEyeHole.getValue();

        fireChoiceEyeHoleChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceEyeHole(ChoiceEyeHole choiceEyeHole)
    {
        if (choiceEyeHole != null)
        {
            acquire(choiceEyeHole, "choiceEyeHole");
        }
        ChoiceEyeHole oldValue = this.choiceEyeHole.getValue();
        this.choiceEyeHole.setDefaultValue(choiceEyeHole);

        ChoiceEyeHole currentValue = this.choiceEyeHole.getValue();

        fireChoiceEyeHoleChange(currentValue, oldValue);
    }
    
    public final ChoiceEyeHole removeDefaultChoiceEyeHole()
    {
        ChoiceEyeHole oldValue = this.choiceEyeHole.getValue();
        ChoiceEyeHole removedValue = this.choiceEyeHole.removeDefaultValue();
        ChoiceEyeHole currentValue = this.choiceEyeHole.getValue();

        fireChoiceEyeHoleChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceEyeHole> getChoiceEyeHoleValueContainer()
    {
        return this.choiceEyeHole;
    }
    public final void fireChoiceEyeHoleChange(ChoiceEyeHole currentValue, ChoiceEyeHole oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceEyeHoleChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEEYEHOLE, oldValue, currentValue);
            afterChoiceEyeHoleChanged(currentValue);
        }
    }

    public void beforeChoiceEyeHoleChanged( ChoiceEyeHole choiceEyeHole)
    { }
    public void afterChoiceEyeHoleChanged( ChoiceEyeHole choiceEyeHole)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Slab")
        ,@LocalizedTag(language="fr", name="label", value="Ouvrant")
    })
    public final ChoiceSlab360 getChoiceSlab360()
    {
         return this.choiceSlab360.getValue();
    }

    public final void setChoiceSlab360(ChoiceSlab360 choiceSlab360)
    {

        if (choiceSlab360 != null)
        {
            acquire(choiceSlab360, "choiceSlab360");
        }
        ChoiceSlab360 oldValue = this.choiceSlab360.getValue();
        this.choiceSlab360.setValue(choiceSlab360);

        ChoiceSlab360 currentValue = this.choiceSlab360.getValue();

        fireChoiceSlab360Change(currentValue, oldValue);
    }
    public final ChoiceSlab360 removeChoiceSlab360()
    {
        ChoiceSlab360 oldValue = this.choiceSlab360.getValue();
        ChoiceSlab360 removedValue = this.choiceSlab360.removeValue();
        ChoiceSlab360 currentValue = this.choiceSlab360.getValue();

        fireChoiceSlab360Change(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceSlab360(ChoiceSlab360 choiceSlab360)
    {
        if (choiceSlab360 != null)
        {
            acquire(choiceSlab360, "choiceSlab360");
        }
        ChoiceSlab360 oldValue = this.choiceSlab360.getValue();
        this.choiceSlab360.setDefaultValue(choiceSlab360);

        ChoiceSlab360 currentValue = this.choiceSlab360.getValue();

        fireChoiceSlab360Change(currentValue, oldValue);
    }
    
    public final ChoiceSlab360 removeDefaultChoiceSlab360()
    {
        ChoiceSlab360 oldValue = this.choiceSlab360.getValue();
        ChoiceSlab360 removedValue = this.choiceSlab360.removeDefaultValue();
        ChoiceSlab360 currentValue = this.choiceSlab360.getValue();

        fireChoiceSlab360Change(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceSlab360> getChoiceSlab360ValueContainer()
    {
        return this.choiceSlab360;
    }
    public final void fireChoiceSlab360Change(ChoiceSlab360 currentValue, ChoiceSlab360 oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceSlab360Changed(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICESLAB360, oldValue, currentValue);
            afterChoiceSlab360Changed(currentValue);
        }
    }

    public void beforeChoiceSlab360Changed( ChoiceSlab360 choiceSlab360)
    { }
    public void afterChoiceSlab360Changed( ChoiceSlab360 choiceSlab360)
    { }


    public boolean isChoiceSlab360Mandatory()
    {
        return true;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Sealed Unit")
        ,@LocalizedTag(language="fr", name="label", value="Vitrage")
    })
    public final ChoiceInsulatedGlass getChoiceInsulatedGlass()
    {
         return this.choiceInsulatedGlass.getValue();
    }

    public final void setChoiceInsulatedGlass(ChoiceInsulatedGlass choiceInsulatedGlass)
    {

        if (choiceInsulatedGlass != null)
        {
            acquire(choiceInsulatedGlass, "choiceInsulatedGlass");
        }
        ChoiceInsulatedGlass oldValue = this.choiceInsulatedGlass.getValue();
        this.choiceInsulatedGlass.setValue(choiceInsulatedGlass);

        ChoiceInsulatedGlass currentValue = this.choiceInsulatedGlass.getValue();

        fireChoiceInsulatedGlassChange(currentValue, oldValue);
    }
    public final ChoiceInsulatedGlass removeChoiceInsulatedGlass()
    {
        ChoiceInsulatedGlass oldValue = this.choiceInsulatedGlass.getValue();
        ChoiceInsulatedGlass removedValue = this.choiceInsulatedGlass.removeValue();
        ChoiceInsulatedGlass currentValue = this.choiceInsulatedGlass.getValue();

        fireChoiceInsulatedGlassChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceInsulatedGlass(ChoiceInsulatedGlass choiceInsulatedGlass)
    {
        if (choiceInsulatedGlass != null)
        {
            acquire(choiceInsulatedGlass, "choiceInsulatedGlass");
        }
        ChoiceInsulatedGlass oldValue = this.choiceInsulatedGlass.getValue();
        this.choiceInsulatedGlass.setDefaultValue(choiceInsulatedGlass);

        ChoiceInsulatedGlass currentValue = this.choiceInsulatedGlass.getValue();

        fireChoiceInsulatedGlassChange(currentValue, oldValue);
    }
    
    public final ChoiceInsulatedGlass removeDefaultChoiceInsulatedGlass()
    {
        ChoiceInsulatedGlass oldValue = this.choiceInsulatedGlass.getValue();
        ChoiceInsulatedGlass removedValue = this.choiceInsulatedGlass.removeDefaultValue();
        ChoiceInsulatedGlass currentValue = this.choiceInsulatedGlass.getValue();

        fireChoiceInsulatedGlassChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceInsulatedGlass> getChoiceInsulatedGlassValueContainer()
    {
        return this.choiceInsulatedGlass;
    }
    public final void fireChoiceInsulatedGlassChange(ChoiceInsulatedGlass currentValue, ChoiceInsulatedGlass oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceInsulatedGlassChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEINSULATEDGLASS, oldValue, currentValue);
            afterChoiceInsulatedGlassChanged(currentValue);
        }
    }

    public void beforeChoiceInsulatedGlassChanged( ChoiceInsulatedGlass choiceInsulatedGlass)
    { }
    public void afterChoiceInsulatedGlassChanged( ChoiceInsulatedGlass choiceInsulatedGlass)
    { }


    public boolean isChoiceInsulatedGlassMandatory()
    {
        return true;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="fr", name="label", value="Choice d\'accessoires 2")
    })
    public final ChoiceAcc2 getChoiceAcc2()
    {
         return this.choiceAcc2.getValue();
    }

    public final void setChoiceAcc2(ChoiceAcc2 choiceAcc2)
    {

        ChoiceAcc2 oldValue = this.choiceAcc2.getValue();
        this.choiceAcc2.setValue(choiceAcc2);

        ChoiceAcc2 currentValue = this.choiceAcc2.getValue();

        fireChoiceAcc2Change(currentValue, oldValue);
    }
    public final ChoiceAcc2 removeChoiceAcc2()
    {
        ChoiceAcc2 oldValue = this.choiceAcc2.getValue();
        ChoiceAcc2 removedValue = this.choiceAcc2.removeValue();
        ChoiceAcc2 currentValue = this.choiceAcc2.getValue();

        fireChoiceAcc2Change(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceAcc2(ChoiceAcc2 choiceAcc2)
    {
        ChoiceAcc2 oldValue = this.choiceAcc2.getValue();
        this.choiceAcc2.setDefaultValue(choiceAcc2);

        ChoiceAcc2 currentValue = this.choiceAcc2.getValue();

        fireChoiceAcc2Change(currentValue, oldValue);
    }
    
    public final ChoiceAcc2 removeDefaultChoiceAcc2()
    {
        ChoiceAcc2 oldValue = this.choiceAcc2.getValue();
        ChoiceAcc2 removedValue = this.choiceAcc2.removeDefaultValue();
        ChoiceAcc2 currentValue = this.choiceAcc2.getValue();

        fireChoiceAcc2Change(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceAcc2> getChoiceAcc2ValueContainer()
    {
        return this.choiceAcc2;
    }
    public final void fireChoiceAcc2Change(ChoiceAcc2 currentValue, ChoiceAcc2 oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceAcc2Changed(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEACC2, oldValue, currentValue);
            afterChoiceAcc2Changed(currentValue);
        }
    }

    public void beforeChoiceAcc2Changed( ChoiceAcc2 choiceAcc2)
    { }
    public void afterChoiceAcc2Changed( ChoiceAcc2 choiceAcc2)
    { }


    public boolean isChoiceAcc2Mandatory()
    {
        return true;
    }
    @LocalizedTags
    ({
    })
    public final Handlesset getHandlesset()
    {
         return this.handlesset.getValue();
    }

    public final void setHandlesset(Handlesset handlesset)
    {

        if (handlesset != null)
        {
            acquire(handlesset, "handlesset");
        }
        Handlesset oldValue = this.handlesset.getValue();
        this.handlesset.setValue(handlesset);

        Handlesset currentValue = this.handlesset.getValue();

        fireHandlessetChange(currentValue, oldValue);
    }
    public final Handlesset removeHandlesset()
    {
        Handlesset oldValue = this.handlesset.getValue();
        Handlesset removedValue = this.handlesset.removeValue();
        Handlesset currentValue = this.handlesset.getValue();

        fireHandlessetChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultHandlesset(Handlesset handlesset)
    {
        if (handlesset != null)
        {
            acquire(handlesset, "handlesset");
        }
        Handlesset oldValue = this.handlesset.getValue();
        this.handlesset.setDefaultValue(handlesset);

        Handlesset currentValue = this.handlesset.getValue();

        fireHandlessetChange(currentValue, oldValue);
    }
    
    public final Handlesset removeDefaultHandlesset()
    {
        Handlesset oldValue = this.handlesset.getValue();
        Handlesset removedValue = this.handlesset.removeDefaultValue();
        Handlesset currentValue = this.handlesset.getValue();

        fireHandlessetChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Handlesset> getHandlessetValueContainer()
    {
        return this.handlesset;
    }
    public final void fireHandlessetChange(Handlesset currentValue, Handlesset oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeHandlessetChanged(currentValue);
            firePropertyChange(PROPERTYNAME_HANDLESSET, oldValue, currentValue);
            afterHandlessetChanged(currentValue);
        }
    }

    public void beforeHandlessetChanged( Handlesset handlesset)
    { }
    public void afterHandlessetChanged( Handlesset handlesset)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Thermos")
        ,@LocalizedTag(language="fr", name="label", value="Thermos")
    })
    public final ChoiceSlabThermos getChoiceSlabThermos()
    {
         return this.choiceSlabThermos.getValue();
    }

    public final void setChoiceSlabThermos(ChoiceSlabThermos choiceSlabThermos)
    {

        if (choiceSlabThermos != null)
        {
            acquire(choiceSlabThermos, "choiceSlabThermos");
        }
        ChoiceSlabThermos oldValue = this.choiceSlabThermos.getValue();
        this.choiceSlabThermos.setValue(choiceSlabThermos);

        ChoiceSlabThermos currentValue = this.choiceSlabThermos.getValue();

        fireChoiceSlabThermosChange(currentValue, oldValue);
    }
    public final ChoiceSlabThermos removeChoiceSlabThermos()
    {
        ChoiceSlabThermos oldValue = this.choiceSlabThermos.getValue();
        ChoiceSlabThermos removedValue = this.choiceSlabThermos.removeValue();
        ChoiceSlabThermos currentValue = this.choiceSlabThermos.getValue();

        fireChoiceSlabThermosChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceSlabThermos(ChoiceSlabThermos choiceSlabThermos)
    {
        if (choiceSlabThermos != null)
        {
            acquire(choiceSlabThermos, "choiceSlabThermos");
        }
        ChoiceSlabThermos oldValue = this.choiceSlabThermos.getValue();
        this.choiceSlabThermos.setDefaultValue(choiceSlabThermos);

        ChoiceSlabThermos currentValue = this.choiceSlabThermos.getValue();

        fireChoiceSlabThermosChange(currentValue, oldValue);
    }
    
    public final ChoiceSlabThermos removeDefaultChoiceSlabThermos()
    {
        ChoiceSlabThermos oldValue = this.choiceSlabThermos.getValue();
        ChoiceSlabThermos removedValue = this.choiceSlabThermos.removeDefaultValue();
        ChoiceSlabThermos currentValue = this.choiceSlabThermos.getValue();

        fireChoiceSlabThermosChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceSlabThermos> getChoiceSlabThermosValueContainer()
    {
        return this.choiceSlabThermos;
    }
    public final void fireChoiceSlabThermosChange(ChoiceSlabThermos currentValue, ChoiceSlabThermos oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceSlabThermosChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICESLABTHERMOS, oldValue, currentValue);
            afterChoiceSlabThermosChanged(currentValue);
        }
    }

    public void beforeChoiceSlabThermosChanged( ChoiceSlabThermos choiceSlabThermos)
    { }
    public void afterChoiceSlabThermosChanged( ChoiceSlabThermos choiceSlabThermos)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="fr", name="desc", value="Autre type de slab")
        ,@LocalizedTag(language="fr", name="label", value="Autre type de slab")
    })
    public final OtherSlabModel getOtherSlabModel()
    {
         return this.otherSlabModel.getValue();
    }

    public final void setOtherSlabModel(OtherSlabModel otherSlabModel)
    {

        OtherSlabModel oldValue = this.otherSlabModel.getValue();
        this.otherSlabModel.setValue(otherSlabModel);

        OtherSlabModel currentValue = this.otherSlabModel.getValue();

        fireOtherSlabModelChange(currentValue, oldValue);
    }
    public final OtherSlabModel removeOtherSlabModel()
    {
        OtherSlabModel oldValue = this.otherSlabModel.getValue();
        OtherSlabModel removedValue = this.otherSlabModel.removeValue();
        OtherSlabModel currentValue = this.otherSlabModel.getValue();

        fireOtherSlabModelChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultOtherSlabModel(OtherSlabModel otherSlabModel)
    {
        OtherSlabModel oldValue = this.otherSlabModel.getValue();
        this.otherSlabModel.setDefaultValue(otherSlabModel);

        OtherSlabModel currentValue = this.otherSlabModel.getValue();

        fireOtherSlabModelChange(currentValue, oldValue);
    }
    
    public final OtherSlabModel removeDefaultOtherSlabModel()
    {
        OtherSlabModel oldValue = this.otherSlabModel.getValue();
        OtherSlabModel removedValue = this.otherSlabModel.removeDefaultValue();
        OtherSlabModel currentValue = this.otherSlabModel.getValue();

        fireOtherSlabModelChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<OtherSlabModel> getOtherSlabModelValueContainer()
    {
        return this.otherSlabModel;
    }
    public final void fireOtherSlabModelChange(OtherSlabModel currentValue, OtherSlabModel oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeOtherSlabModelChanged(currentValue);
            firePropertyChange(PROPERTYNAME_OTHERSLABMODEL, oldValue, currentValue);
            afterOtherSlabModelChanged(currentValue);
        }
    }

    public void beforeOtherSlabModelChanged( OtherSlabModel otherSlabModel)
    { }
    public void afterOtherSlabModelChanged( OtherSlabModel otherSlabModel)
    { }


    public boolean isOtherSlabModelMandatory()
    {
        return true;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Letter Box")
        ,@LocalizedTag(language="fr", name="label", value="Bo�te aux lettres")
        ,@LocalizedTag(language="fr", name="null", value="Sans")
        ,@LocalizedTag(language="en", name="null", value="Without")
        ,@LocalizedTag(language="fr", name="notnull", value="Avec")
        ,@LocalizedTag(language="en", name="notnull", value="With")
    })
    public final ChoiceLetterBox getChoiceLetterBox()
    {
         return this.choiceLetterBox.getValue();
    }

    public final void setChoiceLetterBox(ChoiceLetterBox choiceLetterBox)
    {

        if (choiceLetterBox != null)
        {
            acquire(choiceLetterBox, "choiceLetterBox");
        }
        ChoiceLetterBox oldValue = this.choiceLetterBox.getValue();
        this.choiceLetterBox.setValue(choiceLetterBox);

        ChoiceLetterBox currentValue = this.choiceLetterBox.getValue();

        fireChoiceLetterBoxChange(currentValue, oldValue);
    }
    public final ChoiceLetterBox removeChoiceLetterBox()
    {
        ChoiceLetterBox oldValue = this.choiceLetterBox.getValue();
        ChoiceLetterBox removedValue = this.choiceLetterBox.removeValue();
        ChoiceLetterBox currentValue = this.choiceLetterBox.getValue();

        fireChoiceLetterBoxChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceLetterBox(ChoiceLetterBox choiceLetterBox)
    {
        if (choiceLetterBox != null)
        {
            acquire(choiceLetterBox, "choiceLetterBox");
        }
        ChoiceLetterBox oldValue = this.choiceLetterBox.getValue();
        this.choiceLetterBox.setDefaultValue(choiceLetterBox);

        ChoiceLetterBox currentValue = this.choiceLetterBox.getValue();

        fireChoiceLetterBoxChange(currentValue, oldValue);
    }
    
    public final ChoiceLetterBox removeDefaultChoiceLetterBox()
    {
        ChoiceLetterBox oldValue = this.choiceLetterBox.getValue();
        ChoiceLetterBox removedValue = this.choiceLetterBox.removeDefaultValue();
        ChoiceLetterBox currentValue = this.choiceLetterBox.getValue();

        fireChoiceLetterBoxChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceLetterBox> getChoiceLetterBoxValueContainer()
    {
        return this.choiceLetterBox;
    }
    public final void fireChoiceLetterBoxChange(ChoiceLetterBox currentValue, ChoiceLetterBox oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceLetterBoxChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICELETTERBOX, oldValue, currentValue);
            afterChoiceLetterBoxChanged(currentValue);
        }
    }

    public void beforeChoiceLetterBoxChanged( ChoiceLetterBox choiceLetterBox)
    { }
    public void afterChoiceLetterBoxChanged( ChoiceLetterBox choiceLetterBox)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Stained Glass")
        ,@LocalizedTag(language="fr", name="label", value="Vitraux")
    })
    public final ChoiceStainedGlass getChoiceStainedGlass()
    {
         return this.choiceStainedGlass.getValue();
    }

    public final void setChoiceStainedGlass(ChoiceStainedGlass choiceStainedGlass)
    {

        if (choiceStainedGlass != null)
        {
            acquire(choiceStainedGlass, "choiceStainedGlass");
        }
        ChoiceStainedGlass oldValue = this.choiceStainedGlass.getValue();
        this.choiceStainedGlass.setValue(choiceStainedGlass);

        ChoiceStainedGlass currentValue = this.choiceStainedGlass.getValue();

        fireChoiceStainedGlassChange(currentValue, oldValue);
    }
    public final ChoiceStainedGlass removeChoiceStainedGlass()
    {
        ChoiceStainedGlass oldValue = this.choiceStainedGlass.getValue();
        ChoiceStainedGlass removedValue = this.choiceStainedGlass.removeValue();
        ChoiceStainedGlass currentValue = this.choiceStainedGlass.getValue();

        fireChoiceStainedGlassChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceStainedGlass(ChoiceStainedGlass choiceStainedGlass)
    {
        if (choiceStainedGlass != null)
        {
            acquire(choiceStainedGlass, "choiceStainedGlass");
        }
        ChoiceStainedGlass oldValue = this.choiceStainedGlass.getValue();
        this.choiceStainedGlass.setDefaultValue(choiceStainedGlass);

        ChoiceStainedGlass currentValue = this.choiceStainedGlass.getValue();

        fireChoiceStainedGlassChange(currentValue, oldValue);
    }
    
    public final ChoiceStainedGlass removeDefaultChoiceStainedGlass()
    {
        ChoiceStainedGlass oldValue = this.choiceStainedGlass.getValue();
        ChoiceStainedGlass removedValue = this.choiceStainedGlass.removeDefaultValue();
        ChoiceStainedGlass currentValue = this.choiceStainedGlass.getValue();

        fireChoiceStainedGlassChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceStainedGlass> getChoiceStainedGlassValueContainer()
    {
        return this.choiceStainedGlass;
    }
    public final void fireChoiceStainedGlassChange(ChoiceStainedGlass currentValue, ChoiceStainedGlass oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceStainedGlassChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICESTAINEDGLASS, oldValue, currentValue);
            afterChoiceStainedGlassChanged(currentValue);
        }
    }

    public void beforeChoiceStainedGlassChanged( ChoiceStainedGlass choiceStainedGlass)
    { }
    public void afterChoiceStainedGlassChanged( ChoiceStainedGlass choiceStainedGlass)
    { }


    public boolean isChoiceStainedGlassMandatory()
    {
        return true;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Knocker")
        ,@LocalizedTag(language="fr", name="label", value="Heurtoir")
        ,@LocalizedTag(language="fr", name="null", value="Sans")
        ,@LocalizedTag(language="en", name="null", value="Without")
        ,@LocalizedTag(language="fr", name="notnull", value="Avec")
        ,@LocalizedTag(language="en", name="notnull", value="With")
    })
    public final ChoiceKnocker getChoiceKnocker()
    {
         return this.choiceKnocker.getValue();
    }

    public final void setChoiceKnocker(ChoiceKnocker choiceKnocker)
    {

        if (choiceKnocker != null)
        {
            acquire(choiceKnocker, "choiceKnocker");
        }
        ChoiceKnocker oldValue = this.choiceKnocker.getValue();
        this.choiceKnocker.setValue(choiceKnocker);

        ChoiceKnocker currentValue = this.choiceKnocker.getValue();

        fireChoiceKnockerChange(currentValue, oldValue);
    }
    public final ChoiceKnocker removeChoiceKnocker()
    {
        ChoiceKnocker oldValue = this.choiceKnocker.getValue();
        ChoiceKnocker removedValue = this.choiceKnocker.removeValue();
        ChoiceKnocker currentValue = this.choiceKnocker.getValue();

        fireChoiceKnockerChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceKnocker(ChoiceKnocker choiceKnocker)
    {
        if (choiceKnocker != null)
        {
            acquire(choiceKnocker, "choiceKnocker");
        }
        ChoiceKnocker oldValue = this.choiceKnocker.getValue();
        this.choiceKnocker.setDefaultValue(choiceKnocker);

        ChoiceKnocker currentValue = this.choiceKnocker.getValue();

        fireChoiceKnockerChange(currentValue, oldValue);
    }
    
    public final ChoiceKnocker removeDefaultChoiceKnocker()
    {
        ChoiceKnocker oldValue = this.choiceKnocker.getValue();
        ChoiceKnocker removedValue = this.choiceKnocker.removeDefaultValue();
        ChoiceKnocker currentValue = this.choiceKnocker.getValue();

        fireChoiceKnockerChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceKnocker> getChoiceKnockerValueContainer()
    {
        return this.choiceKnocker;
    }
    public final void fireChoiceKnockerChange(ChoiceKnocker currentValue, ChoiceKnocker oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceKnockerChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEKNOCKER, oldValue, currentValue);
            afterChoiceKnockerChanged(currentValue);
        }
    }

    public void beforeChoiceKnockerChanged( ChoiceKnocker choiceKnocker)
    { }
    public void afterChoiceKnockerChanged( ChoiceKnocker choiceKnocker)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="fr", name="null", value="Sans")
        ,@LocalizedTag(language="en", name="null", value="Without")
        ,@LocalizedTag(language="fr", name="notnull", value="Avec")
        ,@LocalizedTag(language="en", name="notnull", value="With")
        ,@LocalizedTag(language="en", name="label", value="HANDLESET")
        ,@LocalizedTag(language="fr", name="label", value="POIGN�E")
    })
    public final ChoiceHandleSet getChoiceHandleSet()
    {
         return this.choiceHandleSet.getValue();
    }

    public final void setChoiceHandleSet(ChoiceHandleSet choiceHandleSet)
    {

        if (choiceHandleSet != null)
        {
            acquire(choiceHandleSet, "choiceHandleSet");
        }
        ChoiceHandleSet oldValue = this.choiceHandleSet.getValue();
        this.choiceHandleSet.setValue(choiceHandleSet);

        ChoiceHandleSet currentValue = this.choiceHandleSet.getValue();

        fireChoiceHandleSetChange(currentValue, oldValue);
    }
    public final ChoiceHandleSet removeChoiceHandleSet()
    {
        ChoiceHandleSet oldValue = this.choiceHandleSet.getValue();
        ChoiceHandleSet removedValue = this.choiceHandleSet.removeValue();
        ChoiceHandleSet currentValue = this.choiceHandleSet.getValue();

        fireChoiceHandleSetChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceHandleSet(ChoiceHandleSet choiceHandleSet)
    {
        if (choiceHandleSet != null)
        {
            acquire(choiceHandleSet, "choiceHandleSet");
        }
        ChoiceHandleSet oldValue = this.choiceHandleSet.getValue();
        this.choiceHandleSet.setDefaultValue(choiceHandleSet);

        ChoiceHandleSet currentValue = this.choiceHandleSet.getValue();

        fireChoiceHandleSetChange(currentValue, oldValue);
    }
    
    public final ChoiceHandleSet removeDefaultChoiceHandleSet()
    {
        ChoiceHandleSet oldValue = this.choiceHandleSet.getValue();
        ChoiceHandleSet removedValue = this.choiceHandleSet.removeDefaultValue();
        ChoiceHandleSet currentValue = this.choiceHandleSet.getValue();

        fireChoiceHandleSetChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceHandleSet> getChoiceHandleSetValueContainer()
    {
        return this.choiceHandleSet;
    }
    public final void fireChoiceHandleSetChange(ChoiceHandleSet currentValue, ChoiceHandleSet oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceHandleSetChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEHANDLESET, oldValue, currentValue);
            afterChoiceHandleSetChanged(currentValue);
        }
    }

    public void beforeChoiceHandleSetChanged( ChoiceHandleSet choiceHandleSet)
    { }
    public void afterChoiceHandleSetChanged( ChoiceHandleSet choiceHandleSet)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="fr", name="label", value="Choix d\'accessoires 1")
    })
    public final ChoiceAcc1 getChoiceAcc1()
    {
         return this.choiceAcc1.getValue();
    }

    public final void setChoiceAcc1(ChoiceAcc1 choiceAcc1)
    {

        ChoiceAcc1 oldValue = this.choiceAcc1.getValue();
        this.choiceAcc1.setValue(choiceAcc1);

        ChoiceAcc1 currentValue = this.choiceAcc1.getValue();

        fireChoiceAcc1Change(currentValue, oldValue);
    }
    public final ChoiceAcc1 removeChoiceAcc1()
    {
        ChoiceAcc1 oldValue = this.choiceAcc1.getValue();
        ChoiceAcc1 removedValue = this.choiceAcc1.removeValue();
        ChoiceAcc1 currentValue = this.choiceAcc1.getValue();

        fireChoiceAcc1Change(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceAcc1(ChoiceAcc1 choiceAcc1)
    {
        ChoiceAcc1 oldValue = this.choiceAcc1.getValue();
        this.choiceAcc1.setDefaultValue(choiceAcc1);

        ChoiceAcc1 currentValue = this.choiceAcc1.getValue();

        fireChoiceAcc1Change(currentValue, oldValue);
    }
    
    public final ChoiceAcc1 removeDefaultChoiceAcc1()
    {
        ChoiceAcc1 oldValue = this.choiceAcc1.getValue();
        ChoiceAcc1 removedValue = this.choiceAcc1.removeDefaultValue();
        ChoiceAcc1 currentValue = this.choiceAcc1.getValue();

        fireChoiceAcc1Change(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceAcc1> getChoiceAcc1ValueContainer()
    {
        return this.choiceAcc1;
    }
    public final void fireChoiceAcc1Change(ChoiceAcc1 currentValue, ChoiceAcc1 oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceAcc1Changed(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEACC1, oldValue, currentValue);
            afterChoiceAcc1Changed(currentValue);
        }
    }

    public void beforeChoiceAcc1Changed( ChoiceAcc1 choiceAcc1)
    { }
    public void afterChoiceAcc1Changed( ChoiceAcc1 choiceAcc1)
    { }


    public boolean isChoiceAcc1Mandatory()
    {
        return true;
    }
    @LocalizedTags
    ({
    })
    public final GlassType getGlassType()
    {
         return this.glassType.getValue();
    }

    public final void setGlassType(GlassType glassType)
    {

        GlassType oldValue = this.glassType.getValue();
        this.glassType.setValue(glassType);

        GlassType currentValue = this.glassType.getValue();

        fireGlassTypeChange(currentValue, oldValue);
    }
    public final GlassType removeGlassType()
    {
        GlassType oldValue = this.glassType.getValue();
        GlassType removedValue = this.glassType.removeValue();
        GlassType currentValue = this.glassType.getValue();

        fireGlassTypeChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultGlassType(GlassType glassType)
    {
        GlassType oldValue = this.glassType.getValue();
        this.glassType.setDefaultValue(glassType);

        GlassType currentValue = this.glassType.getValue();

        fireGlassTypeChange(currentValue, oldValue);
    }
    
    public final GlassType removeDefaultGlassType()
    {
        GlassType oldValue = this.glassType.getValue();
        GlassType removedValue = this.glassType.removeDefaultValue();
        GlassType currentValue = this.glassType.getValue();

        fireGlassTypeChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<GlassType> getGlassTypeValueContainer()
    {
        return this.glassType;
    }
    public final void fireGlassTypeChange(GlassType currentValue, GlassType oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeGlassTypeChanged(currentValue);
            firePropertyChange(PROPERTYNAME_GLASSTYPE, oldValue, currentValue);
            afterGlassTypeChanged(currentValue);
        }
    }

    public void beforeGlassTypeChanged( GlassType glassType)
    { }
    public void afterGlassTypeChanged( GlassType glassType)
    { }


    public boolean isGlassTypeMandatory()
    {
        return true;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Slab 2")
        ,@LocalizedTag(language="fr", name="label", value="Panneau 2")
    })
    public final ChoicePanel getChoicePanel()
    {
         return this.choicePanel.getValue();
    }

    public final void setChoicePanel(ChoicePanel choicePanel)
    {

        if (choicePanel != null)
        {
            acquire(choicePanel, "choicePanel");
        }
        ChoicePanel oldValue = this.choicePanel.getValue();
        this.choicePanel.setValue(choicePanel);

        ChoicePanel currentValue = this.choicePanel.getValue();

        fireChoicePanelChange(currentValue, oldValue);
    }
    public final ChoicePanel removeChoicePanel()
    {
        ChoicePanel oldValue = this.choicePanel.getValue();
        ChoicePanel removedValue = this.choicePanel.removeValue();
        ChoicePanel currentValue = this.choicePanel.getValue();

        fireChoicePanelChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoicePanel(ChoicePanel choicePanel)
    {
        if (choicePanel != null)
        {
            acquire(choicePanel, "choicePanel");
        }
        ChoicePanel oldValue = this.choicePanel.getValue();
        this.choicePanel.setDefaultValue(choicePanel);

        ChoicePanel currentValue = this.choicePanel.getValue();

        fireChoicePanelChange(currentValue, oldValue);
    }
    
    public final ChoicePanel removeDefaultChoicePanel()
    {
        ChoicePanel oldValue = this.choicePanel.getValue();
        ChoicePanel removedValue = this.choicePanel.removeDefaultValue();
        ChoicePanel currentValue = this.choicePanel.getValue();

        fireChoicePanelChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoicePanel> getChoicePanelValueContainer()
    {
        return this.choicePanel;
    }
    public final void fireChoicePanelChange(ChoicePanel currentValue, ChoicePanel oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoicePanelChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEPANEL, oldValue, currentValue);
            afterChoicePanelChanged(currentValue);
        }
    }

    public void beforeChoicePanelChanged( ChoicePanel choicePanel)
    { }
    public void afterChoicePanelChanged( ChoicePanel choicePanel)
    { }


    public boolean isChoicePanelMandatory()
    {
        return true;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Dead bolt")
        ,@LocalizedTag(language="fr", name="label", value="Verrou")
        ,@LocalizedTag(language="fr", name="null", value="Sans")
        ,@LocalizedTag(language="en", name="null", value="Without")
        ,@LocalizedTag(language="fr", name="notnull", value="Avec")
        ,@LocalizedTag(language="en", name="notnull", value="With")
    })
    public final ChoiceDeadBolt getChoiceDeadBolt()
    {
         return this.choiceDeadBolt.getValue();
    }

    public final void setChoiceDeadBolt(ChoiceDeadBolt choiceDeadBolt)
    {

        if (choiceDeadBolt != null)
        {
            acquire(choiceDeadBolt, "choiceDeadBolt");
        }
        ChoiceDeadBolt oldValue = this.choiceDeadBolt.getValue();
        this.choiceDeadBolt.setValue(choiceDeadBolt);

        ChoiceDeadBolt currentValue = this.choiceDeadBolt.getValue();

        fireChoiceDeadBoltChange(currentValue, oldValue);
    }
    public final ChoiceDeadBolt removeChoiceDeadBolt()
    {
        ChoiceDeadBolt oldValue = this.choiceDeadBolt.getValue();
        ChoiceDeadBolt removedValue = this.choiceDeadBolt.removeValue();
        ChoiceDeadBolt currentValue = this.choiceDeadBolt.getValue();

        fireChoiceDeadBoltChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceDeadBolt(ChoiceDeadBolt choiceDeadBolt)
    {
        if (choiceDeadBolt != null)
        {
            acquire(choiceDeadBolt, "choiceDeadBolt");
        }
        ChoiceDeadBolt oldValue = this.choiceDeadBolt.getValue();
        this.choiceDeadBolt.setDefaultValue(choiceDeadBolt);

        ChoiceDeadBolt currentValue = this.choiceDeadBolt.getValue();

        fireChoiceDeadBoltChange(currentValue, oldValue);
    }
    
    public final ChoiceDeadBolt removeDefaultChoiceDeadBolt()
    {
        ChoiceDeadBolt oldValue = this.choiceDeadBolt.getValue();
        ChoiceDeadBolt removedValue = this.choiceDeadBolt.removeDefaultValue();
        ChoiceDeadBolt currentValue = this.choiceDeadBolt.getValue();

        fireChoiceDeadBoltChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceDeadBolt> getChoiceDeadBoltValueContainer()
    {
        return this.choiceDeadBolt;
    }
    public final void fireChoiceDeadBoltChange(ChoiceDeadBolt currentValue, ChoiceDeadBolt oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceDeadBoltChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEDEADBOLT, oldValue, currentValue);
            afterChoiceDeadBoltChanged(currentValue);
        }
    }

    public void beforeChoiceDeadBoltChanged( ChoiceDeadBolt choiceDeadBolt)
    { }
    public void afterChoiceDeadBoltChanged( ChoiceDeadBolt choiceDeadBolt)
    { }



    @Override
    protected String getSequences()
    {
        return "choiceSlabModel,choicePanel,glassType,choiceInsulatedGlass,choiceStainedGlass,thermos,choiceEyeHole,choiceStainedGlassPosition,choiceOpening,position,EyeHole,StainedGlass,choiceHandlesetSide,HandleSet,InsulatedGlass,exteriorColor,Knocker,SubModel,interiorColor,dimension,Panel,LetterBox,DeadBolt,doorMullionMobile,choiceSlabNAIDModel,choiceAcc2,handlesset,choiceSlabThermos,otherSlabModel,choiceLetterBox,choiceKnocker,choiceHandleSet,choiceAcc1,choiceDeadBolt";  
    }
}
