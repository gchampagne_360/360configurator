// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.DynamicEnum;
import com.netappsid.erp.configurator.annotations.Filtrable;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

@DynamicEnum(fileName = "dynamicEnums/eyehole.tsv", locale = "en", decimalSeparator = ".")
public abstract class ChoiceEyeHoleBase extends com.netappsid.erp.configurator.Configurable
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ChoiceEyeHoleBase.class);

    // attributes
    private ValueContainer<Double> imageHeight = new ValueContainer<Double>(this);
    private ValueContainer<String> form = new ValueContainer<String>(this);
    private ValueContainer<String> description = new ValueContainer<String>(this);
    private ValueContainer<String> path = new ValueContainer<String>(this);
    private ValueContainer<Double> imageWidth = new ValueContainer<Double>(this);
    private ValueContainer<Double> formHeight = new ValueContainer<Double>(this);
    private ValueContainer<Double> formWidth = new ValueContainer<Double>(this);

    // Bound properties

    public static final String PROPERTYNAME_IMAGEHEIGHT = "imageHeight";  
    public static final String PROPERTYNAME_FORM = "form";  
    public static final String PROPERTYNAME_DESCRIPTION = "description";  
    public static final String PROPERTYNAME_PATH = "path";  
    public static final String PROPERTYNAME_IMAGEWIDTH = "imageWidth";  
    public static final String PROPERTYNAME_FORMHEIGHT = "formHeight";  
    public static final String PROPERTYNAME_FORMWIDTH = "formWidth";  

    // Constructors

    public ChoiceEyeHoleBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.ChoiceEyeHole"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="3")
        ,@LocalizedTag(language="fr", name="column", value="3")
        ,@LocalizedTag(language="en", name="label", value="Height")
        ,@LocalizedTag(language="fr", name="label", value="Hauteur")
    })
    public final Double getImageHeight()
    {
         return this.imageHeight.getValue();
    }

    public final void setImageHeight(Double imageHeight)
    {

        Double oldValue = this.imageHeight.getValue();
        this.imageHeight.setValue(imageHeight);

        Double currentValue = this.imageHeight.getValue();

        fireImageHeightChange(currentValue, oldValue);
    }
    public final Double removeImageHeight()
    {
        Double oldValue = this.imageHeight.getValue();
        Double removedValue = this.imageHeight.removeValue();
        Double currentValue = this.imageHeight.getValue();

        fireImageHeightChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultImageHeight(Double imageHeight)
    {
        Double oldValue = this.imageHeight.getValue();
        this.imageHeight.setDefaultValue(imageHeight);

        Double currentValue = this.imageHeight.getValue();

        fireImageHeightChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultImageHeight()
    {
        Double oldValue = this.imageHeight.getValue();
        Double removedValue = this.imageHeight.removeDefaultValue();
        Double currentValue = this.imageHeight.getValue();

        fireImageHeightChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getImageHeightValueContainer()
    {
        return this.imageHeight;
    }
    public final void fireImageHeightChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeImageHeightChanged(currentValue);
            firePropertyChange(PROPERTYNAME_IMAGEHEIGHT, oldValue, currentValue);
            afterImageHeightChanged(currentValue);
        }
    }

    public void beforeImageHeightChanged( Double imageHeight)
    { }
    public void afterImageHeightChanged( Double imageHeight)
    { }


    public boolean isImageHeightVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="6")
        ,@LocalizedTag(language="fr", name="column", value="6")
        ,@LocalizedTag(language="en", name="label", value="Form")
        ,@LocalizedTag(language="fr", name="label", value="Forme")
    })
    public final String getForm()
    {
         return this.form.getValue();
    }

    public final void setForm(String form)
    {

        String oldValue = this.form.getValue();
        this.form.setValue(form);

        String currentValue = this.form.getValue();

        fireFormChange(currentValue, oldValue);
    }
    public final String removeForm()
    {
        String oldValue = this.form.getValue();
        String removedValue = this.form.removeValue();
        String currentValue = this.form.getValue();

        fireFormChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultForm(String form)
    {
        String oldValue = this.form.getValue();
        this.form.setDefaultValue(form);

        String currentValue = this.form.getValue();

        fireFormChange(currentValue, oldValue);
    }
    
    public final String removeDefaultForm()
    {
        String oldValue = this.form.getValue();
        String removedValue = this.form.removeDefaultValue();
        String currentValue = this.form.getValue();

        fireFormChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getFormValueContainer()
    {
        return this.form;
    }
    public final void fireFormChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeFormChanged(currentValue);
            firePropertyChange(PROPERTYNAME_FORM, oldValue, currentValue);
            afterFormChanged(currentValue);
        }
    }

    public void beforeFormChanged( String form)
    { }
    public void afterFormChanged( String form)
    { }


    public boolean isFormVisible()
    {
        return false;
    }
    @Filtrable(value = "true", defaultValue = "", defaultSelection = "")
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="0")
        ,@LocalizedTag(language="fr", name="column", value="0")
        ,@LocalizedTag(language="en", name="label", value="Name")
        ,@LocalizedTag(language="fr", name="label", value="Nom")
    })
    public final String getDescription()
    {
         return this.description.getValue();
    }

    public final void setDescription(String description)
    {

        String oldValue = this.description.getValue();
        this.description.setValue(description);

        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
    }
    public final String removeDescription()
    {
        String oldValue = this.description.getValue();
        String removedValue = this.description.removeValue();
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultDescription(String description)
    {
        String oldValue = this.description.getValue();
        this.description.setDefaultValue(description);

        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
    }
    
    public final String removeDefaultDescription()
    {
        String oldValue = this.description.getValue();
        String removedValue = this.description.removeDefaultValue();
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getDescriptionValueContainer()
    {
        return this.description;
    }
    public final void fireDescriptionChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeDescriptionChanged(currentValue);
            firePropertyChange(PROPERTYNAME_DESCRIPTION, oldValue, currentValue);
            afterDescriptionChanged(currentValue);
        }
    }

    public void beforeDescriptionChanged( String description)
    { }
    public void afterDescriptionChanged( String description)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="1")
        ,@LocalizedTag(language="fr", name="column", value="1")
        ,@LocalizedTag(language="en", name="label", value="File")
        ,@LocalizedTag(language="fr", name="label", value="Fichier")
    })
    public final String getPath()
    {
         return this.path.getValue();
    }

    public final void setPath(String path)
    {

        String oldValue = this.path.getValue();
        this.path.setValue(path);

        String currentValue = this.path.getValue();

        firePathChange(currentValue, oldValue);
    }
    public final String removePath()
    {
        String oldValue = this.path.getValue();
        String removedValue = this.path.removeValue();
        String currentValue = this.path.getValue();

        firePathChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultPath(String path)
    {
        String oldValue = this.path.getValue();
        this.path.setDefaultValue(path);

        String currentValue = this.path.getValue();

        firePathChange(currentValue, oldValue);
    }
    
    public final String removeDefaultPath()
    {
        String oldValue = this.path.getValue();
        String removedValue = this.path.removeDefaultValue();
        String currentValue = this.path.getValue();

        firePathChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getPathValueContainer()
    {
        return this.path;
    }
    public final void firePathChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforePathChanged(currentValue);
            firePropertyChange(PROPERTYNAME_PATH, oldValue, currentValue);
            afterPathChanged(currentValue);
        }
    }

    public void beforePathChanged( String path)
    { }
    public void afterPathChanged( String path)
    { }


    public boolean isPathVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="2")
        ,@LocalizedTag(language="fr", name="column", value="2")
        ,@LocalizedTag(language="en", name="label", value="Width")
        ,@LocalizedTag(language="fr", name="label", value="Largeur")
    })
    public final Double getImageWidth()
    {
         return this.imageWidth.getValue();
    }

    public final void setImageWidth(Double imageWidth)
    {

        Double oldValue = this.imageWidth.getValue();
        this.imageWidth.setValue(imageWidth);

        Double currentValue = this.imageWidth.getValue();

        fireImageWidthChange(currentValue, oldValue);
    }
    public final Double removeImageWidth()
    {
        Double oldValue = this.imageWidth.getValue();
        Double removedValue = this.imageWidth.removeValue();
        Double currentValue = this.imageWidth.getValue();

        fireImageWidthChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultImageWidth(Double imageWidth)
    {
        Double oldValue = this.imageWidth.getValue();
        this.imageWidth.setDefaultValue(imageWidth);

        Double currentValue = this.imageWidth.getValue();

        fireImageWidthChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultImageWidth()
    {
        Double oldValue = this.imageWidth.getValue();
        Double removedValue = this.imageWidth.removeDefaultValue();
        Double currentValue = this.imageWidth.getValue();

        fireImageWidthChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getImageWidthValueContainer()
    {
        return this.imageWidth;
    }
    public final void fireImageWidthChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeImageWidthChanged(currentValue);
            firePropertyChange(PROPERTYNAME_IMAGEWIDTH, oldValue, currentValue);
            afterImageWidthChanged(currentValue);
        }
    }

    public void beforeImageWidthChanged( Double imageWidth)
    { }
    public void afterImageWidthChanged( Double imageWidth)
    { }


    public boolean isImageWidthVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="5")
        ,@LocalizedTag(language="fr", name="column", value="5")
        ,@LocalizedTag(language="en", name="label", value="Height")
        ,@LocalizedTag(language="fr", name="label", value="Hauteur")
    })
    public final Double getFormHeight()
    {
         return this.formHeight.getValue();
    }

    public final void setFormHeight(Double formHeight)
    {

        Double oldValue = this.formHeight.getValue();
        this.formHeight.setValue(formHeight);

        Double currentValue = this.formHeight.getValue();

        fireFormHeightChange(currentValue, oldValue);
    }
    public final Double removeFormHeight()
    {
        Double oldValue = this.formHeight.getValue();
        Double removedValue = this.formHeight.removeValue();
        Double currentValue = this.formHeight.getValue();

        fireFormHeightChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultFormHeight(Double formHeight)
    {
        Double oldValue = this.formHeight.getValue();
        this.formHeight.setDefaultValue(formHeight);

        Double currentValue = this.formHeight.getValue();

        fireFormHeightChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultFormHeight()
    {
        Double oldValue = this.formHeight.getValue();
        Double removedValue = this.formHeight.removeDefaultValue();
        Double currentValue = this.formHeight.getValue();

        fireFormHeightChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getFormHeightValueContainer()
    {
        return this.formHeight;
    }
    public final void fireFormHeightChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeFormHeightChanged(currentValue);
            firePropertyChange(PROPERTYNAME_FORMHEIGHT, oldValue, currentValue);
            afterFormHeightChanged(currentValue);
        }
    }

    public void beforeFormHeightChanged( Double formHeight)
    { }
    public void afterFormHeightChanged( Double formHeight)
    { }


    public boolean isFormHeightVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="4")
        ,@LocalizedTag(language="fr", name="column", value="4")
        ,@LocalizedTag(language="en", name="label", value="Height")
        ,@LocalizedTag(language="fr", name="label", value="Hauteur")
    })
    public final Double getFormWidth()
    {
         return this.formWidth.getValue();
    }

    public final void setFormWidth(Double formWidth)
    {

        Double oldValue = this.formWidth.getValue();
        this.formWidth.setValue(formWidth);

        Double currentValue = this.formWidth.getValue();

        fireFormWidthChange(currentValue, oldValue);
    }
    public final Double removeFormWidth()
    {
        Double oldValue = this.formWidth.getValue();
        Double removedValue = this.formWidth.removeValue();
        Double currentValue = this.formWidth.getValue();

        fireFormWidthChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultFormWidth(Double formWidth)
    {
        Double oldValue = this.formWidth.getValue();
        this.formWidth.setDefaultValue(formWidth);

        Double currentValue = this.formWidth.getValue();

        fireFormWidthChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultFormWidth()
    {
        Double oldValue = this.formWidth.getValue();
        Double removedValue = this.formWidth.removeDefaultValue();
        Double currentValue = this.formWidth.getValue();

        fireFormWidthChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getFormWidthValueContainer()
    {
        return this.formWidth;
    }
    public final void fireFormWidthChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeFormWidthChanged(currentValue);
            firePropertyChange(PROPERTYNAME_FORMWIDTH, oldValue, currentValue);
            afterFormWidthChanged(currentValue);
        }
    }

    public void beforeFormWidthChanged( Double formWidth)
    { }
    public void afterFormWidthChanged( Double formWidth)
    { }


    public boolean isFormWidthVisible()
    {
        return false;
    }

}
