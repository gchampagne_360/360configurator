// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class JambOtherBase extends com.client360.configuration.wad.Jamb
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(JambOtherBase.class);

    // attributes
    private ValueContainer<Double> customDim = new ValueContainer<Double>(this);

    // Bound properties

    public static final String PROPERTYNAME_CUSTOMDIM = "customDim";  

    // Constructors

    public JambOtherBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.JambOther"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
    })
    public final Double getCustomDim()
    {
         return this.customDim.getValue();
    }

    public final void setCustomDim(Double customDim)
    {

        Double oldValue = this.customDim.getValue();
        this.customDim.setValue(customDim);

        Double currentValue = this.customDim.getValue();

        fireCustomDimChange(currentValue, oldValue);
    }
    public final Double removeCustomDim()
    {
        Double oldValue = this.customDim.getValue();
        Double removedValue = this.customDim.removeValue();
        Double currentValue = this.customDim.getValue();

        fireCustomDimChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultCustomDim(Double customDim)
    {
        Double oldValue = this.customDim.getValue();
        this.customDim.setDefaultValue(customDim);

        Double currentValue = this.customDim.getValue();

        fireCustomDimChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultCustomDim()
    {
        Double oldValue = this.customDim.getValue();
        Double removedValue = this.customDim.removeDefaultValue();
        Double currentValue = this.customDim.getValue();

        fireCustomDimChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getCustomDimValueContainer()
    {
        return this.customDim;
    }
    public final void fireCustomDimChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeCustomDimChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CUSTOMDIM, oldValue, currentValue);
            afterCustomDimChanged(currentValue);
        }
    }

    public void beforeCustomDimChanged( Double customDim)
    { }
    public void afterCustomDimChanged( Double customDim)
    { }


}
