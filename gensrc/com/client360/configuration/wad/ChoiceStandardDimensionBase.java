// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.DynamicEnum;
import com.netappsid.erp.configurator.annotations.Filtrable;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

@DynamicEnum(fileName = "ChoiceStd")
public abstract class ChoiceStandardDimensionBase extends com.netappsid.erp.configurator.Configurable
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ChoiceStandardDimensionBase.class);

    // attributes
    private ValueContainer<Integer> nbDoor = new ValueContainer<Integer>(this);
    private ValueContainer<Integer> nbSide = new ValueContainer<Integer>(this);
    private ValueContainer<Double> totalWidth = new ValueContainer<Double>(this);
    private ValueContainer<Double> doorsWidth = new ValueContainer<Double>(this);
    private ValueContainer<Double> sidesWidth = new ValueContainer<Double>(this);
    private ValueContainer<String> sideLightSide = new ValueContainer<String>(this);
    private ValueContainer<String> description = new ValueContainer<String>(this);
    private ValueContainer<Double> price = new ValueContainer<Double>(this);
    private ValueContainer<String> openingType = new ValueContainer<String>(this);
    private ValueContainer<String> operatingSideFormatted = new ValueContainer<String>(this);
    private ValueContainer<String> operatingSide = new ValueContainer<String>(this);
    private ValueContainer<String> code = new ValueContainer<String>(this);

    // Bound properties

    public static final String PROPERTYNAME_NBDOOR = "nbDoor";  
    public static final String PROPERTYNAME_NBSIDE = "nbSide";  
    public static final String PROPERTYNAME_TOTALWIDTH = "totalWidth";  
    public static final String PROPERTYNAME_DOORSWIDTH = "doorsWidth";  
    public static final String PROPERTYNAME_SIDESWIDTH = "sidesWidth";  
    public static final String PROPERTYNAME_SIDELIGHTSIDE = "sideLightSide";  
    public static final String PROPERTYNAME_DESCRIPTION = "description";  
    public static final String PROPERTYNAME_PRICE = "price";  
    public static final String PROPERTYNAME_OPENINGTYPE = "openingType";  
    public static final String PROPERTYNAME_OPERATINGSIDEFORMATTED = "operatingSideFormatted";  
    public static final String PROPERTYNAME_OPERATINGSIDE = "operatingSide";  
    public static final String PROPERTYNAME_CODE = "code";  

    // Constructors

    public ChoiceStandardDimensionBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.ChoiceStandardDimension"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @Filtrable(value = "true", defaultValue = "", defaultSelection = "")
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="2")
        ,@LocalizedTag(language="fr", name="column", value="2")
        ,@LocalizedTag(language="en", name="label", value="Nb door")
        ,@LocalizedTag(language="fr", name="label", value="Nb porte")
    })
    public final Integer getNbDoor()
    {
         return this.nbDoor.getValue();
    }

    public final void setNbDoor(Integer nbDoor)
    {

        Integer oldValue = this.nbDoor.getValue();
        this.nbDoor.setValue(nbDoor);

        Integer currentValue = this.nbDoor.getValue();

        fireNbDoorChange(currentValue, oldValue);
    }
    public final Integer removeNbDoor()
    {
        Integer oldValue = this.nbDoor.getValue();
        Integer removedValue = this.nbDoor.removeValue();
        Integer currentValue = this.nbDoor.getValue();

        fireNbDoorChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultNbDoor(Integer nbDoor)
    {
        Integer oldValue = this.nbDoor.getValue();
        this.nbDoor.setDefaultValue(nbDoor);

        Integer currentValue = this.nbDoor.getValue();

        fireNbDoorChange(currentValue, oldValue);
    }
    
    public final Integer removeDefaultNbDoor()
    {
        Integer oldValue = this.nbDoor.getValue();
        Integer removedValue = this.nbDoor.removeDefaultValue();
        Integer currentValue = this.nbDoor.getValue();

        fireNbDoorChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Integer> getNbDoorValueContainer()
    {
        return this.nbDoor;
    }
    public final void fireNbDoorChange(Integer currentValue, Integer oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeNbDoorChanged(currentValue);
            firePropertyChange(PROPERTYNAME_NBDOOR, oldValue, currentValue);
            afterNbDoorChanged(currentValue);
        }
    }

    public void beforeNbDoorChanged( Integer nbDoor)
    { }
    public void afterNbDoorChanged( Integer nbDoor)
    { }

    @Filtrable(value = "true", defaultValue = "", defaultSelection = "")
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="3")
        ,@LocalizedTag(language="fr", name="column", value="3")
        ,@LocalizedTag(language="en", name="label", value="Nb Sides")
        ,@LocalizedTag(language="fr", name="label", value="Nb Lat�raux")
    })
    public final Integer getNbSide()
    {
         return this.nbSide.getValue();
    }

    public final void setNbSide(Integer nbSide)
    {

        Integer oldValue = this.nbSide.getValue();
        this.nbSide.setValue(nbSide);

        Integer currentValue = this.nbSide.getValue();

        fireNbSideChange(currentValue, oldValue);
    }
    public final Integer removeNbSide()
    {
        Integer oldValue = this.nbSide.getValue();
        Integer removedValue = this.nbSide.removeValue();
        Integer currentValue = this.nbSide.getValue();

        fireNbSideChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultNbSide(Integer nbSide)
    {
        Integer oldValue = this.nbSide.getValue();
        this.nbSide.setDefaultValue(nbSide);

        Integer currentValue = this.nbSide.getValue();

        fireNbSideChange(currentValue, oldValue);
    }
    
    public final Integer removeDefaultNbSide()
    {
        Integer oldValue = this.nbSide.getValue();
        Integer removedValue = this.nbSide.removeDefaultValue();
        Integer currentValue = this.nbSide.getValue();

        fireNbSideChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Integer> getNbSideValueContainer()
    {
        return this.nbSide;
    }
    public final void fireNbSideChange(Integer currentValue, Integer oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeNbSideChanged(currentValue);
            firePropertyChange(PROPERTYNAME_NBSIDE, oldValue, currentValue);
            afterNbSideChanged(currentValue);
        }
    }

    public void beforeNbSideChanged( Integer nbSide)
    { }
    public void afterNbSideChanged( Integer nbSide)
    { }

    @Filtrable(value = "true", defaultValue = "", defaultSelection = "")
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="4")
        ,@LocalizedTag(language="fr", name="column", value="4")
        ,@LocalizedTag(language="en", name="label", value="Total Width")
        ,@LocalizedTag(language="fr", name="label", value="Largeur Total")
    })
    public final Double getTotalWidth()
    {
         return this.totalWidth.getValue();
    }

    public final void setTotalWidth(Double totalWidth)
    {

        Double oldValue = this.totalWidth.getValue();
        this.totalWidth.setValue(totalWidth);

        Double currentValue = this.totalWidth.getValue();

        fireTotalWidthChange(currentValue, oldValue);
    }
    public final Double removeTotalWidth()
    {
        Double oldValue = this.totalWidth.getValue();
        Double removedValue = this.totalWidth.removeValue();
        Double currentValue = this.totalWidth.getValue();

        fireTotalWidthChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultTotalWidth(Double totalWidth)
    {
        Double oldValue = this.totalWidth.getValue();
        this.totalWidth.setDefaultValue(totalWidth);

        Double currentValue = this.totalWidth.getValue();

        fireTotalWidthChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultTotalWidth()
    {
        Double oldValue = this.totalWidth.getValue();
        Double removedValue = this.totalWidth.removeDefaultValue();
        Double currentValue = this.totalWidth.getValue();

        fireTotalWidthChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getTotalWidthValueContainer()
    {
        return this.totalWidth;
    }
    public final void fireTotalWidthChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeTotalWidthChanged(currentValue);
            firePropertyChange(PROPERTYNAME_TOTALWIDTH, oldValue, currentValue);
            afterTotalWidthChanged(currentValue);
        }
    }

    public void beforeTotalWidthChanged( Double totalWidth)
    { }
    public void afterTotalWidthChanged( Double totalWidth)
    { }

    @Filtrable(value = "true", defaultValue = "", defaultSelection = "")
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="5")
        ,@LocalizedTag(language="fr", name="column", value="5")
        ,@LocalizedTag(language="en", name="label", value="Doors width")
        ,@LocalizedTag(language="fr", name="label", value="Largeur des portes")
    })
    public final Double getDoorsWidth()
    {
         return this.doorsWidth.getValue();
    }

    public final void setDoorsWidth(Double doorsWidth)
    {

        Double oldValue = this.doorsWidth.getValue();
        this.doorsWidth.setValue(doorsWidth);

        Double currentValue = this.doorsWidth.getValue();

        fireDoorsWidthChange(currentValue, oldValue);
    }
    public final Double removeDoorsWidth()
    {
        Double oldValue = this.doorsWidth.getValue();
        Double removedValue = this.doorsWidth.removeValue();
        Double currentValue = this.doorsWidth.getValue();

        fireDoorsWidthChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultDoorsWidth(Double doorsWidth)
    {
        Double oldValue = this.doorsWidth.getValue();
        this.doorsWidth.setDefaultValue(doorsWidth);

        Double currentValue = this.doorsWidth.getValue();

        fireDoorsWidthChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultDoorsWidth()
    {
        Double oldValue = this.doorsWidth.getValue();
        Double removedValue = this.doorsWidth.removeDefaultValue();
        Double currentValue = this.doorsWidth.getValue();

        fireDoorsWidthChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getDoorsWidthValueContainer()
    {
        return this.doorsWidth;
    }
    public final void fireDoorsWidthChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeDoorsWidthChanged(currentValue);
            firePropertyChange(PROPERTYNAME_DOORSWIDTH, oldValue, currentValue);
            afterDoorsWidthChanged(currentValue);
        }
    }

    public void beforeDoorsWidthChanged( Double doorsWidth)
    { }
    public void afterDoorsWidthChanged( Double doorsWidth)
    { }

    @Filtrable(value = "true", defaultValue = "", defaultSelection = "")
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="6")
        ,@LocalizedTag(language="fr", name="column", value="6")
        ,@LocalizedTag(language="en", name="label", value="SideLights Width")
        ,@LocalizedTag(language="fr", name="label", value="Largeur des lat�raux")
    })
    public final Double getSidesWidth()
    {
         return this.sidesWidth.getValue();
    }

    public final void setSidesWidth(Double sidesWidth)
    {

        Double oldValue = this.sidesWidth.getValue();
        this.sidesWidth.setValue(sidesWidth);

        Double currentValue = this.sidesWidth.getValue();

        fireSidesWidthChange(currentValue, oldValue);
    }
    public final Double removeSidesWidth()
    {
        Double oldValue = this.sidesWidth.getValue();
        Double removedValue = this.sidesWidth.removeValue();
        Double currentValue = this.sidesWidth.getValue();

        fireSidesWidthChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultSidesWidth(Double sidesWidth)
    {
        Double oldValue = this.sidesWidth.getValue();
        this.sidesWidth.setDefaultValue(sidesWidth);

        Double currentValue = this.sidesWidth.getValue();

        fireSidesWidthChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultSidesWidth()
    {
        Double oldValue = this.sidesWidth.getValue();
        Double removedValue = this.sidesWidth.removeDefaultValue();
        Double currentValue = this.sidesWidth.getValue();

        fireSidesWidthChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getSidesWidthValueContainer()
    {
        return this.sidesWidth;
    }
    public final void fireSidesWidthChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeSidesWidthChanged(currentValue);
            firePropertyChange(PROPERTYNAME_SIDESWIDTH, oldValue, currentValue);
            afterSidesWidthChanged(currentValue);
        }
    }

    public void beforeSidesWidthChanged( Double sidesWidth)
    { }
    public void afterSidesWidthChanged( Double sidesWidth)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="7")
        ,@LocalizedTag(language="fr", name="column", value="7")
        ,@LocalizedTag(language="en", name="label", value="Sidelight side")
        ,@LocalizedTag(language="fr", name="label", value="C�t� du lat�ral")
    })
    public final String getSideLightSide()
    {
         return this.sideLightSide.getValue();
    }

    public final void setSideLightSide(String sideLightSide)
    {

        String oldValue = this.sideLightSide.getValue();
        this.sideLightSide.setValue(sideLightSide);

        String currentValue = this.sideLightSide.getValue();

        fireSideLightSideChange(currentValue, oldValue);
    }
    public final String removeSideLightSide()
    {
        String oldValue = this.sideLightSide.getValue();
        String removedValue = this.sideLightSide.removeValue();
        String currentValue = this.sideLightSide.getValue();

        fireSideLightSideChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultSideLightSide(String sideLightSide)
    {
        String oldValue = this.sideLightSide.getValue();
        this.sideLightSide.setDefaultValue(sideLightSide);

        String currentValue = this.sideLightSide.getValue();

        fireSideLightSideChange(currentValue, oldValue);
    }
    
    public final String removeDefaultSideLightSide()
    {
        String oldValue = this.sideLightSide.getValue();
        String removedValue = this.sideLightSide.removeDefaultValue();
        String currentValue = this.sideLightSide.getValue();

        fireSideLightSideChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getSideLightSideValueContainer()
    {
        return this.sideLightSide;
    }
    public final void fireSideLightSideChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeSideLightSideChanged(currentValue);
            firePropertyChange(PROPERTYNAME_SIDELIGHTSIDE, oldValue, currentValue);
            afterSideLightSideChanged(currentValue);
        }
    }

    public void beforeSideLightSideChanged( String sideLightSide)
    { }
    public void afterSideLightSideChanged( String sideLightSide)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="0")
        ,@LocalizedTag(language="fr", name="column", value="1")
        ,@LocalizedTag(language="en", name="label", value="Description")
        ,@LocalizedTag(language="fr", name="label", value="Description")
    })
    public final String getDescription()
    {
         return this.description.getValue();
    }

    public final void setDescription(String description)
    {

        String oldValue = this.description.getValue();
        this.description.setValue(description);

        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
    }
    public final String removeDescription()
    {
        String oldValue = this.description.getValue();
        String removedValue = this.description.removeValue();
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultDescription(String description)
    {
        String oldValue = this.description.getValue();
        this.description.setDefaultValue(description);

        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
    }
    
    public final String removeDefaultDescription()
    {
        String oldValue = this.description.getValue();
        String removedValue = this.description.removeDefaultValue();
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getDescriptionValueContainer()
    {
        return this.description;
    }
    public final void fireDescriptionChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeDescriptionChanged(currentValue);
            firePropertyChange(PROPERTYNAME_DESCRIPTION, oldValue, currentValue);
            afterDescriptionChanged(currentValue);
        }
    }

    public void beforeDescriptionChanged( String description)
    { }
    public void afterDescriptionChanged( String description)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="10")
        ,@LocalizedTag(language="fr", name="column", value="10")
        ,@LocalizedTag(language="en", name="label", value="Price")
        ,@LocalizedTag(language="fr", name="label", value="Prix")
    })
    public final Double getPrice()
    {
         return this.price.getValue();
    }

    public final void setPrice(Double price)
    {

        Double oldValue = this.price.getValue();
        this.price.setValue(price);

        Double currentValue = this.price.getValue();

        firePriceChange(currentValue, oldValue);
    }
    public final Double removePrice()
    {
        Double oldValue = this.price.getValue();
        Double removedValue = this.price.removeValue();
        Double currentValue = this.price.getValue();

        firePriceChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultPrice(Double price)
    {
        Double oldValue = this.price.getValue();
        this.price.setDefaultValue(price);

        Double currentValue = this.price.getValue();

        firePriceChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultPrice()
    {
        Double oldValue = this.price.getValue();
        Double removedValue = this.price.removeDefaultValue();
        Double currentValue = this.price.getValue();

        firePriceChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getPriceValueContainer()
    {
        return this.price;
    }
    public final void firePriceChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforePriceChanged(currentValue);
            firePropertyChange(PROPERTYNAME_PRICE, oldValue, currentValue);
            afterPriceChanged(currentValue);
        }
    }

    public void beforePriceChanged( Double price)
    { }
    public void afterPriceChanged( Double price)
    { }


    public boolean isPriceVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Opening Type")
        ,@LocalizedTag(language="fr", name="label", value="Type d\'ouvertues")
    })
    public final String getOpeningType()
    {
         return this.openingType.getValue();
    }

    public final void setOpeningType(String openingType)
    {

        String oldValue = this.openingType.getValue();
        this.openingType.setValue(openingType);

        String currentValue = this.openingType.getValue();

        fireOpeningTypeChange(currentValue, oldValue);
    }
    public final String removeOpeningType()
    {
        String oldValue = this.openingType.getValue();
        String removedValue = this.openingType.removeValue();
        String currentValue = this.openingType.getValue();

        fireOpeningTypeChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultOpeningType(String openingType)
    {
        String oldValue = this.openingType.getValue();
        this.openingType.setDefaultValue(openingType);

        String currentValue = this.openingType.getValue();

        fireOpeningTypeChange(currentValue, oldValue);
    }
    
    public final String removeDefaultOpeningType()
    {
        String oldValue = this.openingType.getValue();
        String removedValue = this.openingType.removeDefaultValue();
        String currentValue = this.openingType.getValue();

        fireOpeningTypeChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getOpeningTypeValueContainer()
    {
        return this.openingType;
    }
    public final void fireOpeningTypeChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeOpeningTypeChanged(currentValue);
            firePropertyChange(PROPERTYNAME_OPENINGTYPE, oldValue, currentValue);
            afterOpeningTypeChanged(currentValue);
        }
    }

    public void beforeOpeningTypeChanged( String openingType)
    { }
    public void afterOpeningTypeChanged( String openingType)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="8")
        ,@LocalizedTag(language="fr", name="column", value="8")
        ,@LocalizedTag(language="en", name="label", value="Opening side")
        ,@LocalizedTag(language="fr", name="label", value="Sens d\'ouverture")
    })
    public final String getOperatingSideFormatted()
    {
         return this.operatingSideFormatted.getValue();
    }

    public final void setOperatingSideFormatted(String operatingSideFormatted)
    {

        String oldValue = this.operatingSideFormatted.getValue();
        this.operatingSideFormatted.setValue(operatingSideFormatted);

        String currentValue = this.operatingSideFormatted.getValue();

        fireOperatingSideFormattedChange(currentValue, oldValue);
    }
    public final String removeOperatingSideFormatted()
    {
        String oldValue = this.operatingSideFormatted.getValue();
        String removedValue = this.operatingSideFormatted.removeValue();
        String currentValue = this.operatingSideFormatted.getValue();

        fireOperatingSideFormattedChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultOperatingSideFormatted(String operatingSideFormatted)
    {
        String oldValue = this.operatingSideFormatted.getValue();
        this.operatingSideFormatted.setDefaultValue(operatingSideFormatted);

        String currentValue = this.operatingSideFormatted.getValue();

        fireOperatingSideFormattedChange(currentValue, oldValue);
    }
    
    public final String removeDefaultOperatingSideFormatted()
    {
        String oldValue = this.operatingSideFormatted.getValue();
        String removedValue = this.operatingSideFormatted.removeDefaultValue();
        String currentValue = this.operatingSideFormatted.getValue();

        fireOperatingSideFormattedChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getOperatingSideFormattedValueContainer()
    {
        return this.operatingSideFormatted;
    }
    public final void fireOperatingSideFormattedChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeOperatingSideFormattedChanged(currentValue);
            firePropertyChange(PROPERTYNAME_OPERATINGSIDEFORMATTED, oldValue, currentValue);
            afterOperatingSideFormattedChanged(currentValue);
        }
    }

    public void beforeOperatingSideFormattedChanged( String operatingSideFormatted)
    { }
    public void afterOperatingSideFormattedChanged( String operatingSideFormatted)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="9")
        ,@LocalizedTag(language="fr", name="column", value="9")
        ,@LocalizedTag(language="en", name="label", value="Opening side")
        ,@LocalizedTag(language="fr", name="label", value="Sens d\'ouverture")
    })
    public final String getOperatingSide()
    {
         return this.operatingSide.getValue();
    }

    public final void setOperatingSide(String operatingSide)
    {

        String oldValue = this.operatingSide.getValue();
        this.operatingSide.setValue(operatingSide);

        String currentValue = this.operatingSide.getValue();

        fireOperatingSideChange(currentValue, oldValue);
    }
    public final String removeOperatingSide()
    {
        String oldValue = this.operatingSide.getValue();
        String removedValue = this.operatingSide.removeValue();
        String currentValue = this.operatingSide.getValue();

        fireOperatingSideChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultOperatingSide(String operatingSide)
    {
        String oldValue = this.operatingSide.getValue();
        this.operatingSide.setDefaultValue(operatingSide);

        String currentValue = this.operatingSide.getValue();

        fireOperatingSideChange(currentValue, oldValue);
    }
    
    public final String removeDefaultOperatingSide()
    {
        String oldValue = this.operatingSide.getValue();
        String removedValue = this.operatingSide.removeDefaultValue();
        String currentValue = this.operatingSide.getValue();

        fireOperatingSideChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getOperatingSideValueContainer()
    {
        return this.operatingSide;
    }
    public final void fireOperatingSideChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeOperatingSideChanged(currentValue);
            firePropertyChange(PROPERTYNAME_OPERATINGSIDE, oldValue, currentValue);
            afterOperatingSideChanged(currentValue);
        }
    }

    public void beforeOperatingSideChanged( String operatingSide)
    { }
    public void afterOperatingSideChanged( String operatingSide)
    { }


    public boolean isOperatingSideVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="11")
        ,@LocalizedTag(language="fr", name="column", value="11")
        ,@LocalizedTag(language="en", name="label", value="Index")
        ,@LocalizedTag(language="fr", name="label", value="Index")
    })
    public final String getCode()
    {
         return this.code.getValue();
    }

    public final void setCode(String code)
    {

        String oldValue = this.code.getValue();
        this.code.setValue(code);

        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
    }
    public final String removeCode()
    {
        String oldValue = this.code.getValue();
        String removedValue = this.code.removeValue();
        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultCode(String code)
    {
        String oldValue = this.code.getValue();
        this.code.setDefaultValue(code);

        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
    }
    
    public final String removeDefaultCode()
    {
        String oldValue = this.code.getValue();
        String removedValue = this.code.removeDefaultValue();
        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getCodeValueContainer()
    {
        return this.code;
    }
    public final void fireCodeChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeCodeChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CODE, oldValue, currentValue);
            afterCodeChanged(currentValue);
        }
    }

    public void beforeCodeChanged( String code)
    { }
    public void afterCodeChanged( String code)
    { }


    public boolean isCodeVisible()
    {
        return false;
    }

}
