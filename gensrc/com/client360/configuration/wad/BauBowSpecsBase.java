// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V12.7.0
//2011-10-21

// Imports 

public abstract class BauBowSpecsBase extends com.netappsid.wadconfigurator.BayBowSpecs
{
	// Log4J logger
	protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(BauBowSpecsBase.class);

	// attributes

	// Constructors

	public BauBowSpecsBase(com.netappsid.erp.configurator.Configurable parent) // GENCODE b298fff0-4bff-11e0-b8af-0800200c9a66
	{
		super(parent);
		if (this.getClass().getName().equals("com.client360.configuration.wad.BauBowSpecs"))
		{
			// activate listener before setting the default value to listen the changes
			activateListener();

			// Load the default values dynamically
			setDefaultValues();

			// load 'collection' preferences and values coming from prior items in the order
			applyPreferences();
		}

	}

	// Operations

	// Business methods

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
	}

	// Bound properties

}
