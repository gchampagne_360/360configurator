// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.DynamicEnum;
import com.netappsid.erp.configurator.annotations.Filtrable;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

@DynamicEnum(fileName = "thermos")
public abstract class ChoiceInsulatedGlassBase extends com.netappsid.erp.configurator.Configurable
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ChoiceInsulatedGlassBase.class);

    // attributes
    private ValueContainer<String> code = new ValueContainer<String>(this);
    private ValueContainer<String> dimension = new ValueContainer<String>(this);
    private ValueContainer<String> form = new ValueContainer<String>(this);
    private ValueContainer<String> width = new ValueContainer<String>(this);
    private ValueContainer<String> height = new ValueContainer<String>(this);
    private ValueContainer<String> parametricModelClass = new ValueContainer<String>(this);
    private ValueContainer<String> parametricModelClassSidelite = new ValueContainer<String>(this);
    private ValueContainer<String> askStainedGlassPosition = new ValueContainer<String>(this);

    // Bound properties

    public static final String PROPERTYNAME_CODE = "code";  
    public static final String PROPERTYNAME_DIMENSION = "dimension";  
    public static final String PROPERTYNAME_FORM = "form";  
    public static final String PROPERTYNAME_WIDTH = "width";  
    public static final String PROPERTYNAME_HEIGHT = "height";  
    public static final String PROPERTYNAME_PARAMETRICMODELCLASS = "parametricModelClass";  
    public static final String PROPERTYNAME_PARAMETRICMODELCLASSSIDELITE = "parametricModelClassSidelite";  
    public static final String PROPERTYNAME_ASKSTAINEDGLASSPOSITION = "askStainedGlassPosition";  

    // Constructors

    public ChoiceInsulatedGlassBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.ChoiceInsulatedGlass"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="0")
        ,@LocalizedTag(language="fr", name="column", value="0")
        ,@LocalizedTag(language="en", name="label", value="Code")
        ,@LocalizedTag(language="fr", name="label", value="Code")
    })
    public final String getCode()
    {
         return this.code.getValue();
    }

    public final void setCode(String code)
    {

        String oldValue = this.code.getValue();
        this.code.setValue(code);

        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
    }
    public final String removeCode()
    {
        String oldValue = this.code.getValue();
        String removedValue = this.code.removeValue();
        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultCode(String code)
    {
        String oldValue = this.code.getValue();
        this.code.setDefaultValue(code);

        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
    }
    
    public final String removeDefaultCode()
    {
        String oldValue = this.code.getValue();
        String removedValue = this.code.removeDefaultValue();
        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getCodeValueContainer()
    {
        return this.code;
    }
    public final void fireCodeChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeCodeChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CODE, oldValue, currentValue);
            afterCodeChanged(currentValue);
        }
    }

    public void beforeCodeChanged( String code)
    { }
    public void afterCodeChanged( String code)
    { }


    public boolean isCodeVisible()
    {
        return false;
    }
    @Filtrable(value = "true", defaultValue = "", defaultSelection = "")
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="1")
        ,@LocalizedTag(language="fr", name="column", value="1")
        ,@LocalizedTag(language="en", name="label", value="Dimensions")
        ,@LocalizedTag(language="fr", name="label", value="Dimensions")
    })
    public final String getDimension()
    {
         return this.dimension.getValue();
    }

    public final void setDimension(String dimension)
    {

        String oldValue = this.dimension.getValue();
        this.dimension.setValue(dimension);

        String currentValue = this.dimension.getValue();

        fireDimensionChange(currentValue, oldValue);
    }
    public final String removeDimension()
    {
        String oldValue = this.dimension.getValue();
        String removedValue = this.dimension.removeValue();
        String currentValue = this.dimension.getValue();

        fireDimensionChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultDimension(String dimension)
    {
        String oldValue = this.dimension.getValue();
        this.dimension.setDefaultValue(dimension);

        String currentValue = this.dimension.getValue();

        fireDimensionChange(currentValue, oldValue);
    }
    
    public final String removeDefaultDimension()
    {
        String oldValue = this.dimension.getValue();
        String removedValue = this.dimension.removeDefaultValue();
        String currentValue = this.dimension.getValue();

        fireDimensionChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getDimensionValueContainer()
    {
        return this.dimension;
    }
    public final void fireDimensionChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeDimensionChanged(currentValue);
            firePropertyChange(PROPERTYNAME_DIMENSION, oldValue, currentValue);
            afterDimensionChanged(currentValue);
        }
    }

    public void beforeDimensionChanged( String dimension)
    { }
    public void afterDimensionChanged( String dimension)
    { }

    @Filtrable(value = "true", defaultValue = "", defaultSelection = "")
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="3")
        ,@LocalizedTag(language="fr", name="column", value="2")
        ,@LocalizedTag(language="en", name="label", value="Forme")
        ,@LocalizedTag(language="fr", name="label", value="Forme")
    })
    public final String getForm()
    {
         return this.form.getValue();
    }

    public final void setForm(String form)
    {

        String oldValue = this.form.getValue();
        this.form.setValue(form);

        String currentValue = this.form.getValue();

        fireFormChange(currentValue, oldValue);
    }
    public final String removeForm()
    {
        String oldValue = this.form.getValue();
        String removedValue = this.form.removeValue();
        String currentValue = this.form.getValue();

        fireFormChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultForm(String form)
    {
        String oldValue = this.form.getValue();
        this.form.setDefaultValue(form);

        String currentValue = this.form.getValue();

        fireFormChange(currentValue, oldValue);
    }
    
    public final String removeDefaultForm()
    {
        String oldValue = this.form.getValue();
        String removedValue = this.form.removeDefaultValue();
        String currentValue = this.form.getValue();

        fireFormChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getFormValueContainer()
    {
        return this.form;
    }
    public final void fireFormChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeFormChanged(currentValue);
            firePropertyChange(PROPERTYNAME_FORM, oldValue, currentValue);
            afterFormChanged(currentValue);
        }
    }

    public void beforeFormChanged( String form)
    { }
    public void afterFormChanged( String form)
    { }


    public boolean isFormVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="4")
        ,@LocalizedTag(language="fr", name="column", value="4")
        ,@LocalizedTag(language="en", name="label", value="Width")
        ,@LocalizedTag(language="fr", name="label", value="Largeur")
    })
    public final String getWidth()
    {
         return this.width.getValue();
    }

    public final void setWidth(String width)
    {

        String oldValue = this.width.getValue();
        this.width.setValue(width);

        String currentValue = this.width.getValue();

        fireWidthChange(currentValue, oldValue);
    }
    public final String removeWidth()
    {
        String oldValue = this.width.getValue();
        String removedValue = this.width.removeValue();
        String currentValue = this.width.getValue();

        fireWidthChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultWidth(String width)
    {
        String oldValue = this.width.getValue();
        this.width.setDefaultValue(width);

        String currentValue = this.width.getValue();

        fireWidthChange(currentValue, oldValue);
    }
    
    public final String removeDefaultWidth()
    {
        String oldValue = this.width.getValue();
        String removedValue = this.width.removeDefaultValue();
        String currentValue = this.width.getValue();

        fireWidthChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getWidthValueContainer()
    {
        return this.width;
    }
    public final void fireWidthChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeWidthChanged(currentValue);
            firePropertyChange(PROPERTYNAME_WIDTH, oldValue, currentValue);
            afterWidthChanged(currentValue);
        }
    }

    public void beforeWidthChanged( String width)
    { }
    public void afterWidthChanged( String width)
    { }


    public boolean isWidthVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="5")
        ,@LocalizedTag(language="fr", name="column", value="5")
        ,@LocalizedTag(language="en", name="label", value="Height")
        ,@LocalizedTag(language="fr", name="label", value="Hauteur")
    })
    public final String getHeight()
    {
         return this.height.getValue();
    }

    public final void setHeight(String height)
    {

        String oldValue = this.height.getValue();
        this.height.setValue(height);

        String currentValue = this.height.getValue();

        fireHeightChange(currentValue, oldValue);
    }
    public final String removeHeight()
    {
        String oldValue = this.height.getValue();
        String removedValue = this.height.removeValue();
        String currentValue = this.height.getValue();

        fireHeightChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultHeight(String height)
    {
        String oldValue = this.height.getValue();
        this.height.setDefaultValue(height);

        String currentValue = this.height.getValue();

        fireHeightChange(currentValue, oldValue);
    }
    
    public final String removeDefaultHeight()
    {
        String oldValue = this.height.getValue();
        String removedValue = this.height.removeDefaultValue();
        String currentValue = this.height.getValue();

        fireHeightChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getHeightValueContainer()
    {
        return this.height;
    }
    public final void fireHeightChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeHeightChanged(currentValue);
            firePropertyChange(PROPERTYNAME_HEIGHT, oldValue, currentValue);
            afterHeightChanged(currentValue);
        }
    }

    public void beforeHeightChanged( String height)
    { }
    public void afterHeightChanged( String height)
    { }


    public boolean isHeightVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="7")
        ,@LocalizedTag(language="fr", name="column", value="7")
        ,@LocalizedTag(language="en", name="label", value="parametricModelClass")
        ,@LocalizedTag(language="fr", name="label", value="parametricModelClass")
    })
    public final String getParametricModelClass()
    {
         return this.parametricModelClass.getValue();
    }

    public final void setParametricModelClass(String parametricModelClass)
    {

        String oldValue = this.parametricModelClass.getValue();
        this.parametricModelClass.setValue(parametricModelClass);

        String currentValue = this.parametricModelClass.getValue();

        fireParametricModelClassChange(currentValue, oldValue);
    }
    public final String removeParametricModelClass()
    {
        String oldValue = this.parametricModelClass.getValue();
        String removedValue = this.parametricModelClass.removeValue();
        String currentValue = this.parametricModelClass.getValue();

        fireParametricModelClassChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultParametricModelClass(String parametricModelClass)
    {
        String oldValue = this.parametricModelClass.getValue();
        this.parametricModelClass.setDefaultValue(parametricModelClass);

        String currentValue = this.parametricModelClass.getValue();

        fireParametricModelClassChange(currentValue, oldValue);
    }
    
    public final String removeDefaultParametricModelClass()
    {
        String oldValue = this.parametricModelClass.getValue();
        String removedValue = this.parametricModelClass.removeDefaultValue();
        String currentValue = this.parametricModelClass.getValue();

        fireParametricModelClassChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getParametricModelClassValueContainer()
    {
        return this.parametricModelClass;
    }
    public final void fireParametricModelClassChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeParametricModelClassChanged(currentValue);
            firePropertyChange(PROPERTYNAME_PARAMETRICMODELCLASS, oldValue, currentValue);
            afterParametricModelClassChanged(currentValue);
        }
    }

    public void beforeParametricModelClassChanged( String parametricModelClass)
    { }
    public void afterParametricModelClassChanged( String parametricModelClass)
    { }


    public boolean isParametricModelClassVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="8")
        ,@LocalizedTag(language="fr", name="column", value="8")
        ,@LocalizedTag(language="en", name="label", value="parametricModelClass")
        ,@LocalizedTag(language="fr", name="label", value="parametricModelClass")
    })
    public final String getParametricModelClassSidelite()
    {
         return this.parametricModelClassSidelite.getValue();
    }

    public final void setParametricModelClassSidelite(String parametricModelClassSidelite)
    {

        String oldValue = this.parametricModelClassSidelite.getValue();
        this.parametricModelClassSidelite.setValue(parametricModelClassSidelite);

        String currentValue = this.parametricModelClassSidelite.getValue();

        fireParametricModelClassSideliteChange(currentValue, oldValue);
    }
    public final String removeParametricModelClassSidelite()
    {
        String oldValue = this.parametricModelClassSidelite.getValue();
        String removedValue = this.parametricModelClassSidelite.removeValue();
        String currentValue = this.parametricModelClassSidelite.getValue();

        fireParametricModelClassSideliteChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultParametricModelClassSidelite(String parametricModelClassSidelite)
    {
        String oldValue = this.parametricModelClassSidelite.getValue();
        this.parametricModelClassSidelite.setDefaultValue(parametricModelClassSidelite);

        String currentValue = this.parametricModelClassSidelite.getValue();

        fireParametricModelClassSideliteChange(currentValue, oldValue);
    }
    
    public final String removeDefaultParametricModelClassSidelite()
    {
        String oldValue = this.parametricModelClassSidelite.getValue();
        String removedValue = this.parametricModelClassSidelite.removeDefaultValue();
        String currentValue = this.parametricModelClassSidelite.getValue();

        fireParametricModelClassSideliteChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getParametricModelClassSideliteValueContainer()
    {
        return this.parametricModelClassSidelite;
    }
    public final void fireParametricModelClassSideliteChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeParametricModelClassSideliteChanged(currentValue);
            firePropertyChange(PROPERTYNAME_PARAMETRICMODELCLASSSIDELITE, oldValue, currentValue);
            afterParametricModelClassSideliteChanged(currentValue);
        }
    }

    public void beforeParametricModelClassSideliteChanged( String parametricModelClassSidelite)
    { }
    public void afterParametricModelClassSideliteChanged( String parametricModelClassSidelite)
    { }


    public boolean isParametricModelClassSideliteVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="9")
        ,@LocalizedTag(language="fr", name="column", value="9")
        ,@LocalizedTag(language="en", name="label", value="askStainedGlassPosition")
        ,@LocalizedTag(language="fr", name="label", value="askStainedGlassPosition")
    })
    public final String getAskStainedGlassPosition()
    {
         return this.askStainedGlassPosition.getValue();
    }

    public final void setAskStainedGlassPosition(String askStainedGlassPosition)
    {

        String oldValue = this.askStainedGlassPosition.getValue();
        this.askStainedGlassPosition.setValue(askStainedGlassPosition);

        String currentValue = this.askStainedGlassPosition.getValue();

        fireAskStainedGlassPositionChange(currentValue, oldValue);
    }
    public final String removeAskStainedGlassPosition()
    {
        String oldValue = this.askStainedGlassPosition.getValue();
        String removedValue = this.askStainedGlassPosition.removeValue();
        String currentValue = this.askStainedGlassPosition.getValue();

        fireAskStainedGlassPositionChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultAskStainedGlassPosition(String askStainedGlassPosition)
    {
        String oldValue = this.askStainedGlassPosition.getValue();
        this.askStainedGlassPosition.setDefaultValue(askStainedGlassPosition);

        String currentValue = this.askStainedGlassPosition.getValue();

        fireAskStainedGlassPositionChange(currentValue, oldValue);
    }
    
    public final String removeDefaultAskStainedGlassPosition()
    {
        String oldValue = this.askStainedGlassPosition.getValue();
        String removedValue = this.askStainedGlassPosition.removeDefaultValue();
        String currentValue = this.askStainedGlassPosition.getValue();

        fireAskStainedGlassPositionChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getAskStainedGlassPositionValueContainer()
    {
        return this.askStainedGlassPosition;
    }
    public final void fireAskStainedGlassPositionChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeAskStainedGlassPositionChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ASKSTAINEDGLASSPOSITION, oldValue, currentValue);
            afterAskStainedGlassPositionChanged(currentValue);
        }
    }

    public void beforeAskStainedGlassPositionChanged( String askStainedGlassPosition)
    { }
    public void afterAskStainedGlassPositionChanged( String askStainedGlassPosition)
    { }


    public boolean isAskStainedGlassPositionVisible()
    {
        return false;
    }

}
