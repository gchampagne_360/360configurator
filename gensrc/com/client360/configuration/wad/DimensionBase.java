// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 


public abstract class DimensionBase extends com.netappsid.wadconfigurator.Dimension
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(DimensionBase.class);

    // attributes

    // Bound properties


    // Constructors

    public DimensionBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.Dimension"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations



    @Override
    protected String getSequences()
    {
        return "choiceForm,perimeter,width,width2,height,height2,height3,ray,diameter,slope,feedRay,form,area,widthBrickMould,not set,choiceFlip";  
    }
}
