// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class SpecialQuestionaryBase extends com.netappsid.wadconfigurator.AdvancedSectionGrilles
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(SpecialQuestionaryBase.class);

    // attributes
    private ValueContainer<Measure<Length>> horizTdlPosition = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Integer> nbSdlHorizBars = new ValueContainer<Integer>(this);
    private ValueContainer<Integer> nbSdlVertiBars = new ValueContainer<Integer>(this);
    private ValueContainer<Measure<Length>> widthVertiTdl = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Measure<Length>> widthHorizTdl = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Measure<Length>> widthSdl = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Measure<Length>> widthVertiTdlKickPanel = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Boolean> sdlFrame = new ValueContainer<Boolean>(this);

    // Bound properties

    public static final String PROPERTYNAME_HORIZTDLPOSITION = "horizTdlPosition";  
    public static final String PROPERTYNAME_NBSDLHORIZBARS = "nbSdlHorizBars";  
    public static final String PROPERTYNAME_NBSDLVERTIBARS = "nbSdlVertiBars";  
    public static final String PROPERTYNAME_WIDTHVERTITDL = "widthVertiTdl";  
    public static final String PROPERTYNAME_WIDTHHORIZTDL = "widthHorizTdl";  
    public static final String PROPERTYNAME_WIDTHSDL = "widthSdl";  
    public static final String PROPERTYNAME_WIDTHVERTITDLKICKPANEL = "widthVertiTdlKickPanel";  
    public static final String PROPERTYNAME_SDLFRAME = "sdlFrame";  

    // Constructors

    public SpecialQuestionaryBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.SpecialQuestionary"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="TDL bar Horizontal position")
        ,@LocalizedTag(language="fr", name="label", value="Position horizontale barrotin")
    })
    public final Measure<Length> getHorizTdlPosition()
    {
         return this.horizTdlPosition.getValue();
    }

    public final void setHorizTdlPosition(Measure<Length> horizTdlPosition)
    {

        horizTdlPosition = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(horizTdlPosition, getDefaultUnitFor(PROPERTYNAME_HORIZTDLPOSITION,Length.class));

        Measure<Length> oldValue = this.horizTdlPosition.getValue();
        this.horizTdlPosition.setValue(horizTdlPosition);

        Measure<Length> currentValue = this.horizTdlPosition.getValue();

        fireHorizTdlPositionChange(currentValue, oldValue);
    }
    public final Measure<Length> removeHorizTdlPosition()
    {
        Measure<Length> oldValue = this.horizTdlPosition.getValue();
        Measure<Length> removedValue = this.horizTdlPosition.removeValue();
        Measure<Length> currentValue = this.horizTdlPosition.getValue();

        fireHorizTdlPositionChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultHorizTdlPosition(Measure<Length> horizTdlPosition)
    {
        horizTdlPosition = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(horizTdlPosition, getDefaultUnitFor(PROPERTYNAME_HORIZTDLPOSITION,Length.class));

        Measure<Length> oldValue = this.horizTdlPosition.getValue();
        this.horizTdlPosition.setDefaultValue(horizTdlPosition);

        Measure<Length> currentValue = this.horizTdlPosition.getValue();

        fireHorizTdlPositionChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultHorizTdlPosition()
    {
        Measure<Length> oldValue = this.horizTdlPosition.getValue();
        Measure<Length> removedValue = this.horizTdlPosition.removeDefaultValue();
        Measure<Length> currentValue = this.horizTdlPosition.getValue();

        fireHorizTdlPositionChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getHorizTdlPositionValueContainer()
    {
        return this.horizTdlPosition;
    }
    public final void fireHorizTdlPositionChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeHorizTdlPositionChanged(currentValue);
            firePropertyChange(PROPERTYNAME_HORIZTDLPOSITION, oldValue, currentValue);
            afterHorizTdlPositionChanged(currentValue);
        }
    }

    public void beforeHorizTdlPositionChanged( Measure<Length> horizTdlPosition)
    { }
    public void afterHorizTdlPositionChanged( Measure<Length> horizTdlPosition)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Horizontal grilles bars qty")
        ,@LocalizedTag(language="fr", name="label", value="Barres horizontales petit bois")
    })
    public final Integer getNbSdlHorizBars()
    {
         return this.nbSdlHorizBars.getValue();
    }

    public final void setNbSdlHorizBars(Integer nbSdlHorizBars)
    {

        Integer oldValue = this.nbSdlHorizBars.getValue();
        this.nbSdlHorizBars.setValue(nbSdlHorizBars);

        Integer currentValue = this.nbSdlHorizBars.getValue();

        fireNbSdlHorizBarsChange(currentValue, oldValue);
    }
    public final Integer removeNbSdlHorizBars()
    {
        Integer oldValue = this.nbSdlHorizBars.getValue();
        Integer removedValue = this.nbSdlHorizBars.removeValue();
        Integer currentValue = this.nbSdlHorizBars.getValue();

        fireNbSdlHorizBarsChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultNbSdlHorizBars(Integer nbSdlHorizBars)
    {
        Integer oldValue = this.nbSdlHorizBars.getValue();
        this.nbSdlHorizBars.setDefaultValue(nbSdlHorizBars);

        Integer currentValue = this.nbSdlHorizBars.getValue();

        fireNbSdlHorizBarsChange(currentValue, oldValue);
    }
    
    public final Integer removeDefaultNbSdlHorizBars()
    {
        Integer oldValue = this.nbSdlHorizBars.getValue();
        Integer removedValue = this.nbSdlHorizBars.removeDefaultValue();
        Integer currentValue = this.nbSdlHorizBars.getValue();

        fireNbSdlHorizBarsChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Integer> getNbSdlHorizBarsValueContainer()
    {
        return this.nbSdlHorizBars;
    }
    public final void fireNbSdlHorizBarsChange(Integer currentValue, Integer oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeNbSdlHorizBarsChanged(currentValue);
            firePropertyChange(PROPERTYNAME_NBSDLHORIZBARS, oldValue, currentValue);
            afterNbSdlHorizBarsChanged(currentValue);
        }
    }

    public void beforeNbSdlHorizBarsChanged( Integer nbSdlHorizBars)
    { }
    public void afterNbSdlHorizBarsChanged( Integer nbSdlHorizBars)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Vertical grilles barres qty")
        ,@LocalizedTag(language="fr", name="label", value="Barres verticales petit bois")
    })
    public final Integer getNbSdlVertiBars()
    {
         return this.nbSdlVertiBars.getValue();
    }

    public final void setNbSdlVertiBars(Integer nbSdlVertiBars)
    {

        Integer oldValue = this.nbSdlVertiBars.getValue();
        this.nbSdlVertiBars.setValue(nbSdlVertiBars);

        Integer currentValue = this.nbSdlVertiBars.getValue();

        fireNbSdlVertiBarsChange(currentValue, oldValue);
    }
    public final Integer removeNbSdlVertiBars()
    {
        Integer oldValue = this.nbSdlVertiBars.getValue();
        Integer removedValue = this.nbSdlVertiBars.removeValue();
        Integer currentValue = this.nbSdlVertiBars.getValue();

        fireNbSdlVertiBarsChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultNbSdlVertiBars(Integer nbSdlVertiBars)
    {
        Integer oldValue = this.nbSdlVertiBars.getValue();
        this.nbSdlVertiBars.setDefaultValue(nbSdlVertiBars);

        Integer currentValue = this.nbSdlVertiBars.getValue();

        fireNbSdlVertiBarsChange(currentValue, oldValue);
    }
    
    public final Integer removeDefaultNbSdlVertiBars()
    {
        Integer oldValue = this.nbSdlVertiBars.getValue();
        Integer removedValue = this.nbSdlVertiBars.removeDefaultValue();
        Integer currentValue = this.nbSdlVertiBars.getValue();

        fireNbSdlVertiBarsChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Integer> getNbSdlVertiBarsValueContainer()
    {
        return this.nbSdlVertiBars;
    }
    public final void fireNbSdlVertiBarsChange(Integer currentValue, Integer oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeNbSdlVertiBarsChanged(currentValue);
            firePropertyChange(PROPERTYNAME_NBSDLVERTIBARS, oldValue, currentValue);
            afterNbSdlVertiBarsChanged(currentValue);
        }
    }

    public void beforeNbSdlVertiBarsChanged( Integer nbSdlVertiBars)
    { }
    public void afterNbSdlVertiBarsChanged( Integer nbSdlVertiBars)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Vertical TDL thickness")
        ,@LocalizedTag(language="fr", name="label", value="Épaisseur verticale entre panneaux")
    })
    public final Measure<Length> getWidthVertiTdl()
    {
         return this.widthVertiTdl.getValue();
    }

    public final void setWidthVertiTdl(Measure<Length> widthVertiTdl)
    {

        widthVertiTdl = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(widthVertiTdl, getDefaultUnitFor(PROPERTYNAME_WIDTHVERTITDL,Length.class));

        Measure<Length> oldValue = this.widthVertiTdl.getValue();
        this.widthVertiTdl.setValue(widthVertiTdl);

        Measure<Length> currentValue = this.widthVertiTdl.getValue();

        fireWidthVertiTdlChange(currentValue, oldValue);
    }
    public final Measure<Length> removeWidthVertiTdl()
    {
        Measure<Length> oldValue = this.widthVertiTdl.getValue();
        Measure<Length> removedValue = this.widthVertiTdl.removeValue();
        Measure<Length> currentValue = this.widthVertiTdl.getValue();

        fireWidthVertiTdlChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultWidthVertiTdl(Measure<Length> widthVertiTdl)
    {
        widthVertiTdl = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(widthVertiTdl, getDefaultUnitFor(PROPERTYNAME_WIDTHVERTITDL,Length.class));

        Measure<Length> oldValue = this.widthVertiTdl.getValue();
        this.widthVertiTdl.setDefaultValue(widthVertiTdl);

        Measure<Length> currentValue = this.widthVertiTdl.getValue();

        fireWidthVertiTdlChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultWidthVertiTdl()
    {
        Measure<Length> oldValue = this.widthVertiTdl.getValue();
        Measure<Length> removedValue = this.widthVertiTdl.removeDefaultValue();
        Measure<Length> currentValue = this.widthVertiTdl.getValue();

        fireWidthVertiTdlChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getWidthVertiTdlValueContainer()
    {
        return this.widthVertiTdl;
    }
    public final void fireWidthVertiTdlChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeWidthVertiTdlChanged(currentValue);
            firePropertyChange(PROPERTYNAME_WIDTHVERTITDL, oldValue, currentValue);
            afterWidthVertiTdlChanged(currentValue);
        }
    }

    public void beforeWidthVertiTdlChanged( Measure<Length> widthVertiTdl)
    { }
    public void afterWidthVertiTdlChanged( Measure<Length> widthVertiTdl)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Horizontal TDL thickness")
        ,@LocalizedTag(language="fr", name="label", value="Épaisseur horizontale entre panneaux")
    })
    public final Measure<Length> getWidthHorizTdl()
    {
         return this.widthHorizTdl.getValue();
    }

    public final void setWidthHorizTdl(Measure<Length> widthHorizTdl)
    {

        widthHorizTdl = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(widthHorizTdl, getDefaultUnitFor(PROPERTYNAME_WIDTHHORIZTDL,Length.class));

        Measure<Length> oldValue = this.widthHorizTdl.getValue();
        this.widthHorizTdl.setValue(widthHorizTdl);

        Measure<Length> currentValue = this.widthHorizTdl.getValue();

        fireWidthHorizTdlChange(currentValue, oldValue);
    }
    public final Measure<Length> removeWidthHorizTdl()
    {
        Measure<Length> oldValue = this.widthHorizTdl.getValue();
        Measure<Length> removedValue = this.widthHorizTdl.removeValue();
        Measure<Length> currentValue = this.widthHorizTdl.getValue();

        fireWidthHorizTdlChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultWidthHorizTdl(Measure<Length> widthHorizTdl)
    {
        widthHorizTdl = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(widthHorizTdl, getDefaultUnitFor(PROPERTYNAME_WIDTHHORIZTDL,Length.class));

        Measure<Length> oldValue = this.widthHorizTdl.getValue();
        this.widthHorizTdl.setDefaultValue(widthHorizTdl);

        Measure<Length> currentValue = this.widthHorizTdl.getValue();

        fireWidthHorizTdlChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultWidthHorizTdl()
    {
        Measure<Length> oldValue = this.widthHorizTdl.getValue();
        Measure<Length> removedValue = this.widthHorizTdl.removeDefaultValue();
        Measure<Length> currentValue = this.widthHorizTdl.getValue();

        fireWidthHorizTdlChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getWidthHorizTdlValueContainer()
    {
        return this.widthHorizTdl;
    }
    public final void fireWidthHorizTdlChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeWidthHorizTdlChanged(currentValue);
            firePropertyChange(PROPERTYNAME_WIDTHHORIZTDL, oldValue, currentValue);
            afterWidthHorizTdlChanged(currentValue);
        }
    }

    public void beforeWidthHorizTdlChanged( Measure<Length> widthHorizTdl)
    { }
    public void afterWidthHorizTdlChanged( Measure<Length> widthHorizTdl)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="SDL thickness")
        ,@LocalizedTag(language="fr", name="label", value="Épaisseur petit bois")
    })
    public final Measure<Length> getWidthSdl()
    {
         return this.widthSdl.getValue();
    }

    public final void setWidthSdl(Measure<Length> widthSdl)
    {

        widthSdl = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(widthSdl, getDefaultUnitFor(PROPERTYNAME_WIDTHSDL,Length.class));

        Measure<Length> oldValue = this.widthSdl.getValue();
        this.widthSdl.setValue(widthSdl);

        Measure<Length> currentValue = this.widthSdl.getValue();

        fireWidthSdlChange(currentValue, oldValue);
    }
    public final Measure<Length> removeWidthSdl()
    {
        Measure<Length> oldValue = this.widthSdl.getValue();
        Measure<Length> removedValue = this.widthSdl.removeValue();
        Measure<Length> currentValue = this.widthSdl.getValue();

        fireWidthSdlChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultWidthSdl(Measure<Length> widthSdl)
    {
        widthSdl = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(widthSdl, getDefaultUnitFor(PROPERTYNAME_WIDTHSDL,Length.class));

        Measure<Length> oldValue = this.widthSdl.getValue();
        this.widthSdl.setDefaultValue(widthSdl);

        Measure<Length> currentValue = this.widthSdl.getValue();

        fireWidthSdlChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultWidthSdl()
    {
        Measure<Length> oldValue = this.widthSdl.getValue();
        Measure<Length> removedValue = this.widthSdl.removeDefaultValue();
        Measure<Length> currentValue = this.widthSdl.getValue();

        fireWidthSdlChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getWidthSdlValueContainer()
    {
        return this.widthSdl;
    }
    public final void fireWidthSdlChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeWidthSdlChanged(currentValue);
            firePropertyChange(PROPERTYNAME_WIDTHSDL, oldValue, currentValue);
            afterWidthSdlChanged(currentValue);
        }
    }

    public void beforeWidthSdlChanged( Measure<Length> widthSdl)
    { }
    public void afterWidthSdlChanged( Measure<Length> widthSdl)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Kickpanel vertical TDL thickness")
        ,@LocalizedTag(language="fr", name="label", value="Épaisseur verticale entre-panneau sous-bassement")
    })
    public final Measure<Length> getWidthVertiTdlKickPanel()
    {
         return this.widthVertiTdlKickPanel.getValue();
    }

    public final void setWidthVertiTdlKickPanel(Measure<Length> widthVertiTdlKickPanel)
    {

        widthVertiTdlKickPanel = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(widthVertiTdlKickPanel, getDefaultUnitFor(PROPERTYNAME_WIDTHVERTITDLKICKPANEL,Length.class));

        Measure<Length> oldValue = this.widthVertiTdlKickPanel.getValue();
        this.widthVertiTdlKickPanel.setValue(widthVertiTdlKickPanel);

        Measure<Length> currentValue = this.widthVertiTdlKickPanel.getValue();

        fireWidthVertiTdlKickPanelChange(currentValue, oldValue);
    }
    public final Measure<Length> removeWidthVertiTdlKickPanel()
    {
        Measure<Length> oldValue = this.widthVertiTdlKickPanel.getValue();
        Measure<Length> removedValue = this.widthVertiTdlKickPanel.removeValue();
        Measure<Length> currentValue = this.widthVertiTdlKickPanel.getValue();

        fireWidthVertiTdlKickPanelChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultWidthVertiTdlKickPanel(Measure<Length> widthVertiTdlKickPanel)
    {
        widthVertiTdlKickPanel = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(widthVertiTdlKickPanel, getDefaultUnitFor(PROPERTYNAME_WIDTHVERTITDLKICKPANEL,Length.class));

        Measure<Length> oldValue = this.widthVertiTdlKickPanel.getValue();
        this.widthVertiTdlKickPanel.setDefaultValue(widthVertiTdlKickPanel);

        Measure<Length> currentValue = this.widthVertiTdlKickPanel.getValue();

        fireWidthVertiTdlKickPanelChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultWidthVertiTdlKickPanel()
    {
        Measure<Length> oldValue = this.widthVertiTdlKickPanel.getValue();
        Measure<Length> removedValue = this.widthVertiTdlKickPanel.removeDefaultValue();
        Measure<Length> currentValue = this.widthVertiTdlKickPanel.getValue();

        fireWidthVertiTdlKickPanelChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getWidthVertiTdlKickPanelValueContainer()
    {
        return this.widthVertiTdlKickPanel;
    }
    public final void fireWidthVertiTdlKickPanelChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeWidthVertiTdlKickPanelChanged(currentValue);
            firePropertyChange(PROPERTYNAME_WIDTHVERTITDLKICKPANEL, oldValue, currentValue);
            afterWidthVertiTdlKickPanelChanged(currentValue);
        }
    }

    public void beforeWidthVertiTdlKickPanelChanged( Measure<Length> widthVertiTdlKickPanel)
    { }
    public void afterWidthVertiTdlKickPanelChanged( Measure<Length> widthVertiTdlKickPanel)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="SDL frame")
        ,@LocalizedTag(language="fr", name="label", value="Cadre petit bois")
    })
    public final Boolean getSdlFrame()
    {
         return this.sdlFrame.getValue();
    }

    public final void setSdlFrame(Boolean sdlFrame)
    {

        Boolean oldValue = this.sdlFrame.getValue();
        this.sdlFrame.setValue(sdlFrame);

        Boolean currentValue = this.sdlFrame.getValue();

        fireSdlFrameChange(currentValue, oldValue);
    }
    public final Boolean removeSdlFrame()
    {
        Boolean oldValue = this.sdlFrame.getValue();
        Boolean removedValue = this.sdlFrame.removeValue();
        Boolean currentValue = this.sdlFrame.getValue();

        fireSdlFrameChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultSdlFrame(Boolean sdlFrame)
    {
        Boolean oldValue = this.sdlFrame.getValue();
        this.sdlFrame.setDefaultValue(sdlFrame);

        Boolean currentValue = this.sdlFrame.getValue();

        fireSdlFrameChange(currentValue, oldValue);
    }
    
    public final Boolean removeDefaultSdlFrame()
    {
        Boolean oldValue = this.sdlFrame.getValue();
        Boolean removedValue = this.sdlFrame.removeDefaultValue();
        Boolean currentValue = this.sdlFrame.getValue();

        fireSdlFrameChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Boolean> getSdlFrameValueContainer()
    {
        return this.sdlFrame;
    }
    public final void fireSdlFrameChange(Boolean currentValue, Boolean oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeSdlFrameChanged(currentValue);
            firePropertyChange(PROPERTYNAME_SDLFRAME, oldValue, currentValue);
            afterSdlFrameChanged(currentValue);
        }
    }

    public void beforeSdlFrameChanged( Boolean sdlFrame)
    { }
    public void afterSdlFrameChanged( Boolean sdlFrame)
    { }



    @Override
    protected String getSequences()
    {
        return "horizTdlPosition,widthVertiTdl,widthHorizTdl,nbSdlHorizBars,nbSdlVertiBars,widthSdl,dimension,NoSplittingBars";  
    }
}
