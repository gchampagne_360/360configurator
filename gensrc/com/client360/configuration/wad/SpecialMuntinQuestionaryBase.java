// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class SpecialMuntinQuestionaryBase extends com.netappsid.wadconfigurator.AdvancedSectionGrilles
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(SpecialMuntinQuestionaryBase.class);

    // attributes
    private ValueContainer<Measure<Length>> posTopMuntin = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Measure<Length>> posBotMuntin = new ValueContainer<Measure<Length>>(this);

    // Bound properties

    public static final String PROPERTYNAME_POSTOPMUNTIN = "posTopMuntin";  
    public static final String PROPERTYNAME_POSBOTMUNTIN = "posBotMuntin";  

    // Constructors

    public SpecialMuntinQuestionaryBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.SpecialMuntinQuestionary"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Top muntin position")
        ,@LocalizedTag(language="fr", name="label", value="Postion du barrotin du haut")
    })
    public final Measure<Length> getPosTopMuntin()
    {
         return this.posTopMuntin.getValue();
    }

    public final void setPosTopMuntin(Measure<Length> posTopMuntin)
    {

        posTopMuntin = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(posTopMuntin, getDefaultUnitFor(PROPERTYNAME_POSTOPMUNTIN,Length.class));

        Measure<Length> oldValue = this.posTopMuntin.getValue();
        this.posTopMuntin.setValue(posTopMuntin);

        Measure<Length> currentValue = this.posTopMuntin.getValue();

        firePosTopMuntinChange(currentValue, oldValue);
    }
    public final Measure<Length> removePosTopMuntin()
    {
        Measure<Length> oldValue = this.posTopMuntin.getValue();
        Measure<Length> removedValue = this.posTopMuntin.removeValue();
        Measure<Length> currentValue = this.posTopMuntin.getValue();

        firePosTopMuntinChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultPosTopMuntin(Measure<Length> posTopMuntin)
    {
        posTopMuntin = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(posTopMuntin, getDefaultUnitFor(PROPERTYNAME_POSTOPMUNTIN,Length.class));

        Measure<Length> oldValue = this.posTopMuntin.getValue();
        this.posTopMuntin.setDefaultValue(posTopMuntin);

        Measure<Length> currentValue = this.posTopMuntin.getValue();

        firePosTopMuntinChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultPosTopMuntin()
    {
        Measure<Length> oldValue = this.posTopMuntin.getValue();
        Measure<Length> removedValue = this.posTopMuntin.removeDefaultValue();
        Measure<Length> currentValue = this.posTopMuntin.getValue();

        firePosTopMuntinChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getPosTopMuntinValueContainer()
    {
        return this.posTopMuntin;
    }
    public final void firePosTopMuntinChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforePosTopMuntinChanged(currentValue);
            firePropertyChange(PROPERTYNAME_POSTOPMUNTIN, oldValue, currentValue);
            afterPosTopMuntinChanged(currentValue);
        }
    }

    public void beforePosTopMuntinChanged( Measure<Length> posTopMuntin)
    { }
    public void afterPosTopMuntinChanged( Measure<Length> posTopMuntin)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Bottom muntin position")
        ,@LocalizedTag(language="fr", name="label", value="Position du barrotin du bas")
    })
    public final Measure<Length> getPosBotMuntin()
    {
         return this.posBotMuntin.getValue();
    }

    public final void setPosBotMuntin(Measure<Length> posBotMuntin)
    {

        posBotMuntin = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(posBotMuntin, getDefaultUnitFor(PROPERTYNAME_POSBOTMUNTIN,Length.class));

        Measure<Length> oldValue = this.posBotMuntin.getValue();
        this.posBotMuntin.setValue(posBotMuntin);

        Measure<Length> currentValue = this.posBotMuntin.getValue();

        firePosBotMuntinChange(currentValue, oldValue);
    }
    public final Measure<Length> removePosBotMuntin()
    {
        Measure<Length> oldValue = this.posBotMuntin.getValue();
        Measure<Length> removedValue = this.posBotMuntin.removeValue();
        Measure<Length> currentValue = this.posBotMuntin.getValue();

        firePosBotMuntinChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultPosBotMuntin(Measure<Length> posBotMuntin)
    {
        posBotMuntin = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(posBotMuntin, getDefaultUnitFor(PROPERTYNAME_POSBOTMUNTIN,Length.class));

        Measure<Length> oldValue = this.posBotMuntin.getValue();
        this.posBotMuntin.setDefaultValue(posBotMuntin);

        Measure<Length> currentValue = this.posBotMuntin.getValue();

        firePosBotMuntinChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultPosBotMuntin()
    {
        Measure<Length> oldValue = this.posBotMuntin.getValue();
        Measure<Length> removedValue = this.posBotMuntin.removeDefaultValue();
        Measure<Length> currentValue = this.posBotMuntin.getValue();

        firePosBotMuntinChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getPosBotMuntinValueContainer()
    {
        return this.posBotMuntin;
    }
    public final void firePosBotMuntinChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforePosBotMuntinChanged(currentValue);
            firePropertyChange(PROPERTYNAME_POSBOTMUNTIN, oldValue, currentValue);
            afterPosBotMuntinChanged(currentValue);
        }
    }

    public void beforePosBotMuntinChanged( Measure<Length> posBotMuntin)
    { }
    public void afterPosBotMuntinChanged( Measure<Length> posBotMuntin)
    { }


}
