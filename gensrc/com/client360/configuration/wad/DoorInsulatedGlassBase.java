// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class DoorInsulatedGlassBase extends com.netappsid.erp.configurator.Configurable
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(DoorInsulatedGlassBase.class);

    // attributes
    private ValueContainer<Boolean> inex = new ValueContainer<Boolean>(this);
    private ValueContainer<Boolean> hasCustomGrilles = new ValueContainer<Boolean>(this);
    private ValueContainer<Boolean> otherInsulatedGass = new ValueContainer<Boolean>(this);
    private ValueContainer<String> otherInsulatedGassDescription = new ValueContainer<String>(this);
    private ValueContainer<Double> price = new ValueContainer<Double>(this);
    private ValueContainer<String> customGrillesType = new ValueContainer<String>(this);
    private ValueContainer<String> customGrillesBar = new ValueContainer<String>(this);
    private ValueContainer<String> customGrillesPrice = new ValueContainer<String>(this);
    private ValueContainer<ChoiceInsulatedGlassTheo> choiceInsulatedGlassTheo = new ValueContainer<ChoiceInsulatedGlassTheo>(this);

    // Bound properties

    public static final String PROPERTYNAME_INEX = "inex";  
    public static final String PROPERTYNAME_HASCUSTOMGRILLES = "hasCustomGrilles";  
    public static final String PROPERTYNAME_OTHERINSULATEDGASS = "otherInsulatedGass";  
    public static final String PROPERTYNAME_OTHERINSULATEDGASSDESCRIPTION = "otherInsulatedGassDescription";  
    public static final String PROPERTYNAME_PRICE = "price";  
    public static final String PROPERTYNAME_CUSTOMGRILLESTYPE = "customGrillesType";  
    public static final String PROPERTYNAME_CUSTOMGRILLESBAR = "customGrillesBar";  
    public static final String PROPERTYNAME_CUSTOMGRILLESPRICE = "customGrillesPrice";  
    public static final String PROPERTYNAME_CHOICEINSULATEDGLASSTHEO = "choiceInsulatedGlassTheo";  

    // Constructors

    public DoorInsulatedGlassBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.DoorInsulatedGlass"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="INEX")
        ,@LocalizedTag(language="fr", name="label", value="INEX")
    })
    public final Boolean getInex()
    {
         return this.inex.getValue();
    }

    public final void setInex(Boolean inex)
    {

        Boolean oldValue = this.inex.getValue();
        this.inex.setValue(inex);

        Boolean currentValue = this.inex.getValue();

        fireInexChange(currentValue, oldValue);
    }
    public final Boolean removeInex()
    {
        Boolean oldValue = this.inex.getValue();
        Boolean removedValue = this.inex.removeValue();
        Boolean currentValue = this.inex.getValue();

        fireInexChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultInex(Boolean inex)
    {
        Boolean oldValue = this.inex.getValue();
        this.inex.setDefaultValue(inex);

        Boolean currentValue = this.inex.getValue();

        fireInexChange(currentValue, oldValue);
    }
    
    public final Boolean removeDefaultInex()
    {
        Boolean oldValue = this.inex.getValue();
        Boolean removedValue = this.inex.removeDefaultValue();
        Boolean currentValue = this.inex.getValue();

        fireInexChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Boolean> getInexValueContainer()
    {
        return this.inex;
    }
    public final void fireInexChange(Boolean currentValue, Boolean oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeInexChanged(currentValue);
            firePropertyChange(PROPERTYNAME_INEX, oldValue, currentValue);
            afterInexChanged(currentValue);
        }
    }

    public void beforeInexChanged( Boolean inex)
    { }
    public void afterInexChanged( Boolean inex)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Non Standar Grills")
        ,@LocalizedTag(language="fr", name="label", value="Carrelage non standard")
    })
    public final Boolean getHasCustomGrilles()
    {
         return this.hasCustomGrilles.getValue();
    }

    public final void setHasCustomGrilles(Boolean hasCustomGrilles)
    {

        Boolean oldValue = this.hasCustomGrilles.getValue();
        this.hasCustomGrilles.setValue(hasCustomGrilles);

        Boolean currentValue = this.hasCustomGrilles.getValue();

        fireHasCustomGrillesChange(currentValue, oldValue);
    }
    public final Boolean removeHasCustomGrilles()
    {
        Boolean oldValue = this.hasCustomGrilles.getValue();
        Boolean removedValue = this.hasCustomGrilles.removeValue();
        Boolean currentValue = this.hasCustomGrilles.getValue();

        fireHasCustomGrillesChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultHasCustomGrilles(Boolean hasCustomGrilles)
    {
        Boolean oldValue = this.hasCustomGrilles.getValue();
        this.hasCustomGrilles.setDefaultValue(hasCustomGrilles);

        Boolean currentValue = this.hasCustomGrilles.getValue();

        fireHasCustomGrillesChange(currentValue, oldValue);
    }
    
    public final Boolean removeDefaultHasCustomGrilles()
    {
        Boolean oldValue = this.hasCustomGrilles.getValue();
        Boolean removedValue = this.hasCustomGrilles.removeDefaultValue();
        Boolean currentValue = this.hasCustomGrilles.getValue();

        fireHasCustomGrillesChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Boolean> getHasCustomGrillesValueContainer()
    {
        return this.hasCustomGrilles;
    }
    public final void fireHasCustomGrillesChange(Boolean currentValue, Boolean oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeHasCustomGrillesChanged(currentValue);
            firePropertyChange(PROPERTYNAME_HASCUSTOMGRILLES, oldValue, currentValue);
            afterHasCustomGrillesChanged(currentValue);
        }
    }

    public void beforeHasCustomGrillesChanged( Boolean hasCustomGrilles)
    { }
    public void afterHasCustomGrillesChanged( Boolean hasCustomGrilles)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Other Kind of Thermos ")
        ,@LocalizedTag(language="fr", name="label", value="Autre type de Thermos")
    })
    public final Boolean getOtherInsulatedGass()
    {
         return this.otherInsulatedGass.getValue();
    }

    public final void setOtherInsulatedGass(Boolean otherInsulatedGass)
    {

        Boolean oldValue = this.otherInsulatedGass.getValue();
        this.otherInsulatedGass.setValue(otherInsulatedGass);

        Boolean currentValue = this.otherInsulatedGass.getValue();

        fireOtherInsulatedGassChange(currentValue, oldValue);
    }
    public final Boolean removeOtherInsulatedGass()
    {
        Boolean oldValue = this.otherInsulatedGass.getValue();
        Boolean removedValue = this.otherInsulatedGass.removeValue();
        Boolean currentValue = this.otherInsulatedGass.getValue();

        fireOtherInsulatedGassChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultOtherInsulatedGass(Boolean otherInsulatedGass)
    {
        Boolean oldValue = this.otherInsulatedGass.getValue();
        this.otherInsulatedGass.setDefaultValue(otherInsulatedGass);

        Boolean currentValue = this.otherInsulatedGass.getValue();

        fireOtherInsulatedGassChange(currentValue, oldValue);
    }
    
    public final Boolean removeDefaultOtherInsulatedGass()
    {
        Boolean oldValue = this.otherInsulatedGass.getValue();
        Boolean removedValue = this.otherInsulatedGass.removeDefaultValue();
        Boolean currentValue = this.otherInsulatedGass.getValue();

        fireOtherInsulatedGassChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Boolean> getOtherInsulatedGassValueContainer()
    {
        return this.otherInsulatedGass;
    }
    public final void fireOtherInsulatedGassChange(Boolean currentValue, Boolean oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeOtherInsulatedGassChanged(currentValue);
            firePropertyChange(PROPERTYNAME_OTHERINSULATEDGASS, oldValue, currentValue);
            afterOtherInsulatedGassChanged(currentValue);
        }
    }

    public void beforeOtherInsulatedGassChanged( Boolean otherInsulatedGass)
    { }
    public void afterOtherInsulatedGassChanged( Boolean otherInsulatedGass)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Thermos Description")
        ,@LocalizedTag(language="fr", name="label", value="Thermos description")
    })
    public final String getOtherInsulatedGassDescription()
    {
         return this.otherInsulatedGassDescription.getValue();
    }

    public final void setOtherInsulatedGassDescription(String otherInsulatedGassDescription)
    {

        String oldValue = this.otherInsulatedGassDescription.getValue();
        this.otherInsulatedGassDescription.setValue(otherInsulatedGassDescription);

        String currentValue = this.otherInsulatedGassDescription.getValue();

        fireOtherInsulatedGassDescriptionChange(currentValue, oldValue);
    }
    public final String removeOtherInsulatedGassDescription()
    {
        String oldValue = this.otherInsulatedGassDescription.getValue();
        String removedValue = this.otherInsulatedGassDescription.removeValue();
        String currentValue = this.otherInsulatedGassDescription.getValue();

        fireOtherInsulatedGassDescriptionChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultOtherInsulatedGassDescription(String otherInsulatedGassDescription)
    {
        String oldValue = this.otherInsulatedGassDescription.getValue();
        this.otherInsulatedGassDescription.setDefaultValue(otherInsulatedGassDescription);

        String currentValue = this.otherInsulatedGassDescription.getValue();

        fireOtherInsulatedGassDescriptionChange(currentValue, oldValue);
    }
    
    public final String removeDefaultOtherInsulatedGassDescription()
    {
        String oldValue = this.otherInsulatedGassDescription.getValue();
        String removedValue = this.otherInsulatedGassDescription.removeDefaultValue();
        String currentValue = this.otherInsulatedGassDescription.getValue();

        fireOtherInsulatedGassDescriptionChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getOtherInsulatedGassDescriptionValueContainer()
    {
        return this.otherInsulatedGassDescription;
    }
    public final void fireOtherInsulatedGassDescriptionChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeOtherInsulatedGassDescriptionChanged(currentValue);
            firePropertyChange(PROPERTYNAME_OTHERINSULATEDGASSDESCRIPTION, oldValue, currentValue);
            afterOtherInsulatedGassDescriptionChanged(currentValue);
        }
    }

    public void beforeOtherInsulatedGassDescriptionChanged( String otherInsulatedGassDescription)
    { }
    public void afterOtherInsulatedGassDescriptionChanged( String otherInsulatedGassDescription)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Price per Meter�")
        ,@LocalizedTag(language="fr", name="label", value="Prix au m�")
    })
    public final Double getPrice()
    {
         return this.price.getValue();
    }

    public final void setPrice(Double price)
    {

        Double oldValue = this.price.getValue();
        this.price.setValue(price);

        Double currentValue = this.price.getValue();

        firePriceChange(currentValue, oldValue);
    }
    public final Double removePrice()
    {
        Double oldValue = this.price.getValue();
        Double removedValue = this.price.removeValue();
        Double currentValue = this.price.getValue();

        firePriceChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultPrice(Double price)
    {
        Double oldValue = this.price.getValue();
        this.price.setDefaultValue(price);

        Double currentValue = this.price.getValue();

        firePriceChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultPrice()
    {
        Double oldValue = this.price.getValue();
        Double removedValue = this.price.removeDefaultValue();
        Double currentValue = this.price.getValue();

        firePriceChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getPriceValueContainer()
    {
        return this.price;
    }
    public final void firePriceChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforePriceChanged(currentValue);
            firePropertyChange(PROPERTYNAME_PRICE, oldValue, currentValue);
            afterPriceChanged(currentValue);
        }
    }

    public void beforePriceChanged( Double price)
    { }
    public void afterPriceChanged( Double price)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Grill Type")
        ,@LocalizedTag(language="fr", name="label", value="Type de carrelage")
    })
    public final String getCustomGrillesType()
    {
         return this.customGrillesType.getValue();
    }

    public final void setCustomGrillesType(String customGrillesType)
    {

        String oldValue = this.customGrillesType.getValue();
        this.customGrillesType.setValue(customGrillesType);

        String currentValue = this.customGrillesType.getValue();

        fireCustomGrillesTypeChange(currentValue, oldValue);
    }
    public final String removeCustomGrillesType()
    {
        String oldValue = this.customGrillesType.getValue();
        String removedValue = this.customGrillesType.removeValue();
        String currentValue = this.customGrillesType.getValue();

        fireCustomGrillesTypeChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultCustomGrillesType(String customGrillesType)
    {
        String oldValue = this.customGrillesType.getValue();
        this.customGrillesType.setDefaultValue(customGrillesType);

        String currentValue = this.customGrillesType.getValue();

        fireCustomGrillesTypeChange(currentValue, oldValue);
    }
    
    public final String removeDefaultCustomGrillesType()
    {
        String oldValue = this.customGrillesType.getValue();
        String removedValue = this.customGrillesType.removeDefaultValue();
        String currentValue = this.customGrillesType.getValue();

        fireCustomGrillesTypeChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getCustomGrillesTypeValueContainer()
    {
        return this.customGrillesType;
    }
    public final void fireCustomGrillesTypeChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeCustomGrillesTypeChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CUSTOMGRILLESTYPE, oldValue, currentValue);
            afterCustomGrillesTypeChanged(currentValue);
        }
    }

    public void beforeCustomGrillesTypeChanged( String customGrillesType)
    { }
    public void afterCustomGrillesTypeChanged( String customGrillesType)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Grill Bar Type")
        ,@LocalizedTag(language="fr", name="label", value="Type de tige")
    })
    public final String getCustomGrillesBar()
    {
         return this.customGrillesBar.getValue();
    }

    public final void setCustomGrillesBar(String customGrillesBar)
    {

        String oldValue = this.customGrillesBar.getValue();
        this.customGrillesBar.setValue(customGrillesBar);

        String currentValue = this.customGrillesBar.getValue();

        fireCustomGrillesBarChange(currentValue, oldValue);
    }
    public final String removeCustomGrillesBar()
    {
        String oldValue = this.customGrillesBar.getValue();
        String removedValue = this.customGrillesBar.removeValue();
        String currentValue = this.customGrillesBar.getValue();

        fireCustomGrillesBarChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultCustomGrillesBar(String customGrillesBar)
    {
        String oldValue = this.customGrillesBar.getValue();
        this.customGrillesBar.setDefaultValue(customGrillesBar);

        String currentValue = this.customGrillesBar.getValue();

        fireCustomGrillesBarChange(currentValue, oldValue);
    }
    
    public final String removeDefaultCustomGrillesBar()
    {
        String oldValue = this.customGrillesBar.getValue();
        String removedValue = this.customGrillesBar.removeDefaultValue();
        String currentValue = this.customGrillesBar.getValue();

        fireCustomGrillesBarChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getCustomGrillesBarValueContainer()
    {
        return this.customGrillesBar;
    }
    public final void fireCustomGrillesBarChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeCustomGrillesBarChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CUSTOMGRILLESBAR, oldValue, currentValue);
            afterCustomGrillesBarChanged(currentValue);
        }
    }

    public void beforeCustomGrillesBarChanged( String customGrillesBar)
    { }
    public void afterCustomGrillesBarChanged( String customGrillesBar)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Price")
        ,@LocalizedTag(language="fr", name="label", value="Prix")
    })
    public final String getCustomGrillesPrice()
    {
         return this.customGrillesPrice.getValue();
    }

    public final void setCustomGrillesPrice(String customGrillesPrice)
    {

        String oldValue = this.customGrillesPrice.getValue();
        this.customGrillesPrice.setValue(customGrillesPrice);

        String currentValue = this.customGrillesPrice.getValue();

        fireCustomGrillesPriceChange(currentValue, oldValue);
    }
    public final String removeCustomGrillesPrice()
    {
        String oldValue = this.customGrillesPrice.getValue();
        String removedValue = this.customGrillesPrice.removeValue();
        String currentValue = this.customGrillesPrice.getValue();

        fireCustomGrillesPriceChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultCustomGrillesPrice(String customGrillesPrice)
    {
        String oldValue = this.customGrillesPrice.getValue();
        this.customGrillesPrice.setDefaultValue(customGrillesPrice);

        String currentValue = this.customGrillesPrice.getValue();

        fireCustomGrillesPriceChange(currentValue, oldValue);
    }
    
    public final String removeDefaultCustomGrillesPrice()
    {
        String oldValue = this.customGrillesPrice.getValue();
        String removedValue = this.customGrillesPrice.removeDefaultValue();
        String currentValue = this.customGrillesPrice.getValue();

        fireCustomGrillesPriceChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getCustomGrillesPriceValueContainer()
    {
        return this.customGrillesPrice;
    }
    public final void fireCustomGrillesPriceChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeCustomGrillesPriceChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CUSTOMGRILLESPRICE, oldValue, currentValue);
            afterCustomGrillesPriceChanged(currentValue);
        }
    }

    public void beforeCustomGrillesPriceChanged( String customGrillesPrice)
    { }
    public void afterCustomGrillesPriceChanged( String customGrillesPrice)
    { }

    @LocalizedTags
    ({
    })
    public final ChoiceInsulatedGlassTheo getChoiceInsulatedGlassTheo()
    {
         return this.choiceInsulatedGlassTheo.getValue();
    }

    public final void setChoiceInsulatedGlassTheo(ChoiceInsulatedGlassTheo choiceInsulatedGlassTheo)
    {

        if (choiceInsulatedGlassTheo != null)
        {
            acquire(choiceInsulatedGlassTheo, "choiceInsulatedGlassTheo");
        }
        ChoiceInsulatedGlassTheo oldValue = this.choiceInsulatedGlassTheo.getValue();
        this.choiceInsulatedGlassTheo.setValue(choiceInsulatedGlassTheo);

        ChoiceInsulatedGlassTheo currentValue = this.choiceInsulatedGlassTheo.getValue();

        fireChoiceInsulatedGlassTheoChange(currentValue, oldValue);
    }
    public final ChoiceInsulatedGlassTheo removeChoiceInsulatedGlassTheo()
    {
        ChoiceInsulatedGlassTheo oldValue = this.choiceInsulatedGlassTheo.getValue();
        ChoiceInsulatedGlassTheo removedValue = this.choiceInsulatedGlassTheo.removeValue();
        ChoiceInsulatedGlassTheo currentValue = this.choiceInsulatedGlassTheo.getValue();

        fireChoiceInsulatedGlassTheoChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceInsulatedGlassTheo(ChoiceInsulatedGlassTheo choiceInsulatedGlassTheo)
    {
        if (choiceInsulatedGlassTheo != null)
        {
            acquire(choiceInsulatedGlassTheo, "choiceInsulatedGlassTheo");
        }
        ChoiceInsulatedGlassTheo oldValue = this.choiceInsulatedGlassTheo.getValue();
        this.choiceInsulatedGlassTheo.setDefaultValue(choiceInsulatedGlassTheo);

        ChoiceInsulatedGlassTheo currentValue = this.choiceInsulatedGlassTheo.getValue();

        fireChoiceInsulatedGlassTheoChange(currentValue, oldValue);
    }
    
    public final ChoiceInsulatedGlassTheo removeDefaultChoiceInsulatedGlassTheo()
    {
        ChoiceInsulatedGlassTheo oldValue = this.choiceInsulatedGlassTheo.getValue();
        ChoiceInsulatedGlassTheo removedValue = this.choiceInsulatedGlassTheo.removeDefaultValue();
        ChoiceInsulatedGlassTheo currentValue = this.choiceInsulatedGlassTheo.getValue();

        fireChoiceInsulatedGlassTheoChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceInsulatedGlassTheo> getChoiceInsulatedGlassTheoValueContainer()
    {
        return this.choiceInsulatedGlassTheo;
    }
    public final void fireChoiceInsulatedGlassTheoChange(ChoiceInsulatedGlassTheo currentValue, ChoiceInsulatedGlassTheo oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceInsulatedGlassTheoChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEINSULATEDGLASSTHEO, oldValue, currentValue);
            afterChoiceInsulatedGlassTheoChanged(currentValue);
        }
    }

    public void beforeChoiceInsulatedGlassTheoChanged( ChoiceInsulatedGlassTheo choiceInsulatedGlassTheo)
    { }
    public void afterChoiceInsulatedGlassTheoChanged( ChoiceInsulatedGlassTheo choiceInsulatedGlassTheo)
    { }


    public boolean isChoiceInsulatedGlassTheoMandatory()
    {
        return true;
    }


    @Override
    protected String getSequences()
    {
        return "otherInsulatedGass,otherInsulatedGassDescription,price,choiceGlassType,choiceGlassTreatment,choiceEnergeticGlassTreatment,argon,choiceInsulatedGlassTheo,choiceGlassThickness,choiceGlassSupplier,dimension,choiceSpacerType,inex,hasCustomGrilles,choiceSectionGrille,sectionGrille,choiceLowePosition,choiceGlassSize,choiceTint,customGrillesType,customGrillesBar,customGrillesPrice,choiceArchitecturalGrilles,choicePatioPrimaGrilles";  
    }
}
