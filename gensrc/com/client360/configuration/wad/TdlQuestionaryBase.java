// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class TdlQuestionaryBase extends com.netappsid.wadconfigurator.AdvancedSectionGrilles
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(TdlQuestionaryBase.class);

    // attributes
    private ValueContainer<Integer> nbHorizSquare = new ValueContainer<Integer>(this);
    private ValueContainer<Integer> nbVertiSquare = new ValueContainer<Integer>(this);

    // Bound properties

    public static final String PROPERTYNAME_NBHORIZSQUARE = "nbHorizSquare";  
    public static final String PROPERTYNAME_NBVERTISQUARE = "nbVertiSquare";  

    // Constructors

    public TdlQuestionaryBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.TdlQuestionary"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Horizontal muntin")
        ,@LocalizedTag(language="fr", name="label", value="Barrotin horizontaux")
    })
    public final Integer getNbHorizSquare()
    {
         return this.nbHorizSquare.getValue();
    }

    public final void setNbHorizSquare(Integer nbHorizSquare)
    {

        Integer oldValue = this.nbHorizSquare.getValue();
        this.nbHorizSquare.setValue(nbHorizSquare);

        Integer currentValue = this.nbHorizSquare.getValue();

        fireNbHorizSquareChange(currentValue, oldValue);
    }
    public final Integer removeNbHorizSquare()
    {
        Integer oldValue = this.nbHorizSquare.getValue();
        Integer removedValue = this.nbHorizSquare.removeValue();
        Integer currentValue = this.nbHorizSquare.getValue();

        fireNbHorizSquareChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultNbHorizSquare(Integer nbHorizSquare)
    {
        Integer oldValue = this.nbHorizSquare.getValue();
        this.nbHorizSquare.setDefaultValue(nbHorizSquare);

        Integer currentValue = this.nbHorizSquare.getValue();

        fireNbHorizSquareChange(currentValue, oldValue);
    }
    
    public final Integer removeDefaultNbHorizSquare()
    {
        Integer oldValue = this.nbHorizSquare.getValue();
        Integer removedValue = this.nbHorizSquare.removeDefaultValue();
        Integer currentValue = this.nbHorizSquare.getValue();

        fireNbHorizSquareChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Integer> getNbHorizSquareValueContainer()
    {
        return this.nbHorizSquare;
    }
    public final void fireNbHorizSquareChange(Integer currentValue, Integer oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeNbHorizSquareChanged(currentValue);
            firePropertyChange(PROPERTYNAME_NBHORIZSQUARE, oldValue, currentValue);
            afterNbHorizSquareChanged(currentValue);
        }
    }

    public void beforeNbHorizSquareChanged( Integer nbHorizSquare)
    { }
    public void afterNbHorizSquareChanged( Integer nbHorizSquare)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Vertical muntins")
        ,@LocalizedTag(language="fr", name="label", value="Barrotins verticaux")
    })
    public final Integer getNbVertiSquare()
    {
         return this.nbVertiSquare.getValue();
    }

    public final void setNbVertiSquare(Integer nbVertiSquare)
    {

        Integer oldValue = this.nbVertiSquare.getValue();
        this.nbVertiSquare.setValue(nbVertiSquare);

        Integer currentValue = this.nbVertiSquare.getValue();

        fireNbVertiSquareChange(currentValue, oldValue);
    }
    public final Integer removeNbVertiSquare()
    {
        Integer oldValue = this.nbVertiSquare.getValue();
        Integer removedValue = this.nbVertiSquare.removeValue();
        Integer currentValue = this.nbVertiSquare.getValue();

        fireNbVertiSquareChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultNbVertiSquare(Integer nbVertiSquare)
    {
        Integer oldValue = this.nbVertiSquare.getValue();
        this.nbVertiSquare.setDefaultValue(nbVertiSquare);

        Integer currentValue = this.nbVertiSquare.getValue();

        fireNbVertiSquareChange(currentValue, oldValue);
    }
    
    public final Integer removeDefaultNbVertiSquare()
    {
        Integer oldValue = this.nbVertiSquare.getValue();
        Integer removedValue = this.nbVertiSquare.removeDefaultValue();
        Integer currentValue = this.nbVertiSquare.getValue();

        fireNbVertiSquareChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Integer> getNbVertiSquareValueContainer()
    {
        return this.nbVertiSquare;
    }
    public final void fireNbVertiSquareChange(Integer currentValue, Integer oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeNbVertiSquareChanged(currentValue);
            firePropertyChange(PROPERTYNAME_NBVERTISQUARE, oldValue, currentValue);
            afterNbVertiSquareChanged(currentValue);
        }
    }

    public void beforeNbVertiSquareChanged( Integer nbVertiSquare)
    { }
    public void afterNbVertiSquareChanged( Integer nbVertiSquare)
    { }


}
