// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 


public abstract class CasementVisionPvcMullionBase extends com.client360.configuration.wad.CasementMainMullion
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(CasementVisionPvcMullionBase.class);

    // attributes

    // Bound properties


    // Constructors

    public CasementVisionPvcMullionBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.CasementVisionPvcMullion"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations



    @Override
    protected String getSequences()
    {
        return "ExteriorColor,not set,vertical,length,profile,from,to,leftPosition,topPosition";  
    }
}
