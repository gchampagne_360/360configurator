// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.client360.configuration.wad.enums.ChoiceTypeBlow;
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class BlowingBase extends com.netappsid.wadconfigurator.Blowing
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(BlowingBase.class);

    // attributes
    private ValueContainer<ChoiceTypeBlow> choiceTypeBlow = new ValueContainer<ChoiceTypeBlow>(this);

    // Bound properties

    public static final String PROPERTYNAME_CHOICETYPEBLOW = "choiceTypeBlow";  

    // Constructors

    public BlowingBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.Blowing"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="desc", value="Blowing type")
        ,@LocalizedTag(language="fr", name="desc", value="Type de soufflage")
        ,@LocalizedTag(language="en", name="label", value="Bowing type")
        ,@LocalizedTag(language="fr", name="label", value="Type soufflage")
    })
    public final ChoiceTypeBlow getChoiceTypeBlow()
    {
         return this.choiceTypeBlow.getValue();
    }

    public final void setChoiceTypeBlow(ChoiceTypeBlow choiceTypeBlow)
    {

        ChoiceTypeBlow oldValue = this.choiceTypeBlow.getValue();
        this.choiceTypeBlow.setValue(choiceTypeBlow);

        ChoiceTypeBlow currentValue = this.choiceTypeBlow.getValue();

        fireChoiceTypeBlowChange(currentValue, oldValue);
    }
    public final ChoiceTypeBlow removeChoiceTypeBlow()
    {
        ChoiceTypeBlow oldValue = this.choiceTypeBlow.getValue();
        ChoiceTypeBlow removedValue = this.choiceTypeBlow.removeValue();
        ChoiceTypeBlow currentValue = this.choiceTypeBlow.getValue();

        fireChoiceTypeBlowChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceTypeBlow(ChoiceTypeBlow choiceTypeBlow)
    {
        ChoiceTypeBlow oldValue = this.choiceTypeBlow.getValue();
        this.choiceTypeBlow.setDefaultValue(choiceTypeBlow);

        ChoiceTypeBlow currentValue = this.choiceTypeBlow.getValue();

        fireChoiceTypeBlowChange(currentValue, oldValue);
    }
    
    public final ChoiceTypeBlow removeDefaultChoiceTypeBlow()
    {
        ChoiceTypeBlow oldValue = this.choiceTypeBlow.getValue();
        ChoiceTypeBlow removedValue = this.choiceTypeBlow.removeDefaultValue();
        ChoiceTypeBlow currentValue = this.choiceTypeBlow.getValue();

        fireChoiceTypeBlowChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceTypeBlow> getChoiceTypeBlowValueContainer()
    {
        return this.choiceTypeBlow;
    }
    public final void fireChoiceTypeBlowChange(ChoiceTypeBlow currentValue, ChoiceTypeBlow oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceTypeBlowChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICETYPEBLOW, oldValue, currentValue);
            afterChoiceTypeBlowChanged(currentValue);
        }
    }

    public void beforeChoiceTypeBlowChanged( ChoiceTypeBlow choiceTypeBlow)
    { }
    public void afterChoiceTypeBlowChanged( ChoiceTypeBlow choiceTypeBlow)
    { }


}
