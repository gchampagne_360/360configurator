// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.client360.configuration.wad.enums.ChoiceFrameDepth;
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class CasementMilanoPvcBase extends com.client360.configuration.wad.CasementMain
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(CasementMilanoPvcBase.class);

    // attributes
    private ValueContainer<Boolean> isMabDimension = new ValueContainer<Boolean>(this);
    private ValueContainer<ChoiceFrameDepth> choiceFrameDepth = new ValueContainer<ChoiceFrameDepth>(this);
    private ValueContainer<Hardware> hardware = new ValueContainer<Hardware>(this);
    private ValueContainer<Dimension> dimensionMab = new ValueContainer<Dimension>(this);

    // Bound properties

    public static final String PROPERTYNAME_ISMABDIMENSION = "isMabDimension";  
    public static final String PROPERTYNAME_CHOICEFRAMEDEPTH = "choiceFrameDepth";  
    public static final String PROPERTYNAME_HARDWARE = "hardware";  
    public static final String PROPERTYNAME_DIMENSIONMAB = "dimensionMab";  

    // Constructors

    public CasementMilanoPvcBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.CasementMilanoPvc"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Measures with brick mould")
        ,@LocalizedTag(language="fr", name="label", value="Mesures avec Moulure � brique")
    })
    public final Boolean getIsMabDimension()
    {
         return this.isMabDimension.getValue();
    }

    public final void setIsMabDimension(Boolean isMabDimension)
    {

        Boolean oldValue = this.isMabDimension.getValue();
        this.isMabDimension.setValue(isMabDimension);

        Boolean currentValue = this.isMabDimension.getValue();

        fireIsMabDimensionChange(currentValue, oldValue);
    }
    public final Boolean removeIsMabDimension()
    {
        Boolean oldValue = this.isMabDimension.getValue();
        Boolean removedValue = this.isMabDimension.removeValue();
        Boolean currentValue = this.isMabDimension.getValue();

        fireIsMabDimensionChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultIsMabDimension(Boolean isMabDimension)
    {
        Boolean oldValue = this.isMabDimension.getValue();
        this.isMabDimension.setDefaultValue(isMabDimension);

        Boolean currentValue = this.isMabDimension.getValue();

        fireIsMabDimensionChange(currentValue, oldValue);
    }
    
    public final Boolean removeDefaultIsMabDimension()
    {
        Boolean oldValue = this.isMabDimension.getValue();
        Boolean removedValue = this.isMabDimension.removeDefaultValue();
        Boolean currentValue = this.isMabDimension.getValue();

        fireIsMabDimensionChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Boolean> getIsMabDimensionValueContainer()
    {
        return this.isMabDimension;
    }
    public final void fireIsMabDimensionChange(Boolean currentValue, Boolean oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeIsMabDimensionChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ISMABDIMENSION, oldValue, currentValue);
            afterIsMabDimensionChanged(currentValue);
        }
    }

    public void beforeIsMabDimensionChanged( Boolean isMabDimension)
    { }
    public void afterIsMabDimensionChanged( Boolean isMabDimension)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="desc", value="Frame")
        ,@LocalizedTag(language="fr", name="desc", value="Cadre")
        ,@LocalizedTag(language="en", name="label", value="Frame")
        ,@LocalizedTag(language="fr", name="label", value="Cadre")
    })
    public final ChoiceFrameDepth getChoiceFrameDepth()
    {
         return this.choiceFrameDepth.getValue();
    }

    public final void setChoiceFrameDepth(ChoiceFrameDepth choiceFrameDepth)
    {

        ChoiceFrameDepth oldValue = this.choiceFrameDepth.getValue();
        this.choiceFrameDepth.setValue(choiceFrameDepth);

        ChoiceFrameDepth currentValue = this.choiceFrameDepth.getValue();

        fireChoiceFrameDepthChange(currentValue, oldValue);
    }
    public final ChoiceFrameDepth removeChoiceFrameDepth()
    {
        ChoiceFrameDepth oldValue = this.choiceFrameDepth.getValue();
        ChoiceFrameDepth removedValue = this.choiceFrameDepth.removeValue();
        ChoiceFrameDepth currentValue = this.choiceFrameDepth.getValue();

        fireChoiceFrameDepthChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceFrameDepth(ChoiceFrameDepth choiceFrameDepth)
    {
        ChoiceFrameDepth oldValue = this.choiceFrameDepth.getValue();
        this.choiceFrameDepth.setDefaultValue(choiceFrameDepth);

        ChoiceFrameDepth currentValue = this.choiceFrameDepth.getValue();

        fireChoiceFrameDepthChange(currentValue, oldValue);
    }
    
    public final ChoiceFrameDepth removeDefaultChoiceFrameDepth()
    {
        ChoiceFrameDepth oldValue = this.choiceFrameDepth.getValue();
        ChoiceFrameDepth removedValue = this.choiceFrameDepth.removeDefaultValue();
        ChoiceFrameDepth currentValue = this.choiceFrameDepth.getValue();

        fireChoiceFrameDepthChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceFrameDepth> getChoiceFrameDepthValueContainer()
    {
        return this.choiceFrameDepth;
    }
    public final void fireChoiceFrameDepthChange(ChoiceFrameDepth currentValue, ChoiceFrameDepth oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceFrameDepthChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEFRAMEDEPTH, oldValue, currentValue);
            afterChoiceFrameDepthChanged(currentValue);
        }
    }

    public void beforeChoiceFrameDepthChanged( ChoiceFrameDepth choiceFrameDepth)
    { }
    public void afterChoiceFrameDepthChanged( ChoiceFrameDepth choiceFrameDepth)
    { }


    public boolean isChoiceFrameDepthMandatory()
    {
        return true;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Hardware")
        ,@LocalizedTag(language="fr", name="label", value="Quincaillerie")
    })
    public final Hardware getHardware()
    {
         return this.hardware.getValue();
    }

    public final void setHardware(Hardware hardware)
    {

        if (hardware != null)
        {
            acquire(hardware, "hardware");
        }
        Hardware oldValue = this.hardware.getValue();
        this.hardware.setValue(hardware);

        Hardware currentValue = this.hardware.getValue();

        fireHardwareChange(currentValue, oldValue);
    }
    public final Hardware removeHardware()
    {
        Hardware oldValue = this.hardware.getValue();
        Hardware removedValue = this.hardware.removeValue();
        Hardware currentValue = this.hardware.getValue();

        fireHardwareChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultHardware(Hardware hardware)
    {
        if (hardware != null)
        {
            acquire(hardware, "hardware");
        }
        Hardware oldValue = this.hardware.getValue();
        this.hardware.setDefaultValue(hardware);

        Hardware currentValue = this.hardware.getValue();

        fireHardwareChange(currentValue, oldValue);
    }
    
    public final Hardware removeDefaultHardware()
    {
        Hardware oldValue = this.hardware.getValue();
        Hardware removedValue = this.hardware.removeDefaultValue();
        Hardware currentValue = this.hardware.getValue();

        fireHardwareChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Hardware> getHardwareValueContainer()
    {
        return this.hardware;
    }
    public final void fireHardwareChange(Hardware currentValue, Hardware oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeHardwareChanged(currentValue);
            firePropertyChange(PROPERTYNAME_HARDWARE, oldValue, currentValue);
            afterHardwareChanged(currentValue);
        }
    }

    public void beforeHardwareChanged( Hardware hardware)
    { }
    public void afterHardwareChanged( Hardware hardware)
    { }


    public boolean isHardwareMandatory()
    {
        return true;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Dimension with brick mould")
        ,@LocalizedTag(language="fr", name="label", value="Dimension avec moulure � brique")
        ,@LocalizedTag(language="en", name="desc", value="Dimension with brick mould")
        ,@LocalizedTag(language="fr", name="desc", value="Dimension avec moulure � brique")
    })
    public final Dimension getDimensionMab()
    {
         return this.dimensionMab.getValue();
    }

    public final void setDimensionMab(Dimension dimensionMab)
    {

        if (dimensionMab != null)
        {
            acquire(dimensionMab, "dimensionMab");
        }
        Dimension oldValue = this.dimensionMab.getValue();
        this.dimensionMab.setValue(dimensionMab);

        Dimension currentValue = this.dimensionMab.getValue();

        fireDimensionMabChange(currentValue, oldValue);
    }
    public final Dimension removeDimensionMab()
    {
        Dimension oldValue = this.dimensionMab.getValue();
        Dimension removedValue = this.dimensionMab.removeValue();
        Dimension currentValue = this.dimensionMab.getValue();

        fireDimensionMabChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultDimensionMab(Dimension dimensionMab)
    {
        if (dimensionMab != null)
        {
            acquire(dimensionMab, "dimensionMab");
        }
        Dimension oldValue = this.dimensionMab.getValue();
        this.dimensionMab.setDefaultValue(dimensionMab);

        Dimension currentValue = this.dimensionMab.getValue();

        fireDimensionMabChange(currentValue, oldValue);
    }
    
    public final Dimension removeDefaultDimensionMab()
    {
        Dimension oldValue = this.dimensionMab.getValue();
        Dimension removedValue = this.dimensionMab.removeDefaultValue();
        Dimension currentValue = this.dimensionMab.getValue();

        fireDimensionMabChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Dimension> getDimensionMabValueContainer()
    {
        return this.dimensionMab;
    }
    public final void fireDimensionMabChange(Dimension currentValue, Dimension oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeDimensionMabChanged(currentValue);
            firePropertyChange(PROPERTYNAME_DIMENSIONMAB, oldValue, currentValue);
            afterDimensionMabChanged(currentValue);
        }
    }

    public void beforeDimensionMabChanged( Dimension dimensionMab)
    { }
    public void afterDimensionMabChanged( Dimension dimensionMab)
    { }


    public boolean isDimensionMabMandatory()
    {
        return true;
    }


    @Override
    protected String getSequences()
    {
        return "choiceFrameDepth,awning,vertical,frameDepth,energyStar,installed,nbSections,qDQ,sameGrilles,showMeetingRail,showMullion,noFrame,windowsMullion,paintable,renderingNumber,isMabDimension,choicePresetWindowWidth,dimension,dimensionMab,ExteriorColor,InteriorColor,sectionInput,section,sash,meetingRail,division,hardware";  
    }
}
