// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class FixedVisionPvcBase extends com.netappsid.erp.configurator.Configurable
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(FixedVisionPvcBase.class);

    // attributes
    private ValueContainer<Jamb> jamb = new ValueContainer<Jamb>(this);

    // Bound properties

    public static final String PROPERTYNAME_JAMB = "jamb";  

    // Constructors

    public FixedVisionPvcBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.FixedVisionPvc"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Jamb")
        ,@LocalizedTag(language="fr", name="label", value="Jambage")
    })
    public final Jamb getJamb()
    {
         return this.jamb.getValue();
    }

    public final void setJamb(Jamb jamb)
    {

        if (jamb != null)
        {
            acquire(jamb, "jamb");
        }
        Jamb oldValue = this.jamb.getValue();
        this.jamb.setValue(jamb);

        Jamb currentValue = this.jamb.getValue();

        fireJambChange(currentValue, oldValue);
    }
    public final Jamb removeJamb()
    {
        Jamb oldValue = this.jamb.getValue();
        Jamb removedValue = this.jamb.removeValue();
        Jamb currentValue = this.jamb.getValue();

        fireJambChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultJamb(Jamb jamb)
    {
        if (jamb != null)
        {
            acquire(jamb, "jamb");
        }
        Jamb oldValue = this.jamb.getValue();
        this.jamb.setDefaultValue(jamb);

        Jamb currentValue = this.jamb.getValue();

        fireJambChange(currentValue, oldValue);
    }
    
    public final Jamb removeDefaultJamb()
    {
        Jamb oldValue = this.jamb.getValue();
        Jamb removedValue = this.jamb.removeDefaultValue();
        Jamb currentValue = this.jamb.getValue();

        fireJambChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Jamb> getJambValueContainer()
    {
        return this.jamb;
    }
    public final void fireJambChange(Jamb currentValue, Jamb oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeJambChanged(currentValue);
            firePropertyChange(PROPERTYNAME_JAMB, oldValue, currentValue);
            afterJambChanged(currentValue);
        }
    }

    public void beforeJambChanged( Jamb jamb)
    { }
    public void afterJambChanged( Jamb jamb)
    { }


    public boolean isJambMandatory()
    {
        return true;
    }


    @Override
    protected String getSequences()
    {
        return "vertical,energyStar,installed,nbSections,jamb,qDQ,sameGrilles,showMeetingRail,showMullion,frameDepth,noFrame,windowsMullion,paintable,renderingNumber,dimension,sectionInput,section,sash,meetingRail,division,ExteriorColor,InteriorColor,choicePresetWindowWidth,window,choiceFlip,profiles,option";  
    }
}
