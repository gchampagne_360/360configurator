// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.DynamicEnum;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

@DynamicEnum(fileName = "")
public abstract class ChoiceDeadBoltBase extends com.netappsid.erp.configurator.Configurable
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ChoiceDeadBoltBase.class);

    // attributes
    private ValueContainer<String> description = new ValueContainer<String>(this);
    private ValueContainer<Double> width = new ValueContainer<Double>(this);
    private ValueContainer<Double> height = new ValueContainer<Double>(this);
    private ValueContainer<Double> orix = new ValueContainer<Double>(this);
    private ValueContainer<Double> oriy = new ValueContainer<Double>(this);
    private ValueContainer<String> path = new ValueContainer<String>(this);
    private ValueContainer<String> trform = new ValueContainer<String>(this);
    private ValueContainer<Double> trheight = new ValueContainer<Double>(this);
    private ValueContainer<Double> trposx = new ValueContainer<Double>(this);
    private ValueContainer<Double> trposy = new ValueContainer<Double>(this);
    private ValueContainer<Double> trwidth = new ValueContainer<Double>(this);

    // Bound properties

    public static final String PROPERTYNAME_DESCRIPTION = "description";  
    public static final String PROPERTYNAME_WIDTH = "width";  
    public static final String PROPERTYNAME_HEIGHT = "height";  
    public static final String PROPERTYNAME_ORIX = "orix";  
    public static final String PROPERTYNAME_ORIY = "oriy";  
    public static final String PROPERTYNAME_PATH = "path";  
    public static final String PROPERTYNAME_TRFORM = "trform";  
    public static final String PROPERTYNAME_TRHEIGHT = "trheight";  
    public static final String PROPERTYNAME_TRPOSX = "trposx";  
    public static final String PROPERTYNAME_TRPOSY = "trposy";  
    public static final String PROPERTYNAME_TRWIDTH = "trwidth";  

    // Constructors

    public ChoiceDeadBoltBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.ChoiceDeadBolt"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
    })
    public final String getDescription()
    {
         return this.description.getValue();
    }

    public final void setDescription(String description)
    {

        String oldValue = this.description.getValue();
        this.description.setValue(description);

        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
    }
    public final String removeDescription()
    {
        String oldValue = this.description.getValue();
        String removedValue = this.description.removeValue();
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultDescription(String description)
    {
        String oldValue = this.description.getValue();
        this.description.setDefaultValue(description);

        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
    }
    
    public final String removeDefaultDescription()
    {
        String oldValue = this.description.getValue();
        String removedValue = this.description.removeDefaultValue();
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getDescriptionValueContainer()
    {
        return this.description;
    }
    public final void fireDescriptionChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeDescriptionChanged(currentValue);
            firePropertyChange(PROPERTYNAME_DESCRIPTION, oldValue, currentValue);
            afterDescriptionChanged(currentValue);
        }
    }

    public void beforeDescriptionChanged( String description)
    { }
    public void afterDescriptionChanged( String description)
    { }

    @LocalizedTags
    ({
    })
    public final Double getWidth()
    {
         return this.width.getValue();
    }

    public final void setWidth(Double width)
    {

        Double oldValue = this.width.getValue();
        this.width.setValue(width);

        Double currentValue = this.width.getValue();

        fireWidthChange(currentValue, oldValue);
    }
    public final Double removeWidth()
    {
        Double oldValue = this.width.getValue();
        Double removedValue = this.width.removeValue();
        Double currentValue = this.width.getValue();

        fireWidthChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultWidth(Double width)
    {
        Double oldValue = this.width.getValue();
        this.width.setDefaultValue(width);

        Double currentValue = this.width.getValue();

        fireWidthChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultWidth()
    {
        Double oldValue = this.width.getValue();
        Double removedValue = this.width.removeDefaultValue();
        Double currentValue = this.width.getValue();

        fireWidthChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getWidthValueContainer()
    {
        return this.width;
    }
    public final void fireWidthChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeWidthChanged(currentValue);
            firePropertyChange(PROPERTYNAME_WIDTH, oldValue, currentValue);
            afterWidthChanged(currentValue);
        }
    }

    public void beforeWidthChanged( Double width)
    { }
    public void afterWidthChanged( Double width)
    { }

    @LocalizedTags
    ({
    })
    public final Double getHeight()
    {
         return this.height.getValue();
    }

    public final void setHeight(Double height)
    {

        Double oldValue = this.height.getValue();
        this.height.setValue(height);

        Double currentValue = this.height.getValue();

        fireHeightChange(currentValue, oldValue);
    }
    public final Double removeHeight()
    {
        Double oldValue = this.height.getValue();
        Double removedValue = this.height.removeValue();
        Double currentValue = this.height.getValue();

        fireHeightChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultHeight(Double height)
    {
        Double oldValue = this.height.getValue();
        this.height.setDefaultValue(height);

        Double currentValue = this.height.getValue();

        fireHeightChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultHeight()
    {
        Double oldValue = this.height.getValue();
        Double removedValue = this.height.removeDefaultValue();
        Double currentValue = this.height.getValue();

        fireHeightChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getHeightValueContainer()
    {
        return this.height;
    }
    public final void fireHeightChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeHeightChanged(currentValue);
            firePropertyChange(PROPERTYNAME_HEIGHT, oldValue, currentValue);
            afterHeightChanged(currentValue);
        }
    }

    public void beforeHeightChanged( Double height)
    { }
    public void afterHeightChanged( Double height)
    { }

    @LocalizedTags
    ({
    })
    public final Double getOrix()
    {
         return this.orix.getValue();
    }

    public final void setOrix(Double orix)
    {

        Double oldValue = this.orix.getValue();
        this.orix.setValue(orix);

        Double currentValue = this.orix.getValue();

        fireOrixChange(currentValue, oldValue);
    }
    public final Double removeOrix()
    {
        Double oldValue = this.orix.getValue();
        Double removedValue = this.orix.removeValue();
        Double currentValue = this.orix.getValue();

        fireOrixChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultOrix(Double orix)
    {
        Double oldValue = this.orix.getValue();
        this.orix.setDefaultValue(orix);

        Double currentValue = this.orix.getValue();

        fireOrixChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultOrix()
    {
        Double oldValue = this.orix.getValue();
        Double removedValue = this.orix.removeDefaultValue();
        Double currentValue = this.orix.getValue();

        fireOrixChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getOrixValueContainer()
    {
        return this.orix;
    }
    public final void fireOrixChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeOrixChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ORIX, oldValue, currentValue);
            afterOrixChanged(currentValue);
        }
    }

    public void beforeOrixChanged( Double orix)
    { }
    public void afterOrixChanged( Double orix)
    { }

    @LocalizedTags
    ({
    })
    public final Double getOriy()
    {
         return this.oriy.getValue();
    }

    public final void setOriy(Double oriy)
    {

        Double oldValue = this.oriy.getValue();
        this.oriy.setValue(oriy);

        Double currentValue = this.oriy.getValue();

        fireOriyChange(currentValue, oldValue);
    }
    public final Double removeOriy()
    {
        Double oldValue = this.oriy.getValue();
        Double removedValue = this.oriy.removeValue();
        Double currentValue = this.oriy.getValue();

        fireOriyChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultOriy(Double oriy)
    {
        Double oldValue = this.oriy.getValue();
        this.oriy.setDefaultValue(oriy);

        Double currentValue = this.oriy.getValue();

        fireOriyChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultOriy()
    {
        Double oldValue = this.oriy.getValue();
        Double removedValue = this.oriy.removeDefaultValue();
        Double currentValue = this.oriy.getValue();

        fireOriyChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getOriyValueContainer()
    {
        return this.oriy;
    }
    public final void fireOriyChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeOriyChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ORIY, oldValue, currentValue);
            afterOriyChanged(currentValue);
        }
    }

    public void beforeOriyChanged( Double oriy)
    { }
    public void afterOriyChanged( Double oriy)
    { }

    @LocalizedTags
    ({
    })
    public final String getPath()
    {
         return this.path.getValue();
    }

    public final void setPath(String path)
    {

        String oldValue = this.path.getValue();
        this.path.setValue(path);

        String currentValue = this.path.getValue();

        firePathChange(currentValue, oldValue);
    }
    public final String removePath()
    {
        String oldValue = this.path.getValue();
        String removedValue = this.path.removeValue();
        String currentValue = this.path.getValue();

        firePathChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultPath(String path)
    {
        String oldValue = this.path.getValue();
        this.path.setDefaultValue(path);

        String currentValue = this.path.getValue();

        firePathChange(currentValue, oldValue);
    }
    
    public final String removeDefaultPath()
    {
        String oldValue = this.path.getValue();
        String removedValue = this.path.removeDefaultValue();
        String currentValue = this.path.getValue();

        firePathChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getPathValueContainer()
    {
        return this.path;
    }
    public final void firePathChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforePathChanged(currentValue);
            firePropertyChange(PROPERTYNAME_PATH, oldValue, currentValue);
            afterPathChanged(currentValue);
        }
    }

    public void beforePathChanged( String path)
    { }
    public void afterPathChanged( String path)
    { }

    @LocalizedTags
    ({
    })
    public final String getTrform()
    {
         return this.trform.getValue();
    }

    public final void setTrform(String trform)
    {

        String oldValue = this.trform.getValue();
        this.trform.setValue(trform);

        String currentValue = this.trform.getValue();

        fireTrformChange(currentValue, oldValue);
    }
    public final String removeTrform()
    {
        String oldValue = this.trform.getValue();
        String removedValue = this.trform.removeValue();
        String currentValue = this.trform.getValue();

        fireTrformChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultTrform(String trform)
    {
        String oldValue = this.trform.getValue();
        this.trform.setDefaultValue(trform);

        String currentValue = this.trform.getValue();

        fireTrformChange(currentValue, oldValue);
    }
    
    public final String removeDefaultTrform()
    {
        String oldValue = this.trform.getValue();
        String removedValue = this.trform.removeDefaultValue();
        String currentValue = this.trform.getValue();

        fireTrformChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getTrformValueContainer()
    {
        return this.trform;
    }
    public final void fireTrformChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeTrformChanged(currentValue);
            firePropertyChange(PROPERTYNAME_TRFORM, oldValue, currentValue);
            afterTrformChanged(currentValue);
        }
    }

    public void beforeTrformChanged( String trform)
    { }
    public void afterTrformChanged( String trform)
    { }

    @LocalizedTags
    ({
    })
    public final Double getTrheight()
    {
         return this.trheight.getValue();
    }

    public final void setTrheight(Double trheight)
    {

        Double oldValue = this.trheight.getValue();
        this.trheight.setValue(trheight);

        Double currentValue = this.trheight.getValue();

        fireTrheightChange(currentValue, oldValue);
    }
    public final Double removeTrheight()
    {
        Double oldValue = this.trheight.getValue();
        Double removedValue = this.trheight.removeValue();
        Double currentValue = this.trheight.getValue();

        fireTrheightChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultTrheight(Double trheight)
    {
        Double oldValue = this.trheight.getValue();
        this.trheight.setDefaultValue(trheight);

        Double currentValue = this.trheight.getValue();

        fireTrheightChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultTrheight()
    {
        Double oldValue = this.trheight.getValue();
        Double removedValue = this.trheight.removeDefaultValue();
        Double currentValue = this.trheight.getValue();

        fireTrheightChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getTrheightValueContainer()
    {
        return this.trheight;
    }
    public final void fireTrheightChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeTrheightChanged(currentValue);
            firePropertyChange(PROPERTYNAME_TRHEIGHT, oldValue, currentValue);
            afterTrheightChanged(currentValue);
        }
    }

    public void beforeTrheightChanged( Double trheight)
    { }
    public void afterTrheightChanged( Double trheight)
    { }

    @LocalizedTags
    ({
    })
    public final Double getTrposx()
    {
         return this.trposx.getValue();
    }

    public final void setTrposx(Double trposx)
    {

        Double oldValue = this.trposx.getValue();
        this.trposx.setValue(trposx);

        Double currentValue = this.trposx.getValue();

        fireTrposxChange(currentValue, oldValue);
    }
    public final Double removeTrposx()
    {
        Double oldValue = this.trposx.getValue();
        Double removedValue = this.trposx.removeValue();
        Double currentValue = this.trposx.getValue();

        fireTrposxChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultTrposx(Double trposx)
    {
        Double oldValue = this.trposx.getValue();
        this.trposx.setDefaultValue(trposx);

        Double currentValue = this.trposx.getValue();

        fireTrposxChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultTrposx()
    {
        Double oldValue = this.trposx.getValue();
        Double removedValue = this.trposx.removeDefaultValue();
        Double currentValue = this.trposx.getValue();

        fireTrposxChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getTrposxValueContainer()
    {
        return this.trposx;
    }
    public final void fireTrposxChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeTrposxChanged(currentValue);
            firePropertyChange(PROPERTYNAME_TRPOSX, oldValue, currentValue);
            afterTrposxChanged(currentValue);
        }
    }

    public void beforeTrposxChanged( Double trposx)
    { }
    public void afterTrposxChanged( Double trposx)
    { }

    @LocalizedTags
    ({
    })
    public final Double getTrposy()
    {
         return this.trposy.getValue();
    }

    public final void setTrposy(Double trposy)
    {

        Double oldValue = this.trposy.getValue();
        this.trposy.setValue(trposy);

        Double currentValue = this.trposy.getValue();

        fireTrposyChange(currentValue, oldValue);
    }
    public final Double removeTrposy()
    {
        Double oldValue = this.trposy.getValue();
        Double removedValue = this.trposy.removeValue();
        Double currentValue = this.trposy.getValue();

        fireTrposyChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultTrposy(Double trposy)
    {
        Double oldValue = this.trposy.getValue();
        this.trposy.setDefaultValue(trposy);

        Double currentValue = this.trposy.getValue();

        fireTrposyChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultTrposy()
    {
        Double oldValue = this.trposy.getValue();
        Double removedValue = this.trposy.removeDefaultValue();
        Double currentValue = this.trposy.getValue();

        fireTrposyChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getTrposyValueContainer()
    {
        return this.trposy;
    }
    public final void fireTrposyChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeTrposyChanged(currentValue);
            firePropertyChange(PROPERTYNAME_TRPOSY, oldValue, currentValue);
            afterTrposyChanged(currentValue);
        }
    }

    public void beforeTrposyChanged( Double trposy)
    { }
    public void afterTrposyChanged( Double trposy)
    { }

    @LocalizedTags
    ({
    })
    public final Double getTrwidth()
    {
         return this.trwidth.getValue();
    }

    public final void setTrwidth(Double trwidth)
    {

        Double oldValue = this.trwidth.getValue();
        this.trwidth.setValue(trwidth);

        Double currentValue = this.trwidth.getValue();

        fireTrwidthChange(currentValue, oldValue);
    }
    public final Double removeTrwidth()
    {
        Double oldValue = this.trwidth.getValue();
        Double removedValue = this.trwidth.removeValue();
        Double currentValue = this.trwidth.getValue();

        fireTrwidthChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultTrwidth(Double trwidth)
    {
        Double oldValue = this.trwidth.getValue();
        this.trwidth.setDefaultValue(trwidth);

        Double currentValue = this.trwidth.getValue();

        fireTrwidthChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultTrwidth()
    {
        Double oldValue = this.trwidth.getValue();
        Double removedValue = this.trwidth.removeDefaultValue();
        Double currentValue = this.trwidth.getValue();

        fireTrwidthChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getTrwidthValueContainer()
    {
        return this.trwidth;
    }
    public final void fireTrwidthChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeTrwidthChanged(currentValue);
            firePropertyChange(PROPERTYNAME_TRWIDTH, oldValue, currentValue);
            afterTrwidthChanged(currentValue);
        }
    }

    public void beforeTrwidthChanged( Double trwidth)
    { }
    public void afterTrwidthChanged( Double trwidth)
    { }


}
