// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.DynamicEnum;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

@DynamicEnum(fileName = "PANELS")
public abstract class ChoicePanelBase extends com.netappsid.erp.configurator.Configurable
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ChoicePanelBase.class);

    // attributes
    private ValueContainer<String> description = new ValueContainer<String>(this);
    private ValueContainer<String> parametricModelClass = new ValueContainer<String>(this);

    // Bound properties

    public static final String PROPERTYNAME_DESCRIPTION = "description";  
    public static final String PROPERTYNAME_PARAMETRICMODELCLASS = "parametricModelClass";  

    // Constructors

    public ChoicePanelBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.ChoicePanel"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="1")
        ,@LocalizedTag(language="fr", name="column", value="0")
        ,@LocalizedTag(language="en", name="label", value="Description")
        ,@LocalizedTag(language="fr", name="label", value="Description")
    })
    public final String getDescription()
    {
         return this.description.getValue();
    }

    public final void setDescription(String description)
    {

        String oldValue = this.description.getValue();
        this.description.setValue(description);

        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
    }
    public final String removeDescription()
    {
        String oldValue = this.description.getValue();
        String removedValue = this.description.removeValue();
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultDescription(String description)
    {
        String oldValue = this.description.getValue();
        this.description.setDefaultValue(description);

        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
    }
    
    public final String removeDefaultDescription()
    {
        String oldValue = this.description.getValue();
        String removedValue = this.description.removeDefaultValue();
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getDescriptionValueContainer()
    {
        return this.description;
    }
    public final void fireDescriptionChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeDescriptionChanged(currentValue);
            firePropertyChange(PROPERTYNAME_DESCRIPTION, oldValue, currentValue);
            afterDescriptionChanged(currentValue);
        }
    }

    public void beforeDescriptionChanged( String description)
    { }
    public void afterDescriptionChanged( String description)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="2")
        ,@LocalizedTag(language="fr", name="column", value="2")
        ,@LocalizedTag(language="en", name="label", value="Class")
        ,@LocalizedTag(language="fr", name="label", value="Class")
    })
    public final String getParametricModelClass()
    {
         return this.parametricModelClass.getValue();
    }

    public final void setParametricModelClass(String parametricModelClass)
    {

        String oldValue = this.parametricModelClass.getValue();
        this.parametricModelClass.setValue(parametricModelClass);

        String currentValue = this.parametricModelClass.getValue();

        fireParametricModelClassChange(currentValue, oldValue);
    }
    public final String removeParametricModelClass()
    {
        String oldValue = this.parametricModelClass.getValue();
        String removedValue = this.parametricModelClass.removeValue();
        String currentValue = this.parametricModelClass.getValue();

        fireParametricModelClassChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultParametricModelClass(String parametricModelClass)
    {
        String oldValue = this.parametricModelClass.getValue();
        this.parametricModelClass.setDefaultValue(parametricModelClass);

        String currentValue = this.parametricModelClass.getValue();

        fireParametricModelClassChange(currentValue, oldValue);
    }
    
    public final String removeDefaultParametricModelClass()
    {
        String oldValue = this.parametricModelClass.getValue();
        String removedValue = this.parametricModelClass.removeDefaultValue();
        String currentValue = this.parametricModelClass.getValue();

        fireParametricModelClassChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getParametricModelClassValueContainer()
    {
        return this.parametricModelClass;
    }
    public final void fireParametricModelClassChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeParametricModelClassChanged(currentValue);
            firePropertyChange(PROPERTYNAME_PARAMETRICMODELCLASS, oldValue, currentValue);
            afterParametricModelClassChanged(currentValue);
        }
    }

    public void beforeParametricModelClassChanged( String parametricModelClass)
    { }
    public void afterParametricModelClassChanged( String parametricModelClass)
    { }


    public boolean isParametricModelClassVisible()
    {
        return false;
    }

}
