// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V12.7.0
//2011-08-19

// Imports 
import com.netappsid.configuration.common.Configurable;
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.AssociatedComment;
import com.netappsid.erp.configurator.annotations.AssociatedDrawing;
import com.netappsid.erp.configurator.annotations.DynamicEnum;
import com.netappsid.erp.configurator.annotations.Filtrable;
import com.netappsid.erp.configurator.annotations.GlobalProperty;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;
import com.netappsid.erp.configurator.annotations.Mandatory;
import com.netappsid.erp.configurator.annotations.Printable;
import com.netappsid.erp.configurator.annotations.Visible;

@DynamicEnum(fileName = "dynamicEnums/PA_Dimension_STD.tsv", locale = "Fr", decimalSeparator = ".")
public abstract class ChoiceStandardDimensionsBase extends Configurable
{
	// Log4J logger
	protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ChoiceStandardDimensionsBase.class);

	// attributes
	// GENCODE 3703fd90-c29c-11e0-962b-0800200c9a66 Values changed to ValueContainerwith generic inner type
	private ValueContainer<String> frameType = new ValueContainer<String>(this);
	private ValueContainer<String> nbSlabDoor = new ValueContainer<String>(this);
	private ValueContainer<String> nbMullionMobile = new ValueContainer<String>(this);
	private ValueContainer<String> nbSideLight = new ValueContainer<String>(this);
	private ValueContainer<String> slabWidth = new ValueContainer<String>(this);
	private ValueContainer<String> totalWidth = new ValueContainer<String>(this);
	private ValueContainer<String> capping = new ValueContainer<String>(this);
	private ValueContainer<String> totalHeight = new ValueContainer<String>(this);
	private ValueContainer<String> salePrice = new ValueContainer<String>(this);
	private ValueContainer<String> description = new ValueContainer<String>(this);
	private ValueContainer<String> sideLightWidth = new ValueContainer<String>(this);
	private ValueContainer<String> valTotalWidth = new ValueContainer<String>(this);
	private ValueContainer<String> valTotalHeight = new ValueContainer<String>(this);
	private ValueContainer<ChoiceStandardDimensions> choiceStandardDimensions = new ValueContainer<ChoiceStandardDimensions>(this);

	// Constructors

	public ChoiceStandardDimensionsBase(com.netappsid.erp.configurator.Configurable parent) // GENCODE b298fff0-4bff-11e0-b8af-0800200c9a66
	{
		super(parent);
		if (this.getClass().getName().equals("com.client360.configuration.wad.ChoiceStandardDimensions"))
		{
			// activate listener before setting the default value to listen the changes
			activateListener();

			// Load the default values dynamically
			setDefaultValues();

			// load 'collection' preferences and values coming from prior items in the order
			applyPreferences();
		}

	}

	// Operations

	public final void setFrameType(String frameType)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.frameType.getValue();
		this.frameType.setValue(frameType);
		String currentValue = this.frameType.getValue();

		fireFrameTypeChange(currentValue, oldValue);
	}

	public final String removeFrameType()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.frameType.getValue();
		String removedValue = this.frameType.removeOverride();
		String currentValue = this.frameType.getValue();

		fireFrameTypeChange(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultFrameType(String frameType)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.frameType.getValue();
		this.frameType.setDefaultValue(frameType);
		String currentValue = this.frameType.getValue();

		fireFrameTypeChange(currentValue, oldValue);
	}

	public final String removeDefaultFrameType()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.frameType.getValue();
		String removedValue = this.frameType.removeDefaultValue();
		String currentValue = this.frameType.getValue();

		fireFrameTypeChange(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getFrameTypeValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.frameType;
	}

	public final void fireFrameTypeChange(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeFrameTypeChanged(currentValue);
			firePropertyChange(PROPERTYNAME_FRAMETYPE, oldValue, currentValue);
			afterFrameTypeChanged(currentValue);
		}
	}

	public void beforeFrameTypeChanged(String frameType)
	{}

	public void afterFrameTypeChanged(String frameType)
	{}

	public boolean isFrameTypeReadOnly()
	{
		return false;
	}

	public boolean isFrameTypeEnabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "0"), @LocalizedTag(language = "fr", name = "column", value = "0"),
			@LocalizedTag(language = "en", name = "label", value = "Frame Type"), @LocalizedTag(language = "fr", name = "label", value = "Type de cadre") })
	@Visible("true")
	@Filtrable(value = "true", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getFrameType()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.frameType.getValue();
	}

	public final void setNbSlabDoor(String nbSlabDoor)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.nbSlabDoor.getValue();
		this.nbSlabDoor.setValue(nbSlabDoor);
		String currentValue = this.nbSlabDoor.getValue();

		fireNbSlabDoorChange(currentValue, oldValue);
	}

	public final String removeNbSlabDoor()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.nbSlabDoor.getValue();
		String removedValue = this.nbSlabDoor.removeOverride();
		String currentValue = this.nbSlabDoor.getValue();

		fireNbSlabDoorChange(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultNbSlabDoor(String nbSlabDoor)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.nbSlabDoor.getValue();
		this.nbSlabDoor.setDefaultValue(nbSlabDoor);
		String currentValue = this.nbSlabDoor.getValue();

		fireNbSlabDoorChange(currentValue, oldValue);
	}

	public final String removeDefaultNbSlabDoor()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.nbSlabDoor.getValue();
		String removedValue = this.nbSlabDoor.removeDefaultValue();
		String currentValue = this.nbSlabDoor.getValue();

		fireNbSlabDoorChange(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getNbSlabDoorValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.nbSlabDoor;
	}

	public final void fireNbSlabDoorChange(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeNbSlabDoorChanged(currentValue);
			firePropertyChange(PROPERTYNAME_NBSLABDOOR, oldValue, currentValue);
			afterNbSlabDoorChanged(currentValue);
		}
	}

	public void beforeNbSlabDoorChanged(String nbSlabDoor)
	{}

	public void afterNbSlabDoorChanged(String nbSlabDoor)
	{}

	public boolean isNbSlabDoorReadOnly()
	{
		return false;
	}

	public boolean isNbSlabDoorEnabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "1"), @LocalizedTag(language = "fr", name = "column", value = "1"),
			@LocalizedTag(language = "en", name = "label", value = "Number of slab door"),
			@LocalizedTag(language = "fr", name = "label", value = "Nombre de porte") })
	@Visible("true")
	@Filtrable(value = "true", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getNbSlabDoor()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.nbSlabDoor.getValue();
	}

	public final void setNbMullionMobile(String nbMullionMobile)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.nbMullionMobile.getValue();
		this.nbMullionMobile.setValue(nbMullionMobile);
		String currentValue = this.nbMullionMobile.getValue();

		fireNbMullionMobileChange(currentValue, oldValue);
	}

	public final String removeNbMullionMobile()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.nbMullionMobile.getValue();
		String removedValue = this.nbMullionMobile.removeOverride();
		String currentValue = this.nbMullionMobile.getValue();

		fireNbMullionMobileChange(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultNbMullionMobile(String nbMullionMobile)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.nbMullionMobile.getValue();
		this.nbMullionMobile.setDefaultValue(nbMullionMobile);
		String currentValue = this.nbMullionMobile.getValue();

		fireNbMullionMobileChange(currentValue, oldValue);
	}

	public final String removeDefaultNbMullionMobile()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.nbMullionMobile.getValue();
		String removedValue = this.nbMullionMobile.removeDefaultValue();
		String currentValue = this.nbMullionMobile.getValue();

		fireNbMullionMobileChange(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getNbMullionMobileValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.nbMullionMobile;
	}

	public final void fireNbMullionMobileChange(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeNbMullionMobileChanged(currentValue);
			firePropertyChange(PROPERTYNAME_NBMULLIONMOBILE, oldValue, currentValue);
			afterNbMullionMobileChanged(currentValue);
		}
	}

	public void beforeNbMullionMobileChanged(String nbMullionMobile)
	{}

	public void afterNbMullionMobileChanged(String nbMullionMobile)
	{}

	public boolean isNbMullionMobileReadOnly()
	{
		return false;
	}

	public boolean isNbMullionMobileEnabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "4"), @LocalizedTag(language = "fr", name = "column", value = "4"),
			@LocalizedTag(language = "en", name = "label", value = "Number of Mullion Mobile"),
			@LocalizedTag(language = "fr", name = "label", value = "Nombre de mullion mobile") })
	@Visible("true")
	@Filtrable(value = "true", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getNbMullionMobile()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.nbMullionMobile.getValue();
	}

	public final void setNbSideLight(String nbSideLight)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.nbSideLight.getValue();
		this.nbSideLight.setValue(nbSideLight);
		String currentValue = this.nbSideLight.getValue();

		fireNbSideLightChange(currentValue, oldValue);
	}

	public final String removeNbSideLight()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.nbSideLight.getValue();
		String removedValue = this.nbSideLight.removeOverride();
		String currentValue = this.nbSideLight.getValue();

		fireNbSideLightChange(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultNbSideLight(String nbSideLight)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.nbSideLight.getValue();
		this.nbSideLight.setDefaultValue(nbSideLight);
		String currentValue = this.nbSideLight.getValue();

		fireNbSideLightChange(currentValue, oldValue);
	}

	public final String removeDefaultNbSideLight()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.nbSideLight.getValue();
		String removedValue = this.nbSideLight.removeDefaultValue();
		String currentValue = this.nbSideLight.getValue();

		fireNbSideLightChange(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getNbSideLightValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.nbSideLight;
	}

	public final void fireNbSideLightChange(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeNbSideLightChanged(currentValue);
			firePropertyChange(PROPERTYNAME_NBSIDELIGHT, oldValue, currentValue);
			afterNbSideLightChanged(currentValue);
		}
	}

	public void beforeNbSideLightChanged(String nbSideLight)
	{}

	public void afterNbSideLightChanged(String nbSideLight)
	{}

	public boolean isNbSideLightReadOnly()
	{
		return false;
	}

	public boolean isNbSideLightEnabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "2"), @LocalizedTag(language = "fr", name = "column", value = "2"),
			@LocalizedTag(language = "en", name = "label", value = "Number of Side Light"),
			@LocalizedTag(language = "fr", name = "label", value = "Number of Lat�raux") })
	@Visible("true")
	@Filtrable(value = "true", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getNbSideLight()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.nbSideLight.getValue();
	}

	public final void setSlabWidth(String slabWidth)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.slabWidth.getValue();
		this.slabWidth.setValue(slabWidth);
		String currentValue = this.slabWidth.getValue();

		fireSlabWidthChange(currentValue, oldValue);
	}

	public final String removeSlabWidth()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.slabWidth.getValue();
		String removedValue = this.slabWidth.removeOverride();
		String currentValue = this.slabWidth.getValue();

		fireSlabWidthChange(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultSlabWidth(String slabWidth)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.slabWidth.getValue();
		this.slabWidth.setDefaultValue(slabWidth);
		String currentValue = this.slabWidth.getValue();

		fireSlabWidthChange(currentValue, oldValue);
	}

	public final String removeDefaultSlabWidth()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.slabWidth.getValue();
		String removedValue = this.slabWidth.removeDefaultValue();
		String currentValue = this.slabWidth.getValue();

		fireSlabWidthChange(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getSlabWidthValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.slabWidth;
	}

	public final void fireSlabWidthChange(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeSlabWidthChanged(currentValue);
			firePropertyChange(PROPERTYNAME_SLABWIDTH, oldValue, currentValue);
			afterSlabWidthChanged(currentValue);
		}
	}

	public void beforeSlabWidthChanged(String slabWidth)
	{}

	public void afterSlabWidthChanged(String slabWidth)
	{}

	public boolean isSlabWidthReadOnly()
	{
		return false;
	}

	public boolean isSlabWidthEnabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "5"), @LocalizedTag(language = "fr", name = "column", value = "5"),
			@LocalizedTag(language = "en", name = "label", value = "One Slab Width"),
			@LocalizedTag(language = "fr", name = "label", value = "Largeur d\'une porte") })
	@Visible("true")
	@Filtrable(value = "true", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getSlabWidth()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.slabWidth.getValue();
	}

	public final void setTotalWidth(String totalWidth)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.totalWidth.getValue();
		this.totalWidth.setValue(totalWidth);
		String currentValue = this.totalWidth.getValue();

		fireTotalWidthChange(currentValue, oldValue);
	}

	public final String removeTotalWidth()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.totalWidth.getValue();
		String removedValue = this.totalWidth.removeOverride();
		String currentValue = this.totalWidth.getValue();

		fireTotalWidthChange(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultTotalWidth(String totalWidth)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.totalWidth.getValue();
		this.totalWidth.setDefaultValue(totalWidth);
		String currentValue = this.totalWidth.getValue();

		fireTotalWidthChange(currentValue, oldValue);
	}

	public final String removeDefaultTotalWidth()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.totalWidth.getValue();
		String removedValue = this.totalWidth.removeDefaultValue();
		String currentValue = this.totalWidth.getValue();

		fireTotalWidthChange(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getTotalWidthValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.totalWidth;
	}

	public final void fireTotalWidthChange(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeTotalWidthChanged(currentValue);
			firePropertyChange(PROPERTYNAME_TOTALWIDTH, oldValue, currentValue);
			afterTotalWidthChanged(currentValue);
		}
	}

	public void beforeTotalWidthChanged(String totalWidth)
	{}

	public void afterTotalWidthChanged(String totalWidth)
	{}

	public boolean isTotalWidthReadOnly()
	{
		return false;
	}

	public boolean isTotalWidthEnabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "6"), @LocalizedTag(language = "fr", name = "column", value = "6"),
			@LocalizedTag(language = "en", name = "label", value = "Total Width"), @LocalizedTag(language = "fr", name = "label", value = "Largeur total") })
	@Visible("true")
	@Filtrable(value = "false", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getTotalWidth()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.totalWidth.getValue();
	}

	public final void setCapping(String capping)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.capping.getValue();
		this.capping.setValue(capping);
		String currentValue = this.capping.getValue();

		fireCappingChange(currentValue, oldValue);
	}

	public final String removeCapping()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.capping.getValue();
		String removedValue = this.capping.removeOverride();
		String currentValue = this.capping.getValue();

		fireCappingChange(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultCapping(String capping)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.capping.getValue();
		this.capping.setDefaultValue(capping);
		String currentValue = this.capping.getValue();

		fireCappingChange(currentValue, oldValue);
	}

	public final String removeDefaultCapping()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.capping.getValue();
		String removedValue = this.capping.removeDefaultValue();
		String currentValue = this.capping.getValue();

		fireCappingChange(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getCappingValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.capping;
	}

	public final void fireCappingChange(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeCappingChanged(currentValue);
			firePropertyChange(PROPERTYNAME_CAPPING, oldValue, currentValue);
			afterCappingChanged(currentValue);
		}
	}

	public void beforeCappingChanged(String capping)
	{}

	public void afterCappingChanged(String capping)
	{}

	public boolean isCappingReadOnly()
	{
		return false;
	}

	public boolean isCappingEnabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "8"), @LocalizedTag(language = "fr", name = "column", value = "9"),
			@LocalizedTag(language = "en", name = "label", value = "Capping"), @LocalizedTag(language = "fr", name = "label", value = "Recouvrement") })
	@Visible("false")
	@Filtrable(value = "false", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getCapping()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.capping.getValue();
	}

	public final void setTotalHeight(String totalHeight)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.totalHeight.getValue();
		this.totalHeight.setValue(totalHeight);
		String currentValue = this.totalHeight.getValue();

		fireTotalHeightChange(currentValue, oldValue);
	}

	public final String removeTotalHeight()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.totalHeight.getValue();
		String removedValue = this.totalHeight.removeOverride();
		String currentValue = this.totalHeight.getValue();

		fireTotalHeightChange(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultTotalHeight(String totalHeight)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.totalHeight.getValue();
		this.totalHeight.setDefaultValue(totalHeight);
		String currentValue = this.totalHeight.getValue();

		fireTotalHeightChange(currentValue, oldValue);
	}

	public final String removeDefaultTotalHeight()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.totalHeight.getValue();
		String removedValue = this.totalHeight.removeDefaultValue();
		String currentValue = this.totalHeight.getValue();

		fireTotalHeightChange(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getTotalHeightValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.totalHeight;
	}

	public final void fireTotalHeightChange(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeTotalHeightChanged(currentValue);
			firePropertyChange(PROPERTYNAME_TOTALHEIGHT, oldValue, currentValue);
			afterTotalHeightChanged(currentValue);
		}
	}

	public void beforeTotalHeightChanged(String totalHeight)
	{}

	public void afterTotalHeightChanged(String totalHeight)
	{}

	public boolean isTotalHeightReadOnly()
	{
		return false;
	}

	public boolean isTotalHeightEnabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "7"), @LocalizedTag(language = "fr", name = "column", value = "7"),
			@LocalizedTag(language = "en", name = "label", value = "Total Height"), @LocalizedTag(language = "fr", name = "label", value = "Hauteur total") })
	@Visible("true")
	@Filtrable(value = "false", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getTotalHeight()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.totalHeight.getValue();
	}

	public final void setSalePrice(String salePrice)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.salePrice.getValue();
		this.salePrice.setValue(salePrice);
		String currentValue = this.salePrice.getValue();

		fireSalePriceChange(currentValue, oldValue);
	}

	public final String removeSalePrice()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.salePrice.getValue();
		String removedValue = this.salePrice.removeOverride();
		String currentValue = this.salePrice.getValue();

		fireSalePriceChange(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultSalePrice(String salePrice)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.salePrice.getValue();
		this.salePrice.setDefaultValue(salePrice);
		String currentValue = this.salePrice.getValue();

		fireSalePriceChange(currentValue, oldValue);
	}

	public final String removeDefaultSalePrice()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.salePrice.getValue();
		String removedValue = this.salePrice.removeDefaultValue();
		String currentValue = this.salePrice.getValue();

		fireSalePriceChange(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getSalePriceValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.salePrice;
	}

	public final void fireSalePriceChange(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeSalePriceChanged(currentValue);
			firePropertyChange(PROPERTYNAME_SALEPRICE, oldValue, currentValue);
			afterSalePriceChanged(currentValue);
		}
	}

	public void beforeSalePriceChanged(String salePrice)
	{}

	public void afterSalePriceChanged(String salePrice)
	{}

	public boolean isSalePriceReadOnly()
	{
		return false;
	}

	public boolean isSalePriceEnabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "12"), @LocalizedTag(language = "fr", name = "column", value = "12"),
			@LocalizedTag(language = "en", name = "label", value = "Sale Price"), @LocalizedTag(language = "fr", name = "label", value = "Prix de vente") })
	@Visible("false")
	@Filtrable(value = "false", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getSalePrice()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.salePrice.getValue();
	}

	public final void setDescription(String description)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.description.getValue();
		this.description.setValue(description);
		String currentValue = this.description.getValue();

		fireDescriptionChange(currentValue, oldValue);
	}

	public final String removeDescription()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.description.getValue();
		String removedValue = this.description.removeOverride();
		String currentValue = this.description.getValue();

		fireDescriptionChange(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultDescription(String description)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.description.getValue();
		this.description.setDefaultValue(description);
		String currentValue = this.description.getValue();

		fireDescriptionChange(currentValue, oldValue);
	}

	public final String removeDefaultDescription()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.description.getValue();
		String removedValue = this.description.removeDefaultValue();
		String currentValue = this.description.getValue();

		fireDescriptionChange(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getDescriptionValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.description;
	}

	public final void fireDescriptionChange(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeDescriptionChanged(currentValue);
			firePropertyChange(PROPERTYNAME_DESCRIPTION, oldValue, currentValue);
			afterDescriptionChanged(currentValue);
		}
	}

	public void beforeDescriptionChanged(String description)
	{}

	public void afterDescriptionChanged(String description)
	{}

	public boolean isDescriptionReadOnly()
	{
		return false;
	}

	public boolean isDescriptionEnabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "11"), @LocalizedTag(language = "fr", name = "column", value = "10"),
			@LocalizedTag(language = "en", name = "label", value = "Description"), @LocalizedTag(language = "fr", name = "label", value = "Description") })
	@Visible("false")
	@Filtrable(value = "false", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getDescription()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.description.getValue();
	}

	public final void setSideLightWidth(String sideLightWidth)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.sideLightWidth.getValue();
		this.sideLightWidth.setValue(sideLightWidth);
		String currentValue = this.sideLightWidth.getValue();

		fireSideLightWidthChange(currentValue, oldValue);
	}

	public final String removeSideLightWidth()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.sideLightWidth.getValue();
		String removedValue = this.sideLightWidth.removeOverride();
		String currentValue = this.sideLightWidth.getValue();

		fireSideLightWidthChange(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultSideLightWidth(String sideLightWidth)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.sideLightWidth.getValue();
		this.sideLightWidth.setDefaultValue(sideLightWidth);
		String currentValue = this.sideLightWidth.getValue();

		fireSideLightWidthChange(currentValue, oldValue);
	}

	public final String removeDefaultSideLightWidth()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.sideLightWidth.getValue();
		String removedValue = this.sideLightWidth.removeDefaultValue();
		String currentValue = this.sideLightWidth.getValue();

		fireSideLightWidthChange(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getSideLightWidthValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.sideLightWidth;
	}

	public final void fireSideLightWidthChange(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeSideLightWidthChanged(currentValue);
			firePropertyChange(PROPERTYNAME_SIDELIGHTWIDTH, oldValue, currentValue);
			afterSideLightWidthChanged(currentValue);
		}
	}

	public void beforeSideLightWidthChanged(String sideLightWidth)
	{}

	public void afterSideLightWidthChanged(String sideLightWidth)
	{}

	public boolean isSideLightWidthReadOnly()
	{
		return false;
	}

	public boolean isSideLightWidthEnabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "3"), @LocalizedTag(language = "fr", name = "column", value = "3"),
			@LocalizedTag(language = "en", name = "label", value = "Side Light Width"),
			@LocalizedTag(language = "fr", name = "label", value = "Largeur d\'un lat�rau") })
	@Visible("true")
	@Filtrable(value = "true", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getSideLightWidth()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.sideLightWidth.getValue();
	}

	public final void setValTotalWidth(String valTotalWidth)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.valTotalWidth.getValue();
		this.valTotalWidth.setValue(valTotalWidth);
		String currentValue = this.valTotalWidth.getValue();

		fireValTotalWidthChange(currentValue, oldValue);
	}

	public final String removeValTotalWidth()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.valTotalWidth.getValue();
		String removedValue = this.valTotalWidth.removeOverride();
		String currentValue = this.valTotalWidth.getValue();

		fireValTotalWidthChange(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultValTotalWidth(String valTotalWidth)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.valTotalWidth.getValue();
		this.valTotalWidth.setDefaultValue(valTotalWidth);
		String currentValue = this.valTotalWidth.getValue();

		fireValTotalWidthChange(currentValue, oldValue);
	}

	public final String removeDefaultValTotalWidth()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.valTotalWidth.getValue();
		String removedValue = this.valTotalWidth.removeDefaultValue();
		String currentValue = this.valTotalWidth.getValue();

		fireValTotalWidthChange(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getValTotalWidthValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.valTotalWidth;
	}

	public final void fireValTotalWidthChange(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeValTotalWidthChanged(currentValue);
			firePropertyChange(PROPERTYNAME_VALTOTALWIDTH, oldValue, currentValue);
			afterValTotalWidthChanged(currentValue);
		}
	}

	public void beforeValTotalWidthChanged(String valTotalWidth)
	{}

	public void afterValTotalWidthChanged(String valTotalWidth)
	{}

	public boolean isValTotalWidthReadOnly()
	{
		return false;
	}

	public boolean isValTotalWidthEnabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "13"), @LocalizedTag(language = "fr", name = "column", value = "13"),
			@LocalizedTag(language = "en", name = "label", value = "Value of the Total Width"),
			@LocalizedTag(language = "fr", name = "label", value = "Valeur numerique de largeur total") })
	@Visible("false")
	@Filtrable(value = "false", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getValTotalWidth()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.valTotalWidth.getValue();
	}

	public final void setValTotalHeight(String valTotalHeight)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.valTotalHeight.getValue();
		this.valTotalHeight.setValue(valTotalHeight);
		String currentValue = this.valTotalHeight.getValue();

		fireValTotalHeightChange(currentValue, oldValue);
	}

	public final String removeValTotalHeight()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.valTotalHeight.getValue();
		String removedValue = this.valTotalHeight.removeOverride();
		String currentValue = this.valTotalHeight.getValue();

		fireValTotalHeightChange(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultValTotalHeight(String valTotalHeight)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.valTotalHeight.getValue();
		this.valTotalHeight.setDefaultValue(valTotalHeight);
		String currentValue = this.valTotalHeight.getValue();

		fireValTotalHeightChange(currentValue, oldValue);
	}

	public final String removeDefaultValTotalHeight()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.valTotalHeight.getValue();
		String removedValue = this.valTotalHeight.removeDefaultValue();
		String currentValue = this.valTotalHeight.getValue();

		fireValTotalHeightChange(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getValTotalHeightValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.valTotalHeight;
	}

	public final void fireValTotalHeightChange(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeValTotalHeightChanged(currentValue);
			firePropertyChange(PROPERTYNAME_VALTOTALHEIGHT, oldValue, currentValue);
			afterValTotalHeightChanged(currentValue);
		}
	}

	public void beforeValTotalHeightChanged(String valTotalHeight)
	{}

	public void afterValTotalHeightChanged(String valTotalHeight)
	{}

	public boolean isValTotalHeightReadOnly()
	{
		return false;
	}

	public boolean isValTotalHeightEnabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "14"), @LocalizedTag(language = "fr", name = "column", value = "14"),
			@LocalizedTag(language = "en", name = "label", value = "Value of the Total Height"),
			@LocalizedTag(language = "fr", name = "label", value = "Valuer de la hauteur total") })
	@Visible("false")
	@Filtrable(value = "false", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getValTotalHeight()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.valTotalHeight.getValue();
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "label", value = "Dimension Standard"),
			@LocalizedTag(language = "fr", name = "label", value = "Dimension Standard") })
	@Mandatory("true")
	@GlobalProperty("false")
	@Printable("true")
	@Visible("true")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final ChoiceStandardDimensions getChoiceStandardDimensions()
	{ // GENCODE e76ad2a0-c2bd-11e0-962b-0800200c9a66 Value Container getter operations
		return choiceStandardDimensions.getValue();
	}

	public final void setChoiceStandardDimensions(ChoiceStandardDimensions choiceStandardDimensions)
	{ // GENCODE 9cda9e30-c2bf-11e0-962b-0800200c9a66 SETTER

		ChoiceStandardDimensions oldValue = this.choiceStandardDimensions.getValue();
		this.choiceStandardDimensions.setValue(choiceStandardDimensions);

		ChoiceStandardDimensions currentValue = this.choiceStandardDimensions.getValue();

		fireChoiceStandardDimensionsChange(currentValue, oldValue);
	}

	public final ChoiceStandardDimensions removeChoiceStandardDimensions()
	{ // GENCODE 07d769f0-c376-11e0-962b-0800200c9a66 DEFAULT REMOVE

		ChoiceStandardDimensions oldValue = this.choiceStandardDimensions.getValue();
		ChoiceStandardDimensions removedValue = this.choiceStandardDimensions.removeOverride();
		ChoiceStandardDimensions currentValue = this.choiceStandardDimensions.getValue();

		if (removedValue != oldValue)
			dispose(removedValue);

		fireChoiceStandardDimensionsChange(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultChoiceStandardDimensions(ChoiceStandardDimensions choiceStandardDimensions)
	{ // GENCODE 07d769f1-c376-11e0-962b-0800200c9a66 DEFAULT SETTER
		ChoiceStandardDimensions oldValue = this.choiceStandardDimensions.getValue();
		this.choiceStandardDimensions.setDefaultValue(choiceStandardDimensions);
		ChoiceStandardDimensions currentValue = this.choiceStandardDimensions.getValue();

		fireChoiceStandardDimensionsChange(currentValue, oldValue);
	}

	public final ChoiceStandardDimensions removeDefaultChoiceStandardDimensions()
	{ // GENCODE 07d769f2-c376-11e0-962b-0800200c9a66 DEFAULT REMOVE

		ChoiceStandardDimensions oldValue = this.choiceStandardDimensions.getValue();
		ChoiceStandardDimensions removedValue = this.choiceStandardDimensions.removeDefaultValue();
		ChoiceStandardDimensions currentValue = this.choiceStandardDimensions.getValue();

		if (removedValue != oldValue)
			dispose(removedValue);

		fireChoiceStandardDimensionsChange(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<ChoiceStandardDimensions> getChoiceStandardDimensionsValueContainer()
	{ // GENCODE 5200e500-c2be-11e0-962b-0800200c9a66 ValueContainer GETTER
		return choiceStandardDimensions;
	}

	public final void fireChoiceStandardDimensionsChange(ChoiceStandardDimensions currentValue, ChoiceStandardDimensions oldValue)
	{ // GENCODE 209a8340-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeChoiceStandardDimensionsChanged(currentValue);
			firePropertyChange(PROPERTYNAME_CHOICESTANDARDDIMENSIONS, oldValue, currentValue);
			afterChoiceStandardDimensionsChanged(currentValue);
		}
	}

	public void pushChoiceStandardDimensions()
	{}

	public void beforeChoiceStandardDimensionsChanged(ChoiceStandardDimensions choiceStandardDimensions)
	{}

	public void afterChoiceStandardDimensionsChanged(ChoiceStandardDimensions choiceStandardDimensions)
	{}

	public boolean isChoiceStandardDimensionsVisible()
	{
		return true;
	}

	public boolean isChoiceStandardDimensionsReadOnly()
	{
		return false;
	}

	public boolean isChoiceStandardDimensionsEnabled(ChoiceStandardDimensions choice)
	{
		return true;
	}

	// Business methods

	public boolean isFrameTypeVisible()
	{
		return true;
	}

	public boolean isNbSlabDoorVisible()
	{
		return true;
	}

	public boolean isNbMullionMobileVisible()
	{
		return true;
	}

	public boolean isNbSideLightVisible()
	{
		return true;
	}

	public boolean isSlabWidthVisible()
	{
		return true;
	}

	public boolean isTotalWidthVisible()
	{
		return true;
	}

	public boolean isCappingVisible()
	{
		return false;
	}

	public boolean isTotalHeightVisible()
	{
		return true;
	}

	public boolean isSalePriceVisible()
	{
		return false;
	}

	public boolean isDescriptionVisible()
	{
		return false;
	}

	public boolean isSideLightWidthVisible()
	{
		return true;
	}

	public boolean isValTotalWidthVisible()
	{
		return false;
	}

	public boolean isValTotalHeightVisible()
	{
		return false;
	}

	public boolean isChoiceStandardDimensionsMandatory()
	{
		return true;
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
	}

	// Bound properties

	public static final String PROPERTYNAME_FRAMETYPE = "frameType";
	public static final String PROPERTYNAME_NBSLABDOOR = "nbSlabDoor";
	public static final String PROPERTYNAME_NBMULLIONMOBILE = "nbMullionMobile";
	public static final String PROPERTYNAME_NBSIDELIGHT = "nbSideLight";
	public static final String PROPERTYNAME_SLABWIDTH = "slabWidth";
	public static final String PROPERTYNAME_TOTALWIDTH = "totalWidth";
	public static final String PROPERTYNAME_CAPPING = "capping";
	public static final String PROPERTYNAME_TOTALHEIGHT = "totalHeight";
	public static final String PROPERTYNAME_SALEPRICE = "salePrice";
	public static final String PROPERTYNAME_DESCRIPTION = "description";
	public static final String PROPERTYNAME_SIDELIGHTWIDTH = "sideLightWidth";
	public static final String PROPERTYNAME_VALTOTALWIDTH = "valTotalWidth";
	public static final String PROPERTYNAME_VALTOTALHEIGHT = "valTotalHeight";
	public static final String PROPERTYNAME_CHOICESTANDARDDIMENSIONS = "choiceStandardDimensions";
}
