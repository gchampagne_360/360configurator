// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.wad.enums.Soubassement;
import com.client360.configuration.wad.enums.TypeDePanneaux;
import com.client360.configuration.wad.enums.TypeDeTraverseIntermediaire;
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class QuestionnaireSousBassementBase extends com.netappsid.wadconfigurator.AdvancedSectionGrilles
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(QuestionnaireSousBassementBase.class);

    // attributes
    private ValueContainer<Measure<Length>> hauSbs = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Integer> nbPanneaux = new ValueContainer<Integer>(this);
    private ValueContainer<Measure<Length>> larTraItr = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Soubassement> soubassement = new ValueContainer<Soubassement>(this);
    private ValueContainer<TypeDeTraverseIntermediaire> typeDeTraverseIntermediaire = new ValueContainer<TypeDeTraverseIntermediaire>(this);
    private ValueContainer<TypeDePanneaux> typeDePanneaux = new ValueContainer<TypeDePanneaux>(this);

    // Bound properties

    public static final String PROPERTYNAME_HAUSBS = "hauSbs";  
    public static final String PROPERTYNAME_NBPANNEAUX = "nbPanneaux";  
    public static final String PROPERTYNAME_LARTRAITR = "larTraItr";  
    public static final String PROPERTYNAME_SOUBASSEMENT = "soubassement";  
    public static final String PROPERTYNAME_TYPEDETRAVERSEINTERMEDIAIRE = "typeDeTraverseIntermediaire";  
    public static final String PROPERTYNAME_TYPEDEPANNEAUX = "typeDePanneaux";  

    // Constructors

    public QuestionnaireSousBassementBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.QuestionnaireSousBassement"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Kick panel height")
        ,@LocalizedTag(language="fr", name="label", value="Hauteur sous-bassement")
    })
    public final Measure<Length> getHauSbs()
    {
         return this.hauSbs.getValue();
    }

    public final void setHauSbs(Measure<Length> hauSbs)
    {

        hauSbs = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(hauSbs, getDefaultUnitFor(PROPERTYNAME_HAUSBS,Length.class));

        Measure<Length> oldValue = this.hauSbs.getValue();
        this.hauSbs.setValue(hauSbs);

        Measure<Length> currentValue = this.hauSbs.getValue();

        fireHauSbsChange(currentValue, oldValue);
    }
    public final Measure<Length> removeHauSbs()
    {
        Measure<Length> oldValue = this.hauSbs.getValue();
        Measure<Length> removedValue = this.hauSbs.removeValue();
        Measure<Length> currentValue = this.hauSbs.getValue();

        fireHauSbsChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultHauSbs(Measure<Length> hauSbs)
    {
        hauSbs = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(hauSbs, getDefaultUnitFor(PROPERTYNAME_HAUSBS,Length.class));

        Measure<Length> oldValue = this.hauSbs.getValue();
        this.hauSbs.setDefaultValue(hauSbs);

        Measure<Length> currentValue = this.hauSbs.getValue();

        fireHauSbsChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultHauSbs()
    {
        Measure<Length> oldValue = this.hauSbs.getValue();
        Measure<Length> removedValue = this.hauSbs.removeDefaultValue();
        Measure<Length> currentValue = this.hauSbs.getValue();

        fireHauSbsChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getHauSbsValueContainer()
    {
        return this.hauSbs;
    }
    public final void fireHauSbsChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeHauSbsChanged(currentValue);
            firePropertyChange(PROPERTYNAME_HAUSBS, oldValue, currentValue);
            afterHauSbsChanged(currentValue);
        }
    }

    public void beforeHauSbsChanged( Measure<Length> hauSbs)
    { }
    public void afterHauSbsChanged( Measure<Length> hauSbs)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Kick panels qty")
        ,@LocalizedTag(language="fr", name="label", value="Nb de panneaux")
    })
    public final Integer getNbPanneaux()
    {
         return this.nbPanneaux.getValue();
    }

    public final void setNbPanneaux(Integer nbPanneaux)
    {

        Integer oldValue = this.nbPanneaux.getValue();
        this.nbPanneaux.setValue(nbPanneaux);

        Integer currentValue = this.nbPanneaux.getValue();

        fireNbPanneauxChange(currentValue, oldValue);
    }
    public final Integer removeNbPanneaux()
    {
        Integer oldValue = this.nbPanneaux.getValue();
        Integer removedValue = this.nbPanneaux.removeValue();
        Integer currentValue = this.nbPanneaux.getValue();

        fireNbPanneauxChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultNbPanneaux(Integer nbPanneaux)
    {
        Integer oldValue = this.nbPanneaux.getValue();
        this.nbPanneaux.setDefaultValue(nbPanneaux);

        Integer currentValue = this.nbPanneaux.getValue();

        fireNbPanneauxChange(currentValue, oldValue);
    }
    
    public final Integer removeDefaultNbPanneaux()
    {
        Integer oldValue = this.nbPanneaux.getValue();
        Integer removedValue = this.nbPanneaux.removeDefaultValue();
        Integer currentValue = this.nbPanneaux.getValue();

        fireNbPanneauxChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Integer> getNbPanneauxValueContainer()
    {
        return this.nbPanneaux;
    }
    public final void fireNbPanneauxChange(Integer currentValue, Integer oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeNbPanneauxChanged(currentValue);
            firePropertyChange(PROPERTYNAME_NBPANNEAUX, oldValue, currentValue);
            afterNbPanneauxChanged(currentValue);
        }
    }

    public void beforeNbPanneauxChanged( Integer nbPanneaux)
    { }
    public void afterNbPanneauxChanged( Integer nbPanneaux)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="fr", name="label", value="Largeurr traverse intermédiaire")
    })
    public final Measure<Length> getLarTraItr()
    {
         return this.larTraItr.getValue();
    }

    public final void setLarTraItr(Measure<Length> larTraItr)
    {

        larTraItr = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(larTraItr, getDefaultUnitFor(PROPERTYNAME_LARTRAITR,Length.class));

        Measure<Length> oldValue = this.larTraItr.getValue();
        this.larTraItr.setValue(larTraItr);

        Measure<Length> currentValue = this.larTraItr.getValue();

        fireLarTraItrChange(currentValue, oldValue);
    }
    public final Measure<Length> removeLarTraItr()
    {
        Measure<Length> oldValue = this.larTraItr.getValue();
        Measure<Length> removedValue = this.larTraItr.removeValue();
        Measure<Length> currentValue = this.larTraItr.getValue();

        fireLarTraItrChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultLarTraItr(Measure<Length> larTraItr)
    {
        larTraItr = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(larTraItr, getDefaultUnitFor(PROPERTYNAME_LARTRAITR,Length.class));

        Measure<Length> oldValue = this.larTraItr.getValue();
        this.larTraItr.setDefaultValue(larTraItr);

        Measure<Length> currentValue = this.larTraItr.getValue();

        fireLarTraItrChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultLarTraItr()
    {
        Measure<Length> oldValue = this.larTraItr.getValue();
        Measure<Length> removedValue = this.larTraItr.removeDefaultValue();
        Measure<Length> currentValue = this.larTraItr.getValue();

        fireLarTraItrChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getLarTraItrValueContainer()
    {
        return this.larTraItr;
    }
    public final void fireLarTraItrChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeLarTraItrChanged(currentValue);
            firePropertyChange(PROPERTYNAME_LARTRAITR, oldValue, currentValue);
            afterLarTraItrChanged(currentValue);
        }
    }

    public void beforeLarTraItrChanged( Measure<Length> larTraItr)
    { }
    public void afterLarTraItrChanged( Measure<Length> larTraItr)
    { }

    @LocalizedTags
    ({
    })
    public final Soubassement getSoubassement()
    {
         return this.soubassement.getValue();
    }

    public final void setSoubassement(Soubassement soubassement)
    {

        Soubassement oldValue = this.soubassement.getValue();
        this.soubassement.setValue(soubassement);

        Soubassement currentValue = this.soubassement.getValue();

        fireSoubassementChange(currentValue, oldValue);
    }
    public final Soubassement removeSoubassement()
    {
        Soubassement oldValue = this.soubassement.getValue();
        Soubassement removedValue = this.soubassement.removeValue();
        Soubassement currentValue = this.soubassement.getValue();

        fireSoubassementChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultSoubassement(Soubassement soubassement)
    {
        Soubassement oldValue = this.soubassement.getValue();
        this.soubassement.setDefaultValue(soubassement);

        Soubassement currentValue = this.soubassement.getValue();

        fireSoubassementChange(currentValue, oldValue);
    }
    
    public final Soubassement removeDefaultSoubassement()
    {
        Soubassement oldValue = this.soubassement.getValue();
        Soubassement removedValue = this.soubassement.removeDefaultValue();
        Soubassement currentValue = this.soubassement.getValue();

        fireSoubassementChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Soubassement> getSoubassementValueContainer()
    {
        return this.soubassement;
    }
    public final void fireSoubassementChange(Soubassement currentValue, Soubassement oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeSoubassementChanged(currentValue);
            firePropertyChange(PROPERTYNAME_SOUBASSEMENT, oldValue, currentValue);
            afterSoubassementChanged(currentValue);
        }
    }

    public void beforeSoubassementChanged( Soubassement soubassement)
    { }
    public void afterSoubassementChanged( Soubassement soubassement)
    { }


    public boolean isSoubassementMandatory()
    {
        return true;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="fr", name="label", value="Type de traverse intermédiaire")
    })
    public final TypeDeTraverseIntermediaire getTypeDeTraverseIntermediaire()
    {
         return this.typeDeTraverseIntermediaire.getValue();
    }

    public final void setTypeDeTraverseIntermediaire(TypeDeTraverseIntermediaire typeDeTraverseIntermediaire)
    {

        TypeDeTraverseIntermediaire oldValue = this.typeDeTraverseIntermediaire.getValue();
        this.typeDeTraverseIntermediaire.setValue(typeDeTraverseIntermediaire);

        TypeDeTraverseIntermediaire currentValue = this.typeDeTraverseIntermediaire.getValue();

        fireTypeDeTraverseIntermediaireChange(currentValue, oldValue);
    }
    public final TypeDeTraverseIntermediaire removeTypeDeTraverseIntermediaire()
    {
        TypeDeTraverseIntermediaire oldValue = this.typeDeTraverseIntermediaire.getValue();
        TypeDeTraverseIntermediaire removedValue = this.typeDeTraverseIntermediaire.removeValue();
        TypeDeTraverseIntermediaire currentValue = this.typeDeTraverseIntermediaire.getValue();

        fireTypeDeTraverseIntermediaireChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultTypeDeTraverseIntermediaire(TypeDeTraverseIntermediaire typeDeTraverseIntermediaire)
    {
        TypeDeTraverseIntermediaire oldValue = this.typeDeTraverseIntermediaire.getValue();
        this.typeDeTraverseIntermediaire.setDefaultValue(typeDeTraverseIntermediaire);

        TypeDeTraverseIntermediaire currentValue = this.typeDeTraverseIntermediaire.getValue();

        fireTypeDeTraverseIntermediaireChange(currentValue, oldValue);
    }
    
    public final TypeDeTraverseIntermediaire removeDefaultTypeDeTraverseIntermediaire()
    {
        TypeDeTraverseIntermediaire oldValue = this.typeDeTraverseIntermediaire.getValue();
        TypeDeTraverseIntermediaire removedValue = this.typeDeTraverseIntermediaire.removeDefaultValue();
        TypeDeTraverseIntermediaire currentValue = this.typeDeTraverseIntermediaire.getValue();

        fireTypeDeTraverseIntermediaireChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<TypeDeTraverseIntermediaire> getTypeDeTraverseIntermediaireValueContainer()
    {
        return this.typeDeTraverseIntermediaire;
    }
    public final void fireTypeDeTraverseIntermediaireChange(TypeDeTraverseIntermediaire currentValue, TypeDeTraverseIntermediaire oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeTypeDeTraverseIntermediaireChanged(currentValue);
            firePropertyChange(PROPERTYNAME_TYPEDETRAVERSEINTERMEDIAIRE, oldValue, currentValue);
            afterTypeDeTraverseIntermediaireChanged(currentValue);
        }
    }

    public void beforeTypeDeTraverseIntermediaireChanged( TypeDeTraverseIntermediaire typeDeTraverseIntermediaire)
    { }
    public void afterTypeDeTraverseIntermediaireChanged( TypeDeTraverseIntermediaire typeDeTraverseIntermediaire)
    { }


    public boolean isTypeDeTraverseIntermediaireMandatory()
    {
        return true;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="fr", name="label", value="Type de sous-bassement")
    })
    public final TypeDePanneaux getTypeDePanneaux()
    {
         return this.typeDePanneaux.getValue();
    }

    public final void setTypeDePanneaux(TypeDePanneaux typeDePanneaux)
    {

        TypeDePanneaux oldValue = this.typeDePanneaux.getValue();
        this.typeDePanneaux.setValue(typeDePanneaux);

        TypeDePanneaux currentValue = this.typeDePanneaux.getValue();

        fireTypeDePanneauxChange(currentValue, oldValue);
    }
    public final TypeDePanneaux removeTypeDePanneaux()
    {
        TypeDePanneaux oldValue = this.typeDePanneaux.getValue();
        TypeDePanneaux removedValue = this.typeDePanneaux.removeValue();
        TypeDePanneaux currentValue = this.typeDePanneaux.getValue();

        fireTypeDePanneauxChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultTypeDePanneaux(TypeDePanneaux typeDePanneaux)
    {
        TypeDePanneaux oldValue = this.typeDePanneaux.getValue();
        this.typeDePanneaux.setDefaultValue(typeDePanneaux);

        TypeDePanneaux currentValue = this.typeDePanneaux.getValue();

        fireTypeDePanneauxChange(currentValue, oldValue);
    }
    
    public final TypeDePanneaux removeDefaultTypeDePanneaux()
    {
        TypeDePanneaux oldValue = this.typeDePanneaux.getValue();
        TypeDePanneaux removedValue = this.typeDePanneaux.removeDefaultValue();
        TypeDePanneaux currentValue = this.typeDePanneaux.getValue();

        fireTypeDePanneauxChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<TypeDePanneaux> getTypeDePanneauxValueContainer()
    {
        return this.typeDePanneaux;
    }
    public final void fireTypeDePanneauxChange(TypeDePanneaux currentValue, TypeDePanneaux oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeTypeDePanneauxChanged(currentValue);
            firePropertyChange(PROPERTYNAME_TYPEDEPANNEAUX, oldValue, currentValue);
            afterTypeDePanneauxChanged(currentValue);
        }
    }

    public void beforeTypeDePanneauxChanged( TypeDePanneaux typeDePanneaux)
    { }
    public void afterTypeDePanneauxChanged( TypeDePanneaux typeDePanneaux)
    { }


    public boolean isTypeDePanneauxMandatory()
    {
        return true;
    }


    @Override
    protected String getSequences()
    {
        return "qtyPanneaux,soubassement,hauSbs,traverseBasse,largeurPetitBoisEnfourche,dimension,NoSplittingBars,SplittingBars";  
    }
}
