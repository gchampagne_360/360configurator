// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.client360.configuration.wad.enums.ChoiceExteriorMould;
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class ExteriorMouldBase extends com.netappsid.wadconfigurator.ExteriorMould
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ExteriorMouldBase.class);

    // attributes
    private ValueContainer<ChoiceExteriorMould> choiceExteriorMould = new ValueContainer<ChoiceExteriorMould>(this);

    // Bound properties

    public static final String PROPERTYNAME_CHOICEEXTERIORMOULD = "choiceExteriorMould";  

    // Constructors

    public ExteriorMouldBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.ExteriorMould"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Brick mould choice")
        ,@LocalizedTag(language="fr", name="label", value="Choix moulure � brique")
        ,@LocalizedTag(language="fr", name="desc", value="Choix moulure � brique")
        ,@LocalizedTag(language="en", name="desc", value="Brick mould choice")
    })
    public final ChoiceExteriorMould getChoiceExteriorMould()
    {
         return this.choiceExteriorMould.getValue();
    }

    public final void setChoiceExteriorMould(ChoiceExteriorMould choiceExteriorMould)
    {

        ChoiceExteriorMould oldValue = this.choiceExteriorMould.getValue();
        this.choiceExteriorMould.setValue(choiceExteriorMould);

        ChoiceExteriorMould currentValue = this.choiceExteriorMould.getValue();

        fireChoiceExteriorMouldChange(currentValue, oldValue);
    }
    public final ChoiceExteriorMould removeChoiceExteriorMould()
    {
        ChoiceExteriorMould oldValue = this.choiceExteriorMould.getValue();
        ChoiceExteriorMould removedValue = this.choiceExteriorMould.removeValue();
        ChoiceExteriorMould currentValue = this.choiceExteriorMould.getValue();

        fireChoiceExteriorMouldChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceExteriorMould(ChoiceExteriorMould choiceExteriorMould)
    {
        ChoiceExteriorMould oldValue = this.choiceExteriorMould.getValue();
        this.choiceExteriorMould.setDefaultValue(choiceExteriorMould);

        ChoiceExteriorMould currentValue = this.choiceExteriorMould.getValue();

        fireChoiceExteriorMouldChange(currentValue, oldValue);
    }
    
    public final ChoiceExteriorMould removeDefaultChoiceExteriorMould()
    {
        ChoiceExteriorMould oldValue = this.choiceExteriorMould.getValue();
        ChoiceExteriorMould removedValue = this.choiceExteriorMould.removeDefaultValue();
        ChoiceExteriorMould currentValue = this.choiceExteriorMould.getValue();

        fireChoiceExteriorMouldChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceExteriorMould> getChoiceExteriorMouldValueContainer()
    {
        return this.choiceExteriorMould;
    }
    public final void fireChoiceExteriorMouldChange(ChoiceExteriorMould currentValue, ChoiceExteriorMould oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceExteriorMouldChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEEXTERIORMOULD, oldValue, currentValue);
            afterChoiceExteriorMouldChanged(currentValue);
        }
    }

    public void beforeChoiceExteriorMouldChanged( ChoiceExteriorMould choiceExteriorMould)
    { }
    public void afterChoiceExteriorMouldChanged( ChoiceExteriorMould choiceExteriorMould)
    { }



    @Override
    protected String getSequences()
    {
        return "brickMould";  
    }
}
