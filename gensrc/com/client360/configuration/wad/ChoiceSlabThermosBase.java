// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.DynamicEnum;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

@DynamicEnum(fileName = "dynamicEnums/PA_SlabThermos.tsv")
public abstract class ChoiceSlabThermosBase extends com.netappsid.erp.configurator.Configurable
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ChoiceSlabThermosBase.class);

    // attributes
    private ValueContainer<String> dimension = new ValueContainer<String>(this);
    private ValueContainer<String> height = new ValueContainer<String>(this);
    private ValueContainer<String> width = new ValueContainer<String>(this);
    private ValueContainer<String> form = new ValueContainer<String>(this);
    private ValueContainer<String> isSlabContainer = new ValueContainer<String>(this);
    private ValueContainer<String> isSideLightContainer = new ValueContainer<String>(this);

    // Bound properties

    public static final String PROPERTYNAME_DIMENSION = "dimension";  
    public static final String PROPERTYNAME_HEIGHT = "height";  
    public static final String PROPERTYNAME_WIDTH = "width";  
    public static final String PROPERTYNAME_FORM = "form";  
    public static final String PROPERTYNAME_ISSLABCONTAINER = "isSlabContainer";  
    public static final String PROPERTYNAME_ISSIDELIGHTCONTAINER = "isSideLightContainer";  

    // Constructors

    public ChoiceSlabThermosBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.ChoiceSlabThermos"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="2")
        ,@LocalizedTag(language="fr", name="column", value="2")
        ,@LocalizedTag(language="en", name="label", value="Dimension")
        ,@LocalizedTag(language="fr", name="label", value="Dimension")
    })
    public final String getDimension()
    {
         return this.dimension.getValue();
    }

    public final void setDimension(String dimension)
    {

        String oldValue = this.dimension.getValue();
        this.dimension.setValue(dimension);

        String currentValue = this.dimension.getValue();

        fireDimensionChange(currentValue, oldValue);
    }
    public final String removeDimension()
    {
        String oldValue = this.dimension.getValue();
        String removedValue = this.dimension.removeValue();
        String currentValue = this.dimension.getValue();

        fireDimensionChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultDimension(String dimension)
    {
        String oldValue = this.dimension.getValue();
        this.dimension.setDefaultValue(dimension);

        String currentValue = this.dimension.getValue();

        fireDimensionChange(currentValue, oldValue);
    }
    
    public final String removeDefaultDimension()
    {
        String oldValue = this.dimension.getValue();
        String removedValue = this.dimension.removeDefaultValue();
        String currentValue = this.dimension.getValue();

        fireDimensionChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getDimensionValueContainer()
    {
        return this.dimension;
    }
    public final void fireDimensionChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeDimensionChanged(currentValue);
            firePropertyChange(PROPERTYNAME_DIMENSION, oldValue, currentValue);
            afterDimensionChanged(currentValue);
        }
    }

    public void beforeDimensionChanged( String dimension)
    { }
    public void afterDimensionChanged( String dimension)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="1")
        ,@LocalizedTag(language="fr", name="column", value="1")
        ,@LocalizedTag(language="en", name="label", value="Height")
        ,@LocalizedTag(language="fr", name="label", value="Hauteur")
    })
    public final String getHeight()
    {
         return this.height.getValue();
    }

    public final void setHeight(String height)
    {

        String oldValue = this.height.getValue();
        this.height.setValue(height);

        String currentValue = this.height.getValue();

        fireHeightChange(currentValue, oldValue);
    }
    public final String removeHeight()
    {
        String oldValue = this.height.getValue();
        String removedValue = this.height.removeValue();
        String currentValue = this.height.getValue();

        fireHeightChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultHeight(String height)
    {
        String oldValue = this.height.getValue();
        this.height.setDefaultValue(height);

        String currentValue = this.height.getValue();

        fireHeightChange(currentValue, oldValue);
    }
    
    public final String removeDefaultHeight()
    {
        String oldValue = this.height.getValue();
        String removedValue = this.height.removeDefaultValue();
        String currentValue = this.height.getValue();

        fireHeightChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getHeightValueContainer()
    {
        return this.height;
    }
    public final void fireHeightChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeHeightChanged(currentValue);
            firePropertyChange(PROPERTYNAME_HEIGHT, oldValue, currentValue);
            afterHeightChanged(currentValue);
        }
    }

    public void beforeHeightChanged( String height)
    { }
    public void afterHeightChanged( String height)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="0")
        ,@LocalizedTag(language="fr", name="column", value="0")
        ,@LocalizedTag(language="en", name="label", value="Width")
        ,@LocalizedTag(language="fr", name="label", value="Largeur")
    })
    public final String getWidth()
    {
         return this.width.getValue();
    }

    public final void setWidth(String width)
    {

        String oldValue = this.width.getValue();
        this.width.setValue(width);

        String currentValue = this.width.getValue();

        fireWidthChange(currentValue, oldValue);
    }
    public final String removeWidth()
    {
        String oldValue = this.width.getValue();
        String removedValue = this.width.removeValue();
        String currentValue = this.width.getValue();

        fireWidthChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultWidth(String width)
    {
        String oldValue = this.width.getValue();
        this.width.setDefaultValue(width);

        String currentValue = this.width.getValue();

        fireWidthChange(currentValue, oldValue);
    }
    
    public final String removeDefaultWidth()
    {
        String oldValue = this.width.getValue();
        String removedValue = this.width.removeDefaultValue();
        String currentValue = this.width.getValue();

        fireWidthChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getWidthValueContainer()
    {
        return this.width;
    }
    public final void fireWidthChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeWidthChanged(currentValue);
            firePropertyChange(PROPERTYNAME_WIDTH, oldValue, currentValue);
            afterWidthChanged(currentValue);
        }
    }

    public void beforeWidthChanged( String width)
    { }
    public void afterWidthChanged( String width)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="3")
        ,@LocalizedTag(language="fr", name="column", value="3")
        ,@LocalizedTag(language="en", name="label", value="Form")
        ,@LocalizedTag(language="fr", name="label", value="Forme")
    })
    public final String getForm()
    {
         return this.form.getValue();
    }

    public final void setForm(String form)
    {

        String oldValue = this.form.getValue();
        this.form.setValue(form);

        String currentValue = this.form.getValue();

        fireFormChange(currentValue, oldValue);
    }
    public final String removeForm()
    {
        String oldValue = this.form.getValue();
        String removedValue = this.form.removeValue();
        String currentValue = this.form.getValue();

        fireFormChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultForm(String form)
    {
        String oldValue = this.form.getValue();
        this.form.setDefaultValue(form);

        String currentValue = this.form.getValue();

        fireFormChange(currentValue, oldValue);
    }
    
    public final String removeDefaultForm()
    {
        String oldValue = this.form.getValue();
        String removedValue = this.form.removeDefaultValue();
        String currentValue = this.form.getValue();

        fireFormChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getFormValueContainer()
    {
        return this.form;
    }
    public final void fireFormChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeFormChanged(currentValue);
            firePropertyChange(PROPERTYNAME_FORM, oldValue, currentValue);
            afterFormChanged(currentValue);
        }
    }

    public void beforeFormChanged( String form)
    { }
    public void afterFormChanged( String form)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="4")
        ,@LocalizedTag(language="fr", name="column", value="4")
        ,@LocalizedTag(language="en", name="label", value="is SlabDoor Container")
        ,@LocalizedTag(language="fr", name="label", value="is SlabDoor Container")
    })
    public final String getIsSlabContainer()
    {
         return this.isSlabContainer.getValue();
    }

    public final void setIsSlabContainer(String isSlabContainer)
    {

        String oldValue = this.isSlabContainer.getValue();
        this.isSlabContainer.setValue(isSlabContainer);

        String currentValue = this.isSlabContainer.getValue();

        fireIsSlabContainerChange(currentValue, oldValue);
    }
    public final String removeIsSlabContainer()
    {
        String oldValue = this.isSlabContainer.getValue();
        String removedValue = this.isSlabContainer.removeValue();
        String currentValue = this.isSlabContainer.getValue();

        fireIsSlabContainerChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultIsSlabContainer(String isSlabContainer)
    {
        String oldValue = this.isSlabContainer.getValue();
        this.isSlabContainer.setDefaultValue(isSlabContainer);

        String currentValue = this.isSlabContainer.getValue();

        fireIsSlabContainerChange(currentValue, oldValue);
    }
    
    public final String removeDefaultIsSlabContainer()
    {
        String oldValue = this.isSlabContainer.getValue();
        String removedValue = this.isSlabContainer.removeDefaultValue();
        String currentValue = this.isSlabContainer.getValue();

        fireIsSlabContainerChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getIsSlabContainerValueContainer()
    {
        return this.isSlabContainer;
    }
    public final void fireIsSlabContainerChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeIsSlabContainerChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ISSLABCONTAINER, oldValue, currentValue);
            afterIsSlabContainerChanged(currentValue);
        }
    }

    public void beforeIsSlabContainerChanged( String isSlabContainer)
    { }
    public void afterIsSlabContainerChanged( String isSlabContainer)
    { }


    public boolean isIsSlabContainerVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="5")
        ,@LocalizedTag(language="fr", name="column", value="5")
        ,@LocalizedTag(language="en", name="label", value="is SideLight Container")
        ,@LocalizedTag(language="fr", name="label", value="is SideLight Container")
    })
    public final String getIsSideLightContainer()
    {
         return this.isSideLightContainer.getValue();
    }

    public final void setIsSideLightContainer(String isSideLightContainer)
    {

        String oldValue = this.isSideLightContainer.getValue();
        this.isSideLightContainer.setValue(isSideLightContainer);

        String currentValue = this.isSideLightContainer.getValue();

        fireIsSideLightContainerChange(currentValue, oldValue);
    }
    public final String removeIsSideLightContainer()
    {
        String oldValue = this.isSideLightContainer.getValue();
        String removedValue = this.isSideLightContainer.removeValue();
        String currentValue = this.isSideLightContainer.getValue();

        fireIsSideLightContainerChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultIsSideLightContainer(String isSideLightContainer)
    {
        String oldValue = this.isSideLightContainer.getValue();
        this.isSideLightContainer.setDefaultValue(isSideLightContainer);

        String currentValue = this.isSideLightContainer.getValue();

        fireIsSideLightContainerChange(currentValue, oldValue);
    }
    
    public final String removeDefaultIsSideLightContainer()
    {
        String oldValue = this.isSideLightContainer.getValue();
        String removedValue = this.isSideLightContainer.removeDefaultValue();
        String currentValue = this.isSideLightContainer.getValue();

        fireIsSideLightContainerChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getIsSideLightContainerValueContainer()
    {
        return this.isSideLightContainer;
    }
    public final void fireIsSideLightContainerChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeIsSideLightContainerChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ISSIDELIGHTCONTAINER, oldValue, currentValue);
            afterIsSideLightContainerChanged(currentValue);
        }
    }

    public void beforeIsSideLightContainerChanged( String isSideLightContainer)
    { }
    public void afterIsSideLightContainerChanged( String isSideLightContainer)
    { }


    public boolean isIsSideLightContainerVisible()
    {
        return false;
    }

}
