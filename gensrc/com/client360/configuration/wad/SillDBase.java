// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class SillDBase extends com.client360.configuration.wad.Sill
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(SillDBase.class);

    // attributes
    private ValueContainer<Sill> sill = new ValueContainer<Sill>(this);

    // Bound properties

    public static final String PROPERTYNAME_SILL = "sill";  

    // Constructors

    public SillDBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.SillD"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
    })
    public final Sill getSill()
    {
         return this.sill.getValue();
    }

    public final void setSill(Sill sill)
    {

        if (sill != null)
        {
            acquire(sill, "sill");
        }
        Sill oldValue = this.sill.getValue();
        this.sill.setValue(sill);

        Sill currentValue = this.sill.getValue();

        fireSillChange(currentValue, oldValue);
    }
    public final Sill removeSill()
    {
        Sill oldValue = this.sill.getValue();
        Sill removedValue = this.sill.removeValue();
        Sill currentValue = this.sill.getValue();

        fireSillChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultSill(Sill sill)
    {
        if (sill != null)
        {
            acquire(sill, "sill");
        }
        Sill oldValue = this.sill.getValue();
        this.sill.setDefaultValue(sill);

        Sill currentValue = this.sill.getValue();

        fireSillChange(currentValue, oldValue);
    }
    
    public final Sill removeDefaultSill()
    {
        Sill oldValue = this.sill.getValue();
        Sill removedValue = this.sill.removeDefaultValue();
        Sill currentValue = this.sill.getValue();

        fireSillChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Sill> getSillValueContainer()
    {
        return this.sill;
    }
    public final void fireSillChange(Sill currentValue, Sill oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeSillChanged(currentValue);
            firePropertyChange(PROPERTYNAME_SILL, oldValue, currentValue);
            afterSillChanged(currentValue);
        }
    }

    public void beforeSillChanged( Sill sill)
    { }
    public void afterSillChanged( Sill sill)
    { }


    public boolean isSillMandatory()
    {
        return true;
    }

}
