// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class FrameBase extends com.netappsid.wadconfigurator.Frame
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(FrameBase.class);

    // attributes
    private ValueContainer<Measure<Length>> sufflageDepth = new ValueContainer<Measure<Length>>(this);

    // Bound properties

    public static final String PROPERTYNAME_SUFFLAGEDEPTH = "sufflageDepth";  

    // Constructors

    public FrameBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.Frame"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="desc", value="Blowing depth")
        ,@LocalizedTag(language="fr", name="desc", value="Profondeur souflage")
        ,@LocalizedTag(language="en", name="label", value="Blowing depth")
        ,@LocalizedTag(language="fr", name="label", value="Profondeur souflage")
    })
    public final Measure<Length> getSufflageDepth()
    {
         return this.sufflageDepth.getValue();
    }

    public final void setSufflageDepth(Measure<Length> sufflageDepth)
    {

        sufflageDepth = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(sufflageDepth, getDefaultUnitFor(PROPERTYNAME_SUFFLAGEDEPTH,Length.class));

        Measure<Length> oldValue = this.sufflageDepth.getValue();
        this.sufflageDepth.setValue(sufflageDepth);

        Measure<Length> currentValue = this.sufflageDepth.getValue();

        fireSufflageDepthChange(currentValue, oldValue);
    }
    public final Measure<Length> removeSufflageDepth()
    {
        Measure<Length> oldValue = this.sufflageDepth.getValue();
        Measure<Length> removedValue = this.sufflageDepth.removeValue();
        Measure<Length> currentValue = this.sufflageDepth.getValue();

        fireSufflageDepthChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultSufflageDepth(Measure<Length> sufflageDepth)
    {
        sufflageDepth = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(sufflageDepth, getDefaultUnitFor(PROPERTYNAME_SUFFLAGEDEPTH,Length.class));

        Measure<Length> oldValue = this.sufflageDepth.getValue();
        this.sufflageDepth.setDefaultValue(sufflageDepth);

        Measure<Length> currentValue = this.sufflageDepth.getValue();

        fireSufflageDepthChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultSufflageDepth()
    {
        Measure<Length> oldValue = this.sufflageDepth.getValue();
        Measure<Length> removedValue = this.sufflageDepth.removeDefaultValue();
        Measure<Length> currentValue = this.sufflageDepth.getValue();

        fireSufflageDepthChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getSufflageDepthValueContainer()
    {
        return this.sufflageDepth;
    }
    public final void fireSufflageDepthChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeSufflageDepthChanged(currentValue);
            firePropertyChange(PROPERTYNAME_SUFFLAGEDEPTH, oldValue, currentValue);
            afterSufflageDepthChanged(currentValue);
        }
    }

    public void beforeSufflageDepthChanged( Measure<Length> sufflageDepth)
    { }
    public void afterSufflageDepthChanged( Measure<Length> sufflageDepth)
    { }



    @Override
    protected String getSequences()
    {
        return "depth,woodFrame,woodenBox,interiorMould,exteriorMould,casing";  
    }
}
