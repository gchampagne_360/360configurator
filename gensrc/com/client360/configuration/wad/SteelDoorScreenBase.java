// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class SteelDoorScreenBase extends com.netappsid.erp.configurator.Configurable
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(SteelDoorScreenBase.class);

    // attributes
    private ValueContainer<SteelDoorScreenDoor> steelDoorScreenDoor = new ValueContainer<SteelDoorScreenDoor>(this);

    // Bound properties

    public static final String PROPERTYNAME_STEELDOORSCREENDOOR = "steelDoorScreenDoor";  

    // Constructors

    public SteelDoorScreenBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.SteelDoorScreen"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Screen")
        ,@LocalizedTag(language="fr", name="label", value="Moustiquaire")
    })
    public final SteelDoorScreenDoor getSteelDoorScreenDoor()
    {
         return this.steelDoorScreenDoor.getValue();
    }

    public final void setSteelDoorScreenDoor(SteelDoorScreenDoor steelDoorScreenDoor)
    {

        if (steelDoorScreenDoor != null)
        {
            acquire(steelDoorScreenDoor, "steelDoorScreenDoor");
        }
        SteelDoorScreenDoor oldValue = this.steelDoorScreenDoor.getValue();
        this.steelDoorScreenDoor.setValue(steelDoorScreenDoor);

        SteelDoorScreenDoor currentValue = this.steelDoorScreenDoor.getValue();

        fireSteelDoorScreenDoorChange(currentValue, oldValue);
    }
    public final SteelDoorScreenDoor removeSteelDoorScreenDoor()
    {
        SteelDoorScreenDoor oldValue = this.steelDoorScreenDoor.getValue();
        SteelDoorScreenDoor removedValue = this.steelDoorScreenDoor.removeValue();
        SteelDoorScreenDoor currentValue = this.steelDoorScreenDoor.getValue();

        fireSteelDoorScreenDoorChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultSteelDoorScreenDoor(SteelDoorScreenDoor steelDoorScreenDoor)
    {
        if (steelDoorScreenDoor != null)
        {
            acquire(steelDoorScreenDoor, "steelDoorScreenDoor");
        }
        SteelDoorScreenDoor oldValue = this.steelDoorScreenDoor.getValue();
        this.steelDoorScreenDoor.setDefaultValue(steelDoorScreenDoor);

        SteelDoorScreenDoor currentValue = this.steelDoorScreenDoor.getValue();

        fireSteelDoorScreenDoorChange(currentValue, oldValue);
    }
    
    public final SteelDoorScreenDoor removeDefaultSteelDoorScreenDoor()
    {
        SteelDoorScreenDoor oldValue = this.steelDoorScreenDoor.getValue();
        SteelDoorScreenDoor removedValue = this.steelDoorScreenDoor.removeDefaultValue();
        SteelDoorScreenDoor currentValue = this.steelDoorScreenDoor.getValue();

        fireSteelDoorScreenDoorChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<SteelDoorScreenDoor> getSteelDoorScreenDoorValueContainer()
    {
        return this.steelDoorScreenDoor;
    }
    public final void fireSteelDoorScreenDoorChange(SteelDoorScreenDoor currentValue, SteelDoorScreenDoor oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeSteelDoorScreenDoorChanged(currentValue);
            firePropertyChange(PROPERTYNAME_STEELDOORSCREENDOOR, oldValue, currentValue);
            afterSteelDoorScreenDoorChanged(currentValue);
        }
    }

    public void beforeSteelDoorScreenDoorChanged( SteelDoorScreenDoor steelDoorScreenDoor)
    { }
    public void afterSteelDoorScreenDoorChanged( SteelDoorScreenDoor steelDoorScreenDoor)
    { }


}
