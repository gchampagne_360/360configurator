// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.DynamicEnum;
import com.netappsid.erp.configurator.annotations.Filtrable;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

@DynamicEnum(fileName = "dynamicEnums/InsulatedGlassOptifghdghfhfons.tsv", locale = "fr")
public abstract class ChoiceInsulatedGlassOptionBase extends com.netappsid.erp.configurator.Configurable
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ChoiceInsulatedGlassOptionBase.class);

    // attributes
    private ValueContainer<Double> id = new ValueContainer<Double>(this);
    private ValueContainer<String> glassType = new ValueContainer<String>(this);
    private ValueContainer<String> glassTreatment = new ValueContainer<String>(this);
    private ValueContainer<String> energeticGlassTreatment = new ValueContainer<String>(this);
    private ValueContainer<Double> glassThickness = new ValueContainer<Double>(this);
    private ValueContainer<String> tint = new ValueContainer<String>(this);
    private ValueContainer<String> choiceGlassThickness = new ValueContainer<String>(this);
    private ValueContainer<String> choiceGlassTreatment = new ValueContainer<String>(this);
    private ValueContainer<String> choiceTint = new ValueContainer<String>(this);

    // Bound properties

    public static final String PROPERTYNAME_ID = "id";  
    public static final String PROPERTYNAME_GLASSTYPE = "glassType";  
    public static final String PROPERTYNAME_GLASSTREATMENT = "glassTreatment";  
    public static final String PROPERTYNAME_ENERGETICGLASSTREATMENT = "energeticGlassTreatment";  
    public static final String PROPERTYNAME_GLASSTHICKNESS = "glassThickness";  
    public static final String PROPERTYNAME_TINT = "tint";  
    public static final String PROPERTYNAME_CHOICEGLASSTHICKNESS = "choiceGlassThickness";  
    public static final String PROPERTYNAME_CHOICEGLASSTREATMENT = "choiceGlassTreatment";  
    public static final String PROPERTYNAME_CHOICETINT = "choiceTint";  

    // Constructors

    public ChoiceInsulatedGlassOptionBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.ChoiceInsulatedGlassOption"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="0")
        ,@LocalizedTag(language="fr", name="column", value="0")
    })
    public final Double getId()
    {
         return this.id.getValue();
    }

    public final void setId(Double id)
    {

        Double oldValue = this.id.getValue();
        this.id.setValue(id);

        Double currentValue = this.id.getValue();

        fireIdChange(currentValue, oldValue);
    }
    public final Double removeId()
    {
        Double oldValue = this.id.getValue();
        Double removedValue = this.id.removeValue();
        Double currentValue = this.id.getValue();

        fireIdChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultId(Double id)
    {
        Double oldValue = this.id.getValue();
        this.id.setDefaultValue(id);

        Double currentValue = this.id.getValue();

        fireIdChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultId()
    {
        Double oldValue = this.id.getValue();
        Double removedValue = this.id.removeDefaultValue();
        Double currentValue = this.id.getValue();

        fireIdChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getIdValueContainer()
    {
        return this.id;
    }
    public final void fireIdChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeIdChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ID, oldValue, currentValue);
            afterIdChanged(currentValue);
        }
    }

    public void beforeIdChanged( Double id)
    { }
    public void afterIdChanged( Double id)
    { }


    public boolean isIdVisible()
    {
        return false;
    }
    @Filtrable(value = "true", defaultValue = "", defaultSelection = "")
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="1")
        ,@LocalizedTag(language="fr", name="column", value="1")
        ,@LocalizedTag(language="en", name="label", value="Glass type")
        ,@LocalizedTag(language="fr", name="label", value="Type de verre")
    })
    public final String getGlassType()
    {
         return this.glassType.getValue();
    }

    public final void setGlassType(String glassType)
    {

        String oldValue = this.glassType.getValue();
        this.glassType.setValue(glassType);

        String currentValue = this.glassType.getValue();

        fireGlassTypeChange(currentValue, oldValue);
    }
    public final String removeGlassType()
    {
        String oldValue = this.glassType.getValue();
        String removedValue = this.glassType.removeValue();
        String currentValue = this.glassType.getValue();

        fireGlassTypeChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultGlassType(String glassType)
    {
        String oldValue = this.glassType.getValue();
        this.glassType.setDefaultValue(glassType);

        String currentValue = this.glassType.getValue();

        fireGlassTypeChange(currentValue, oldValue);
    }
    
    public final String removeDefaultGlassType()
    {
        String oldValue = this.glassType.getValue();
        String removedValue = this.glassType.removeDefaultValue();
        String currentValue = this.glassType.getValue();

        fireGlassTypeChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getGlassTypeValueContainer()
    {
        return this.glassType;
    }
    public final void fireGlassTypeChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeGlassTypeChanged(currentValue);
            firePropertyChange(PROPERTYNAME_GLASSTYPE, oldValue, currentValue);
            afterGlassTypeChanged(currentValue);
        }
    }

    public void beforeGlassTypeChanged( String glassType)
    { }
    public void afterGlassTypeChanged( String glassType)
    { }

    @Filtrable(value = "true", defaultValue = "", defaultSelection = "")
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="2")
        ,@LocalizedTag(language="fr", name="column", value="2")
        ,@LocalizedTag(language="en", name="label", value="glass Treatment")
        ,@LocalizedTag(language="fr", name="label", value="Traitement sur le verre")
    })
    public final String getGlassTreatment()
    {
         return this.glassTreatment.getValue();
    }

    public final void setGlassTreatment(String glassTreatment)
    {

        String oldValue = this.glassTreatment.getValue();
        this.glassTreatment.setValue(glassTreatment);

        String currentValue = this.glassTreatment.getValue();

        fireGlassTreatmentChange(currentValue, oldValue);
    }
    public final String removeGlassTreatment()
    {
        String oldValue = this.glassTreatment.getValue();
        String removedValue = this.glassTreatment.removeValue();
        String currentValue = this.glassTreatment.getValue();

        fireGlassTreatmentChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultGlassTreatment(String glassTreatment)
    {
        String oldValue = this.glassTreatment.getValue();
        this.glassTreatment.setDefaultValue(glassTreatment);

        String currentValue = this.glassTreatment.getValue();

        fireGlassTreatmentChange(currentValue, oldValue);
    }
    
    public final String removeDefaultGlassTreatment()
    {
        String oldValue = this.glassTreatment.getValue();
        String removedValue = this.glassTreatment.removeDefaultValue();
        String currentValue = this.glassTreatment.getValue();

        fireGlassTreatmentChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getGlassTreatmentValueContainer()
    {
        return this.glassTreatment;
    }
    public final void fireGlassTreatmentChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeGlassTreatmentChanged(currentValue);
            firePropertyChange(PROPERTYNAME_GLASSTREATMENT, oldValue, currentValue);
            afterGlassTreatmentChanged(currentValue);
        }
    }

    public void beforeGlassTreatmentChanged( String glassTreatment)
    { }
    public void afterGlassTreatmentChanged( String glassTreatment)
    { }

    @Filtrable(value = "true", defaultValue = "", defaultSelection = "")
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="3")
        ,@LocalizedTag(language="fr", name="column", value="3")
        ,@LocalizedTag(language="en", name="label", value="Energetic treatment")
        ,@LocalizedTag(language="fr", name="label", value="Traitement énergétique")
    })
    public final String getEnergeticGlassTreatment()
    {
         return this.energeticGlassTreatment.getValue();
    }

    public final void setEnergeticGlassTreatment(String energeticGlassTreatment)
    {

        String oldValue = this.energeticGlassTreatment.getValue();
        this.energeticGlassTreatment.setValue(energeticGlassTreatment);

        String currentValue = this.energeticGlassTreatment.getValue();

        fireEnergeticGlassTreatmentChange(currentValue, oldValue);
    }
    public final String removeEnergeticGlassTreatment()
    {
        String oldValue = this.energeticGlassTreatment.getValue();
        String removedValue = this.energeticGlassTreatment.removeValue();
        String currentValue = this.energeticGlassTreatment.getValue();

        fireEnergeticGlassTreatmentChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultEnergeticGlassTreatment(String energeticGlassTreatment)
    {
        String oldValue = this.energeticGlassTreatment.getValue();
        this.energeticGlassTreatment.setDefaultValue(energeticGlassTreatment);

        String currentValue = this.energeticGlassTreatment.getValue();

        fireEnergeticGlassTreatmentChange(currentValue, oldValue);
    }
    
    public final String removeDefaultEnergeticGlassTreatment()
    {
        String oldValue = this.energeticGlassTreatment.getValue();
        String removedValue = this.energeticGlassTreatment.removeDefaultValue();
        String currentValue = this.energeticGlassTreatment.getValue();

        fireEnergeticGlassTreatmentChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getEnergeticGlassTreatmentValueContainer()
    {
        return this.energeticGlassTreatment;
    }
    public final void fireEnergeticGlassTreatmentChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeEnergeticGlassTreatmentChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ENERGETICGLASSTREATMENT, oldValue, currentValue);
            afterEnergeticGlassTreatmentChanged(currentValue);
        }
    }

    public void beforeEnergeticGlassTreatmentChanged( String energeticGlassTreatment)
    { }
    public void afterEnergeticGlassTreatmentChanged( String energeticGlassTreatment)
    { }

    @Filtrable(value = "true", defaultValue = "", defaultSelection = "")
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="4")
        ,@LocalizedTag(language="fr", name="column", value="4")
        ,@LocalizedTag(language="en", name="label", value="Glass thickness")
        ,@LocalizedTag(language="fr", name="label", value="Épaisseur de verre")
    })
    public final Double getGlassThickness()
    {
         return this.glassThickness.getValue();
    }

    public final void setGlassThickness(Double glassThickness)
    {

        Double oldValue = this.glassThickness.getValue();
        this.glassThickness.setValue(glassThickness);

        Double currentValue = this.glassThickness.getValue();

        fireGlassThicknessChange(currentValue, oldValue);
    }
    public final Double removeGlassThickness()
    {
        Double oldValue = this.glassThickness.getValue();
        Double removedValue = this.glassThickness.removeValue();
        Double currentValue = this.glassThickness.getValue();

        fireGlassThicknessChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultGlassThickness(Double glassThickness)
    {
        Double oldValue = this.glassThickness.getValue();
        this.glassThickness.setDefaultValue(glassThickness);

        Double currentValue = this.glassThickness.getValue();

        fireGlassThicknessChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultGlassThickness()
    {
        Double oldValue = this.glassThickness.getValue();
        Double removedValue = this.glassThickness.removeDefaultValue();
        Double currentValue = this.glassThickness.getValue();

        fireGlassThicknessChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getGlassThicknessValueContainer()
    {
        return this.glassThickness;
    }
    public final void fireGlassThicknessChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeGlassThicknessChanged(currentValue);
            firePropertyChange(PROPERTYNAME_GLASSTHICKNESS, oldValue, currentValue);
            afterGlassThicknessChanged(currentValue);
        }
    }

    public void beforeGlassThicknessChanged( Double glassThickness)
    { }
    public void afterGlassThicknessChanged( Double glassThickness)
    { }

    @Filtrable(value = "true", defaultValue = "", defaultSelection = "")
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="5")
        ,@LocalizedTag(language="fr", name="column", value="5")
        ,@LocalizedTag(language="en", name="label", value="Tint")
        ,@LocalizedTag(language="fr", name="label", value="Teinte")
    })
    public final String getTint()
    {
         return this.tint.getValue();
    }

    public final void setTint(String tint)
    {

        String oldValue = this.tint.getValue();
        this.tint.setValue(tint);

        String currentValue = this.tint.getValue();

        fireTintChange(currentValue, oldValue);
    }
    public final String removeTint()
    {
        String oldValue = this.tint.getValue();
        String removedValue = this.tint.removeValue();
        String currentValue = this.tint.getValue();

        fireTintChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultTint(String tint)
    {
        String oldValue = this.tint.getValue();
        this.tint.setDefaultValue(tint);

        String currentValue = this.tint.getValue();

        fireTintChange(currentValue, oldValue);
    }
    
    public final String removeDefaultTint()
    {
        String oldValue = this.tint.getValue();
        String removedValue = this.tint.removeDefaultValue();
        String currentValue = this.tint.getValue();

        fireTintChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getTintValueContainer()
    {
        return this.tint;
    }
    public final void fireTintChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeTintChanged(currentValue);
            firePropertyChange(PROPERTYNAME_TINT, oldValue, currentValue);
            afterTintChanged(currentValue);
        }
    }

    public void beforeTintChanged( String tint)
    { }
    public void afterTintChanged( String tint)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="6")
        ,@LocalizedTag(language="fr", name="column", value="6")
    })
    public final String getChoiceGlassThickness()
    {
         return this.choiceGlassThickness.getValue();
    }

    public final void setChoiceGlassThickness(String choiceGlassThickness)
    {

        String oldValue = this.choiceGlassThickness.getValue();
        this.choiceGlassThickness.setValue(choiceGlassThickness);

        String currentValue = this.choiceGlassThickness.getValue();

        fireChoiceGlassThicknessChange(currentValue, oldValue);
    }
    public final String removeChoiceGlassThickness()
    {
        String oldValue = this.choiceGlassThickness.getValue();
        String removedValue = this.choiceGlassThickness.removeValue();
        String currentValue = this.choiceGlassThickness.getValue();

        fireChoiceGlassThicknessChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceGlassThickness(String choiceGlassThickness)
    {
        String oldValue = this.choiceGlassThickness.getValue();
        this.choiceGlassThickness.setDefaultValue(choiceGlassThickness);

        String currentValue = this.choiceGlassThickness.getValue();

        fireChoiceGlassThicknessChange(currentValue, oldValue);
    }
    
    public final String removeDefaultChoiceGlassThickness()
    {
        String oldValue = this.choiceGlassThickness.getValue();
        String removedValue = this.choiceGlassThickness.removeDefaultValue();
        String currentValue = this.choiceGlassThickness.getValue();

        fireChoiceGlassThicknessChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getChoiceGlassThicknessValueContainer()
    {
        return this.choiceGlassThickness;
    }
    public final void fireChoiceGlassThicknessChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceGlassThicknessChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEGLASSTHICKNESS, oldValue, currentValue);
            afterChoiceGlassThicknessChanged(currentValue);
        }
    }

    public void beforeChoiceGlassThicknessChanged( String choiceGlassThickness)
    { }
    public void afterChoiceGlassThicknessChanged( String choiceGlassThickness)
    { }


    public boolean isChoiceGlassThicknessVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="7")
        ,@LocalizedTag(language="fr", name="column", value="7")
    })
    public final String getChoiceGlassTreatment()
    {
         return this.choiceGlassTreatment.getValue();
    }

    public final void setChoiceGlassTreatment(String choiceGlassTreatment)
    {

        String oldValue = this.choiceGlassTreatment.getValue();
        this.choiceGlassTreatment.setValue(choiceGlassTreatment);

        String currentValue = this.choiceGlassTreatment.getValue();

        fireChoiceGlassTreatmentChange(currentValue, oldValue);
    }
    public final String removeChoiceGlassTreatment()
    {
        String oldValue = this.choiceGlassTreatment.getValue();
        String removedValue = this.choiceGlassTreatment.removeValue();
        String currentValue = this.choiceGlassTreatment.getValue();

        fireChoiceGlassTreatmentChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceGlassTreatment(String choiceGlassTreatment)
    {
        String oldValue = this.choiceGlassTreatment.getValue();
        this.choiceGlassTreatment.setDefaultValue(choiceGlassTreatment);

        String currentValue = this.choiceGlassTreatment.getValue();

        fireChoiceGlassTreatmentChange(currentValue, oldValue);
    }
    
    public final String removeDefaultChoiceGlassTreatment()
    {
        String oldValue = this.choiceGlassTreatment.getValue();
        String removedValue = this.choiceGlassTreatment.removeDefaultValue();
        String currentValue = this.choiceGlassTreatment.getValue();

        fireChoiceGlassTreatmentChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getChoiceGlassTreatmentValueContainer()
    {
        return this.choiceGlassTreatment;
    }
    public final void fireChoiceGlassTreatmentChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceGlassTreatmentChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEGLASSTREATMENT, oldValue, currentValue);
            afterChoiceGlassTreatmentChanged(currentValue);
        }
    }

    public void beforeChoiceGlassTreatmentChanged( String choiceGlassTreatment)
    { }
    public void afterChoiceGlassTreatmentChanged( String choiceGlassTreatment)
    { }


    public boolean isChoiceGlassTreatmentVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="8")
        ,@LocalizedTag(language="fr", name="column", value="8")
    })
    public final String getChoiceTint()
    {
         return this.choiceTint.getValue();
    }

    public final void setChoiceTint(String choiceTint)
    {

        String oldValue = this.choiceTint.getValue();
        this.choiceTint.setValue(choiceTint);

        String currentValue = this.choiceTint.getValue();

        fireChoiceTintChange(currentValue, oldValue);
    }
    public final String removeChoiceTint()
    {
        String oldValue = this.choiceTint.getValue();
        String removedValue = this.choiceTint.removeValue();
        String currentValue = this.choiceTint.getValue();

        fireChoiceTintChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceTint(String choiceTint)
    {
        String oldValue = this.choiceTint.getValue();
        this.choiceTint.setDefaultValue(choiceTint);

        String currentValue = this.choiceTint.getValue();

        fireChoiceTintChange(currentValue, oldValue);
    }
    
    public final String removeDefaultChoiceTint()
    {
        String oldValue = this.choiceTint.getValue();
        String removedValue = this.choiceTint.removeDefaultValue();
        String currentValue = this.choiceTint.getValue();

        fireChoiceTintChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getChoiceTintValueContainer()
    {
        return this.choiceTint;
    }
    public final void fireChoiceTintChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceTintChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICETINT, oldValue, currentValue);
            afterChoiceTintChanged(currentValue);
        }
    }

    public void beforeChoiceTintChanged( String choiceTint)
    { }
    public void afterChoiceTintChanged( String choiceTint)
    { }


    public boolean isChoiceTintVisible()
    {
        return false;
    }

}
