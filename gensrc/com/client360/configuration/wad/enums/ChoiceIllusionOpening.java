// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 


public enum ChoiceIllusionOpening
{
    @SuppressWarnings("nls") LEFT("Gauche - Passive(fixe)", "Left - Passive(fixed)",  ""),
    @SuppressWarnings("nls") RIGHT("Passive(fixe) - Droite", "Passive(fixed) - Right",  "");

    public String label_fr;
    public String label_en;
    public String image;
    
    ChoiceIllusionOpening(String label_fr, String label_en, String image)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
    }

}

