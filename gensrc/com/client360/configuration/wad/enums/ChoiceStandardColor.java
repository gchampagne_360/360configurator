// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 


public enum ChoiceStandardColor
{
    @SuppressWarnings("nls") MUNTIN("NOIR", "BLACK",  "/images/jpgColorChart/0-0-0.jpg", 0, 0, 0, "ALU", "", "", null, ""),
    @SuppressWarnings("nls") WHITE("BLANC", "WHITE",  "/images/jpgColorChart/255-255-237.jpg", 255, 255, 237, "ALU,PVC", "", "", null, ""),
    @SuppressWarnings("nls") ALMOND_ALU("AMANDE", "ALMOND",  "/images/jpgColorChart/237-217-140.jpg", 237, 217, 140, "ALU", "", "", null, ""),
    @SuppressWarnings("nls") BROWN_ALU("BRUN MUSCADE", "BROWN",  "/images/jpgColorChart/73-61-35.jpg", 73, 61, 35, "ALU", "", "", null, ""),
    @SuppressWarnings("nls") BEIGE_ALU("BEIGE ANTIQUE", "BEIGE",  "/images/jpgColorChart/188-195-190.jpg", 188, 195, 190, "ALU", "", "", null, ""),
    @SuppressWarnings("nls") SABLON_ALU("SABLON", "SABLON",  "/images/jpgColorChart/199-172-95.jpg", 199, 172, 95, "ALU", "", "", null, ""),
    @SuppressWarnings("nls") CHARBON_ALU("CHARBON", "COAL",  "/images/jpgColorChart/112-129-144.jpg", 112, 129, 144, "ALU", "", "", null, ""),
    @SuppressWarnings("nls") BEECH("Beech", "Beech",  "/images/jpgWoodChart/wood_beech.jpg", 255, 255, 255, "", "", "", null, "Beech.jpg"),
    @SuppressWarnings("nls") BIRCH("Chene", "Oak",  "/images/jpgWoodChart/wood_birch.jpg", 255, 255, 255, "", "", "", null, "Birch.jpg"),
    @SuppressWarnings("nls") BUTCHER_BLOCK("Planche � d�couper", "Butcher block",  "/images/jpgWoodChart/wood_butcher_block.jpg", 255, 255, 255, "", "", "", null, "Butcher_block.jpg"),
    @SuppressWarnings("nls") CHERRY("Bois de rose", "Rosewood",  "/images/jpgWoodChart/120-wood-textures.jpg", 255, 255, 255, "", "", "", null, "Butcher_block.jpg"),
    @SuppressWarnings("nls") CHERRY_2("Cerisier 2", "Cherry 2",  "/images/jpgWoodChart/wood_cherry_2.jpg", 255, 255, 255, "", "", "", null, "Cherry_2.jpg"),
    @SuppressWarnings("nls") DOUGLAS("Douglas", "Douglas",  "/images/jpgWoodChart/wood_douglas.jpg", 255, 255, 255, "", "", "", null, "Douglas.jpg"),
    @SuppressWarnings("nls") HICKORY("Hickory", "Hickory",  "/images/jpgWoodChart/wood_hickory.jpg", 255, 255, 255, "", "", "", null, "Hickory.jpg"),
    @SuppressWarnings("nls") OAK("Chene", "Oak",  "/images/jpgWoodChart/wood_oak.jpg", 255, 255, 255, "", "", "", null, "Oak.jpg"),
    @SuppressWarnings("nls") OAK_2("Chene 2", "Oak 2",  "/images/jpgWoodChart/wood_oak_2.jpg", 255, 255, 255, "", "", "", null, "Oak_2.jpg"),
    @SuppressWarnings("nls") PINE("Pin", "Pine",  "/images/jpgWoodChart/wood_pine.jpg", 255, 255, 255, "", "", "", null, "Pine.jpg"),
    @SuppressWarnings("nls") PINE_2("Pin 2", "Pine 2",  "/images/jpgWoodChart/wood_pine_2.jpg", 255, 255, 255, "", "", "", null, "Pine_2.jpg"),
    @SuppressWarnings("nls") RED_CHERRY("Cerisier rouge", "Red cherry",  "/images/jpgWoodChart/wood_red_cherry.jpg", 255, 255, 255, "", "", "", null, "Red_cherry.jpg");

    public String label_fr;
    public String label_en;
    public String image;
    public Integer red;
    public Integer green;
    public Integer blue;
    public String colorStd;
    public String colorNStd;
    public String validProvider;
    public Boolean flagDyn;
    public String textureFileName;
    
    ChoiceStandardColor(String label_fr, String label_en, String image, Integer red, Integer green, Integer blue, String colorStd, String colorNStd, String validProvider, Boolean flagDyn, String textureFileName)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.colorStd = colorStd;
        this.colorNStd = colorNStd;
        this.validProvider = validProvider;
        this.flagDyn = flagDyn;
        this.textureFileName = textureFileName;
    }

}

