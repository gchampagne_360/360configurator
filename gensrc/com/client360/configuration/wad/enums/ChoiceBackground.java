// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 


public enum ChoiceBackground
{
    @SuppressWarnings("nls") HOUSE1("Aucun d�cor", "",  "", "", "", "", ""),
    @SuppressWarnings("nls") HOUSE2("Maison Style A", "",  "", "/images/maison2.jpg", "/images/fences.png", "/images/concrete.jpg", "/images/grass.jpg");

    public String label_fr;
    public String label_en;
    public String image;
    public String path;
    public String pathFences;
    public String pathConcrete;
    public String pathGrass;
    
    ChoiceBackground(String label_fr, String label_en, String image, String path, String pathFences, String pathConcrete, String pathGrass)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
        this.path = path;
        this.pathFences = pathFences;
        this.pathConcrete = pathConcrete;
        this.pathGrass = pathGrass;
    }

}

