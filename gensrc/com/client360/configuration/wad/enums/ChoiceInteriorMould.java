// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 


public enum ChoiceInteriorMould
{
    @SuppressWarnings("nls") J12("J 1/2", "J 1/2",  ""),
    @SuppressWarnings("nls") J34("J 3/4", "J 3/4",  "");

    public String label_fr;
    public String label_en;
    public String image;
    
    ChoiceInteriorMould(String label_fr, String label_en, String image)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
    }

}

