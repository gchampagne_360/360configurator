// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 


public enum ChoiceOperator
{
    @SuppressWarnings("nls") DYAD("Opérateur Dyad", "Dyad operator",  ""),
    @SuppressWarnings("nls") AWNING("Opérateur Auvent", "Awning operator",  ""),
    @SuppressWarnings("nls") DUAL("Opérateur Dual Dyad", "Dual Dyad operator",  "");

    public String label_fr;
    public String label_en;
    public String image;
    
    ChoiceOperator(String label_fr, String label_en, String image)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
    }

}

