// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 


public enum ChoicePaintingSide
{
    @SuppressWarnings("nls") NONE("Aucune", "None",  ""),
    @SuppressWarnings("nls") FRONT("Avant", "Front",  ""),
    @SuppressWarnings("nls") BACK("Arri�re", "Back",  ""),
    @SuppressWarnings("nls") BOTH("Avant & Arri�re", "Front & Back",  "");

    public String label_fr;
    public String label_en;
    public String image;
    
    ChoicePaintingSide(String label_fr, String label_en, String image)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
    }

}

