// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 


public enum ChoiceScreenType
{
    @SuppressWarnings("nls") HALF("Demi moustiquaire", "Half screen",  ""),
    @SuppressWarnings("nls") COMPLET("Moustiquaire complet", "Complet Screen",  "");

    public String label_fr;
    public String label_en;
    public String image;
    
    ChoiceScreenType(String label_fr, String label_en, String image)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
    }

}

