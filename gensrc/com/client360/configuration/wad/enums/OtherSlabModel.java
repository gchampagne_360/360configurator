// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 


public enum OtherSlabModel
{
	@SuppressWarnings("nls")
	SLAB1("Slab avec 1 �l�ment", "", "", com.client360.configuration.wad.doormodel.Slab1SubModel.class),
 @SuppressWarnings("nls")
	SLAB2("Slab avec 2 �l�ments", "", "", com.client360.configuration.wad.doormodel.Slab2SubModel.class);

    public String label_fr;
    public String label_en;
    public String image;
    public Class slabClass;
    
    OtherSlabModel(String label_fr, String label_en, String image, Class slabClass)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
        this.slabClass = slabClass;
    }

}

