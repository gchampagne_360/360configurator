// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 


public enum ChoiceColorType
{
    @SuppressWarnings("nls") RAL("RAL", "RAL",  ""),
    @SuppressWarnings("nls") STANDARD("Standard", "Standard",  ""),
    @SuppressWarnings("nls") SPECIAL("Sp�ciale", "Sp�cial",  ""),
    @SuppressWarnings("nls") WOOD("Bois", "Wood",  "");

    public String label_fr;
    public String label_en;
    public String image;
    
    ChoiceColorType(String label_fr, String label_en, String image)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
    }

}

