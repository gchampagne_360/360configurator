// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 


public enum ChoiceMilanoSashStyle
{
    @SuppressWarnings("nls") STYLISED("Volet stylis�", "Stylized sash",  ""),
    @SuppressWarnings("nls") FLAT("Volet carr�", "Squared sash",  "");

    public String label_fr;
    public String label_en;
    public String image;
    
    ChoiceMilanoSashStyle(String label_fr, String label_en, String image)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
    }

}

