// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 


public enum ChoiceHungFrame
{
    @SuppressWarnings("nls") F_8007("Cadre 4\" 1/2 - Guillotine simple", "4\" 1/2 Frame - Single hung",  ""),
    @SuppressWarnings("nls") F_8006("Cadre 4\" 1/2 - Guillotine double", "4\" 1/2 Frame - Double hung",  ""),
    @SuppressWarnings("nls") F_8057("Cadre hybride - Guillotine Simple / Double", "Hybrid Frame - Single / Double Hung",  "");

    public String label_fr;
    public String label_en;
    public String image;
    
    ChoiceHungFrame(String label_fr, String label_en, String image)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
    }

}

