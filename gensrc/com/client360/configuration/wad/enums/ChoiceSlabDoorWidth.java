// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 
import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.commonutils.math.Units;


public enum ChoiceSlabDoorWidth
{
    @SuppressWarnings("nls") SLAB30("30\"", "30\"",  "", Units.inch(29.75d)),
    @SuppressWarnings("nls") SLAB32("32\"", "32\"",  "", Units.inch(31.75d)),
    @SuppressWarnings("nls") SLAB34("34\"", "34\"",  "", Units.inch(33.75d)),
    @SuppressWarnings("nls") SLAB36("36\"", "36\"",  "", Units.inch(35.75d));

    public String label_fr;
    public String label_en;
    public String image;
    public Measure<Length> width;
    
    ChoiceSlabDoorWidth(String label_fr, String label_en, String image, Measure<Length> width)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
        this.width = width;
    }

}

