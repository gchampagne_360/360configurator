// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 
import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.commonutils.math.Units;


public enum ChoiceFrameDepth
{
    @SuppressWarnings("nls") F5("Cadre 5\"", "5\" frame",  "", Units.inch(5.0)),
    @SuppressWarnings("nls") F2_78("Cadre 2 7/8\"", "Frame 2 7/8\"",  "", Units.inch(2.875)),
    @SuppressWarnings("nls") F412("Cadre 4 1/2\"", "4 1/2\" Frame",  "", Units.inch(4.5));

    public String label_fr;
    public String label_en;
    public String image;
    public Measure<Length> thickness;
    
    ChoiceFrameDepth(String label_fr, String label_en, String image, Measure<Length> thickness)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
        this.thickness = thickness;
    }

}

