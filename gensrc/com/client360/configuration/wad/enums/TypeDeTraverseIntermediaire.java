// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 
import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.commonutils.math.Units;


public enum TypeDeTraverseIntermediaire
{
    @SuppressWarnings("nls") TRAVERSE75("Traverse inter. de 75 mm", "",  "", Units.mm(75), 78, 55, 55),
    @SuppressWarnings("nls") TRAVERSE91("Traverse inter. de 91 mm", "",  "", Units.mm(91), 94, 55, 55),
    @SuppressWarnings("nls") TRAVERSE111("Traverse inter. de 111 mm", "",  "", Units.mm(111), 114, 55, 55),
    @SuppressWarnings("nls") TRAVERSE114("Traverse inter. de 114 mm", "",  "", Units.mm(114), 117, 55, 55),
    @SuppressWarnings("nls") TRAVERSE123("Traverse inter. de 123 mm", "",  "", Units.mm(123), 128, 55, 55),
    @SuppressWarnings("nls") TRAVERSE125("Traverse inter. de 125 mm", "",  "", Units.mm(125), 128, 55, 55),
    @SuppressWarnings("nls") TRAVERSE153("Traverse inter. de 153 mm", "",  "", Units.mm(153), 158, 55, 55),
    @SuppressWarnings("nls") TRAVERSE155("Traverse inter. de 155 mm", "",  "", Units.mm(155), 158, 55, 55),
    @SuppressWarnings("nls") TRAVERSE173("Traverse inter. de 173 mm", "",  "", Units.mm(173), 176, 55, 55),
    @SuppressWarnings("nls") TRAVERSE194("Traverse inter. de 194 mm", "",  "", Units.mm(194), 201, 55, 55),
    @SuppressWarnings("nls") TRAVERSE196("Traverse inter. de 196 mm", "",  "", Units.mm(196), 201, 55, 55),
    @SuppressWarnings("nls") TRAVERSE198("Traverse inter. de 198 mm", "",  "", Units.mm(198), 201, 55, 55);

    public String label_fr;
    public String label_en;
    public String image;
    public Measure<Length> larTraItr;
    public Integer larTraItrDeb;
    public Integer epaTraItrFin;
    public Integer epaTraItrDeb;
    
    TypeDeTraverseIntermediaire(String label_fr, String label_en, String image, Measure<Length> larTraItr, Integer larTraItrDeb, Integer epaTraItrFin, Integer epaTraItrDeb)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
        this.larTraItr = larTraItr;
        this.larTraItrDeb = larTraItrDeb;
        this.epaTraItrFin = epaTraItrFin;
        this.epaTraItrDeb = epaTraItrDeb;
    }

}

