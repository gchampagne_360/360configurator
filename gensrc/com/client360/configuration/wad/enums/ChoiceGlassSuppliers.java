// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 


public enum ChoiceGlassSuppliers
{
    @SuppressWarnings("nls") FENERGIC("Fenergic", "Fenergic",  ""),
    @SuppressWarnings("nls") OTHER("Other", "Autre",  "");

    public String label_fr;
    public String label_en;
    public String image;
    
    ChoiceGlassSuppliers(String label_fr, String label_en, String image)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
    }

}

