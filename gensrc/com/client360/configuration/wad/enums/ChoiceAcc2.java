// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 


public enum ChoiceAcc2
{
	@SuppressWarnings("nls")
	ACCA("A", "", "", "com.exemple.doormodel.SubModel.class", com.client360.configuration.wad.doormodel.ACC2.class);

    public String label_fr;
    public String label_en;
    public String image;
    public String accClass;
    public Class spaceClass;
    
    ChoiceAcc2(String label_fr, String label_en, String image, String accClass, Class spaceClass)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
        this.accClass = accClass;
        this.spaceClass = spaceClass;
    }

}

