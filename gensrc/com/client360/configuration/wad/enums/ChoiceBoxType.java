// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 


public enum ChoiceBoxType
{
    @SuppressWarnings("nls") STD658("Cadre 6 5/8\"", "6 5/8\" Frame",  ""),
    @SuppressWarnings("nls") STD714("Cadre 7 1/4\" ", "7 1/4\" Frame",  "");

    public String label_fr;
    public String label_en;
    public String image;
    
    ChoiceBoxType(String label_fr, String label_en, String image)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
    }

}

