// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 
import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.commonutils.math.Units;


public enum ChoiceExteriorMould
{
    @SuppressWarnings("nls") MOUL_A("TYPE A", "TYPE A",  "", Units.inch(0.375)),
    @SuppressWarnings("nls") MOUL_B("TYPE B", "TYPE B",  "", Units.inch(0.625)),
    @SuppressWarnings("nls") E784("E784", "E784",  "", Units.inch(0.5)),
    @SuppressWarnings("nls") T116("T116", "T116",  "", Units.inch(1.25)),
    @SuppressWarnings("nls") T117("T117", "T117",  "", Units.inch(2.3125)),
    @SuppressWarnings("nls") T124("T124", "T124",  "", Units.inch(2.0)),
    @SuppressWarnings("nls") T128("T128", "T128",  "", Units.inch(2.3125)),
    @SuppressWarnings("nls") E308("E308", "E308",  "", Units.inch(1.1875)),
    @SuppressWarnings("nls") M4551("4551", "4551",  "", Units.inch(1.375)),
    @SuppressWarnings("nls") M4552("4552", "4552",  "", Units.inch(1.25)),
    @SuppressWarnings("nls") M4645("4645", "4645",  "", Units.inch(2.0));

    public String label_fr;
    public String label_en;
    public String image;
    public Measure<Length> depth;
    
    ChoiceExteriorMould(String label_fr, String label_en, String image, Measure<Length> depth)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
        this.depth = depth;
    }

}

