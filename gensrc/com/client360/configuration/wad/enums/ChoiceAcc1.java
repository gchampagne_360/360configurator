// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 

public enum ChoiceAcc1
{
	@SuppressWarnings("nls")
	ACCA("A", "", "", "", "", "", "", "", "com.client360.configuration.wad.doormodel.SubModel", com.client360.configuration.wad.doormodel.ACC1.class), @SuppressWarnings("nls")
	ACCB("B", "", "", "", "", "", "", "", "com.client360.configuration.wad.doormodel.SubModel", com.client360.configuration.wad.doormodel.ACC3.class);

	public String label_fr;
	public String label_en;
	public String defaultChoice;
	public String enabled;
	public String messageEnabled_fr;
	public String messageEnabled_en;
	public String price;
	public String image;
	public String accClass;
	public Class spaceClass;

	ChoiceAcc1(String label_fr, String label_en, String defaultChoice, String enabled, String messageEnabled_fr, String messageEnabled_en, String price,
			String image, String accClass, Class spaceClass)
	{
		this.label_fr = label_fr;
		this.label_en = label_en;
		this.defaultChoice = defaultChoice;
		this.enabled = enabled;
		this.messageEnabled_fr = messageEnabled_fr;
		this.messageEnabled_en = messageEnabled_en;
		this.price = price;
		this.image = image;
		this.accClass = accClass;
		this.spaceClass = spaceClass;
	}

}
