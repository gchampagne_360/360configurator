// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 


public enum ChoiceWoodTone
{
    @SuppressWarnings("nls") RAL5003("RAL5003", "RAL5003",  "", 144, 86, 26, 20F),
    @SuppressWarnings("nls") RAL5004("RAL5004", "RAL5004",  "", 89, 144, 86, 25F),
    @SuppressWarnings("nls") RAL5005("RAL5005", "RAL5005",  "", 33, 0, 211, 30F),
    @SuppressWarnings("nls") NONE("Aucun", "None",  "", 255, 255, 255, 0F);

    public String label_fr;
    public String label_en;
    public String image;
    public Integer blue;
    public Integer green;
    public Integer red;
    public Float alpha;
    
    ChoiceWoodTone(String label_fr, String label_en, String image, Integer blue, Integer green, Integer red, Float alpha)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
        this.blue = blue;
        this.green = green;
        this.red = red;
        this.alpha = alpha;
    }

}

