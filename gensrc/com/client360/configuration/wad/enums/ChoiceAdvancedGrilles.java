// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 


public enum ChoiceAdvancedGrilles
{
	@SuppressWarnings("nls")
	PETITSBOISENFOURCHES("Petits bois enfourch�s", "Muntins", "", com.client360.configuration.wad.QuestionnairePetitBoisEnfourche.class, false),
 @SuppressWarnings("nls")
	SOUSBASSEMENT("Sous-Bassement", "Kick panels", "", com.client360.configuration.wad.QuestionnaireSousBassement.class, false), @SuppressWarnings("nls")
	REGULAR("R�gulier", "Regular", "", com.client360.configuration.wad.RegularGrillesQuestionary.class, true), @SuppressWarnings("nls")
	SPECIAL("Exemple sp�cial", "Special example", "", com.client360.configuration.wad.SpecialQuestionary.class, false), @SuppressWarnings("nls")
	MUNTIN("Barrotins", "Muntins", "", com.client360.configuration.wad.MuntinQuestionary.class, false), @SuppressWarnings("nls")
	SPECIALMUNTIN("Barrotins sp�ciaux", "Special muntins", "", com.client360.configuration.wad.SpecialMuntinQuestionary.class, false),
    @SuppressWarnings("nls") CONTOUR("Contour", "Contour",  "", com.netappsid.wadconfigurator.ContourGrillesQuestionary.class, true),
    @SuppressWarnings("nls") PARTIALREGULAR("R�gulier partiel", "Parital square",  "", com.netappsid.wadconfigurator.PartialRegularGrillesQuestionary.class, true),
    @SuppressWarnings("nls") HUBSANDSPOKES("Soleil", "Hubs and spokes",  "", com.netappsid.wadconfigurator.HubsAndSpokesGrillesQuestionary.class, true),
    @SuppressWarnings("nls") PERIMETER("Perim�tre", "Perimeter",  "", com.netappsid.wadconfigurator.PerimeterGrillesQuestionary.class, true),
    @SuppressWarnings("nls") LADDER("Maille", "Ladder",  "", com.netappsid.wadconfigurator.LadderGrillesQuestionary.class, true),
    @SuppressWarnings("nls") EMPRESSPATTERN("Pattern imp�ratrice", "Empress pattern",  "", com.netappsid.wadconfigurator.EmpressPatternGrillesQuestionary.class, true),
    @SuppressWarnings("nls") PARTIALPERIMETER("Contour partiel", "Parital Perimeter",  "", com.netappsid.wadconfigurator.PartialPerimeterGrillesQuestionary.class, true),
    @SuppressWarnings("nls") PARTIALDIAMOND("Losange partiel", "Parital Diamond",  "", com.netappsid.wadconfigurator.PartialDiamondGrillesQuestionary.class, true);

    public String label_fr;
    public String label_en;
    public String image;
    public Class<? extends com.netappsid.wadconfigurator.AdvancedSectionGrilles> configClass;
    public Boolean glass;
    
    ChoiceAdvancedGrilles(String label_fr, String label_en, String image, Class<? extends com.netappsid.wadconfigurator.AdvancedSectionGrilles> configClass, Boolean glass)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
        this.configClass = configClass;
        this.glass = glass;
    }

}

