// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 


public enum ChoiceParametricModel
{
	@SuppressWarnings("nls")
	STDSLAB("Standard", "Standard", "", com.client360.configuration.wad.kickpanel.StandardParametricModel.class), @SuppressWarnings("nls")
	CUSTOMSLAB("Adapt�", "Custom", "", com.client360.configuration.wad.kickpanel.CustomParametricModel.class);

    public String label_fr;
    public String label_en;
    public String image;
    public Class<? extends com.netappsid.wadconfigurator.entranceDoor.ParametricModel> configClass;
    
    ChoiceParametricModel(String label_fr, String label_en, String image, Class<? extends com.netappsid.wadconfigurator.entranceDoor.ParametricModel> configClass)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
        this.configClass = configClass;
    }

}

