// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 


public enum TypeDePanneaux
{
	@SuppressWarnings("nls")
	MASSIF("Massif", "", "", com.client360.configuration.wad.doormodel.accessoires.CfpSt1PanRec.class), @SuppressWarnings("nls")
	POSTFORME("Postform�", "", "", com.client360.configuration.wad.doormodel.accessoires.CfpSt1PanRec.class), @SuppressWarnings("nls")
	RAINURELAMESVERTICALES("Rainur� lames verticales", "", "", com.client360.configuration.wad.doormodel.accessoires.PanneauMoucVrt.class), @SuppressWarnings("nls")
	RAINURELAMESHORIZONTALES("Rainur� lames horizontales", "", "", com.client360.configuration.wad.doormodel.accessoires.PanneauMoucHoz.class);

    public String label_fr;
    public String label_en;
    public String image;
    public Class<? extends com.netappsid.wadconfigurator.entranceDoor.ParametricModel> configClass;
    
    TypeDePanneaux(String label_fr, String label_en, String image, Class<? extends com.netappsid.wadconfigurator.entranceDoor.ParametricModel> configClass)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
        this.configClass = configClass;
    }

}

