// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 


public enum ChoiceStandarHinge
{
    @SuppressWarnings("nls") REGLAI("Penture r�guli�re laiton", "Regular Brass Hinge",  ""),
    @SuppressWarnings("nls") BILLAI("Penture � bille laiton", "Brass Ball Hinge",  "");

    public String label_fr;
    public String label_en;
    public String image;
    
    ChoiceStandarHinge(String label_fr, String label_en, String image)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
    }

}

