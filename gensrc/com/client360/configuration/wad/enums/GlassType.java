// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 


public enum GlassType
{
    @SuppressWarnings("nls") WITHSTAINEDGLASS("Vitrail", "Stained Glass",  ""),
    @SuppressWarnings("nls") WITHTHERMOS("Thermos", "Insulated Glass",  "");

    public String label_fr;
    public String label_en;
    public String image;
    
    GlassType(String label_fr, String label_en, String image)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
    }

}

