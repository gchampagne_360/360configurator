// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 


public enum ChoiceSlabModel
{
    @SuppressWarnings("nls") FLUSH("Uni", "Flush",  "/images/PA-Slabs/SlabFlush.jpg", "com.netappsid.wadconfigurator.doormodel.Flush"),
    @SuppressWarnings("nls") MODEL300("Mod�le 300", "300 Model",  "/images/PA-Slabs/Slab3PanelArchTop.jpg", "com.netappsid.wadconfigurator.doormodel.ScrollTopThreePanels"),
    @SuppressWarnings("nls") MODEL400("Mod�le 400", "400 Model",  "/images/PA-Slabs/Slab4PanelBlankTop.jpg", "com.netappsid.wadconfigurator.doormodel.FourPanels"),
    @SuppressWarnings("nls") MODEL900("Mod�le 900", "900 Model",  "/images/PA-Slabs/Slab9Panel.jpg", "com.netappsid.wadconfigurator.doormodel.NinePanels"),
    @SuppressWarnings("nls") MODEL600("Mod�le 600", "600 Model",  "/images/PA-Slabs/Slab6Panel.jpg", "com.netappsid.wadconfigurator.doormodel.SixPanels");

    public String label_fr;
    public String label_en;
    public String image;
    public String slabModelClass;
    
    ChoiceSlabModel(String label_fr, String label_en, String image, String slabModelClass)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
        this.slabModelClass = slabModelClass;
    }

}

