// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 


public enum Post
{
    @SuppressWarnings("nls") LEFT("Gauche Seulement", "Left only",  ""),
    @SuppressWarnings("nls") RIGHT("Droit seulement", "Right only",  ""),
    @SuppressWarnings("nls") BOTH("Deux bouts", "Both ends",  "");

    public String label_fr;
    public String label_en;
    public String image;
    
    Post(String label_fr, String label_en, String image)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
    }

}

