// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 
import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.commonutils.math.Units;


public enum Soubassement
{
    @SuppressWarnings("nls") SOUBASSEMNT400("Soubassement de 400 mm", "",  "", Units.mm(400), Units.mm(91), "SEUILALUOSCILLOCOULISSANT;", true, true),
    @SuppressWarnings("nls") SOUBASSEMNTHORSCOTE("Soubassement hors c�te", "",  "", Units.mm(0), Units.mm(91), "SEUIL;SEUILEXTRAPLAT;APPUISTANDARD;APPUIARRONDI;APPUIACHAPEAUTER;APPUITRAVERSE;SEUILALUOSCILLOCOULISSANT;AUCUN;", true, true),
    @SuppressWarnings("nls") PANORAMIQUE("Panoramique", "",  "", Units.mm(0), Units.mm(91), "SEUIL;SEUILEXTRAPLAT;APPUISTANDARD;APPUIARRONDI;APPUIACHAPEAUTER;APPUITRAVERSE;AUCUN;", false, true),
    @SuppressWarnings("nls") SOUBASSEMNT500("Soubassement de 500 mm", "",  "", Units.mm(500), Units.mm(91), "SEUILALUOSCILLOCOULISSANT;", true, true),
    @SuppressWarnings("nls") SOUBASSEMNT600("Soubassement de 600 mm", "",  "", Units.mm(600), Units.mm(91), "SEUILALUOSCILLOCOULISSANT;", true, true);

    public String label_fr;
    public String label_en;
    public String image;
    public Measure<Length> hauSbs;
    public Measure<Length> traverseBasse;
    public String typeAppuiSeuil;
    public Boolean panneaux;
    public Boolean traItr;
    
    Soubassement(String label_fr, String label_en, String image, Measure<Length> hauSbs, Measure<Length> traverseBasse, String typeAppuiSeuil, Boolean panneaux, Boolean traItr)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
        this.hauSbs = hauSbs;
        this.traverseBasse = traverseBasse;
        this.typeAppuiSeuil = typeAppuiSeuil;
        this.panneaux = panneaux;
        this.traItr = traItr;
    }

}

