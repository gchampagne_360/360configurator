// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 


public enum ChoiceHinge
{
    @SuppressWarnings("nls") ELLIPSE("Poign� �llipse", "Handle Ellipse",  ""),
    @SuppressWarnings("nls") PLIANTE("Poign�e pliante", "Retractile handle",  "");

    public String label_fr;
    public String label_en;
    public String image;
    
    ChoiceHinge(String label_fr, String label_en, String image)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
    }

}

