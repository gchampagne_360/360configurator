// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 


public enum ChoiceScreenDoor
{
    @SuppressWarnings("nls") FULLSCREEN("Vue totale (moustiquaire pleine grandeur)", "Full screen",  ""),
    @SuppressWarnings("nls") SCREEN1PANEL("Moustiquaire avec 1 panneau dans le bas", "Screen with 1 panel at the bottom",  ""),
    @SuppressWarnings("nls") SCREEN2PANEL("Moustiquaire avec 2 panneaux dans le bas", "Screen with 2 panel at the bottom",  ""),
    @SuppressWarnings("nls") SCREEN3PANEL("Moustiquaire avec 3 panneaux dans le bas", "Screen with 3 panel at the bottom",  "");

    public String label_fr;
    public String label_en;
    public String image;
    
    ChoiceScreenDoor(String label_fr, String label_en, String image)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
    }

}

