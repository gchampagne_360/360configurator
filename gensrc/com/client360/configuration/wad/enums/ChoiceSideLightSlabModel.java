// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 
import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.commonutils.math.Units;
import com.netappsid.wadconfigurator.doormodel.Flush;
import com.netappsid.wadconfigurator.doormodel.FourPanel;
import com.netappsid.wadconfigurator.doormodel.ThreePanelRaised;
import com.netappsid.wadconfigurator.sidelightmodel.FlushSidelite;


public enum ChoiceSideLightSlabModel
{
	@SuppressWarnings("nls")
	SL_21("(SL-21) Lat�ral vide", "(SL-21) Empty Sidelight", "/images/SlabModel/SlabSLFlush.jpg", Flush.class, "SL-21", Units.inch(15.5d), "SL-21"),
 @SuppressWarnings("nls")
	SL_24("(SL-24) Lat�ral, verre clair 8x64\"", "(SL-24) Sidelight, clear glass 8x64\"", "/images/SlabModel/SlabSLFlush.jpg", Flush.class, "SL-24", Units
			.inch(15.5d), "SL-21"), @SuppressWarnings("nls")
	SL_24C("(SL-24C) Lat�ral, verre carrelage 8x64\"", "(SL-24C) Sidelight, glass with grills 8x64\"", "/images/SlabModel/SlabSLFlush.jpg", Flush.class,
			"SL-24C", Units.inch(15.5d), "SL-21"), @SuppressWarnings("nls")
	SL_45("(SL-45)  Lat�ral, verre clair 8x36\"", "(SL-45)  Sidelight, clear glass 8x36\"", "/images/SlabModel/SlabSLFlush.jpg", ThreePanelRaised.class,
			"SL-45",
			Units.inch(15.5d), "SL-47"), @SuppressWarnings("nls")
	SL_45C("(SL-45C)  Lat�ral, verre carrelage 8x36\"", "(SL-45C)  Sidelight, glass with grills 8x36\"", "/images/SlabModel/SlabSLFlush.jpg",
			FlushSidelite.class,
			"SL-45C", Units.inch(15.5d), "SL-47"), @SuppressWarnings("nls")
	SL_47("(SL-47) Lat�ral, 3 emboss�s", "(SL-47) Sidelight, 3 Panels", "/images/SlabModel/SlabSL3Panel.jpg", FlushSidelite.class, "SL-47",
			Units.inch(15.5d),
			"SL-47"), @SuppressWarnings("nls")
	SL_72("(SL-72)  Lat�ral, verre clair 8x48\"", "(SL-72)  Sidelight, clear glass 8x48\"", "/images/SlabModel/SlabSL3Panel.jpg", FourPanel.class, "SL-72",
			Units.inch(15.5d), "SL-70"), @SuppressWarnings("nls")
	SL_72C("(SL-72C)  Lat�ral, verre carrelage 8x48\"", "(SL-72C)  Sidelight, glass with grills 8x48\"", "/images/SlabModel/SlabSL3Panel.jpg", FourPanel.class,
			"SL-72C", Units.inch(15.5d), "SL-70"), @SuppressWarnings("nls")
	SL_12("(SL-12) Lat�ral, plein verre", "(SL-12) Sidelight, full glass", "/images/SlabModel/SlabSL3Panel.jpg", Flush.class, "SL-12", Units.inch(15.5d),
			"SL-12"), @SuppressWarnings("nls")
	SL_12C("(SL-12C) Lat�ral, plein verre carrelage", "(SL-12C) Sidelight, full glass with grills", "/images/SlabModel/SlabSL3Panel.jpg", Flush.class,
			"SL-12C", Units.inch(15.5d), "SL-12C"), @SuppressWarnings("nls")
	SL_14("(SL-14) Lat�ral, plein verre + panneau fran�ais", "(SL-14) Sidelight, full glass + french panel", "/images/SlabModel/SlabSL3Panel.jpg", Flush.class,
			"SL-14", Units.inch(15.5d), "SL-14"), @SuppressWarnings("nls")
	SL_14C("(SL-14C) Lat�ral, plein verre carrelage + panneau fran�ais", "(SL-12C) Sidelight, full glass with grills + french panel",
			"/images/SlabModel/SlabSL3Panel.jpg", Flush.class, "SL-14C", Units.inch(15.5d), "SL-14C");

    public String label_fr;
    public String label_en;
    public String image;
    public Class slabModelClass;
    public String modelCode;
    public Measure<Length> width;
    public String baseModelCode;
    
    ChoiceSideLightSlabModel(String label_fr, String label_en, String image, Class slabModelClass, String modelCode, Measure<Length> width, String baseModelCode)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
        this.slabModelClass = slabModelClass;
        this.modelCode = modelCode;
        this.width = width;
        this.baseModelCode = baseModelCode;
    }

}

