// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 
import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.commonutils.math.Units;


public enum ChoiceSlabSidelightWidth
{
    @SuppressWarnings("nls") SIDELIGHT1512("15\" 1/2", "15\" 1/2",  "", Units.inch(15.5d));

    public String label_fr;
    public String label_en;
    public String image;
    public Measure<Length> width;
    
    ChoiceSlabSidelightWidth(String label_fr, String label_en, String image, Measure<Length> width)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
        this.width = width;
    }

}

