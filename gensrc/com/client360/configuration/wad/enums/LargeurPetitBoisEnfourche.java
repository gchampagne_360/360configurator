// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 
import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.commonutils.math.Units;


public enum LargeurPetitBoisEnfourche
{
    @SuppressWarnings("nls") EPAISSEUR30MM("30 mm", "",  "", Units.mm(30.0)),
    @SuppressWarnings("nls") EPAISSEUR50MM("50 mm", "",  "", Units.mm(50.0)),
    @SuppressWarnings("nls") EPAISSEUR60MM("60 mm", "",  "", Units.mm(60.0));

    public String label_fr;
    public String label_en;
    public String image;
    public Measure<Length> valeur;
    
    LargeurPetitBoisEnfourche(String label_fr, String label_en, String image, Measure<Length> valeur)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
        this.valeur = valeur;
    }

}

