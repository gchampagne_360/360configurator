// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 


public enum ChoiceTypeBlow
{
    @SuppressWarnings("nls") PINCLAIR("PIN CLAIR", "CLEAR PINE",  ""),
    @SuppressWarnings("nls") PIN("PIN", "PINE",  ""),
    @SuppressWarnings("nls") RECOUVERTPVC("RECOUVERT PVC", "PVC  COVERED",  "");

    public String label_fr;
    public String label_en;
    public String image;
    
    ChoiceTypeBlow(String label_fr, String label_en, String image)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
    }

}

