// generated class, do not edit

package com.client360.configuration.wad.enums;

// Imports 


public enum ChoiceSlabDoorModel
{
    @SuppressWarnings("nls") PA201("(PA-201) Battant vide", "(PA-201) Empty slab",  "", "PA-201", "PA-201"),
    @SuppressWarnings("nls") PA204("(PA-204) Battant, Verre clair 22x64\"", "(PA-204) Slab, Clear glass 22x64\"",  "", "PA-204", "PA-201"),
    @SuppressWarnings("nls") PA204C("(PA-204C) Battant, Verre carrelage 22x64\"", "(PA-204C) Slab, Glass with grills 22x64\"",  "", "PA-204C", "PA-201"),
    @SuppressWarnings("nls") PA205("(PA-205) Battant, Guillotine 22x64\"", "(PA-205) Slab, Hung 22x64\"",  "", "PA-205", "PA-201"),
    @SuppressWarnings("nls") PA205C("(PA-205C) Battant, Guillotine � carrelage 22x64\"", "(PA-205C) Slab, Hung with grills 22x64\"",  "", "PA-205C", "PA-201"),
    @SuppressWarnings("nls") PA400("(PA-400) Battant, Verre clair 22x36\"", "(PA-400) Slab, Hung 22x36\"",  "", "PA-400", "PA-407"),
    @SuppressWarnings("nls") PA400C("(PA-400C) Battant, Guillotine � carrelage 22x36\"", "(PA-400C) Slab,  Hung with grills 22x36\"",  "", "PA-400C", "PA-407"),
    @SuppressWarnings("nls") PA401("(PA-401) Battant, Verre clair 22x36\"", "(PA-401) Slab, Clear glass 22x36\"",  "", "PA-401", "PA-407"),
    @SuppressWarnings("nls") PA401C("(PA-401C) Battant, Verre carrelage 22x36\"", "(PA-401C) Slab, Glass with grills 22x36\"",  "", "PA-401C", "PA-407"),
    @SuppressWarnings("nls") PA405("(PA-405) Battant, Verre clair 8x36\" (double)", "(PA-405) Slab, Clear glass 8x36\" (double)",  "", "PA-405", "PA-407"),
    @SuppressWarnings("nls") PA405C("(PA-405C) Battant, Verre carrelage 8x36\" (double)", "(PA-405C) Slab, Glass with grills 22x36\" (double)",  "", "PA-405C", "PA-407"),
    @SuppressWarnings("nls") PA407("(PA-407C) Battant, 6 panneaux", "(PA-407C) Slab, 6 panels",  "", "PA-407C", "PA-407"),
    @SuppressWarnings("nls") PA409("(PA-409) Battant, 6 emboss�s + Demi-lune", "(PA-409) Slab, 6 panels + Half-moon",  "", "PA-409", "PA-407"),
    @SuppressWarnings("nls") PA411("(PA-411) Battant, 4 emboss�s + Demi-lune", "(PA-411) Slab, 4 panels + Half-moon",  "", "PA-411", "PA-407"),
    @SuppressWarnings("nls") PA702("(PA-702) Battant, Verre clair 22x48\"", "(PA-702) Slab, Clear glass 22x48\"",  "", "PA-702", "PA-700"),
    @SuppressWarnings("nls") PA702C("(PA-702C) Battant, Verre carrelage 22x48\"", "(PA-702C) Slab, Glass with grills 22x48\"",  "", "PA-702C", "PA-700"),
    @SuppressWarnings("nls") PA703("(PA-703) Battant, Guillotine 22x64\"", "(PA-703) Slab, Hung 22x64\"",  "", "PA-703", "PA-700"),
    @SuppressWarnings("nls") PA703C("(PA-703C) Battant, Guillotine � carrelage 22x64\"", "(PA-703C) Slab, Hung with grills 22x64\"",  "", "PA-703C", "PA-700"),
    @SuppressWarnings("nls") PA1100("(PA-1100) Battant, 2 panneaux au bas", "(PA-1100) Slab, 2 bottom panels",  "", "PA-1100", "PA-700");

    public String label_fr;
    public String label_en;
    public String image;
    public String modelCode;
    public String baseModelCode;
    
    ChoiceSlabDoorModel(String label_fr, String label_en, String image, String modelCode, String baseModelCode)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
        this.modelCode = modelCode;
        this.baseModelCode = baseModelCode;
    }

}

