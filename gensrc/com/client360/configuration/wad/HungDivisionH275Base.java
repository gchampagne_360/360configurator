// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 


public abstract class HungDivisionH275Base extends com.client360.configuration.wad.HungMainDivision
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(HungDivisionH275Base.class);

    // attributes

    // Bound properties


    // Constructors

    public HungDivisionH275Base(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.HungDivisionH275"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations



    @Override
    protected String getSequences()
    {
        return "sectionInput,choicePresetWindowWidth,energyStar,installed,nbSections,qDQ,sameGrilles,showMeetingRail,meetingRail,showMullion,windowsMullion,paintable,renderingNumber,dimension,section,sash,division,ExteriorColor,InteriorColor";  
    }
}
