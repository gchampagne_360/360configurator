// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 


public abstract class PatioSashFusionBase extends com.client360.configuration.wad.PatioSash
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(PatioSashFusionBase.class);

    // attributes

    // Bound properties


    // Constructors

    public PatioSashFusionBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.PatioSashFusion"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations



    @Override
    protected String getSequences()
    {
        return "FrenchPanelActive,FrenchPanelHeight,frenchPanel,installed,sashSection,screen,choiceSashType,dimension,ExteriorColor,InteriorColor,choiceExteriorInterior,rail,profiles,glazingBeadProfiles";  
    }
}
