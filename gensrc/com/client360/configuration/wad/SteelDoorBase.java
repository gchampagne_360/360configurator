// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.client360.configuration.wad.enums.ChoiceBoxType;
import com.client360.configuration.wad.enums.ChoiceIllusionOpening;
import com.client360.configuration.wad.enums.ChoicePaintingSide;
import com.client360.configuration.wad.enums.ChoiceSlabDoorWidth;
import com.client360.configuration.wad.enums.ChoiceSlabSidelightWidth;
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class SteelDoorBase extends com.netappsid.wadconfigurator.EntranceDoor
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(SteelDoorBase.class);

    // attributes
    private ValueContainer<ChoiceSlabSidelightWidth> choiceSlabSidelightWidth = new ValueContainer<ChoiceSlabSidelightWidth>(this);
    private ValueContainer<ChoiceSlabDoorWidth> choiceSlabDoorWidth = new ValueContainer<ChoiceSlabDoorWidth>(this);
    private ValueContainer<ChoiceStandardDimension> choiceStandardDimension = new ValueContainer<ChoiceStandardDimension>(this);
    private ValueContainer<ChoicePaintingSide> choicePaintingSide = new ValueContainer<ChoicePaintingSide>(this);
    private ValueContainer<ChoiceBoxType> choiceBoxType = new ValueContainer<ChoiceBoxType>(this);
    private ValueContainer<ChoiceIllusionOpening> choiceIllusionOpening = new ValueContainer<ChoiceIllusionOpening>(this);
    private ValueContainer<SteelDoorOptions> steelDoorOptions = new ValueContainer<SteelDoorOptions>(this);

    // Bound properties

    public static final String PROPERTYNAME_CHOICESLABSIDELIGHTWIDTH = "choiceSlabSidelightWidth";  
    public static final String PROPERTYNAME_CHOICESLABDOORWIDTH = "choiceSlabDoorWidth";  
    public static final String PROPERTYNAME_CHOICESTANDARDDIMENSION = "choiceStandardDimension";  
    public static final String PROPERTYNAME_CHOICEPAINTINGSIDE = "choicePaintingSide";  
    public static final String PROPERTYNAME_CHOICEBOXTYPE = "choiceBoxType";  
    public static final String PROPERTYNAME_CHOICEILLUSIONOPENING = "choiceIllusionOpening";  
    public static final String PROPERTYNAME_STEELDOOROPTIONS = "steelDoorOptions";  

    // Constructors

    public SteelDoorBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.SteelDoor"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Sideligth width")
        ,@LocalizedTag(language="fr", name="label", value="Largeur Lat�raux")
    })
    public final ChoiceSlabSidelightWidth getChoiceSlabSidelightWidth()
    {
         return this.choiceSlabSidelightWidth.getValue();
    }

    public final void setChoiceSlabSidelightWidth(ChoiceSlabSidelightWidth choiceSlabSidelightWidth)
    {

        ChoiceSlabSidelightWidth oldValue = this.choiceSlabSidelightWidth.getValue();
        this.choiceSlabSidelightWidth.setValue(choiceSlabSidelightWidth);

        ChoiceSlabSidelightWidth currentValue = this.choiceSlabSidelightWidth.getValue();

        fireChoiceSlabSidelightWidthChange(currentValue, oldValue);
    }
    public final ChoiceSlabSidelightWidth removeChoiceSlabSidelightWidth()
    {
        ChoiceSlabSidelightWidth oldValue = this.choiceSlabSidelightWidth.getValue();
        ChoiceSlabSidelightWidth removedValue = this.choiceSlabSidelightWidth.removeValue();
        ChoiceSlabSidelightWidth currentValue = this.choiceSlabSidelightWidth.getValue();

        fireChoiceSlabSidelightWidthChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceSlabSidelightWidth(ChoiceSlabSidelightWidth choiceSlabSidelightWidth)
    {
        ChoiceSlabSidelightWidth oldValue = this.choiceSlabSidelightWidth.getValue();
        this.choiceSlabSidelightWidth.setDefaultValue(choiceSlabSidelightWidth);

        ChoiceSlabSidelightWidth currentValue = this.choiceSlabSidelightWidth.getValue();

        fireChoiceSlabSidelightWidthChange(currentValue, oldValue);
    }
    
    public final ChoiceSlabSidelightWidth removeDefaultChoiceSlabSidelightWidth()
    {
        ChoiceSlabSidelightWidth oldValue = this.choiceSlabSidelightWidth.getValue();
        ChoiceSlabSidelightWidth removedValue = this.choiceSlabSidelightWidth.removeDefaultValue();
        ChoiceSlabSidelightWidth currentValue = this.choiceSlabSidelightWidth.getValue();

        fireChoiceSlabSidelightWidthChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceSlabSidelightWidth> getChoiceSlabSidelightWidthValueContainer()
    {
        return this.choiceSlabSidelightWidth;
    }
    public final void fireChoiceSlabSidelightWidthChange(ChoiceSlabSidelightWidth currentValue, ChoiceSlabSidelightWidth oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceSlabSidelightWidthChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICESLABSIDELIGHTWIDTH, oldValue, currentValue);
            afterChoiceSlabSidelightWidthChanged(currentValue);
        }
    }

    public void beforeChoiceSlabSidelightWidthChanged( ChoiceSlabSidelightWidth choiceSlabSidelightWidth)
    { }
    public void afterChoiceSlabSidelightWidthChanged( ChoiceSlabSidelightWidth choiceSlabSidelightWidth)
    { }


    public boolean isChoiceSlabSidelightWidthMandatory()
    {
        return true;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Slab width")
        ,@LocalizedTag(language="fr", name="label", value="Largeur Slabs")
    })
    public final ChoiceSlabDoorWidth getChoiceSlabDoorWidth()
    {
         return this.choiceSlabDoorWidth.getValue();
    }

    public final void setChoiceSlabDoorWidth(ChoiceSlabDoorWidth choiceSlabDoorWidth)
    {

        ChoiceSlabDoorWidth oldValue = this.choiceSlabDoorWidth.getValue();
        this.choiceSlabDoorWidth.setValue(choiceSlabDoorWidth);

        ChoiceSlabDoorWidth currentValue = this.choiceSlabDoorWidth.getValue();

        fireChoiceSlabDoorWidthChange(currentValue, oldValue);
    }
    public final ChoiceSlabDoorWidth removeChoiceSlabDoorWidth()
    {
        ChoiceSlabDoorWidth oldValue = this.choiceSlabDoorWidth.getValue();
        ChoiceSlabDoorWidth removedValue = this.choiceSlabDoorWidth.removeValue();
        ChoiceSlabDoorWidth currentValue = this.choiceSlabDoorWidth.getValue();

        fireChoiceSlabDoorWidthChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceSlabDoorWidth(ChoiceSlabDoorWidth choiceSlabDoorWidth)
    {
        ChoiceSlabDoorWidth oldValue = this.choiceSlabDoorWidth.getValue();
        this.choiceSlabDoorWidth.setDefaultValue(choiceSlabDoorWidth);

        ChoiceSlabDoorWidth currentValue = this.choiceSlabDoorWidth.getValue();

        fireChoiceSlabDoorWidthChange(currentValue, oldValue);
    }
    
    public final ChoiceSlabDoorWidth removeDefaultChoiceSlabDoorWidth()
    {
        ChoiceSlabDoorWidth oldValue = this.choiceSlabDoorWidth.getValue();
        ChoiceSlabDoorWidth removedValue = this.choiceSlabDoorWidth.removeDefaultValue();
        ChoiceSlabDoorWidth currentValue = this.choiceSlabDoorWidth.getValue();

        fireChoiceSlabDoorWidthChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceSlabDoorWidth> getChoiceSlabDoorWidthValueContainer()
    {
        return this.choiceSlabDoorWidth;
    }
    public final void fireChoiceSlabDoorWidthChange(ChoiceSlabDoorWidth currentValue, ChoiceSlabDoorWidth oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceSlabDoorWidthChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICESLABDOORWIDTH, oldValue, currentValue);
            afterChoiceSlabDoorWidthChanged(currentValue);
        }
    }

    public void beforeChoiceSlabDoorWidthChanged( ChoiceSlabDoorWidth choiceSlabDoorWidth)
    { }
    public void afterChoiceSlabDoorWidthChanged( ChoiceSlabDoorWidth choiceSlabDoorWidth)
    { }


    public boolean isChoiceSlabDoorWidthMandatory()
    {
        return true;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Standard Dimensions")
        ,@LocalizedTag(language="fr", name="label", value="Dimensions Standard")
    })
    public final ChoiceStandardDimension getChoiceStandardDimension()
    {
         return this.choiceStandardDimension.getValue();
    }

    public final void setChoiceStandardDimension(ChoiceStandardDimension choiceStandardDimension)
    {

        if (choiceStandardDimension != null)
        {
            acquire(choiceStandardDimension, "choiceStandardDimension");
        }
        ChoiceStandardDimension oldValue = this.choiceStandardDimension.getValue();
        this.choiceStandardDimension.setValue(choiceStandardDimension);

        ChoiceStandardDimension currentValue = this.choiceStandardDimension.getValue();

        fireChoiceStandardDimensionChange(currentValue, oldValue);
    }
    public final ChoiceStandardDimension removeChoiceStandardDimension()
    {
        ChoiceStandardDimension oldValue = this.choiceStandardDimension.getValue();
        ChoiceStandardDimension removedValue = this.choiceStandardDimension.removeValue();
        ChoiceStandardDimension currentValue = this.choiceStandardDimension.getValue();

        fireChoiceStandardDimensionChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceStandardDimension(ChoiceStandardDimension choiceStandardDimension)
    {
        if (choiceStandardDimension != null)
        {
            acquire(choiceStandardDimension, "choiceStandardDimension");
        }
        ChoiceStandardDimension oldValue = this.choiceStandardDimension.getValue();
        this.choiceStandardDimension.setDefaultValue(choiceStandardDimension);

        ChoiceStandardDimension currentValue = this.choiceStandardDimension.getValue();

        fireChoiceStandardDimensionChange(currentValue, oldValue);
    }
    
    public final ChoiceStandardDimension removeDefaultChoiceStandardDimension()
    {
        ChoiceStandardDimension oldValue = this.choiceStandardDimension.getValue();
        ChoiceStandardDimension removedValue = this.choiceStandardDimension.removeDefaultValue();
        ChoiceStandardDimension currentValue = this.choiceStandardDimension.getValue();

        fireChoiceStandardDimensionChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceStandardDimension> getChoiceStandardDimensionValueContainer()
    {
        return this.choiceStandardDimension;
    }
    public final void fireChoiceStandardDimensionChange(ChoiceStandardDimension currentValue, ChoiceStandardDimension oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceStandardDimensionChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICESTANDARDDIMENSION, oldValue, currentValue);
            afterChoiceStandardDimensionChanged(currentValue);
        }
    }

    public void beforeChoiceStandardDimensionChanged( ChoiceStandardDimension choiceStandardDimension)
    { }
    public void afterChoiceStandardDimensionChanged( ChoiceStandardDimension choiceStandardDimension)
    { }


    public boolean isChoiceStandardDimensionMandatory()
    {
        return true;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Paintjob")
        ,@LocalizedTag(language="fr", name="label", value="Choix de peinture")
    })
    public final ChoicePaintingSide getChoicePaintingSide()
    {
         return this.choicePaintingSide.getValue();
    }

    public final void setChoicePaintingSide(ChoicePaintingSide choicePaintingSide)
    {

        ChoicePaintingSide oldValue = this.choicePaintingSide.getValue();
        this.choicePaintingSide.setValue(choicePaintingSide);

        ChoicePaintingSide currentValue = this.choicePaintingSide.getValue();

        fireChoicePaintingSideChange(currentValue, oldValue);
    }
    public final ChoicePaintingSide removeChoicePaintingSide()
    {
        ChoicePaintingSide oldValue = this.choicePaintingSide.getValue();
        ChoicePaintingSide removedValue = this.choicePaintingSide.removeValue();
        ChoicePaintingSide currentValue = this.choicePaintingSide.getValue();

        fireChoicePaintingSideChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoicePaintingSide(ChoicePaintingSide choicePaintingSide)
    {
        ChoicePaintingSide oldValue = this.choicePaintingSide.getValue();
        this.choicePaintingSide.setDefaultValue(choicePaintingSide);

        ChoicePaintingSide currentValue = this.choicePaintingSide.getValue();

        fireChoicePaintingSideChange(currentValue, oldValue);
    }
    
    public final ChoicePaintingSide removeDefaultChoicePaintingSide()
    {
        ChoicePaintingSide oldValue = this.choicePaintingSide.getValue();
        ChoicePaintingSide removedValue = this.choicePaintingSide.removeDefaultValue();
        ChoicePaintingSide currentValue = this.choicePaintingSide.getValue();

        fireChoicePaintingSideChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoicePaintingSide> getChoicePaintingSideValueContainer()
    {
        return this.choicePaintingSide;
    }
    public final void fireChoicePaintingSideChange(ChoicePaintingSide currentValue, ChoicePaintingSide oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoicePaintingSideChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEPAINTINGSIDE, oldValue, currentValue);
            afterChoicePaintingSideChanged(currentValue);
        }
    }

    public void beforeChoicePaintingSideChanged( ChoicePaintingSide choicePaintingSide)
    { }
    public void afterChoicePaintingSideChanged( ChoicePaintingSide choicePaintingSide)
    { }


    public boolean isChoicePaintingSideMandatory()
    {
        return true;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Choix du cadre")
        ,@LocalizedTag(language="fr", name="label", value="Choix du cadre")
    })
    public final ChoiceBoxType getChoiceBoxType()
    {
         return this.choiceBoxType.getValue();
    }

    public final void setChoiceBoxType(ChoiceBoxType choiceBoxType)
    {

        ChoiceBoxType oldValue = this.choiceBoxType.getValue();
        this.choiceBoxType.setValue(choiceBoxType);

        ChoiceBoxType currentValue = this.choiceBoxType.getValue();

        fireChoiceBoxTypeChange(currentValue, oldValue);
    }
    public final ChoiceBoxType removeChoiceBoxType()
    {
        ChoiceBoxType oldValue = this.choiceBoxType.getValue();
        ChoiceBoxType removedValue = this.choiceBoxType.removeValue();
        ChoiceBoxType currentValue = this.choiceBoxType.getValue();

        fireChoiceBoxTypeChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceBoxType(ChoiceBoxType choiceBoxType)
    {
        ChoiceBoxType oldValue = this.choiceBoxType.getValue();
        this.choiceBoxType.setDefaultValue(choiceBoxType);

        ChoiceBoxType currentValue = this.choiceBoxType.getValue();

        fireChoiceBoxTypeChange(currentValue, oldValue);
    }
    
    public final ChoiceBoxType removeDefaultChoiceBoxType()
    {
        ChoiceBoxType oldValue = this.choiceBoxType.getValue();
        ChoiceBoxType removedValue = this.choiceBoxType.removeDefaultValue();
        ChoiceBoxType currentValue = this.choiceBoxType.getValue();

        fireChoiceBoxTypeChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceBoxType> getChoiceBoxTypeValueContainer()
    {
        return this.choiceBoxType;
    }
    public final void fireChoiceBoxTypeChange(ChoiceBoxType currentValue, ChoiceBoxType oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceBoxTypeChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEBOXTYPE, oldValue, currentValue);
            afterChoiceBoxTypeChanged(currentValue);
        }
    }

    public void beforeChoiceBoxTypeChanged( ChoiceBoxType choiceBoxType)
    { }
    public void afterChoiceBoxTypeChanged( ChoiceBoxType choiceBoxType)
    { }


    public boolean isChoiceBoxTypeMandatory()
    {
        return true;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Openning")
        ,@LocalizedTag(language="fr", name="label", value="Overture")
    })
    public final ChoiceIllusionOpening getChoiceIllusionOpening()
    {
         return this.choiceIllusionOpening.getValue();
    }

    public final void setChoiceIllusionOpening(ChoiceIllusionOpening choiceIllusionOpening)
    {

        ChoiceIllusionOpening oldValue = this.choiceIllusionOpening.getValue();
        this.choiceIllusionOpening.setValue(choiceIllusionOpening);

        ChoiceIllusionOpening currentValue = this.choiceIllusionOpening.getValue();

        fireChoiceIllusionOpeningChange(currentValue, oldValue);
    }
    public final ChoiceIllusionOpening removeChoiceIllusionOpening()
    {
        ChoiceIllusionOpening oldValue = this.choiceIllusionOpening.getValue();
        ChoiceIllusionOpening removedValue = this.choiceIllusionOpening.removeValue();
        ChoiceIllusionOpening currentValue = this.choiceIllusionOpening.getValue();

        fireChoiceIllusionOpeningChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceIllusionOpening(ChoiceIllusionOpening choiceIllusionOpening)
    {
        ChoiceIllusionOpening oldValue = this.choiceIllusionOpening.getValue();
        this.choiceIllusionOpening.setDefaultValue(choiceIllusionOpening);

        ChoiceIllusionOpening currentValue = this.choiceIllusionOpening.getValue();

        fireChoiceIllusionOpeningChange(currentValue, oldValue);
    }
    
    public final ChoiceIllusionOpening removeDefaultChoiceIllusionOpening()
    {
        ChoiceIllusionOpening oldValue = this.choiceIllusionOpening.getValue();
        ChoiceIllusionOpening removedValue = this.choiceIllusionOpening.removeDefaultValue();
        ChoiceIllusionOpening currentValue = this.choiceIllusionOpening.getValue();

        fireChoiceIllusionOpeningChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceIllusionOpening> getChoiceIllusionOpeningValueContainer()
    {
        return this.choiceIllusionOpening;
    }
    public final void fireChoiceIllusionOpeningChange(ChoiceIllusionOpening currentValue, ChoiceIllusionOpening oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceIllusionOpeningChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEILLUSIONOPENING, oldValue, currentValue);
            afterChoiceIllusionOpeningChanged(currentValue);
        }
    }

    public void beforeChoiceIllusionOpeningChanged( ChoiceIllusionOpening choiceIllusionOpening)
    { }
    public void afterChoiceIllusionOpeningChanged( ChoiceIllusionOpening choiceIllusionOpening)
    { }


    public boolean isChoiceIllusionOpeningMandatory()
    {
        return true;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Options")
        ,@LocalizedTag(language="fr", name="label", value="Options")
    })
    public final SteelDoorOptions getSteelDoorOptions()
    {
         return this.steelDoorOptions.getValue();
    }

    public final void setSteelDoorOptions(SteelDoorOptions steelDoorOptions)
    {

        if (steelDoorOptions != null)
        {
            acquire(steelDoorOptions, "steelDoorOptions");
        }
        SteelDoorOptions oldValue = this.steelDoorOptions.getValue();
        this.steelDoorOptions.setValue(steelDoorOptions);

        SteelDoorOptions currentValue = this.steelDoorOptions.getValue();

        fireSteelDoorOptionsChange(currentValue, oldValue);
    }
    public final SteelDoorOptions removeSteelDoorOptions()
    {
        SteelDoorOptions oldValue = this.steelDoorOptions.getValue();
        SteelDoorOptions removedValue = this.steelDoorOptions.removeValue();
        SteelDoorOptions currentValue = this.steelDoorOptions.getValue();

        fireSteelDoorOptionsChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultSteelDoorOptions(SteelDoorOptions steelDoorOptions)
    {
        if (steelDoorOptions != null)
        {
            acquire(steelDoorOptions, "steelDoorOptions");
        }
        SteelDoorOptions oldValue = this.steelDoorOptions.getValue();
        this.steelDoorOptions.setDefaultValue(steelDoorOptions);

        SteelDoorOptions currentValue = this.steelDoorOptions.getValue();

        fireSteelDoorOptionsChange(currentValue, oldValue);
    }
    
    public final SteelDoorOptions removeDefaultSteelDoorOptions()
    {
        SteelDoorOptions oldValue = this.steelDoorOptions.getValue();
        SteelDoorOptions removedValue = this.steelDoorOptions.removeDefaultValue();
        SteelDoorOptions currentValue = this.steelDoorOptions.getValue();

        fireSteelDoorOptionsChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<SteelDoorOptions> getSteelDoorOptionsValueContainer()
    {
        return this.steelDoorOptions;
    }
    public final void fireSteelDoorOptionsChange(SteelDoorOptions currentValue, SteelDoorOptions oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeSteelDoorOptionsChanged(currentValue);
            firePropertyChange(PROPERTYNAME_STEELDOOROPTIONS, oldValue, currentValue);
            afterSteelDoorOptionsChanged(currentValue);
        }
    }

    public void beforeSteelDoorOptionsChanged( SteelDoorOptions steelDoorOptions)
    { }
    public void afterSteelDoorOptionsChanged( SteelDoorOptions steelDoorOptions)
    { }


    public boolean isSteelDoorOptionsMandatory()
    {
        return true;
    }


    @Override
    protected String getSequences()
    {
        return "choiceStandardDimension,NonStandardDimensions,dimension,exteriorColor,interiorColor,door,sideLight,choiceSlabSidelightWidth,choiceSlabDoorWidth,choicePaintingSide,choiceBoxType,choiceIllusionOpening,steelDoorOptions,profilesNOS,nbDoors,sillProfiles,profilesOS,slabDoorWidth,nbSideLight,slabSidelightWidth,doorFrame,renderingNumber,paintable,choiceSideLightSide,choiceSwing,doorMullionFix,choiceBuildType";  
    }
}
