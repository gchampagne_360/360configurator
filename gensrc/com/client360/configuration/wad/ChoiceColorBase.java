// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.client360.configuration.wad.enums.*; 

import com.netappsid.erp.configurator.*;
import com.netappsid.erp.configurator.annotations.*;

import org.apache.log4j.Logger;

import org.jscience.physics.measures.*;
import javax.measure.quantities.*;

import java.awt.geom.Line2D;
import java.awt.geom.Arc2D;
import java.awt.geom.Point2D;

import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.netappsid.rendering.common.advanced.Profile;

import com.netappsid.commonutils.geo.Form;
import com.netappsid.commonutils.geo.NAIDShape;
import com.netappsid.commonutils.geo.CornerType;

@DynamicEnum(fileName = "colors")
@Image("37")
@IdColumn("0")
public abstract class ChoiceColorBase extends com.netappsid.erp.configurator.Configurable
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ChoiceColorBase.class);

    // attributes
    private ValueContainer<String> code = new ValueContainer<String>(this);
    private ValueContainer<String> description = new ValueContainer<String>(this);
    private ValueContainer<Integer> red = new ValueContainer<Integer>(this);
    private ValueContainer<Integer> green = new ValueContainer<Integer>(this);
    private ValueContainer<Integer> blue = new ValueContainer<Integer>(this);
    private ValueContainer<String> colorChart = new ValueContainer<String>(this);
    private ValueContainer<Boolean> wood = new ValueContainer<Boolean>(this);
    private ValueContainer<String> imgPath = new ValueContainer<String>(this);
    private ValueContainer<String> colorCodeHandle = new ValueContainer<String>(this);
    private ValueContainer<String> texturePath = new ValueContainer<String>(this);

    // Bound properties

    public static final String PROPERTYNAME_CODE = "code";  
    public static final String PROPERTYNAME_DESCRIPTION = "description";  
    public static final String PROPERTYNAME_RED = "red";  
    public static final String PROPERTYNAME_GREEN = "green";  
    public static final String PROPERTYNAME_BLUE = "blue";  
    public static final String PROPERTYNAME_COLORCHART = "colorChart";  
    public static final String PROPERTYNAME_WOOD = "wood";  
    public static final String PROPERTYNAME_IMGPATH = "imgPath";  
    public static final String PROPERTYNAME_COLORCODEHANDLE = "colorCodeHandle";  
    public static final String PROPERTYNAME_TEXTUREPATH = "texturePath";  

    // Constructors

    public ChoiceColorBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.ChoiceColor"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="0")
        ,@LocalizedTag(language="fr", name="column", value="0")
        ,@LocalizedTag(language="en", name="label", value="Code")
        ,@LocalizedTag(language="fr", name="label", value="Code")
    })
    public final String getCode()
    {
         return this.code.getValue();
    }

    public final void setCode(String code)
    {

        String oldValue = this.code.getValue();
        this.code.setValue(code);

        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
    }
    public final String removeCode()
    {
        String oldValue = this.code.getValue();
        String removedValue = this.code.removeValue();
        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultCode(String code)
    {
        String oldValue = this.code.getValue();
        this.code.setDefaultValue(code);

        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
    }
    
    public final String removeDefaultCode()
    {
        String oldValue = this.code.getValue();
        String removedValue = this.code.removeDefaultValue();
        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getCodeValueContainer()
    {
        return this.code;
    }
    public final void fireCodeChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeCodeChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CODE, oldValue, currentValue);
            afterCodeChanged(currentValue);
        }
    }

    public void beforeCodeChanged( String code)
    { }
    public void afterCodeChanged( String code)
    { }


    public boolean isCodeVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="2")
        ,@LocalizedTag(language="fr", name="column", value="1")
        ,@LocalizedTag(language="en", name="label", value="Description")
        ,@LocalizedTag(language="fr", name="label", value="Description")
    })
    public final String getDescription()
    {
         return this.description.getValue();
    }

    public final void setDescription(String description)
    {

        String oldValue = this.description.getValue();
        this.description.setValue(description);

        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
    }
    public final String removeDescription()
    {
        String oldValue = this.description.getValue();
        String removedValue = this.description.removeValue();
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultDescription(String description)
    {
        String oldValue = this.description.getValue();
        this.description.setDefaultValue(description);

        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
    }
    
    public final String removeDefaultDescription()
    {
        String oldValue = this.description.getValue();
        String removedValue = this.description.removeDefaultValue();
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getDescriptionValueContainer()
    {
        return this.description;
    }
    public final void fireDescriptionChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeDescriptionChanged(currentValue);
            firePropertyChange(PROPERTYNAME_DESCRIPTION, oldValue, currentValue);
            afterDescriptionChanged(currentValue);
        }
    }

    public void beforeDescriptionChanged( String description)
    { }
    public void afterDescriptionChanged( String description)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="3")
        ,@LocalizedTag(language="fr", name="column", value="3")
        ,@LocalizedTag(language="en", name="label", value="Red")
        ,@LocalizedTag(language="fr", name="label", value="Rouge")
    })
    public final Integer getRed()
    {
         return this.red.getValue();
    }

    public final void setRed(Integer red)
    {

        Integer oldValue = this.red.getValue();
        this.red.setValue(red);

        Integer currentValue = this.red.getValue();

        fireRedChange(currentValue, oldValue);
    }
    public final Integer removeRed()
    {
        Integer oldValue = this.red.getValue();
        Integer removedValue = this.red.removeValue();
        Integer currentValue = this.red.getValue();

        fireRedChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultRed(Integer red)
    {
        Integer oldValue = this.red.getValue();
        this.red.setDefaultValue(red);

        Integer currentValue = this.red.getValue();

        fireRedChange(currentValue, oldValue);
    }
    
    public final Integer removeDefaultRed()
    {
        Integer oldValue = this.red.getValue();
        Integer removedValue = this.red.removeDefaultValue();
        Integer currentValue = this.red.getValue();

        fireRedChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Integer> getRedValueContainer()
    {
        return this.red;
    }
    public final void fireRedChange(Integer currentValue, Integer oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeRedChanged(currentValue);
            firePropertyChange(PROPERTYNAME_RED, oldValue, currentValue);
            afterRedChanged(currentValue);
        }
    }

    public void beforeRedChanged( Integer red)
    { }
    public void afterRedChanged( Integer red)
    { }


    public boolean isRedVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="4")
        ,@LocalizedTag(language="fr", name="column", value="4")
        ,@LocalizedTag(language="en", name="label", value="Green")
        ,@LocalizedTag(language="fr", name="label", value="Vert")
    })
    public final Integer getGreen()
    {
         return this.green.getValue();
    }

    public final void setGreen(Integer green)
    {

        Integer oldValue = this.green.getValue();
        this.green.setValue(green);

        Integer currentValue = this.green.getValue();

        fireGreenChange(currentValue, oldValue);
    }
    public final Integer removeGreen()
    {
        Integer oldValue = this.green.getValue();
        Integer removedValue = this.green.removeValue();
        Integer currentValue = this.green.getValue();

        fireGreenChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultGreen(Integer green)
    {
        Integer oldValue = this.green.getValue();
        this.green.setDefaultValue(green);

        Integer currentValue = this.green.getValue();

        fireGreenChange(currentValue, oldValue);
    }
    
    public final Integer removeDefaultGreen()
    {
        Integer oldValue = this.green.getValue();
        Integer removedValue = this.green.removeDefaultValue();
        Integer currentValue = this.green.getValue();

        fireGreenChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Integer> getGreenValueContainer()
    {
        return this.green;
    }
    public final void fireGreenChange(Integer currentValue, Integer oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeGreenChanged(currentValue);
            firePropertyChange(PROPERTYNAME_GREEN, oldValue, currentValue);
            afterGreenChanged(currentValue);
        }
    }

    public void beforeGreenChanged( Integer green)
    { }
    public void afterGreenChanged( Integer green)
    { }


    public boolean isGreenVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="5")
        ,@LocalizedTag(language="fr", name="column", value="5")
        ,@LocalizedTag(language="en", name="label", value="Blue")
        ,@LocalizedTag(language="fr", name="label", value="Bleu")
    })
    public final Integer getBlue()
    {
         return this.blue.getValue();
    }

    public final void setBlue(Integer blue)
    {

        Integer oldValue = this.blue.getValue();
        this.blue.setValue(blue);

        Integer currentValue = this.blue.getValue();

        fireBlueChange(currentValue, oldValue);
    }
    public final Integer removeBlue()
    {
        Integer oldValue = this.blue.getValue();
        Integer removedValue = this.blue.removeValue();
        Integer currentValue = this.blue.getValue();

        fireBlueChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultBlue(Integer blue)
    {
        Integer oldValue = this.blue.getValue();
        this.blue.setDefaultValue(blue);

        Integer currentValue = this.blue.getValue();

        fireBlueChange(currentValue, oldValue);
    }
    
    public final Integer removeDefaultBlue()
    {
        Integer oldValue = this.blue.getValue();
        Integer removedValue = this.blue.removeDefaultValue();
        Integer currentValue = this.blue.getValue();

        fireBlueChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Integer> getBlueValueContainer()
    {
        return this.blue;
    }
    public final void fireBlueChange(Integer currentValue, Integer oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeBlueChanged(currentValue);
            firePropertyChange(PROPERTYNAME_BLUE, oldValue, currentValue);
            afterBlueChanged(currentValue);
        }
    }

    public void beforeBlueChanged( Integer blue)
    { }
    public void afterBlueChanged( Integer blue)
    { }


    public boolean isBlueVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="6")
        ,@LocalizedTag(language="fr", name="column", value="6")
        ,@LocalizedTag(language="en", name="label", value="Color chart")
        ,@LocalizedTag(language="fr", name="label", value="Charte de couleur")
    })
    public final String getColorChart()
    {
         return this.colorChart.getValue();
    }

    public final void setColorChart(String colorChart)
    {

        String oldValue = this.colorChart.getValue();
        this.colorChart.setValue(colorChart);

        String currentValue = this.colorChart.getValue();

        fireColorChartChange(currentValue, oldValue);
    }
    public final String removeColorChart()
    {
        String oldValue = this.colorChart.getValue();
        String removedValue = this.colorChart.removeValue();
        String currentValue = this.colorChart.getValue();

        fireColorChartChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultColorChart(String colorChart)
    {
        String oldValue = this.colorChart.getValue();
        this.colorChart.setDefaultValue(colorChart);

        String currentValue = this.colorChart.getValue();

        fireColorChartChange(currentValue, oldValue);
    }
    
    public final String removeDefaultColorChart()
    {
        String oldValue = this.colorChart.getValue();
        String removedValue = this.colorChart.removeDefaultValue();
        String currentValue = this.colorChart.getValue();

        fireColorChartChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getColorChartValueContainer()
    {
        return this.colorChart;
    }
    public final void fireColorChartChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeColorChartChanged(currentValue);
            firePropertyChange(PROPERTYNAME_COLORCHART, oldValue, currentValue);
            afterColorChartChanged(currentValue);
        }
    }

    public void beforeColorChartChanged( String colorChart)
    { }
    public void afterColorChartChanged( String colorChart)
    { }


    public boolean isColorChartVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="7")
        ,@LocalizedTag(language="fr", name="column", value="7")
        ,@LocalizedTag(language="en", name="label", value="Wood")
        ,@LocalizedTag(language="fr", name="label", value="Bois")
    })
    public final Boolean getWood()
    {
         return this.wood.getValue();
    }

    public final void setWood(Boolean wood)
    {

        Boolean oldValue = this.wood.getValue();
        this.wood.setValue(wood);

        Boolean currentValue = this.wood.getValue();

        fireWoodChange(currentValue, oldValue);
    }
    public final Boolean removeWood()
    {
        Boolean oldValue = this.wood.getValue();
        Boolean removedValue = this.wood.removeValue();
        Boolean currentValue = this.wood.getValue();

        fireWoodChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultWood(Boolean wood)
    {
        Boolean oldValue = this.wood.getValue();
        this.wood.setDefaultValue(wood);

        Boolean currentValue = this.wood.getValue();

        fireWoodChange(currentValue, oldValue);
    }
    
    public final Boolean removeDefaultWood()
    {
        Boolean oldValue = this.wood.getValue();
        Boolean removedValue = this.wood.removeDefaultValue();
        Boolean currentValue = this.wood.getValue();

        fireWoodChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Boolean> getWoodValueContainer()
    {
        return this.wood;
    }
    public final void fireWoodChange(Boolean currentValue, Boolean oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeWoodChanged(currentValue);
            firePropertyChange(PROPERTYNAME_WOOD, oldValue, currentValue);
            afterWoodChanged(currentValue);
        }
    }

    public void beforeWoodChanged( Boolean wood)
    { }
    public void afterWoodChanged( Boolean wood)
    { }


    public boolean isWoodVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="8")
        ,@LocalizedTag(language="fr", name="column", value="8")
        ,@LocalizedTag(language="fr", name="label", value="Path d\'images")
    })
    public final String getImgPath()
    {
         return this.imgPath.getValue();
    }

    public final void setImgPath(String imgPath)
    {

        String oldValue = this.imgPath.getValue();
        this.imgPath.setValue(imgPath);

        String currentValue = this.imgPath.getValue();

        fireImgPathChange(currentValue, oldValue);
    }
    public final String removeImgPath()
    {
        String oldValue = this.imgPath.getValue();
        String removedValue = this.imgPath.removeValue();
        String currentValue = this.imgPath.getValue();

        fireImgPathChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultImgPath(String imgPath)
    {
        String oldValue = this.imgPath.getValue();
        this.imgPath.setDefaultValue(imgPath);

        String currentValue = this.imgPath.getValue();

        fireImgPathChange(currentValue, oldValue);
    }
    
    public final String removeDefaultImgPath()
    {
        String oldValue = this.imgPath.getValue();
        String removedValue = this.imgPath.removeDefaultValue();
        String currentValue = this.imgPath.getValue();

        fireImgPathChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getImgPathValueContainer()
    {
        return this.imgPath;
    }
    public final void fireImgPathChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeImgPathChanged(currentValue);
            firePropertyChange(PROPERTYNAME_IMGPATH, oldValue, currentValue);
            afterImgPathChanged(currentValue);
        }
    }

    public void beforeImgPathChanged( String imgPath)
    { }
    public void afterImgPathChanged( String imgPath)
    { }


    public boolean isImgPathVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="9")
        ,@LocalizedTag(language="fr", name="column", value="9")
        ,@LocalizedTag(language="fr", name="label", value="Path d\'images")
    })
    public final String getColorCodeHandle()
    {
         return this.colorCodeHandle.getValue();
    }

    public final void setColorCodeHandle(String colorCodeHandle)
    {

        String oldValue = this.colorCodeHandle.getValue();
        this.colorCodeHandle.setValue(colorCodeHandle);

        String currentValue = this.colorCodeHandle.getValue();

        fireColorCodeHandleChange(currentValue, oldValue);
    }
    public final String removeColorCodeHandle()
    {
        String oldValue = this.colorCodeHandle.getValue();
        String removedValue = this.colorCodeHandle.removeValue();
        String currentValue = this.colorCodeHandle.getValue();

        fireColorCodeHandleChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultColorCodeHandle(String colorCodeHandle)
    {
        String oldValue = this.colorCodeHandle.getValue();
        this.colorCodeHandle.setDefaultValue(colorCodeHandle);

        String currentValue = this.colorCodeHandle.getValue();

        fireColorCodeHandleChange(currentValue, oldValue);
    }
    
    public final String removeDefaultColorCodeHandle()
    {
        String oldValue = this.colorCodeHandle.getValue();
        String removedValue = this.colorCodeHandle.removeDefaultValue();
        String currentValue = this.colorCodeHandle.getValue();

        fireColorCodeHandleChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getColorCodeHandleValueContainer()
    {
        return this.colorCodeHandle;
    }
    public final void fireColorCodeHandleChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeColorCodeHandleChanged(currentValue);
            firePropertyChange(PROPERTYNAME_COLORCODEHANDLE, oldValue, currentValue);
            afterColorCodeHandleChanged(currentValue);
        }
    }

    public void beforeColorCodeHandleChanged( String colorCodeHandle)
    { }
    public void afterColorCodeHandleChanged( String colorCodeHandle)
    { }


    public boolean isColorCodeHandleVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="10")
        ,@LocalizedTag(language="fr", name="column", value="10")
        ,@LocalizedTag(language="fr", name="label", value="Path des textures")
    })
    public final String getTexturePath()
    {
         return this.texturePath.getValue();
    }

    public final void setTexturePath(String texturePath)
    {

        String oldValue = this.texturePath.getValue();
        this.texturePath.setValue(texturePath);

        String currentValue = this.texturePath.getValue();

        fireTexturePathChange(currentValue, oldValue);
    }
    public final String removeTexturePath()
    {
        String oldValue = this.texturePath.getValue();
        String removedValue = this.texturePath.removeValue();
        String currentValue = this.texturePath.getValue();

        fireTexturePathChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultTexturePath(String texturePath)
    {
        String oldValue = this.texturePath.getValue();
        this.texturePath.setDefaultValue(texturePath);

        String currentValue = this.texturePath.getValue();

        fireTexturePathChange(currentValue, oldValue);
    }
    
    public final String removeDefaultTexturePath()
    {
        String oldValue = this.texturePath.getValue();
        String removedValue = this.texturePath.removeDefaultValue();
        String currentValue = this.texturePath.getValue();

        fireTexturePathChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getTexturePathValueContainer()
    {
        return this.texturePath;
    }
    public final void fireTexturePathChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeTexturePathChanged(currentValue);
            firePropertyChange(PROPERTYNAME_TEXTUREPATH, oldValue, currentValue);
            afterTexturePathChanged(currentValue);
        }
    }

    public void beforeTexturePathChanged( String texturePath)
    { }
    public void afterTexturePathChanged( String texturePath)
    { }


    public boolean isTexturePathVisible()
    {
        return false;
    }

}
