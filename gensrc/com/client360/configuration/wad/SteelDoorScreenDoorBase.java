// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.client360.configuration.wad.enums.ChoiceScreenDoor;
import com.client360.configuration.wad.enums.ChoiceScreenDoorHingeSide;
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class SteelDoorScreenDoorBase extends com.netappsid.erp.configurator.Configurable
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(SteelDoorScreenDoorBase.class);

    // attributes
    private ValueContainer<ChoiceScreenDoor> choiceScreenDoor = new ValueContainer<ChoiceScreenDoor>(this);
    private ValueContainer<ChoiceScreenDoorHingeSide> choiceScreenDoorHingeSide = new ValueContainer<ChoiceScreenDoorHingeSide>(this);

    // Bound properties

    public static final String PROPERTYNAME_CHOICESCREENDOOR = "choiceScreenDoor";  
    public static final String PROPERTYNAME_CHOICESCREENDOORHINGESIDE = "choiceScreenDoorHingeSide";  

    // Constructors

    public SteelDoorScreenDoorBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.SteelDoorScreenDoor"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
    })
    public final ChoiceScreenDoor getChoiceScreenDoor()
    {
         return this.choiceScreenDoor.getValue();
    }

    public final void setChoiceScreenDoor(ChoiceScreenDoor choiceScreenDoor)
    {

        ChoiceScreenDoor oldValue = this.choiceScreenDoor.getValue();
        this.choiceScreenDoor.setValue(choiceScreenDoor);

        ChoiceScreenDoor currentValue = this.choiceScreenDoor.getValue();

        fireChoiceScreenDoorChange(currentValue, oldValue);
    }
    public final ChoiceScreenDoor removeChoiceScreenDoor()
    {
        ChoiceScreenDoor oldValue = this.choiceScreenDoor.getValue();
        ChoiceScreenDoor removedValue = this.choiceScreenDoor.removeValue();
        ChoiceScreenDoor currentValue = this.choiceScreenDoor.getValue();

        fireChoiceScreenDoorChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceScreenDoor(ChoiceScreenDoor choiceScreenDoor)
    {
        ChoiceScreenDoor oldValue = this.choiceScreenDoor.getValue();
        this.choiceScreenDoor.setDefaultValue(choiceScreenDoor);

        ChoiceScreenDoor currentValue = this.choiceScreenDoor.getValue();

        fireChoiceScreenDoorChange(currentValue, oldValue);
    }
    
    public final ChoiceScreenDoor removeDefaultChoiceScreenDoor()
    {
        ChoiceScreenDoor oldValue = this.choiceScreenDoor.getValue();
        ChoiceScreenDoor removedValue = this.choiceScreenDoor.removeDefaultValue();
        ChoiceScreenDoor currentValue = this.choiceScreenDoor.getValue();

        fireChoiceScreenDoorChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceScreenDoor> getChoiceScreenDoorValueContainer()
    {
        return this.choiceScreenDoor;
    }
    public final void fireChoiceScreenDoorChange(ChoiceScreenDoor currentValue, ChoiceScreenDoor oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceScreenDoorChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICESCREENDOOR, oldValue, currentValue);
            afterChoiceScreenDoorChanged(currentValue);
        }
    }

    public void beforeChoiceScreenDoorChanged( ChoiceScreenDoor choiceScreenDoor)
    { }
    public void afterChoiceScreenDoorChanged( ChoiceScreenDoor choiceScreenDoor)
    { }


    public boolean isChoiceScreenDoorMandatory()
    {
        return true;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Screen side")
        ,@LocalizedTag(language="fr", name="label", value="C�t� du moustiquaire")
    })
    public final ChoiceScreenDoorHingeSide getChoiceScreenDoorHingeSide()
    {
         return this.choiceScreenDoorHingeSide.getValue();
    }

    public final void setChoiceScreenDoorHingeSide(ChoiceScreenDoorHingeSide choiceScreenDoorHingeSide)
    {

        ChoiceScreenDoorHingeSide oldValue = this.choiceScreenDoorHingeSide.getValue();
        this.choiceScreenDoorHingeSide.setValue(choiceScreenDoorHingeSide);

        ChoiceScreenDoorHingeSide currentValue = this.choiceScreenDoorHingeSide.getValue();

        fireChoiceScreenDoorHingeSideChange(currentValue, oldValue);
    }
    public final ChoiceScreenDoorHingeSide removeChoiceScreenDoorHingeSide()
    {
        ChoiceScreenDoorHingeSide oldValue = this.choiceScreenDoorHingeSide.getValue();
        ChoiceScreenDoorHingeSide removedValue = this.choiceScreenDoorHingeSide.removeValue();
        ChoiceScreenDoorHingeSide currentValue = this.choiceScreenDoorHingeSide.getValue();

        fireChoiceScreenDoorHingeSideChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceScreenDoorHingeSide(ChoiceScreenDoorHingeSide choiceScreenDoorHingeSide)
    {
        ChoiceScreenDoorHingeSide oldValue = this.choiceScreenDoorHingeSide.getValue();
        this.choiceScreenDoorHingeSide.setDefaultValue(choiceScreenDoorHingeSide);

        ChoiceScreenDoorHingeSide currentValue = this.choiceScreenDoorHingeSide.getValue();

        fireChoiceScreenDoorHingeSideChange(currentValue, oldValue);
    }
    
    public final ChoiceScreenDoorHingeSide removeDefaultChoiceScreenDoorHingeSide()
    {
        ChoiceScreenDoorHingeSide oldValue = this.choiceScreenDoorHingeSide.getValue();
        ChoiceScreenDoorHingeSide removedValue = this.choiceScreenDoorHingeSide.removeDefaultValue();
        ChoiceScreenDoorHingeSide currentValue = this.choiceScreenDoorHingeSide.getValue();

        fireChoiceScreenDoorHingeSideChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceScreenDoorHingeSide> getChoiceScreenDoorHingeSideValueContainer()
    {
        return this.choiceScreenDoorHingeSide;
    }
    public final void fireChoiceScreenDoorHingeSideChange(ChoiceScreenDoorHingeSide currentValue, ChoiceScreenDoorHingeSide oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceScreenDoorHingeSideChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICESCREENDOORHINGESIDE, oldValue, currentValue);
            afterChoiceScreenDoorHingeSideChanged(currentValue);
        }
    }

    public void beforeChoiceScreenDoorHingeSideChanged( ChoiceScreenDoorHingeSide choiceScreenDoorHingeSide)
    { }
    public void afterChoiceScreenDoorHingeSideChanged( ChoiceScreenDoorHingeSide choiceScreenDoorHingeSide)
    { }


    public boolean isChoiceScreenDoorHingeSideMandatory()
    {
        return true;
    }

}
