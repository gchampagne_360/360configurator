// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.client360.configuration.wad.enums.ChoiceBackground;
import com.client360.configuration.wad.enums.Post;
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class PortailBase extends com.client360.configuration.wad.SteelDoor
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(PortailBase.class);

    // attributes
    private ValueContainer<ChoiceBackground> choiceBackground = new ValueContainer<ChoiceBackground>(this);
    private ValueContainer<Post> post = new ValueContainer<Post>(this);

    // Bound properties

    public static final String PROPERTYNAME_CHOICEBACKGROUND = "choiceBackground";  
    public static final String PROPERTYNAME_POST = "post";  

    // Constructors

    public PortailBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.Portail"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="fr", name="label", value="Environnement")
    })
    public final ChoiceBackground getChoiceBackground()
    {
         return this.choiceBackground.getValue();
    }

    public final void setChoiceBackground(ChoiceBackground choiceBackground)
    {

        ChoiceBackground oldValue = this.choiceBackground.getValue();
        this.choiceBackground.setValue(choiceBackground);

        ChoiceBackground currentValue = this.choiceBackground.getValue();

        fireChoiceBackgroundChange(currentValue, oldValue);
    }
    public final ChoiceBackground removeChoiceBackground()
    {
        ChoiceBackground oldValue = this.choiceBackground.getValue();
        ChoiceBackground removedValue = this.choiceBackground.removeValue();
        ChoiceBackground currentValue = this.choiceBackground.getValue();

        fireChoiceBackgroundChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceBackground(ChoiceBackground choiceBackground)
    {
        ChoiceBackground oldValue = this.choiceBackground.getValue();
        this.choiceBackground.setDefaultValue(choiceBackground);

        ChoiceBackground currentValue = this.choiceBackground.getValue();

        fireChoiceBackgroundChange(currentValue, oldValue);
    }
    
    public final ChoiceBackground removeDefaultChoiceBackground()
    {
        ChoiceBackground oldValue = this.choiceBackground.getValue();
        ChoiceBackground removedValue = this.choiceBackground.removeDefaultValue();
        ChoiceBackground currentValue = this.choiceBackground.getValue();

        fireChoiceBackgroundChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceBackground> getChoiceBackgroundValueContainer()
    {
        return this.choiceBackground;
    }
    public final void fireChoiceBackgroundChange(ChoiceBackground currentValue, ChoiceBackground oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceBackgroundChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEBACKGROUND, oldValue, currentValue);
            afterChoiceBackgroundChanged(currentValue);
        }
    }

    public void beforeChoiceBackgroundChanged( ChoiceBackground choiceBackground)
    { }
    public void afterChoiceBackgroundChanged( ChoiceBackground choiceBackground)
    { }


    public boolean isChoiceBackgroundMandatory()
    {
        return true;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Posts")
        ,@LocalizedTag(language="fr", name="label", value="Poteaux")
    })
    public final Post getPost()
    {
         return this.post.getValue();
    }

    public final void setPost(Post post)
    {

        Post oldValue = this.post.getValue();
        this.post.setValue(post);

        Post currentValue = this.post.getValue();

        firePostChange(currentValue, oldValue);
    }
    public final Post removePost()
    {
        Post oldValue = this.post.getValue();
        Post removedValue = this.post.removeValue();
        Post currentValue = this.post.getValue();

        firePostChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultPost(Post post)
    {
        Post oldValue = this.post.getValue();
        this.post.setDefaultValue(post);

        Post currentValue = this.post.getValue();

        firePostChange(currentValue, oldValue);
    }
    
    public final Post removeDefaultPost()
    {
        Post oldValue = this.post.getValue();
        Post removedValue = this.post.removeDefaultValue();
        Post currentValue = this.post.getValue();

        firePostChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Post> getPostValueContainer()
    {
        return this.post;
    }
    public final void firePostChange(Post currentValue, Post oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforePostChanged(currentValue);
            firePropertyChange(PROPERTYNAME_POST, oldValue, currentValue);
            afterPostChanged(currentValue);
        }
    }

    public void beforePostChanged( Post post)
    { }
    public void afterPostChanged( Post post)
    { }


    public boolean isPostMandatory()
    {
        return true;
    }


    @Override
    protected String getSequences()
    {
        return "choiceBackground,choiceStandardDimension,ChoiceDoorSlabModel,steelDoorOptions,choiceSlabSidelightWidth,nbDoors,nbSideLight,slabDoorWidth,slabSidelightWidth,profilesOS,profilesNOS,sillProfiles,NonStandardDimensions,doorMullionFix,doorFrame,choiceSideLightSide,choiceSwing,interiorColor,sideLight,choiceBuildType,door,exteriorColor,opening,paintable,renderingNumber,dimension,post,choiceStandardDimension,choiceIllusionOpening,choiceSlabDoorWidth,ChoiceSideLightSlabModel,choiceBoxType,choicePaintingSide";  
    }
}
