// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.client360.configuration.wad.enums.ChoiceFrameDepth;
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class CasementTiltTurnBase extends com.client360.configuration.wad.CasementMain
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(CasementTiltTurnBase.class);

    // attributes
    private ValueContainer<ChoiceFrameDepth> choiceFrameDepth = new ValueContainer<ChoiceFrameDepth>(this);

    // Bound properties

    public static final String PROPERTYNAME_CHOICEFRAMEDEPTH = "choiceFrameDepth";  

    // Constructors

    public CasementTiltTurnBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.CasementTiltTurn"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="desc", value="Frame")
        ,@LocalizedTag(language="fr", name="desc", value="Cadre")
        ,@LocalizedTag(language="en", name="label", value="Frame")
        ,@LocalizedTag(language="fr", name="label", value="Cadre")
    })
    public final ChoiceFrameDepth getChoiceFrameDepth()
    {
         return this.choiceFrameDepth.getValue();
    }

    public final void setChoiceFrameDepth(ChoiceFrameDepth choiceFrameDepth)
    {

        ChoiceFrameDepth oldValue = this.choiceFrameDepth.getValue();
        this.choiceFrameDepth.setValue(choiceFrameDepth);

        ChoiceFrameDepth currentValue = this.choiceFrameDepth.getValue();

        fireChoiceFrameDepthChange(currentValue, oldValue);
    }
    public final ChoiceFrameDepth removeChoiceFrameDepth()
    {
        ChoiceFrameDepth oldValue = this.choiceFrameDepth.getValue();
        ChoiceFrameDepth removedValue = this.choiceFrameDepth.removeValue();
        ChoiceFrameDepth currentValue = this.choiceFrameDepth.getValue();

        fireChoiceFrameDepthChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceFrameDepth(ChoiceFrameDepth choiceFrameDepth)
    {
        ChoiceFrameDepth oldValue = this.choiceFrameDepth.getValue();
        this.choiceFrameDepth.setDefaultValue(choiceFrameDepth);

        ChoiceFrameDepth currentValue = this.choiceFrameDepth.getValue();

        fireChoiceFrameDepthChange(currentValue, oldValue);
    }
    
    public final ChoiceFrameDepth removeDefaultChoiceFrameDepth()
    {
        ChoiceFrameDepth oldValue = this.choiceFrameDepth.getValue();
        ChoiceFrameDepth removedValue = this.choiceFrameDepth.removeDefaultValue();
        ChoiceFrameDepth currentValue = this.choiceFrameDepth.getValue();

        fireChoiceFrameDepthChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceFrameDepth> getChoiceFrameDepthValueContainer()
    {
        return this.choiceFrameDepth;
    }
    public final void fireChoiceFrameDepthChange(ChoiceFrameDepth currentValue, ChoiceFrameDepth oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceFrameDepthChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEFRAMEDEPTH, oldValue, currentValue);
            afterChoiceFrameDepthChanged(currentValue);
        }
    }

    public void beforeChoiceFrameDepthChanged( ChoiceFrameDepth choiceFrameDepth)
    { }
    public void afterChoiceFrameDepthChanged( ChoiceFrameDepth choiceFrameDepth)
    { }


    public boolean isChoiceFrameDepthMandatory()
    {
        return true;
    }


    @Override
    protected String getSequences()
    {
        return "energyStar,noFrame,frameDepth,installed,nbSections,qDQ,sameGrilles,showMeetingRail,showMullion,profiles,InteriorColor,ExteriorColor,windowsMullion,advancedSectionGrilles,sash,paintable,renderingNumber,dimension,sectionInput,section,choicePresetWindowWidth,division,meetingRail,option";  
    }
}
