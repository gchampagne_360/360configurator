// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 


public abstract class BayBowBase extends com.netappsid.wadconfigurator.BayBow
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(BayBowBase.class);

    // attributes

    // Bound properties


    // Constructors

    public BayBowBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.BayBow"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations



    @Override
    protected String getSequences()
    {
        return "nbSections,dimension,sectionInput,choicePresetWindowWidth,energyStar,installed,qDQ,sameGrilles,showMeetingRail,showMullion,meetingRail,division,windowsMullion,sash,ExteriorColor,paintable,section,InteriorColor,window,bayBowSpecs,renderingNumber,frameDepth";  
    }
}
