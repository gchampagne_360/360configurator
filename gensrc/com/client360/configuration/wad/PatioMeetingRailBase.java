// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 


public abstract class PatioMeetingRailBase extends com.netappsid.wadconfigurator.PatioMeetingRail
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(PatioMeetingRailBase.class);

    // attributes

    // Bound properties


    // Constructors

    public PatioMeetingRailBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.PatioMeetingRail"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations


}
