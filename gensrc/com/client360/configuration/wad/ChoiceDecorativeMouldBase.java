// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.DynamicEnum;
import com.netappsid.erp.configurator.annotations.Filtrable;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

@DynamicEnum(fileName = "dynamicEnums/PA_CombinaisonsPanneauxPalaceVictor2009.tsv")
public abstract class ChoiceDecorativeMouldBase extends com.netappsid.erp.configurator.Configurable
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ChoiceDecorativeMouldBase.class);

    // attributes
    private ValueContainer<String> description = new ValueContainer<String>(this);
    private ValueContainer<String> isSlabDoor = new ValueContainer<String>(this);
    private ValueContainer<String> isSideLight = new ValueContainer<String>(this);
    private ValueContainer<String> slabDoorModels = new ValueContainer<String>(this);
    private ValueContainer<String> sideLightModels = new ValueContainer<String>(this);
    private ValueContainer<String> codes = new ValueContainer<String>(this);
    private ValueContainer<String> positions = new ValueContainer<String>(this);

    // Bound properties

    public static final String PROPERTYNAME_DESCRIPTION = "description";  
    public static final String PROPERTYNAME_ISSLABDOOR = "isSlabDoor";  
    public static final String PROPERTYNAME_ISSIDELIGHT = "isSideLight";  
    public static final String PROPERTYNAME_SLABDOORMODELS = "slabDoorModels";  
    public static final String PROPERTYNAME_SIDELIGHTMODELS = "sideLightModels";  
    public static final String PROPERTYNAME_CODES = "codes";  
    public static final String PROPERTYNAME_POSITIONS = "positions";  

    // Constructors

    public ChoiceDecorativeMouldBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.ChoiceDecorativeMould"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @Filtrable(value = "true", defaultValue = "", defaultSelection = "")
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="1")
        ,@LocalizedTag(language="fr", name="column", value="0")
        ,@LocalizedTag(language="en", name="label", value="Description")
        ,@LocalizedTag(language="fr", name="label", value="Description")
    })
    public final String getDescription()
    {
         return this.description.getValue();
    }

    public final void setDescription(String description)
    {

        String oldValue = this.description.getValue();
        this.description.setValue(description);

        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
    }
    public final String removeDescription()
    {
        String oldValue = this.description.getValue();
        String removedValue = this.description.removeValue();
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultDescription(String description)
    {
        String oldValue = this.description.getValue();
        this.description.setDefaultValue(description);

        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
    }
    
    public final String removeDefaultDescription()
    {
        String oldValue = this.description.getValue();
        String removedValue = this.description.removeDefaultValue();
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getDescriptionValueContainer()
    {
        return this.description;
    }
    public final void fireDescriptionChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeDescriptionChanged(currentValue);
            firePropertyChange(PROPERTYNAME_DESCRIPTION, oldValue, currentValue);
            afterDescriptionChanged(currentValue);
        }
    }

    public void beforeDescriptionChanged( String description)
    { }
    public void afterDescriptionChanged( String description)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="2")
        ,@LocalizedTag(language="fr", name="column", value="2")
        ,@LocalizedTag(language="en", name="label", value="isSlabDoor")
        ,@LocalizedTag(language="fr", name="label", value="isSlabDoor")
    })
    public final String getIsSlabDoor()
    {
         return this.isSlabDoor.getValue();
    }

    public final void setIsSlabDoor(String isSlabDoor)
    {

        String oldValue = this.isSlabDoor.getValue();
        this.isSlabDoor.setValue(isSlabDoor);

        String currentValue = this.isSlabDoor.getValue();

        fireIsSlabDoorChange(currentValue, oldValue);
    }
    public final String removeIsSlabDoor()
    {
        String oldValue = this.isSlabDoor.getValue();
        String removedValue = this.isSlabDoor.removeValue();
        String currentValue = this.isSlabDoor.getValue();

        fireIsSlabDoorChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultIsSlabDoor(String isSlabDoor)
    {
        String oldValue = this.isSlabDoor.getValue();
        this.isSlabDoor.setDefaultValue(isSlabDoor);

        String currentValue = this.isSlabDoor.getValue();

        fireIsSlabDoorChange(currentValue, oldValue);
    }
    
    public final String removeDefaultIsSlabDoor()
    {
        String oldValue = this.isSlabDoor.getValue();
        String removedValue = this.isSlabDoor.removeDefaultValue();
        String currentValue = this.isSlabDoor.getValue();

        fireIsSlabDoorChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getIsSlabDoorValueContainer()
    {
        return this.isSlabDoor;
    }
    public final void fireIsSlabDoorChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeIsSlabDoorChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ISSLABDOOR, oldValue, currentValue);
            afterIsSlabDoorChanged(currentValue);
        }
    }

    public void beforeIsSlabDoorChanged( String isSlabDoor)
    { }
    public void afterIsSlabDoorChanged( String isSlabDoor)
    { }


    public boolean isIsSlabDoorVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="3")
        ,@LocalizedTag(language="fr", name="column", value="3")
        ,@LocalizedTag(language="en", name="label", value="isSideLight")
        ,@LocalizedTag(language="fr", name="label", value="isSideLight")
    })
    public final String getIsSideLight()
    {
         return this.isSideLight.getValue();
    }

    public final void setIsSideLight(String isSideLight)
    {

        String oldValue = this.isSideLight.getValue();
        this.isSideLight.setValue(isSideLight);

        String currentValue = this.isSideLight.getValue();

        fireIsSideLightChange(currentValue, oldValue);
    }
    public final String removeIsSideLight()
    {
        String oldValue = this.isSideLight.getValue();
        String removedValue = this.isSideLight.removeValue();
        String currentValue = this.isSideLight.getValue();

        fireIsSideLightChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultIsSideLight(String isSideLight)
    {
        String oldValue = this.isSideLight.getValue();
        this.isSideLight.setDefaultValue(isSideLight);

        String currentValue = this.isSideLight.getValue();

        fireIsSideLightChange(currentValue, oldValue);
    }
    
    public final String removeDefaultIsSideLight()
    {
        String oldValue = this.isSideLight.getValue();
        String removedValue = this.isSideLight.removeDefaultValue();
        String currentValue = this.isSideLight.getValue();

        fireIsSideLightChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getIsSideLightValueContainer()
    {
        return this.isSideLight;
    }
    public final void fireIsSideLightChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeIsSideLightChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ISSIDELIGHT, oldValue, currentValue);
            afterIsSideLightChanged(currentValue);
        }
    }

    public void beforeIsSideLightChanged( String isSideLight)
    { }
    public void afterIsSideLightChanged( String isSideLight)
    { }


    public boolean isIsSideLightVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="4")
        ,@LocalizedTag(language="fr", name="column", value="4")
        ,@LocalizedTag(language="en", name="label", value="slabDoorModels")
        ,@LocalizedTag(language="fr", name="label", value="slabDoorModels")
    })
    public final String getSlabDoorModels()
    {
         return this.slabDoorModels.getValue();
    }

    public final void setSlabDoorModels(String slabDoorModels)
    {

        String oldValue = this.slabDoorModels.getValue();
        this.slabDoorModels.setValue(slabDoorModels);

        String currentValue = this.slabDoorModels.getValue();

        fireSlabDoorModelsChange(currentValue, oldValue);
    }
    public final String removeSlabDoorModels()
    {
        String oldValue = this.slabDoorModels.getValue();
        String removedValue = this.slabDoorModels.removeValue();
        String currentValue = this.slabDoorModels.getValue();

        fireSlabDoorModelsChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultSlabDoorModels(String slabDoorModels)
    {
        String oldValue = this.slabDoorModels.getValue();
        this.slabDoorModels.setDefaultValue(slabDoorModels);

        String currentValue = this.slabDoorModels.getValue();

        fireSlabDoorModelsChange(currentValue, oldValue);
    }
    
    public final String removeDefaultSlabDoorModels()
    {
        String oldValue = this.slabDoorModels.getValue();
        String removedValue = this.slabDoorModels.removeDefaultValue();
        String currentValue = this.slabDoorModels.getValue();

        fireSlabDoorModelsChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getSlabDoorModelsValueContainer()
    {
        return this.slabDoorModels;
    }
    public final void fireSlabDoorModelsChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeSlabDoorModelsChanged(currentValue);
            firePropertyChange(PROPERTYNAME_SLABDOORMODELS, oldValue, currentValue);
            afterSlabDoorModelsChanged(currentValue);
        }
    }

    public void beforeSlabDoorModelsChanged( String slabDoorModels)
    { }
    public void afterSlabDoorModelsChanged( String slabDoorModels)
    { }


    public boolean isSlabDoorModelsVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="5")
        ,@LocalizedTag(language="fr", name="column", value="5")
        ,@LocalizedTag(language="en", name="label", value="sideLightModels")
        ,@LocalizedTag(language="fr", name="label", value="sideLightModels")
    })
    public final String getSideLightModels()
    {
         return this.sideLightModels.getValue();
    }

    public final void setSideLightModels(String sideLightModels)
    {

        String oldValue = this.sideLightModels.getValue();
        this.sideLightModels.setValue(sideLightModels);

        String currentValue = this.sideLightModels.getValue();

        fireSideLightModelsChange(currentValue, oldValue);
    }
    public final String removeSideLightModels()
    {
        String oldValue = this.sideLightModels.getValue();
        String removedValue = this.sideLightModels.removeValue();
        String currentValue = this.sideLightModels.getValue();

        fireSideLightModelsChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultSideLightModels(String sideLightModels)
    {
        String oldValue = this.sideLightModels.getValue();
        this.sideLightModels.setDefaultValue(sideLightModels);

        String currentValue = this.sideLightModels.getValue();

        fireSideLightModelsChange(currentValue, oldValue);
    }
    
    public final String removeDefaultSideLightModels()
    {
        String oldValue = this.sideLightModels.getValue();
        String removedValue = this.sideLightModels.removeDefaultValue();
        String currentValue = this.sideLightModels.getValue();

        fireSideLightModelsChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getSideLightModelsValueContainer()
    {
        return this.sideLightModels;
    }
    public final void fireSideLightModelsChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeSideLightModelsChanged(currentValue);
            firePropertyChange(PROPERTYNAME_SIDELIGHTMODELS, oldValue, currentValue);
            afterSideLightModelsChanged(currentValue);
        }
    }

    public void beforeSideLightModelsChanged( String sideLightModels)
    { }
    public void afterSideLightModelsChanged( String sideLightModels)
    { }


    public boolean isSideLightModelsVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="6")
        ,@LocalizedTag(language="fr", name="column", value="6")
        ,@LocalizedTag(language="en", name="label", value="Codes")
        ,@LocalizedTag(language="fr", name="label", value="Codes")
    })
    public final String getCodes()
    {
         return this.codes.getValue();
    }

    public final void setCodes(String codes)
    {

        String oldValue = this.codes.getValue();
        this.codes.setValue(codes);

        String currentValue = this.codes.getValue();

        fireCodesChange(currentValue, oldValue);
    }
    public final String removeCodes()
    {
        String oldValue = this.codes.getValue();
        String removedValue = this.codes.removeValue();
        String currentValue = this.codes.getValue();

        fireCodesChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultCodes(String codes)
    {
        String oldValue = this.codes.getValue();
        this.codes.setDefaultValue(codes);

        String currentValue = this.codes.getValue();

        fireCodesChange(currentValue, oldValue);
    }
    
    public final String removeDefaultCodes()
    {
        String oldValue = this.codes.getValue();
        String removedValue = this.codes.removeDefaultValue();
        String currentValue = this.codes.getValue();

        fireCodesChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getCodesValueContainer()
    {
        return this.codes;
    }
    public final void fireCodesChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeCodesChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CODES, oldValue, currentValue);
            afterCodesChanged(currentValue);
        }
    }

    public void beforeCodesChanged( String codes)
    { }
    public void afterCodesChanged( String codes)
    { }


    public boolean isCodesVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="7")
        ,@LocalizedTag(language="fr", name="column", value="7")
        ,@LocalizedTag(language="en", name="label", value="Positions")
        ,@LocalizedTag(language="fr", name="label", value="Positions")
    })
    public final String getPositions()
    {
         return this.positions.getValue();
    }

    public final void setPositions(String positions)
    {

        String oldValue = this.positions.getValue();
        this.positions.setValue(positions);

        String currentValue = this.positions.getValue();

        firePositionsChange(currentValue, oldValue);
    }
    public final String removePositions()
    {
        String oldValue = this.positions.getValue();
        String removedValue = this.positions.removeValue();
        String currentValue = this.positions.getValue();

        firePositionsChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultPositions(String positions)
    {
        String oldValue = this.positions.getValue();
        this.positions.setDefaultValue(positions);

        String currentValue = this.positions.getValue();

        firePositionsChange(currentValue, oldValue);
    }
    
    public final String removeDefaultPositions()
    {
        String oldValue = this.positions.getValue();
        String removedValue = this.positions.removeDefaultValue();
        String currentValue = this.positions.getValue();

        firePositionsChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getPositionsValueContainer()
    {
        return this.positions;
    }
    public final void firePositionsChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforePositionsChanged(currentValue);
            firePropertyChange(PROPERTYNAME_POSITIONS, oldValue, currentValue);
            afterPositionsChanged(currentValue);
        }
    }

    public void beforePositionsChanged( String positions)
    { }
    public void afterPositionsChanged( String positions)
    { }


    public boolean isPositionsVisible()
    {
        return false;
    }

}
