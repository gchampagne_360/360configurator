// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.DynamicEnum;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

@DynamicEnum(fileName = "slabs", locale = "en")
public abstract class ChoiceSlab360Base extends com.netappsid.erp.configurator.Configurable
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ChoiceSlab360Base.class);

    // attributes
    private ValueContainer<String> description = new ValueContainer<String>(this);
    private ValueContainer<String> parametricModelClass = new ValueContainer<String>(this);
    private ValueContainer<String> code = new ValueContainer<String>(this);
    private ValueContainer<String> type = new ValueContainer<String>(this);
    private ValueContainer<String> frame = new ValueContainer<String>(this);
    private ValueContainer<String> acc = new ValueContainer<String>(this);
    private ValueContainer<String> renduPbh = new ValueContainer<String>(this);
    private ValueContainer<String> renduPbv = new ValueContainer<String>(this);
    private ValueContainer<String> renduLarPbe = new ValueContainer<String>(this);
    private ValueContainer<String> renduLarMoulure = new ValueContainer<String>(this);
    private ValueContainer<String> imageGrille = new ValueContainer<String>(this);

    // Bound properties

    public static final String PROPERTYNAME_DESCRIPTION = "description";  
    public static final String PROPERTYNAME_PARAMETRICMODELCLASS = "parametricModelClass";  
    public static final String PROPERTYNAME_CODE = "code";  
    public static final String PROPERTYNAME_TYPE = "type";  
    public static final String PROPERTYNAME_FRAME = "frame";  
    public static final String PROPERTYNAME_ACC = "acc";  
    public static final String PROPERTYNAME_RENDUPBH = "renduPbh";  
    public static final String PROPERTYNAME_RENDUPBV = "renduPbv";  
    public static final String PROPERTYNAME_RENDULARPBE = "renduLarPbe";  
    public static final String PROPERTYNAME_RENDULARMOULURE = "renduLarMoulure";  
    public static final String PROPERTYNAME_IMAGEGRILLE = "imageGrille";  

    // Constructors

    public ChoiceSlab360Base(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.ChoiceSlab360"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="0")
        ,@LocalizedTag(language="fr", name="column", value="1")
        ,@LocalizedTag(language="en", name="label", value="Description")
        ,@LocalizedTag(language="fr", name="label", value="Description")
    })
    public final String getDescription()
    {
         return this.description.getValue();
    }

    public final void setDescription(String description)
    {

        String oldValue = this.description.getValue();
        this.description.setValue(description);

        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
    }
    public final String removeDescription()
    {
        String oldValue = this.description.getValue();
        String removedValue = this.description.removeValue();
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultDescription(String description)
    {
        String oldValue = this.description.getValue();
        this.description.setDefaultValue(description);

        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
    }
    
    public final String removeDefaultDescription()
    {
        String oldValue = this.description.getValue();
        String removedValue = this.description.removeDefaultValue();
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getDescriptionValueContainer()
    {
        return this.description;
    }
    public final void fireDescriptionChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeDescriptionChanged(currentValue);
            firePropertyChange(PROPERTYNAME_DESCRIPTION, oldValue, currentValue);
            afterDescriptionChanged(currentValue);
        }
    }

    public void beforeDescriptionChanged( String description)
    { }
    public void afterDescriptionChanged( String description)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="2")
        ,@LocalizedTag(language="fr", name="column", value="2")
    })
    public final String getParametricModelClass()
    {
         return this.parametricModelClass.getValue();
    }

    public final void setParametricModelClass(String parametricModelClass)
    {

        String oldValue = this.parametricModelClass.getValue();
        this.parametricModelClass.setValue(parametricModelClass);

        String currentValue = this.parametricModelClass.getValue();

        fireParametricModelClassChange(currentValue, oldValue);
    }
    public final String removeParametricModelClass()
    {
        String oldValue = this.parametricModelClass.getValue();
        String removedValue = this.parametricModelClass.removeValue();
        String currentValue = this.parametricModelClass.getValue();

        fireParametricModelClassChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultParametricModelClass(String parametricModelClass)
    {
        String oldValue = this.parametricModelClass.getValue();
        this.parametricModelClass.setDefaultValue(parametricModelClass);

        String currentValue = this.parametricModelClass.getValue();

        fireParametricModelClassChange(currentValue, oldValue);
    }
    
    public final String removeDefaultParametricModelClass()
    {
        String oldValue = this.parametricModelClass.getValue();
        String removedValue = this.parametricModelClass.removeDefaultValue();
        String currentValue = this.parametricModelClass.getValue();

        fireParametricModelClassChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getParametricModelClassValueContainer()
    {
        return this.parametricModelClass;
    }
    public final void fireParametricModelClassChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeParametricModelClassChanged(currentValue);
            firePropertyChange(PROPERTYNAME_PARAMETRICMODELCLASS, oldValue, currentValue);
            afterParametricModelClassChanged(currentValue);
        }
    }

    public void beforeParametricModelClassChanged( String parametricModelClass)
    { }
    public void afterParametricModelClassChanged( String parametricModelClass)
    { }


    public boolean isParametricModelClassVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="3")
        ,@LocalizedTag(language="fr", name="column", value="3")
        ,@LocalizedTag(language="en", name="label", value="Code")
        ,@LocalizedTag(language="fr", name="label", value="Code")
    })
    public final String getCode()
    {
         return this.code.getValue();
    }

    public final void setCode(String code)
    {

        String oldValue = this.code.getValue();
        this.code.setValue(code);

        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
    }
    public final String removeCode()
    {
        String oldValue = this.code.getValue();
        String removedValue = this.code.removeValue();
        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultCode(String code)
    {
        String oldValue = this.code.getValue();
        this.code.setDefaultValue(code);

        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
    }
    
    public final String removeDefaultCode()
    {
        String oldValue = this.code.getValue();
        String removedValue = this.code.removeDefaultValue();
        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getCodeValueContainer()
    {
        return this.code;
    }
    public final void fireCodeChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeCodeChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CODE, oldValue, currentValue);
            afterCodeChanged(currentValue);
        }
    }

    public void beforeCodeChanged( String code)
    { }
    public void afterCodeChanged( String code)
    { }


    public boolean isCodeVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="4")
        ,@LocalizedTag(language="fr", name="column", value="4")
        ,@LocalizedTag(language="en", name="label", value="Type")
        ,@LocalizedTag(language="fr", name="label", value="Type")
    })
    public final String getType()
    {
         return this.type.getValue();
    }

    public final void setType(String type)
    {

        String oldValue = this.type.getValue();
        this.type.setValue(type);

        String currentValue = this.type.getValue();

        fireTypeChange(currentValue, oldValue);
    }
    public final String removeType()
    {
        String oldValue = this.type.getValue();
        String removedValue = this.type.removeValue();
        String currentValue = this.type.getValue();

        fireTypeChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultType(String type)
    {
        String oldValue = this.type.getValue();
        this.type.setDefaultValue(type);

        String currentValue = this.type.getValue();

        fireTypeChange(currentValue, oldValue);
    }
    
    public final String removeDefaultType()
    {
        String oldValue = this.type.getValue();
        String removedValue = this.type.removeDefaultValue();
        String currentValue = this.type.getValue();

        fireTypeChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getTypeValueContainer()
    {
        return this.type;
    }
    public final void fireTypeChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeTypeChanged(currentValue);
            firePropertyChange(PROPERTYNAME_TYPE, oldValue, currentValue);
            afterTypeChanged(currentValue);
        }
    }

    public void beforeTypeChanged( String type)
    { }
    public void afterTypeChanged( String type)
    { }


    public boolean isTypeVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="5")
        ,@LocalizedTag(language="fr", name="column", value="5")
        ,@LocalizedTag(language="en", name="label", value="Frame")
        ,@LocalizedTag(language="fr", name="label", value="Batis")
    })
    public final String getFrame()
    {
         return this.frame.getValue();
    }

    public final void setFrame(String frame)
    {

        String oldValue = this.frame.getValue();
        this.frame.setValue(frame);

        String currentValue = this.frame.getValue();

        fireFrameChange(currentValue, oldValue);
    }
    public final String removeFrame()
    {
        String oldValue = this.frame.getValue();
        String removedValue = this.frame.removeValue();
        String currentValue = this.frame.getValue();

        fireFrameChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultFrame(String frame)
    {
        String oldValue = this.frame.getValue();
        this.frame.setDefaultValue(frame);

        String currentValue = this.frame.getValue();

        fireFrameChange(currentValue, oldValue);
    }
    
    public final String removeDefaultFrame()
    {
        String oldValue = this.frame.getValue();
        String removedValue = this.frame.removeDefaultValue();
        String currentValue = this.frame.getValue();

        fireFrameChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getFrameValueContainer()
    {
        return this.frame;
    }
    public final void fireFrameChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeFrameChanged(currentValue);
            firePropertyChange(PROPERTYNAME_FRAME, oldValue, currentValue);
            afterFrameChanged(currentValue);
        }
    }

    public void beforeFrameChanged( String frame)
    { }
    public void afterFrameChanged( String frame)
    { }


    public boolean isFrameVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="6")
        ,@LocalizedTag(language="fr", name="column", value="6")
        ,@LocalizedTag(language="en", name="label", value="Accessoire")
        ,@LocalizedTag(language="fr", name="label", value="Accessories")
    })
    public final String getAcc()
    {
         return this.acc.getValue();
    }

    public final void setAcc(String acc)
    {

        String oldValue = this.acc.getValue();
        this.acc.setValue(acc);

        String currentValue = this.acc.getValue();

        fireAccChange(currentValue, oldValue);
    }
    public final String removeAcc()
    {
        String oldValue = this.acc.getValue();
        String removedValue = this.acc.removeValue();
        String currentValue = this.acc.getValue();

        fireAccChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultAcc(String acc)
    {
        String oldValue = this.acc.getValue();
        this.acc.setDefaultValue(acc);

        String currentValue = this.acc.getValue();

        fireAccChange(currentValue, oldValue);
    }
    
    public final String removeDefaultAcc()
    {
        String oldValue = this.acc.getValue();
        String removedValue = this.acc.removeDefaultValue();
        String currentValue = this.acc.getValue();

        fireAccChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getAccValueContainer()
    {
        return this.acc;
    }
    public final void fireAccChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeAccChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ACC, oldValue, currentValue);
            afterAccChanged(currentValue);
        }
    }

    public void beforeAccChanged( String acc)
    { }
    public void afterAccChanged( String acc)
    { }


    public boolean isAccVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="7")
        ,@LocalizedTag(language="fr", name="column", value="7")
    })
    public final String getRenduPbh()
    {
         return this.renduPbh.getValue();
    }

    public final void setRenduPbh(String renduPbh)
    {

        String oldValue = this.renduPbh.getValue();
        this.renduPbh.setValue(renduPbh);

        String currentValue = this.renduPbh.getValue();

        fireRenduPbhChange(currentValue, oldValue);
    }
    public final String removeRenduPbh()
    {
        String oldValue = this.renduPbh.getValue();
        String removedValue = this.renduPbh.removeValue();
        String currentValue = this.renduPbh.getValue();

        fireRenduPbhChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultRenduPbh(String renduPbh)
    {
        String oldValue = this.renduPbh.getValue();
        this.renduPbh.setDefaultValue(renduPbh);

        String currentValue = this.renduPbh.getValue();

        fireRenduPbhChange(currentValue, oldValue);
    }
    
    public final String removeDefaultRenduPbh()
    {
        String oldValue = this.renduPbh.getValue();
        String removedValue = this.renduPbh.removeDefaultValue();
        String currentValue = this.renduPbh.getValue();

        fireRenduPbhChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getRenduPbhValueContainer()
    {
        return this.renduPbh;
    }
    public final void fireRenduPbhChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeRenduPbhChanged(currentValue);
            firePropertyChange(PROPERTYNAME_RENDUPBH, oldValue, currentValue);
            afterRenduPbhChanged(currentValue);
        }
    }

    public void beforeRenduPbhChanged( String renduPbh)
    { }
    public void afterRenduPbhChanged( String renduPbh)
    { }


    public boolean isRenduPbhVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="8")
        ,@LocalizedTag(language="fr", name="column", value="8")
    })
    public final String getRenduPbv()
    {
         return this.renduPbv.getValue();
    }

    public final void setRenduPbv(String renduPbv)
    {

        String oldValue = this.renduPbv.getValue();
        this.renduPbv.setValue(renduPbv);

        String currentValue = this.renduPbv.getValue();

        fireRenduPbvChange(currentValue, oldValue);
    }
    public final String removeRenduPbv()
    {
        String oldValue = this.renduPbv.getValue();
        String removedValue = this.renduPbv.removeValue();
        String currentValue = this.renduPbv.getValue();

        fireRenduPbvChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultRenduPbv(String renduPbv)
    {
        String oldValue = this.renduPbv.getValue();
        this.renduPbv.setDefaultValue(renduPbv);

        String currentValue = this.renduPbv.getValue();

        fireRenduPbvChange(currentValue, oldValue);
    }
    
    public final String removeDefaultRenduPbv()
    {
        String oldValue = this.renduPbv.getValue();
        String removedValue = this.renduPbv.removeDefaultValue();
        String currentValue = this.renduPbv.getValue();

        fireRenduPbvChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getRenduPbvValueContainer()
    {
        return this.renduPbv;
    }
    public final void fireRenduPbvChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeRenduPbvChanged(currentValue);
            firePropertyChange(PROPERTYNAME_RENDUPBV, oldValue, currentValue);
            afterRenduPbvChanged(currentValue);
        }
    }

    public void beforeRenduPbvChanged( String renduPbv)
    { }
    public void afterRenduPbvChanged( String renduPbv)
    { }


    public boolean isRenduPbvVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="9")
        ,@LocalizedTag(language="fr", name="column", value="9")
    })
    public final String getRenduLarPbe()
    {
         return this.renduLarPbe.getValue();
    }

    public final void setRenduLarPbe(String renduLarPbe)
    {

        String oldValue = this.renduLarPbe.getValue();
        this.renduLarPbe.setValue(renduLarPbe);

        String currentValue = this.renduLarPbe.getValue();

        fireRenduLarPbeChange(currentValue, oldValue);
    }
    public final String removeRenduLarPbe()
    {
        String oldValue = this.renduLarPbe.getValue();
        String removedValue = this.renduLarPbe.removeValue();
        String currentValue = this.renduLarPbe.getValue();

        fireRenduLarPbeChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultRenduLarPbe(String renduLarPbe)
    {
        String oldValue = this.renduLarPbe.getValue();
        this.renduLarPbe.setDefaultValue(renduLarPbe);

        String currentValue = this.renduLarPbe.getValue();

        fireRenduLarPbeChange(currentValue, oldValue);
    }
    
    public final String removeDefaultRenduLarPbe()
    {
        String oldValue = this.renduLarPbe.getValue();
        String removedValue = this.renduLarPbe.removeDefaultValue();
        String currentValue = this.renduLarPbe.getValue();

        fireRenduLarPbeChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getRenduLarPbeValueContainer()
    {
        return this.renduLarPbe;
    }
    public final void fireRenduLarPbeChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeRenduLarPbeChanged(currentValue);
            firePropertyChange(PROPERTYNAME_RENDULARPBE, oldValue, currentValue);
            afterRenduLarPbeChanged(currentValue);
        }
    }

    public void beforeRenduLarPbeChanged( String renduLarPbe)
    { }
    public void afterRenduLarPbeChanged( String renduLarPbe)
    { }


    public boolean isRenduLarPbeVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="10")
        ,@LocalizedTag(language="fr", name="column", value="10")
    })
    public final String getRenduLarMoulure()
    {
         return this.renduLarMoulure.getValue();
    }

    public final void setRenduLarMoulure(String renduLarMoulure)
    {

        String oldValue = this.renduLarMoulure.getValue();
        this.renduLarMoulure.setValue(renduLarMoulure);

        String currentValue = this.renduLarMoulure.getValue();

        fireRenduLarMoulureChange(currentValue, oldValue);
    }
    public final String removeRenduLarMoulure()
    {
        String oldValue = this.renduLarMoulure.getValue();
        String removedValue = this.renduLarMoulure.removeValue();
        String currentValue = this.renduLarMoulure.getValue();

        fireRenduLarMoulureChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultRenduLarMoulure(String renduLarMoulure)
    {
        String oldValue = this.renduLarMoulure.getValue();
        this.renduLarMoulure.setDefaultValue(renduLarMoulure);

        String currentValue = this.renduLarMoulure.getValue();

        fireRenduLarMoulureChange(currentValue, oldValue);
    }
    
    public final String removeDefaultRenduLarMoulure()
    {
        String oldValue = this.renduLarMoulure.getValue();
        String removedValue = this.renduLarMoulure.removeDefaultValue();
        String currentValue = this.renduLarMoulure.getValue();

        fireRenduLarMoulureChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getRenduLarMoulureValueContainer()
    {
        return this.renduLarMoulure;
    }
    public final void fireRenduLarMoulureChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeRenduLarMoulureChanged(currentValue);
            firePropertyChange(PROPERTYNAME_RENDULARMOULURE, oldValue, currentValue);
            afterRenduLarMoulureChanged(currentValue);
        }
    }

    public void beforeRenduLarMoulureChanged( String renduLarMoulure)
    { }
    public void afterRenduLarMoulureChanged( String renduLarMoulure)
    { }


    public boolean isRenduLarMoulureVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="11")
        ,@LocalizedTag(language="fr", name="column", value="11")
    })
    public final String getImageGrille()
    {
         return this.imageGrille.getValue();
    }

    public final void setImageGrille(String imageGrille)
    {

        String oldValue = this.imageGrille.getValue();
        this.imageGrille.setValue(imageGrille);

        String currentValue = this.imageGrille.getValue();

        fireImageGrilleChange(currentValue, oldValue);
    }
    public final String removeImageGrille()
    {
        String oldValue = this.imageGrille.getValue();
        String removedValue = this.imageGrille.removeValue();
        String currentValue = this.imageGrille.getValue();

        fireImageGrilleChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultImageGrille(String imageGrille)
    {
        String oldValue = this.imageGrille.getValue();
        this.imageGrille.setDefaultValue(imageGrille);

        String currentValue = this.imageGrille.getValue();

        fireImageGrilleChange(currentValue, oldValue);
    }
    
    public final String removeDefaultImageGrille()
    {
        String oldValue = this.imageGrille.getValue();
        String removedValue = this.imageGrille.removeDefaultValue();
        String currentValue = this.imageGrille.getValue();

        fireImageGrilleChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getImageGrilleValueContainer()
    {
        return this.imageGrille;
    }
    public final void fireImageGrilleChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeImageGrilleChanged(currentValue);
            firePropertyChange(PROPERTYNAME_IMAGEGRILLE, oldValue, currentValue);
            afterImageGrilleChanged(currentValue);
        }
    }

    public void beforeImageGrilleChanged( String imageGrille)
    { }
    public void afterImageGrilleChanged( String imageGrille)
    { }


    public boolean isImageGrilleVisible()
    {
        return false;
    }

}
