// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V13.0.2
//2013-10-11

// Imports 

public abstract class RollingShutterMainBladeBase extends com.netappsid.wadconfigurator.RollingShutterMainBlade
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(RollingShutterMainBladeBase.class);

    // attributes

    // Constructors

    public RollingShutterMainBladeBase(com.netappsid.erp.configurator.Configurable parent) // GENCODE b298fff0-4bff-11e0-b8af-0800200c9a66
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.RollingShutterMainBlade"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations


    // Business methods 



    @Override
    public void setDefaultValues()
    { 
        super.setDefaultValues();
    }

    // Bound properties

}
