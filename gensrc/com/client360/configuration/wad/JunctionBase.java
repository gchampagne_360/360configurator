// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V12.5.1
//2011-05-01

// Imports 

public abstract class JunctionBase extends com.netappsid.wadconfigurator.Junction
{
	// Log4J logger
	protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(JunctionBase.class);

	// attributes

	// Constructors

	public JunctionBase(com.netappsid.erp.configurator.Configurable parent) // GENCODE b298fff0-4bff-11e0-b8af-0800200c9a66
	{
		super(parent);
		if (this.getClass().getName().equals("com.client360.configuration.wad.Junction"))
		{
			// activate listener before setting the default value to listen the changes
			activateListener();

			// Load the default values dynamically
			setDefaultValues();

			// load 'collection' preferences and values coming from prior items in the order
			applyPreferences();
		}

	}

	// Business methods

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
	}

	// Bound properties
}
