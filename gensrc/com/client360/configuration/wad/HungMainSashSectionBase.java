// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTags;
import com.netappsid.wadconfigurator.enums.ChoicePresetWindowWidth;

public abstract class HungMainSashSectionBase extends com.netappsid.wadconfigurator.HungSashSection
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(HungMainSashSectionBase.class);

    // attributes
    private ValueContainer<ChoicePresetWindowWidth> choicePresetWindowWidth = new ValueContainer<ChoicePresetWindowWidth>(this);

    // Bound properties

    public static final String PROPERTYNAME_CHOICEPRESETWINDOWWIDTH = "choicePresetWindowWidth";  

    // Constructors

    public HungMainSashSectionBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.HungMainSashSection"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
    })
    public final ChoicePresetWindowWidth getChoicePresetWindowWidth()
    {
         return this.choicePresetWindowWidth.getValue();
    }

    public final void setChoicePresetWindowWidth(ChoicePresetWindowWidth choicePresetWindowWidth)
    {

        ChoicePresetWindowWidth oldValue = this.choicePresetWindowWidth.getValue();
        this.choicePresetWindowWidth.setValue(choicePresetWindowWidth);

        ChoicePresetWindowWidth currentValue = this.choicePresetWindowWidth.getValue();

        fireChoicePresetWindowWidthChange(currentValue, oldValue);
    }
    public final ChoicePresetWindowWidth removeChoicePresetWindowWidth()
    {
        ChoicePresetWindowWidth oldValue = this.choicePresetWindowWidth.getValue();
        ChoicePresetWindowWidth removedValue = this.choicePresetWindowWidth.removeValue();
        ChoicePresetWindowWidth currentValue = this.choicePresetWindowWidth.getValue();

        fireChoicePresetWindowWidthChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoicePresetWindowWidth(ChoicePresetWindowWidth choicePresetWindowWidth)
    {
        ChoicePresetWindowWidth oldValue = this.choicePresetWindowWidth.getValue();
        this.choicePresetWindowWidth.setDefaultValue(choicePresetWindowWidth);

        ChoicePresetWindowWidth currentValue = this.choicePresetWindowWidth.getValue();

        fireChoicePresetWindowWidthChange(currentValue, oldValue);
    }
    
    public final ChoicePresetWindowWidth removeDefaultChoicePresetWindowWidth()
    {
        ChoicePresetWindowWidth oldValue = this.choicePresetWindowWidth.getValue();
        ChoicePresetWindowWidth removedValue = this.choicePresetWindowWidth.removeDefaultValue();
        ChoicePresetWindowWidth currentValue = this.choicePresetWindowWidth.getValue();

        fireChoicePresetWindowWidthChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoicePresetWindowWidth> getChoicePresetWindowWidthValueContainer()
    {
        return this.choicePresetWindowWidth;
    }
    public final void fireChoicePresetWindowWidthChange(ChoicePresetWindowWidth currentValue, ChoicePresetWindowWidth oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoicePresetWindowWidthChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEPRESETWINDOWWIDTH, oldValue, currentValue);
            afterChoicePresetWindowWidthChanged(currentValue);
        }
    }

    public void beforeChoicePresetWindowWidthChanged( ChoicePresetWindowWidth choicePresetWindowWidth)
    { }
    public void afterChoicePresetWindowWidthChanged( ChoicePresetWindowWidth choicePresetWindowWidth)
    { }



    @Override
    protected String getSequences()
    {
        return "insulatedGlass,advancedSectionGrilles,muntin,glass,withInsulatedGlass,not set,dimension";  
    }
}
