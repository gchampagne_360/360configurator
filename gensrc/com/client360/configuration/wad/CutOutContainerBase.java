// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class CutOutContainerBase extends com.netappsid.erp.configurator.Configurable
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(CutOutContainerBase.class);

    // attributes
    private ValueContainer<Boolean> isCustom = new ValueContainer<Boolean>(this);
    private ValueContainer<ChoiceSlabThermos> choiceSlabThermos = new ValueContainer<ChoiceSlabThermos>(this);

    // Bound properties

    public static final String PROPERTYNAME_ISCUSTOM = "isCustom";  
    public static final String PROPERTYNAME_CHOICESLABTHERMOS = "choiceSlabThermos";  

    // Constructors

    public CutOutContainerBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.CutOutContainer"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Customize")
        ,@LocalizedTag(language="fr", name="label", value="Personaliser")
    })
    public final Boolean getIsCustom()
    {
         return this.isCustom.getValue();
    }

    public final void setIsCustom(Boolean isCustom)
    {

        Boolean oldValue = this.isCustom.getValue();
        this.isCustom.setValue(isCustom);

        Boolean currentValue = this.isCustom.getValue();

        fireIsCustomChange(currentValue, oldValue);
    }
    public final Boolean removeIsCustom()
    {
        Boolean oldValue = this.isCustom.getValue();
        Boolean removedValue = this.isCustom.removeValue();
        Boolean currentValue = this.isCustom.getValue();

        fireIsCustomChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultIsCustom(Boolean isCustom)
    {
        Boolean oldValue = this.isCustom.getValue();
        this.isCustom.setDefaultValue(isCustom);

        Boolean currentValue = this.isCustom.getValue();

        fireIsCustomChange(currentValue, oldValue);
    }
    
    public final Boolean removeDefaultIsCustom()
    {
        Boolean oldValue = this.isCustom.getValue();
        Boolean removedValue = this.isCustom.removeDefaultValue();
        Boolean currentValue = this.isCustom.getValue();

        fireIsCustomChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Boolean> getIsCustomValueContainer()
    {
        return this.isCustom;
    }
    public final void fireIsCustomChange(Boolean currentValue, Boolean oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeIsCustomChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ISCUSTOM, oldValue, currentValue);
            afterIsCustomChanged(currentValue);
        }
    }

    public void beforeIsCustomChanged( Boolean isCustom)
    { }
    public void afterIsCustomChanged( Boolean isCustom)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Default CutOut")
        ,@LocalizedTag(language="fr", name="label", value="Per�age d�faut")
    })
    public final ChoiceSlabThermos getChoiceSlabThermos()
    {
         return this.choiceSlabThermos.getValue();
    }

    public final void setChoiceSlabThermos(ChoiceSlabThermos choiceSlabThermos)
    {

        if (choiceSlabThermos != null)
        {
            acquire(choiceSlabThermos, "choiceSlabThermos");
        }
        ChoiceSlabThermos oldValue = this.choiceSlabThermos.getValue();
        this.choiceSlabThermos.setValue(choiceSlabThermos);

        ChoiceSlabThermos currentValue = this.choiceSlabThermos.getValue();

        fireChoiceSlabThermosChange(currentValue, oldValue);
    }
    public final ChoiceSlabThermos removeChoiceSlabThermos()
    {
        ChoiceSlabThermos oldValue = this.choiceSlabThermos.getValue();
        ChoiceSlabThermos removedValue = this.choiceSlabThermos.removeValue();
        ChoiceSlabThermos currentValue = this.choiceSlabThermos.getValue();

        fireChoiceSlabThermosChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceSlabThermos(ChoiceSlabThermos choiceSlabThermos)
    {
        if (choiceSlabThermos != null)
        {
            acquire(choiceSlabThermos, "choiceSlabThermos");
        }
        ChoiceSlabThermos oldValue = this.choiceSlabThermos.getValue();
        this.choiceSlabThermos.setDefaultValue(choiceSlabThermos);

        ChoiceSlabThermos currentValue = this.choiceSlabThermos.getValue();

        fireChoiceSlabThermosChange(currentValue, oldValue);
    }
    
    public final ChoiceSlabThermos removeDefaultChoiceSlabThermos()
    {
        ChoiceSlabThermos oldValue = this.choiceSlabThermos.getValue();
        ChoiceSlabThermos removedValue = this.choiceSlabThermos.removeDefaultValue();
        ChoiceSlabThermos currentValue = this.choiceSlabThermos.getValue();

        fireChoiceSlabThermosChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceSlabThermos> getChoiceSlabThermosValueContainer()
    {
        return this.choiceSlabThermos;
    }
    public final void fireChoiceSlabThermosChange(ChoiceSlabThermos currentValue, ChoiceSlabThermos oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceSlabThermosChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICESLABTHERMOS, oldValue, currentValue);
            afterChoiceSlabThermosChanged(currentValue);
        }
    }

    public void beforeChoiceSlabThermosChanged( ChoiceSlabThermos choiceSlabThermos)
    { }
    public void afterChoiceSlabThermosChanged( ChoiceSlabThermos choiceSlabThermos)
    { }



    @Override
    protected String getSequences()
    {
        return "Custom,choiceSlabThermos,nbCutOut,cutOut";  
    }
}
