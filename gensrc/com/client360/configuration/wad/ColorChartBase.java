// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.client360.configuration.wad.enums.ChoiceWoodTone;
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class ColorChartBase extends com.netappsid.erp.configurator.Configurable
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ColorChartBase.class);

    // attributes
    private ValueContainer<ChoiceWoodTone> choiceWoodTone = new ValueContainer<ChoiceWoodTone>(this);
    private ValueContainer<ChoiceColor> choiceColor = new ValueContainer<ChoiceColor>(this);

    // Bound properties

    public static final String PROPERTYNAME_CHOICEWOODTONE = "choiceWoodTone";  
    public static final String PROPERTYNAME_CHOICECOLOR = "choiceColor";  

    // Constructors

    public ColorChartBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.ColorChart"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Stain")
        ,@LocalizedTag(language="fr", name="label", value="Teinture")
    })
    public final ChoiceWoodTone getChoiceWoodTone()
    {
         return this.choiceWoodTone.getValue();
    }

    public final void setChoiceWoodTone(ChoiceWoodTone choiceWoodTone)
    {

        ChoiceWoodTone oldValue = this.choiceWoodTone.getValue();
        this.choiceWoodTone.setValue(choiceWoodTone);

        ChoiceWoodTone currentValue = this.choiceWoodTone.getValue();

        fireChoiceWoodToneChange(currentValue, oldValue);
    }
    public final ChoiceWoodTone removeChoiceWoodTone()
    {
        ChoiceWoodTone oldValue = this.choiceWoodTone.getValue();
        ChoiceWoodTone removedValue = this.choiceWoodTone.removeValue();
        ChoiceWoodTone currentValue = this.choiceWoodTone.getValue();

        fireChoiceWoodToneChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceWoodTone(ChoiceWoodTone choiceWoodTone)
    {
        ChoiceWoodTone oldValue = this.choiceWoodTone.getValue();
        this.choiceWoodTone.setDefaultValue(choiceWoodTone);

        ChoiceWoodTone currentValue = this.choiceWoodTone.getValue();

        fireChoiceWoodToneChange(currentValue, oldValue);
    }
    
    public final ChoiceWoodTone removeDefaultChoiceWoodTone()
    {
        ChoiceWoodTone oldValue = this.choiceWoodTone.getValue();
        ChoiceWoodTone removedValue = this.choiceWoodTone.removeDefaultValue();
        ChoiceWoodTone currentValue = this.choiceWoodTone.getValue();

        fireChoiceWoodToneChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceWoodTone> getChoiceWoodToneValueContainer()
    {
        return this.choiceWoodTone;
    }
    public final void fireChoiceWoodToneChange(ChoiceWoodTone currentValue, ChoiceWoodTone oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceWoodToneChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEWOODTONE, oldValue, currentValue);
            afterChoiceWoodToneChanged(currentValue);
        }
    }

    public void beforeChoiceWoodToneChanged( ChoiceWoodTone choiceWoodTone)
    { }
    public void afterChoiceWoodToneChanged( ChoiceWoodTone choiceWoodTone)
    { }


    public boolean isChoiceWoodToneMandatory()
    {
        return true;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Color")
        ,@LocalizedTag(language="fr", name="label", value="Couleur")
    })
    public final ChoiceColor getChoiceColor()
    {
         return this.choiceColor.getValue();
    }

    public final void setChoiceColor(ChoiceColor choiceColor)
    {

        if (choiceColor != null)
        {
            acquire(choiceColor, "choiceColor");
        }
        ChoiceColor oldValue = this.choiceColor.getValue();
        this.choiceColor.setValue(choiceColor);

        ChoiceColor currentValue = this.choiceColor.getValue();

        fireChoiceColorChange(currentValue, oldValue);
    }
    public final ChoiceColor removeChoiceColor()
    {
        ChoiceColor oldValue = this.choiceColor.getValue();
        ChoiceColor removedValue = this.choiceColor.removeValue();
        ChoiceColor currentValue = this.choiceColor.getValue();

        fireChoiceColorChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceColor(ChoiceColor choiceColor)
    {
        if (choiceColor != null)
        {
            acquire(choiceColor, "choiceColor");
        }
        ChoiceColor oldValue = this.choiceColor.getValue();
        this.choiceColor.setDefaultValue(choiceColor);

        ChoiceColor currentValue = this.choiceColor.getValue();

        fireChoiceColorChange(currentValue, oldValue);
    }
    
    public final ChoiceColor removeDefaultChoiceColor()
    {
        ChoiceColor oldValue = this.choiceColor.getValue();
        ChoiceColor removedValue = this.choiceColor.removeDefaultValue();
        ChoiceColor currentValue = this.choiceColor.getValue();

        fireChoiceColorChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceColor> getChoiceColorValueContainer()
    {
        return this.choiceColor;
    }
    public final void fireChoiceColorChange(ChoiceColor currentValue, ChoiceColor oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceColorChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICECOLOR, oldValue, currentValue);
            afterChoiceColorChanged(currentValue);
        }
    }

    public void beforeChoiceColorChanged( ChoiceColor choiceColor)
    { }
    public void afterChoiceColorChanged( ChoiceColor choiceColor)
    { }


    public boolean isChoiceColorMandatory()
    {
        return true;
    }


    @Override
    protected String getSequences()
    {
        return "colorChartName,choiceStandardColor,casementHybrid";  
    }
}
