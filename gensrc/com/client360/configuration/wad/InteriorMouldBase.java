// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.client360.configuration.wad.enums.ChoiceInteriorMould;
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class InteriorMouldBase extends com.netappsid.wadconfigurator.InteriorMould
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(InteriorMouldBase.class);

    // attributes
    private ValueContainer<ChoiceInteriorMould> choiceInteriorMould = new ValueContainer<ChoiceInteriorMould>(this);

    // Bound properties

    public static final String PROPERTYNAME_CHOICEINTERIORMOULD = "choiceInteriorMould";  

    // Constructors

    public InteriorMouldBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.InteriorMould"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Interior mould")
        ,@LocalizedTag(language="fr", name="label", value="Moulure intérieure")
        ,@LocalizedTag(language="en", name="desc", value="Interior mould choice ")
        ,@LocalizedTag(language="fr", name="desc", value="Choix moulure intérieure")
    })
    public final ChoiceInteriorMould getChoiceInteriorMould()
    {
         return this.choiceInteriorMould.getValue();
    }

    public final void setChoiceInteriorMould(ChoiceInteriorMould choiceInteriorMould)
    {

        ChoiceInteriorMould oldValue = this.choiceInteriorMould.getValue();
        this.choiceInteriorMould.setValue(choiceInteriorMould);

        ChoiceInteriorMould currentValue = this.choiceInteriorMould.getValue();

        fireChoiceInteriorMouldChange(currentValue, oldValue);
    }
    public final ChoiceInteriorMould removeChoiceInteriorMould()
    {
        ChoiceInteriorMould oldValue = this.choiceInteriorMould.getValue();
        ChoiceInteriorMould removedValue = this.choiceInteriorMould.removeValue();
        ChoiceInteriorMould currentValue = this.choiceInteriorMould.getValue();

        fireChoiceInteriorMouldChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceInteriorMould(ChoiceInteriorMould choiceInteriorMould)
    {
        ChoiceInteriorMould oldValue = this.choiceInteriorMould.getValue();
        this.choiceInteriorMould.setDefaultValue(choiceInteriorMould);

        ChoiceInteriorMould currentValue = this.choiceInteriorMould.getValue();

        fireChoiceInteriorMouldChange(currentValue, oldValue);
    }
    
    public final ChoiceInteriorMould removeDefaultChoiceInteriorMould()
    {
        ChoiceInteriorMould oldValue = this.choiceInteriorMould.getValue();
        ChoiceInteriorMould removedValue = this.choiceInteriorMould.removeDefaultValue();
        ChoiceInteriorMould currentValue = this.choiceInteriorMould.getValue();

        fireChoiceInteriorMouldChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceInteriorMould> getChoiceInteriorMouldValueContainer()
    {
        return this.choiceInteriorMould;
    }
    public final void fireChoiceInteriorMouldChange(ChoiceInteriorMould currentValue, ChoiceInteriorMould oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceInteriorMouldChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEINTERIORMOULD, oldValue, currentValue);
            afterChoiceInteriorMouldChanged(currentValue);
        }
    }

    public void beforeChoiceInteriorMouldChanged( ChoiceInteriorMould choiceInteriorMould)
    { }
    public void afterChoiceInteriorMouldChanged( ChoiceInteriorMould choiceInteriorMould)
    { }


}
