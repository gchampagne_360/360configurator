// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.client360.configuration.wad.enums.ChoiceAdvancedGrilles;
import com.client360.configuration.wad.enums.ChoiceGlassSuppliers;
import com.client360.configuration.wad.enums.ChoiceParametricModel;
import com.client360.configuration.wad.enums.TypeDePanneaux;
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class InsulatedGlassBase extends com.netappsid.wadconfigurator.InsulatedGlass
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(InsulatedGlassBase.class);

    // attributes
    private ValueContainer<ChoiceGlassSuppliers> choiceGlassSuppliers = new ValueContainer<ChoiceGlassSuppliers>(this);
    private ValueContainer<ChoiceInsulatedGlassOption> choiceInsulatedGlassOption = new ValueContainer<ChoiceInsulatedGlassOption>(this);
    private ValueContainer<TypeDePanneaux> typeDePanneaux = new ValueContainer<TypeDePanneaux>(this);
    private ValueContainer<ChoiceAdvancedGrilles> choiceAdvancedGrilles = new ValueContainer<ChoiceAdvancedGrilles>(this);
    private ValueContainer<ChoiceParametricModel> choiceParametricModel = new ValueContainer<ChoiceParametricModel>(this);

    // Bound properties

    public static final String PROPERTYNAME_CHOICEGLASSSUPPLIERS = "choiceGlassSuppliers";  
    public static final String PROPERTYNAME_CHOICEINSULATEDGLASSOPTION = "choiceInsulatedGlassOption";  
    public static final String PROPERTYNAME_TYPEDEPANNEAUX = "typeDePanneaux";  
    public static final String PROPERTYNAME_CHOICEADVANCEDGRILLES = "choiceAdvancedGrilles";  
    public static final String PROPERTYNAME_CHOICEPARAMETRICMODEL = "choiceParametricModel";  

    // Constructors

    public InsulatedGlassBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.InsulatedGlass"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="desc", value="Glass supplier")
        ,@LocalizedTag(language="fr", name="desc", value="Fournisseur de verre")
        ,@LocalizedTag(language="en", name="label", value="Glass supplier")
        ,@LocalizedTag(language="fr", name="label", value="Fournisseur de verre")
    })
    public final ChoiceGlassSuppliers getChoiceGlassSuppliers()
    {
         return this.choiceGlassSuppliers.getValue();
    }

    public final void setChoiceGlassSuppliers(ChoiceGlassSuppliers choiceGlassSuppliers)
    {

        ChoiceGlassSuppliers oldValue = this.choiceGlassSuppliers.getValue();
        this.choiceGlassSuppliers.setValue(choiceGlassSuppliers);

        ChoiceGlassSuppliers currentValue = this.choiceGlassSuppliers.getValue();

        fireChoiceGlassSuppliersChange(currentValue, oldValue);
    }
    public final ChoiceGlassSuppliers removeChoiceGlassSuppliers()
    {
        ChoiceGlassSuppliers oldValue = this.choiceGlassSuppliers.getValue();
        ChoiceGlassSuppliers removedValue = this.choiceGlassSuppliers.removeValue();
        ChoiceGlassSuppliers currentValue = this.choiceGlassSuppliers.getValue();

        fireChoiceGlassSuppliersChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceGlassSuppliers(ChoiceGlassSuppliers choiceGlassSuppliers)
    {
        ChoiceGlassSuppliers oldValue = this.choiceGlassSuppliers.getValue();
        this.choiceGlassSuppliers.setDefaultValue(choiceGlassSuppliers);

        ChoiceGlassSuppliers currentValue = this.choiceGlassSuppliers.getValue();

        fireChoiceGlassSuppliersChange(currentValue, oldValue);
    }
    
    public final ChoiceGlassSuppliers removeDefaultChoiceGlassSuppliers()
    {
        ChoiceGlassSuppliers oldValue = this.choiceGlassSuppliers.getValue();
        ChoiceGlassSuppliers removedValue = this.choiceGlassSuppliers.removeDefaultValue();
        ChoiceGlassSuppliers currentValue = this.choiceGlassSuppliers.getValue();

        fireChoiceGlassSuppliersChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceGlassSuppliers> getChoiceGlassSuppliersValueContainer()
    {
        return this.choiceGlassSuppliers;
    }
    public final void fireChoiceGlassSuppliersChange(ChoiceGlassSuppliers currentValue, ChoiceGlassSuppliers oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceGlassSuppliersChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEGLASSSUPPLIERS, oldValue, currentValue);
            afterChoiceGlassSuppliersChanged(currentValue);
        }
    }

    public void beforeChoiceGlassSuppliersChanged( ChoiceGlassSuppliers choiceGlassSuppliers)
    { }
    public void afterChoiceGlassSuppliersChanged( ChoiceGlassSuppliers choiceGlassSuppliers)
    { }


    public boolean isChoiceGlassSuppliersMandatory()
    {
        return true;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="desc", value="Glass options")
        ,@LocalizedTag(language="fr", name="desc", value="Options de verre")
        ,@LocalizedTag(language="en", name="label", value="Glass options")
        ,@LocalizedTag(language="fr", name="label", value="Option de vitrage")
    })
    public final ChoiceInsulatedGlassOption getChoiceInsulatedGlassOption()
    {
         return this.choiceInsulatedGlassOption.getValue();
    }

    public final void setChoiceInsulatedGlassOption(ChoiceInsulatedGlassOption choiceInsulatedGlassOption)
    {

        if (choiceInsulatedGlassOption != null)
        {
            acquire(choiceInsulatedGlassOption, "choiceInsulatedGlassOption");
        }
        ChoiceInsulatedGlassOption oldValue = this.choiceInsulatedGlassOption.getValue();
        this.choiceInsulatedGlassOption.setValue(choiceInsulatedGlassOption);

        ChoiceInsulatedGlassOption currentValue = this.choiceInsulatedGlassOption.getValue();

        fireChoiceInsulatedGlassOptionChange(currentValue, oldValue);
    }
    public final ChoiceInsulatedGlassOption removeChoiceInsulatedGlassOption()
    {
        ChoiceInsulatedGlassOption oldValue = this.choiceInsulatedGlassOption.getValue();
        ChoiceInsulatedGlassOption removedValue = this.choiceInsulatedGlassOption.removeValue();
        ChoiceInsulatedGlassOption currentValue = this.choiceInsulatedGlassOption.getValue();

        fireChoiceInsulatedGlassOptionChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceInsulatedGlassOption(ChoiceInsulatedGlassOption choiceInsulatedGlassOption)
    {
        if (choiceInsulatedGlassOption != null)
        {
            acquire(choiceInsulatedGlassOption, "choiceInsulatedGlassOption");
        }
        ChoiceInsulatedGlassOption oldValue = this.choiceInsulatedGlassOption.getValue();
        this.choiceInsulatedGlassOption.setDefaultValue(choiceInsulatedGlassOption);

        ChoiceInsulatedGlassOption currentValue = this.choiceInsulatedGlassOption.getValue();

        fireChoiceInsulatedGlassOptionChange(currentValue, oldValue);
    }
    
    public final ChoiceInsulatedGlassOption removeDefaultChoiceInsulatedGlassOption()
    {
        ChoiceInsulatedGlassOption oldValue = this.choiceInsulatedGlassOption.getValue();
        ChoiceInsulatedGlassOption removedValue = this.choiceInsulatedGlassOption.removeDefaultValue();
        ChoiceInsulatedGlassOption currentValue = this.choiceInsulatedGlassOption.getValue();

        fireChoiceInsulatedGlassOptionChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceInsulatedGlassOption> getChoiceInsulatedGlassOptionValueContainer()
    {
        return this.choiceInsulatedGlassOption;
    }
    public final void fireChoiceInsulatedGlassOptionChange(ChoiceInsulatedGlassOption currentValue, ChoiceInsulatedGlassOption oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceInsulatedGlassOptionChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEINSULATEDGLASSOPTION, oldValue, currentValue);
            afterChoiceInsulatedGlassOptionChanged(currentValue);
        }
    }

    public void beforeChoiceInsulatedGlassOptionChanged( ChoiceInsulatedGlassOption choiceInsulatedGlassOption)
    { }
    public void afterChoiceInsulatedGlassOptionChanged( ChoiceInsulatedGlassOption choiceInsulatedGlassOption)
    { }


    public boolean isChoiceInsulatedGlassOptionMandatory()
    {
        return true;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="fr", name="label", value="Type de panneaux")
    })
    public final TypeDePanneaux getTypeDePanneaux()
    {
         return this.typeDePanneaux.getValue();
    }

    public final void setTypeDePanneaux(TypeDePanneaux typeDePanneaux)
    {

        TypeDePanneaux oldValue = this.typeDePanneaux.getValue();
        this.typeDePanneaux.setValue(typeDePanneaux);

        TypeDePanneaux currentValue = this.typeDePanneaux.getValue();

        fireTypeDePanneauxChange(currentValue, oldValue);
    }
    public final TypeDePanneaux removeTypeDePanneaux()
    {
        TypeDePanneaux oldValue = this.typeDePanneaux.getValue();
        TypeDePanneaux removedValue = this.typeDePanneaux.removeValue();
        TypeDePanneaux currentValue = this.typeDePanneaux.getValue();

        fireTypeDePanneauxChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultTypeDePanneaux(TypeDePanneaux typeDePanneaux)
    {
        TypeDePanneaux oldValue = this.typeDePanneaux.getValue();
        this.typeDePanneaux.setDefaultValue(typeDePanneaux);

        TypeDePanneaux currentValue = this.typeDePanneaux.getValue();

        fireTypeDePanneauxChange(currentValue, oldValue);
    }
    
    public final TypeDePanneaux removeDefaultTypeDePanneaux()
    {
        TypeDePanneaux oldValue = this.typeDePanneaux.getValue();
        TypeDePanneaux removedValue = this.typeDePanneaux.removeDefaultValue();
        TypeDePanneaux currentValue = this.typeDePanneaux.getValue();

        fireTypeDePanneauxChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<TypeDePanneaux> getTypeDePanneauxValueContainer()
    {
        return this.typeDePanneaux;
    }
    public final void fireTypeDePanneauxChange(TypeDePanneaux currentValue, TypeDePanneaux oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeTypeDePanneauxChanged(currentValue);
            firePropertyChange(PROPERTYNAME_TYPEDEPANNEAUX, oldValue, currentValue);
            afterTypeDePanneauxChanged(currentValue);
        }
    }

    public void beforeTypeDePanneauxChanged( TypeDePanneaux typeDePanneaux)
    { }
    public void afterTypeDePanneauxChanged( TypeDePanneaux typeDePanneaux)
    { }


    public boolean isTypeDePanneauxMandatory()
    {
        return true;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Griles")
        ,@LocalizedTag(language="fr", name="label", value="Petits Bois")
    })
    public final ChoiceAdvancedGrilles getChoiceAdvancedGrilles()
    {
         return this.choiceAdvancedGrilles.getValue();
    }

    public final void setChoiceAdvancedGrilles(ChoiceAdvancedGrilles choiceAdvancedGrilles)
    {

        ChoiceAdvancedGrilles oldValue = this.choiceAdvancedGrilles.getValue();
        this.choiceAdvancedGrilles.setValue(choiceAdvancedGrilles);

        ChoiceAdvancedGrilles currentValue = this.choiceAdvancedGrilles.getValue();

        fireChoiceAdvancedGrillesChange(currentValue, oldValue);
    }
    public final ChoiceAdvancedGrilles removeChoiceAdvancedGrilles()
    {
        ChoiceAdvancedGrilles oldValue = this.choiceAdvancedGrilles.getValue();
        ChoiceAdvancedGrilles removedValue = this.choiceAdvancedGrilles.removeValue();
        ChoiceAdvancedGrilles currentValue = this.choiceAdvancedGrilles.getValue();

        fireChoiceAdvancedGrillesChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceAdvancedGrilles(ChoiceAdvancedGrilles choiceAdvancedGrilles)
    {
        ChoiceAdvancedGrilles oldValue = this.choiceAdvancedGrilles.getValue();
        this.choiceAdvancedGrilles.setDefaultValue(choiceAdvancedGrilles);

        ChoiceAdvancedGrilles currentValue = this.choiceAdvancedGrilles.getValue();

        fireChoiceAdvancedGrillesChange(currentValue, oldValue);
    }
    
    public final ChoiceAdvancedGrilles removeDefaultChoiceAdvancedGrilles()
    {
        ChoiceAdvancedGrilles oldValue = this.choiceAdvancedGrilles.getValue();
        ChoiceAdvancedGrilles removedValue = this.choiceAdvancedGrilles.removeDefaultValue();
        ChoiceAdvancedGrilles currentValue = this.choiceAdvancedGrilles.getValue();

        fireChoiceAdvancedGrillesChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceAdvancedGrilles> getChoiceAdvancedGrillesValueContainer()
    {
        return this.choiceAdvancedGrilles;
    }
    public final void fireChoiceAdvancedGrillesChange(ChoiceAdvancedGrilles currentValue, ChoiceAdvancedGrilles oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceAdvancedGrillesChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEADVANCEDGRILLES, oldValue, currentValue);
            afterChoiceAdvancedGrillesChanged(currentValue);
        }
    }

    public void beforeChoiceAdvancedGrillesChanged( ChoiceAdvancedGrilles choiceAdvancedGrilles)
    { }
    public void afterChoiceAdvancedGrillesChanged( ChoiceAdvancedGrilles choiceAdvancedGrilles)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="fr", name="label", value="Sous-bassement")
        ,@LocalizedTag(language="en", name="label", value="Kick panel")
    })
    public final ChoiceParametricModel getChoiceParametricModel()
    {
         return this.choiceParametricModel.getValue();
    }

    public final void setChoiceParametricModel(ChoiceParametricModel choiceParametricModel)
    {

        ChoiceParametricModel oldValue = this.choiceParametricModel.getValue();
        this.choiceParametricModel.setValue(choiceParametricModel);

        ChoiceParametricModel currentValue = this.choiceParametricModel.getValue();

        fireChoiceParametricModelChange(currentValue, oldValue);
    }
    public final ChoiceParametricModel removeChoiceParametricModel()
    {
        ChoiceParametricModel oldValue = this.choiceParametricModel.getValue();
        ChoiceParametricModel removedValue = this.choiceParametricModel.removeValue();
        ChoiceParametricModel currentValue = this.choiceParametricModel.getValue();

        fireChoiceParametricModelChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceParametricModel(ChoiceParametricModel choiceParametricModel)
    {
        ChoiceParametricModel oldValue = this.choiceParametricModel.getValue();
        this.choiceParametricModel.setDefaultValue(choiceParametricModel);

        ChoiceParametricModel currentValue = this.choiceParametricModel.getValue();

        fireChoiceParametricModelChange(currentValue, oldValue);
    }
    
    public final ChoiceParametricModel removeDefaultChoiceParametricModel()
    {
        ChoiceParametricModel oldValue = this.choiceParametricModel.getValue();
        ChoiceParametricModel removedValue = this.choiceParametricModel.removeDefaultValue();
        ChoiceParametricModel currentValue = this.choiceParametricModel.getValue();

        fireChoiceParametricModelChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceParametricModel> getChoiceParametricModelValueContainer()
    {
        return this.choiceParametricModel;
    }
    public final void fireChoiceParametricModelChange(ChoiceParametricModel currentValue, ChoiceParametricModel oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceParametricModelChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEPARAMETRICMODEL, oldValue, currentValue);
            afterChoiceParametricModelChanged(currentValue);
        }
    }

    public void beforeChoiceParametricModelChanged( ChoiceParametricModel choiceParametricModel)
    { }
    public void afterChoiceParametricModelChanged( ChoiceParametricModel choiceParametricModel)
    { }



    @Override
    protected String getSequences()
    {
        return "choiceGlassThickness,dimension,choiceGlassSuppliers,choiceInsulatedGlassOption,choiceSectionGrille,not set,choiceAdvancedGrilles,advancedSectionGrilles,kickPanel,pinkGlasssectionGrille";  
    }
}
