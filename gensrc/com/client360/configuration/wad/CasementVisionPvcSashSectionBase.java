// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.client360.configuration.wad.enums.ChoiceAdvancedGrilles;
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class CasementVisionPvcSashSectionBase extends com.client360.configuration.wad.CasementMainSashSection
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(CasementVisionPvcSashSectionBase.class);

    // attributes
    private ValueContainer<ChoiceAdvancedGrilles> choiceAdvancedGrilles = new ValueContainer<ChoiceAdvancedGrilles>(this);

    // Bound properties

    public static final String PROPERTYNAME_CHOICEADVANCEDGRILLES = "choiceAdvancedGrilles";  

    // Constructors

    public CasementVisionPvcSashSectionBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.CasementVisionPvcSashSection"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Advanced muntin choice")
        ,@LocalizedTag(language="fr", name="label", value="Choix de barrotin avanc�")
        ,@LocalizedTag(language="fr", name="null", value="Sans")
        ,@LocalizedTag(language="en", name="null", value="Without")
        ,@LocalizedTag(language="fr", name="notnull", value="Avec")
        ,@LocalizedTag(language="en", name="notnull", value="With")
    })
    public final ChoiceAdvancedGrilles getChoiceAdvancedGrilles()
    {
         return this.choiceAdvancedGrilles.getValue();
    }

    public final void setChoiceAdvancedGrilles(ChoiceAdvancedGrilles choiceAdvancedGrilles)
    {

        ChoiceAdvancedGrilles oldValue = this.choiceAdvancedGrilles.getValue();
        this.choiceAdvancedGrilles.setValue(choiceAdvancedGrilles);

        ChoiceAdvancedGrilles currentValue = this.choiceAdvancedGrilles.getValue();

        fireChoiceAdvancedGrillesChange(currentValue, oldValue);
    }
    public final ChoiceAdvancedGrilles removeChoiceAdvancedGrilles()
    {
        ChoiceAdvancedGrilles oldValue = this.choiceAdvancedGrilles.getValue();
        ChoiceAdvancedGrilles removedValue = this.choiceAdvancedGrilles.removeValue();
        ChoiceAdvancedGrilles currentValue = this.choiceAdvancedGrilles.getValue();

        fireChoiceAdvancedGrillesChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceAdvancedGrilles(ChoiceAdvancedGrilles choiceAdvancedGrilles)
    {
        ChoiceAdvancedGrilles oldValue = this.choiceAdvancedGrilles.getValue();
        this.choiceAdvancedGrilles.setDefaultValue(choiceAdvancedGrilles);

        ChoiceAdvancedGrilles currentValue = this.choiceAdvancedGrilles.getValue();

        fireChoiceAdvancedGrillesChange(currentValue, oldValue);
    }
    
    public final ChoiceAdvancedGrilles removeDefaultChoiceAdvancedGrilles()
    {
        ChoiceAdvancedGrilles oldValue = this.choiceAdvancedGrilles.getValue();
        ChoiceAdvancedGrilles removedValue = this.choiceAdvancedGrilles.removeDefaultValue();
        ChoiceAdvancedGrilles currentValue = this.choiceAdvancedGrilles.getValue();

        fireChoiceAdvancedGrillesChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceAdvancedGrilles> getChoiceAdvancedGrillesValueContainer()
    {
        return this.choiceAdvancedGrilles;
    }
    public final void fireChoiceAdvancedGrillesChange(ChoiceAdvancedGrilles currentValue, ChoiceAdvancedGrilles oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceAdvancedGrillesChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEADVANCEDGRILLES, oldValue, currentValue);
            afterChoiceAdvancedGrillesChanged(currentValue);
        }
    }

    public void beforeChoiceAdvancedGrillesChanged( ChoiceAdvancedGrilles choiceAdvancedGrilles)
    { }
    public void afterChoiceAdvancedGrillesChanged( ChoiceAdvancedGrilles choiceAdvancedGrilles)
    { }



    @Override
    protected String getSequences()
    {
        return "choiceSashType,withInsulatedGlass,bottomPanel,glass,choiceAdvancedGrilles,advancedSectionGrilles,muntin,insulatedGlass";  
    }
}
