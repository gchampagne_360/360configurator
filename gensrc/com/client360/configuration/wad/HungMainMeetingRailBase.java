// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTags;
import com.netappsid.wadconfigurator.enums.ChoicePresetWindowWidth;

public abstract class HungMainMeetingRailBase extends com.netappsid.wadconfigurator.HungMeetingRail
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(HungMainMeetingRailBase.class);

    // attributes
    private ValueContainer<ChoicePresetWindowWidth> choicePresetWindowWidth = new ValueContainer<ChoicePresetWindowWidth>(this);
    private ValueContainer<Measure<Length>> length = new ValueContainer<Measure<Length>>(this);

    // Bound properties

    public static final String PROPERTYNAME_CHOICEPRESETWINDOWWIDTH = "choicePresetWindowWidth";  
    public static final String PROPERTYNAME_LENGTH = "length";  

    // Constructors

    public HungMainMeetingRailBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.HungMainMeetingRail"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
    })
    public final ChoicePresetWindowWidth getChoicePresetWindowWidth()
    {
         return this.choicePresetWindowWidth.getValue();
    }

    public final void setChoicePresetWindowWidth(ChoicePresetWindowWidth choicePresetWindowWidth)
    {

        ChoicePresetWindowWidth oldValue = this.choicePresetWindowWidth.getValue();
        this.choicePresetWindowWidth.setValue(choicePresetWindowWidth);

        ChoicePresetWindowWidth currentValue = this.choicePresetWindowWidth.getValue();

        fireChoicePresetWindowWidthChange(currentValue, oldValue);
    }
    public final ChoicePresetWindowWidth removeChoicePresetWindowWidth()
    {
        ChoicePresetWindowWidth oldValue = this.choicePresetWindowWidth.getValue();
        ChoicePresetWindowWidth removedValue = this.choicePresetWindowWidth.removeValue();
        ChoicePresetWindowWidth currentValue = this.choicePresetWindowWidth.getValue();

        fireChoicePresetWindowWidthChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoicePresetWindowWidth(ChoicePresetWindowWidth choicePresetWindowWidth)
    {
        ChoicePresetWindowWidth oldValue = this.choicePresetWindowWidth.getValue();
        this.choicePresetWindowWidth.setDefaultValue(choicePresetWindowWidth);

        ChoicePresetWindowWidth currentValue = this.choicePresetWindowWidth.getValue();

        fireChoicePresetWindowWidthChange(currentValue, oldValue);
    }
    
    public final ChoicePresetWindowWidth removeDefaultChoicePresetWindowWidth()
    {
        ChoicePresetWindowWidth oldValue = this.choicePresetWindowWidth.getValue();
        ChoicePresetWindowWidth removedValue = this.choicePresetWindowWidth.removeDefaultValue();
        ChoicePresetWindowWidth currentValue = this.choicePresetWindowWidth.getValue();

        fireChoicePresetWindowWidthChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoicePresetWindowWidth> getChoicePresetWindowWidthValueContainer()
    {
        return this.choicePresetWindowWidth;
    }
    public final void fireChoicePresetWindowWidthChange(ChoicePresetWindowWidth currentValue, ChoicePresetWindowWidth oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoicePresetWindowWidthChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEPRESETWINDOWWIDTH, oldValue, currentValue);
            afterChoicePresetWindowWidthChanged(currentValue);
        }
    }

    public void beforeChoicePresetWindowWidthChanged( ChoicePresetWindowWidth choicePresetWindowWidth)
    { }
    public void afterChoicePresetWindowWidthChanged( ChoicePresetWindowWidth choicePresetWindowWidth)
    { }

    @LocalizedTags
    ({
    })
    public final Measure<Length> getLength()
    {
         return this.length.getValue();
    }

    public final void setLength(Measure<Length> length)
    {

        length = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(length, getDefaultUnitFor(PROPERTYNAME_LENGTH,Length.class));

        Measure<Length> oldValue = this.length.getValue();
        this.length.setValue(length);

        Measure<Length> currentValue = this.length.getValue();

        fireLengthChange(currentValue, oldValue);
    }
    public final Measure<Length> removeLength()
    {
        Measure<Length> oldValue = this.length.getValue();
        Measure<Length> removedValue = this.length.removeValue();
        Measure<Length> currentValue = this.length.getValue();

        fireLengthChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultLength(Measure<Length> length)
    {
        length = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(length, getDefaultUnitFor(PROPERTYNAME_LENGTH,Length.class));

        Measure<Length> oldValue = this.length.getValue();
        this.length.setDefaultValue(length);

        Measure<Length> currentValue = this.length.getValue();

        fireLengthChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultLength()
    {
        Measure<Length> oldValue = this.length.getValue();
        Measure<Length> removedValue = this.length.removeDefaultValue();
        Measure<Length> currentValue = this.length.getValue();

        fireLengthChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getLengthValueContainer()
    {
        return this.length;
    }
    public final void fireLengthChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeLengthChanged(currentValue);
            firePropertyChange(PROPERTYNAME_LENGTH, oldValue, currentValue);
            afterLengthChanged(currentValue);
        }
    }

    public void beforeLengthChanged( Measure<Length> length)
    { }
    public void afterLengthChanged( Measure<Length> length)
    { }


}
