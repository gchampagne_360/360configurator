// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 


public abstract class MuntinQuestionaryBase extends com.netappsid.wadconfigurator.MuntinQuestionary
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(MuntinQuestionaryBase.class);

    // attributes

    // Bound properties


    // Constructors

    public MuntinQuestionaryBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.MuntinQuestionary"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations



    @Override
    protected String getSequences()
    {
        return "activateMultiGrilles,copyToInsulatedGlass,nbSdlHorizBars,nbSdlVertiBars,nbTdlHorizBars,nbTdlVertiBars,sdlThickness,tdlThickness,cutEqualGlass,advancedSectionGrilles,tdlHorizBarsPositionInputs,tdlVertiBarsPositionInputs,sdlHorizBarsPositionInputs,partialDistance,splittingGrillesForm,noSplittingGrillesForm,visibleGlassForm,glassForm,squaresNb,totalNoSplittingBarsLength,totalSplittingBarsLength,NoSplittingBars,choiceBarPositionTo,SplittingBars,choiceBarPositionFrom,interiorColor,exteriorColor,not set,sdlVertiBarsPositionInputs";  
    }
}
