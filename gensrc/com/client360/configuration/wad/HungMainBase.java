// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.client360.configuration.wad.enums.ChoiceHungFrame;
import com.client360.configuration.wad.enums.ChoiceScreenType;
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class HungMainBase extends com.netappsid.wadconfigurator.Hung
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(HungMainBase.class);

    // attributes
    private ValueContainer<ChoiceHungFrame> choiceHungFrame = new ValueContainer<ChoiceHungFrame>(this);
    private ValueContainer<ChoiceScreenType> choiceScreenType = new ValueContainer<ChoiceScreenType>(this);

    // Bound properties

    public static final String PROPERTYNAME_CHOICEHUNGFRAME = "choiceHungFrame";  
    public static final String PROPERTYNAME_CHOICESCREENTYPE = "choiceScreenType";  

    // Constructors

    public HungMainBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.HungMain"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Frame type")
        ,@LocalizedTag(language="fr", name="label", value="Type de cadre")
    })
    public final ChoiceHungFrame getChoiceHungFrame()
    {
         return this.choiceHungFrame.getValue();
    }

    public final void setChoiceHungFrame(ChoiceHungFrame choiceHungFrame)
    {

        ChoiceHungFrame oldValue = this.choiceHungFrame.getValue();
        this.choiceHungFrame.setValue(choiceHungFrame);

        ChoiceHungFrame currentValue = this.choiceHungFrame.getValue();

        fireChoiceHungFrameChange(currentValue, oldValue);
    }
    public final ChoiceHungFrame removeChoiceHungFrame()
    {
        ChoiceHungFrame oldValue = this.choiceHungFrame.getValue();
        ChoiceHungFrame removedValue = this.choiceHungFrame.removeValue();
        ChoiceHungFrame currentValue = this.choiceHungFrame.getValue();

        fireChoiceHungFrameChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceHungFrame(ChoiceHungFrame choiceHungFrame)
    {
        ChoiceHungFrame oldValue = this.choiceHungFrame.getValue();
        this.choiceHungFrame.setDefaultValue(choiceHungFrame);

        ChoiceHungFrame currentValue = this.choiceHungFrame.getValue();

        fireChoiceHungFrameChange(currentValue, oldValue);
    }
    
    public final ChoiceHungFrame removeDefaultChoiceHungFrame()
    {
        ChoiceHungFrame oldValue = this.choiceHungFrame.getValue();
        ChoiceHungFrame removedValue = this.choiceHungFrame.removeDefaultValue();
        ChoiceHungFrame currentValue = this.choiceHungFrame.getValue();

        fireChoiceHungFrameChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceHungFrame> getChoiceHungFrameValueContainer()
    {
        return this.choiceHungFrame;
    }
    public final void fireChoiceHungFrameChange(ChoiceHungFrame currentValue, ChoiceHungFrame oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceHungFrameChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEHUNGFRAME, oldValue, currentValue);
            afterChoiceHungFrameChanged(currentValue);
        }
    }

    public void beforeChoiceHungFrameChanged( ChoiceHungFrame choiceHungFrame)
    { }
    public void afterChoiceHungFrameChanged( ChoiceHungFrame choiceHungFrame)
    { }


    public boolean isChoiceHungFrameMandatory()
    {
        return true;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Screen Type")
        ,@LocalizedTag(language="fr", name="label", value="Type de moustiquaire")
    })
    public final ChoiceScreenType getChoiceScreenType()
    {
         return this.choiceScreenType.getValue();
    }

    public final void setChoiceScreenType(ChoiceScreenType choiceScreenType)
    {

        ChoiceScreenType oldValue = this.choiceScreenType.getValue();
        this.choiceScreenType.setValue(choiceScreenType);

        ChoiceScreenType currentValue = this.choiceScreenType.getValue();

        fireChoiceScreenTypeChange(currentValue, oldValue);
    }
    public final ChoiceScreenType removeChoiceScreenType()
    {
        ChoiceScreenType oldValue = this.choiceScreenType.getValue();
        ChoiceScreenType removedValue = this.choiceScreenType.removeValue();
        ChoiceScreenType currentValue = this.choiceScreenType.getValue();

        fireChoiceScreenTypeChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChoiceScreenType(ChoiceScreenType choiceScreenType)
    {
        ChoiceScreenType oldValue = this.choiceScreenType.getValue();
        this.choiceScreenType.setDefaultValue(choiceScreenType);

        ChoiceScreenType currentValue = this.choiceScreenType.getValue();

        fireChoiceScreenTypeChange(currentValue, oldValue);
    }
    
    public final ChoiceScreenType removeDefaultChoiceScreenType()
    {
        ChoiceScreenType oldValue = this.choiceScreenType.getValue();
        ChoiceScreenType removedValue = this.choiceScreenType.removeDefaultValue();
        ChoiceScreenType currentValue = this.choiceScreenType.getValue();

        fireChoiceScreenTypeChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ChoiceScreenType> getChoiceScreenTypeValueContainer()
    {
        return this.choiceScreenType;
    }
    public final void fireChoiceScreenTypeChange(ChoiceScreenType currentValue, ChoiceScreenType oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceScreenTypeChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICESCREENTYPE, oldValue, currentValue);
            afterChoiceScreenTypeChanged(currentValue);
        }
    }

    public void beforeChoiceScreenTypeChanged( ChoiceScreenType choiceScreenType)
    { }
    public void afterChoiceScreenTypeChanged( ChoiceScreenType choiceScreenType)
    { }



    @Override
    protected String getSequences()
    {
        return "Hybrid,choiceHungFrame,choiceOpeningType,nbSections,choiceScreenType,choicePresetWindowWidth,ExteriorColor,InteriorColor,dimension,sectionInput,division,energyStar,installed,qDQ,sameGrilles,showMeetingRail,showMullion,frameDepth,noFrame,windowsMullion,section,sash,meetingRail,paintable,renderingNumber";  
    }
}
