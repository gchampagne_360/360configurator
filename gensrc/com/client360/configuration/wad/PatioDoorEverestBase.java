// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 


public abstract class PatioDoorEverestBase extends com.client360.configuration.wad.PatioDoor
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(PatioDoorEverestBase.class);

    // attributes

    // Bound properties


    // Constructors

    public PatioDoorEverestBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.PatioDoorEverest"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations



    @Override
    protected String getSequences()
    {
        return "choicePatioDoorScreenOpening,baseFrameThickness,exteriorCap,halfExterior,halfInterior,withInsulatedGlass,energyStar,installed,nbSections,qDQ,sameGrilles,showMeetingRail,showMullion,meetingRail,division,sectionInput,windowsMullion,sash,ExteriorColor,paintable,renderingNumber,dimension,section,InteriorColor,choicePresetWindowWidth,InteriorSash";  
    }
}
