// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 


public abstract class SillBase extends com.netappsid.erp.configurator.Configurable
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(SillBase.class);

    // attributes

    // Bound properties


    // Constructors

    public SillBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
    }

    // Operations


}
