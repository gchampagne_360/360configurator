// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.DynamicEnum;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

@DynamicEnum(fileName = "dynamicEnums/PA_Thermos.tsv")
public abstract class ChoiceThermosDimensionBase extends com.netappsid.erp.configurator.Configurable
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ChoiceThermosDimensionBase.class);

    // attributes
    private ValueContainer<String> thermosWidth = new ValueContainer<String>(this);
    private ValueContainer<String> thermosHeight = new ValueContainer<String>(this);
    private ValueContainer<String> dimensionDescription = new ValueContainer<String>(this);
    private ValueContainer<String> qt = new ValueContainer<String>(this);
    private ValueContainer<String> doorSlabModel = new ValueContainer<String>(this);
    private ValueContainer<String> sideLightModel = new ValueContainer<String>(this);
    private ValueContainer<String> form = new ValueContainer<String>(this);
    private ValueContainer<String> pox1_32 = new ValueContainer<String>(this);
    private ValueContainer<String> pox1_34 = new ValueContainer<String>(this);
    private ValueContainer<String> pox1_36 = new ValueContainer<String>(this);
    private ValueContainer<String> posY1 = new ValueContainer<String>(this);
    private ValueContainer<String> pox2_32 = new ValueContainer<String>(this);
    private ValueContainer<String> pox2_34 = new ValueContainer<String>(this);
    private ValueContainer<String> pox2_36 = new ValueContainer<String>(this);
    private ValueContainer<String> posY2 = new ValueContainer<String>(this);
    private ValueContainer<String> posX_sideLight = new ValueContainer<String>(this);
    private ValueContainer<String> posY_sideLight = new ValueContainer<String>(this);

    // Bound properties

    public static final String PROPERTYNAME_THERMOSWIDTH = "thermosWidth";  
    public static final String PROPERTYNAME_THERMOSHEIGHT = "thermosHeight";  
    public static final String PROPERTYNAME_DIMENSIONDESCRIPTION = "dimensionDescription";  
    public static final String PROPERTYNAME_QT = "qt";  
    public static final String PROPERTYNAME_DOORSLABMODEL = "doorSlabModel";  
    public static final String PROPERTYNAME_SIDELIGHTMODEL = "sideLightModel";  
    public static final String PROPERTYNAME_FORM = "form";  
    public static final String PROPERTYNAME_POX1_32 = "pox1_32";  
    public static final String PROPERTYNAME_POX1_34 = "pox1_34";  
    public static final String PROPERTYNAME_POX1_36 = "pox1_36";  
    public static final String PROPERTYNAME_POSY1 = "posY1";  
    public static final String PROPERTYNAME_POX2_32 = "pox2_32";  
    public static final String PROPERTYNAME_POX2_34 = "pox2_34";  
    public static final String PROPERTYNAME_POX2_36 = "pox2_36";  
    public static final String PROPERTYNAME_POSY2 = "posY2";  
    public static final String PROPERTYNAME_POSX_SIDELIGHT = "posX_sideLight";  
    public static final String PROPERTYNAME_POSY_SIDELIGHT = "posY_sideLight";  

    // Constructors

    public ChoiceThermosDimensionBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.ChoiceThermosDimension"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="0")
        ,@LocalizedTag(language="fr", name="column", value="0")
        ,@LocalizedTag(language="en", name="label", value="Width")
        ,@LocalizedTag(language="fr", name="label", value="Largeur")
    })
    public final String getThermosWidth()
    {
         return this.thermosWidth.getValue();
    }

    public final void setThermosWidth(String thermosWidth)
    {

        String oldValue = this.thermosWidth.getValue();
        this.thermosWidth.setValue(thermosWidth);

        String currentValue = this.thermosWidth.getValue();

        fireThermosWidthChange(currentValue, oldValue);
    }
    public final String removeThermosWidth()
    {
        String oldValue = this.thermosWidth.getValue();
        String removedValue = this.thermosWidth.removeValue();
        String currentValue = this.thermosWidth.getValue();

        fireThermosWidthChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultThermosWidth(String thermosWidth)
    {
        String oldValue = this.thermosWidth.getValue();
        this.thermosWidth.setDefaultValue(thermosWidth);

        String currentValue = this.thermosWidth.getValue();

        fireThermosWidthChange(currentValue, oldValue);
    }
    
    public final String removeDefaultThermosWidth()
    {
        String oldValue = this.thermosWidth.getValue();
        String removedValue = this.thermosWidth.removeDefaultValue();
        String currentValue = this.thermosWidth.getValue();

        fireThermosWidthChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getThermosWidthValueContainer()
    {
        return this.thermosWidth;
    }
    public final void fireThermosWidthChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeThermosWidthChanged(currentValue);
            firePropertyChange(PROPERTYNAME_THERMOSWIDTH, oldValue, currentValue);
            afterThermosWidthChanged(currentValue);
        }
    }

    public void beforeThermosWidthChanged( String thermosWidth)
    { }
    public void afterThermosWidthChanged( String thermosWidth)
    { }


    public boolean isThermosWidthVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="1")
        ,@LocalizedTag(language="fr", name="column", value="1")
        ,@LocalizedTag(language="en", name="label", value="Height")
        ,@LocalizedTag(language="fr", name="label", value="Hauteur")
    })
    public final String getThermosHeight()
    {
         return this.thermosHeight.getValue();
    }

    public final void setThermosHeight(String thermosHeight)
    {

        String oldValue = this.thermosHeight.getValue();
        this.thermosHeight.setValue(thermosHeight);

        String currentValue = this.thermosHeight.getValue();

        fireThermosHeightChange(currentValue, oldValue);
    }
    public final String removeThermosHeight()
    {
        String oldValue = this.thermosHeight.getValue();
        String removedValue = this.thermosHeight.removeValue();
        String currentValue = this.thermosHeight.getValue();

        fireThermosHeightChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultThermosHeight(String thermosHeight)
    {
        String oldValue = this.thermosHeight.getValue();
        this.thermosHeight.setDefaultValue(thermosHeight);

        String currentValue = this.thermosHeight.getValue();

        fireThermosHeightChange(currentValue, oldValue);
    }
    
    public final String removeDefaultThermosHeight()
    {
        String oldValue = this.thermosHeight.getValue();
        String removedValue = this.thermosHeight.removeDefaultValue();
        String currentValue = this.thermosHeight.getValue();

        fireThermosHeightChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getThermosHeightValueContainer()
    {
        return this.thermosHeight;
    }
    public final void fireThermosHeightChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeThermosHeightChanged(currentValue);
            firePropertyChange(PROPERTYNAME_THERMOSHEIGHT, oldValue, currentValue);
            afterThermosHeightChanged(currentValue);
        }
    }

    public void beforeThermosHeightChanged( String thermosHeight)
    { }
    public void afterThermosHeightChanged( String thermosHeight)
    { }


    public boolean isThermosHeightVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="2")
        ,@LocalizedTag(language="fr", name="column", value="2")
        ,@LocalizedTag(language="en", name="label", value="Dimension")
        ,@LocalizedTag(language="fr", name="label", value="Dimension")
    })
    public final String getDimensionDescription()
    {
         return this.dimensionDescription.getValue();
    }

    public final void setDimensionDescription(String dimensionDescription)
    {

        String oldValue = this.dimensionDescription.getValue();
        this.dimensionDescription.setValue(dimensionDescription);

        String currentValue = this.dimensionDescription.getValue();

        fireDimensionDescriptionChange(currentValue, oldValue);
    }
    public final String removeDimensionDescription()
    {
        String oldValue = this.dimensionDescription.getValue();
        String removedValue = this.dimensionDescription.removeValue();
        String currentValue = this.dimensionDescription.getValue();

        fireDimensionDescriptionChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultDimensionDescription(String dimensionDescription)
    {
        String oldValue = this.dimensionDescription.getValue();
        this.dimensionDescription.setDefaultValue(dimensionDescription);

        String currentValue = this.dimensionDescription.getValue();

        fireDimensionDescriptionChange(currentValue, oldValue);
    }
    
    public final String removeDefaultDimensionDescription()
    {
        String oldValue = this.dimensionDescription.getValue();
        String removedValue = this.dimensionDescription.removeDefaultValue();
        String currentValue = this.dimensionDescription.getValue();

        fireDimensionDescriptionChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getDimensionDescriptionValueContainer()
    {
        return this.dimensionDescription;
    }
    public final void fireDimensionDescriptionChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeDimensionDescriptionChanged(currentValue);
            firePropertyChange(PROPERTYNAME_DIMENSIONDESCRIPTION, oldValue, currentValue);
            afterDimensionDescriptionChanged(currentValue);
        }
    }

    public void beforeDimensionDescriptionChanged( String dimensionDescription)
    { }
    public void afterDimensionDescriptionChanged( String dimensionDescription)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="3")
        ,@LocalizedTag(language="fr", name="column", value="3")
        ,@LocalizedTag(language="en", name="label", value="Quantity")
        ,@LocalizedTag(language="fr", name="label", value="Quantit�")
    })
    public final String getQt()
    {
         return this.qt.getValue();
    }

    public final void setQt(String qt)
    {

        String oldValue = this.qt.getValue();
        this.qt.setValue(qt);

        String currentValue = this.qt.getValue();

        fireQtChange(currentValue, oldValue);
    }
    public final String removeQt()
    {
        String oldValue = this.qt.getValue();
        String removedValue = this.qt.removeValue();
        String currentValue = this.qt.getValue();

        fireQtChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultQt(String qt)
    {
        String oldValue = this.qt.getValue();
        this.qt.setDefaultValue(qt);

        String currentValue = this.qt.getValue();

        fireQtChange(currentValue, oldValue);
    }
    
    public final String removeDefaultQt()
    {
        String oldValue = this.qt.getValue();
        String removedValue = this.qt.removeDefaultValue();
        String currentValue = this.qt.getValue();

        fireQtChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getQtValueContainer()
    {
        return this.qt;
    }
    public final void fireQtChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeQtChanged(currentValue);
            firePropertyChange(PROPERTYNAME_QT, oldValue, currentValue);
            afterQtChanged(currentValue);
        }
    }

    public void beforeQtChanged( String qt)
    { }
    public void afterQtChanged( String qt)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="4")
        ,@LocalizedTag(language="fr", name="column", value="4")
        ,@LocalizedTag(language="en", name="label", value="Door Model")
        ,@LocalizedTag(language="fr", name="label", value="Mo�le de porte")
    })
    public final String getDoorSlabModel()
    {
         return this.doorSlabModel.getValue();
    }

    public final void setDoorSlabModel(String doorSlabModel)
    {

        String oldValue = this.doorSlabModel.getValue();
        this.doorSlabModel.setValue(doorSlabModel);

        String currentValue = this.doorSlabModel.getValue();

        fireDoorSlabModelChange(currentValue, oldValue);
    }
    public final String removeDoorSlabModel()
    {
        String oldValue = this.doorSlabModel.getValue();
        String removedValue = this.doorSlabModel.removeValue();
        String currentValue = this.doorSlabModel.getValue();

        fireDoorSlabModelChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultDoorSlabModel(String doorSlabModel)
    {
        String oldValue = this.doorSlabModel.getValue();
        this.doorSlabModel.setDefaultValue(doorSlabModel);

        String currentValue = this.doorSlabModel.getValue();

        fireDoorSlabModelChange(currentValue, oldValue);
    }
    
    public final String removeDefaultDoorSlabModel()
    {
        String oldValue = this.doorSlabModel.getValue();
        String removedValue = this.doorSlabModel.removeDefaultValue();
        String currentValue = this.doorSlabModel.getValue();

        fireDoorSlabModelChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getDoorSlabModelValueContainer()
    {
        return this.doorSlabModel;
    }
    public final void fireDoorSlabModelChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeDoorSlabModelChanged(currentValue);
            firePropertyChange(PROPERTYNAME_DOORSLABMODEL, oldValue, currentValue);
            afterDoorSlabModelChanged(currentValue);
        }
    }

    public void beforeDoorSlabModelChanged( String doorSlabModel)
    { }
    public void afterDoorSlabModelChanged( String doorSlabModel)
    { }


    public boolean isDoorSlabModelVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="5")
        ,@LocalizedTag(language="fr", name="column", value="5")
        ,@LocalizedTag(language="en", name="label", value="Side-Light Model")
        ,@LocalizedTag(language="fr", name="label", value="Mod�le lat�raux")
    })
    public final String getSideLightModel()
    {
         return this.sideLightModel.getValue();
    }

    public final void setSideLightModel(String sideLightModel)
    {

        String oldValue = this.sideLightModel.getValue();
        this.sideLightModel.setValue(sideLightModel);

        String currentValue = this.sideLightModel.getValue();

        fireSideLightModelChange(currentValue, oldValue);
    }
    public final String removeSideLightModel()
    {
        String oldValue = this.sideLightModel.getValue();
        String removedValue = this.sideLightModel.removeValue();
        String currentValue = this.sideLightModel.getValue();

        fireSideLightModelChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultSideLightModel(String sideLightModel)
    {
        String oldValue = this.sideLightModel.getValue();
        this.sideLightModel.setDefaultValue(sideLightModel);

        String currentValue = this.sideLightModel.getValue();

        fireSideLightModelChange(currentValue, oldValue);
    }
    
    public final String removeDefaultSideLightModel()
    {
        String oldValue = this.sideLightModel.getValue();
        String removedValue = this.sideLightModel.removeDefaultValue();
        String currentValue = this.sideLightModel.getValue();

        fireSideLightModelChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getSideLightModelValueContainer()
    {
        return this.sideLightModel;
    }
    public final void fireSideLightModelChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeSideLightModelChanged(currentValue);
            firePropertyChange(PROPERTYNAME_SIDELIGHTMODEL, oldValue, currentValue);
            afterSideLightModelChanged(currentValue);
        }
    }

    public void beforeSideLightModelChanged( String sideLightModel)
    { }
    public void afterSideLightModelChanged( String sideLightModel)
    { }


    public boolean isSideLightModelVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="6")
        ,@LocalizedTag(language="fr", name="column", value="6")
        ,@LocalizedTag(language="en", name="label", value="Form")
        ,@LocalizedTag(language="fr", name="label", value="Forme")
    })
    public final String getForm()
    {
         return this.form.getValue();
    }

    public final void setForm(String form)
    {

        String oldValue = this.form.getValue();
        this.form.setValue(form);

        String currentValue = this.form.getValue();

        fireFormChange(currentValue, oldValue);
    }
    public final String removeForm()
    {
        String oldValue = this.form.getValue();
        String removedValue = this.form.removeValue();
        String currentValue = this.form.getValue();

        fireFormChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultForm(String form)
    {
        String oldValue = this.form.getValue();
        this.form.setDefaultValue(form);

        String currentValue = this.form.getValue();

        fireFormChange(currentValue, oldValue);
    }
    
    public final String removeDefaultForm()
    {
        String oldValue = this.form.getValue();
        String removedValue = this.form.removeDefaultValue();
        String currentValue = this.form.getValue();

        fireFormChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getFormValueContainer()
    {
        return this.form;
    }
    public final void fireFormChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeFormChanged(currentValue);
            firePropertyChange(PROPERTYNAME_FORM, oldValue, currentValue);
            afterFormChanged(currentValue);
        }
    }

    public void beforeFormChanged( String form)
    { }
    public void afterFormChanged( String form)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="7")
        ,@LocalizedTag(language="fr", name="column", value="7")
        ,@LocalizedTag(language="en", name="label", value="Pos X1 32")
        ,@LocalizedTag(language="fr", name="label", value="Pos X1 32")
    })
    public final String getPox1_32()
    {
         return this.pox1_32.getValue();
    }

    public final void setPox1_32(String pox1_32)
    {

        String oldValue = this.pox1_32.getValue();
        this.pox1_32.setValue(pox1_32);

        String currentValue = this.pox1_32.getValue();

        firePox1_32Change(currentValue, oldValue);
    }
    public final String removePox1_32()
    {
        String oldValue = this.pox1_32.getValue();
        String removedValue = this.pox1_32.removeValue();
        String currentValue = this.pox1_32.getValue();

        firePox1_32Change(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultPox1_32(String pox1_32)
    {
        String oldValue = this.pox1_32.getValue();
        this.pox1_32.setDefaultValue(pox1_32);

        String currentValue = this.pox1_32.getValue();

        firePox1_32Change(currentValue, oldValue);
    }
    
    public final String removeDefaultPox1_32()
    {
        String oldValue = this.pox1_32.getValue();
        String removedValue = this.pox1_32.removeDefaultValue();
        String currentValue = this.pox1_32.getValue();

        firePox1_32Change(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getPox1_32ValueContainer()
    {
        return this.pox1_32;
    }
    public final void firePox1_32Change(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforePox1_32Changed(currentValue);
            firePropertyChange(PROPERTYNAME_POX1_32, oldValue, currentValue);
            afterPox1_32Changed(currentValue);
        }
    }

    public void beforePox1_32Changed( String pox1_32)
    { }
    public void afterPox1_32Changed( String pox1_32)
    { }


    public boolean isPox1_32Visible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="8")
        ,@LocalizedTag(language="fr", name="column", value="8")
        ,@LocalizedTag(language="en", name="label", value="Pos X1 34")
        ,@LocalizedTag(language="fr", name="label", value="Pos X1 34")
    })
    public final String getPox1_34()
    {
         return this.pox1_34.getValue();
    }

    public final void setPox1_34(String pox1_34)
    {

        String oldValue = this.pox1_34.getValue();
        this.pox1_34.setValue(pox1_34);

        String currentValue = this.pox1_34.getValue();

        firePox1_34Change(currentValue, oldValue);
    }
    public final String removePox1_34()
    {
        String oldValue = this.pox1_34.getValue();
        String removedValue = this.pox1_34.removeValue();
        String currentValue = this.pox1_34.getValue();

        firePox1_34Change(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultPox1_34(String pox1_34)
    {
        String oldValue = this.pox1_34.getValue();
        this.pox1_34.setDefaultValue(pox1_34);

        String currentValue = this.pox1_34.getValue();

        firePox1_34Change(currentValue, oldValue);
    }
    
    public final String removeDefaultPox1_34()
    {
        String oldValue = this.pox1_34.getValue();
        String removedValue = this.pox1_34.removeDefaultValue();
        String currentValue = this.pox1_34.getValue();

        firePox1_34Change(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getPox1_34ValueContainer()
    {
        return this.pox1_34;
    }
    public final void firePox1_34Change(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforePox1_34Changed(currentValue);
            firePropertyChange(PROPERTYNAME_POX1_34, oldValue, currentValue);
            afterPox1_34Changed(currentValue);
        }
    }

    public void beforePox1_34Changed( String pox1_34)
    { }
    public void afterPox1_34Changed( String pox1_34)
    { }


    public boolean isPox1_34Visible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="9")
        ,@LocalizedTag(language="fr", name="column", value="9")
        ,@LocalizedTag(language="en", name="label", value="Pos X1 36")
        ,@LocalizedTag(language="fr", name="label", value="Pos X1 36")
    })
    public final String getPox1_36()
    {
         return this.pox1_36.getValue();
    }

    public final void setPox1_36(String pox1_36)
    {

        String oldValue = this.pox1_36.getValue();
        this.pox1_36.setValue(pox1_36);

        String currentValue = this.pox1_36.getValue();

        firePox1_36Change(currentValue, oldValue);
    }
    public final String removePox1_36()
    {
        String oldValue = this.pox1_36.getValue();
        String removedValue = this.pox1_36.removeValue();
        String currentValue = this.pox1_36.getValue();

        firePox1_36Change(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultPox1_36(String pox1_36)
    {
        String oldValue = this.pox1_36.getValue();
        this.pox1_36.setDefaultValue(pox1_36);

        String currentValue = this.pox1_36.getValue();

        firePox1_36Change(currentValue, oldValue);
    }
    
    public final String removeDefaultPox1_36()
    {
        String oldValue = this.pox1_36.getValue();
        String removedValue = this.pox1_36.removeDefaultValue();
        String currentValue = this.pox1_36.getValue();

        firePox1_36Change(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getPox1_36ValueContainer()
    {
        return this.pox1_36;
    }
    public final void firePox1_36Change(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforePox1_36Changed(currentValue);
            firePropertyChange(PROPERTYNAME_POX1_36, oldValue, currentValue);
            afterPox1_36Changed(currentValue);
        }
    }

    public void beforePox1_36Changed( String pox1_36)
    { }
    public void afterPox1_36Changed( String pox1_36)
    { }


    public boolean isPox1_36Visible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="10")
        ,@LocalizedTag(language="fr", name="column", value="10")
        ,@LocalizedTag(language="en", name="label", value="Pos Y1")
        ,@LocalizedTag(language="fr", name="label", value="Pos Y1")
    })
    public final String getPosY1()
    {
         return this.posY1.getValue();
    }

    public final void setPosY1(String posY1)
    {

        String oldValue = this.posY1.getValue();
        this.posY1.setValue(posY1);

        String currentValue = this.posY1.getValue();

        firePosY1Change(currentValue, oldValue);
    }
    public final String removePosY1()
    {
        String oldValue = this.posY1.getValue();
        String removedValue = this.posY1.removeValue();
        String currentValue = this.posY1.getValue();

        firePosY1Change(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultPosY1(String posY1)
    {
        String oldValue = this.posY1.getValue();
        this.posY1.setDefaultValue(posY1);

        String currentValue = this.posY1.getValue();

        firePosY1Change(currentValue, oldValue);
    }
    
    public final String removeDefaultPosY1()
    {
        String oldValue = this.posY1.getValue();
        String removedValue = this.posY1.removeDefaultValue();
        String currentValue = this.posY1.getValue();

        firePosY1Change(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getPosY1ValueContainer()
    {
        return this.posY1;
    }
    public final void firePosY1Change(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforePosY1Changed(currentValue);
            firePropertyChange(PROPERTYNAME_POSY1, oldValue, currentValue);
            afterPosY1Changed(currentValue);
        }
    }

    public void beforePosY1Changed( String posY1)
    { }
    public void afterPosY1Changed( String posY1)
    { }


    public boolean isPosY1Visible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="11")
        ,@LocalizedTag(language="fr", name="column", value="11")
        ,@LocalizedTag(language="en", name="label", value="Pos X2 32")
        ,@LocalizedTag(language="fr", name="label", value="Pos X2 32")
    })
    public final String getPox2_32()
    {
         return this.pox2_32.getValue();
    }

    public final void setPox2_32(String pox2_32)
    {

        String oldValue = this.pox2_32.getValue();
        this.pox2_32.setValue(pox2_32);

        String currentValue = this.pox2_32.getValue();

        firePox2_32Change(currentValue, oldValue);
    }
    public final String removePox2_32()
    {
        String oldValue = this.pox2_32.getValue();
        String removedValue = this.pox2_32.removeValue();
        String currentValue = this.pox2_32.getValue();

        firePox2_32Change(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultPox2_32(String pox2_32)
    {
        String oldValue = this.pox2_32.getValue();
        this.pox2_32.setDefaultValue(pox2_32);

        String currentValue = this.pox2_32.getValue();

        firePox2_32Change(currentValue, oldValue);
    }
    
    public final String removeDefaultPox2_32()
    {
        String oldValue = this.pox2_32.getValue();
        String removedValue = this.pox2_32.removeDefaultValue();
        String currentValue = this.pox2_32.getValue();

        firePox2_32Change(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getPox2_32ValueContainer()
    {
        return this.pox2_32;
    }
    public final void firePox2_32Change(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforePox2_32Changed(currentValue);
            firePropertyChange(PROPERTYNAME_POX2_32, oldValue, currentValue);
            afterPox2_32Changed(currentValue);
        }
    }

    public void beforePox2_32Changed( String pox2_32)
    { }
    public void afterPox2_32Changed( String pox2_32)
    { }


    public boolean isPox2_32Visible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="12")
        ,@LocalizedTag(language="fr", name="column", value="12")
        ,@LocalizedTag(language="en", name="label", value="Pos X2 34")
        ,@LocalizedTag(language="fr", name="label", value="Pos X2 34")
    })
    public final String getPox2_34()
    {
         return this.pox2_34.getValue();
    }

    public final void setPox2_34(String pox2_34)
    {

        String oldValue = this.pox2_34.getValue();
        this.pox2_34.setValue(pox2_34);

        String currentValue = this.pox2_34.getValue();

        firePox2_34Change(currentValue, oldValue);
    }
    public final String removePox2_34()
    {
        String oldValue = this.pox2_34.getValue();
        String removedValue = this.pox2_34.removeValue();
        String currentValue = this.pox2_34.getValue();

        firePox2_34Change(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultPox2_34(String pox2_34)
    {
        String oldValue = this.pox2_34.getValue();
        this.pox2_34.setDefaultValue(pox2_34);

        String currentValue = this.pox2_34.getValue();

        firePox2_34Change(currentValue, oldValue);
    }
    
    public final String removeDefaultPox2_34()
    {
        String oldValue = this.pox2_34.getValue();
        String removedValue = this.pox2_34.removeDefaultValue();
        String currentValue = this.pox2_34.getValue();

        firePox2_34Change(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getPox2_34ValueContainer()
    {
        return this.pox2_34;
    }
    public final void firePox2_34Change(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforePox2_34Changed(currentValue);
            firePropertyChange(PROPERTYNAME_POX2_34, oldValue, currentValue);
            afterPox2_34Changed(currentValue);
        }
    }

    public void beforePox2_34Changed( String pox2_34)
    { }
    public void afterPox2_34Changed( String pox2_34)
    { }


    public boolean isPox2_34Visible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="13")
        ,@LocalizedTag(language="fr", name="column", value="13")
        ,@LocalizedTag(language="en", name="label", value="Pos X2 36")
        ,@LocalizedTag(language="fr", name="label", value="Pos X2 36")
    })
    public final String getPox2_36()
    {
         return this.pox2_36.getValue();
    }

    public final void setPox2_36(String pox2_36)
    {

        String oldValue = this.pox2_36.getValue();
        this.pox2_36.setValue(pox2_36);

        String currentValue = this.pox2_36.getValue();

        firePox2_36Change(currentValue, oldValue);
    }
    public final String removePox2_36()
    {
        String oldValue = this.pox2_36.getValue();
        String removedValue = this.pox2_36.removeValue();
        String currentValue = this.pox2_36.getValue();

        firePox2_36Change(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultPox2_36(String pox2_36)
    {
        String oldValue = this.pox2_36.getValue();
        this.pox2_36.setDefaultValue(pox2_36);

        String currentValue = this.pox2_36.getValue();

        firePox2_36Change(currentValue, oldValue);
    }
    
    public final String removeDefaultPox2_36()
    {
        String oldValue = this.pox2_36.getValue();
        String removedValue = this.pox2_36.removeDefaultValue();
        String currentValue = this.pox2_36.getValue();

        firePox2_36Change(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getPox2_36ValueContainer()
    {
        return this.pox2_36;
    }
    public final void firePox2_36Change(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforePox2_36Changed(currentValue);
            firePropertyChange(PROPERTYNAME_POX2_36, oldValue, currentValue);
            afterPox2_36Changed(currentValue);
        }
    }

    public void beforePox2_36Changed( String pox2_36)
    { }
    public void afterPox2_36Changed( String pox2_36)
    { }


    public boolean isPox2_36Visible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="14")
        ,@LocalizedTag(language="fr", name="column", value="14")
        ,@LocalizedTag(language="en", name="label", value="Pos Y2")
        ,@LocalizedTag(language="fr", name="label", value="Pos Y2")
    })
    public final String getPosY2()
    {
         return this.posY2.getValue();
    }

    public final void setPosY2(String posY2)
    {

        String oldValue = this.posY2.getValue();
        this.posY2.setValue(posY2);

        String currentValue = this.posY2.getValue();

        firePosY2Change(currentValue, oldValue);
    }
    public final String removePosY2()
    {
        String oldValue = this.posY2.getValue();
        String removedValue = this.posY2.removeValue();
        String currentValue = this.posY2.getValue();

        firePosY2Change(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultPosY2(String posY2)
    {
        String oldValue = this.posY2.getValue();
        this.posY2.setDefaultValue(posY2);

        String currentValue = this.posY2.getValue();

        firePosY2Change(currentValue, oldValue);
    }
    
    public final String removeDefaultPosY2()
    {
        String oldValue = this.posY2.getValue();
        String removedValue = this.posY2.removeDefaultValue();
        String currentValue = this.posY2.getValue();

        firePosY2Change(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getPosY2ValueContainer()
    {
        return this.posY2;
    }
    public final void firePosY2Change(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforePosY2Changed(currentValue);
            firePropertyChange(PROPERTYNAME_POSY2, oldValue, currentValue);
            afterPosY2Changed(currentValue);
        }
    }

    public void beforePosY2Changed( String posY2)
    { }
    public void afterPosY2Changed( String posY2)
    { }


    public boolean isPosY2Visible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="15")
        ,@LocalizedTag(language="fr", name="column", value="15")
        ,@LocalizedTag(language="en", name="label", value="Pos X SideLight")
        ,@LocalizedTag(language="fr", name="label", value="Pos X Lat�raux")
    })
    public final String getPosX_sideLight()
    {
         return this.posX_sideLight.getValue();
    }

    public final void setPosX_sideLight(String posX_sideLight)
    {

        String oldValue = this.posX_sideLight.getValue();
        this.posX_sideLight.setValue(posX_sideLight);

        String currentValue = this.posX_sideLight.getValue();

        firePosX_sideLightChange(currentValue, oldValue);
    }
    public final String removePosX_sideLight()
    {
        String oldValue = this.posX_sideLight.getValue();
        String removedValue = this.posX_sideLight.removeValue();
        String currentValue = this.posX_sideLight.getValue();

        firePosX_sideLightChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultPosX_sideLight(String posX_sideLight)
    {
        String oldValue = this.posX_sideLight.getValue();
        this.posX_sideLight.setDefaultValue(posX_sideLight);

        String currentValue = this.posX_sideLight.getValue();

        firePosX_sideLightChange(currentValue, oldValue);
    }
    
    public final String removeDefaultPosX_sideLight()
    {
        String oldValue = this.posX_sideLight.getValue();
        String removedValue = this.posX_sideLight.removeDefaultValue();
        String currentValue = this.posX_sideLight.getValue();

        firePosX_sideLightChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getPosX_sideLightValueContainer()
    {
        return this.posX_sideLight;
    }
    public final void firePosX_sideLightChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforePosX_sideLightChanged(currentValue);
            firePropertyChange(PROPERTYNAME_POSX_SIDELIGHT, oldValue, currentValue);
            afterPosX_sideLightChanged(currentValue);
        }
    }

    public void beforePosX_sideLightChanged( String posX_sideLight)
    { }
    public void afterPosX_sideLightChanged( String posX_sideLight)
    { }


    public boolean isPosX_sideLightVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="16")
        ,@LocalizedTag(language="fr", name="column", value="16")
        ,@LocalizedTag(language="en", name="label", value="Pos Y SideLight")
        ,@LocalizedTag(language="fr", name="label", value="Pos Y Lat�raux")
    })
    public final String getPosY_sideLight()
    {
         return this.posY_sideLight.getValue();
    }

    public final void setPosY_sideLight(String posY_sideLight)
    {

        String oldValue = this.posY_sideLight.getValue();
        this.posY_sideLight.setValue(posY_sideLight);

        String currentValue = this.posY_sideLight.getValue();

        firePosY_sideLightChange(currentValue, oldValue);
    }
    public final String removePosY_sideLight()
    {
        String oldValue = this.posY_sideLight.getValue();
        String removedValue = this.posY_sideLight.removeValue();
        String currentValue = this.posY_sideLight.getValue();

        firePosY_sideLightChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultPosY_sideLight(String posY_sideLight)
    {
        String oldValue = this.posY_sideLight.getValue();
        this.posY_sideLight.setDefaultValue(posY_sideLight);

        String currentValue = this.posY_sideLight.getValue();

        firePosY_sideLightChange(currentValue, oldValue);
    }
    
    public final String removeDefaultPosY_sideLight()
    {
        String oldValue = this.posY_sideLight.getValue();
        String removedValue = this.posY_sideLight.removeDefaultValue();
        String currentValue = this.posY_sideLight.getValue();

        firePosY_sideLightChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getPosY_sideLightValueContainer()
    {
        return this.posY_sideLight;
    }
    public final void firePosY_sideLightChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforePosY_sideLightChanged(currentValue);
            firePropertyChange(PROPERTYNAME_POSY_SIDELIGHT, oldValue, currentValue);
            afterPosY_sideLightChanged(currentValue);
        }
    }

    public void beforePosY_sideLightChanged( String posY_sideLight)
    { }
    public void afterPosY_sideLightChanged( String posY_sideLight)
    { }


    public boolean isPosY_sideLightVisible()
    {
        return false;
    }

}
