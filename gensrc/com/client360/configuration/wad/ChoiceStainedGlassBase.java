// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V12.7.0
//2011-11-17

// Imports 
import com.netappsid.configuration.common.Configurable;
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.AssociatedComment;
import com.netappsid.erp.configurator.annotations.AssociatedDrawing;
import com.netappsid.erp.configurator.annotations.DynamicEnum;
import com.netappsid.erp.configurator.annotations.Filtrable;
import com.netappsid.erp.configurator.annotations.Image;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;
import com.netappsid.erp.configurator.annotations.Visible;

@DynamicEnum(fileName = "dynamicEnums/stainedGlass", locale = "en", decimalSeparator = ".")
@Image("11")
public abstract class ChoiceStainedGlassBase extends Configurable
{
	// Log4J logger
	protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ChoiceStainedGlassBase.class);

	// attributes
	// GENCODE 3703fd90-c29c-11e0-962b-0800200c9a66 Values changed to ValueContainerwith generic inner type
	private ValueContainer<String> serie = new ValueContainer<String>(this);
	private ValueContainer<String> model = new ValueContainer<String>(this);
	private ValueContainer<String> code = new ValueContainer<String>(this);
	private ValueContainer<String> dimension = new ValueContainer<String>(this);
	private ValueContainer<String> frame = new ValueContainer<String>(this);
	private ValueContainer<String> glass = new ValueContainer<String>(this);
	private ValueContainer<String> privacy = new ValueContainer<String>(this);
	private ValueContainer<String> form = new ValueContainer<String>(this);
	private ValueContainer<String> note1 = new ValueContainer<String>(this);
	private ValueContainer<String> note2 = new ValueContainer<String>(this);
	private ValueContainer<String> image = new ValueContainer<String>(this);
	private ValueContainer<String> width = new ValueContainer<String>(this);
	private ValueContainer<String> height = new ValueContainer<String>(this);
	private ValueContainer<String> stainedGlassForDoor = new ValueContainer<String>(this);
	private ValueContainer<String> height79 = new ValueContainer<String>(this);
	private ValueContainer<String> height84 = new ValueContainer<String>(this);
	private ValueContainer<String> height95 = new ValueContainer<String>(this);
	private ValueContainer<String> parametricModelClass = new ValueContainer<String>(this);
	private ValueContainer<String> parametricModelClassSidelite = new ValueContainer<String>(this);
	private ValueContainer<String> askStainedGlassPosition = new ValueContainer<String>(this);
	private ValueContainer<String> formCode = new ValueContainer<String>(this);

	// Constructors

	public ChoiceStainedGlassBase(com.netappsid.erp.configurator.Configurable parent) // GENCODE b298fff0-4bff-11e0-b8af-0800200c9a66
	{
		super(parent);
		if (this.getClass().getName().equals("com.client360.configuration.wad.ChoiceStainedGlass"))
		{
			// activate listener before setting the default value to listen the changes
			activateListener();

			// Load the default values dynamically
			setDefaultValues();

			// load 'collection' preferences and values coming from prior items in the order
			applyPreferences();
		}

	}

	// Operations

	public final void setSerie(String serie)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.serie.getValue();
		this.serie.setValue(serie);
		String currentValue = this.serie.getValue();

		fireSerieChange(currentValue, oldValue);
	}

	public final String removeSerie()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.serie.getValue();
		String removedValue = this.serie.removeOverride();
		String currentValue = this.serie.getValue();

		fireSerieChange(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultSerie(String serie)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.serie.getValue();
		this.serie.setDefaultValue(serie);
		String currentValue = this.serie.getValue();

		fireSerieChange(currentValue, oldValue);
	}

	public final String removeDefaultSerie()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.serie.getValue();
		String removedValue = this.serie.removeDefaultValue();
		String currentValue = this.serie.getValue();

		fireSerieChange(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getSerieValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.serie;
	}

	public final void fireSerieChange(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeSerieChanged(currentValue);
			firePropertyChange(PROPERTYNAME_SERIE, oldValue, currentValue);
			afterSerieChanged(currentValue);
		}
	}

	public void beforeSerieChanged(String serie)
	{}

	public void afterSerieChanged(String serie)
	{}

	public boolean isSerieReadOnly()
	{
		return false;
	}

	public boolean isSerieEnabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "0"), @LocalizedTag(language = "fr", name = "column", value = "0"),
			@LocalizedTag(language = "en", name = "label", value = "Serie"), @LocalizedTag(language = "fr", name = "label", value = "S�rie") })
	@Visible("true")
	@Filtrable(value = "true", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getSerie()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.serie.getValue();
	}

	public final void setModel(String model)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.model.getValue();
		this.model.setValue(model);
		String currentValue = this.model.getValue();

		fireModelChange(currentValue, oldValue);
	}

	public final String removeModel()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.model.getValue();
		String removedValue = this.model.removeOverride();
		String currentValue = this.model.getValue();

		fireModelChange(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultModel(String model)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.model.getValue();
		this.model.setDefaultValue(model);
		String currentValue = this.model.getValue();

		fireModelChange(currentValue, oldValue);
	}

	public final String removeDefaultModel()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.model.getValue();
		String removedValue = this.model.removeDefaultValue();
		String currentValue = this.model.getValue();

		fireModelChange(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getModelValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.model;
	}

	public final void fireModelChange(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeModelChanged(currentValue);
			firePropertyChange(PROPERTYNAME_MODEL, oldValue, currentValue);
			afterModelChanged(currentValue);
		}
	}

	public void beforeModelChanged(String model)
	{}

	public void afterModelChanged(String model)
	{}

	public boolean isModelReadOnly()
	{
		return false;
	}

	public boolean isModelEnabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "1"), @LocalizedTag(language = "fr", name = "column", value = "1"),
			@LocalizedTag(language = "en", name = "label", value = "Model"), @LocalizedTag(language = "fr", name = "label", value = "Mod�le") })
	@Visible("true")
	@Filtrable(value = "true", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getModel()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.model.getValue();
	}

	public final void setCode(String code)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.code.getValue();
		this.code.setValue(code);
		String currentValue = this.code.getValue();

		fireCodeChange(currentValue, oldValue);
	}

	public final String removeCode()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.code.getValue();
		String removedValue = this.code.removeOverride();
		String currentValue = this.code.getValue();

		fireCodeChange(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultCode(String code)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.code.getValue();
		this.code.setDefaultValue(code);
		String currentValue = this.code.getValue();

		fireCodeChange(currentValue, oldValue);
	}

	public final String removeDefaultCode()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.code.getValue();
		String removedValue = this.code.removeDefaultValue();
		String currentValue = this.code.getValue();

		fireCodeChange(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getCodeValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.code;
	}

	public final void fireCodeChange(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeCodeChanged(currentValue);
			firePropertyChange(PROPERTYNAME_CODE, oldValue, currentValue);
			afterCodeChanged(currentValue);
		}
	}

	public void beforeCodeChanged(String code)
	{}

	public void afterCodeChanged(String code)
	{}

	public boolean isCodeReadOnly()
	{
		return false;
	}

	public boolean isCodeEnabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "2"), @LocalizedTag(language = "fr", name = "column", value = "2"),
			@LocalizedTag(language = "en", name = "label", value = "Code"), @LocalizedTag(language = "fr", name = "label", value = "Code") })
	@Visible("true")
	@Filtrable(value = "true", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getCode()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.code.getValue();
	}

	public final void setDimension(String dimension)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.dimension.getValue();
		this.dimension.setValue(dimension);
		String currentValue = this.dimension.getValue();

		fireDimensionChange(currentValue, oldValue);
	}

	public final String removeDimension()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.dimension.getValue();
		String removedValue = this.dimension.removeOverride();
		String currentValue = this.dimension.getValue();

		fireDimensionChange(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultDimension(String dimension)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.dimension.getValue();
		this.dimension.setDefaultValue(dimension);
		String currentValue = this.dimension.getValue();

		fireDimensionChange(currentValue, oldValue);
	}

	public final String removeDefaultDimension()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.dimension.getValue();
		String removedValue = this.dimension.removeDefaultValue();
		String currentValue = this.dimension.getValue();

		fireDimensionChange(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getDimensionValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.dimension;
	}

	public final void fireDimensionChange(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeDimensionChanged(currentValue);
			firePropertyChange(PROPERTYNAME_DIMENSION, oldValue, currentValue);
			afterDimensionChanged(currentValue);
		}
	}

	public void beforeDimensionChanged(String dimension)
	{}

	public void afterDimensionChanged(String dimension)
	{}

	public boolean isDimensionReadOnly()
	{
		return false;
	}

	public boolean isDimensionEnabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "3"), @LocalizedTag(language = "fr", name = "column", value = "3"),
			@LocalizedTag(language = "en", name = "label", value = "Dimension"), @LocalizedTag(language = "fr", name = "label", value = "Dimension") })
	@Visible("true")
	@Filtrable(value = "true", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getDimension()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.dimension.getValue();
	}

	public final void setFrame(String frame)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.frame.getValue();
		this.frame.setValue(frame);
		String currentValue = this.frame.getValue();

		fireFrameChange(currentValue, oldValue);
	}

	public final String removeFrame()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.frame.getValue();
		String removedValue = this.frame.removeOverride();
		String currentValue = this.frame.getValue();

		fireFrameChange(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultFrame(String frame)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.frame.getValue();
		this.frame.setDefaultValue(frame);
		String currentValue = this.frame.getValue();

		fireFrameChange(currentValue, oldValue);
	}

	public final String removeDefaultFrame()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.frame.getValue();
		String removedValue = this.frame.removeDefaultValue();
		String currentValue = this.frame.getValue();

		fireFrameChange(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getFrameValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.frame;
	}

	public final void fireFrameChange(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeFrameChanged(currentValue);
			firePropertyChange(PROPERTYNAME_FRAME, oldValue, currentValue);
			afterFrameChanged(currentValue);
		}
	}

	public void beforeFrameChanged(String frame)
	{}

	public void afterFrameChanged(String frame)
	{}

	public boolean isFrameReadOnly()
	{
		return false;
	}

	public boolean isFrameEnabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "4"), @LocalizedTag(language = "fr", name = "column", value = "4"),
			@LocalizedTag(language = "en", name = "label", value = "Frame"), @LocalizedTag(language = "fr", name = "label", value = "Cadre") })
	@Visible("true")
	@Filtrable(value = "false", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getFrame()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.frame.getValue();
	}

	public final void setGlass(String glass)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.glass.getValue();
		this.glass.setValue(glass);
		String currentValue = this.glass.getValue();

		fireGlassChange(currentValue, oldValue);
	}

	public final String removeGlass()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.glass.getValue();
		String removedValue = this.glass.removeOverride();
		String currentValue = this.glass.getValue();

		fireGlassChange(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultGlass(String glass)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.glass.getValue();
		this.glass.setDefaultValue(glass);
		String currentValue = this.glass.getValue();

		fireGlassChange(currentValue, oldValue);
	}

	public final String removeDefaultGlass()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.glass.getValue();
		String removedValue = this.glass.removeDefaultValue();
		String currentValue = this.glass.getValue();

		fireGlassChange(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getGlassValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.glass;
	}

	public final void fireGlassChange(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeGlassChanged(currentValue);
			firePropertyChange(PROPERTYNAME_GLASS, oldValue, currentValue);
			afterGlassChanged(currentValue);
		}
	}

	public void beforeGlassChanged(String glass)
	{}

	public void afterGlassChanged(String glass)
	{}

	public boolean isGlassReadOnly()
	{
		return false;
	}

	public boolean isGlassEnabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "5"), @LocalizedTag(language = "fr", name = "column", value = "5"),
			@LocalizedTag(language = "en", name = "label", value = "Sealed Unit"), @LocalizedTag(language = "fr", name = "label", value = "Unit� scell�e") })
	@Visible("true")
	@Filtrable(value = "true", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getGlass()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.glass.getValue();
	}

	public final void setPrivacy(String privacy)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.privacy.getValue();
		this.privacy.setValue(privacy);
		String currentValue = this.privacy.getValue();

		firePrivacyChange(currentValue, oldValue);
	}

	public final String removePrivacy()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.privacy.getValue();
		String removedValue = this.privacy.removeOverride();
		String currentValue = this.privacy.getValue();

		firePrivacyChange(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultPrivacy(String privacy)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.privacy.getValue();
		this.privacy.setDefaultValue(privacy);
		String currentValue = this.privacy.getValue();

		firePrivacyChange(currentValue, oldValue);
	}

	public final String removeDefaultPrivacy()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.privacy.getValue();
		String removedValue = this.privacy.removeDefaultValue();
		String currentValue = this.privacy.getValue();

		firePrivacyChange(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getPrivacyValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.privacy;
	}

	public final void firePrivacyChange(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforePrivacyChanged(currentValue);
			firePropertyChange(PROPERTYNAME_PRIVACY, oldValue, currentValue);
			afterPrivacyChanged(currentValue);
		}
	}

	public void beforePrivacyChanged(String privacy)
	{}

	public void afterPrivacyChanged(String privacy)
	{}

	public boolean isPrivacyReadOnly()
	{
		return false;
	}

	public boolean isPrivacyEnabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "6"), @LocalizedTag(language = "fr", name = "column", value = "6"),
			@LocalizedTag(language = "en", name = "label", value = "Privacy"), @LocalizedTag(language = "fr", name = "label", value = "Intimit�") })
	@Visible("true")
	@Filtrable(value = "true", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getPrivacy()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.privacy.getValue();
	}

	public final void setForm(String form)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.form.getValue();
		this.form.setValue(form);
		String currentValue = this.form.getValue();

		fireFormChange(currentValue, oldValue);
	}

	public final String removeForm()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.form.getValue();
		String removedValue = this.form.removeOverride();
		String currentValue = this.form.getValue();

		fireFormChange(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultForm(String form)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.form.getValue();
		this.form.setDefaultValue(form);
		String currentValue = this.form.getValue();

		fireFormChange(currentValue, oldValue);
	}

	public final String removeDefaultForm()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.form.getValue();
		String removedValue = this.form.removeDefaultValue();
		String currentValue = this.form.getValue();

		fireFormChange(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getFormValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.form;
	}

	public final void fireFormChange(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeFormChanged(currentValue);
			firePropertyChange(PROPERTYNAME_FORM, oldValue, currentValue);
			afterFormChanged(currentValue);
		}
	}

	public void beforeFormChanged(String form)
	{}

	public void afterFormChanged(String form)
	{}

	public boolean isFormReadOnly()
	{
		return false;
	}

	public boolean isFormEnabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "7"), @LocalizedTag(language = "fr", name = "column", value = "7"),
			@LocalizedTag(language = "en", name = "label", value = "Shape"), @LocalizedTag(language = "fr", name = "label", value = "Forme") })
	@Visible("true")
	@Filtrable(value = "true", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getForm()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.form.getValue();
	}

	public final void setNote1(String note1)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.note1.getValue();
		this.note1.setValue(note1);
		String currentValue = this.note1.getValue();

		fireNote1Change(currentValue, oldValue);
	}

	public final String removeNote1()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.note1.getValue();
		String removedValue = this.note1.removeOverride();
		String currentValue = this.note1.getValue();

		fireNote1Change(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultNote1(String note1)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.note1.getValue();
		this.note1.setDefaultValue(note1);
		String currentValue = this.note1.getValue();

		fireNote1Change(currentValue, oldValue);
	}

	public final String removeDefaultNote1()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.note1.getValue();
		String removedValue = this.note1.removeDefaultValue();
		String currentValue = this.note1.getValue();

		fireNote1Change(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getNote1ValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.note1;
	}

	public final void fireNote1Change(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeNote1Changed(currentValue);
			firePropertyChange(PROPERTYNAME_NOTE1, oldValue, currentValue);
			afterNote1Changed(currentValue);
		}
	}

	public void beforeNote1Changed(String note1)
	{}

	public void afterNote1Changed(String note1)
	{}

	public boolean isNote1ReadOnly()
	{
		return false;
	}

	public boolean isNote1Enabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "8"), @LocalizedTag(language = "fr", name = "column", value = "8"),
			@LocalizedTag(language = "en", name = "label", value = "Note 1"), @LocalizedTag(language = "fr", name = "label", value = "Note 1") })
	@Visible("true")
	@Filtrable(value = "false", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getNote1()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.note1.getValue();
	}

	public final void setNote2(String note2)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.note2.getValue();
		this.note2.setValue(note2);
		String currentValue = this.note2.getValue();

		fireNote2Change(currentValue, oldValue);
	}

	public final String removeNote2()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.note2.getValue();
		String removedValue = this.note2.removeOverride();
		String currentValue = this.note2.getValue();

		fireNote2Change(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultNote2(String note2)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.note2.getValue();
		this.note2.setDefaultValue(note2);
		String currentValue = this.note2.getValue();

		fireNote2Change(currentValue, oldValue);
	}

	public final String removeDefaultNote2()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.note2.getValue();
		String removedValue = this.note2.removeDefaultValue();
		String currentValue = this.note2.getValue();

		fireNote2Change(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getNote2ValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.note2;
	}

	public final void fireNote2Change(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeNote2Changed(currentValue);
			firePropertyChange(PROPERTYNAME_NOTE2, oldValue, currentValue);
			afterNote2Changed(currentValue);
		}
	}

	public void beforeNote2Changed(String note2)
	{}

	public void afterNote2Changed(String note2)
	{}

	public boolean isNote2ReadOnly()
	{
		return false;
	}

	public boolean isNote2Enabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "9"), @LocalizedTag(language = "fr", name = "column", value = "9"),
			@LocalizedTag(language = "en", name = "label", value = "Note 2"), @LocalizedTag(language = "fr", name = "label", value = "Note 2") })
	@Visible("true")
	@Filtrable(value = "false", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getNote2()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.note2.getValue();
	}

	public final void setImage(String image)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.image.getValue();
		this.image.setValue(image);
		String currentValue = this.image.getValue();

		fireImageChange(currentValue, oldValue);
	}

	public final String removeImage()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.image.getValue();
		String removedValue = this.image.removeOverride();
		String currentValue = this.image.getValue();

		fireImageChange(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultImage(String image)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.image.getValue();
		this.image.setDefaultValue(image);
		String currentValue = this.image.getValue();

		fireImageChange(currentValue, oldValue);
	}

	public final String removeDefaultImage()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.image.getValue();
		String removedValue = this.image.removeDefaultValue();
		String currentValue = this.image.getValue();

		fireImageChange(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getImageValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.image;
	}

	public final void fireImageChange(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeImageChanged(currentValue);
			firePropertyChange(PROPERTYNAME_IMAGE, oldValue, currentValue);
			afterImageChanged(currentValue);
		}
	}

	public void beforeImageChanged(String image)
	{}

	public void afterImageChanged(String image)
	{}

	public boolean isImageReadOnly()
	{
		return false;
	}

	public boolean isImageEnabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "10"), @LocalizedTag(language = "fr", name = "column", value = "10"),
			@LocalizedTag(language = "en", name = "label", value = "Image"), @LocalizedTag(language = "fr", name = "label", value = "Image") })
	@Visible("false")
	@Filtrable(value = "false", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getImage()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.image.getValue();
	}

	public final void setWidth(String width)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.width.getValue();
		this.width.setValue(width);
		String currentValue = this.width.getValue();

		fireWidthChange(currentValue, oldValue);
	}

	public final String removeWidth()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.width.getValue();
		String removedValue = this.width.removeOverride();
		String currentValue = this.width.getValue();

		fireWidthChange(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultWidth(String width)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.width.getValue();
		this.width.setDefaultValue(width);
		String currentValue = this.width.getValue();

		fireWidthChange(currentValue, oldValue);
	}

	public final String removeDefaultWidth()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.width.getValue();
		String removedValue = this.width.removeDefaultValue();
		String currentValue = this.width.getValue();

		fireWidthChange(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getWidthValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.width;
	}

	public final void fireWidthChange(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeWidthChanged(currentValue);
			firePropertyChange(PROPERTYNAME_WIDTH, oldValue, currentValue);
			afterWidthChanged(currentValue);
		}
	}

	public void beforeWidthChanged(String width)
	{}

	public void afterWidthChanged(String width)
	{}

	public boolean isWidthReadOnly()
	{
		return false;
	}

	public boolean isWidthEnabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "11"), @LocalizedTag(language = "fr", name = "column", value = "11"),
			@LocalizedTag(language = "en", name = "label", value = "Width"), @LocalizedTag(language = "fr", name = "label", value = "Largeur") })
	@Visible("false")
	@Filtrable(value = "false", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getWidth()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.width.getValue();
	}

	public final void setHeight(String height)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.height.getValue();
		this.height.setValue(height);
		String currentValue = this.height.getValue();

		fireHeightChange(currentValue, oldValue);
	}

	public final String removeHeight()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.height.getValue();
		String removedValue = this.height.removeOverride();
		String currentValue = this.height.getValue();

		fireHeightChange(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultHeight(String height)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.height.getValue();
		this.height.setDefaultValue(height);
		String currentValue = this.height.getValue();

		fireHeightChange(currentValue, oldValue);
	}

	public final String removeDefaultHeight()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.height.getValue();
		String removedValue = this.height.removeDefaultValue();
		String currentValue = this.height.getValue();

		fireHeightChange(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getHeightValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.height;
	}

	public final void fireHeightChange(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeHeightChanged(currentValue);
			firePropertyChange(PROPERTYNAME_HEIGHT, oldValue, currentValue);
			afterHeightChanged(currentValue);
		}
	}

	public void beforeHeightChanged(String height)
	{}

	public void afterHeightChanged(String height)
	{}

	public boolean isHeightReadOnly()
	{
		return false;
	}

	public boolean isHeightEnabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "12"), @LocalizedTag(language = "fr", name = "column", value = "12"),
			@LocalizedTag(language = "en", name = "label", value = "Height"), @LocalizedTag(language = "fr", name = "label", value = "Hauteur") })
	@Visible("false")
	@Filtrable(value = "false", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getHeight()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.height.getValue();
	}

	public final void setStainedGlassForDoor(String stainedGlassForDoor)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.stainedGlassForDoor.getValue();
		this.stainedGlassForDoor.setValue(stainedGlassForDoor);
		String currentValue = this.stainedGlassForDoor.getValue();

		fireStainedGlassForDoorChange(currentValue, oldValue);
	}

	public final String removeStainedGlassForDoor()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.stainedGlassForDoor.getValue();
		String removedValue = this.stainedGlassForDoor.removeOverride();
		String currentValue = this.stainedGlassForDoor.getValue();

		fireStainedGlassForDoorChange(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultStainedGlassForDoor(String stainedGlassForDoor)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.stainedGlassForDoor.getValue();
		this.stainedGlassForDoor.setDefaultValue(stainedGlassForDoor);
		String currentValue = this.stainedGlassForDoor.getValue();

		fireStainedGlassForDoorChange(currentValue, oldValue);
	}

	public final String removeDefaultStainedGlassForDoor()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.stainedGlassForDoor.getValue();
		String removedValue = this.stainedGlassForDoor.removeDefaultValue();
		String currentValue = this.stainedGlassForDoor.getValue();

		fireStainedGlassForDoorChange(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getStainedGlassForDoorValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.stainedGlassForDoor;
	}

	public final void fireStainedGlassForDoorChange(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeStainedGlassForDoorChanged(currentValue);
			firePropertyChange(PROPERTYNAME_STAINEDGLASSFORDOOR, oldValue, currentValue);
			afterStainedGlassForDoorChanged(currentValue);
		}
	}

	public void beforeStainedGlassForDoorChanged(String stainedGlassForDoor)
	{}

	public void afterStainedGlassForDoorChanged(String stainedGlassForDoor)
	{}

	public boolean isStainedGlassForDoorReadOnly()
	{
		return false;
	}

	public boolean isStainedGlassForDoorEnabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "13"), @LocalizedTag(language = "fr", name = "column", value = "13"),
			@LocalizedTag(language = "en", name = "label", value = "Stained Glass pour porte"),
			@LocalizedTag(language = "fr", name = "label", value = "vitrail pour porte") })
	@Visible("false")
	@Filtrable(value = "false", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getStainedGlassForDoor()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.stainedGlassForDoor.getValue();
	}

	public final void setHeight79(String height79)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.height79.getValue();
		this.height79.setValue(height79);
		String currentValue = this.height79.getValue();

		fireHeight79Change(currentValue, oldValue);
	}

	public final String removeHeight79()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.height79.getValue();
		String removedValue = this.height79.removeOverride();
		String currentValue = this.height79.getValue();

		fireHeight79Change(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultHeight79(String height79)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.height79.getValue();
		this.height79.setDefaultValue(height79);
		String currentValue = this.height79.getValue();

		fireHeight79Change(currentValue, oldValue);
	}

	public final String removeDefaultHeight79()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.height79.getValue();
		String removedValue = this.height79.removeDefaultValue();
		String currentValue = this.height79.getValue();

		fireHeight79Change(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getHeight79ValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.height79;
	}

	public final void fireHeight79Change(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeHeight79Changed(currentValue);
			firePropertyChange(PROPERTYNAME_HEIGHT79, oldValue, currentValue);
			afterHeight79Changed(currentValue);
		}
	}

	public void beforeHeight79Changed(String height79)
	{}

	public void afterHeight79Changed(String height79)
	{}

	public boolean isHeight79ReadOnly()
	{
		return false;
	}

	public boolean isHeight79Enabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "14"), @LocalizedTag(language = "fr", name = "column", value = "14"),
			@LocalizedTag(language = "en", name = "label", value = "height79"), @LocalizedTag(language = "fr", name = "label", value = "height79") })
	@Visible("false")
	@Filtrable(value = "false", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getHeight79()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.height79.getValue();
	}

	public final void setHeight84(String height84)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.height84.getValue();
		this.height84.setValue(height84);
		String currentValue = this.height84.getValue();

		fireHeight84Change(currentValue, oldValue);
	}

	public final String removeHeight84()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.height84.getValue();
		String removedValue = this.height84.removeOverride();
		String currentValue = this.height84.getValue();

		fireHeight84Change(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultHeight84(String height84)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.height84.getValue();
		this.height84.setDefaultValue(height84);
		String currentValue = this.height84.getValue();

		fireHeight84Change(currentValue, oldValue);
	}

	public final String removeDefaultHeight84()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.height84.getValue();
		String removedValue = this.height84.removeDefaultValue();
		String currentValue = this.height84.getValue();

		fireHeight84Change(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getHeight84ValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.height84;
	}

	public final void fireHeight84Change(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeHeight84Changed(currentValue);
			firePropertyChange(PROPERTYNAME_HEIGHT84, oldValue, currentValue);
			afterHeight84Changed(currentValue);
		}
	}

	public void beforeHeight84Changed(String height84)
	{}

	public void afterHeight84Changed(String height84)
	{}

	public boolean isHeight84ReadOnly()
	{
		return false;
	}

	public boolean isHeight84Enabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "15"), @LocalizedTag(language = "fr", name = "column", value = "15"),
			@LocalizedTag(language = "en", name = "label", value = "Height84"), @LocalizedTag(language = "fr", name = "label", value = "Height84") })
	@Visible("false")
	@Filtrable(value = "false", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getHeight84()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.height84.getValue();
	}

	public final void setHeight95(String height95)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.height95.getValue();
		this.height95.setValue(height95);
		String currentValue = this.height95.getValue();

		fireHeight95Change(currentValue, oldValue);
	}

	public final String removeHeight95()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.height95.getValue();
		String removedValue = this.height95.removeOverride();
		String currentValue = this.height95.getValue();

		fireHeight95Change(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultHeight95(String height95)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.height95.getValue();
		this.height95.setDefaultValue(height95);
		String currentValue = this.height95.getValue();

		fireHeight95Change(currentValue, oldValue);
	}

	public final String removeDefaultHeight95()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.height95.getValue();
		String removedValue = this.height95.removeDefaultValue();
		String currentValue = this.height95.getValue();

		fireHeight95Change(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getHeight95ValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.height95;
	}

	public final void fireHeight95Change(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeHeight95Changed(currentValue);
			firePropertyChange(PROPERTYNAME_HEIGHT95, oldValue, currentValue);
			afterHeight95Changed(currentValue);
		}
	}

	public void beforeHeight95Changed(String height95)
	{}

	public void afterHeight95Changed(String height95)
	{}

	public boolean isHeight95ReadOnly()
	{
		return false;
	}

	public boolean isHeight95Enabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "16"), @LocalizedTag(language = "fr", name = "column", value = "16"),
			@LocalizedTag(language = "en", name = "label", value = "height95"), @LocalizedTag(language = "fr", name = "label", value = "height95") })
	@Visible("false")
	@Filtrable(value = "false", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getHeight95()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.height95.getValue();
	}

	public final void setParametricModelClass(String parametricModelClass)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.parametricModelClass.getValue();
		this.parametricModelClass.setValue(parametricModelClass);
		String currentValue = this.parametricModelClass.getValue();

		fireParametricModelClassChange(currentValue, oldValue);
	}

	public final String removeParametricModelClass()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.parametricModelClass.getValue();
		String removedValue = this.parametricModelClass.removeOverride();
		String currentValue = this.parametricModelClass.getValue();

		fireParametricModelClassChange(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultParametricModelClass(String parametricModelClass)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.parametricModelClass.getValue();
		this.parametricModelClass.setDefaultValue(parametricModelClass);
		String currentValue = this.parametricModelClass.getValue();

		fireParametricModelClassChange(currentValue, oldValue);
	}

	public final String removeDefaultParametricModelClass()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.parametricModelClass.getValue();
		String removedValue = this.parametricModelClass.removeDefaultValue();
		String currentValue = this.parametricModelClass.getValue();

		fireParametricModelClassChange(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getParametricModelClassValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.parametricModelClass;
	}

	public final void fireParametricModelClassChange(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeParametricModelClassChanged(currentValue);
			firePropertyChange(PROPERTYNAME_PARAMETRICMODELCLASS, oldValue, currentValue);
			afterParametricModelClassChanged(currentValue);
		}
	}

	public void beforeParametricModelClassChanged(String parametricModelClass)
	{}

	public void afterParametricModelClassChanged(String parametricModelClass)
	{}

	public boolean isParametricModelClassReadOnly()
	{
		return false;
	}

	public boolean isParametricModelClassEnabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "19"), @LocalizedTag(language = "fr", name = "column", value = "19"),
			@LocalizedTag(language = "en", name = "label", value = "ParametricModelClass"),
			@LocalizedTag(language = "fr", name = "label", value = "ParametricModelClass") })
	@Visible("false")
	@Filtrable(value = "false", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getParametricModelClass()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.parametricModelClass.getValue();
	}

	public final void setParametricModelClassSidelite(String parametricModelClassSidelite)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.parametricModelClassSidelite.getValue();
		this.parametricModelClassSidelite.setValue(parametricModelClassSidelite);
		String currentValue = this.parametricModelClassSidelite.getValue();

		fireParametricModelClassSideliteChange(currentValue, oldValue);
	}

	public final String removeParametricModelClassSidelite()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.parametricModelClassSidelite.getValue();
		String removedValue = this.parametricModelClassSidelite.removeOverride();
		String currentValue = this.parametricModelClassSidelite.getValue();

		fireParametricModelClassSideliteChange(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultParametricModelClassSidelite(String parametricModelClassSidelite)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.parametricModelClassSidelite.getValue();
		this.parametricModelClassSidelite.setDefaultValue(parametricModelClassSidelite);
		String currentValue = this.parametricModelClassSidelite.getValue();

		fireParametricModelClassSideliteChange(currentValue, oldValue);
	}

	public final String removeDefaultParametricModelClassSidelite()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.parametricModelClassSidelite.getValue();
		String removedValue = this.parametricModelClassSidelite.removeDefaultValue();
		String currentValue = this.parametricModelClassSidelite.getValue();

		fireParametricModelClassSideliteChange(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getParametricModelClassSideliteValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.parametricModelClassSidelite;
	}

	public final void fireParametricModelClassSideliteChange(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeParametricModelClassSideliteChanged(currentValue);
			firePropertyChange(PROPERTYNAME_PARAMETRICMODELCLASSSIDELITE, oldValue, currentValue);
			afterParametricModelClassSideliteChanged(currentValue);
		}
	}

	public void beforeParametricModelClassSideliteChanged(String parametricModelClassSidelite)
	{}

	public void afterParametricModelClassSideliteChanged(String parametricModelClassSidelite)
	{}

	public boolean isParametricModelClassSideliteReadOnly()
	{
		return false;
	}

	public boolean isParametricModelClassSideliteEnabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "20"), @LocalizedTag(language = "fr", name = "column", value = "20"),
			@LocalizedTag(language = "en", name = "label", value = "ParametricModelClassSidelite"),
			@LocalizedTag(language = "fr", name = "label", value = "ParametricModelClassSidelite") })
	@Visible("false")
	@Filtrable(value = "false", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getParametricModelClassSidelite()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.parametricModelClassSidelite.getValue();
	}

	public final void setAskStainedGlassPosition(String askStainedGlassPosition)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.askStainedGlassPosition.getValue();
		this.askStainedGlassPosition.setValue(askStainedGlassPosition);
		String currentValue = this.askStainedGlassPosition.getValue();

		fireAskStainedGlassPositionChange(currentValue, oldValue);
	}

	public final String removeAskStainedGlassPosition()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.askStainedGlassPosition.getValue();
		String removedValue = this.askStainedGlassPosition.removeOverride();
		String currentValue = this.askStainedGlassPosition.getValue();

		fireAskStainedGlassPositionChange(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultAskStainedGlassPosition(String askStainedGlassPosition)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.askStainedGlassPosition.getValue();
		this.askStainedGlassPosition.setDefaultValue(askStainedGlassPosition);
		String currentValue = this.askStainedGlassPosition.getValue();

		fireAskStainedGlassPositionChange(currentValue, oldValue);
	}

	public final String removeDefaultAskStainedGlassPosition()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.askStainedGlassPosition.getValue();
		String removedValue = this.askStainedGlassPosition.removeDefaultValue();
		String currentValue = this.askStainedGlassPosition.getValue();

		fireAskStainedGlassPositionChange(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getAskStainedGlassPositionValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.askStainedGlassPosition;
	}

	public final void fireAskStainedGlassPositionChange(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeAskStainedGlassPositionChanged(currentValue);
			firePropertyChange(PROPERTYNAME_ASKSTAINEDGLASSPOSITION, oldValue, currentValue);
			afterAskStainedGlassPositionChanged(currentValue);
		}
	}

	public void beforeAskStainedGlassPositionChanged(String askStainedGlassPosition)
	{}

	public void afterAskStainedGlassPositionChanged(String askStainedGlassPosition)
	{}

	public boolean isAskStainedGlassPositionReadOnly()
	{
		return false;
	}

	public boolean isAskStainedGlassPositionEnabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "21"), @LocalizedTag(language = "fr", name = "column", value = "21"),
			@LocalizedTag(language = "en", name = "label", value = "askStainedGlassPosition"),
			@LocalizedTag(language = "fr", name = "label", value = "askStainedGlassPosition") })
	@Visible("false")
	@Filtrable(value = "false", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getAskStainedGlassPosition()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.askStainedGlassPosition.getValue();
	}

	public final void setFormCode(String formCode)
	{ // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

		String oldValue = this.formCode.getValue();
		this.formCode.setValue(formCode);
		String currentValue = this.formCode.getValue();

		fireFormCodeChange(currentValue, oldValue);
	}

	public final String removeFormCode()
	{ // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
		String oldValue = this.formCode.getValue();
		String removedValue = this.formCode.removeOverride();
		String currentValue = this.formCode.getValue();

		fireFormCodeChange(currentValue, oldValue);
		return removedValue;
	}

	public final void setDefaultFormCode(String formCode)
	{ // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
		String oldValue = this.formCode.getValue();
		this.formCode.setDefaultValue(formCode);
		String currentValue = this.formCode.getValue();

		fireFormCodeChange(currentValue, oldValue);
	}

	public final String removeDefaultFormCode()
	{ // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
		String oldValue = this.formCode.getValue();
		String removedValue = this.formCode.removeDefaultValue();
		String currentValue = this.formCode.getValue();

		fireFormCodeChange(currentValue, oldValue);
		return removedValue;
	}

	public final ValueContainerGetter<String> getFormCodeValueContainer()
	{ // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
		return this.formCode;
	}

	public final void fireFormCodeChange(String currentValue, String oldValue)
	{ // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
		if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
		{
			beforeFormCodeChanged(currentValue);
			firePropertyChange(PROPERTYNAME_FORMCODE, oldValue, currentValue);
			afterFormCodeChanged(currentValue);
		}
	}

	public void beforeFormCodeChanged(String formCode)
	{}

	public void afterFormCodeChanged(String formCode)
	{}

	public boolean isFormCodeReadOnly()
	{
		return false;
	}

	public boolean isFormCodeEnabled()
	{
		return true;
	}

	@LocalizedTags({ @LocalizedTag(language = "en", name = "column", value = "22"), @LocalizedTag(language = "fr", name = "column", value = "22"),
			@LocalizedTag(language = "en", name = "label", value = "Code forme"), @LocalizedTag(language = "fr", name = "label", value = "askStainedGlassSide") })
	@Visible("true")
	@Filtrable(value = "true", defaultValue = "")
	@AssociatedComment("false")
	@AssociatedDrawing("false")
	public final String getFormCode()
	{ // GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
		return this.formCode.getValue();
	}

	// Business methods

	public boolean isSerieVisible()
	{
		return true;
	}

	public boolean isModelVisible()
	{
		return true;
	}

	public boolean isCodeVisible()
	{
		return true;
	}

	public boolean isDimensionVisible()
	{
		return true;
	}

	public boolean isFrameVisible()
	{
		return true;
	}

	public boolean isGlassVisible()
	{
		return true;
	}

	public boolean isPrivacyVisible()
	{
		return true;
	}

	public boolean isFormVisible()
	{
		return true;
	}

	public boolean isNote1Visible()
	{
		return true;
	}

	public boolean isNote2Visible()
	{
		return true;
	}

	public boolean isImageVisible()
	{
		return false;
	}

	public boolean isWidthVisible()
	{
		return false;
	}

	public boolean isHeightVisible()
	{
		return false;
	}

	public boolean isStainedGlassForDoorVisible()
	{
		return false;
	}

	public boolean isHeight79Visible()
	{
		return false;
	}

	public boolean isHeight84Visible()
	{
		return false;
	}

	public boolean isHeight95Visible()
	{
		return false;
	}

	public boolean isParametricModelClassVisible()
	{
		return false;
	}

	public boolean isParametricModelClassSideliteVisible()
	{
		return false;
	}

	public boolean isAskStainedGlassPositionVisible()
	{
		return false;
	}

	public boolean isFormCodeVisible()
	{
		return true;
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
	}

	// Bound properties

	public static final String PROPERTYNAME_SERIE = "serie";
	public static final String PROPERTYNAME_MODEL = "model";
	public static final String PROPERTYNAME_CODE = "code";
	public static final String PROPERTYNAME_DIMENSION = "dimension";
	public static final String PROPERTYNAME_FRAME = "frame";
	public static final String PROPERTYNAME_GLASS = "glass";
	public static final String PROPERTYNAME_PRIVACY = "privacy";
	public static final String PROPERTYNAME_FORM = "form";
	public static final String PROPERTYNAME_NOTE1 = "note1";
	public static final String PROPERTYNAME_NOTE2 = "note2";
	public static final String PROPERTYNAME_IMAGE = "image";
	public static final String PROPERTYNAME_WIDTH = "width";
	public static final String PROPERTYNAME_HEIGHT = "height";
	public static final String PROPERTYNAME_STAINEDGLASSFORDOOR = "stainedGlassForDoor";
	public static final String PROPERTYNAME_HEIGHT79 = "height79";
	public static final String PROPERTYNAME_HEIGHT84 = "height84";
	public static final String PROPERTYNAME_HEIGHT95 = "height95";
	public static final String PROPERTYNAME_PARAMETRICMODELCLASS = "parametricModelClass";
	public static final String PROPERTYNAME_PARAMETRICMODELCLASSSIDELITE = "parametricModelClassSidelite";
	public static final String PROPERTYNAME_ASKSTAINEDGLASSPOSITION = "askStainedGlassPosition";
	public static final String PROPERTYNAME_FORMCODE = "formCode";
}
