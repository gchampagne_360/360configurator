// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class SliderBase extends com.client360.configuration.wad.SliderMain
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(SliderBase.class);

    // attributes
    private ValueContainer<Boolean> hybrid = new ValueContainer<Boolean>(this);

    // Bound properties

    public static final String PROPERTYNAME_HYBRID = "hybrid";  

    // Constructors

    public SliderBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.Slider"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="desc", value="Hybrid")
        ,@LocalizedTag(language="fr", name="desc", value="Hybride")
        ,@LocalizedTag(language="en", name="label", value="Hybrid")
        ,@LocalizedTag(language="fr", name="label", value="Hybride")
    })
    public final Boolean getHybrid()
    {
         return this.hybrid.getValue();
    }

    public final void setHybrid(Boolean hybrid)
    {

        Boolean oldValue = this.hybrid.getValue();
        this.hybrid.setValue(hybrid);

        Boolean currentValue = this.hybrid.getValue();

        fireHybridChange(currentValue, oldValue);
    }
    public final Boolean removeHybrid()
    {
        Boolean oldValue = this.hybrid.getValue();
        Boolean removedValue = this.hybrid.removeValue();
        Boolean currentValue = this.hybrid.getValue();

        fireHybridChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultHybrid(Boolean hybrid)
    {
        Boolean oldValue = this.hybrid.getValue();
        this.hybrid.setDefaultValue(hybrid);

        Boolean currentValue = this.hybrid.getValue();

        fireHybridChange(currentValue, oldValue);
    }
    
    public final Boolean removeDefaultHybrid()
    {
        Boolean oldValue = this.hybrid.getValue();
        Boolean removedValue = this.hybrid.removeDefaultValue();
        Boolean currentValue = this.hybrid.getValue();

        fireHybridChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Boolean> getHybridValueContainer()
    {
        return this.hybrid;
    }
    public final void fireHybridChange(Boolean currentValue, Boolean oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeHybridChanged(currentValue);
            firePropertyChange(PROPERTYNAME_HYBRID, oldValue, currentValue);
            afterHybridChanged(currentValue);
        }
    }

    public void beforeHybridChanged( Boolean hybrid)
    { }
    public void afterHybridChanged( Boolean hybrid)
    { }



    @Override
    protected String getSequences()
    {
        return "hybrid,nbSections,ExteriorColor,InteriorColor,dimension,halfExterior,halfInterior,withInsulatedGlass,choiceOpeningSide,interiorSash,frameDepth,energyStar,installed,qDQ,sameGrilles,showMeetingRail,showMullion,noFrame,windowsMullion,paintable,renderingNumber,sectionInput,section,sash,meetingRail,division,choicePresetWindowWidth,choiceOpeningType,choiceMosquito";  
    }
}
