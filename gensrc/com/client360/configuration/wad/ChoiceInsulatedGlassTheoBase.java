// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.DynamicEnum;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

@DynamicEnum(fileName = "dynamicEnums/TP_Thermos_Price.tsv", decimalSeparator = ",")
public abstract class ChoiceInsulatedGlassTheoBase extends com.netappsid.erp.configurator.Configurable
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ChoiceInsulatedGlassTheoBase.class);

    // attributes
    private ValueContainer<String> description = new ValueContainer<String>(this);
    private ValueContainer<Double> id = new ValueContainer<Double>(this);
    private ValueContainer<Integer> isForPatioDoor = new ValueContainer<Integer>(this);
    private ValueContainer<String> isTriple = new ValueContainer<String>(this);
    private ValueContainer<String> price = new ValueContainer<String>(this);
    private ValueContainer<String> special = new ValueContainer<String>(this);

    // Bound properties

    public static final String PROPERTYNAME_DESCRIPTION = "description";  
    public static final String PROPERTYNAME_ID = "id";  
    public static final String PROPERTYNAME_ISFORPATIODOOR = "isForPatioDoor";  
    public static final String PROPERTYNAME_ISTRIPLE = "isTriple";  
    public static final String PROPERTYNAME_PRICE = "price";  
    public static final String PROPERTYNAME_SPECIAL = "special";  

    // Constructors

    public ChoiceInsulatedGlassTheoBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.ChoiceInsulatedGlassTheo"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="2")
        ,@LocalizedTag(language="fr", name="column", value="1")
        ,@LocalizedTag(language="en", name="label", value="Description")
        ,@LocalizedTag(language="fr", name="label", value="Description")
    })
    public final String getDescription()
    {
         return this.description.getValue();
    }

    public final void setDescription(String description)
    {

        String oldValue = this.description.getValue();
        this.description.setValue(description);

        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
    }
    public final String removeDescription()
    {
        String oldValue = this.description.getValue();
        String removedValue = this.description.removeValue();
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultDescription(String description)
    {
        String oldValue = this.description.getValue();
        this.description.setDefaultValue(description);

        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
    }
    
    public final String removeDefaultDescription()
    {
        String oldValue = this.description.getValue();
        String removedValue = this.description.removeDefaultValue();
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getDescriptionValueContainer()
    {
        return this.description;
    }
    public final void fireDescriptionChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeDescriptionChanged(currentValue);
            firePropertyChange(PROPERTYNAME_DESCRIPTION, oldValue, currentValue);
            afterDescriptionChanged(currentValue);
        }
    }

    public void beforeDescriptionChanged( String description)
    { }
    public void afterDescriptionChanged( String description)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="0")
        ,@LocalizedTag(language="fr", name="column", value="0")
        ,@LocalizedTag(language="en", name="label", value="PID")
        ,@LocalizedTag(language="fr", name="label", value="Id")
    })
    public final Double getId()
    {
         return this.id.getValue();
    }

    public final void setId(Double id)
    {

        Double oldValue = this.id.getValue();
        this.id.setValue(id);

        Double currentValue = this.id.getValue();

        fireIdChange(currentValue, oldValue);
    }
    public final Double removeId()
    {
        Double oldValue = this.id.getValue();
        Double removedValue = this.id.removeValue();
        Double currentValue = this.id.getValue();

        fireIdChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultId(Double id)
    {
        Double oldValue = this.id.getValue();
        this.id.setDefaultValue(id);

        Double currentValue = this.id.getValue();

        fireIdChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultId()
    {
        Double oldValue = this.id.getValue();
        Double removedValue = this.id.removeDefaultValue();
        Double currentValue = this.id.getValue();

        fireIdChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getIdValueContainer()
    {
        return this.id;
    }
    public final void fireIdChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeIdChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ID, oldValue, currentValue);
            afterIdChanged(currentValue);
        }
    }

    public void beforeIdChanged( Double id)
    { }
    public void afterIdChanged( Double id)
    { }


    public boolean isIdVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="6")
        ,@LocalizedTag(language="fr", name="column", value="6")
    })
    public final Integer getIsForPatioDoor()
    {
         return this.isForPatioDoor.getValue();
    }

    public final void setIsForPatioDoor(Integer isForPatioDoor)
    {

        Integer oldValue = this.isForPatioDoor.getValue();
        this.isForPatioDoor.setValue(isForPatioDoor);

        Integer currentValue = this.isForPatioDoor.getValue();

        fireIsForPatioDoorChange(currentValue, oldValue);
    }
    public final Integer removeIsForPatioDoor()
    {
        Integer oldValue = this.isForPatioDoor.getValue();
        Integer removedValue = this.isForPatioDoor.removeValue();
        Integer currentValue = this.isForPatioDoor.getValue();

        fireIsForPatioDoorChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultIsForPatioDoor(Integer isForPatioDoor)
    {
        Integer oldValue = this.isForPatioDoor.getValue();
        this.isForPatioDoor.setDefaultValue(isForPatioDoor);

        Integer currentValue = this.isForPatioDoor.getValue();

        fireIsForPatioDoorChange(currentValue, oldValue);
    }
    
    public final Integer removeDefaultIsForPatioDoor()
    {
        Integer oldValue = this.isForPatioDoor.getValue();
        Integer removedValue = this.isForPatioDoor.removeDefaultValue();
        Integer currentValue = this.isForPatioDoor.getValue();

        fireIsForPatioDoorChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Integer> getIsForPatioDoorValueContainer()
    {
        return this.isForPatioDoor;
    }
    public final void fireIsForPatioDoorChange(Integer currentValue, Integer oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeIsForPatioDoorChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ISFORPATIODOOR, oldValue, currentValue);
            afterIsForPatioDoorChanged(currentValue);
        }
    }

    public void beforeIsForPatioDoorChanged( Integer isForPatioDoor)
    { }
    public void afterIsForPatioDoorChanged( Integer isForPatioDoor)
    { }


    public boolean isIsForPatioDoorVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="7")
        ,@LocalizedTag(language="fr", name="column", value="7")
    })
    public final String getIsTriple()
    {
         return this.isTriple.getValue();
    }

    public final void setIsTriple(String isTriple)
    {

        String oldValue = this.isTriple.getValue();
        this.isTriple.setValue(isTriple);

        String currentValue = this.isTriple.getValue();

        fireIsTripleChange(currentValue, oldValue);
    }
    public final String removeIsTriple()
    {
        String oldValue = this.isTriple.getValue();
        String removedValue = this.isTriple.removeValue();
        String currentValue = this.isTriple.getValue();

        fireIsTripleChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultIsTriple(String isTriple)
    {
        String oldValue = this.isTriple.getValue();
        this.isTriple.setDefaultValue(isTriple);

        String currentValue = this.isTriple.getValue();

        fireIsTripleChange(currentValue, oldValue);
    }
    
    public final String removeDefaultIsTriple()
    {
        String oldValue = this.isTriple.getValue();
        String removedValue = this.isTriple.removeDefaultValue();
        String currentValue = this.isTriple.getValue();

        fireIsTripleChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getIsTripleValueContainer()
    {
        return this.isTriple;
    }
    public final void fireIsTripleChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeIsTripleChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ISTRIPLE, oldValue, currentValue);
            afterIsTripleChanged(currentValue);
        }
    }

    public void beforeIsTripleChanged( String isTriple)
    { }
    public void afterIsTripleChanged( String isTriple)
    { }


    public boolean isIsTripleVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="5")
        ,@LocalizedTag(language="fr", name="column", value="5")
        ,@LocalizedTag(language="en", name="label", value="Price")
        ,@LocalizedTag(language="fr", name="label", value="Prix")
    })
    public final String getPrice()
    {
         return this.price.getValue();
    }

    public final void setPrice(String price)
    {

        String oldValue = this.price.getValue();
        this.price.setValue(price);

        String currentValue = this.price.getValue();

        firePriceChange(currentValue, oldValue);
    }
    public final String removePrice()
    {
        String oldValue = this.price.getValue();
        String removedValue = this.price.removeValue();
        String currentValue = this.price.getValue();

        firePriceChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultPrice(String price)
    {
        String oldValue = this.price.getValue();
        this.price.setDefaultValue(price);

        String currentValue = this.price.getValue();

        firePriceChange(currentValue, oldValue);
    }
    
    public final String removeDefaultPrice()
    {
        String oldValue = this.price.getValue();
        String removedValue = this.price.removeDefaultValue();
        String currentValue = this.price.getValue();

        firePriceChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getPriceValueContainer()
    {
        return this.price;
    }
    public final void firePriceChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforePriceChanged(currentValue);
            firePropertyChange(PROPERTYNAME_PRICE, oldValue, currentValue);
            afterPriceChanged(currentValue);
        }
    }

    public void beforePriceChanged( String price)
    { }
    public void afterPriceChanged( String price)
    { }


    public boolean isPriceVisible()
    {
        return false;
    }
    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="4")
        ,@LocalizedTag(language="fr", name="column", value="3")
        ,@LocalizedTag(language="en", name="label", value="Promotion")
        ,@LocalizedTag(language="fr", name="label", value="Promotion")
    })
    public final String getSpecial()
    {
         return this.special.getValue();
    }

    public final void setSpecial(String special)
    {

        String oldValue = this.special.getValue();
        this.special.setValue(special);

        String currentValue = this.special.getValue();

        fireSpecialChange(currentValue, oldValue);
    }
    public final String removeSpecial()
    {
        String oldValue = this.special.getValue();
        String removedValue = this.special.removeValue();
        String currentValue = this.special.getValue();

        fireSpecialChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultSpecial(String special)
    {
        String oldValue = this.special.getValue();
        this.special.setDefaultValue(special);

        String currentValue = this.special.getValue();

        fireSpecialChange(currentValue, oldValue);
    }
    
    public final String removeDefaultSpecial()
    {
        String oldValue = this.special.getValue();
        String removedValue = this.special.removeDefaultValue();
        String currentValue = this.special.getValue();

        fireSpecialChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getSpecialValueContainer()
    {
        return this.special;
    }
    public final void fireSpecialChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeSpecialChanged(currentValue);
            firePropertyChange(PROPERTYNAME_SPECIAL, oldValue, currentValue);
            afterSpecialChanged(currentValue);
        }
    }

    public void beforeSpecialChanged( String special)
    { }
    public void afterSpecialChanged( String special)
    { }


}
