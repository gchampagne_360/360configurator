// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.DynamicEnum;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

@DynamicEnum(fileName = "")
public abstract class VitrageBase extends com.netappsid.erp.configurator.Configurable
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(VitrageBase.class);

    // attributes
    private ValueContainer<String> description = new ValueContainer<String>(this);
    private ValueContainer<String> glassThickness = new ValueContainer<String>(this);
    private ValueContainer<String> glassType = new ValueContainer<String>(this);
    private ValueContainer<String> spacer = new ValueContainer<String>(this);
    private ValueContainer<String> type = new ValueContainer<String>(this);

    // Bound properties

    public static final String PROPERTYNAME_DESCRIPTION = "description";  
    public static final String PROPERTYNAME_GLASSTHICKNESS = "glassThickness";  
    public static final String PROPERTYNAME_GLASSTYPE = "glassType";  
    public static final String PROPERTYNAME_SPACER = "spacer";  
    public static final String PROPERTYNAME_TYPE = "type";  

    // Constructors

    public VitrageBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.Vitrage"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
    })
    public final String getDescription()
    {
         return this.description.getValue();
    }

    public final void setDescription(String description)
    {

        String oldValue = this.description.getValue();
        this.description.setValue(description);

        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
    }
    public final String removeDescription()
    {
        String oldValue = this.description.getValue();
        String removedValue = this.description.removeValue();
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultDescription(String description)
    {
        String oldValue = this.description.getValue();
        this.description.setDefaultValue(description);

        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
    }
    
    public final String removeDefaultDescription()
    {
        String oldValue = this.description.getValue();
        String removedValue = this.description.removeDefaultValue();
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getDescriptionValueContainer()
    {
        return this.description;
    }
    public final void fireDescriptionChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeDescriptionChanged(currentValue);
            firePropertyChange(PROPERTYNAME_DESCRIPTION, oldValue, currentValue);
            afterDescriptionChanged(currentValue);
        }
    }

    public void beforeDescriptionChanged( String description)
    { }
    public void afterDescriptionChanged( String description)
    { }

    @LocalizedTags
    ({
    })
    public final String getGlassThickness()
    {
         return this.glassThickness.getValue();
    }

    public final void setGlassThickness(String glassThickness)
    {

        String oldValue = this.glassThickness.getValue();
        this.glassThickness.setValue(glassThickness);

        String currentValue = this.glassThickness.getValue();

        fireGlassThicknessChange(currentValue, oldValue);
    }
    public final String removeGlassThickness()
    {
        String oldValue = this.glassThickness.getValue();
        String removedValue = this.glassThickness.removeValue();
        String currentValue = this.glassThickness.getValue();

        fireGlassThicknessChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultGlassThickness(String glassThickness)
    {
        String oldValue = this.glassThickness.getValue();
        this.glassThickness.setDefaultValue(glassThickness);

        String currentValue = this.glassThickness.getValue();

        fireGlassThicknessChange(currentValue, oldValue);
    }
    
    public final String removeDefaultGlassThickness()
    {
        String oldValue = this.glassThickness.getValue();
        String removedValue = this.glassThickness.removeDefaultValue();
        String currentValue = this.glassThickness.getValue();

        fireGlassThicknessChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getGlassThicknessValueContainer()
    {
        return this.glassThickness;
    }
    public final void fireGlassThicknessChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeGlassThicknessChanged(currentValue);
            firePropertyChange(PROPERTYNAME_GLASSTHICKNESS, oldValue, currentValue);
            afterGlassThicknessChanged(currentValue);
        }
    }

    public void beforeGlassThicknessChanged( String glassThickness)
    { }
    public void afterGlassThicknessChanged( String glassThickness)
    { }

    @LocalizedTags
    ({
    })
    public final String getGlassType()
    {
         return this.glassType.getValue();
    }

    public final void setGlassType(String glassType)
    {

        String oldValue = this.glassType.getValue();
        this.glassType.setValue(glassType);

        String currentValue = this.glassType.getValue();

        fireGlassTypeChange(currentValue, oldValue);
    }
    public final String removeGlassType()
    {
        String oldValue = this.glassType.getValue();
        String removedValue = this.glassType.removeValue();
        String currentValue = this.glassType.getValue();

        fireGlassTypeChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultGlassType(String glassType)
    {
        String oldValue = this.glassType.getValue();
        this.glassType.setDefaultValue(glassType);

        String currentValue = this.glassType.getValue();

        fireGlassTypeChange(currentValue, oldValue);
    }
    
    public final String removeDefaultGlassType()
    {
        String oldValue = this.glassType.getValue();
        String removedValue = this.glassType.removeDefaultValue();
        String currentValue = this.glassType.getValue();

        fireGlassTypeChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getGlassTypeValueContainer()
    {
        return this.glassType;
    }
    public final void fireGlassTypeChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeGlassTypeChanged(currentValue);
            firePropertyChange(PROPERTYNAME_GLASSTYPE, oldValue, currentValue);
            afterGlassTypeChanged(currentValue);
        }
    }

    public void beforeGlassTypeChanged( String glassType)
    { }
    public void afterGlassTypeChanged( String glassType)
    { }

    @LocalizedTags
    ({
    })
    public final String getSpacer()
    {
         return this.spacer.getValue();
    }

    public final void setSpacer(String spacer)
    {

        String oldValue = this.spacer.getValue();
        this.spacer.setValue(spacer);

        String currentValue = this.spacer.getValue();

        fireSpacerChange(currentValue, oldValue);
    }
    public final String removeSpacer()
    {
        String oldValue = this.spacer.getValue();
        String removedValue = this.spacer.removeValue();
        String currentValue = this.spacer.getValue();

        fireSpacerChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultSpacer(String spacer)
    {
        String oldValue = this.spacer.getValue();
        this.spacer.setDefaultValue(spacer);

        String currentValue = this.spacer.getValue();

        fireSpacerChange(currentValue, oldValue);
    }
    
    public final String removeDefaultSpacer()
    {
        String oldValue = this.spacer.getValue();
        String removedValue = this.spacer.removeDefaultValue();
        String currentValue = this.spacer.getValue();

        fireSpacerChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getSpacerValueContainer()
    {
        return this.spacer;
    }
    public final void fireSpacerChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeSpacerChanged(currentValue);
            firePropertyChange(PROPERTYNAME_SPACER, oldValue, currentValue);
            afterSpacerChanged(currentValue);
        }
    }

    public void beforeSpacerChanged( String spacer)
    { }
    public void afterSpacerChanged( String spacer)
    { }

    @LocalizedTags
    ({
    })
    public final String getType()
    {
         return this.type.getValue();
    }

    public final void setType(String type)
    {

        String oldValue = this.type.getValue();
        this.type.setValue(type);

        String currentValue = this.type.getValue();

        fireTypeChange(currentValue, oldValue);
    }
    public final String removeType()
    {
        String oldValue = this.type.getValue();
        String removedValue = this.type.removeValue();
        String currentValue = this.type.getValue();

        fireTypeChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultType(String type)
    {
        String oldValue = this.type.getValue();
        this.type.setDefaultValue(type);

        String currentValue = this.type.getValue();

        fireTypeChange(currentValue, oldValue);
    }
    
    public final String removeDefaultType()
    {
        String oldValue = this.type.getValue();
        String removedValue = this.type.removeDefaultValue();
        String currentValue = this.type.getValue();

        fireTypeChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getTypeValueContainer()
    {
        return this.type;
    }
    public final void fireTypeChange(String currentValue, String oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeTypeChanged(currentValue);
            firePropertyChange(PROPERTYNAME_TYPE, oldValue, currentValue);
            afterTypeChanged(currentValue);
        }
    }

    public void beforeTypeChanged( String type)
    { }
    public void afterTypeChanged( String type)
    { }


}
