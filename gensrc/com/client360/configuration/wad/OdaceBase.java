// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V14.0.1

// Imports 


public abstract class OdaceBase extends com.client360.configuration.wad.CasementMain
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(OdaceBase.class);

    // attributes

    // Bound properties


    // Constructors

    public OdaceBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.Odace"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations



    @Override
    protected String getSequences()
    {
        return "energyStar,noFrame,frameDepth,installed,nbSections,qDQ,sameGrilles,showMeetingRail,showMullion,profiles,InteriorColor,ExteriorColor,windowsMullion,advancedSectionGrilles,sash,paintable,renderingNumber,dimension,sectionInput,section,choicePresetWindowWidth,division,meetingRail,option";  
    }
}
