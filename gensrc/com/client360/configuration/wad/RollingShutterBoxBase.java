// generated class, do not edit

package com.client360.configuration.wad;

//Plugin V13.1.8
//2014-07-01

// Imports 
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.AssociatedComment;
import com.netappsid.erp.configurator.annotations.AssociatedDrawing;
import com.netappsid.erp.configurator.annotations.GlobalProperty;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;
import com.netappsid.erp.configurator.annotations.Printable;
import com.netappsid.erp.configurator.annotations.Visible;

public abstract class RollingShutterBoxBase extends com.netappsid.wadconfigurator.RollingShutterBox
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(RollingShutterBoxBase.class);

    // attributes
    //GENCODE 3703fd90-c29c-11e0-962b-0800200c9a66 Values changed to ValueContainerwith generic inner type
    private ValueContainer<Boolean> displayMasonryBox = new ValueContainer<Boolean>(this);

    // Constructors

    public RollingShutterBoxBase(com.netappsid.erp.configurator.Configurable parent) // GENCODE b298fff0-4bff-11e0-b8af-0800200c9a66
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.wad.RollingShutterBox"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    public final void setDisplayMasonryBox(Boolean displayMasonryBox)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        Boolean oldValue = this.displayMasonryBox.getValue();
        this.displayMasonryBox.setValue(displayMasonryBox);
        Boolean currentValue = this.displayMasonryBox.getValue();

        fireDisplayMasonryBoxChange(currentValue, oldValue);
    }
    public final Boolean removeDisplayMasonryBox()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        Boolean oldValue = this.displayMasonryBox.getValue();
        Boolean removedValue = this.displayMasonryBox.removeValue();
        Boolean currentValue = this.displayMasonryBox.getValue();

        fireDisplayMasonryBoxChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultDisplayMasonryBox(Boolean displayMasonryBox)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        Boolean oldValue = this.displayMasonryBox.getValue();
        this.displayMasonryBox.setDefaultValue(displayMasonryBox);
        Boolean currentValue = this.displayMasonryBox.getValue();

        fireDisplayMasonryBoxChange(currentValue, oldValue);
    }
    
    public final Boolean removeDefaultDisplayMasonryBox()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        Boolean oldValue = this.displayMasonryBox.getValue();
        Boolean removedValue = this.displayMasonryBox.removeDefaultValue();
        Boolean currentValue = this.displayMasonryBox.getValue();

        fireDisplayMasonryBoxChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Boolean> getDisplayMasonryBoxValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.displayMasonryBox;
    }
    public final void fireDisplayMasonryBoxChange(Boolean currentValue, Boolean oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeDisplayMasonryBoxChanged(currentValue);
            firePropertyChange(PROPERTYNAME_DISPLAYMASONRYBOX, oldValue, currentValue);
            afterDisplayMasonryBoxChanged(currentValue);
        }
    }

    public void beforeDisplayMasonryBoxChanged( Boolean displayMasonryBox)
    { }
    public void afterDisplayMasonryBoxChanged( Boolean displayMasonryBox)
    { }

    public boolean isDisplayMasonryBoxVisible()
    { return true; }

    public boolean isDisplayMasonryBoxReadOnly()
    { return false; }

    public boolean isDisplayMasonryBoxEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getDisplayMasonryBoxPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Display masonry box")
        ,@LocalizedTag(language="fr", name="label", value="Afficher coffre ma�onn�")
    })
    @GlobalProperty("false")
    @Printable("true")
    @Visible("true")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final Boolean getDisplayMasonryBox()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.displayMasonryBox.getValue();
    }



    // Business methods 



    @Override
    public void setDefaultValues()
    { 
        super.setDefaultValues();
    }

    @Override
    protected String getSequences()
    {
        return "height,interiorColor,exteriorColor,zone,displayMasonryBox,masonryBox,masonryBoxOffsetTop,masonryBoxOffsetLeft,masonryBoxOffsetRight";  
    }

    // Bound properties

    public static final String PROPERTYNAME_DISPLAYMASONRYBOX = "displayMasonryBox";  
}
