// generated class, do not edit

package com.client360.configuration.blinds;

//Plugin V13.1.2
//2013-12-09

// Imports 
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.AssociatedComment;
import com.netappsid.erp.configurator.annotations.AssociatedDrawing;
import com.netappsid.erp.configurator.annotations.LocalizedTags;
import com.netappsid.erp.configurator.annotations.Visible;

public abstract class ColorBase extends com.netappsid.configuration.common.Color
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ColorBase.class);

    // attributes
    //GENCODE 3703fd90-c29c-11e0-962b-0800200c9a66 Values changed to ValueContainerwith generic inner type
    private ValueContainer<String> texturePath = new ValueContainer<String>(this);

    // Constructors

    public ColorBase(com.netappsid.erp.configurator.Configurable parent) // GENCODE b298fff0-4bff-11e0-b8af-0800200c9a66
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.blinds.Color"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    public final void setTexturePath(String texturePath)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.texturePath.getValue();
        this.texturePath.setValue(texturePath);
        String currentValue = this.texturePath.getValue();

        fireTexturePathChange(currentValue, oldValue);
    }
    public final String removeTexturePath()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.texturePath.getValue();
        String removedValue = this.texturePath.removeValue();
        String currentValue = this.texturePath.getValue();

        fireTexturePathChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultTexturePath(String texturePath)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.texturePath.getValue();
        this.texturePath.setDefaultValue(texturePath);
        String currentValue = this.texturePath.getValue();

        fireTexturePathChange(currentValue, oldValue);
    }
    
    public final String removeDefaultTexturePath()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.texturePath.getValue();
        String removedValue = this.texturePath.removeDefaultValue();
        String currentValue = this.texturePath.getValue();

        fireTexturePathChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getTexturePathValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.texturePath;
    }
    public final void fireTexturePathChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeTexturePathChanged(currentValue);
            firePropertyChange(PROPERTYNAME_TEXTUREPATH, oldValue, currentValue);
            afterTexturePathChanged(currentValue);
        }
    }

    public void beforeTexturePathChanged( String texturePath)
    { }
    public void afterTexturePathChanged( String texturePath)
    { }

    public boolean isTexturePathVisible()
    { return true; }

    public boolean isTexturePathReadOnly()
    { return false; }

    public boolean isTexturePathEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getTexturePathPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
    })
    @Visible("true")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getTexturePath()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.texturePath.getValue();
    }



    // Business methods 



    @Override
    public void setDefaultValues()
    { 
        super.setDefaultValues();
    }

    // Bound properties

    public static final String PROPERTYNAME_TEXTUREPATH = "texturePath";  
}
