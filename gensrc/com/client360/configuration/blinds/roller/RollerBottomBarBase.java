// generated class, do not edit

package com.client360.configuration.blinds.roller;

//Plugin V13.1.2
//2013-12-17

// Imports 
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.AssociatedComment;
import com.netappsid.erp.configurator.annotations.AssociatedDrawing;
import com.netappsid.erp.configurator.annotations.GlobalProperty;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;
import com.netappsid.erp.configurator.annotations.Printable;
import com.netappsid.erp.configurator.annotations.Visible;

public abstract class RollerBottomBarBase extends com.netappsid.configuration.blinds.roller.RollerBottomBar
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(RollerBottomBarBase.class);

    // attributes
    //GENCODE 3703fd90-c29c-11e0-962b-0800200c9a66 Values changed to ValueContainerwith generic inner type
    private ValueContainer<Boolean> hasTips = new ValueContainer<Boolean>(this);
    private ValueContainer<Boolean> hasMantling = new ValueContainer<Boolean>(this);

    // Constructors

    public RollerBottomBarBase(com.netappsid.erp.configurator.Configurable parent) // GENCODE b298fff0-4bff-11e0-b8af-0800200c9a66
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.blinds.roller.RollerBottomBar"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    public final void setHasTips(Boolean hasTips)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        Boolean oldValue = this.hasTips.getValue();
        this.hasTips.setValue(hasTips);
        Boolean currentValue = this.hasTips.getValue();

        fireHasTipsChange(currentValue, oldValue);
    }
    public final Boolean removeHasTips()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        Boolean oldValue = this.hasTips.getValue();
        Boolean removedValue = this.hasTips.removeValue();
        Boolean currentValue = this.hasTips.getValue();

        fireHasTipsChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultHasTips(Boolean hasTips)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        Boolean oldValue = this.hasTips.getValue();
        this.hasTips.setDefaultValue(hasTips);
        Boolean currentValue = this.hasTips.getValue();

        fireHasTipsChange(currentValue, oldValue);
    }
    
    public final Boolean removeDefaultHasTips()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        Boolean oldValue = this.hasTips.getValue();
        Boolean removedValue = this.hasTips.removeDefaultValue();
        Boolean currentValue = this.hasTips.getValue();

        fireHasTipsChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Boolean> getHasTipsValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.hasTips;
    }
    public final void fireHasTipsChange(Boolean currentValue, Boolean oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeHasTipsChanged(currentValue);
            firePropertyChange(PROPERTYNAME_HASTIPS, oldValue, currentValue);
            afterHasTipsChanged(currentValue);
        }
    }

    public void beforeHasTipsChanged( Boolean hasTips)
    { }
    public void afterHasTipsChanged( Boolean hasTips)
    { }

    public boolean isHasTipsVisible()
    { return true; }

    public boolean isHasTipsReadOnly()
    { return false; }

    public boolean isHasTipsEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getHasTipsPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Decorative tips")
        ,@LocalizedTag(language="fr", name="label", value="Embouts décoratifs")
    })
    @GlobalProperty("false")
    @Printable("true")
    @Visible("true")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final Boolean getHasTips()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.hasTips.getValue();
    }


    public final void setHasMantling(Boolean hasMantling)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        Boolean oldValue = this.hasMantling.getValue();
        this.hasMantling.setValue(hasMantling);
        Boolean currentValue = this.hasMantling.getValue();

        fireHasMantlingChange(currentValue, oldValue);
    }
    public final Boolean removeHasMantling()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        Boolean oldValue = this.hasMantling.getValue();
        Boolean removedValue = this.hasMantling.removeValue();
        Boolean currentValue = this.hasMantling.getValue();

        fireHasMantlingChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultHasMantling(Boolean hasMantling)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        Boolean oldValue = this.hasMantling.getValue();
        this.hasMantling.setDefaultValue(hasMantling);
        Boolean currentValue = this.hasMantling.getValue();

        fireHasMantlingChange(currentValue, oldValue);
    }
    
    public final Boolean removeDefaultHasMantling()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        Boolean oldValue = this.hasMantling.getValue();
        Boolean removedValue = this.hasMantling.removeDefaultValue();
        Boolean currentValue = this.hasMantling.getValue();

        fireHasMantlingChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Boolean> getHasMantlingValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.hasMantling;
    }
    public final void fireHasMantlingChange(Boolean currentValue, Boolean oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeHasMantlingChanged(currentValue);
            firePropertyChange(PROPERTYNAME_HASMANTLING, oldValue, currentValue);
            afterHasMantlingChanged(currentValue);
        }
    }

    public void beforeHasMantlingChanged( Boolean hasMantling)
    { }
    public void afterHasMantlingChanged( Boolean hasMantling)
    { }

    public boolean isHasMantlingVisible()
    { return true; }

    public boolean isHasMantlingReadOnly()
    { return false; }

    public boolean isHasMantlingEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getHasMantlingPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Mantling")
        ,@LocalizedTag(language="fr", name="label", value="Lambrequin")
    })
    @GlobalProperty("false")
    @Printable("true")
    @Visible("true")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final Boolean getHasMantling()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.hasMantling.getValue();
    }



    // Business methods 



    @Override
    public void setDefaultValues()
    { 
        super.setDefaultValues();
    }

    @Override
    protected String getSequences()
    {
        return "profile,height,color,leftOffset,rightOffset,hasTips,hasMantling,mantling,sewnIns,form,not set,leftTipDisplay";  
    }

    // Bound properties

    public static final String PROPERTYNAME_HASTIPS = "hasTips";  
    public static final String PROPERTYNAME_HASMANTLING = "hasMantling";  
}
