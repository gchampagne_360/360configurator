// generated class, do not edit

package com.client360.configuration.blinds.roller;

//Plugin V13.1.2
//2013-12-06

// Imports 

public abstract class RollerBoxBase extends com.netappsid.configuration.blinds.Box
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(RollerBoxBase.class);

    // attributes

    // Constructors

    public RollerBoxBase(com.netappsid.erp.configurator.Configurable parent) // GENCODE b298fff0-4bff-11e0-b8af-0800200c9a66
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.blinds.roller.RollerBox"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations


    // Business methods 



    @Override
    public void setDefaultValues()
    { 
        super.setDefaultValues();
    }

    @Override
    protected String getSequences()
    {
        return "choixFormeCoffre,color,couleurCoffre,profile,height,leftOverflow,rightOverflow,dimension";  
    }

    // Bound properties

}
