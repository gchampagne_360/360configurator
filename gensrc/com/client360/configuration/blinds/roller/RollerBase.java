// generated class, do not edit

package com.client360.configuration.blinds.roller;

//Plugin V13.1.2
//2013-12-31

// Imports 
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.AssociatedComment;
import com.netappsid.erp.configurator.annotations.AssociatedDrawing;
import com.netappsid.erp.configurator.annotations.GlobalProperty;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;
import com.netappsid.erp.configurator.annotations.Mandatory;
import com.netappsid.erp.configurator.annotations.Printable;
import com.netappsid.erp.configurator.annotations.Visible;

public abstract class RollerBase extends com.netappsid.configuration.blinds.roller.RollerBlind
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(RollerBase.class);

    // attributes
    //GENCODE 3703fd90-c29c-11e0-962b-0800200c9a66 Values changed to ValueContainerwith generic inner type
    private ValueContainer<Boolean> hasBox = new ValueContainer<Boolean>(this);
    private ValueContainer<com.client360.configuration.blinds.roller.colors.RollerColor> bracketsColor = new ValueContainer<com.client360.configuration.blinds.roller.colors.RollerColor>(this);
    private ValueContainer<com.client360.configuration.blinds.enums.ChoiceGuide> choiceGuide = new ValueContainer<com.client360.configuration.blinds.enums.ChoiceGuide>(this);

    // Constructors

    public RollerBase(com.netappsid.erp.configurator.Configurable parent) // GENCODE b298fff0-4bff-11e0-b8af-0800200c9a66
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.blinds.roller.Roller"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    public final void setHasBox(Boolean hasBox)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        Boolean oldValue = this.hasBox.getValue();
        this.hasBox.setValue(hasBox);
        Boolean currentValue = this.hasBox.getValue();

        fireHasBoxChange(currentValue, oldValue);
    }
    public final Boolean removeHasBox()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        Boolean oldValue = this.hasBox.getValue();
        Boolean removedValue = this.hasBox.removeValue();
        Boolean currentValue = this.hasBox.getValue();

        fireHasBoxChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultHasBox(Boolean hasBox)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        Boolean oldValue = this.hasBox.getValue();
        this.hasBox.setDefaultValue(hasBox);
        Boolean currentValue = this.hasBox.getValue();

        fireHasBoxChange(currentValue, oldValue);
    }
    
    public final Boolean removeDefaultHasBox()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        Boolean oldValue = this.hasBox.getValue();
        Boolean removedValue = this.hasBox.removeDefaultValue();
        Boolean currentValue = this.hasBox.getValue();

        fireHasBoxChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Boolean> getHasBoxValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.hasBox;
    }
    public final void fireHasBoxChange(Boolean currentValue, Boolean oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeHasBoxChanged(currentValue);
            firePropertyChange(PROPERTYNAME_HASBOX, oldValue, currentValue);
            afterHasBoxChanged(currentValue);
        }
    }

    public void beforeHasBoxChanged( Boolean hasBox)
    { }
    public void afterHasBoxChanged( Boolean hasBox)
    { }

    public boolean isHasBoxVisible()
    { return true; }

    public boolean isHasBoxReadOnly()
    { return false; }

    public boolean isHasBoxEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getHasBoxPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Box")
        ,@LocalizedTag(language="fr", name="label", value="Bo�tier")
    })
    @GlobalProperty("false")
    @Printable("true")
    @Visible("true")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final Boolean getHasBox()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.hasBox.getValue();
    }


    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Brackets color")
        ,@LocalizedTag(language="fr", name="label", value="Couleur des supports")
    })
    @Mandatory("true")
    @GlobalProperty("false")
    @Printable("true")
    @Visible("true")
    @AssociatedComment("false")
    @AssociatedDrawing("false")

    public final com.client360.configuration.blinds.roller.colors.RollerColor getBracketsColor()
    {   //GENCODE e76ad2a0-c2bd-11e0-962b-0800200c9a66 Value Container getter operations
        return bracketsColor.getValue();
    }


    public final void setBracketsColor(com.client360.configuration.blinds.roller.colors.RollerColor bracketsColor)
    {   // GENCODE 9cda9e30-c2bf-11e0-962b-0800200c9a66 SETTER

        com.client360.configuration.blinds.roller.colors.RollerColor oldValue = this.bracketsColor.getValue();
        this.bracketsColor.setValue(bracketsColor);

        com.client360.configuration.blinds.roller.colors.RollerColor currentValue = this.bracketsColor.getValue();

         fireBracketsColorChange(currentValue , oldValue);
    }

    public final com.client360.configuration.blinds.roller.colors.RollerColor removeBracketsColor()
    {   // GENCODE 07d769f0-c376-11e0-962b-0800200c9a66 DEFAULT REMOVE
        
        com.client360.configuration.blinds.roller.colors.RollerColor oldValue = this.bracketsColor.getValue();
        com.client360.configuration.blinds.roller.colors.RollerColor removedValue = this.bracketsColor.removeValue();
        com.client360.configuration.blinds.roller.colors.RollerColor currentValue = this.bracketsColor.getValue();

        if(removedValue != oldValue)
             dispose(removedValue);

        fireBracketsColorChange(currentValue, oldValue);
        return removedValue;
    }

    public final void setDefaultBracketsColor(com.client360.configuration.blinds.roller.colors.RollerColor bracketsColor)
    {   // GENCODE 07d769f1-c376-11e0-962b-0800200c9a66 DEFAULT SETTER
        com.client360.configuration.blinds.roller.colors.RollerColor oldValue = this.bracketsColor.getValue();
        this.bracketsColor.setDefaultValue(bracketsColor);
        com.client360.configuration.blinds.roller.colors.RollerColor currentValue = this.bracketsColor.getValue();

         fireBracketsColorChange(currentValue, oldValue);
    }
    
    public final com.client360.configuration.blinds.roller.colors.RollerColor removeDefaultBracketsColor()
    {   // GENCODE 07d769f2-c376-11e0-962b-0800200c9a66 DEFAULT REMOVE
        
        com.client360.configuration.blinds.roller.colors.RollerColor oldValue = this.bracketsColor.getValue();
        com.client360.configuration.blinds.roller.colors.RollerColor removedValue = this.bracketsColor.removeDefaultValue();
        com.client360.configuration.blinds.roller.colors.RollerColor currentValue = this.bracketsColor.getValue();

        if(removedValue != oldValue)
             dispose(removedValue);

        fireBracketsColorChange(currentValue, oldValue);
        return removedValue;
    }

    public final ValueContainerGetter<com.client360.configuration.blinds.roller.colors.RollerColor> getBracketsColorValueContainer()
    {   //GENCODE 5200e500-c2be-11e0-962b-0800200c9a66 ValueContainer GETTER
        return bracketsColor;
    }

    public final void fireBracketsColorChange(com.client360.configuration.blinds.roller.colors.RollerColor currentValue, com.client360.configuration.blinds.roller.colors.RollerColor oldValue)
    {   //GENCODE 209a8340-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
                if (currentValue != null)
                {
                    //It's a configurable, set the parent and role
                    acquire(currentValue, "bracketsColor");
                }
            beforeBracketsColorChanged(currentValue);
            firePropertyChange(PROPERTYNAME_BRACKETSCOLOR, oldValue, currentValue);
            afterBracketsColorChanged(currentValue);
        }
    }
 
    public void pushBracketsColor()
    { }
 
    public void beforeBracketsColorChanged( com.client360.configuration.blinds.roller.colors.RollerColor bracketsColor)
    { }
    public void afterBracketsColorChanged( com.client360.configuration.blinds.roller.colors.RollerColor bracketsColor)
    { }
 
    public boolean isBracketsColorVisible()
    { return true; }

    public boolean isBracketsColorReadOnly()
    { return false; }

    public double getBracketsColorPrice()
    { return 0d; }

    public boolean isBracketsColorEnabled()
    { return true; /* GENCODE bdccd463-4c1c-4ba1-aece-55f4c7cfeae7 */ }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Guide")
        ,@LocalizedTag(language="fr", name="label", value="Guidage")
        ,@LocalizedTag(language="fr", name="null", value="Aucun")
        ,@LocalizedTag(language="en", name="null", value="None")
    })
    @Mandatory("")
    @GlobalProperty("false")
    @Printable("true")
    @Visible("true")
    @AssociatedComment("false")
    @AssociatedDrawing("false")

    public final com.client360.configuration.blinds.enums.ChoiceGuide getChoiceGuide()
    {   //GENCODE e76ad2a0-c2bd-11e0-962b-0800200c9a66 Value Container getter operations
        return choiceGuide.getValue();
    }


    public final void setChoiceGuide(com.client360.configuration.blinds.enums.ChoiceGuide choiceGuide)
    {   // GENCODE 9cda9e30-c2bf-11e0-962b-0800200c9a66 SETTER

        com.client360.configuration.blinds.enums.ChoiceGuide oldValue = this.choiceGuide.getValue();
        this.choiceGuide.setValue(choiceGuide);

        com.client360.configuration.blinds.enums.ChoiceGuide currentValue = this.choiceGuide.getValue();

         fireChoiceGuideChange(currentValue , oldValue);
    }

    public final com.client360.configuration.blinds.enums.ChoiceGuide removeChoiceGuide()
    {   // GENCODE 07d769f0-c376-11e0-962b-0800200c9a66 DEFAULT REMOVE
        
        com.client360.configuration.blinds.enums.ChoiceGuide oldValue = this.choiceGuide.getValue();
        com.client360.configuration.blinds.enums.ChoiceGuide removedValue = this.choiceGuide.removeValue();
        com.client360.configuration.blinds.enums.ChoiceGuide currentValue = this.choiceGuide.getValue();

        if(removedValue != oldValue)
             dispose(removedValue);

        fireChoiceGuideChange(currentValue, oldValue);
        return removedValue;
    }

    public final void setDefaultChoiceGuide(com.client360.configuration.blinds.enums.ChoiceGuide choiceGuide)
    {   // GENCODE 07d769f1-c376-11e0-962b-0800200c9a66 DEFAULT SETTER
        com.client360.configuration.blinds.enums.ChoiceGuide oldValue = this.choiceGuide.getValue();
        this.choiceGuide.setDefaultValue(choiceGuide);
        com.client360.configuration.blinds.enums.ChoiceGuide currentValue = this.choiceGuide.getValue();

         fireChoiceGuideChange(currentValue, oldValue);
    }
    
    public final com.client360.configuration.blinds.enums.ChoiceGuide removeDefaultChoiceGuide()
    {   // GENCODE 07d769f2-c376-11e0-962b-0800200c9a66 DEFAULT REMOVE
        
        com.client360.configuration.blinds.enums.ChoiceGuide oldValue = this.choiceGuide.getValue();
        com.client360.configuration.blinds.enums.ChoiceGuide removedValue = this.choiceGuide.removeDefaultValue();
        com.client360.configuration.blinds.enums.ChoiceGuide currentValue = this.choiceGuide.getValue();

        if(removedValue != oldValue)
             dispose(removedValue);

        fireChoiceGuideChange(currentValue, oldValue);
        return removedValue;
    }

    public final ValueContainerGetter<com.client360.configuration.blinds.enums.ChoiceGuide> getChoiceGuideValueContainer()
    {   //GENCODE 5200e500-c2be-11e0-962b-0800200c9a66 ValueContainer GETTER
        return choiceGuide;
    }

    public final void fireChoiceGuideChange(com.client360.configuration.blinds.enums.ChoiceGuide currentValue, com.client360.configuration.blinds.enums.ChoiceGuide oldValue)
    {   //GENCODE 209a8340-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoiceGuideChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOICEGUIDE, oldValue, currentValue);
            afterChoiceGuideChanged(currentValue);
        }
    }
 
    public void pushChoiceGuide()
    { }
 
    public void beforeChoiceGuideChanged( com.client360.configuration.blinds.enums.ChoiceGuide choiceGuide)
    { }
    public void afterChoiceGuideChanged( com.client360.configuration.blinds.enums.ChoiceGuide choiceGuide)
    { }
 
    public boolean isChoiceGuideVisible()
    { return true; }

    public boolean isChoiceGuideReadOnly()
    { return false; }

    public double getChoiceGuidePrice()
    { return 0d; }

    public boolean isChoiceGuideEnabled()
    { return true; /* GENCODE bdccd463-4c1c-4ba1-aece-55f4c7cfeae7 */ }

    public boolean isChoiceGuideEnabled(com.client360.configuration.blinds.enums.ChoiceGuide choice)
    { return true; }


    // Business methods 



    public boolean isBracketsColorMandatory()
    {
        return true;
    }

    public boolean isChoiceGuideMandatory()
    {
        return false;
    }

    @Override
    public void setDefaultValues()
    { 
        super.setDefaultValues();
    }

    @Override
    protected String getSequences()
    {
        return "choiceEntryMode,dimension,hasBox,box,bracketsColor,nbRolls,roll,choiceGuide,guide,renderingNumber,paintable,leftBracketDisplay,rightBracketDisplay,customVisualCompositionsguide,couleurMecanisme,profilReception,couleurProfilReception,choixCalePoignee,renderingNumber,paintable,leftBracketDisplay,not set,rightBracketDisplay,tissu";  
    }

    // Bound properties

    public static final String PROPERTYNAME_HASBOX = "hasBox";  
    public static final String PROPERTYNAME_BRACKETSCOLOR = "bracketsColor";  
    public static final String PROPERTYNAME_CHOICEGUIDE = "choiceGuide";  
}
