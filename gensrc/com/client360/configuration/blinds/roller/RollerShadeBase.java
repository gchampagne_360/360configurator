// generated class, do not edit

package com.client360.configuration.blinds.roller;

//Plugin V13.1.2
//2013-12-17

// Imports 
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.AssociatedComment;
import com.netappsid.erp.configurator.annotations.AssociatedDrawing;
import com.netappsid.erp.configurator.annotations.GlobalProperty;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;
import com.netappsid.erp.configurator.annotations.Printable;
import com.netappsid.erp.configurator.annotations.Visible;

public abstract class RollerShadeBase extends com.netappsid.configuration.blinds.roller.RollerShade
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(RollerShadeBase.class);

    // attributes
    //GENCODE 3703fd90-c29c-11e0-962b-0800200c9a66 Values changed to ValueContainerwith generic inner type
    private ValueContainer<Boolean> hasBottomBar = new ValueContainer<Boolean>(this);

    // Constructors

    public RollerShadeBase(com.netappsid.erp.configurator.Configurable parent) // GENCODE b298fff0-4bff-11e0-b8af-0800200c9a66
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.blinds.roller.RollerShade"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    public final void setHasBottomBar(Boolean hasBottomBar)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        Boolean oldValue = this.hasBottomBar.getValue();
        this.hasBottomBar.setValue(hasBottomBar);
        Boolean currentValue = this.hasBottomBar.getValue();

        fireHasBottomBarChange(currentValue, oldValue);
    }
    public final Boolean removeHasBottomBar()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        Boolean oldValue = this.hasBottomBar.getValue();
        Boolean removedValue = this.hasBottomBar.removeValue();
        Boolean currentValue = this.hasBottomBar.getValue();

        fireHasBottomBarChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultHasBottomBar(Boolean hasBottomBar)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        Boolean oldValue = this.hasBottomBar.getValue();
        this.hasBottomBar.setDefaultValue(hasBottomBar);
        Boolean currentValue = this.hasBottomBar.getValue();

        fireHasBottomBarChange(currentValue, oldValue);
    }
    
    public final Boolean removeDefaultHasBottomBar()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        Boolean oldValue = this.hasBottomBar.getValue();
        Boolean removedValue = this.hasBottomBar.removeDefaultValue();
        Boolean currentValue = this.hasBottomBar.getValue();

        fireHasBottomBarChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Boolean> getHasBottomBarValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.hasBottomBar;
    }
    public final void fireHasBottomBarChange(Boolean currentValue, Boolean oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeHasBottomBarChanged(currentValue);
            firePropertyChange(PROPERTYNAME_HASBOTTOMBAR, oldValue, currentValue);
            afterHasBottomBarChanged(currentValue);
        }
    }

    public void beforeHasBottomBarChanged( Boolean hasBottomBar)
    { }
    public void afterHasBottomBarChanged( Boolean hasBottomBar)
    { }

    public boolean isHasBottomBarVisible()
    { return true; }

    public boolean isHasBottomBarReadOnly()
    { return false; }

    public boolean isHasBottomBarEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getHasBottomBarPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Bottom bar")
        ,@LocalizedTag(language="fr", name="label", value="Barre de charge")
    })
    @GlobalProperty("false")
    @Printable("true")
    @Visible("true")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final Boolean getHasBottomBar()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.hasBottomBar.getValue();
    }



    // Business methods 



    @Override
    public void setDefaultValues()
    { 
        super.setDefaultValues();
    }

    @Override
    protected String getSequences()
    {
        return "dimension,diameter,leftOffset,rightOffset,opening,color,hasBottomBar,bottomBar";  
    }

    // Bound properties

    public static final String PROPERTYNAME_HASBOTTOMBAR = "hasBottomBar";  
}
