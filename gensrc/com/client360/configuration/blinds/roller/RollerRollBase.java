// generated class, do not edit

package com.client360.configuration.blinds.roller;

//Plugin V13.1.2
//2013-12-06

// Imports 

public abstract class RollerRollBase extends com.netappsid.configuration.blinds.roller.RollerRoll
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(RollerRollBase.class);

    // attributes

    // Constructors

    public RollerRollBase(com.netappsid.erp.configurator.Configurable parent) // GENCODE b298fff0-4bff-11e0-b8af-0800200c9a66
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.blinds.roller.RollerRoll"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations


    // Business methods 



    @Override
    public void setDefaultValues()
    { 
        super.setDefaultValues();
    }

    @Override
    protected String getSequences()
    {
        return "choixSensEnroulement,exteriorRollup,diameter,shade,operatorDisplay,leftOffset,rightOffset,form,not set,enrConfManoeuvre,mecanismeChainette,axisY";  
    }

    // Bound properties

}
