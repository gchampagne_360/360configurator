// generated class, do not edit

package com.client360.configuration.blinds.roller.colors;

//Plugin V13.1.2
//2013-12-06

// Imports 
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.AssociatedComment;
import com.netappsid.erp.configurator.annotations.AssociatedDrawing;
import com.netappsid.erp.configurator.annotations.DynamicEnum;
import com.netappsid.erp.configurator.annotations.Filtrable;
import com.netappsid.erp.configurator.annotations.Image;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;
import com.netappsid.erp.configurator.annotations.Visible;

@DynamicEnum(fileName = "ENR_couleurs")
@Image("7")
public abstract class RollerChoiceColorBase extends com.netappsid.erp.configurator.Configurable
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(RollerChoiceColorBase.class);

    // attributes
    //GENCODE 3703fd90-c29c-11e0-962b-0800200c9a66 Values changed to ValueContainerwith generic inner type
    private ValueContainer<String> code = new ValueContainer<String>(this);
    private ValueContainer<String> description = new ValueContainer<String>(this);
    private ValueContainer<Integer> red = new ValueContainer<Integer>(this);
    private ValueContainer<Integer> green = new ValueContainer<Integer>(this);
    private ValueContainer<Integer> blue = new ValueContainer<Integer>(this);
    private ValueContainer<String> wood = new ValueContainer<String>(this);
    private ValueContainer<String> imgPath = new ValueContainer<String>(this);
    private ValueContainer<String> texturePath = new ValueContainer<String>(this);
    private ValueContainer<String> enrTissus = new ValueContainer<String>(this);
    private ValueContainer<String> enrModeleSM = new ValueContainer<String>(this);
    private ValueContainer<String> enrModeleLarge = new ValueContainer<String>(this);
    private ValueContainer<String> enrModeleMultistop = new ValueContainer<String>(this);
    private ValueContainer<String> enrBdcSM = new ValueContainer<String>(this);
    private ValueContainer<String> enrBdcLarge = new ValueContainer<String>(this);
    private ValueContainer<String> enrBdcRonde = new ValueContainer<String>(this);
    private ValueContainer<String> enrBdcMultistop = new ValueContainer<String>(this);
    private ValueContainer<String> enrProfilStorclip = new ValueContainer<String>(this);
    private ValueContainer<String> enrClipVertStorclip = new ValueContainer<String>(this);
    private ValueContainer<String> enrClipToitStorclip = new ValueContainer<String>(this);
    private ValueContainer<String> enrCachesSM = new ValueContainer<String>(this);
    private ValueContainer<String> enrManivelleSM = new ValueContainer<String>(this);
    private ValueContainer<String> enrManivelleLarge = new ValueContainer<String>(this);
    private ValueContainer<String> enrChainettePVCSM = new ValueContainer<String>(this);
    private ValueContainer<String> enrStorclipVertClip = new ValueContainer<String>(this);
    private ValueContainer<String> enrChainettePVCLarge = new ValueContainer<String>(this);
    private ValueContainer<String> enrStorclipToitClip = new ValueContainer<String>(this);

    // Constructors

    public RollerChoiceColorBase(com.netappsid.erp.configurator.Configurable parent) // GENCODE b298fff0-4bff-11e0-b8af-0800200c9a66
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.blinds.roller.colors.RollerChoiceColor"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    public final void setCode(String code)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.code.getValue();
        this.code.setValue(code);
        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
    }
    public final String removeCode()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.code.getValue();
        String removedValue = this.code.removeValue();
        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultCode(String code)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.code.getValue();
        this.code.setDefaultValue(code);
        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
    }
    
    public final String removeDefaultCode()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.code.getValue();
        String removedValue = this.code.removeDefaultValue();
        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getCodeValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.code;
    }
    public final void fireCodeChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeCodeChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CODE, oldValue, currentValue);
            afterCodeChanged(currentValue);
        }
    }

    public void beforeCodeChanged( String code)
    { }
    public void afterCodeChanged( String code)
    { }

    public boolean isCodeVisible()
    { return true; }

    public boolean isCodeReadOnly()
    { return false; }

    public boolean isCodeEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getCodePrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="0")
        ,@LocalizedTag(language="fr", name="column", value="0")
        ,@LocalizedTag(language="en", name="label", value="Code")
        ,@LocalizedTag(language="fr", name="label", value="Code")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getCode()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.code.getValue();
    }


    public final void setDescription(String description)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.description.getValue();
        this.description.setValue(description);
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
    }
    public final String removeDescription()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.description.getValue();
        String removedValue = this.description.removeValue();
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultDescription(String description)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.description.getValue();
        this.description.setDefaultValue(description);
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
    }
    
    public final String removeDefaultDescription()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.description.getValue();
        String removedValue = this.description.removeDefaultValue();
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getDescriptionValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.description;
    }
    public final void fireDescriptionChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeDescriptionChanged(currentValue);
            firePropertyChange(PROPERTYNAME_DESCRIPTION, oldValue, currentValue);
            afterDescriptionChanged(currentValue);
        }
    }

    public void beforeDescriptionChanged( String description)
    { }
    public void afterDescriptionChanged( String description)
    { }

    public boolean isDescriptionVisible()
    { return true; }

    public boolean isDescriptionReadOnly()
    { return false; }

    public boolean isDescriptionEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getDescriptionPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="2")
        ,@LocalizedTag(language="fr", name="column", value="1")
        ,@LocalizedTag(language="en", name="label", value="Description")
        ,@LocalizedTag(language="fr", name="label", value="Description")
    })
    @Visible("true")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getDescription()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.description.getValue();
    }


    public final void setRed(Integer red)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        Integer oldValue = this.red.getValue();
        this.red.setValue(red);
        Integer currentValue = this.red.getValue();

        fireRedChange(currentValue, oldValue);
    }
    public final Integer removeRed()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        Integer oldValue = this.red.getValue();
        Integer removedValue = this.red.removeValue();
        Integer currentValue = this.red.getValue();

        fireRedChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultRed(Integer red)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        Integer oldValue = this.red.getValue();
        this.red.setDefaultValue(red);
        Integer currentValue = this.red.getValue();

        fireRedChange(currentValue, oldValue);
    }
    
    public final Integer removeDefaultRed()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        Integer oldValue = this.red.getValue();
        Integer removedValue = this.red.removeDefaultValue();
        Integer currentValue = this.red.getValue();

        fireRedChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Integer> getRedValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.red;
    }
    public final void fireRedChange(Integer currentValue, Integer oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeRedChanged(currentValue);
            firePropertyChange(PROPERTYNAME_RED, oldValue, currentValue);
            afterRedChanged(currentValue);
        }
    }

    public void beforeRedChanged( Integer red)
    { }
    public void afterRedChanged( Integer red)
    { }

    public boolean isRedVisible()
    { return true; }

    public boolean isRedReadOnly()
    { return false; }

    public boolean isRedEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getRedPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="3")
        ,@LocalizedTag(language="fr", name="column", value="3")
        ,@LocalizedTag(language="en", name="label", value="Red")
        ,@LocalizedTag(language="fr", name="label", value="Rouge")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final Integer getRed()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.red.getValue();
    }


    public final void setGreen(Integer green)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        Integer oldValue = this.green.getValue();
        this.green.setValue(green);
        Integer currentValue = this.green.getValue();

        fireGreenChange(currentValue, oldValue);
    }
    public final Integer removeGreen()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        Integer oldValue = this.green.getValue();
        Integer removedValue = this.green.removeValue();
        Integer currentValue = this.green.getValue();

        fireGreenChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultGreen(Integer green)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        Integer oldValue = this.green.getValue();
        this.green.setDefaultValue(green);
        Integer currentValue = this.green.getValue();

        fireGreenChange(currentValue, oldValue);
    }
    
    public final Integer removeDefaultGreen()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        Integer oldValue = this.green.getValue();
        Integer removedValue = this.green.removeDefaultValue();
        Integer currentValue = this.green.getValue();

        fireGreenChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Integer> getGreenValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.green;
    }
    public final void fireGreenChange(Integer currentValue, Integer oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeGreenChanged(currentValue);
            firePropertyChange(PROPERTYNAME_GREEN, oldValue, currentValue);
            afterGreenChanged(currentValue);
        }
    }

    public void beforeGreenChanged( Integer green)
    { }
    public void afterGreenChanged( Integer green)
    { }

    public boolean isGreenVisible()
    { return true; }

    public boolean isGreenReadOnly()
    { return false; }

    public boolean isGreenEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getGreenPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="4")
        ,@LocalizedTag(language="fr", name="column", value="4")
        ,@LocalizedTag(language="en", name="label", value="Green")
        ,@LocalizedTag(language="fr", name="label", value="Vert")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final Integer getGreen()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.green.getValue();
    }


    public final void setBlue(Integer blue)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        Integer oldValue = this.blue.getValue();
        this.blue.setValue(blue);
        Integer currentValue = this.blue.getValue();

        fireBlueChange(currentValue, oldValue);
    }
    public final Integer removeBlue()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        Integer oldValue = this.blue.getValue();
        Integer removedValue = this.blue.removeValue();
        Integer currentValue = this.blue.getValue();

        fireBlueChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultBlue(Integer blue)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        Integer oldValue = this.blue.getValue();
        this.blue.setDefaultValue(blue);
        Integer currentValue = this.blue.getValue();

        fireBlueChange(currentValue, oldValue);
    }
    
    public final Integer removeDefaultBlue()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        Integer oldValue = this.blue.getValue();
        Integer removedValue = this.blue.removeDefaultValue();
        Integer currentValue = this.blue.getValue();

        fireBlueChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Integer> getBlueValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.blue;
    }
    public final void fireBlueChange(Integer currentValue, Integer oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeBlueChanged(currentValue);
            firePropertyChange(PROPERTYNAME_BLUE, oldValue, currentValue);
            afterBlueChanged(currentValue);
        }
    }

    public void beforeBlueChanged( Integer blue)
    { }
    public void afterBlueChanged( Integer blue)
    { }

    public boolean isBlueVisible()
    { return true; }

    public boolean isBlueReadOnly()
    { return false; }

    public boolean isBlueEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getBluePrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="5")
        ,@LocalizedTag(language="fr", name="column", value="5")
        ,@LocalizedTag(language="en", name="label", value="Blue")
        ,@LocalizedTag(language="fr", name="label", value="Bleu")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final Integer getBlue()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.blue.getValue();
    }


    public final void setWood(String wood)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.wood.getValue();
        this.wood.setValue(wood);
        String currentValue = this.wood.getValue();

        fireWoodChange(currentValue, oldValue);
    }
    public final String removeWood()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.wood.getValue();
        String removedValue = this.wood.removeValue();
        String currentValue = this.wood.getValue();

        fireWoodChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultWood(String wood)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.wood.getValue();
        this.wood.setDefaultValue(wood);
        String currentValue = this.wood.getValue();

        fireWoodChange(currentValue, oldValue);
    }
    
    public final String removeDefaultWood()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.wood.getValue();
        String removedValue = this.wood.removeDefaultValue();
        String currentValue = this.wood.getValue();

        fireWoodChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getWoodValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.wood;
    }
    public final void fireWoodChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeWoodChanged(currentValue);
            firePropertyChange(PROPERTYNAME_WOOD, oldValue, currentValue);
            afterWoodChanged(currentValue);
        }
    }

    public void beforeWoodChanged( String wood)
    { }
    public void afterWoodChanged( String wood)
    { }

    public boolean isWoodVisible()
    { return true; }

    public boolean isWoodReadOnly()
    { return false; }

    public boolean isWoodEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getWoodPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="6")
        ,@LocalizedTag(language="fr", name="column", value="6")
        ,@LocalizedTag(language="en", name="label", value="Wood")
        ,@LocalizedTag(language="fr", name="label", value="Bois")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getWood()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.wood.getValue();
    }


    public final void setImgPath(String imgPath)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.imgPath.getValue();
        this.imgPath.setValue(imgPath);
        String currentValue = this.imgPath.getValue();

        fireImgPathChange(currentValue, oldValue);
    }
    public final String removeImgPath()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.imgPath.getValue();
        String removedValue = this.imgPath.removeValue();
        String currentValue = this.imgPath.getValue();

        fireImgPathChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultImgPath(String imgPath)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.imgPath.getValue();
        this.imgPath.setDefaultValue(imgPath);
        String currentValue = this.imgPath.getValue();

        fireImgPathChange(currentValue, oldValue);
    }
    
    public final String removeDefaultImgPath()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.imgPath.getValue();
        String removedValue = this.imgPath.removeDefaultValue();
        String currentValue = this.imgPath.getValue();

        fireImgPathChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getImgPathValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.imgPath;
    }
    public final void fireImgPathChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeImgPathChanged(currentValue);
            firePropertyChange(PROPERTYNAME_IMGPATH, oldValue, currentValue);
            afterImgPathChanged(currentValue);
        }
    }

    public void beforeImgPathChanged( String imgPath)
    { }
    public void afterImgPathChanged( String imgPath)
    { }

    public boolean isImgPathVisible()
    { return true; }

    public boolean isImgPathReadOnly()
    { return false; }

    public boolean isImgPathEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getImgPathPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="7")
        ,@LocalizedTag(language="fr", name="column", value="7")
        ,@LocalizedTag(language="en", name="label", value="images path")
        ,@LocalizedTag(language="fr", name="label", value="Chemin des images")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getImgPath()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.imgPath.getValue();
    }


    public final void setTexturePath(String texturePath)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.texturePath.getValue();
        this.texturePath.setValue(texturePath);
        String currentValue = this.texturePath.getValue();

        fireTexturePathChange(currentValue, oldValue);
    }
    public final String removeTexturePath()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.texturePath.getValue();
        String removedValue = this.texturePath.removeValue();
        String currentValue = this.texturePath.getValue();

        fireTexturePathChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultTexturePath(String texturePath)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.texturePath.getValue();
        this.texturePath.setDefaultValue(texturePath);
        String currentValue = this.texturePath.getValue();

        fireTexturePathChange(currentValue, oldValue);
    }
    
    public final String removeDefaultTexturePath()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.texturePath.getValue();
        String removedValue = this.texturePath.removeDefaultValue();
        String currentValue = this.texturePath.getValue();

        fireTexturePathChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getTexturePathValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.texturePath;
    }
    public final void fireTexturePathChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeTexturePathChanged(currentValue);
            firePropertyChange(PROPERTYNAME_TEXTUREPATH, oldValue, currentValue);
            afterTexturePathChanged(currentValue);
        }
    }

    public void beforeTexturePathChanged( String texturePath)
    { }
    public void afterTexturePathChanged( String texturePath)
    { }

    public boolean isTexturePathVisible()
    { return true; }

    public boolean isTexturePathReadOnly()
    { return false; }

    public boolean isTexturePathEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getTexturePathPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="8")
        ,@LocalizedTag(language="fr", name="column", value="8")
        ,@LocalizedTag(language="en", name="label", value="Textures path")
        ,@LocalizedTag(language="fr", name="label", value="Chemin des textures")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getTexturePath()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.texturePath.getValue();
    }


    public final void setEnrTissus(String enrTissus)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.enrTissus.getValue();
        this.enrTissus.setValue(enrTissus);
        String currentValue = this.enrTissus.getValue();

        fireEnrTissusChange(currentValue, oldValue);
    }
    public final String removeEnrTissus()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.enrTissus.getValue();
        String removedValue = this.enrTissus.removeValue();
        String currentValue = this.enrTissus.getValue();

        fireEnrTissusChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultEnrTissus(String enrTissus)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.enrTissus.getValue();
        this.enrTissus.setDefaultValue(enrTissus);
        String currentValue = this.enrTissus.getValue();

        fireEnrTissusChange(currentValue, oldValue);
    }
    
    public final String removeDefaultEnrTissus()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.enrTissus.getValue();
        String removedValue = this.enrTissus.removeDefaultValue();
        String currentValue = this.enrTissus.getValue();

        fireEnrTissusChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getEnrTissusValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.enrTissus;
    }
    public final void fireEnrTissusChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeEnrTissusChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ENRTISSUS, oldValue, currentValue);
            afterEnrTissusChanged(currentValue);
        }
    }

    public void beforeEnrTissusChanged( String enrTissus)
    { }
    public void afterEnrTissusChanged( String enrTissus)
    { }

    public boolean isEnrTissusVisible()
    { return true; }

    public boolean isEnrTissusReadOnly()
    { return false; }

    public boolean isEnrTissusEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getEnrTissusPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="9")
        ,@LocalizedTag(language="fr", name="column", value="9")
        ,@LocalizedTag(language="en", name="label", value="Shade")
        ,@LocalizedTag(language="fr", name="label", value="Tissus")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getEnrTissus()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.enrTissus.getValue();
    }


    public final void setEnrModeleSM(String enrModeleSM)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.enrModeleSM.getValue();
        this.enrModeleSM.setValue(enrModeleSM);
        String currentValue = this.enrModeleSM.getValue();

        fireEnrModeleSMChange(currentValue, oldValue);
    }
    public final String removeEnrModeleSM()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.enrModeleSM.getValue();
        String removedValue = this.enrModeleSM.removeValue();
        String currentValue = this.enrModeleSM.getValue();

        fireEnrModeleSMChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultEnrModeleSM(String enrModeleSM)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.enrModeleSM.getValue();
        this.enrModeleSM.setDefaultValue(enrModeleSM);
        String currentValue = this.enrModeleSM.getValue();

        fireEnrModeleSMChange(currentValue, oldValue);
    }
    
    public final String removeDefaultEnrModeleSM()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.enrModeleSM.getValue();
        String removedValue = this.enrModeleSM.removeDefaultValue();
        String currentValue = this.enrModeleSM.getValue();

        fireEnrModeleSMChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getEnrModeleSMValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.enrModeleSM;
    }
    public final void fireEnrModeleSMChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeEnrModeleSMChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ENRMODELESM, oldValue, currentValue);
            afterEnrModeleSMChanged(currentValue);
        }
    }

    public void beforeEnrModeleSMChanged( String enrModeleSM)
    { }
    public void afterEnrModeleSMChanged( String enrModeleSM)
    { }

    public boolean isEnrModeleSMVisible()
    { return true; }

    public boolean isEnrModeleSMReadOnly()
    { return false; }

    public boolean isEnrModeleSMEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getEnrModeleSMPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="10")
        ,@LocalizedTag(language="fr", name="column", value="10")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getEnrModeleSM()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.enrModeleSM.getValue();
    }


    public final void setEnrModeleLarge(String enrModeleLarge)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.enrModeleLarge.getValue();
        this.enrModeleLarge.setValue(enrModeleLarge);
        String currentValue = this.enrModeleLarge.getValue();

        fireEnrModeleLargeChange(currentValue, oldValue);
    }
    public final String removeEnrModeleLarge()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.enrModeleLarge.getValue();
        String removedValue = this.enrModeleLarge.removeValue();
        String currentValue = this.enrModeleLarge.getValue();

        fireEnrModeleLargeChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultEnrModeleLarge(String enrModeleLarge)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.enrModeleLarge.getValue();
        this.enrModeleLarge.setDefaultValue(enrModeleLarge);
        String currentValue = this.enrModeleLarge.getValue();

        fireEnrModeleLargeChange(currentValue, oldValue);
    }
    
    public final String removeDefaultEnrModeleLarge()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.enrModeleLarge.getValue();
        String removedValue = this.enrModeleLarge.removeDefaultValue();
        String currentValue = this.enrModeleLarge.getValue();

        fireEnrModeleLargeChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getEnrModeleLargeValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.enrModeleLarge;
    }
    public final void fireEnrModeleLargeChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeEnrModeleLargeChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ENRMODELELARGE, oldValue, currentValue);
            afterEnrModeleLargeChanged(currentValue);
        }
    }

    public void beforeEnrModeleLargeChanged( String enrModeleLarge)
    { }
    public void afterEnrModeleLargeChanged( String enrModeleLarge)
    { }

    public boolean isEnrModeleLargeVisible()
    { return true; }

    public boolean isEnrModeleLargeReadOnly()
    { return false; }

    public boolean isEnrModeleLargeEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getEnrModeleLargePrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="11")
        ,@LocalizedTag(language="fr", name="column", value="11")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getEnrModeleLarge()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.enrModeleLarge.getValue();
    }


    public final void setEnrModeleMultistop(String enrModeleMultistop)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.enrModeleMultistop.getValue();
        this.enrModeleMultistop.setValue(enrModeleMultistop);
        String currentValue = this.enrModeleMultistop.getValue();

        fireEnrModeleMultistopChange(currentValue, oldValue);
    }
    public final String removeEnrModeleMultistop()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.enrModeleMultistop.getValue();
        String removedValue = this.enrModeleMultistop.removeValue();
        String currentValue = this.enrModeleMultistop.getValue();

        fireEnrModeleMultistopChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultEnrModeleMultistop(String enrModeleMultistop)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.enrModeleMultistop.getValue();
        this.enrModeleMultistop.setDefaultValue(enrModeleMultistop);
        String currentValue = this.enrModeleMultistop.getValue();

        fireEnrModeleMultistopChange(currentValue, oldValue);
    }
    
    public final String removeDefaultEnrModeleMultistop()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.enrModeleMultistop.getValue();
        String removedValue = this.enrModeleMultistop.removeDefaultValue();
        String currentValue = this.enrModeleMultistop.getValue();

        fireEnrModeleMultistopChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getEnrModeleMultistopValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.enrModeleMultistop;
    }
    public final void fireEnrModeleMultistopChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeEnrModeleMultistopChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ENRMODELEMULTISTOP, oldValue, currentValue);
            afterEnrModeleMultistopChanged(currentValue);
        }
    }

    public void beforeEnrModeleMultistopChanged( String enrModeleMultistop)
    { }
    public void afterEnrModeleMultistopChanged( String enrModeleMultistop)
    { }

    public boolean isEnrModeleMultistopVisible()
    { return true; }

    public boolean isEnrModeleMultistopReadOnly()
    { return false; }

    public boolean isEnrModeleMultistopEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getEnrModeleMultistopPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="12")
        ,@LocalizedTag(language="fr", name="column", value="12")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getEnrModeleMultistop()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.enrModeleMultistop.getValue();
    }


    public final void setEnrBdcSM(String enrBdcSM)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.enrBdcSM.getValue();
        this.enrBdcSM.setValue(enrBdcSM);
        String currentValue = this.enrBdcSM.getValue();

        fireEnrBdcSMChange(currentValue, oldValue);
    }
    public final String removeEnrBdcSM()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.enrBdcSM.getValue();
        String removedValue = this.enrBdcSM.removeValue();
        String currentValue = this.enrBdcSM.getValue();

        fireEnrBdcSMChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultEnrBdcSM(String enrBdcSM)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.enrBdcSM.getValue();
        this.enrBdcSM.setDefaultValue(enrBdcSM);
        String currentValue = this.enrBdcSM.getValue();

        fireEnrBdcSMChange(currentValue, oldValue);
    }
    
    public final String removeDefaultEnrBdcSM()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.enrBdcSM.getValue();
        String removedValue = this.enrBdcSM.removeDefaultValue();
        String currentValue = this.enrBdcSM.getValue();

        fireEnrBdcSMChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getEnrBdcSMValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.enrBdcSM;
    }
    public final void fireEnrBdcSMChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeEnrBdcSMChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ENRBDCSM, oldValue, currentValue);
            afterEnrBdcSMChanged(currentValue);
        }
    }

    public void beforeEnrBdcSMChanged( String enrBdcSM)
    { }
    public void afterEnrBdcSMChanged( String enrBdcSM)
    { }

    public boolean isEnrBdcSMVisible()
    { return true; }

    public boolean isEnrBdcSMReadOnly()
    { return false; }

    public boolean isEnrBdcSMEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getEnrBdcSMPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="13")
        ,@LocalizedTag(language="fr", name="column", value="13")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getEnrBdcSM()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.enrBdcSM.getValue();
    }


    public final void setEnrBdcLarge(String enrBdcLarge)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.enrBdcLarge.getValue();
        this.enrBdcLarge.setValue(enrBdcLarge);
        String currentValue = this.enrBdcLarge.getValue();

        fireEnrBdcLargeChange(currentValue, oldValue);
    }
    public final String removeEnrBdcLarge()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.enrBdcLarge.getValue();
        String removedValue = this.enrBdcLarge.removeValue();
        String currentValue = this.enrBdcLarge.getValue();

        fireEnrBdcLargeChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultEnrBdcLarge(String enrBdcLarge)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.enrBdcLarge.getValue();
        this.enrBdcLarge.setDefaultValue(enrBdcLarge);
        String currentValue = this.enrBdcLarge.getValue();

        fireEnrBdcLargeChange(currentValue, oldValue);
    }
    
    public final String removeDefaultEnrBdcLarge()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.enrBdcLarge.getValue();
        String removedValue = this.enrBdcLarge.removeDefaultValue();
        String currentValue = this.enrBdcLarge.getValue();

        fireEnrBdcLargeChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getEnrBdcLargeValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.enrBdcLarge;
    }
    public final void fireEnrBdcLargeChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeEnrBdcLargeChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ENRBDCLARGE, oldValue, currentValue);
            afterEnrBdcLargeChanged(currentValue);
        }
    }

    public void beforeEnrBdcLargeChanged( String enrBdcLarge)
    { }
    public void afterEnrBdcLargeChanged( String enrBdcLarge)
    { }

    public boolean isEnrBdcLargeVisible()
    { return true; }

    public boolean isEnrBdcLargeReadOnly()
    { return false; }

    public boolean isEnrBdcLargeEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getEnrBdcLargePrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="14")
        ,@LocalizedTag(language="fr", name="column", value="14")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getEnrBdcLarge()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.enrBdcLarge.getValue();
    }


    public final void setEnrBdcRonde(String enrBdcRonde)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.enrBdcRonde.getValue();
        this.enrBdcRonde.setValue(enrBdcRonde);
        String currentValue = this.enrBdcRonde.getValue();

        fireEnrBdcRondeChange(currentValue, oldValue);
    }
    public final String removeEnrBdcRonde()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.enrBdcRonde.getValue();
        String removedValue = this.enrBdcRonde.removeValue();
        String currentValue = this.enrBdcRonde.getValue();

        fireEnrBdcRondeChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultEnrBdcRonde(String enrBdcRonde)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.enrBdcRonde.getValue();
        this.enrBdcRonde.setDefaultValue(enrBdcRonde);
        String currentValue = this.enrBdcRonde.getValue();

        fireEnrBdcRondeChange(currentValue, oldValue);
    }
    
    public final String removeDefaultEnrBdcRonde()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.enrBdcRonde.getValue();
        String removedValue = this.enrBdcRonde.removeDefaultValue();
        String currentValue = this.enrBdcRonde.getValue();

        fireEnrBdcRondeChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getEnrBdcRondeValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.enrBdcRonde;
    }
    public final void fireEnrBdcRondeChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeEnrBdcRondeChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ENRBDCRONDE, oldValue, currentValue);
            afterEnrBdcRondeChanged(currentValue);
        }
    }

    public void beforeEnrBdcRondeChanged( String enrBdcRonde)
    { }
    public void afterEnrBdcRondeChanged( String enrBdcRonde)
    { }

    public boolean isEnrBdcRondeVisible()
    { return true; }

    public boolean isEnrBdcRondeReadOnly()
    { return false; }

    public boolean isEnrBdcRondeEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getEnrBdcRondePrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="15")
        ,@LocalizedTag(language="fr", name="column", value="15")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getEnrBdcRonde()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.enrBdcRonde.getValue();
    }


    public final void setEnrBdcMultistop(String enrBdcMultistop)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.enrBdcMultistop.getValue();
        this.enrBdcMultistop.setValue(enrBdcMultistop);
        String currentValue = this.enrBdcMultistop.getValue();

        fireEnrBdcMultistopChange(currentValue, oldValue);
    }
    public final String removeEnrBdcMultistop()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.enrBdcMultistop.getValue();
        String removedValue = this.enrBdcMultistop.removeValue();
        String currentValue = this.enrBdcMultistop.getValue();

        fireEnrBdcMultistopChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultEnrBdcMultistop(String enrBdcMultistop)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.enrBdcMultistop.getValue();
        this.enrBdcMultistop.setDefaultValue(enrBdcMultistop);
        String currentValue = this.enrBdcMultistop.getValue();

        fireEnrBdcMultistopChange(currentValue, oldValue);
    }
    
    public final String removeDefaultEnrBdcMultistop()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.enrBdcMultistop.getValue();
        String removedValue = this.enrBdcMultistop.removeDefaultValue();
        String currentValue = this.enrBdcMultistop.getValue();

        fireEnrBdcMultistopChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getEnrBdcMultistopValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.enrBdcMultistop;
    }
    public final void fireEnrBdcMultistopChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeEnrBdcMultistopChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ENRBDCMULTISTOP, oldValue, currentValue);
            afterEnrBdcMultistopChanged(currentValue);
        }
    }

    public void beforeEnrBdcMultistopChanged( String enrBdcMultistop)
    { }
    public void afterEnrBdcMultistopChanged( String enrBdcMultistop)
    { }

    public boolean isEnrBdcMultistopVisible()
    { return true; }

    public boolean isEnrBdcMultistopReadOnly()
    { return false; }

    public boolean isEnrBdcMultistopEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getEnrBdcMultistopPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="16")
        ,@LocalizedTag(language="fr", name="column", value="16")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getEnrBdcMultistop()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.enrBdcMultistop.getValue();
    }


    public final void setEnrProfilStorclip(String enrProfilStorclip)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.enrProfilStorclip.getValue();
        this.enrProfilStorclip.setValue(enrProfilStorclip);
        String currentValue = this.enrProfilStorclip.getValue();

        fireEnrProfilStorclipChange(currentValue, oldValue);
    }
    public final String removeEnrProfilStorclip()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.enrProfilStorclip.getValue();
        String removedValue = this.enrProfilStorclip.removeValue();
        String currentValue = this.enrProfilStorclip.getValue();

        fireEnrProfilStorclipChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultEnrProfilStorclip(String enrProfilStorclip)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.enrProfilStorclip.getValue();
        this.enrProfilStorclip.setDefaultValue(enrProfilStorclip);
        String currentValue = this.enrProfilStorclip.getValue();

        fireEnrProfilStorclipChange(currentValue, oldValue);
    }
    
    public final String removeDefaultEnrProfilStorclip()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.enrProfilStorclip.getValue();
        String removedValue = this.enrProfilStorclip.removeDefaultValue();
        String currentValue = this.enrProfilStorclip.getValue();

        fireEnrProfilStorclipChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getEnrProfilStorclipValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.enrProfilStorclip;
    }
    public final void fireEnrProfilStorclipChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeEnrProfilStorclipChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ENRPROFILSTORCLIP, oldValue, currentValue);
            afterEnrProfilStorclipChanged(currentValue);
        }
    }

    public void beforeEnrProfilStorclipChanged( String enrProfilStorclip)
    { }
    public void afterEnrProfilStorclipChanged( String enrProfilStorclip)
    { }

    public boolean isEnrProfilStorclipVisible()
    { return true; }

    public boolean isEnrProfilStorclipReadOnly()
    { return false; }

    public boolean isEnrProfilStorclipEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getEnrProfilStorclipPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="17")
        ,@LocalizedTag(language="fr", name="column", value="17")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getEnrProfilStorclip()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.enrProfilStorclip.getValue();
    }


    public final void setEnrClipVertStorclip(String enrClipVertStorclip)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.enrClipVertStorclip.getValue();
        this.enrClipVertStorclip.setValue(enrClipVertStorclip);
        String currentValue = this.enrClipVertStorclip.getValue();

        fireEnrClipVertStorclipChange(currentValue, oldValue);
    }
    public final String removeEnrClipVertStorclip()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.enrClipVertStorclip.getValue();
        String removedValue = this.enrClipVertStorclip.removeValue();
        String currentValue = this.enrClipVertStorclip.getValue();

        fireEnrClipVertStorclipChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultEnrClipVertStorclip(String enrClipVertStorclip)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.enrClipVertStorclip.getValue();
        this.enrClipVertStorclip.setDefaultValue(enrClipVertStorclip);
        String currentValue = this.enrClipVertStorclip.getValue();

        fireEnrClipVertStorclipChange(currentValue, oldValue);
    }
    
    public final String removeDefaultEnrClipVertStorclip()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.enrClipVertStorclip.getValue();
        String removedValue = this.enrClipVertStorclip.removeDefaultValue();
        String currentValue = this.enrClipVertStorclip.getValue();

        fireEnrClipVertStorclipChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getEnrClipVertStorclipValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.enrClipVertStorclip;
    }
    public final void fireEnrClipVertStorclipChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeEnrClipVertStorclipChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ENRCLIPVERTSTORCLIP, oldValue, currentValue);
            afterEnrClipVertStorclipChanged(currentValue);
        }
    }

    public void beforeEnrClipVertStorclipChanged( String enrClipVertStorclip)
    { }
    public void afterEnrClipVertStorclipChanged( String enrClipVertStorclip)
    { }

    public boolean isEnrClipVertStorclipVisible()
    { return true; }

    public boolean isEnrClipVertStorclipReadOnly()
    { return false; }

    public boolean isEnrClipVertStorclipEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getEnrClipVertStorclipPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="18")
        ,@LocalizedTag(language="fr", name="column", value="18")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getEnrClipVertStorclip()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.enrClipVertStorclip.getValue();
    }


    public final void setEnrClipToitStorclip(String enrClipToitStorclip)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.enrClipToitStorclip.getValue();
        this.enrClipToitStorclip.setValue(enrClipToitStorclip);
        String currentValue = this.enrClipToitStorclip.getValue();

        fireEnrClipToitStorclipChange(currentValue, oldValue);
    }
    public final String removeEnrClipToitStorclip()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.enrClipToitStorclip.getValue();
        String removedValue = this.enrClipToitStorclip.removeValue();
        String currentValue = this.enrClipToitStorclip.getValue();

        fireEnrClipToitStorclipChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultEnrClipToitStorclip(String enrClipToitStorclip)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.enrClipToitStorclip.getValue();
        this.enrClipToitStorclip.setDefaultValue(enrClipToitStorclip);
        String currentValue = this.enrClipToitStorclip.getValue();

        fireEnrClipToitStorclipChange(currentValue, oldValue);
    }
    
    public final String removeDefaultEnrClipToitStorclip()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.enrClipToitStorclip.getValue();
        String removedValue = this.enrClipToitStorclip.removeDefaultValue();
        String currentValue = this.enrClipToitStorclip.getValue();

        fireEnrClipToitStorclipChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getEnrClipToitStorclipValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.enrClipToitStorclip;
    }
    public final void fireEnrClipToitStorclipChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeEnrClipToitStorclipChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ENRCLIPTOITSTORCLIP, oldValue, currentValue);
            afterEnrClipToitStorclipChanged(currentValue);
        }
    }

    public void beforeEnrClipToitStorclipChanged( String enrClipToitStorclip)
    { }
    public void afterEnrClipToitStorclipChanged( String enrClipToitStorclip)
    { }

    public boolean isEnrClipToitStorclipVisible()
    { return true; }

    public boolean isEnrClipToitStorclipReadOnly()
    { return false; }

    public boolean isEnrClipToitStorclipEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getEnrClipToitStorclipPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="19")
        ,@LocalizedTag(language="fr", name="column", value="19")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getEnrClipToitStorclip()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.enrClipToitStorclip.getValue();
    }


    public final void setEnrCachesSM(String enrCachesSM)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.enrCachesSM.getValue();
        this.enrCachesSM.setValue(enrCachesSM);
        String currentValue = this.enrCachesSM.getValue();

        fireEnrCachesSMChange(currentValue, oldValue);
    }
    public final String removeEnrCachesSM()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.enrCachesSM.getValue();
        String removedValue = this.enrCachesSM.removeValue();
        String currentValue = this.enrCachesSM.getValue();

        fireEnrCachesSMChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultEnrCachesSM(String enrCachesSM)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.enrCachesSM.getValue();
        this.enrCachesSM.setDefaultValue(enrCachesSM);
        String currentValue = this.enrCachesSM.getValue();

        fireEnrCachesSMChange(currentValue, oldValue);
    }
    
    public final String removeDefaultEnrCachesSM()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.enrCachesSM.getValue();
        String removedValue = this.enrCachesSM.removeDefaultValue();
        String currentValue = this.enrCachesSM.getValue();

        fireEnrCachesSMChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getEnrCachesSMValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.enrCachesSM;
    }
    public final void fireEnrCachesSMChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeEnrCachesSMChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ENRCACHESSM, oldValue, currentValue);
            afterEnrCachesSMChanged(currentValue);
        }
    }

    public void beforeEnrCachesSMChanged( String enrCachesSM)
    { }
    public void afterEnrCachesSMChanged( String enrCachesSM)
    { }

    public boolean isEnrCachesSMVisible()
    { return true; }

    public boolean isEnrCachesSMReadOnly()
    { return false; }

    public boolean isEnrCachesSMEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getEnrCachesSMPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="20")
        ,@LocalizedTag(language="fr", name="column", value="20")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getEnrCachesSM()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.enrCachesSM.getValue();
    }


    public final void setEnrManivelleSM(String enrManivelleSM)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.enrManivelleSM.getValue();
        this.enrManivelleSM.setValue(enrManivelleSM);
        String currentValue = this.enrManivelleSM.getValue();

        fireEnrManivelleSMChange(currentValue, oldValue);
    }
    public final String removeEnrManivelleSM()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.enrManivelleSM.getValue();
        String removedValue = this.enrManivelleSM.removeValue();
        String currentValue = this.enrManivelleSM.getValue();

        fireEnrManivelleSMChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultEnrManivelleSM(String enrManivelleSM)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.enrManivelleSM.getValue();
        this.enrManivelleSM.setDefaultValue(enrManivelleSM);
        String currentValue = this.enrManivelleSM.getValue();

        fireEnrManivelleSMChange(currentValue, oldValue);
    }
    
    public final String removeDefaultEnrManivelleSM()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.enrManivelleSM.getValue();
        String removedValue = this.enrManivelleSM.removeDefaultValue();
        String currentValue = this.enrManivelleSM.getValue();

        fireEnrManivelleSMChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getEnrManivelleSMValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.enrManivelleSM;
    }
    public final void fireEnrManivelleSMChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeEnrManivelleSMChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ENRMANIVELLESM, oldValue, currentValue);
            afterEnrManivelleSMChanged(currentValue);
        }
    }

    public void beforeEnrManivelleSMChanged( String enrManivelleSM)
    { }
    public void afterEnrManivelleSMChanged( String enrManivelleSM)
    { }

    public boolean isEnrManivelleSMVisible()
    { return true; }

    public boolean isEnrManivelleSMReadOnly()
    { return false; }

    public boolean isEnrManivelleSMEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getEnrManivelleSMPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="21")
        ,@LocalizedTag(language="fr", name="column", value="21")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getEnrManivelleSM()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.enrManivelleSM.getValue();
    }


    public final void setEnrManivelleLarge(String enrManivelleLarge)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.enrManivelleLarge.getValue();
        this.enrManivelleLarge.setValue(enrManivelleLarge);
        String currentValue = this.enrManivelleLarge.getValue();

        fireEnrManivelleLargeChange(currentValue, oldValue);
    }
    public final String removeEnrManivelleLarge()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.enrManivelleLarge.getValue();
        String removedValue = this.enrManivelleLarge.removeValue();
        String currentValue = this.enrManivelleLarge.getValue();

        fireEnrManivelleLargeChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultEnrManivelleLarge(String enrManivelleLarge)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.enrManivelleLarge.getValue();
        this.enrManivelleLarge.setDefaultValue(enrManivelleLarge);
        String currentValue = this.enrManivelleLarge.getValue();

        fireEnrManivelleLargeChange(currentValue, oldValue);
    }
    
    public final String removeDefaultEnrManivelleLarge()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.enrManivelleLarge.getValue();
        String removedValue = this.enrManivelleLarge.removeDefaultValue();
        String currentValue = this.enrManivelleLarge.getValue();

        fireEnrManivelleLargeChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getEnrManivelleLargeValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.enrManivelleLarge;
    }
    public final void fireEnrManivelleLargeChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeEnrManivelleLargeChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ENRMANIVELLELARGE, oldValue, currentValue);
            afterEnrManivelleLargeChanged(currentValue);
        }
    }

    public void beforeEnrManivelleLargeChanged( String enrManivelleLarge)
    { }
    public void afterEnrManivelleLargeChanged( String enrManivelleLarge)
    { }

    public boolean isEnrManivelleLargeVisible()
    { return true; }

    public boolean isEnrManivelleLargeReadOnly()
    { return false; }

    public boolean isEnrManivelleLargeEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getEnrManivelleLargePrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="22")
        ,@LocalizedTag(language="fr", name="column", value="22")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getEnrManivelleLarge()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.enrManivelleLarge.getValue();
    }


    public final void setEnrChainettePVCSM(String enrChainettePVCSM)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.enrChainettePVCSM.getValue();
        this.enrChainettePVCSM.setValue(enrChainettePVCSM);
        String currentValue = this.enrChainettePVCSM.getValue();

        fireEnrChainettePVCSMChange(currentValue, oldValue);
    }
    public final String removeEnrChainettePVCSM()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.enrChainettePVCSM.getValue();
        String removedValue = this.enrChainettePVCSM.removeValue();
        String currentValue = this.enrChainettePVCSM.getValue();

        fireEnrChainettePVCSMChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultEnrChainettePVCSM(String enrChainettePVCSM)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.enrChainettePVCSM.getValue();
        this.enrChainettePVCSM.setDefaultValue(enrChainettePVCSM);
        String currentValue = this.enrChainettePVCSM.getValue();

        fireEnrChainettePVCSMChange(currentValue, oldValue);
    }
    
    public final String removeDefaultEnrChainettePVCSM()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.enrChainettePVCSM.getValue();
        String removedValue = this.enrChainettePVCSM.removeDefaultValue();
        String currentValue = this.enrChainettePVCSM.getValue();

        fireEnrChainettePVCSMChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getEnrChainettePVCSMValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.enrChainettePVCSM;
    }
    public final void fireEnrChainettePVCSMChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeEnrChainettePVCSMChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ENRCHAINETTEPVCSM, oldValue, currentValue);
            afterEnrChainettePVCSMChanged(currentValue);
        }
    }

    public void beforeEnrChainettePVCSMChanged( String enrChainettePVCSM)
    { }
    public void afterEnrChainettePVCSMChanged( String enrChainettePVCSM)
    { }

    public boolean isEnrChainettePVCSMVisible()
    { return true; }

    public boolean isEnrChainettePVCSMReadOnly()
    { return false; }

    public boolean isEnrChainettePVCSMEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getEnrChainettePVCSMPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="23")
        ,@LocalizedTag(language="fr", name="column", value="23")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getEnrChainettePVCSM()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.enrChainettePVCSM.getValue();
    }


    public final void setEnrStorclipVertClip(String enrStorclipVertClip)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.enrStorclipVertClip.getValue();
        this.enrStorclipVertClip.setValue(enrStorclipVertClip);
        String currentValue = this.enrStorclipVertClip.getValue();

        fireEnrStorclipVertClipChange(currentValue, oldValue);
    }
    public final String removeEnrStorclipVertClip()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.enrStorclipVertClip.getValue();
        String removedValue = this.enrStorclipVertClip.removeValue();
        String currentValue = this.enrStorclipVertClip.getValue();

        fireEnrStorclipVertClipChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultEnrStorclipVertClip(String enrStorclipVertClip)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.enrStorclipVertClip.getValue();
        this.enrStorclipVertClip.setDefaultValue(enrStorclipVertClip);
        String currentValue = this.enrStorclipVertClip.getValue();

        fireEnrStorclipVertClipChange(currentValue, oldValue);
    }
    
    public final String removeDefaultEnrStorclipVertClip()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.enrStorclipVertClip.getValue();
        String removedValue = this.enrStorclipVertClip.removeDefaultValue();
        String currentValue = this.enrStorclipVertClip.getValue();

        fireEnrStorclipVertClipChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getEnrStorclipVertClipValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.enrStorclipVertClip;
    }
    public final void fireEnrStorclipVertClipChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeEnrStorclipVertClipChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ENRSTORCLIPVERTCLIP, oldValue, currentValue);
            afterEnrStorclipVertClipChanged(currentValue);
        }
    }

    public void beforeEnrStorclipVertClipChanged( String enrStorclipVertClip)
    { }
    public void afterEnrStorclipVertClipChanged( String enrStorclipVertClip)
    { }

    public boolean isEnrStorclipVertClipVisible()
    { return true; }

    public boolean isEnrStorclipVertClipReadOnly()
    { return false; }

    public boolean isEnrStorclipVertClipEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getEnrStorclipVertClipPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="24")
        ,@LocalizedTag(language="fr", name="column", value="24")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getEnrStorclipVertClip()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.enrStorclipVertClip.getValue();
    }


    public final void setEnrChainettePVCLarge(String enrChainettePVCLarge)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.enrChainettePVCLarge.getValue();
        this.enrChainettePVCLarge.setValue(enrChainettePVCLarge);
        String currentValue = this.enrChainettePVCLarge.getValue();

        fireEnrChainettePVCLargeChange(currentValue, oldValue);
    }
    public final String removeEnrChainettePVCLarge()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.enrChainettePVCLarge.getValue();
        String removedValue = this.enrChainettePVCLarge.removeValue();
        String currentValue = this.enrChainettePVCLarge.getValue();

        fireEnrChainettePVCLargeChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultEnrChainettePVCLarge(String enrChainettePVCLarge)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.enrChainettePVCLarge.getValue();
        this.enrChainettePVCLarge.setDefaultValue(enrChainettePVCLarge);
        String currentValue = this.enrChainettePVCLarge.getValue();

        fireEnrChainettePVCLargeChange(currentValue, oldValue);
    }
    
    public final String removeDefaultEnrChainettePVCLarge()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.enrChainettePVCLarge.getValue();
        String removedValue = this.enrChainettePVCLarge.removeDefaultValue();
        String currentValue = this.enrChainettePVCLarge.getValue();

        fireEnrChainettePVCLargeChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getEnrChainettePVCLargeValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.enrChainettePVCLarge;
    }
    public final void fireEnrChainettePVCLargeChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeEnrChainettePVCLargeChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ENRCHAINETTEPVCLARGE, oldValue, currentValue);
            afterEnrChainettePVCLargeChanged(currentValue);
        }
    }

    public void beforeEnrChainettePVCLargeChanged( String enrChainettePVCLarge)
    { }
    public void afterEnrChainettePVCLargeChanged( String enrChainettePVCLarge)
    { }

    public boolean isEnrChainettePVCLargeVisible()
    { return true; }

    public boolean isEnrChainettePVCLargeReadOnly()
    { return false; }

    public boolean isEnrChainettePVCLargeEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getEnrChainettePVCLargePrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="24")
        ,@LocalizedTag(language="fr", name="column", value="24")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getEnrChainettePVCLarge()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.enrChainettePVCLarge.getValue();
    }


    public final void setEnrStorclipToitClip(String enrStorclipToitClip)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.enrStorclipToitClip.getValue();
        this.enrStorclipToitClip.setValue(enrStorclipToitClip);
        String currentValue = this.enrStorclipToitClip.getValue();

        fireEnrStorclipToitClipChange(currentValue, oldValue);
    }
    public final String removeEnrStorclipToitClip()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.enrStorclipToitClip.getValue();
        String removedValue = this.enrStorclipToitClip.removeValue();
        String currentValue = this.enrStorclipToitClip.getValue();

        fireEnrStorclipToitClipChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultEnrStorclipToitClip(String enrStorclipToitClip)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.enrStorclipToitClip.getValue();
        this.enrStorclipToitClip.setDefaultValue(enrStorclipToitClip);
        String currentValue = this.enrStorclipToitClip.getValue();

        fireEnrStorclipToitClipChange(currentValue, oldValue);
    }
    
    public final String removeDefaultEnrStorclipToitClip()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.enrStorclipToitClip.getValue();
        String removedValue = this.enrStorclipToitClip.removeDefaultValue();
        String currentValue = this.enrStorclipToitClip.getValue();

        fireEnrStorclipToitClipChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getEnrStorclipToitClipValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.enrStorclipToitClip;
    }
    public final void fireEnrStorclipToitClipChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeEnrStorclipToitClipChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ENRSTORCLIPTOITCLIP, oldValue, currentValue);
            afterEnrStorclipToitClipChanged(currentValue);
        }
    }

    public void beforeEnrStorclipToitClipChanged( String enrStorclipToitClip)
    { }
    public void afterEnrStorclipToitClipChanged( String enrStorclipToitClip)
    { }

    public boolean isEnrStorclipToitClipVisible()
    { return true; }

    public boolean isEnrStorclipToitClipReadOnly()
    { return false; }

    public boolean isEnrStorclipToitClipEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getEnrStorclipToitClipPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="25")
        ,@LocalizedTag(language="fr", name="column", value="25")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getEnrStorclipToitClip()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.enrStorclipToitClip.getValue();
    }



    // Business methods 



    @Override
    public void setDefaultValues()
    { 
        super.setDefaultValues();
    }

    // Bound properties

    public static final String PROPERTYNAME_CODE = "code";  
    public static final String PROPERTYNAME_DESCRIPTION = "description";  
    public static final String PROPERTYNAME_RED = "red";  
    public static final String PROPERTYNAME_GREEN = "green";  
    public static final String PROPERTYNAME_BLUE = "blue";  
    public static final String PROPERTYNAME_WOOD = "wood";  
    public static final String PROPERTYNAME_IMGPATH = "imgPath";  
    public static final String PROPERTYNAME_TEXTUREPATH = "texturePath";  
    public static final String PROPERTYNAME_ENRTISSUS = "enrTissus";  
    public static final String PROPERTYNAME_ENRMODELESM = "enrModeleSM";  
    public static final String PROPERTYNAME_ENRMODELELARGE = "enrModeleLarge";  
    public static final String PROPERTYNAME_ENRMODELEMULTISTOP = "enrModeleMultistop";  
    public static final String PROPERTYNAME_ENRBDCSM = "enrBdcSM";  
    public static final String PROPERTYNAME_ENRBDCLARGE = "enrBdcLarge";  
    public static final String PROPERTYNAME_ENRBDCRONDE = "enrBdcRonde";  
    public static final String PROPERTYNAME_ENRBDCMULTISTOP = "enrBdcMultistop";  
    public static final String PROPERTYNAME_ENRPROFILSTORCLIP = "enrProfilStorclip";  
    public static final String PROPERTYNAME_ENRCLIPVERTSTORCLIP = "enrClipVertStorclip";  
    public static final String PROPERTYNAME_ENRCLIPTOITSTORCLIP = "enrClipToitStorclip";  
    public static final String PROPERTYNAME_ENRCACHESSM = "enrCachesSM";  
    public static final String PROPERTYNAME_ENRMANIVELLESM = "enrManivelleSM";  
    public static final String PROPERTYNAME_ENRMANIVELLELARGE = "enrManivelleLarge";  
    public static final String PROPERTYNAME_ENRCHAINETTEPVCSM = "enrChainettePVCSM";  
    public static final String PROPERTYNAME_ENRSTORCLIPVERTCLIP = "enrStorclipVertClip";  
    public static final String PROPERTYNAME_ENRCHAINETTEPVCLARGE = "enrChainettePVCLarge";  
    public static final String PROPERTYNAME_ENRSTORCLIPTOITCLIP = "enrStorclipToitClip";  
}
