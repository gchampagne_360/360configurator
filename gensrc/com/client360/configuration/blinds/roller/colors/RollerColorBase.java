// generated class, do not edit

package com.client360.configuration.blinds.roller.colors;

//Plugin V13.1.2
//2013-12-06

// Imports 
import com.client360.configuration.blinds.roller.colors.RollerChoiceColor;
import com.client360.configuration.blinds.roller.colors.enums.RollerChoiceColorType;
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.AssociatedComment;
import com.netappsid.erp.configurator.annotations.AssociatedDrawing;
import com.netappsid.erp.configurator.annotations.GlobalProperty;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;
import com.netappsid.erp.configurator.annotations.Mandatory;
import com.netappsid.erp.configurator.annotations.Printable;
import com.netappsid.erp.configurator.annotations.Visible;

public abstract class RollerColorBase extends com.client360.configuration.blinds.Color
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(RollerColorBase.class);

    // attributes
    private ValueContainer<RollerChoiceColor> rollerChoiceColor = new ValueContainer<RollerChoiceColor>(this);
    private ValueContainer<RollerChoiceColorType> rollerChoiceColorType = new ValueContainer<RollerChoiceColorType>(this);

    // Constructors

    public RollerColorBase(com.netappsid.erp.configurator.Configurable parent) // GENCODE b298fff0-4bff-11e0-b8af-0800200c9a66
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.blinds.roller.colors.RollerColor"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Color")
        ,@LocalizedTag(language="fr", name="label", value="Couleur")
    })
    @Mandatory("true")
    @GlobalProperty("false")
    @Printable("true")
    @Visible("true")
    @AssociatedComment("false")
    @AssociatedDrawing("false")

    public final RollerChoiceColor getRollerChoiceColor()
    {   //GENCODE e76ad2a0-c2bd-11e0-962b-0800200c9a66 Value Container getter operations
        return rollerChoiceColor.getValue();
    }


    public final void setRollerChoiceColor(RollerChoiceColor rollerChoiceColor)
    {   // GENCODE 9cda9e30-c2bf-11e0-962b-0800200c9a66 SETTER

        RollerChoiceColor oldValue = this.rollerChoiceColor.getValue();
        this.rollerChoiceColor.setValue(rollerChoiceColor);

        RollerChoiceColor currentValue = this.rollerChoiceColor.getValue();

         fireRollerChoiceColorChange(currentValue , oldValue);
    }

    public final RollerChoiceColor removeRollerChoiceColor()
    {   // GENCODE 07d769f0-c376-11e0-962b-0800200c9a66 DEFAULT REMOVE
        
        RollerChoiceColor oldValue = this.rollerChoiceColor.getValue();
        RollerChoiceColor removedValue = this.rollerChoiceColor.removeValue();
        RollerChoiceColor currentValue = this.rollerChoiceColor.getValue();

        if(removedValue != oldValue)
             dispose(removedValue);

        fireRollerChoiceColorChange(currentValue, oldValue);
        return removedValue;
    }

    public final void setDefaultRollerChoiceColor(RollerChoiceColor rollerChoiceColor)
    {   // GENCODE 07d769f1-c376-11e0-962b-0800200c9a66 DEFAULT SETTER
        RollerChoiceColor oldValue = this.rollerChoiceColor.getValue();
        this.rollerChoiceColor.setDefaultValue(rollerChoiceColor);
        RollerChoiceColor currentValue = this.rollerChoiceColor.getValue();

         fireRollerChoiceColorChange(currentValue, oldValue);
    }
    
    public final RollerChoiceColor removeDefaultRollerChoiceColor()
    {   // GENCODE 07d769f2-c376-11e0-962b-0800200c9a66 DEFAULT REMOVE
        
        RollerChoiceColor oldValue = this.rollerChoiceColor.getValue();
        RollerChoiceColor removedValue = this.rollerChoiceColor.removeDefaultValue();
        RollerChoiceColor currentValue = this.rollerChoiceColor.getValue();

        if(removedValue != oldValue)
             dispose(removedValue);

        fireRollerChoiceColorChange(currentValue, oldValue);
        return removedValue;
    }

    public final ValueContainerGetter<RollerChoiceColor> getRollerChoiceColorValueContainer()
    {   //GENCODE 5200e500-c2be-11e0-962b-0800200c9a66 ValueContainer GETTER
        return rollerChoiceColor;
    }

    public final void fireRollerChoiceColorChange(RollerChoiceColor currentValue, RollerChoiceColor oldValue)
    {   //GENCODE 209a8340-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeRollerChoiceColorChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ROLLERCHOICECOLOR, oldValue, currentValue);
            afterRollerChoiceColorChanged(currentValue);
        }
    }
 
    public void pushRollerChoiceColor()
    { }
 
    public void beforeRollerChoiceColorChanged( RollerChoiceColor rollerChoiceColor)
    { }
    public void afterRollerChoiceColorChanged( RollerChoiceColor rollerChoiceColor)
    { }
 
    public boolean isRollerChoiceColorVisible()
    { return true; }

    public boolean isRollerChoiceColorReadOnly()
    { return false; }

    public double getRollerChoiceColorPrice()
    { return 0d; }

    public boolean isRollerChoiceColorEnabled()
    { return true; /* GENCODE bdccd463-4c1c-4ba1-aece-55f4c7cfeae7 */ }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Color type")
        ,@LocalizedTag(language="fr", name="label", value="Type de couleur")
    })
    @Mandatory("true")
    @GlobalProperty("false")
    @Printable("true")
    @Visible("true")
    @AssociatedComment("false")
    @AssociatedDrawing("false")

    public final RollerChoiceColorType getRollerChoiceColorType()
    {   //GENCODE e76ad2a0-c2bd-11e0-962b-0800200c9a66 Value Container getter operations
        return rollerChoiceColorType.getValue();
    }


    public final void setRollerChoiceColorType(RollerChoiceColorType rollerChoiceColorType)
    {   // GENCODE 9cda9e30-c2bf-11e0-962b-0800200c9a66 SETTER

        RollerChoiceColorType oldValue = this.rollerChoiceColorType.getValue();
        this.rollerChoiceColorType.setValue(rollerChoiceColorType);

        RollerChoiceColorType currentValue = this.rollerChoiceColorType.getValue();

         fireRollerChoiceColorTypeChange(currentValue , oldValue);
    }

    public final RollerChoiceColorType removeRollerChoiceColorType()
    {   // GENCODE 07d769f0-c376-11e0-962b-0800200c9a66 DEFAULT REMOVE
        
        RollerChoiceColorType oldValue = this.rollerChoiceColorType.getValue();
        RollerChoiceColorType removedValue = this.rollerChoiceColorType.removeValue();
        RollerChoiceColorType currentValue = this.rollerChoiceColorType.getValue();

        if(removedValue != oldValue)
             dispose(removedValue);

        fireRollerChoiceColorTypeChange(currentValue, oldValue);
        return removedValue;
    }

    public final void setDefaultRollerChoiceColorType(RollerChoiceColorType rollerChoiceColorType)
    {   // GENCODE 07d769f1-c376-11e0-962b-0800200c9a66 DEFAULT SETTER
        RollerChoiceColorType oldValue = this.rollerChoiceColorType.getValue();
        this.rollerChoiceColorType.setDefaultValue(rollerChoiceColorType);
        RollerChoiceColorType currentValue = this.rollerChoiceColorType.getValue();

         fireRollerChoiceColorTypeChange(currentValue, oldValue);
    }
    
    public final RollerChoiceColorType removeDefaultRollerChoiceColorType()
    {   // GENCODE 07d769f2-c376-11e0-962b-0800200c9a66 DEFAULT REMOVE
        
        RollerChoiceColorType oldValue = this.rollerChoiceColorType.getValue();
        RollerChoiceColorType removedValue = this.rollerChoiceColorType.removeDefaultValue();
        RollerChoiceColorType currentValue = this.rollerChoiceColorType.getValue();

        if(removedValue != oldValue)
             dispose(removedValue);

        fireRollerChoiceColorTypeChange(currentValue, oldValue);
        return removedValue;
    }

    public final ValueContainerGetter<RollerChoiceColorType> getRollerChoiceColorTypeValueContainer()
    {   //GENCODE 5200e500-c2be-11e0-962b-0800200c9a66 ValueContainer GETTER
        return rollerChoiceColorType;
    }

    public final void fireRollerChoiceColorTypeChange(RollerChoiceColorType currentValue, RollerChoiceColorType oldValue)
    {   //GENCODE 209a8340-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeRollerChoiceColorTypeChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ROLLERCHOICECOLORTYPE, oldValue, currentValue);
            afterRollerChoiceColorTypeChanged(currentValue);
        }
    }
 
    public void pushRollerChoiceColorType()
    { }
 
    public void beforeRollerChoiceColorTypeChanged( RollerChoiceColorType rollerChoiceColorType)
    { }
    public void afterRollerChoiceColorTypeChanged( RollerChoiceColorType rollerChoiceColorType)
    { }
 
    public boolean isRollerChoiceColorTypeVisible()
    { return true; }

    public boolean isRollerChoiceColorTypeReadOnly()
    { return false; }

    public double getRollerChoiceColorTypePrice()
    { return 0d; }

    public boolean isRollerChoiceColorTypeEnabled()
    { return true; /* GENCODE bdccd463-4c1c-4ba1-aece-55f4c7cfeae7 */ }

    public boolean isRollerChoiceColorTypeEnabled(RollerChoiceColorType choice)
    { return true; }


    // Business methods 



    public boolean isRollerChoiceColorMandatory()
    {
        return true;
    }

    public boolean isRollerChoiceColorTypeMandatory()
    {
        return true;
    }

    @Override
    public void setDefaultValues()
    { 
        super.setDefaultValues();
    }

    @Override
    protected String getSequences()
    {
        return "choiceColorType,choiceColor,codeCouleurSpeciale,colorChartName,texturePath";  
    }

    // Bound properties

    public static final String PROPERTYNAME_ROLLERCHOICECOLOR = "rollerChoiceColor";  
    public static final String PROPERTYNAME_ROLLERCHOICECOLORTYPE = "rollerChoiceColorType";  
}
