// generated class, do not edit

package com.client360.configuration.blinds.roller.colors.enums;

// Imports 


public enum RollerChoiceColorType
{
    @SuppressWarnings("nls") RAL("RAL", "RAL", "", "", "", "", "", "images/color/RAL_Classic.png"),
    @SuppressWarnings("nls") STD("Standard", "Standard", "", "", "", "", "", "images/color/colorsstd.png"),
    @SuppressWarnings("nls") AUTRE("Autre", "Other", "", "", "", "", "", ""),
    @SuppressWarnings("nls") TOILE("TOILE", "TOILE", "", "", "", "", "", "");

    public String label_fr;
    public String label_en;
    public String defaultChoice;
    public String enabled;
    public String messageEnabled_fr;
    public String messageEnabled_en;
    public String price;
    public String image;
    
    RollerChoiceColorType(String label_fr, String label_en, String defaultChoice, String enabled, String messageEnabled_fr, String messageEnabled_en, String price, String image)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.defaultChoice = defaultChoice;
        this.enabled = enabled;
        this.messageEnabled_fr = messageEnabled_fr;
        this.messageEnabled_en = messageEnabled_en;
        this.price = price;
        this.image = image;
    }

}

