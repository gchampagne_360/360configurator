// generated class, do not edit

package com.client360.configuration.blinds.global;

//Plugin V13.1.2
//2013-12-06

// Imports 
import com.client360.configuration.blinds.global.enums.GlbEnumChoixCoteMan;
import com.client360.configuration.blinds.global.enums.GlbEnumChoixMarque;
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.AssociatedComment;
import com.netappsid.erp.configurator.annotations.AssociatedDrawing;
import com.netappsid.erp.configurator.annotations.GlobalProperty;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;
import com.netappsid.erp.configurator.annotations.Mandatory;
import com.netappsid.erp.configurator.annotations.Printable;
import com.netappsid.erp.configurator.annotations.Visible;

public abstract class GlbConfMoteurBase extends com.netappsid.erp.configurator.Configurable
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(GlbConfMoteurBase.class);

    // attributes
    private ValueContainer<GlbEnumChoixCoteMan> choixSortieCable = new ValueContainer<GlbEnumChoixCoteMan>(this);
    private ValueContainer<GlbEnumChoixMarque> choixMarque = new ValueContainer<GlbEnumChoixMarque>(this);
    private ValueContainer<GlbConfOrganeCde[]> choixOrganeCde = new ValueContainer<GlbConfOrganeCde[]>(this);

    // Constructors

    public GlbConfMoteurBase(com.netappsid.erp.configurator.Configurable parent) // GENCODE b298fff0-4bff-11e0-b8af-0800200c9a66
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.blinds.global.GlbConfMoteur"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="fr", name="desc", value="Choisir le c�t� de sortie du c�ble")
        ,@LocalizedTag(language="fr", name="label", value="Choix sortie de c�ble")
    })
    @Mandatory("true")
    @GlobalProperty("false")
    @Printable("true")
    @Visible("true")
    @AssociatedComment("false")
    @AssociatedDrawing("false")

    public final GlbEnumChoixCoteMan getChoixSortieCable()
    {   //GENCODE e76ad2a0-c2bd-11e0-962b-0800200c9a66 Value Container getter operations
        return choixSortieCable.getValue();
    }


    public final void setChoixSortieCable(GlbEnumChoixCoteMan choixSortieCable)
    {   // GENCODE 9cda9e30-c2bf-11e0-962b-0800200c9a66 SETTER

        GlbEnumChoixCoteMan oldValue = this.choixSortieCable.getValue();
        this.choixSortieCable.setValue(choixSortieCable);

        GlbEnumChoixCoteMan currentValue = this.choixSortieCable.getValue();

         fireChoixSortieCableChange(currentValue , oldValue);
    }

    public final GlbEnumChoixCoteMan removeChoixSortieCable()
    {   // GENCODE 07d769f0-c376-11e0-962b-0800200c9a66 DEFAULT REMOVE
        
        GlbEnumChoixCoteMan oldValue = this.choixSortieCable.getValue();
        GlbEnumChoixCoteMan removedValue = this.choixSortieCable.removeValue();
        GlbEnumChoixCoteMan currentValue = this.choixSortieCable.getValue();

        if(removedValue != oldValue)
             dispose(removedValue);

        fireChoixSortieCableChange(currentValue, oldValue);
        return removedValue;
    }

    public final void setDefaultChoixSortieCable(GlbEnumChoixCoteMan choixSortieCable)
    {   // GENCODE 07d769f1-c376-11e0-962b-0800200c9a66 DEFAULT SETTER
        GlbEnumChoixCoteMan oldValue = this.choixSortieCable.getValue();
        this.choixSortieCable.setDefaultValue(choixSortieCable);
        GlbEnumChoixCoteMan currentValue = this.choixSortieCable.getValue();

         fireChoixSortieCableChange(currentValue, oldValue);
    }
    
    public final GlbEnumChoixCoteMan removeDefaultChoixSortieCable()
    {   // GENCODE 07d769f2-c376-11e0-962b-0800200c9a66 DEFAULT REMOVE
        
        GlbEnumChoixCoteMan oldValue = this.choixSortieCable.getValue();
        GlbEnumChoixCoteMan removedValue = this.choixSortieCable.removeDefaultValue();
        GlbEnumChoixCoteMan currentValue = this.choixSortieCable.getValue();

        if(removedValue != oldValue)
             dispose(removedValue);

        fireChoixSortieCableChange(currentValue, oldValue);
        return removedValue;
    }

    public final ValueContainerGetter<GlbEnumChoixCoteMan> getChoixSortieCableValueContainer()
    {   //GENCODE 5200e500-c2be-11e0-962b-0800200c9a66 ValueContainer GETTER
        return choixSortieCable;
    }

    public final void fireChoixSortieCableChange(GlbEnumChoixCoteMan currentValue, GlbEnumChoixCoteMan oldValue)
    {   //GENCODE 209a8340-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoixSortieCableChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOIXSORTIECABLE, oldValue, currentValue);
            afterChoixSortieCableChanged(currentValue);
        }
    }
 
    public void pushChoixSortieCable()
    { }
 
    public void beforeChoixSortieCableChanged( GlbEnumChoixCoteMan choixSortieCable)
    { }
    public void afterChoixSortieCableChanged( GlbEnumChoixCoteMan choixSortieCable)
    { }
 
    public boolean isChoixSortieCableVisible()
    { return true; }

    public boolean isChoixSortieCableReadOnly()
    { return false; }

    public double getChoixSortieCablePrice()
    { return 0d; }

    public boolean isChoixSortieCableEnabled()
    { return true; /* GENCODE bdccd463-4c1c-4ba1-aece-55f4c7cfeae7 */ }

    public boolean isChoixSortieCableEnabled(GlbEnumChoixCoteMan choice)
    { return true; }

    @LocalizedTags
    ({
        @LocalizedTag(language="fr", name="label", value="Choix marque")
        ,@LocalizedTag(language="fr", name="desc", value="Choirisr une marque de moteur")
    })
    @Mandatory("true")
    @GlobalProperty("false")
    @Printable("true")
    @Visible("true")
    @AssociatedComment("false")
    @AssociatedDrawing("false")

    public final GlbEnumChoixMarque getChoixMarque()
    {   //GENCODE e76ad2a0-c2bd-11e0-962b-0800200c9a66 Value Container getter operations
        return choixMarque.getValue();
    }


    public final void setChoixMarque(GlbEnumChoixMarque choixMarque)
    {   // GENCODE 9cda9e30-c2bf-11e0-962b-0800200c9a66 SETTER

        GlbEnumChoixMarque oldValue = this.choixMarque.getValue();
        this.choixMarque.setValue(choixMarque);

        GlbEnumChoixMarque currentValue = this.choixMarque.getValue();

         fireChoixMarqueChange(currentValue , oldValue);
    }

    public final GlbEnumChoixMarque removeChoixMarque()
    {   // GENCODE 07d769f0-c376-11e0-962b-0800200c9a66 DEFAULT REMOVE
        
        GlbEnumChoixMarque oldValue = this.choixMarque.getValue();
        GlbEnumChoixMarque removedValue = this.choixMarque.removeValue();
        GlbEnumChoixMarque currentValue = this.choixMarque.getValue();

        if(removedValue != oldValue)
             dispose(removedValue);

        fireChoixMarqueChange(currentValue, oldValue);
        return removedValue;
    }

    public final void setDefaultChoixMarque(GlbEnumChoixMarque choixMarque)
    {   // GENCODE 07d769f1-c376-11e0-962b-0800200c9a66 DEFAULT SETTER
        GlbEnumChoixMarque oldValue = this.choixMarque.getValue();
        this.choixMarque.setDefaultValue(choixMarque);
        GlbEnumChoixMarque currentValue = this.choixMarque.getValue();

         fireChoixMarqueChange(currentValue, oldValue);
    }
    
    public final GlbEnumChoixMarque removeDefaultChoixMarque()
    {   // GENCODE 07d769f2-c376-11e0-962b-0800200c9a66 DEFAULT REMOVE
        
        GlbEnumChoixMarque oldValue = this.choixMarque.getValue();
        GlbEnumChoixMarque removedValue = this.choixMarque.removeDefaultValue();
        GlbEnumChoixMarque currentValue = this.choixMarque.getValue();

        if(removedValue != oldValue)
             dispose(removedValue);

        fireChoixMarqueChange(currentValue, oldValue);
        return removedValue;
    }

    public final ValueContainerGetter<GlbEnumChoixMarque> getChoixMarqueValueContainer()
    {   //GENCODE 5200e500-c2be-11e0-962b-0800200c9a66 ValueContainer GETTER
        return choixMarque;
    }

    public final void fireChoixMarqueChange(GlbEnumChoixMarque currentValue, GlbEnumChoixMarque oldValue)
    {   //GENCODE 209a8340-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoixMarqueChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOIXMARQUE, oldValue, currentValue);
            afterChoixMarqueChanged(currentValue);
        }
    }
 
    public void pushChoixMarque()
    { }
 
    public void beforeChoixMarqueChanged( GlbEnumChoixMarque choixMarque)
    { }
    public void afterChoixMarqueChanged( GlbEnumChoixMarque choixMarque)
    { }
 
    public boolean isChoixMarqueVisible()
    { return true; }

    public boolean isChoixMarqueReadOnly()
    { return false; }

    public double getChoixMarquePrice()
    { return 0d; }

    public boolean isChoixMarqueEnabled()
    { return true; /* GENCODE bdccd463-4c1c-4ba1-aece-55f4c7cfeae7 */ }

    public boolean isChoixMarqueEnabled(GlbEnumChoixMarque choice)
    { return true; }

    @LocalizedTags
    ({
        @LocalizedTag(language="fr", name="label", value="Organe de commande")
        ,@LocalizedTag(language="en", name="notExplose", value="")
        ,@LocalizedTag(language="fr", name="desc", value="Choisir l\' (les) organe(s) de commande")
        ,@LocalizedTag(language="fr", name="null", value="Aucune")
        ,@LocalizedTag(language="fr", name="explose", value="")
        ,@LocalizedTag(language="fr", name="notExplose", value="")
        ,@LocalizedTag(language="en", name="null", value="None")
        ,@LocalizedTag(language="en", name="explose", value="")
    })
    @Mandatory("")
    @GlobalProperty("false")
    @Printable("true")
    @Visible("true")
    @AssociatedComment("false")
    @AssociatedDrawing("false")

    public final GlbConfOrganeCde[] getChoixOrganeCde()
    {   //GENCODE e76ad2a0-c2bd-11e0-962b-0800200c9a66 Value Container getter operations
        return choixOrganeCde.getValue();
    }


    public final void setChoixOrganeCde(GlbConfOrganeCde[] choixOrganeCde)
    {   // GENCODE 9cda9e30-c2bf-11e0-962b-0800200c9a66 SETTER

        GlbConfOrganeCde[] oldValue = this.choixOrganeCde.getValue();
        this.choixOrganeCde.setValue(choixOrganeCde);

        GlbConfOrganeCde[] currentValue = this.choixOrganeCde.getValue();

         fireChoixOrganeCdeChange(currentValue , oldValue);
    }

    public final GlbConfOrganeCde[] removeChoixOrganeCde()
    {   // GENCODE 07d769f0-c376-11e0-962b-0800200c9a66 DEFAULT REMOVE
        
        GlbConfOrganeCde[] oldValue = this.choixOrganeCde.getValue();
        GlbConfOrganeCde[] removedValue = this.choixOrganeCde.removeValue();
        GlbConfOrganeCde[] currentValue = this.choixOrganeCde.getValue();

        if(removedValue != oldValue)
             dispose(removedValue);

        fireChoixOrganeCdeChange(currentValue, oldValue);
        return removedValue;
    }

    public final void setDefaultChoixOrganeCde(GlbConfOrganeCde[] choixOrganeCde)
    {   // GENCODE 07d769f1-c376-11e0-962b-0800200c9a66 DEFAULT SETTER
        GlbConfOrganeCde[] oldValue = this.choixOrganeCde.getValue();
        this.choixOrganeCde.setDefaultValue(choixOrganeCde);
        GlbConfOrganeCde[] currentValue = this.choixOrganeCde.getValue();

         fireChoixOrganeCdeChange(currentValue, oldValue);
    }
    
    public final GlbConfOrganeCde[] removeDefaultChoixOrganeCde()
    {   // GENCODE 07d769f2-c376-11e0-962b-0800200c9a66 DEFAULT REMOVE
        
        GlbConfOrganeCde[] oldValue = this.choixOrganeCde.getValue();
        GlbConfOrganeCde[] removedValue = this.choixOrganeCde.removeDefaultValue();
        GlbConfOrganeCde[] currentValue = this.choixOrganeCde.getValue();

        if(removedValue != oldValue)
             dispose(removedValue);

        fireChoixOrganeCdeChange(currentValue, oldValue);
        return removedValue;
    }

    public final ValueContainerGetter<GlbConfOrganeCde[]> getChoixOrganeCdeValueContainer()
    {   //GENCODE 5200e500-c2be-11e0-962b-0800200c9a66 ValueContainer GETTER
        return choixOrganeCde;
    }

    public final void fireChoixOrganeCdeChange(GlbConfOrganeCde[] currentValue, GlbConfOrganeCde[] oldValue)
    {   //GENCODE 209a8340-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
                if (currentValue != null)
                {
                    //It's a configurable, set the parent and role
                    acquire(currentValue, "choixOrganeCde");
                }
            beforeChoixOrganeCdeChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOIXORGANECDE, oldValue, currentValue);
            afterChoixOrganeCdeChanged(currentValue);
        }
    }
 
    public void pushChoixOrganeCde()
    { }
 
    public void beforeChoixOrganeCdeChanged( GlbConfOrganeCde[] choixOrganeCde)
    { }
    public void afterChoixOrganeCdeChanged( GlbConfOrganeCde[] choixOrganeCde)
    { }
 
    public boolean isChoixOrganeCdeVisible()
    { return true; }

    public boolean isChoixOrganeCdeReadOnly()
    { return false; }

    public double getChoixOrganeCdePrice()
    { return 0d; }

    public boolean isChoixOrganeCdeEnabled()
    { return true; /* GENCODE bdccd463-4c1c-4ba1-aece-55f4c7cfeae7 */ }


    // Business methods 



    public boolean isChoixSortieCableMandatory()
    {
        return true;
    }

    public boolean isChoixMarqueMandatory()
    {
        return true;
    }

    public boolean isChoixOrganeCdeMandatory()
    {
        return false;
    }

    @Override
    public void setDefaultValues()
    { 
        super.setDefaultValues();
    }

    // Bound properties

    public static final String PROPERTYNAME_CHOIXSORTIECABLE = "choixSortieCable";  
    public static final String PROPERTYNAME_CHOIXMARQUE = "choixMarque";  
    public static final String PROPERTYNAME_CHOIXORGANECDE = "choixOrganeCde";  
}
