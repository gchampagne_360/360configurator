// generated class, do not edit

package com.client360.configuration.blinds.global;

//Plugin V13.1.2
//2013-12-06

// Imports 
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.AssociatedComment;
import com.netappsid.erp.configurator.annotations.AssociatedDrawing;
import com.netappsid.erp.configurator.annotations.GlobalProperty;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;
import com.netappsid.erp.configurator.annotations.Printable;
import com.netappsid.erp.configurator.annotations.Visible;

public abstract class GlbConfOrganeCdeBase extends com.netappsid.erp.configurator.Configurable
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(GlbConfOrganeCdeBase.class);

    // attributes
    //GENCODE 3703fd90-c29c-11e0-962b-0800200c9a66 Values changed to ValueContainerwith generic inner type
    private ValueContainer<String> code = new ValueContainer<String>(this);
    private ValueContainer<Integer> quantite = new ValueContainer<Integer>(this);

    // Constructors

    public GlbConfOrganeCdeBase(com.netappsid.erp.configurator.Configurable parent) // GENCODE b298fff0-4bff-11e0-b8af-0800200c9a66
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.blinds.global.GlbConfOrganeCde"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    public final void setCode(String code)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.code.getValue();
        this.code.setValue(code);
        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
    }
    public final String removeCode()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.code.getValue();
        String removedValue = this.code.removeValue();
        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultCode(String code)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.code.getValue();
        this.code.setDefaultValue(code);
        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
    }
    
    public final String removeDefaultCode()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.code.getValue();
        String removedValue = this.code.removeDefaultValue();
        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getCodeValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.code;
    }
    public final void fireCodeChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeCodeChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CODE, oldValue, currentValue);
            afterCodeChanged(currentValue);
        }
    }

    public void beforeCodeChanged( String code)
    { }
    public void afterCodeChanged( String code)
    { }

    public boolean isCodeVisible()
    { return true; }

    public boolean isCodeReadOnly()
    { return false; }

    public boolean isCodeEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getCodePrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="fr", name="desc", value="Code")
        ,@LocalizedTag(language="fr", name="label", value="Code")
    })
    @GlobalProperty("false")
    @Printable("true")
    @Visible("true")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getCode()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.code.getValue();
    }


    public final void setQuantite(Integer quantite)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        Integer oldValue = this.quantite.getValue();
        this.quantite.setValue(quantite);
        Integer currentValue = this.quantite.getValue();

        fireQuantiteChange(currentValue, oldValue);
    }
    public final Integer removeQuantite()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        Integer oldValue = this.quantite.getValue();
        Integer removedValue = this.quantite.removeValue();
        Integer currentValue = this.quantite.getValue();

        fireQuantiteChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultQuantite(Integer quantite)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        Integer oldValue = this.quantite.getValue();
        this.quantite.setDefaultValue(quantite);
        Integer currentValue = this.quantite.getValue();

        fireQuantiteChange(currentValue, oldValue);
    }
    
    public final Integer removeDefaultQuantite()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        Integer oldValue = this.quantite.getValue();
        Integer removedValue = this.quantite.removeDefaultValue();
        Integer currentValue = this.quantite.getValue();

        fireQuantiteChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Integer> getQuantiteValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.quantite;
    }
    public final void fireQuantiteChange(Integer currentValue, Integer oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeQuantiteChanged(currentValue);
            firePropertyChange(PROPERTYNAME_QUANTITE, oldValue, currentValue);
            afterQuantiteChanged(currentValue);
        }
    }

    public void beforeQuantiteChanged( Integer quantite)
    { }
    public void afterQuantiteChanged( Integer quantite)
    { }

    public boolean isQuantiteVisible()
    { return true; }

    public boolean isQuantiteReadOnly()
    { return false; }

    public boolean isQuantiteEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getQuantitePrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="fr", name="desc", value="Renseigner la quantit�")
        ,@LocalizedTag(language="fr", name="label", value="Quantit�")
    })
    @GlobalProperty("false")
    @Printable("true")
    @Visible("true")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final Integer getQuantite()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.quantite.getValue();
    }



    // Business methods 



    @Override
    public void setDefaultValues()
    { 
        super.setDefaultValues();
    }

    // Bound properties

    public static final String PROPERTYNAME_CODE = "code";  
    public static final String PROPERTYNAME_QUANTITE = "quantite";  
}
