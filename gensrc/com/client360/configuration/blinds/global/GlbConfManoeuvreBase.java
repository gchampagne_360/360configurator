// generated class, do not edit

package com.client360.configuration.blinds.global;

//Plugin V13.1.2
//2013-12-06

// Imports 
import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.blinds.global.enums.GlbEnumChoixCoteMan;
import com.jidesoft.converter.EnumConverter;
import com.netappsid.erp.configurator.NAIDEnumConverter;
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.AssociatedComment;
import com.netappsid.erp.configurator.annotations.AssociatedDrawing;
import com.netappsid.erp.configurator.annotations.GlobalProperty;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;
import com.netappsid.erp.configurator.annotations.Mandatory;
import com.netappsid.erp.configurator.annotations.Printable;
import com.netappsid.erp.configurator.annotations.Visible;

public abstract class GlbConfManoeuvreBase extends com.netappsid.erp.configurator.Configurable
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(GlbConfManoeuvreBase.class);

    // attributes
    //GENCODE 3703fd90-c29c-11e0-962b-0800200c9a66 Values changed to ValueContainerwith generic inner type
    private ValueContainer<Measure<Length>> longueurManoeuvre = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<GlbEnumChoixCoteMan> choixCoteManoeuvre = new ValueContainer<GlbEnumChoixCoteMan>(this);
    private ValueContainer<GlbConfMoteur> moteur = new ValueContainer<GlbConfMoteur>(this);
    private ValueContainer<GlbEnumChoixCoteMan> choixCoteOrientation = new ValueContainer<GlbEnumChoixCoteMan>(this);

    // Constructors

    public GlbConfManoeuvreBase(com.netappsid.erp.configurator.Configurable parent) // GENCODE b298fff0-4bff-11e0-b8af-0800200c9a66
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.blinds.global.GlbConfManoeuvre"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    public final void setLongueurManoeuvre(Measure<Length> longueurManoeuvre)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        longueurManoeuvre = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(longueurManoeuvre, getDefaultUnitFor(PROPERTYNAME_LONGUEURMANOEUVRE,Length.class));

        Measure<Length> oldValue = this.longueurManoeuvre.getValue();
        this.longueurManoeuvre.setValue(longueurManoeuvre);
        Measure<Length> currentValue = this.longueurManoeuvre.getValue();

        fireLongueurManoeuvreChange(currentValue, oldValue);
    }
    public final Measure<Length> removeLongueurManoeuvre()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        Measure<Length> oldValue = this.longueurManoeuvre.getValue();
        Measure<Length> removedValue = this.longueurManoeuvre.removeValue();
        Measure<Length> currentValue = this.longueurManoeuvre.getValue();

        fireLongueurManoeuvreChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultLongueurManoeuvre(Measure<Length> longueurManoeuvre)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        longueurManoeuvre = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(longueurManoeuvre, getDefaultUnitFor(PROPERTYNAME_LONGUEURMANOEUVRE,Length.class));

        Measure<Length> oldValue = this.longueurManoeuvre.getValue();
        this.longueurManoeuvre.setDefaultValue(longueurManoeuvre);
        Measure<Length> currentValue = this.longueurManoeuvre.getValue();

        fireLongueurManoeuvreChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultLongueurManoeuvre()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        Measure<Length> oldValue = this.longueurManoeuvre.getValue();
        Measure<Length> removedValue = this.longueurManoeuvre.removeDefaultValue();
        Measure<Length> currentValue = this.longueurManoeuvre.getValue();

        fireLongueurManoeuvreChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getLongueurManoeuvreValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.longueurManoeuvre;
    }
    public final void fireLongueurManoeuvreChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeLongueurManoeuvreChanged(currentValue);
            firePropertyChange(PROPERTYNAME_LONGUEURMANOEUVRE, oldValue, currentValue);
            afterLongueurManoeuvreChanged(currentValue);
        }
    }

    public void beforeLongueurManoeuvreChanged( Measure<Length> longueurManoeuvre)
    { }
    public void afterLongueurManoeuvreChanged( Measure<Length> longueurManoeuvre)
    { }

    public boolean isLongueurManoeuvreVisible()
    { return true; }

    public boolean isLongueurManoeuvreReadOnly()
    { return false; }

    public boolean isLongueurManoeuvreEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getLongueurManoeuvrePrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="desc", value="Operator length")
        ,@LocalizedTag(language="fr", name="desc", value="Longueur de manoeuvre")
        ,@LocalizedTag(language="en", name="label", value="Operator length")
        ,@LocalizedTag(language="fr", name="label", value="Longueur de manoeuvre")
    })
    @GlobalProperty("false")
    @Printable("true")
    @Visible("true")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final Measure<Length> getLongueurManoeuvre()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.longueurManoeuvre.getValue();
    }


    @LocalizedTags
    ({
        @LocalizedTag(language="fr", name="desc", value="Choisir le c�t� de manoeuvre")
        ,@LocalizedTag(language="fr", name="label", value="C�t� de manoeuvre")
    })
    @Mandatory("true")
    @GlobalProperty("false")
    @Printable("true")
    @Visible("true")
    @AssociatedComment("false")
    @AssociatedDrawing("false")

    public final GlbEnumChoixCoteMan getChoixCoteManoeuvre()
    {   //GENCODE e76ad2a0-c2bd-11e0-962b-0800200c9a66 Value Container getter operations
        return choixCoteManoeuvre.getValue();
    }


    public final void setChoixCoteManoeuvre(GlbEnumChoixCoteMan choixCoteManoeuvre)
    {   // GENCODE 9cda9e30-c2bf-11e0-962b-0800200c9a66 SETTER

        GlbEnumChoixCoteMan oldValue = this.choixCoteManoeuvre.getValue();
        this.choixCoteManoeuvre.setValue(choixCoteManoeuvre);

        GlbEnumChoixCoteMan currentValue = this.choixCoteManoeuvre.getValue();

         fireChoixCoteManoeuvreChange(currentValue , oldValue);
    }

    public final GlbEnumChoixCoteMan removeChoixCoteManoeuvre()
    {   // GENCODE 07d769f0-c376-11e0-962b-0800200c9a66 DEFAULT REMOVE
        
        GlbEnumChoixCoteMan oldValue = this.choixCoteManoeuvre.getValue();
        GlbEnumChoixCoteMan removedValue = this.choixCoteManoeuvre.removeValue();
        GlbEnumChoixCoteMan currentValue = this.choixCoteManoeuvre.getValue();

        if(removedValue != oldValue)
             dispose(removedValue);

        fireChoixCoteManoeuvreChange(currentValue, oldValue);
        return removedValue;
    }

    public final void setDefaultChoixCoteManoeuvre(GlbEnumChoixCoteMan choixCoteManoeuvre)
    {   // GENCODE 07d769f1-c376-11e0-962b-0800200c9a66 DEFAULT SETTER
        GlbEnumChoixCoteMan oldValue = this.choixCoteManoeuvre.getValue();
        this.choixCoteManoeuvre.setDefaultValue(choixCoteManoeuvre);
        GlbEnumChoixCoteMan currentValue = this.choixCoteManoeuvre.getValue();

         fireChoixCoteManoeuvreChange(currentValue, oldValue);
    }
    
    public final GlbEnumChoixCoteMan removeDefaultChoixCoteManoeuvre()
    {   // GENCODE 07d769f2-c376-11e0-962b-0800200c9a66 DEFAULT REMOVE
        
        GlbEnumChoixCoteMan oldValue = this.choixCoteManoeuvre.getValue();
        GlbEnumChoixCoteMan removedValue = this.choixCoteManoeuvre.removeDefaultValue();
        GlbEnumChoixCoteMan currentValue = this.choixCoteManoeuvre.getValue();

        if(removedValue != oldValue)
             dispose(removedValue);

        fireChoixCoteManoeuvreChange(currentValue, oldValue);
        return removedValue;
    }

    public final ValueContainerGetter<GlbEnumChoixCoteMan> getChoixCoteManoeuvreValueContainer()
    {   //GENCODE 5200e500-c2be-11e0-962b-0800200c9a66 ValueContainer GETTER
        return choixCoteManoeuvre;
    }

    public final void fireChoixCoteManoeuvreChange(GlbEnumChoixCoteMan currentValue, GlbEnumChoixCoteMan oldValue)
    {   //GENCODE 209a8340-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoixCoteManoeuvreChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOIXCOTEMANOEUVRE, oldValue, currentValue);
            afterChoixCoteManoeuvreChanged(currentValue);
        }
    }
 
    public void pushChoixCoteManoeuvre()
    { }
 
    public void beforeChoixCoteManoeuvreChanged( GlbEnumChoixCoteMan choixCoteManoeuvre)
    { }
    public void afterChoixCoteManoeuvreChanged( GlbEnumChoixCoteMan choixCoteManoeuvre)
    { }
 
    public boolean isChoixCoteManoeuvreVisible()
    { return true; }

    public boolean isChoixCoteManoeuvreReadOnly()
    { return false; }

    public double getChoixCoteManoeuvrePrice()
    { return 0d; }

    public boolean isChoixCoteManoeuvreEnabled()
    { return true; /* GENCODE bdccd463-4c1c-4ba1-aece-55f4c7cfeae7 */ }

    public boolean isChoixCoteManoeuvreEnabled(GlbEnumChoixCoteMan choice)
    { return true; }

    @LocalizedTags
    ({
        @LocalizedTag(language="fr", name="desc", value="Choisir le moteur")
        ,@LocalizedTag(language="fr", name="label", value="Moteur")
    })
    @Mandatory("")
    @GlobalProperty("false")
    @Printable("true")
    @Visible("true")
    @AssociatedComment("false")
    @AssociatedDrawing("false")

    public final GlbConfMoteur getMoteur()
    {   //GENCODE e76ad2a0-c2bd-11e0-962b-0800200c9a66 Value Container getter operations
        return moteur.getValue();
    }


    public final void setMoteur(GlbConfMoteur moteur)
    {   // GENCODE 9cda9e30-c2bf-11e0-962b-0800200c9a66 SETTER

        GlbConfMoteur oldValue = this.moteur.getValue();
        this.moteur.setValue(moteur);

        GlbConfMoteur currentValue = this.moteur.getValue();

         fireMoteurChange(currentValue , oldValue);
    }

    public final GlbConfMoteur removeMoteur()
    {   // GENCODE 07d769f0-c376-11e0-962b-0800200c9a66 DEFAULT REMOVE
        
        GlbConfMoteur oldValue = this.moteur.getValue();
        GlbConfMoteur removedValue = this.moteur.removeValue();
        GlbConfMoteur currentValue = this.moteur.getValue();

        if(removedValue != oldValue)
             dispose(removedValue);

        fireMoteurChange(currentValue, oldValue);
        return removedValue;
    }

    public final void setDefaultMoteur(GlbConfMoteur moteur)
    {   // GENCODE 07d769f1-c376-11e0-962b-0800200c9a66 DEFAULT SETTER
        GlbConfMoteur oldValue = this.moteur.getValue();
        this.moteur.setDefaultValue(moteur);
        GlbConfMoteur currentValue = this.moteur.getValue();

         fireMoteurChange(currentValue, oldValue);
    }
    
    public final GlbConfMoteur removeDefaultMoteur()
    {   // GENCODE 07d769f2-c376-11e0-962b-0800200c9a66 DEFAULT REMOVE
        
        GlbConfMoteur oldValue = this.moteur.getValue();
        GlbConfMoteur removedValue = this.moteur.removeDefaultValue();
        GlbConfMoteur currentValue = this.moteur.getValue();

        if(removedValue != oldValue)
             dispose(removedValue);

        fireMoteurChange(currentValue, oldValue);
        return removedValue;
    }

    public final ValueContainerGetter<GlbConfMoteur> getMoteurValueContainer()
    {   //GENCODE 5200e500-c2be-11e0-962b-0800200c9a66 ValueContainer GETTER
        return moteur;
    }

    public final void fireMoteurChange(GlbConfMoteur currentValue, GlbConfMoteur oldValue)
    {   //GENCODE 209a8340-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
                if (currentValue != null)
                {
                    //It's a configurable, set the parent and role
                    acquire(currentValue, "moteur");
                }
            beforeMoteurChanged(currentValue);
            firePropertyChange(PROPERTYNAME_MOTEUR, oldValue, currentValue);
            afterMoteurChanged(currentValue);
        }
    }
 
    public void pushMoteur()
    { }
 
    public void beforeMoteurChanged( GlbConfMoteur moteur)
    { }
    public void afterMoteurChanged( GlbConfMoteur moteur)
    { }
 
    public boolean isMoteurVisible()
    { return true; }

    public boolean isMoteurReadOnly()
    { return false; }

    public double getMoteurPrice()
    { return 0d; }

    public boolean isMoteurEnabled()
    { return true; /* GENCODE bdccd463-4c1c-4ba1-aece-55f4c7cfeae7 */ }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Orientation side choice")
        ,@LocalizedTag(language="fr", name="label", value="Choix cote orientation")
    })
    @Mandatory("")
    @GlobalProperty("false")
    @Printable("true")
    @Visible("true")
    @AssociatedComment("false")
    @AssociatedDrawing("false")

    public final GlbEnumChoixCoteMan getChoixCoteOrientation()
    {   //GENCODE e76ad2a0-c2bd-11e0-962b-0800200c9a66 Value Container getter operations
        return choixCoteOrientation.getValue();
    }


    public final void setChoixCoteOrientation(GlbEnumChoixCoteMan choixCoteOrientation)
    {   // GENCODE 9cda9e30-c2bf-11e0-962b-0800200c9a66 SETTER

        GlbEnumChoixCoteMan oldValue = this.choixCoteOrientation.getValue();
        this.choixCoteOrientation.setValue(choixCoteOrientation);

        GlbEnumChoixCoteMan currentValue = this.choixCoteOrientation.getValue();

         fireChoixCoteOrientationChange(currentValue , oldValue);
    }

    public final GlbEnumChoixCoteMan removeChoixCoteOrientation()
    {   // GENCODE 07d769f0-c376-11e0-962b-0800200c9a66 DEFAULT REMOVE
        
        GlbEnumChoixCoteMan oldValue = this.choixCoteOrientation.getValue();
        GlbEnumChoixCoteMan removedValue = this.choixCoteOrientation.removeValue();
        GlbEnumChoixCoteMan currentValue = this.choixCoteOrientation.getValue();

        if(removedValue != oldValue)
             dispose(removedValue);

        fireChoixCoteOrientationChange(currentValue, oldValue);
        return removedValue;
    }

    public final void setDefaultChoixCoteOrientation(GlbEnumChoixCoteMan choixCoteOrientation)
    {   // GENCODE 07d769f1-c376-11e0-962b-0800200c9a66 DEFAULT SETTER
        GlbEnumChoixCoteMan oldValue = this.choixCoteOrientation.getValue();
        this.choixCoteOrientation.setDefaultValue(choixCoteOrientation);
        GlbEnumChoixCoteMan currentValue = this.choixCoteOrientation.getValue();

         fireChoixCoteOrientationChange(currentValue, oldValue);
    }
    
    public final GlbEnumChoixCoteMan removeDefaultChoixCoteOrientation()
    {   // GENCODE 07d769f2-c376-11e0-962b-0800200c9a66 DEFAULT REMOVE
        
        GlbEnumChoixCoteMan oldValue = this.choixCoteOrientation.getValue();
        GlbEnumChoixCoteMan removedValue = this.choixCoteOrientation.removeDefaultValue();
        GlbEnumChoixCoteMan currentValue = this.choixCoteOrientation.getValue();

        if(removedValue != oldValue)
             dispose(removedValue);

        fireChoixCoteOrientationChange(currentValue, oldValue);
        return removedValue;
    }

    public final ValueContainerGetter<GlbEnumChoixCoteMan> getChoixCoteOrientationValueContainer()
    {   //GENCODE 5200e500-c2be-11e0-962b-0800200c9a66 ValueContainer GETTER
        return choixCoteOrientation;
    }

    public final void fireChoixCoteOrientationChange(GlbEnumChoixCoteMan currentValue, GlbEnumChoixCoteMan oldValue)
    {   //GENCODE 209a8340-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChoixCoteOrientationChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHOIXCOTEORIENTATION, oldValue, currentValue);
            afterChoixCoteOrientationChanged(currentValue);
        }
    }
 
    public void pushChoixCoteOrientation()
    { }
 
    public void beforeChoixCoteOrientationChanged( GlbEnumChoixCoteMan choixCoteOrientation)
    { }
    public void afterChoixCoteOrientationChanged( GlbEnumChoixCoteMan choixCoteOrientation)
    { }
 
    public boolean isChoixCoteOrientationVisible()
    { return true; }

    public boolean isChoixCoteOrientationReadOnly()
    { return false; }

    public double getChoixCoteOrientationPrice()
    { return 0d; }

    public boolean isChoixCoteOrientationEnabled()
    { return true; /* GENCODE bdccd463-4c1c-4ba1-aece-55f4c7cfeae7 */ }

    public boolean isChoixCoteOrientationEnabled(GlbEnumChoixCoteMan choice)
    { return true; }


    // Business methods 



    public boolean isChoixCoteManoeuvreMandatory()
    {
        return true;
    }

    public boolean isMoteurMandatory()
    {
        return false;
    }

    public EnumConverter getMoteurFKConverter(PropertyDescriptor pd, Class clazz)
    {
        List<Object> choices = new ArrayList<Object>();
        List<String> labels = new ArrayList<String>();
        Object defaultChoice = null;
        Object tmpObject = null;
        

         choices.add(null);
         labels.add(getLocalizedTag(pd, "null"));
        choices.add(getRedirectedClassIfPresent(GlbConfMoteur.class));
        if (Locale.getDefault().getLanguage().compareTo("fr") == 0)  
        {
            labels.add("");  
        }
        else
        {
            labels.add("");  
        }

        String[] labelArray = new String[labels.size()];
        int i = 0;
        
        for (String s : labels)
        {
            labelArray[i++] = s;
        }

        if (defaultChoice == null)
        {
            return new NAIDEnumConverter(clazz.getName() + "." + pd.getName() + this.hashCode(), clazz, choices.toArray(), labelArray);  
        }
        else
        {
            return new NAIDEnumConverter(clazz.getName() + "." + pd.getName() + this.hashCode(), clazz, choices.toArray(), labelArray, defaultChoice);  
        }
    }


    public boolean isChoixCoteOrientationMandatory()
    {
        return false;
    }

    @Override
    public void setDefaultValues()
    { 
        super.setDefaultValues();
    }

    // Bound properties

    public static final String PROPERTYNAME_LONGUEURMANOEUVRE = "longueurManoeuvre";  
    public static final String PROPERTYNAME_CHOIXCOTEMANOEUVRE = "choixCoteManoeuvre";  
    public static final String PROPERTYNAME_MOTEUR = "moteur";  
    public static final String PROPERTYNAME_CHOIXCOTEORIENTATION = "choixCoteOrientation";  
}
