// generated class, do not edit

package com.client360.configuration.blinds.global.enums;

// Imports 


public enum GlbEnumChoixCalePoignee
{
    @SuppressWarnings("nls") ENR_CALE_SANS("Sans c�le", "", "", "", "", "", "", ""),
    @SuppressWarnings("nls") ENR_CALE_2MM_BLC("C�les blanches �paisseur 2 mm", "", "", "", "", "", "", ""),
    @SuppressWarnings("nls") ENR_CALE_6MM_BLC("C�les blanches �paisseur 6 mm", "", "", "", "", "", "", ""),
    @SuppressWarnings("nls") ENR_CALE_2MM_MAR("C�les marron �paisseur 2 mm", "", "", "", "", "", "", ""),
    @SuppressWarnings("nls") ENR_CALE_6MM_MAR("C�les marron �paisseur 6 mm", "", "", "", "", "", "", "");

    public String label_fr;
    public String label_en;
    public String defaultChoice;
    public String enabled;
    public String messageEnabled_fr;
    public String messageEnabled_en;
    public String price;
    public String image;
    
    GlbEnumChoixCalePoignee(String label_fr, String label_en, String defaultChoice, String enabled, String messageEnabled_fr, String messageEnabled_en, String price, String image)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.defaultChoice = defaultChoice;
        this.enabled = enabled;
        this.messageEnabled_fr = messageEnabled_fr;
        this.messageEnabled_en = messageEnabled_en;
        this.price = price;
        this.image = image;
    }

}

