// generated class, do not edit

package com.client360.configuration.blinds.vertical.color;

//Plugin V13.1.2
//2013-12-06

// Imports 
import com.client360.configuration.blinds.vertical.color.VerticalChoiceColor;
import com.client360.configuration.blinds.vertical.color.enums.VerticalChoiceColorType;
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.AssociatedComment;
import com.netappsid.erp.configurator.annotations.AssociatedDrawing;
import com.netappsid.erp.configurator.annotations.GlobalProperty;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;
import com.netappsid.erp.configurator.annotations.Mandatory;
import com.netappsid.erp.configurator.annotations.Printable;
import com.netappsid.erp.configurator.annotations.Visible;

public abstract class VerticalColorBase extends com.client360.configuration.blinds.Color
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(VerticalColorBase.class);

    // attributes
    private ValueContainer<VerticalChoiceColor> verticalChoiceColor = new ValueContainer<VerticalChoiceColor>(this);
    private ValueContainer<VerticalChoiceColorType> verticalChoiceColorType = new ValueContainer<VerticalChoiceColorType>(this);

    // Constructors

    public VerticalColorBase(com.netappsid.erp.configurator.Configurable parent) // GENCODE b298fff0-4bff-11e0-b8af-0800200c9a66
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.blinds.vertical.color.VerticalColor"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="fr", name="desc", value="Choix du coloris")
        ,@LocalizedTag(language="fr", name="label", value="Coloris")
    })
    @Mandatory("true")
    @GlobalProperty("false")
    @Printable("true")
    @Visible("true")
    @AssociatedComment("false")
    @AssociatedDrawing("false")

    public final VerticalChoiceColor getVerticalChoiceColor()
    {   //GENCODE e76ad2a0-c2bd-11e0-962b-0800200c9a66 Value Container getter operations
        return verticalChoiceColor.getValue();
    }


    public final void setVerticalChoiceColor(VerticalChoiceColor verticalChoiceColor)
    {   // GENCODE 9cda9e30-c2bf-11e0-962b-0800200c9a66 SETTER

        VerticalChoiceColor oldValue = this.verticalChoiceColor.getValue();
        this.verticalChoiceColor.setValue(verticalChoiceColor);

        VerticalChoiceColor currentValue = this.verticalChoiceColor.getValue();

         fireVerticalChoiceColorChange(currentValue , oldValue);
    }

    public final VerticalChoiceColor removeVerticalChoiceColor()
    {   // GENCODE 07d769f0-c376-11e0-962b-0800200c9a66 DEFAULT REMOVE
        
        VerticalChoiceColor oldValue = this.verticalChoiceColor.getValue();
        VerticalChoiceColor removedValue = this.verticalChoiceColor.removeValue();
        VerticalChoiceColor currentValue = this.verticalChoiceColor.getValue();

        if(removedValue != oldValue)
             dispose(removedValue);

        fireVerticalChoiceColorChange(currentValue, oldValue);
        return removedValue;
    }

    public final void setDefaultVerticalChoiceColor(VerticalChoiceColor verticalChoiceColor)
    {   // GENCODE 07d769f1-c376-11e0-962b-0800200c9a66 DEFAULT SETTER
        VerticalChoiceColor oldValue = this.verticalChoiceColor.getValue();
        this.verticalChoiceColor.setDefaultValue(verticalChoiceColor);
        VerticalChoiceColor currentValue = this.verticalChoiceColor.getValue();

         fireVerticalChoiceColorChange(currentValue, oldValue);
    }
    
    public final VerticalChoiceColor removeDefaultVerticalChoiceColor()
    {   // GENCODE 07d769f2-c376-11e0-962b-0800200c9a66 DEFAULT REMOVE
        
        VerticalChoiceColor oldValue = this.verticalChoiceColor.getValue();
        VerticalChoiceColor removedValue = this.verticalChoiceColor.removeDefaultValue();
        VerticalChoiceColor currentValue = this.verticalChoiceColor.getValue();

        if(removedValue != oldValue)
             dispose(removedValue);

        fireVerticalChoiceColorChange(currentValue, oldValue);
        return removedValue;
    }

    public final ValueContainerGetter<VerticalChoiceColor> getVerticalChoiceColorValueContainer()
    {   //GENCODE 5200e500-c2be-11e0-962b-0800200c9a66 ValueContainer GETTER
        return verticalChoiceColor;
    }

    public final void fireVerticalChoiceColorChange(VerticalChoiceColor currentValue, VerticalChoiceColor oldValue)
    {   //GENCODE 209a8340-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeVerticalChoiceColorChanged(currentValue);
            firePropertyChange(PROPERTYNAME_VERTICALCHOICECOLOR, oldValue, currentValue);
            afterVerticalChoiceColorChanged(currentValue);
        }
    }
 
    public void pushVerticalChoiceColor()
    { }
 
    public void beforeVerticalChoiceColorChanged( VerticalChoiceColor verticalChoiceColor)
    { }
    public void afterVerticalChoiceColorChanged( VerticalChoiceColor verticalChoiceColor)
    { }
 
    public boolean isVerticalChoiceColorVisible()
    { return true; }

    public boolean isVerticalChoiceColorReadOnly()
    { return false; }

    public double getVerticalChoiceColorPrice()
    { return 0d; }

    public boolean isVerticalChoiceColorEnabled()
    { return true; /* GENCODE bdccd463-4c1c-4ba1-aece-55f4c7cfeae7 */ }

    @LocalizedTags
    ({
    })
    @Mandatory("true")
    @Visible("true")
    @AssociatedComment("false")
    @AssociatedDrawing("false")

    public final VerticalChoiceColorType getVerticalChoiceColorType()
    {   //GENCODE e76ad2a0-c2bd-11e0-962b-0800200c9a66 Value Container getter operations
        return verticalChoiceColorType.getValue();
    }


    public final void setVerticalChoiceColorType(VerticalChoiceColorType verticalChoiceColorType)
    {   // GENCODE 9cda9e30-c2bf-11e0-962b-0800200c9a66 SETTER

        VerticalChoiceColorType oldValue = this.verticalChoiceColorType.getValue();
        this.verticalChoiceColorType.setValue(verticalChoiceColorType);

        VerticalChoiceColorType currentValue = this.verticalChoiceColorType.getValue();

         fireVerticalChoiceColorTypeChange(currentValue , oldValue);
    }

    public final VerticalChoiceColorType removeVerticalChoiceColorType()
    {   // GENCODE 07d769f0-c376-11e0-962b-0800200c9a66 DEFAULT REMOVE
        
        VerticalChoiceColorType oldValue = this.verticalChoiceColorType.getValue();
        VerticalChoiceColorType removedValue = this.verticalChoiceColorType.removeValue();
        VerticalChoiceColorType currentValue = this.verticalChoiceColorType.getValue();

        if(removedValue != oldValue)
             dispose(removedValue);

        fireVerticalChoiceColorTypeChange(currentValue, oldValue);
        return removedValue;
    }

    public final void setDefaultVerticalChoiceColorType(VerticalChoiceColorType verticalChoiceColorType)
    {   // GENCODE 07d769f1-c376-11e0-962b-0800200c9a66 DEFAULT SETTER
        VerticalChoiceColorType oldValue = this.verticalChoiceColorType.getValue();
        this.verticalChoiceColorType.setDefaultValue(verticalChoiceColorType);
        VerticalChoiceColorType currentValue = this.verticalChoiceColorType.getValue();

         fireVerticalChoiceColorTypeChange(currentValue, oldValue);
    }
    
    public final VerticalChoiceColorType removeDefaultVerticalChoiceColorType()
    {   // GENCODE 07d769f2-c376-11e0-962b-0800200c9a66 DEFAULT REMOVE
        
        VerticalChoiceColorType oldValue = this.verticalChoiceColorType.getValue();
        VerticalChoiceColorType removedValue = this.verticalChoiceColorType.removeDefaultValue();
        VerticalChoiceColorType currentValue = this.verticalChoiceColorType.getValue();

        if(removedValue != oldValue)
             dispose(removedValue);

        fireVerticalChoiceColorTypeChange(currentValue, oldValue);
        return removedValue;
    }

    public final ValueContainerGetter<VerticalChoiceColorType> getVerticalChoiceColorTypeValueContainer()
    {   //GENCODE 5200e500-c2be-11e0-962b-0800200c9a66 ValueContainer GETTER
        return verticalChoiceColorType;
    }

    public final void fireVerticalChoiceColorTypeChange(VerticalChoiceColorType currentValue, VerticalChoiceColorType oldValue)
    {   //GENCODE 209a8340-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeVerticalChoiceColorTypeChanged(currentValue);
            firePropertyChange(PROPERTYNAME_VERTICALCHOICECOLORTYPE, oldValue, currentValue);
            afterVerticalChoiceColorTypeChanged(currentValue);
        }
    }
 
    public void pushVerticalChoiceColorType()
    { }
 
    public void beforeVerticalChoiceColorTypeChanged( VerticalChoiceColorType verticalChoiceColorType)
    { }
    public void afterVerticalChoiceColorTypeChanged( VerticalChoiceColorType verticalChoiceColorType)
    { }
 
    public boolean isVerticalChoiceColorTypeVisible()
    { return true; }

    public boolean isVerticalChoiceColorTypeReadOnly()
    { return false; }

    public double getVerticalChoiceColorTypePrice()
    { return 0d; }

    public boolean isVerticalChoiceColorTypeEnabled()
    { return true; /* GENCODE bdccd463-4c1c-4ba1-aece-55f4c7cfeae7 */ }

    public boolean isVerticalChoiceColorTypeEnabled(VerticalChoiceColorType choice)
    { return true; }


    // Business methods 



    public boolean isVerticalChoiceColorMandatory()
    {
        return true;
    }

    public boolean isVerticalChoiceColorTypeMandatory()
    {
        return true;
    }

    @Override
    public void setDefaultValues()
    { 
        super.setDefaultValues();
    }

    @Override
    protected String getSequences()
    {
        return "venEnumChoixCouleurType,codeCouleurSpeciale,texturePath,colorChartName,class1";  
    }

    // Bound properties

    public static final String PROPERTYNAME_VERTICALCHOICECOLOR = "verticalChoiceColor";  
    public static final String PROPERTYNAME_VERTICALCHOICECOLORTYPE = "verticalChoiceColorType";  
}
