// generated class, do not edit

package com.client360.configuration.blinds.vertical.color;

//Plugin V13.1.2
//2013-12-06

// Imports 
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.AssociatedComment;
import com.netappsid.erp.configurator.annotations.AssociatedDrawing;
import com.netappsid.erp.configurator.annotations.DynamicEnum;
import com.netappsid.erp.configurator.annotations.Filtrable;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;
import com.netappsid.erp.configurator.annotations.Visible;

@DynamicEnum(fileName = "VER_couleurs")
public abstract class VerticalChoiceColorBase extends com.netappsid.erp.configurator.Configurable
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(VerticalChoiceColorBase.class);

    // attributes
    //GENCODE 3703fd90-c29c-11e0-962b-0800200c9a66 Values changed to ValueContainerwith generic inner type
    private ValueContainer<String> bandes127 = new ValueContainer<String>(this);
    private ValueContainer<String> bandes89 = new ValueContainer<String>(this);
    private ValueContainer<Integer> blue = new ValueContainer<Integer>(this);
    private ValueContainer<String> clips = new ValueContainer<String>(this);
    private ValueContainer<String> code = new ValueContainer<String>(this);
    private ValueContainer<String> description = new ValueContainer<String>(this);
    private ValueContainer<String> equerre = new ValueContainer<String>(this);
    private ValueContainer<Integer> green = new ValueContainer<Integer>(this);
    private ValueContainer<String> imgPath = new ValueContainer<String>(this);
    private ValueContainer<String> manoeuvre = new ValueContainer<String>(this);
    private ValueContainer<String> plaques = new ValueContainer<String>(this);
    private ValueContainer<String> rail = new ValueContainer<String>(this);
    private ValueContainer<Integer> red = new ValueContainer<Integer>(this);
    private ValueContainer<String> supports = new ValueContainer<String>(this);
    private ValueContainer<String> texturePath = new ValueContainer<String>(this);
    private ValueContainer<String> chainettes = new ValueContainer<String>(this);

    // Constructors

    public VerticalChoiceColorBase(com.netappsid.erp.configurator.Configurable parent) // GENCODE b298fff0-4bff-11e0-b8af-0800200c9a66
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.blinds.vertical.color.VerticalChoiceColor"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    public final void setBandes127(String bandes127)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.bandes127.getValue();
        this.bandes127.setValue(bandes127);
        String currentValue = this.bandes127.getValue();

        fireBandes127Change(currentValue, oldValue);
    }
    public final String removeBandes127()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.bandes127.getValue();
        String removedValue = this.bandes127.removeValue();
        String currentValue = this.bandes127.getValue();

        fireBandes127Change(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultBandes127(String bandes127)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.bandes127.getValue();
        this.bandes127.setDefaultValue(bandes127);
        String currentValue = this.bandes127.getValue();

        fireBandes127Change(currentValue, oldValue);
    }
    
    public final String removeDefaultBandes127()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.bandes127.getValue();
        String removedValue = this.bandes127.removeDefaultValue();
        String currentValue = this.bandes127.getValue();

        fireBandes127Change(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getBandes127ValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.bandes127;
    }
    public final void fireBandes127Change(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeBandes127Changed(currentValue);
            firePropertyChange(PROPERTYNAME_BANDES127, oldValue, currentValue);
            afterBandes127Changed(currentValue);
        }
    }

    public void beforeBandes127Changed( String bandes127)
    { }
    public void afterBandes127Changed( String bandes127)
    { }

    public boolean isBandes127Visible()
    { return true; }

    public boolean isBandes127ReadOnly()
    { return false; }

    public boolean isBandes127Enabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getBandes127Price()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="13")
        ,@LocalizedTag(language="fr", name="column", value="13")
        ,@LocalizedTag(language="fr", name="label", value="bandes 127 mm")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getBandes127()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.bandes127.getValue();
    }


    public final void setBandes89(String bandes89)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.bandes89.getValue();
        this.bandes89.setValue(bandes89);
        String currentValue = this.bandes89.getValue();

        fireBandes89Change(currentValue, oldValue);
    }
    public final String removeBandes89()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.bandes89.getValue();
        String removedValue = this.bandes89.removeValue();
        String currentValue = this.bandes89.getValue();

        fireBandes89Change(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultBandes89(String bandes89)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.bandes89.getValue();
        this.bandes89.setDefaultValue(bandes89);
        String currentValue = this.bandes89.getValue();

        fireBandes89Change(currentValue, oldValue);
    }
    
    public final String removeDefaultBandes89()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.bandes89.getValue();
        String removedValue = this.bandes89.removeDefaultValue();
        String currentValue = this.bandes89.getValue();

        fireBandes89Change(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getBandes89ValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.bandes89;
    }
    public final void fireBandes89Change(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeBandes89Changed(currentValue);
            firePropertyChange(PROPERTYNAME_BANDES89, oldValue, currentValue);
            afterBandes89Changed(currentValue);
        }
    }

    public void beforeBandes89Changed( String bandes89)
    { }
    public void afterBandes89Changed( String bandes89)
    { }

    public boolean isBandes89Visible()
    { return true; }

    public boolean isBandes89ReadOnly()
    { return false; }

    public boolean isBandes89Enabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getBandes89Price()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="12")
        ,@LocalizedTag(language="fr", name="column", value="12")
        ,@LocalizedTag(language="fr", name="label", value="bandes 89 mm")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getBandes89()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.bandes89.getValue();
    }


    public final void setBlue(Integer blue)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        Integer oldValue = this.blue.getValue();
        this.blue.setValue(blue);
        Integer currentValue = this.blue.getValue();

        fireBlueChange(currentValue, oldValue);
    }
    public final Integer removeBlue()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        Integer oldValue = this.blue.getValue();
        Integer removedValue = this.blue.removeValue();
        Integer currentValue = this.blue.getValue();

        fireBlueChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultBlue(Integer blue)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        Integer oldValue = this.blue.getValue();
        this.blue.setDefaultValue(blue);
        Integer currentValue = this.blue.getValue();

        fireBlueChange(currentValue, oldValue);
    }
    
    public final Integer removeDefaultBlue()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        Integer oldValue = this.blue.getValue();
        Integer removedValue = this.blue.removeDefaultValue();
        Integer currentValue = this.blue.getValue();

        fireBlueChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Integer> getBlueValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.blue;
    }
    public final void fireBlueChange(Integer currentValue, Integer oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeBlueChanged(currentValue);
            firePropertyChange(PROPERTYNAME_BLUE, oldValue, currentValue);
            afterBlueChanged(currentValue);
        }
    }

    public void beforeBlueChanged( Integer blue)
    { }
    public void afterBlueChanged( Integer blue)
    { }

    public boolean isBlueVisible()
    { return true; }

    public boolean isBlueReadOnly()
    { return false; }

    public boolean isBlueEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getBluePrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="5")
        ,@LocalizedTag(language="fr", name="column", value="5")
        ,@LocalizedTag(language="en", name="label", value="blue")
        ,@LocalizedTag(language="fr", name="label", value="bleu")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final Integer getBlue()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.blue.getValue();
    }


    public final void setClips(String clips)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.clips.getValue();
        this.clips.setValue(clips);
        String currentValue = this.clips.getValue();

        fireClipsChange(currentValue, oldValue);
    }
    public final String removeClips()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.clips.getValue();
        String removedValue = this.clips.removeValue();
        String currentValue = this.clips.getValue();

        fireClipsChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultClips(String clips)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.clips.getValue();
        this.clips.setDefaultValue(clips);
        String currentValue = this.clips.getValue();

        fireClipsChange(currentValue, oldValue);
    }
    
    public final String removeDefaultClips()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.clips.getValue();
        String removedValue = this.clips.removeDefaultValue();
        String currentValue = this.clips.getValue();

        fireClipsChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getClipsValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.clips;
    }
    public final void fireClipsChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeClipsChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CLIPS, oldValue, currentValue);
            afterClipsChanged(currentValue);
        }
    }

    public void beforeClipsChanged( String clips)
    { }
    public void afterClipsChanged( String clips)
    { }

    public boolean isClipsVisible()
    { return true; }

    public boolean isClipsReadOnly()
    { return false; }

    public boolean isClipsEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getClipsPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="15")
        ,@LocalizedTag(language="fr", name="column", value="15")
        ,@LocalizedTag(language="fr", name="label", value="Clips")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getClips()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.clips.getValue();
    }


    public final void setCode(String code)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.code.getValue();
        this.code.setValue(code);
        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
    }
    public final String removeCode()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.code.getValue();
        String removedValue = this.code.removeValue();
        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultCode(String code)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.code.getValue();
        this.code.setDefaultValue(code);
        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
    }
    
    public final String removeDefaultCode()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.code.getValue();
        String removedValue = this.code.removeDefaultValue();
        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getCodeValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.code;
    }
    public final void fireCodeChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeCodeChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CODE, oldValue, currentValue);
            afterCodeChanged(currentValue);
        }
    }

    public void beforeCodeChanged( String code)
    { }
    public void afterCodeChanged( String code)
    { }

    public boolean isCodeVisible()
    { return true; }

    public boolean isCodeReadOnly()
    { return false; }

    public boolean isCodeEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getCodePrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="0")
        ,@LocalizedTag(language="fr", name="column", value="0")
        ,@LocalizedTag(language="fr", name="label", value="code")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getCode()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.code.getValue();
    }


    public final void setDescription(String description)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.description.getValue();
        this.description.setValue(description);
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
    }
    public final String removeDescription()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.description.getValue();
        String removedValue = this.description.removeValue();
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultDescription(String description)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.description.getValue();
        this.description.setDefaultValue(description);
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
    }
    
    public final String removeDefaultDescription()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.description.getValue();
        String removedValue = this.description.removeDefaultValue();
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getDescriptionValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.description;
    }
    public final void fireDescriptionChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeDescriptionChanged(currentValue);
            firePropertyChange(PROPERTYNAME_DESCRIPTION, oldValue, currentValue);
            afterDescriptionChanged(currentValue);
        }
    }

    public void beforeDescriptionChanged( String description)
    { }
    public void afterDescriptionChanged( String description)
    { }

    public boolean isDescriptionVisible()
    { return true; }

    public boolean isDescriptionReadOnly()
    { return false; }

    public boolean isDescriptionEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getDescriptionPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="2")
        ,@LocalizedTag(language="fr", name="column", value="1")
        ,@LocalizedTag(language="fr", name="label", value="description")
    })
    @Visible("true")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getDescription()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.description.getValue();
    }


    public final void setEquerre(String equerre)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.equerre.getValue();
        this.equerre.setValue(equerre);
        String currentValue = this.equerre.getValue();

        fireEquerreChange(currentValue, oldValue);
    }
    public final String removeEquerre()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.equerre.getValue();
        String removedValue = this.equerre.removeValue();
        String currentValue = this.equerre.getValue();

        fireEquerreChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultEquerre(String equerre)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.equerre.getValue();
        this.equerre.setDefaultValue(equerre);
        String currentValue = this.equerre.getValue();

        fireEquerreChange(currentValue, oldValue);
    }
    
    public final String removeDefaultEquerre()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.equerre.getValue();
        String removedValue = this.equerre.removeDefaultValue();
        String currentValue = this.equerre.getValue();

        fireEquerreChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getEquerreValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.equerre;
    }
    public final void fireEquerreChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeEquerreChanged(currentValue);
            firePropertyChange(PROPERTYNAME_EQUERRE, oldValue, currentValue);
            afterEquerreChanged(currentValue);
        }
    }

    public void beforeEquerreChanged( String equerre)
    { }
    public void afterEquerreChanged( String equerre)
    { }

    public boolean isEquerreVisible()
    { return true; }

    public boolean isEquerreReadOnly()
    { return false; }

    public boolean isEquerreEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getEquerrePrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="10")
        ,@LocalizedTag(language="fr", name="column", value="10")
        ,@LocalizedTag(language="fr", name="label", value="equerre")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getEquerre()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.equerre.getValue();
    }


    public final void setGreen(Integer green)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        Integer oldValue = this.green.getValue();
        this.green.setValue(green);
        Integer currentValue = this.green.getValue();

        fireGreenChange(currentValue, oldValue);
    }
    public final Integer removeGreen()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        Integer oldValue = this.green.getValue();
        Integer removedValue = this.green.removeValue();
        Integer currentValue = this.green.getValue();

        fireGreenChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultGreen(Integer green)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        Integer oldValue = this.green.getValue();
        this.green.setDefaultValue(green);
        Integer currentValue = this.green.getValue();

        fireGreenChange(currentValue, oldValue);
    }
    
    public final Integer removeDefaultGreen()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        Integer oldValue = this.green.getValue();
        Integer removedValue = this.green.removeDefaultValue();
        Integer currentValue = this.green.getValue();

        fireGreenChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Integer> getGreenValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.green;
    }
    public final void fireGreenChange(Integer currentValue, Integer oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeGreenChanged(currentValue);
            firePropertyChange(PROPERTYNAME_GREEN, oldValue, currentValue);
            afterGreenChanged(currentValue);
        }
    }

    public void beforeGreenChanged( Integer green)
    { }
    public void afterGreenChanged( Integer green)
    { }

    public boolean isGreenVisible()
    { return true; }

    public boolean isGreenReadOnly()
    { return false; }

    public boolean isGreenEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getGreenPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="4")
        ,@LocalizedTag(language="fr", name="column", value="4")
        ,@LocalizedTag(language="en", name="label", value="green")
        ,@LocalizedTag(language="fr", name="label", value="vert")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final Integer getGreen()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.green.getValue();
    }


    public final void setImgPath(String imgPath)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.imgPath.getValue();
        this.imgPath.setValue(imgPath);
        String currentValue = this.imgPath.getValue();

        fireImgPathChange(currentValue, oldValue);
    }
    public final String removeImgPath()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.imgPath.getValue();
        String removedValue = this.imgPath.removeValue();
        String currentValue = this.imgPath.getValue();

        fireImgPathChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultImgPath(String imgPath)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.imgPath.getValue();
        this.imgPath.setDefaultValue(imgPath);
        String currentValue = this.imgPath.getValue();

        fireImgPathChange(currentValue, oldValue);
    }
    
    public final String removeDefaultImgPath()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.imgPath.getValue();
        String removedValue = this.imgPath.removeDefaultValue();
        String currentValue = this.imgPath.getValue();

        fireImgPathChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getImgPathValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.imgPath;
    }
    public final void fireImgPathChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeImgPathChanged(currentValue);
            firePropertyChange(PROPERTYNAME_IMGPATH, oldValue, currentValue);
            afterImgPathChanged(currentValue);
        }
    }

    public void beforeImgPathChanged( String imgPath)
    { }
    public void afterImgPathChanged( String imgPath)
    { }

    public boolean isImgPathVisible()
    { return true; }

    public boolean isImgPathReadOnly()
    { return false; }

    public boolean isImgPathEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getImgPathPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="7")
        ,@LocalizedTag(language="fr", name="column", value="7")
        ,@LocalizedTag(language="en", name="label", value="image path")
        ,@LocalizedTag(language="fr", name="label", value="chemin image")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getImgPath()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.imgPath.getValue();
    }


    public final void setManoeuvre(String manoeuvre)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.manoeuvre.getValue();
        this.manoeuvre.setValue(manoeuvre);
        String currentValue = this.manoeuvre.getValue();

        fireManoeuvreChange(currentValue, oldValue);
    }
    public final String removeManoeuvre()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.manoeuvre.getValue();
        String removedValue = this.manoeuvre.removeValue();
        String currentValue = this.manoeuvre.getValue();

        fireManoeuvreChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultManoeuvre(String manoeuvre)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.manoeuvre.getValue();
        this.manoeuvre.setDefaultValue(manoeuvre);
        String currentValue = this.manoeuvre.getValue();

        fireManoeuvreChange(currentValue, oldValue);
    }
    
    public final String removeDefaultManoeuvre()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.manoeuvre.getValue();
        String removedValue = this.manoeuvre.removeDefaultValue();
        String currentValue = this.manoeuvre.getValue();

        fireManoeuvreChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getManoeuvreValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.manoeuvre;
    }
    public final void fireManoeuvreChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeManoeuvreChanged(currentValue);
            firePropertyChange(PROPERTYNAME_MANOEUVRE, oldValue, currentValue);
            afterManoeuvreChanged(currentValue);
        }
    }

    public void beforeManoeuvreChanged( String manoeuvre)
    { }
    public void afterManoeuvreChanged( String manoeuvre)
    { }

    public boolean isManoeuvreVisible()
    { return true; }

    public boolean isManoeuvreReadOnly()
    { return false; }

    public boolean isManoeuvreEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getManoeuvrePrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="9")
        ,@LocalizedTag(language="fr", name="column", value="9")
        ,@LocalizedTag(language="fr", name="label", value="manoeuvre")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getManoeuvre()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.manoeuvre.getValue();
    }


    public final void setPlaques(String plaques)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.plaques.getValue();
        this.plaques.setValue(plaques);
        String currentValue = this.plaques.getValue();

        firePlaquesChange(currentValue, oldValue);
    }
    public final String removePlaques()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.plaques.getValue();
        String removedValue = this.plaques.removeValue();
        String currentValue = this.plaques.getValue();

        firePlaquesChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultPlaques(String plaques)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.plaques.getValue();
        this.plaques.setDefaultValue(plaques);
        String currentValue = this.plaques.getValue();

        firePlaquesChange(currentValue, oldValue);
    }
    
    public final String removeDefaultPlaques()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.plaques.getValue();
        String removedValue = this.plaques.removeDefaultValue();
        String currentValue = this.plaques.getValue();

        firePlaquesChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getPlaquesValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.plaques;
    }
    public final void firePlaquesChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforePlaquesChanged(currentValue);
            firePropertyChange(PROPERTYNAME_PLAQUES, oldValue, currentValue);
            afterPlaquesChanged(currentValue);
        }
    }

    public void beforePlaquesChanged( String plaques)
    { }
    public void afterPlaquesChanged( String plaques)
    { }

    public boolean isPlaquesVisible()
    { return true; }

    public boolean isPlaquesReadOnly()
    { return false; }

    public boolean isPlaquesEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getPlaquesPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="17")
        ,@LocalizedTag(language="fr", name="column", value="17")
        ,@LocalizedTag(language="fr", name="label", value="Plaques")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getPlaques()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.plaques.getValue();
    }


    public final void setRail(String rail)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.rail.getValue();
        this.rail.setValue(rail);
        String currentValue = this.rail.getValue();

        fireRailChange(currentValue, oldValue);
    }
    public final String removeRail()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.rail.getValue();
        String removedValue = this.rail.removeValue();
        String currentValue = this.rail.getValue();

        fireRailChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultRail(String rail)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.rail.getValue();
        this.rail.setDefaultValue(rail);
        String currentValue = this.rail.getValue();

        fireRailChange(currentValue, oldValue);
    }
    
    public final String removeDefaultRail()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.rail.getValue();
        String removedValue = this.rail.removeDefaultValue();
        String currentValue = this.rail.getValue();

        fireRailChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getRailValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.rail;
    }
    public final void fireRailChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeRailChanged(currentValue);
            firePropertyChange(PROPERTYNAME_RAIL, oldValue, currentValue);
            afterRailChanged(currentValue);
        }
    }

    public void beforeRailChanged( String rail)
    { }
    public void afterRailChanged( String rail)
    { }

    public boolean isRailVisible()
    { return true; }

    public boolean isRailReadOnly()
    { return false; }

    public boolean isRailEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getRailPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="11")
        ,@LocalizedTag(language="fr", name="column", value="11")
        ,@LocalizedTag(language="fr", name="label", value="rail")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getRail()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.rail.getValue();
    }


    public final void setRed(Integer red)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        Integer oldValue = this.red.getValue();
        this.red.setValue(red);
        Integer currentValue = this.red.getValue();

        fireRedChange(currentValue, oldValue);
    }
    public final Integer removeRed()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        Integer oldValue = this.red.getValue();
        Integer removedValue = this.red.removeValue();
        Integer currentValue = this.red.getValue();

        fireRedChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultRed(Integer red)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        Integer oldValue = this.red.getValue();
        this.red.setDefaultValue(red);
        Integer currentValue = this.red.getValue();

        fireRedChange(currentValue, oldValue);
    }
    
    public final Integer removeDefaultRed()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        Integer oldValue = this.red.getValue();
        Integer removedValue = this.red.removeDefaultValue();
        Integer currentValue = this.red.getValue();

        fireRedChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Integer> getRedValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.red;
    }
    public final void fireRedChange(Integer currentValue, Integer oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeRedChanged(currentValue);
            firePropertyChange(PROPERTYNAME_RED, oldValue, currentValue);
            afterRedChanged(currentValue);
        }
    }

    public void beforeRedChanged( Integer red)
    { }
    public void afterRedChanged( Integer red)
    { }

    public boolean isRedVisible()
    { return true; }

    public boolean isRedReadOnly()
    { return false; }

    public boolean isRedEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getRedPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="3")
        ,@LocalizedTag(language="fr", name="column", value="3")
        ,@LocalizedTag(language="en", name="label", value="red")
        ,@LocalizedTag(language="fr", name="label", value="rouge")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final Integer getRed()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.red.getValue();
    }


    public final void setSupports(String supports)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.supports.getValue();
        this.supports.setValue(supports);
        String currentValue = this.supports.getValue();

        fireSupportsChange(currentValue, oldValue);
    }
    public final String removeSupports()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.supports.getValue();
        String removedValue = this.supports.removeValue();
        String currentValue = this.supports.getValue();

        fireSupportsChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultSupports(String supports)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.supports.getValue();
        this.supports.setDefaultValue(supports);
        String currentValue = this.supports.getValue();

        fireSupportsChange(currentValue, oldValue);
    }
    
    public final String removeDefaultSupports()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.supports.getValue();
        String removedValue = this.supports.removeDefaultValue();
        String currentValue = this.supports.getValue();

        fireSupportsChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getSupportsValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.supports;
    }
    public final void fireSupportsChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeSupportsChanged(currentValue);
            firePropertyChange(PROPERTYNAME_SUPPORTS, oldValue, currentValue);
            afterSupportsChanged(currentValue);
        }
    }

    public void beforeSupportsChanged( String supports)
    { }
    public void afterSupportsChanged( String supports)
    { }

    public boolean isSupportsVisible()
    { return true; }

    public boolean isSupportsReadOnly()
    { return false; }

    public boolean isSupportsEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getSupportsPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="14")
        ,@LocalizedTag(language="fr", name="column", value="14")
        ,@LocalizedTag(language="fr", name="label", value="Supports")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getSupports()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.supports.getValue();
    }


    public final void setTexturePath(String texturePath)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.texturePath.getValue();
        this.texturePath.setValue(texturePath);
        String currentValue = this.texturePath.getValue();

        fireTexturePathChange(currentValue, oldValue);
    }
    public final String removeTexturePath()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.texturePath.getValue();
        String removedValue = this.texturePath.removeValue();
        String currentValue = this.texturePath.getValue();

        fireTexturePathChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultTexturePath(String texturePath)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.texturePath.getValue();
        this.texturePath.setDefaultValue(texturePath);
        String currentValue = this.texturePath.getValue();

        fireTexturePathChange(currentValue, oldValue);
    }
    
    public final String removeDefaultTexturePath()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.texturePath.getValue();
        String removedValue = this.texturePath.removeDefaultValue();
        String currentValue = this.texturePath.getValue();

        fireTexturePathChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getTexturePathValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.texturePath;
    }
    public final void fireTexturePathChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeTexturePathChanged(currentValue);
            firePropertyChange(PROPERTYNAME_TEXTUREPATH, oldValue, currentValue);
            afterTexturePathChanged(currentValue);
        }
    }

    public void beforeTexturePathChanged( String texturePath)
    { }
    public void afterTexturePathChanged( String texturePath)
    { }

    public boolean isTexturePathVisible()
    { return true; }

    public boolean isTexturePathReadOnly()
    { return false; }

    public boolean isTexturePathEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getTexturePathPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="8")
        ,@LocalizedTag(language="fr", name="column", value="8")
        ,@LocalizedTag(language="fr", name="label", value="chemin texture")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getTexturePath()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.texturePath.getValue();
    }


    public final void setChainettes(String chainettes)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.chainettes.getValue();
        this.chainettes.setValue(chainettes);
        String currentValue = this.chainettes.getValue();

        fireChainettesChange(currentValue, oldValue);
    }
    public final String removeChainettes()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.chainettes.getValue();
        String removedValue = this.chainettes.removeValue();
        String currentValue = this.chainettes.getValue();

        fireChainettesChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultChainettes(String chainettes)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.chainettes.getValue();
        this.chainettes.setDefaultValue(chainettes);
        String currentValue = this.chainettes.getValue();

        fireChainettesChange(currentValue, oldValue);
    }
    
    public final String removeDefaultChainettes()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.chainettes.getValue();
        String removedValue = this.chainettes.removeDefaultValue();
        String currentValue = this.chainettes.getValue();

        fireChainettesChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getChainettesValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.chainettes;
    }
    public final void fireChainettesChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeChainettesChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CHAINETTES, oldValue, currentValue);
            afterChainettesChanged(currentValue);
        }
    }

    public void beforeChainettesChanged( String chainettes)
    { }
    public void afterChainettesChanged( String chainettes)
    { }

    public boolean isChainettesVisible()
    { return true; }

    public boolean isChainettesReadOnly()
    { return false; }

    public boolean isChainettesEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getChainettesPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="16")
        ,@LocalizedTag(language="fr", name="column", value="16")
        ,@LocalizedTag(language="fr", name="label", value="Chainettes")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getChainettes()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.chainettes.getValue();
    }



    // Business methods 



    @Override
    public void setDefaultValues()
    { 
        super.setDefaultValues();
    }

    // Bound properties

    public static final String PROPERTYNAME_BANDES127 = "bandes127";  
    public static final String PROPERTYNAME_BANDES89 = "bandes89";  
    public static final String PROPERTYNAME_BLUE = "blue";  
    public static final String PROPERTYNAME_CLIPS = "clips";  
    public static final String PROPERTYNAME_CODE = "code";  
    public static final String PROPERTYNAME_DESCRIPTION = "description";  
    public static final String PROPERTYNAME_EQUERRE = "equerre";  
    public static final String PROPERTYNAME_GREEN = "green";  
    public static final String PROPERTYNAME_IMGPATH = "imgPath";  
    public static final String PROPERTYNAME_MANOEUVRE = "manoeuvre";  
    public static final String PROPERTYNAME_PLAQUES = "plaques";  
    public static final String PROPERTYNAME_RAIL = "rail";  
    public static final String PROPERTYNAME_RED = "red";  
    public static final String PROPERTYNAME_SUPPORTS = "supports";  
    public static final String PROPERTYNAME_TEXTUREPATH = "texturePath";  
    public static final String PROPERTYNAME_CHAINETTES = "chainettes";  
}
