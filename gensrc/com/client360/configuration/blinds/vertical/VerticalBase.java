// generated class, do not edit

package com.client360.configuration.blinds.vertical;

//Plugin V13.1.2
//2013-12-06

// Imports 

public abstract class VerticalBase extends com.netappsid.configuration.blinds.verticalvenitian.VerticalVenitian
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(VerticalBase.class);

    // attributes

    // Constructors

    public VerticalBase(com.netappsid.erp.configurator.Configurable parent) // GENCODE b298fff0-4bff-11e0-b8af-0800200c9a66
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.blinds.vertical.Vertical"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations


    // Business methods 



    @Override
    public void setDefaultValues()
    { 
        super.setDefaultValues();
    }

    @Override
    protected String getSequences()
    {
        return "choixModele,modeleRemise,optionRemise,railSeul,bandesSeules,choixTypePose,choiceEntryMode,dimension,verticalBlades,rail,guide,box,customVisualCompositions,bracketDisplays,paintable,renderingNumber,not setnot set,not set,not set";  
    }

    // Bound properties

}
