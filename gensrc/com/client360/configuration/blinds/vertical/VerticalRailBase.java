// generated class, do not edit

package com.client360.configuration.blinds.vertical;

//Plugin V13.1.2
//2013-12-06

// Imports 

public abstract class VerticalRailBase extends com.netappsid.configuration.blinds.verticalvenitian.VerticalRail
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(VerticalRailBase.class);

    // attributes

    // Constructors

    public VerticalRailBase(com.netappsid.erp.configurator.Configurable parent) // GENCODE b298fff0-4bff-11e0-b8af-0800200c9a66
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.blinds.vertical.VerticalRail"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations


    // Business methods 



    @Override
    public void setDefaultValues()
    { 
        super.setDefaultValues();
    }

    @Override
    protected String getSequences()
    {
        return "ChoixRail,fleche,corde,verConfCouleurRail,height,color,profile,choixTypeManoeuvre,verConfManoeuvre,choixSensRef,choixLongManoeuvre,longueurManoeuvreSaisie,allege,form,leftOperatorDisplay,rightOperatorDisplay,recouvrement,Supports,choixCanto,choixContrepoids";  
    }

    // Bound properties

}
