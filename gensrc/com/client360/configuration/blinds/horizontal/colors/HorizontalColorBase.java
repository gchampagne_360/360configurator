// generated class, do not edit

package com.client360.configuration.blinds.horizontal.colors;

//Plugin V13.1.2
//2013-12-06

// Imports 
import com.client360.configuration.blinds.horizontal.colors.HorizontalChoiceColor;
import com.client360.configuration.blinds.horizontal.colors.enums.HorizontalChoiceColorType;
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.AssociatedComment;
import com.netappsid.erp.configurator.annotations.AssociatedDrawing;
import com.netappsid.erp.configurator.annotations.GlobalProperty;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;
import com.netappsid.erp.configurator.annotations.Mandatory;
import com.netappsid.erp.configurator.annotations.Printable;
import com.netappsid.erp.configurator.annotations.Visible;

public abstract class HorizontalColorBase extends com.client360.configuration.blinds.Color
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(HorizontalColorBase.class);

    // attributes
    private ValueContainer<HorizontalChoiceColor> horizontalChoiceColor = new ValueContainer<HorizontalChoiceColor>(this);
    private ValueContainer<HorizontalChoiceColorType> horizontalChoiceColorType = new ValueContainer<HorizontalChoiceColorType>(this);

    // Constructors

    public HorizontalColorBase(com.netappsid.erp.configurator.Configurable parent) // GENCODE b298fff0-4bff-11e0-b8af-0800200c9a66
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.blinds.horizontal.colors.HorizontalColor"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="fr", name="label", value="Couleur")
        ,@LocalizedTag(language="en", name="label", value="Color")
    })
    @Mandatory("true")
    @GlobalProperty("false")
    @Printable("true")
    @Visible("true")
    @AssociatedComment("false")
    @AssociatedDrawing("false")

    public final HorizontalChoiceColor getHorizontalChoiceColor()
    {   //GENCODE e76ad2a0-c2bd-11e0-962b-0800200c9a66 Value Container getter operations
        return horizontalChoiceColor.getValue();
    }


    public final void setHorizontalChoiceColor(HorizontalChoiceColor horizontalChoiceColor)
    {   // GENCODE 9cda9e30-c2bf-11e0-962b-0800200c9a66 SETTER

        HorizontalChoiceColor oldValue = this.horizontalChoiceColor.getValue();
        this.horizontalChoiceColor.setValue(horizontalChoiceColor);

        HorizontalChoiceColor currentValue = this.horizontalChoiceColor.getValue();

         fireHorizontalChoiceColorChange(currentValue , oldValue);
    }

    public final HorizontalChoiceColor removeHorizontalChoiceColor()
    {   // GENCODE 07d769f0-c376-11e0-962b-0800200c9a66 DEFAULT REMOVE
        
        HorizontalChoiceColor oldValue = this.horizontalChoiceColor.getValue();
        HorizontalChoiceColor removedValue = this.horizontalChoiceColor.removeValue();
        HorizontalChoiceColor currentValue = this.horizontalChoiceColor.getValue();

        if(removedValue != oldValue)
             dispose(removedValue);

        fireHorizontalChoiceColorChange(currentValue, oldValue);
        return removedValue;
    }

    public final void setDefaultHorizontalChoiceColor(HorizontalChoiceColor horizontalChoiceColor)
    {   // GENCODE 07d769f1-c376-11e0-962b-0800200c9a66 DEFAULT SETTER
        HorizontalChoiceColor oldValue = this.horizontalChoiceColor.getValue();
        this.horizontalChoiceColor.setDefaultValue(horizontalChoiceColor);
        HorizontalChoiceColor currentValue = this.horizontalChoiceColor.getValue();

         fireHorizontalChoiceColorChange(currentValue, oldValue);
    }
    
    public final HorizontalChoiceColor removeDefaultHorizontalChoiceColor()
    {   // GENCODE 07d769f2-c376-11e0-962b-0800200c9a66 DEFAULT REMOVE
        
        HorizontalChoiceColor oldValue = this.horizontalChoiceColor.getValue();
        HorizontalChoiceColor removedValue = this.horizontalChoiceColor.removeDefaultValue();
        HorizontalChoiceColor currentValue = this.horizontalChoiceColor.getValue();

        if(removedValue != oldValue)
             dispose(removedValue);

        fireHorizontalChoiceColorChange(currentValue, oldValue);
        return removedValue;
    }

    public final ValueContainerGetter<HorizontalChoiceColor> getHorizontalChoiceColorValueContainer()
    {   //GENCODE 5200e500-c2be-11e0-962b-0800200c9a66 ValueContainer GETTER
        return horizontalChoiceColor;
    }

    public final void fireHorizontalChoiceColorChange(HorizontalChoiceColor currentValue, HorizontalChoiceColor oldValue)
    {   //GENCODE 209a8340-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeHorizontalChoiceColorChanged(currentValue);
            firePropertyChange(PROPERTYNAME_HORIZONTALCHOICECOLOR, oldValue, currentValue);
            afterHorizontalChoiceColorChanged(currentValue);
        }
    }
 
    public void pushHorizontalChoiceColor()
    { }
 
    public void beforeHorizontalChoiceColorChanged( HorizontalChoiceColor horizontalChoiceColor)
    { }
    public void afterHorizontalChoiceColorChanged( HorizontalChoiceColor horizontalChoiceColor)
    { }
 
    public boolean isHorizontalChoiceColorVisible()
    { return true; }

    public boolean isHorizontalChoiceColorReadOnly()
    { return false; }

    public double getHorizontalChoiceColorPrice()
    { return 0d; }

    public boolean isHorizontalChoiceColorEnabled()
    { return true; /* GENCODE bdccd463-4c1c-4ba1-aece-55f4c7cfeae7 */ }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Color type")
        ,@LocalizedTag(language="fr", name="label", value="Type de couleur")
    })
    @Mandatory("true")
    @GlobalProperty("false")
    @Printable("true")
    @Visible("true")
    @AssociatedComment("false")
    @AssociatedDrawing("false")

    public final HorizontalChoiceColorType getHorizontalChoiceColorType()
    {   //GENCODE e76ad2a0-c2bd-11e0-962b-0800200c9a66 Value Container getter operations
        return horizontalChoiceColorType.getValue();
    }


    public final void setHorizontalChoiceColorType(HorizontalChoiceColorType horizontalChoiceColorType)
    {   // GENCODE 9cda9e30-c2bf-11e0-962b-0800200c9a66 SETTER

        HorizontalChoiceColorType oldValue = this.horizontalChoiceColorType.getValue();
        this.horizontalChoiceColorType.setValue(horizontalChoiceColorType);

        HorizontalChoiceColorType currentValue = this.horizontalChoiceColorType.getValue();

         fireHorizontalChoiceColorTypeChange(currentValue , oldValue);
    }

    public final HorizontalChoiceColorType removeHorizontalChoiceColorType()
    {   // GENCODE 07d769f0-c376-11e0-962b-0800200c9a66 DEFAULT REMOVE
        
        HorizontalChoiceColorType oldValue = this.horizontalChoiceColorType.getValue();
        HorizontalChoiceColorType removedValue = this.horizontalChoiceColorType.removeValue();
        HorizontalChoiceColorType currentValue = this.horizontalChoiceColorType.getValue();

        if(removedValue != oldValue)
             dispose(removedValue);

        fireHorizontalChoiceColorTypeChange(currentValue, oldValue);
        return removedValue;
    }

    public final void setDefaultHorizontalChoiceColorType(HorizontalChoiceColorType horizontalChoiceColorType)
    {   // GENCODE 07d769f1-c376-11e0-962b-0800200c9a66 DEFAULT SETTER
        HorizontalChoiceColorType oldValue = this.horizontalChoiceColorType.getValue();
        this.horizontalChoiceColorType.setDefaultValue(horizontalChoiceColorType);
        HorizontalChoiceColorType currentValue = this.horizontalChoiceColorType.getValue();

         fireHorizontalChoiceColorTypeChange(currentValue, oldValue);
    }
    
    public final HorizontalChoiceColorType removeDefaultHorizontalChoiceColorType()
    {   // GENCODE 07d769f2-c376-11e0-962b-0800200c9a66 DEFAULT REMOVE
        
        HorizontalChoiceColorType oldValue = this.horizontalChoiceColorType.getValue();
        HorizontalChoiceColorType removedValue = this.horizontalChoiceColorType.removeDefaultValue();
        HorizontalChoiceColorType currentValue = this.horizontalChoiceColorType.getValue();

        if(removedValue != oldValue)
             dispose(removedValue);

        fireHorizontalChoiceColorTypeChange(currentValue, oldValue);
        return removedValue;
    }

    public final ValueContainerGetter<HorizontalChoiceColorType> getHorizontalChoiceColorTypeValueContainer()
    {   //GENCODE 5200e500-c2be-11e0-962b-0800200c9a66 ValueContainer GETTER
        return horizontalChoiceColorType;
    }

    public final void fireHorizontalChoiceColorTypeChange(HorizontalChoiceColorType currentValue, HorizontalChoiceColorType oldValue)
    {   //GENCODE 209a8340-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeHorizontalChoiceColorTypeChanged(currentValue);
            firePropertyChange(PROPERTYNAME_HORIZONTALCHOICECOLORTYPE, oldValue, currentValue);
            afterHorizontalChoiceColorTypeChanged(currentValue);
        }
    }
 
    public void pushHorizontalChoiceColorType()
    { }
 
    public void beforeHorizontalChoiceColorTypeChanged( HorizontalChoiceColorType horizontalChoiceColorType)
    { }
    public void afterHorizontalChoiceColorTypeChanged( HorizontalChoiceColorType horizontalChoiceColorType)
    { }
 
    public boolean isHorizontalChoiceColorTypeVisible()
    { return true; }

    public boolean isHorizontalChoiceColorTypeReadOnly()
    { return false; }

    public double getHorizontalChoiceColorTypePrice()
    { return 0d; }

    public boolean isHorizontalChoiceColorTypeEnabled()
    { return true; /* GENCODE bdccd463-4c1c-4ba1-aece-55f4c7cfeae7 */ }

    public boolean isHorizontalChoiceColorTypeEnabled(HorizontalChoiceColorType choice)
    { return true; }


    // Business methods 



    public boolean isHorizontalChoiceColorMandatory()
    {
        return true;
    }

    public boolean isHorizontalChoiceColorTypeMandatory()
    {
        return true;
    }

    @Override
    public void setDefaultValues()
    { 
        super.setDefaultValues();
    }

    @Override
    protected String getSequences()
    {
        return "venEnumChoixCouleurType,codeCouleurSpeciale,texturePath,colorChartName,class1";  
    }

    // Bound properties

    public static final String PROPERTYNAME_HORIZONTALCHOICECOLOR = "horizontalChoiceColor";  
    public static final String PROPERTYNAME_HORIZONTALCHOICECOLORTYPE = "horizontalChoiceColorType";  
}
