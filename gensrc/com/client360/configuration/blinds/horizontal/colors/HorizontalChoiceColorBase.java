// generated class, do not edit

package com.client360.configuration.blinds.horizontal.colors;

//Plugin V13.1.2
//2013-12-06

// Imports 
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.AssociatedComment;
import com.netappsid.erp.configurator.annotations.AssociatedDrawing;
import com.netappsid.erp.configurator.annotations.DynamicEnum;
import com.netappsid.erp.configurator.annotations.Filtrable;
import com.netappsid.erp.configurator.annotations.Image;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;
import com.netappsid.erp.configurator.annotations.Visible;

@DynamicEnum(fileName = "VEN_couleurs")
@Image("7")
public abstract class HorizontalChoiceColorBase extends com.netappsid.erp.configurator.Configurable
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(HorizontalChoiceColorBase.class);

    // attributes
    //GENCODE 3703fd90-c29c-11e0-962b-0800200c9a66 Values changed to ValueContainerwith generic inner type
    private ValueContainer<String> code = new ValueContainer<String>(this);
    private ValueContainer<String> description = new ValueContainer<String>(this);
    private ValueContainer<Integer> red = new ValueContainer<Integer>(this);
    private ValueContainer<Integer> green = new ValueContainer<Integer>(this);
    private ValueContainer<Integer> blue = new ValueContainer<Integer>(this);
    private ValueContainer<String> wood = new ValueContainer<String>(this);
    private ValueContainer<String> imgPath = new ValueContainer<String>(this);
    private ValueContainer<String> texture = new ValueContainer<String>(this);
    private ValueContainer<String> lames = new ValueContainer<String>(this);
    private ValueContainer<String> modele = new ValueContainer<String>(this);
    private ValueContainer<String> venUlti25 = new ValueContainer<String>(this);
    private ValueContainer<String> venMet25 = new ValueContainer<String>(this);
    private ValueContainer<String> venSoft35 = new ValueContainer<String>(this);
    private ValueContainer<String> venMidi40 = new ValueContainer<String>(this);
    private ValueContainer<String> venFab28 = new ValueContainer<String>(this);
    private ValueContainer<String> venClass50 = new ValueContainer<String>(this);
    private ValueContainer<String> venRetro50 = new ValueContainer<String>(this);
    private ValueContainer<String> venBso80 = new ValueContainer<String>(this);
    private ValueContainer<String> sousFace25Ulti = new ValueContainer<String>(this);
    private ValueContainer<String> sousFace25 = new ValueContainer<String>(this);
    private ValueContainer<String> sousFace50 = new ValueContainer<String>(this);
    private ValueContainer<String> sousFaceRetro = new ValueContainer<String>(this);
    private ValueContainer<String> galonFaber = new ValueContainer<String>(this);
    private ValueContainer<String> galonTsf = new ValueContainer<String>(this);
    private ValueContainer<String> echelleTsf = new ValueContainer<String>(this);
    private ValueContainer<String> echelle16 = new ValueContainer<String>(this);
    private ValueContainer<String> echelle25 = new ValueContainer<String>(this);
    private ValueContainer<String> echelle50 = new ValueContainer<String>(this);
    private ValueContainer<String> echelle70 = new ValueContainer<String>(this);
    private ValueContainer<String> cadreStorClip = new ValueContainer<String>(this);
    private ValueContainer<String> mecaRetro = new ValueContainer<String>(this);
    private ValueContainer<String> tigeRetro = new ValueContainer<String>(this);
    private ValueContainer<String> coulBsoClass = new ValueContainer<String>(this);
    private ValueContainer<String> coulBsoMono = new ValueContainer<String>(this);
    private ValueContainer<String> sousFaceBso = new ValueContainer<String>(this);
    private ValueContainer<String> bandeauClass = new ValueContainer<String>(this);
    private ValueContainer<String> bandeauMono = new ValueContainer<String>(this);

    // Constructors

    public HorizontalChoiceColorBase(com.netappsid.erp.configurator.Configurable parent) // GENCODE b298fff0-4bff-11e0-b8af-0800200c9a66
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.blinds.horizontal.colors.HorizontalChoiceColor"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    public final void setCode(String code)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.code.getValue();
        this.code.setValue(code);
        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
    }
    public final String removeCode()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.code.getValue();
        String removedValue = this.code.removeValue();
        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultCode(String code)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.code.getValue();
        this.code.setDefaultValue(code);
        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
    }
    
    public final String removeDefaultCode()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.code.getValue();
        String removedValue = this.code.removeDefaultValue();
        String currentValue = this.code.getValue();

        fireCodeChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getCodeValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.code;
    }
    public final void fireCodeChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeCodeChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CODE, oldValue, currentValue);
            afterCodeChanged(currentValue);
        }
    }

    public void beforeCodeChanged( String code)
    { }
    public void afterCodeChanged( String code)
    { }

    public boolean isCodeVisible()
    { return true; }

    public boolean isCodeReadOnly()
    { return false; }

    public boolean isCodeEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getCodePrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="0")
        ,@LocalizedTag(language="fr", name="column", value="0")
        ,@LocalizedTag(language="en", name="label", value="Color code")
        ,@LocalizedTag(language="fr", name="label", value="Code coloris")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getCode()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.code.getValue();
    }


    public final void setDescription(String description)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.description.getValue();
        this.description.setValue(description);
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
    }
    public final String removeDescription()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.description.getValue();
        String removedValue = this.description.removeValue();
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultDescription(String description)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.description.getValue();
        this.description.setDefaultValue(description);
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
    }
    
    public final String removeDefaultDescription()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.description.getValue();
        String removedValue = this.description.removeDefaultValue();
        String currentValue = this.description.getValue();

        fireDescriptionChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getDescriptionValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.description;
    }
    public final void fireDescriptionChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeDescriptionChanged(currentValue);
            firePropertyChange(PROPERTYNAME_DESCRIPTION, oldValue, currentValue);
            afterDescriptionChanged(currentValue);
        }
    }

    public void beforeDescriptionChanged( String description)
    { }
    public void afterDescriptionChanged( String description)
    { }

    public boolean isDescriptionVisible()
    { return true; }

    public boolean isDescriptionReadOnly()
    { return false; }

    public boolean isDescriptionEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getDescriptionPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="2")
        ,@LocalizedTag(language="fr", name="column", value="1")
        ,@LocalizedTag(language="en", name="label", value="Description")
        ,@LocalizedTag(language="fr", name="label", value="Description coloris")
    })
    @Visible("true")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getDescription()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.description.getValue();
    }


    public final void setRed(Integer red)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        Integer oldValue = this.red.getValue();
        this.red.setValue(red);
        Integer currentValue = this.red.getValue();

        fireRedChange(currentValue, oldValue);
    }
    public final Integer removeRed()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        Integer oldValue = this.red.getValue();
        Integer removedValue = this.red.removeValue();
        Integer currentValue = this.red.getValue();

        fireRedChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultRed(Integer red)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        Integer oldValue = this.red.getValue();
        this.red.setDefaultValue(red);
        Integer currentValue = this.red.getValue();

        fireRedChange(currentValue, oldValue);
    }
    
    public final Integer removeDefaultRed()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        Integer oldValue = this.red.getValue();
        Integer removedValue = this.red.removeDefaultValue();
        Integer currentValue = this.red.getValue();

        fireRedChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Integer> getRedValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.red;
    }
    public final void fireRedChange(Integer currentValue, Integer oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeRedChanged(currentValue);
            firePropertyChange(PROPERTYNAME_RED, oldValue, currentValue);
            afterRedChanged(currentValue);
        }
    }

    public void beforeRedChanged( Integer red)
    { }
    public void afterRedChanged( Integer red)
    { }

    public boolean isRedVisible()
    { return true; }

    public boolean isRedReadOnly()
    { return false; }

    public boolean isRedEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getRedPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="3")
        ,@LocalizedTag(language="fr", name="column", value="3")
        ,@LocalizedTag(language="en", name="label", value="Red")
        ,@LocalizedTag(language="fr", name="label", value="Dominante rouge")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final Integer getRed()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.red.getValue();
    }


    public final void setGreen(Integer green)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        Integer oldValue = this.green.getValue();
        this.green.setValue(green);
        Integer currentValue = this.green.getValue();

        fireGreenChange(currentValue, oldValue);
    }
    public final Integer removeGreen()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        Integer oldValue = this.green.getValue();
        Integer removedValue = this.green.removeValue();
        Integer currentValue = this.green.getValue();

        fireGreenChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultGreen(Integer green)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        Integer oldValue = this.green.getValue();
        this.green.setDefaultValue(green);
        Integer currentValue = this.green.getValue();

        fireGreenChange(currentValue, oldValue);
    }
    
    public final Integer removeDefaultGreen()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        Integer oldValue = this.green.getValue();
        Integer removedValue = this.green.removeDefaultValue();
        Integer currentValue = this.green.getValue();

        fireGreenChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Integer> getGreenValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.green;
    }
    public final void fireGreenChange(Integer currentValue, Integer oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeGreenChanged(currentValue);
            firePropertyChange(PROPERTYNAME_GREEN, oldValue, currentValue);
            afterGreenChanged(currentValue);
        }
    }

    public void beforeGreenChanged( Integer green)
    { }
    public void afterGreenChanged( Integer green)
    { }

    public boolean isGreenVisible()
    { return true; }

    public boolean isGreenReadOnly()
    { return false; }

    public boolean isGreenEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getGreenPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="4")
        ,@LocalizedTag(language="fr", name="column", value="4")
        ,@LocalizedTag(language="en", name="label", value="Green")
        ,@LocalizedTag(language="fr", name="label", value="Dominante verte")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final Integer getGreen()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.green.getValue();
    }


    public final void setBlue(Integer blue)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        Integer oldValue = this.blue.getValue();
        this.blue.setValue(blue);
        Integer currentValue = this.blue.getValue();

        fireBlueChange(currentValue, oldValue);
    }
    public final Integer removeBlue()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        Integer oldValue = this.blue.getValue();
        Integer removedValue = this.blue.removeValue();
        Integer currentValue = this.blue.getValue();

        fireBlueChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultBlue(Integer blue)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        Integer oldValue = this.blue.getValue();
        this.blue.setDefaultValue(blue);
        Integer currentValue = this.blue.getValue();

        fireBlueChange(currentValue, oldValue);
    }
    
    public final Integer removeDefaultBlue()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        Integer oldValue = this.blue.getValue();
        Integer removedValue = this.blue.removeDefaultValue();
        Integer currentValue = this.blue.getValue();

        fireBlueChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Integer> getBlueValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.blue;
    }
    public final void fireBlueChange(Integer currentValue, Integer oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeBlueChanged(currentValue);
            firePropertyChange(PROPERTYNAME_BLUE, oldValue, currentValue);
            afterBlueChanged(currentValue);
        }
    }

    public void beforeBlueChanged( Integer blue)
    { }
    public void afterBlueChanged( Integer blue)
    { }

    public boolean isBlueVisible()
    { return true; }

    public boolean isBlueReadOnly()
    { return false; }

    public boolean isBlueEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getBluePrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="5")
        ,@LocalizedTag(language="fr", name="column", value="5")
        ,@LocalizedTag(language="en", name="label", value="Blue")
        ,@LocalizedTag(language="fr", name="label", value="Dominante bleue")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final Integer getBlue()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.blue.getValue();
    }


    public final void setWood(String wood)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.wood.getValue();
        this.wood.setValue(wood);
        String currentValue = this.wood.getValue();

        fireWoodChange(currentValue, oldValue);
    }
    public final String removeWood()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.wood.getValue();
        String removedValue = this.wood.removeValue();
        String currentValue = this.wood.getValue();

        fireWoodChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultWood(String wood)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.wood.getValue();
        this.wood.setDefaultValue(wood);
        String currentValue = this.wood.getValue();

        fireWoodChange(currentValue, oldValue);
    }
    
    public final String removeDefaultWood()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.wood.getValue();
        String removedValue = this.wood.removeDefaultValue();
        String currentValue = this.wood.getValue();

        fireWoodChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getWoodValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.wood;
    }
    public final void fireWoodChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeWoodChanged(currentValue);
            firePropertyChange(PROPERTYNAME_WOOD, oldValue, currentValue);
            afterWoodChanged(currentValue);
        }
    }

    public void beforeWoodChanged( String wood)
    { }
    public void afterWoodChanged( String wood)
    { }

    public boolean isWoodVisible()
    { return true; }

    public boolean isWoodReadOnly()
    { return false; }

    public boolean isWoodEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getWoodPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="6")
        ,@LocalizedTag(language="fr", name="column", value="6")
        ,@LocalizedTag(language="en", name="label", value="Wood")
        ,@LocalizedTag(language="fr", name="label", value="Ton bois")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getWood()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.wood.getValue();
    }


    public final void setImgPath(String imgPath)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.imgPath.getValue();
        this.imgPath.setValue(imgPath);
        String currentValue = this.imgPath.getValue();

        fireImgPathChange(currentValue, oldValue);
    }
    public final String removeImgPath()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.imgPath.getValue();
        String removedValue = this.imgPath.removeValue();
        String currentValue = this.imgPath.getValue();

        fireImgPathChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultImgPath(String imgPath)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.imgPath.getValue();
        this.imgPath.setDefaultValue(imgPath);
        String currentValue = this.imgPath.getValue();

        fireImgPathChange(currentValue, oldValue);
    }
    
    public final String removeDefaultImgPath()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.imgPath.getValue();
        String removedValue = this.imgPath.removeDefaultValue();
        String currentValue = this.imgPath.getValue();

        fireImgPathChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getImgPathValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.imgPath;
    }
    public final void fireImgPathChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeImgPathChanged(currentValue);
            firePropertyChange(PROPERTYNAME_IMGPATH, oldValue, currentValue);
            afterImgPathChanged(currentValue);
        }
    }

    public void beforeImgPathChanged( String imgPath)
    { }
    public void afterImgPathChanged( String imgPath)
    { }

    public boolean isImgPathVisible()
    { return true; }

    public boolean isImgPathReadOnly()
    { return false; }

    public boolean isImgPathEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getImgPathPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="7")
        ,@LocalizedTag(language="fr", name="column", value="7")
        ,@LocalizedTag(language="en", name="label", value="Pictures folder")
        ,@LocalizedTag(language="fr", name="label", value="R�pertoire stockage des images")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getImgPath()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.imgPath.getValue();
    }


    public final void setTexture(String texture)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.texture.getValue();
        this.texture.setValue(texture);
        String currentValue = this.texture.getValue();

        fireTextureChange(currentValue, oldValue);
    }
    public final String removeTexture()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.texture.getValue();
        String removedValue = this.texture.removeValue();
        String currentValue = this.texture.getValue();

        fireTextureChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultTexture(String texture)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.texture.getValue();
        this.texture.setDefaultValue(texture);
        String currentValue = this.texture.getValue();

        fireTextureChange(currentValue, oldValue);
    }
    
    public final String removeDefaultTexture()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.texture.getValue();
        String removedValue = this.texture.removeDefaultValue();
        String currentValue = this.texture.getValue();

        fireTextureChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getTextureValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.texture;
    }
    public final void fireTextureChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeTextureChanged(currentValue);
            firePropertyChange(PROPERTYNAME_TEXTURE, oldValue, currentValue);
            afterTextureChanged(currentValue);
        }
    }

    public void beforeTextureChanged( String texture)
    { }
    public void afterTextureChanged( String texture)
    { }

    public boolean isTextureVisible()
    { return true; }

    public boolean isTextureReadOnly()
    { return false; }

    public boolean isTextureEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getTexturePrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="8")
        ,@LocalizedTag(language="fr", name="column", value="8")
        ,@LocalizedTag(language="en", name="label", value="Texture")
        ,@LocalizedTag(language="fr", name="label", value="Texture")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getTexture()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.texture.getValue();
    }


    public final void setLames(String lames)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.lames.getValue();
        this.lames.setValue(lames);
        String currentValue = this.lames.getValue();

        fireLamesChange(currentValue, oldValue);
    }
    public final String removeLames()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.lames.getValue();
        String removedValue = this.lames.removeValue();
        String currentValue = this.lames.getValue();

        fireLamesChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultLames(String lames)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.lames.getValue();
        this.lames.setDefaultValue(lames);
        String currentValue = this.lames.getValue();

        fireLamesChange(currentValue, oldValue);
    }
    
    public final String removeDefaultLames()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.lames.getValue();
        String removedValue = this.lames.removeDefaultValue();
        String currentValue = this.lames.getValue();

        fireLamesChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getLamesValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.lames;
    }
    public final void fireLamesChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeLamesChanged(currentValue);
            firePropertyChange(PROPERTYNAME_LAMES, oldValue, currentValue);
            afterLamesChanged(currentValue);
        }
    }

    public void beforeLamesChanged( String lames)
    { }
    public void afterLamesChanged( String lames)
    { }

    public boolean isLamesVisible()
    { return true; }

    public boolean isLamesReadOnly()
    { return false; }

    public boolean isLamesEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getLamesPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="9")
        ,@LocalizedTag(language="fr", name="column", value="9")
        ,@LocalizedTag(language="en", name="label", value="Blinds type")
        ,@LocalizedTag(language="fr", name="label", value="Type de lames")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getLames()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.lames.getValue();
    }


    public final void setModele(String modele)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.modele.getValue();
        this.modele.setValue(modele);
        String currentValue = this.modele.getValue();

        fireModeleChange(currentValue, oldValue);
    }
    public final String removeModele()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.modele.getValue();
        String removedValue = this.modele.removeValue();
        String currentValue = this.modele.getValue();

        fireModeleChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultModele(String modele)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.modele.getValue();
        this.modele.setDefaultValue(modele);
        String currentValue = this.modele.getValue();

        fireModeleChange(currentValue, oldValue);
    }
    
    public final String removeDefaultModele()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.modele.getValue();
        String removedValue = this.modele.removeDefaultValue();
        String currentValue = this.modele.getValue();

        fireModeleChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getModeleValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.modele;
    }
    public final void fireModeleChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeModeleChanged(currentValue);
            firePropertyChange(PROPERTYNAME_MODELE, oldValue, currentValue);
            afterModeleChanged(currentValue);
        }
    }

    public void beforeModeleChanged( String modele)
    { }
    public void afterModeleChanged( String modele)
    { }

    public boolean isModeleVisible()
    { return true; }

    public boolean isModeleReadOnly()
    { return false; }

    public boolean isModeleEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getModelePrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="10")
        ,@LocalizedTag(language="fr", name="column", value="10")
        ,@LocalizedTag(language="en", name="label", value="Venetian model")
        ,@LocalizedTag(language="fr", name="label", value="Mod�le de v�nitien")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getModele()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.modele.getValue();
    }


    public final void setVenUlti25(String venUlti25)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.venUlti25.getValue();
        this.venUlti25.setValue(venUlti25);
        String currentValue = this.venUlti25.getValue();

        fireVenUlti25Change(currentValue, oldValue);
    }
    public final String removeVenUlti25()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.venUlti25.getValue();
        String removedValue = this.venUlti25.removeValue();
        String currentValue = this.venUlti25.getValue();

        fireVenUlti25Change(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultVenUlti25(String venUlti25)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.venUlti25.getValue();
        this.venUlti25.setDefaultValue(venUlti25);
        String currentValue = this.venUlti25.getValue();

        fireVenUlti25Change(currentValue, oldValue);
    }
    
    public final String removeDefaultVenUlti25()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.venUlti25.getValue();
        String removedValue = this.venUlti25.removeDefaultValue();
        String currentValue = this.venUlti25.getValue();

        fireVenUlti25Change(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getVenUlti25ValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.venUlti25;
    }
    public final void fireVenUlti25Change(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeVenUlti25Changed(currentValue);
            firePropertyChange(PROPERTYNAME_VENULTI25, oldValue, currentValue);
            afterVenUlti25Changed(currentValue);
        }
    }

    public void beforeVenUlti25Changed( String venUlti25)
    { }
    public void afterVenUlti25Changed( String venUlti25)
    { }

    public boolean isVenUlti25Visible()
    { return true; }

    public boolean isVenUlti25ReadOnly()
    { return false; }

    public boolean isVenUlti25Enabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getVenUlti25Price()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="11")
        ,@LocalizedTag(language="fr", name="column", value="11")
        ,@LocalizedTag(language="en", name="label", value="Headrail ultimate 25X25")
        ,@LocalizedTag(language="fr", name="label", value="Boitier ultimate 25X25")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getVenUlti25()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.venUlti25.getValue();
    }


    public final void setVenMet25(String venMet25)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.venMet25.getValue();
        this.venMet25.setValue(venMet25);
        String currentValue = this.venMet25.getValue();

        fireVenMet25Change(currentValue, oldValue);
    }
    public final String removeVenMet25()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.venMet25.getValue();
        String removedValue = this.venMet25.removeValue();
        String currentValue = this.venMet25.getValue();

        fireVenMet25Change(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultVenMet25(String venMet25)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.venMet25.getValue();
        this.venMet25.setDefaultValue(venMet25);
        String currentValue = this.venMet25.getValue();

        fireVenMet25Change(currentValue, oldValue);
    }
    
    public final String removeDefaultVenMet25()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.venMet25.getValue();
        String removedValue = this.venMet25.removeDefaultValue();
        String currentValue = this.venMet25.getValue();

        fireVenMet25Change(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getVenMet25ValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.venMet25;
    }
    public final void fireVenMet25Change(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeVenMet25Changed(currentValue);
            firePropertyChange(PROPERTYNAME_VENMET25, oldValue, currentValue);
            afterVenMet25Changed(currentValue);
        }
    }

    public void beforeVenMet25Changed( String venMet25)
    { }
    public void afterVenMet25Changed( String venMet25)
    { }

    public boolean isVenMet25Visible()
    { return true; }

    public boolean isVenMet25ReadOnly()
    { return false; }

    public boolean isVenMet25Enabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getVenMet25Price()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="12")
        ,@LocalizedTag(language="fr", name="column", value="12")
        ,@LocalizedTag(language="en", name="label", value="Headrail metalet 25X25")
        ,@LocalizedTag(language="fr", name="label", value="Boitier metalet 25X25")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getVenMet25()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.venMet25.getValue();
    }


    public final void setVenSoft35(String venSoft35)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.venSoft35.getValue();
        this.venSoft35.setValue(venSoft35);
        String currentValue = this.venSoft35.getValue();

        fireVenSoft35Change(currentValue, oldValue);
    }
    public final String removeVenSoft35()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.venSoft35.getValue();
        String removedValue = this.venSoft35.removeValue();
        String currentValue = this.venSoft35.getValue();

        fireVenSoft35Change(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultVenSoft35(String venSoft35)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.venSoft35.getValue();
        this.venSoft35.setDefaultValue(venSoft35);
        String currentValue = this.venSoft35.getValue();

        fireVenSoft35Change(currentValue, oldValue);
    }
    
    public final String removeDefaultVenSoft35()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.venSoft35.getValue();
        String removedValue = this.venSoft35.removeDefaultValue();
        String currentValue = this.venSoft35.getValue();

        fireVenSoft35Change(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getVenSoft35ValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.venSoft35;
    }
    public final void fireVenSoft35Change(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeVenSoft35Changed(currentValue);
            firePropertyChange(PROPERTYNAME_VENSOFT35, oldValue, currentValue);
            afterVenSoft35Changed(currentValue);
        }
    }

    public void beforeVenSoft35Changed( String venSoft35)
    { }
    public void afterVenSoft35Changed( String venSoft35)
    { }

    public boolean isVenSoft35Visible()
    { return true; }

    public boolean isVenSoft35ReadOnly()
    { return false; }

    public boolean isVenSoft35Enabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getVenSoft35Price()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="13")
        ,@LocalizedTag(language="fr", name="column", value="13")
        ,@LocalizedTag(language="en", name="label", value="Headrail softline 35X27")
        ,@LocalizedTag(language="fr", name="label", value="Boitier softline 35X27")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getVenSoft35()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.venSoft35.getValue();
    }


    public final void setVenMidi40(String venMidi40)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.venMidi40.getValue();
        this.venMidi40.setValue(venMidi40);
        String currentValue = this.venMidi40.getValue();

        fireVenMidi40Change(currentValue, oldValue);
    }
    public final String removeVenMidi40()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.venMidi40.getValue();
        String removedValue = this.venMidi40.removeValue();
        String currentValue = this.venMidi40.getValue();

        fireVenMidi40Change(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultVenMidi40(String venMidi40)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.venMidi40.getValue();
        this.venMidi40.setDefaultValue(venMidi40);
        String currentValue = this.venMidi40.getValue();

        fireVenMidi40Change(currentValue, oldValue);
    }
    
    public final String removeDefaultVenMidi40()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.venMidi40.getValue();
        String removedValue = this.venMidi40.removeDefaultValue();
        String currentValue = this.venMidi40.getValue();

        fireVenMidi40Change(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getVenMidi40ValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.venMidi40;
    }
    public final void fireVenMidi40Change(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeVenMidi40Changed(currentValue);
            firePropertyChange(PROPERTYNAME_VENMIDI40, oldValue, currentValue);
            afterVenMidi40Changed(currentValue);
        }
    }

    public void beforeVenMidi40Changed( String venMidi40)
    { }
    public void afterVenMidi40Changed( String venMidi40)
    { }

    public boolean isVenMidi40Visible()
    { return true; }

    public boolean isVenMidi40ReadOnly()
    { return false; }

    public boolean isVenMidi40Enabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getVenMidi40Price()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="14")
        ,@LocalizedTag(language="fr", name="column", value="14")
        ,@LocalizedTag(language="en", name="label", value="Headrail midimatic 40X40")
        ,@LocalizedTag(language="fr", name="label", value="Boitier midimatic 40X40")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getVenMidi40()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.venMidi40.getValue();
    }


    public final void setVenFab28(String venFab28)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.venFab28.getValue();
        this.venFab28.setValue(venFab28);
        String currentValue = this.venFab28.getValue();

        fireVenFab28Change(currentValue, oldValue);
    }
    public final String removeVenFab28()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.venFab28.getValue();
        String removedValue = this.venFab28.removeValue();
        String currentValue = this.venFab28.getValue();

        fireVenFab28Change(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultVenFab28(String venFab28)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.venFab28.getValue();
        this.venFab28.setDefaultValue(venFab28);
        String currentValue = this.venFab28.getValue();

        fireVenFab28Change(currentValue, oldValue);
    }
    
    public final String removeDefaultVenFab28()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.venFab28.getValue();
        String removedValue = this.venFab28.removeDefaultValue();
        String currentValue = this.venFab28.getValue();

        fireVenFab28Change(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getVenFab28ValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.venFab28;
    }
    public final void fireVenFab28Change(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeVenFab28Changed(currentValue);
            firePropertyChange(PROPERTYNAME_VENFAB28, oldValue, currentValue);
            afterVenFab28Changed(currentValue);
        }
    }

    public void beforeVenFab28Changed( String venFab28)
    { }
    public void afterVenFab28Changed( String venFab28)
    { }

    public boolean isVenFab28Visible()
    { return true; }

    public boolean isVenFab28ReadOnly()
    { return false; }

    public boolean isVenFab28Enabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getVenFab28Price()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="15")
        ,@LocalizedTag(language="fr", name="column", value="15")
        ,@LocalizedTag(language="en", name="label", value="Headrail faber 61X28")
        ,@LocalizedTag(language="fr", name="label", value="Boitier faber 61X28")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getVenFab28()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.venFab28.getValue();
    }


    public final void setVenClass50(String venClass50)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.venClass50.getValue();
        this.venClass50.setValue(venClass50);
        String currentValue = this.venClass50.getValue();

        fireVenClass50Change(currentValue, oldValue);
    }
    public final String removeVenClass50()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.venClass50.getValue();
        String removedValue = this.venClass50.removeValue();
        String currentValue = this.venClass50.getValue();

        fireVenClass50Change(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultVenClass50(String venClass50)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.venClass50.getValue();
        this.venClass50.setDefaultValue(venClass50);
        String currentValue = this.venClass50.getValue();

        fireVenClass50Change(currentValue, oldValue);
    }
    
    public final String removeDefaultVenClass50()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.venClass50.getValue();
        String removedValue = this.venClass50.removeDefaultValue();
        String currentValue = this.venClass50.getValue();

        fireVenClass50Change(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getVenClass50ValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.venClass50;
    }
    public final void fireVenClass50Change(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeVenClass50Changed(currentValue);
            firePropertyChange(PROPERTYNAME_VENCLASS50, oldValue, currentValue);
            afterVenClass50Changed(currentValue);
        }
    }

    public void beforeVenClass50Changed( String venClass50)
    { }
    public void afterVenClass50Changed( String venClass50)
    { }

    public boolean isVenClass50Visible()
    { return true; }

    public boolean isVenClass50ReadOnly()
    { return false; }

    public boolean isVenClass50Enabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getVenClass50Price()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="16")
        ,@LocalizedTag(language="fr", name="column", value="16")
        ,@LocalizedTag(language="en", name="label", value="Headrail classique 51X57")
        ,@LocalizedTag(language="fr", name="label", value="Boitier classique 51X57")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getVenClass50()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.venClass50.getValue();
    }


    public final void setVenRetro50(String venRetro50)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.venRetro50.getValue();
        this.venRetro50.setValue(venRetro50);
        String currentValue = this.venRetro50.getValue();

        fireVenRetro50Change(currentValue, oldValue);
    }
    public final String removeVenRetro50()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.venRetro50.getValue();
        String removedValue = this.venRetro50.removeValue();
        String currentValue = this.venRetro50.getValue();

        fireVenRetro50Change(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultVenRetro50(String venRetro50)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.venRetro50.getValue();
        this.venRetro50.setDefaultValue(venRetro50);
        String currentValue = this.venRetro50.getValue();

        fireVenRetro50Change(currentValue, oldValue);
    }
    
    public final String removeDefaultVenRetro50()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.venRetro50.getValue();
        String removedValue = this.venRetro50.removeDefaultValue();
        String currentValue = this.venRetro50.getValue();

        fireVenRetro50Change(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getVenRetro50ValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.venRetro50;
    }
    public final void fireVenRetro50Change(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeVenRetro50Changed(currentValue);
            firePropertyChange(PROPERTYNAME_VENRETRO50, oldValue, currentValue);
            afterVenRetro50Changed(currentValue);
        }
    }

    public void beforeVenRetro50Changed( String venRetro50)
    { }
    public void afterVenRetro50Changed( String venRetro50)
    { }

    public boolean isVenRetro50Visible()
    { return true; }

    public boolean isVenRetro50ReadOnly()
    { return false; }

    public boolean isVenRetro50Enabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getVenRetro50Price()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="17")
        ,@LocalizedTag(language="fr", name="column", value="17")
        ,@LocalizedTag(language="en", name="label", value="Headrail retro 50X65")
        ,@LocalizedTag(language="fr", name="label", value="Boitier r�tro 50X65")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getVenRetro50()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.venRetro50.getValue();
    }


    public final void setVenBso80(String venBso80)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.venBso80.getValue();
        this.venBso80.setValue(venBso80);
        String currentValue = this.venBso80.getValue();

        fireVenBso80Change(currentValue, oldValue);
    }
    public final String removeVenBso80()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.venBso80.getValue();
        String removedValue = this.venBso80.removeValue();
        String currentValue = this.venBso80.getValue();

        fireVenBso80Change(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultVenBso80(String venBso80)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.venBso80.getValue();
        this.venBso80.setDefaultValue(venBso80);
        String currentValue = this.venBso80.getValue();

        fireVenBso80Change(currentValue, oldValue);
    }
    
    public final String removeDefaultVenBso80()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.venBso80.getValue();
        String removedValue = this.venBso80.removeDefaultValue();
        String currentValue = this.venBso80.getValue();

        fireVenBso80Change(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getVenBso80ValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.venBso80;
    }
    public final void fireVenBso80Change(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeVenBso80Changed(currentValue);
            firePropertyChange(PROPERTYNAME_VENBSO80, oldValue, currentValue);
            afterVenBso80Changed(currentValue);
        }
    }

    public void beforeVenBso80Changed( String venBso80)
    { }
    public void afterVenBso80Changed( String venBso80)
    { }

    public boolean isVenBso80Visible()
    { return true; }

    public boolean isVenBso80ReadOnly()
    { return false; }

    public boolean isVenBso80Enabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getVenBso80Price()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="18")
        ,@LocalizedTag(language="fr", name="column", value="18")
        ,@LocalizedTag(language="en", name="label", value="Headrail 80X65")
        ,@LocalizedTag(language="fr", name="label", value="Boitier 80X65")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getVenBso80()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.venBso80.getValue();
    }


    public final void setSousFace25Ulti(String sousFace25Ulti)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.sousFace25Ulti.getValue();
        this.sousFace25Ulti.setValue(sousFace25Ulti);
        String currentValue = this.sousFace25Ulti.getValue();

        fireSousFace25UltiChange(currentValue, oldValue);
    }
    public final String removeSousFace25Ulti()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.sousFace25Ulti.getValue();
        String removedValue = this.sousFace25Ulti.removeValue();
        String currentValue = this.sousFace25Ulti.getValue();

        fireSousFace25UltiChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultSousFace25Ulti(String sousFace25Ulti)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.sousFace25Ulti.getValue();
        this.sousFace25Ulti.setDefaultValue(sousFace25Ulti);
        String currentValue = this.sousFace25Ulti.getValue();

        fireSousFace25UltiChange(currentValue, oldValue);
    }
    
    public final String removeDefaultSousFace25Ulti()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.sousFace25Ulti.getValue();
        String removedValue = this.sousFace25Ulti.removeDefaultValue();
        String currentValue = this.sousFace25Ulti.getValue();

        fireSousFace25UltiChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getSousFace25UltiValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.sousFace25Ulti;
    }
    public final void fireSousFace25UltiChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeSousFace25UltiChanged(currentValue);
            firePropertyChange(PROPERTYNAME_SOUSFACE25ULTI, oldValue, currentValue);
            afterSousFace25UltiChanged(currentValue);
        }
    }

    public void beforeSousFace25UltiChanged( String sousFace25Ulti)
    { }
    public void afterSousFace25UltiChanged( String sousFace25Ulti)
    { }

    public boolean isSousFace25UltiVisible()
    { return true; }

    public boolean isSousFace25UltiReadOnly()
    { return false; }

    public boolean isSousFace25UltiEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getSousFace25UltiPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="19")
        ,@LocalizedTag(language="fr", name="column", value="19")
        ,@LocalizedTag(language="en", name="label", value="Lather 25 mm Ultimate")
        ,@LocalizedTag(language="fr", name="label", value="Lame finale 25 mm Ultimate")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getSousFace25Ulti()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.sousFace25Ulti.getValue();
    }


    public final void setSousFace25(String sousFace25)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.sousFace25.getValue();
        this.sousFace25.setValue(sousFace25);
        String currentValue = this.sousFace25.getValue();

        fireSousFace25Change(currentValue, oldValue);
    }
    public final String removeSousFace25()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.sousFace25.getValue();
        String removedValue = this.sousFace25.removeValue();
        String currentValue = this.sousFace25.getValue();

        fireSousFace25Change(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultSousFace25(String sousFace25)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.sousFace25.getValue();
        this.sousFace25.setDefaultValue(sousFace25);
        String currentValue = this.sousFace25.getValue();

        fireSousFace25Change(currentValue, oldValue);
    }
    
    public final String removeDefaultSousFace25()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.sousFace25.getValue();
        String removedValue = this.sousFace25.removeDefaultValue();
        String currentValue = this.sousFace25.getValue();

        fireSousFace25Change(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getSousFace25ValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.sousFace25;
    }
    public final void fireSousFace25Change(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeSousFace25Changed(currentValue);
            firePropertyChange(PROPERTYNAME_SOUSFACE25, oldValue, currentValue);
            afterSousFace25Changed(currentValue);
        }
    }

    public void beforeSousFace25Changed( String sousFace25)
    { }
    public void afterSousFace25Changed( String sousFace25)
    { }

    public boolean isSousFace25Visible()
    { return true; }

    public boolean isSousFace25ReadOnly()
    { return false; }

    public boolean isSousFace25Enabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getSousFace25Price()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="20")
        ,@LocalizedTag(language="fr", name="column", value="20")
        ,@LocalizedTag(language="en", name="label", value="Lather 25 mm")
        ,@LocalizedTag(language="fr", name="label", value="Lame finale 25 mm")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getSousFace25()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.sousFace25.getValue();
    }


    public final void setSousFace50(String sousFace50)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.sousFace50.getValue();
        this.sousFace50.setValue(sousFace50);
        String currentValue = this.sousFace50.getValue();

        fireSousFace50Change(currentValue, oldValue);
    }
    public final String removeSousFace50()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.sousFace50.getValue();
        String removedValue = this.sousFace50.removeValue();
        String currentValue = this.sousFace50.getValue();

        fireSousFace50Change(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultSousFace50(String sousFace50)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.sousFace50.getValue();
        this.sousFace50.setDefaultValue(sousFace50);
        String currentValue = this.sousFace50.getValue();

        fireSousFace50Change(currentValue, oldValue);
    }
    
    public final String removeDefaultSousFace50()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.sousFace50.getValue();
        String removedValue = this.sousFace50.removeDefaultValue();
        String currentValue = this.sousFace50.getValue();

        fireSousFace50Change(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getSousFace50ValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.sousFace50;
    }
    public final void fireSousFace50Change(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeSousFace50Changed(currentValue);
            firePropertyChange(PROPERTYNAME_SOUSFACE50, oldValue, currentValue);
            afterSousFace50Changed(currentValue);
        }
    }

    public void beforeSousFace50Changed( String sousFace50)
    { }
    public void afterSousFace50Changed( String sousFace50)
    { }

    public boolean isSousFace50Visible()
    { return true; }

    public boolean isSousFace50ReadOnly()
    { return false; }

    public boolean isSousFace50Enabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getSousFace50Price()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="21")
        ,@LocalizedTag(language="fr", name="column", value="21")
        ,@LocalizedTag(language="en", name="label", value="Lather 50 mm")
        ,@LocalizedTag(language="fr", name="label", value="Lame finale 50 mm")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getSousFace50()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.sousFace50.getValue();
    }


    public final void setSousFaceRetro(String sousFaceRetro)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.sousFaceRetro.getValue();
        this.sousFaceRetro.setValue(sousFaceRetro);
        String currentValue = this.sousFaceRetro.getValue();

        fireSousFaceRetroChange(currentValue, oldValue);
    }
    public final String removeSousFaceRetro()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.sousFaceRetro.getValue();
        String removedValue = this.sousFaceRetro.removeValue();
        String currentValue = this.sousFaceRetro.getValue();

        fireSousFaceRetroChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultSousFaceRetro(String sousFaceRetro)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.sousFaceRetro.getValue();
        this.sousFaceRetro.setDefaultValue(sousFaceRetro);
        String currentValue = this.sousFaceRetro.getValue();

        fireSousFaceRetroChange(currentValue, oldValue);
    }
    
    public final String removeDefaultSousFaceRetro()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.sousFaceRetro.getValue();
        String removedValue = this.sousFaceRetro.removeDefaultValue();
        String currentValue = this.sousFaceRetro.getValue();

        fireSousFaceRetroChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getSousFaceRetroValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.sousFaceRetro;
    }
    public final void fireSousFaceRetroChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeSousFaceRetroChanged(currentValue);
            firePropertyChange(PROPERTYNAME_SOUSFACERETRO, oldValue, currentValue);
            afterSousFaceRetroChanged(currentValue);
        }
    }

    public void beforeSousFaceRetroChanged( String sousFaceRetro)
    { }
    public void afterSousFaceRetroChanged( String sousFaceRetro)
    { }

    public boolean isSousFaceRetroVisible()
    { return true; }

    public boolean isSousFaceRetroReadOnly()
    { return false; }

    public boolean isSousFaceRetroEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getSousFaceRetroPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="22")
        ,@LocalizedTag(language="fr", name="column", value="22")
        ,@LocalizedTag(language="en", name="label", value="Lather 50 mm Retro")
        ,@LocalizedTag(language="fr", name="label", value="Lame finale 50 mm R�tro")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getSousFaceRetro()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.sousFaceRetro.getValue();
    }


    public final void setGalonFaber(String galonFaber)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.galonFaber.getValue();
        this.galonFaber.setValue(galonFaber);
        String currentValue = this.galonFaber.getValue();

        fireGalonFaberChange(currentValue, oldValue);
    }
    public final String removeGalonFaber()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.galonFaber.getValue();
        String removedValue = this.galonFaber.removeValue();
        String currentValue = this.galonFaber.getValue();

        fireGalonFaberChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultGalonFaber(String galonFaber)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.galonFaber.getValue();
        this.galonFaber.setDefaultValue(galonFaber);
        String currentValue = this.galonFaber.getValue();

        fireGalonFaberChange(currentValue, oldValue);
    }
    
    public final String removeDefaultGalonFaber()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.galonFaber.getValue();
        String removedValue = this.galonFaber.removeDefaultValue();
        String currentValue = this.galonFaber.getValue();

        fireGalonFaberChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getGalonFaberValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.galonFaber;
    }
    public final void fireGalonFaberChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeGalonFaberChanged(currentValue);
            firePropertyChange(PROPERTYNAME_GALONFABER, oldValue, currentValue);
            afterGalonFaberChanged(currentValue);
        }
    }

    public void beforeGalonFaberChanged( String galonFaber)
    { }
    public void afterGalonFaberChanged( String galonFaber)
    { }

    public boolean isGalonFaberVisible()
    { return true; }

    public boolean isGalonFaberReadOnly()
    { return false; }

    public boolean isGalonFaberEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getGalonFaberPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="23")
        ,@LocalizedTag(language="fr", name="column", value="23")
        ,@LocalizedTag(language="en", name="label", value="Faber ribbon")
        ,@LocalizedTag(language="fr", name="label", value="Galon Faber")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getGalonFaber()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.galonFaber.getValue();
    }


    public final void setGalonTsf(String galonTsf)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.galonTsf.getValue();
        this.galonTsf.setValue(galonTsf);
        String currentValue = this.galonTsf.getValue();

        fireGalonTsfChange(currentValue, oldValue);
    }
    public final String removeGalonTsf()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.galonTsf.getValue();
        String removedValue = this.galonTsf.removeValue();
        String currentValue = this.galonTsf.getValue();

        fireGalonTsfChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultGalonTsf(String galonTsf)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.galonTsf.getValue();
        this.galonTsf.setDefaultValue(galonTsf);
        String currentValue = this.galonTsf.getValue();

        fireGalonTsfChange(currentValue, oldValue);
    }
    
    public final String removeDefaultGalonTsf()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.galonTsf.getValue();
        String removedValue = this.galonTsf.removeDefaultValue();
        String currentValue = this.galonTsf.getValue();

        fireGalonTsfChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getGalonTsfValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.galonTsf;
    }
    public final void fireGalonTsfChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeGalonTsfChanged(currentValue);
            firePropertyChange(PROPERTYNAME_GALONTSF, oldValue, currentValue);
            afterGalonTsfChanged(currentValue);
        }
    }

    public void beforeGalonTsfChanged( String galonTsf)
    { }
    public void afterGalonTsfChanged( String galonTsf)
    { }

    public boolean isGalonTsfVisible()
    { return true; }

    public boolean isGalonTsfReadOnly()
    { return false; }

    public boolean isGalonTsfEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getGalonTsfPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="24")
        ,@LocalizedTag(language="fr", name="column", value="24")
        ,@LocalizedTag(language="en", name="label", value="Tsf ribbon")
        ,@LocalizedTag(language="fr", name="label", value="Galon Tsf")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getGalonTsf()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.galonTsf.getValue();
    }


    public final void setEchelleTsf(String echelleTsf)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.echelleTsf.getValue();
        this.echelleTsf.setValue(echelleTsf);
        String currentValue = this.echelleTsf.getValue();

        fireEchelleTsfChange(currentValue, oldValue);
    }
    public final String removeEchelleTsf()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.echelleTsf.getValue();
        String removedValue = this.echelleTsf.removeValue();
        String currentValue = this.echelleTsf.getValue();

        fireEchelleTsfChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultEchelleTsf(String echelleTsf)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.echelleTsf.getValue();
        this.echelleTsf.setDefaultValue(echelleTsf);
        String currentValue = this.echelleTsf.getValue();

        fireEchelleTsfChange(currentValue, oldValue);
    }
    
    public final String removeDefaultEchelleTsf()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.echelleTsf.getValue();
        String removedValue = this.echelleTsf.removeDefaultValue();
        String currentValue = this.echelleTsf.getValue();

        fireEchelleTsfChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getEchelleTsfValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.echelleTsf;
    }
    public final void fireEchelleTsfChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeEchelleTsfChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ECHELLETSF, oldValue, currentValue);
            afterEchelleTsfChanged(currentValue);
        }
    }

    public void beforeEchelleTsfChanged( String echelleTsf)
    { }
    public void afterEchelleTsfChanged( String echelleTsf)
    { }

    public boolean isEchelleTsfVisible()
    { return true; }

    public boolean isEchelleTsfReadOnly()
    { return false; }

    public boolean isEchelleTsfEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getEchelleTsfPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="25")
        ,@LocalizedTag(language="fr", name="column", value="25")
        ,@LocalizedTag(language="en", name="label", value="Tsf ladder")
        ,@LocalizedTag(language="fr", name="label", value="Echelle Tsf")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getEchelleTsf()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.echelleTsf.getValue();
    }


    public final void setEchelle16(String echelle16)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.echelle16.getValue();
        this.echelle16.setValue(echelle16);
        String currentValue = this.echelle16.getValue();

        fireEchelle16Change(currentValue, oldValue);
    }
    public final String removeEchelle16()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.echelle16.getValue();
        String removedValue = this.echelle16.removeValue();
        String currentValue = this.echelle16.getValue();

        fireEchelle16Change(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultEchelle16(String echelle16)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.echelle16.getValue();
        this.echelle16.setDefaultValue(echelle16);
        String currentValue = this.echelle16.getValue();

        fireEchelle16Change(currentValue, oldValue);
    }
    
    public final String removeDefaultEchelle16()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.echelle16.getValue();
        String removedValue = this.echelle16.removeDefaultValue();
        String currentValue = this.echelle16.getValue();

        fireEchelle16Change(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getEchelle16ValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.echelle16;
    }
    public final void fireEchelle16Change(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeEchelle16Changed(currentValue);
            firePropertyChange(PROPERTYNAME_ECHELLE16, oldValue, currentValue);
            afterEchelle16Changed(currentValue);
        }
    }

    public void beforeEchelle16Changed( String echelle16)
    { }
    public void afterEchelle16Changed( String echelle16)
    { }

    public boolean isEchelle16Visible()
    { return true; }

    public boolean isEchelle16ReadOnly()
    { return false; }

    public boolean isEchelle16Enabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getEchelle16Price()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="26")
        ,@LocalizedTag(language="fr", name="column", value="26")
        ,@LocalizedTag(language="en", name="label", value="16 mm ladder")
        ,@LocalizedTag(language="fr", name="label", value="Echelle 16 mm")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getEchelle16()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.echelle16.getValue();
    }


    public final void setEchelle25(String echelle25)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.echelle25.getValue();
        this.echelle25.setValue(echelle25);
        String currentValue = this.echelle25.getValue();

        fireEchelle25Change(currentValue, oldValue);
    }
    public final String removeEchelle25()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.echelle25.getValue();
        String removedValue = this.echelle25.removeValue();
        String currentValue = this.echelle25.getValue();

        fireEchelle25Change(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultEchelle25(String echelle25)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.echelle25.getValue();
        this.echelle25.setDefaultValue(echelle25);
        String currentValue = this.echelle25.getValue();

        fireEchelle25Change(currentValue, oldValue);
    }
    
    public final String removeDefaultEchelle25()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.echelle25.getValue();
        String removedValue = this.echelle25.removeDefaultValue();
        String currentValue = this.echelle25.getValue();

        fireEchelle25Change(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getEchelle25ValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.echelle25;
    }
    public final void fireEchelle25Change(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeEchelle25Changed(currentValue);
            firePropertyChange(PROPERTYNAME_ECHELLE25, oldValue, currentValue);
            afterEchelle25Changed(currentValue);
        }
    }

    public void beforeEchelle25Changed( String echelle25)
    { }
    public void afterEchelle25Changed( String echelle25)
    { }

    public boolean isEchelle25Visible()
    { return true; }

    public boolean isEchelle25ReadOnly()
    { return false; }

    public boolean isEchelle25Enabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getEchelle25Price()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="27")
        ,@LocalizedTag(language="fr", name="column", value="27")
        ,@LocalizedTag(language="en", name="label", value="25 mm ladder")
        ,@LocalizedTag(language="fr", name="label", value="Echelle 25 mm")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getEchelle25()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.echelle25.getValue();
    }


    public final void setEchelle50(String echelle50)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.echelle50.getValue();
        this.echelle50.setValue(echelle50);
        String currentValue = this.echelle50.getValue();

        fireEchelle50Change(currentValue, oldValue);
    }
    public final String removeEchelle50()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.echelle50.getValue();
        String removedValue = this.echelle50.removeValue();
        String currentValue = this.echelle50.getValue();

        fireEchelle50Change(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultEchelle50(String echelle50)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.echelle50.getValue();
        this.echelle50.setDefaultValue(echelle50);
        String currentValue = this.echelle50.getValue();

        fireEchelle50Change(currentValue, oldValue);
    }
    
    public final String removeDefaultEchelle50()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.echelle50.getValue();
        String removedValue = this.echelle50.removeDefaultValue();
        String currentValue = this.echelle50.getValue();

        fireEchelle50Change(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getEchelle50ValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.echelle50;
    }
    public final void fireEchelle50Change(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeEchelle50Changed(currentValue);
            firePropertyChange(PROPERTYNAME_ECHELLE50, oldValue, currentValue);
            afterEchelle50Changed(currentValue);
        }
    }

    public void beforeEchelle50Changed( String echelle50)
    { }
    public void afterEchelle50Changed( String echelle50)
    { }

    public boolean isEchelle50Visible()
    { return true; }

    public boolean isEchelle50ReadOnly()
    { return false; }

    public boolean isEchelle50Enabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getEchelle50Price()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="28")
        ,@LocalizedTag(language="fr", name="column", value="28")
        ,@LocalizedTag(language="en", name="label", value="50 mm ladder")
        ,@LocalizedTag(language="fr", name="label", value="Echelle 50 mm")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getEchelle50()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.echelle50.getValue();
    }


    public final void setEchelle70(String echelle70)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.echelle70.getValue();
        this.echelle70.setValue(echelle70);
        String currentValue = this.echelle70.getValue();

        fireEchelle70Change(currentValue, oldValue);
    }
    public final String removeEchelle70()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.echelle70.getValue();
        String removedValue = this.echelle70.removeValue();
        String currentValue = this.echelle70.getValue();

        fireEchelle70Change(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultEchelle70(String echelle70)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.echelle70.getValue();
        this.echelle70.setDefaultValue(echelle70);
        String currentValue = this.echelle70.getValue();

        fireEchelle70Change(currentValue, oldValue);
    }
    
    public final String removeDefaultEchelle70()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.echelle70.getValue();
        String removedValue = this.echelle70.removeDefaultValue();
        String currentValue = this.echelle70.getValue();

        fireEchelle70Change(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getEchelle70ValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.echelle70;
    }
    public final void fireEchelle70Change(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeEchelle70Changed(currentValue);
            firePropertyChange(PROPERTYNAME_ECHELLE70, oldValue, currentValue);
            afterEchelle70Changed(currentValue);
        }
    }

    public void beforeEchelle70Changed( String echelle70)
    { }
    public void afterEchelle70Changed( String echelle70)
    { }

    public boolean isEchelle70Visible()
    { return true; }

    public boolean isEchelle70ReadOnly()
    { return false; }

    public boolean isEchelle70Enabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getEchelle70Price()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="29")
        ,@LocalizedTag(language="fr", name="column", value="29")
        ,@LocalizedTag(language="en", name="label", value="70 mm ladder")
        ,@LocalizedTag(language="fr", name="label", value="Echelle 70 mm")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getEchelle70()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.echelle70.getValue();
    }


    public final void setCadreStorClip(String cadreStorClip)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.cadreStorClip.getValue();
        this.cadreStorClip.setValue(cadreStorClip);
        String currentValue = this.cadreStorClip.getValue();

        fireCadreStorClipChange(currentValue, oldValue);
    }
    public final String removeCadreStorClip()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.cadreStorClip.getValue();
        String removedValue = this.cadreStorClip.removeValue();
        String currentValue = this.cadreStorClip.getValue();

        fireCadreStorClipChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultCadreStorClip(String cadreStorClip)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.cadreStorClip.getValue();
        this.cadreStorClip.setDefaultValue(cadreStorClip);
        String currentValue = this.cadreStorClip.getValue();

        fireCadreStorClipChange(currentValue, oldValue);
    }
    
    public final String removeDefaultCadreStorClip()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.cadreStorClip.getValue();
        String removedValue = this.cadreStorClip.removeDefaultValue();
        String currentValue = this.cadreStorClip.getValue();

        fireCadreStorClipChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getCadreStorClipValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.cadreStorClip;
    }
    public final void fireCadreStorClipChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeCadreStorClipChanged(currentValue);
            firePropertyChange(PROPERTYNAME_CADRESTORCLIP, oldValue, currentValue);
            afterCadreStorClipChanged(currentValue);
        }
    }

    public void beforeCadreStorClipChanged( String cadreStorClip)
    { }
    public void afterCadreStorClipChanged( String cadreStorClip)
    { }

    public boolean isCadreStorClipVisible()
    { return true; }

    public boolean isCadreStorClipReadOnly()
    { return false; }

    public boolean isCadreStorClipEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getCadreStorClipPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="30")
        ,@LocalizedTag(language="fr", name="column", value="30")
        ,@LocalizedTag(language="fr", name="label", value="Cadre store clip")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getCadreStorClip()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.cadreStorClip.getValue();
    }


    public final void setMecaRetro(String mecaRetro)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.mecaRetro.getValue();
        this.mecaRetro.setValue(mecaRetro);
        String currentValue = this.mecaRetro.getValue();

        fireMecaRetroChange(currentValue, oldValue);
    }
    public final String removeMecaRetro()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.mecaRetro.getValue();
        String removedValue = this.mecaRetro.removeValue();
        String currentValue = this.mecaRetro.getValue();

        fireMecaRetroChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultMecaRetro(String mecaRetro)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.mecaRetro.getValue();
        this.mecaRetro.setDefaultValue(mecaRetro);
        String currentValue = this.mecaRetro.getValue();

        fireMecaRetroChange(currentValue, oldValue);
    }
    
    public final String removeDefaultMecaRetro()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.mecaRetro.getValue();
        String removedValue = this.mecaRetro.removeDefaultValue();
        String currentValue = this.mecaRetro.getValue();

        fireMecaRetroChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getMecaRetroValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.mecaRetro;
    }
    public final void fireMecaRetroChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeMecaRetroChanged(currentValue);
            firePropertyChange(PROPERTYNAME_MECARETRO, oldValue, currentValue);
            afterMecaRetroChanged(currentValue);
        }
    }

    public void beforeMecaRetroChanged( String mecaRetro)
    { }
    public void afterMecaRetroChanged( String mecaRetro)
    { }

    public boolean isMecaRetroVisible()
    { return true; }

    public boolean isMecaRetroReadOnly()
    { return false; }

    public boolean isMecaRetroEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getMecaRetroPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="31")
        ,@LocalizedTag(language="fr", name="column", value="31")
        ,@LocalizedTag(language="fr", name="label", value="M�canisme boitier r�tro")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getMecaRetro()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.mecaRetro.getValue();
    }


    public final void setTigeRetro(String tigeRetro)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.tigeRetro.getValue();
        this.tigeRetro.setValue(tigeRetro);
        String currentValue = this.tigeRetro.getValue();

        fireTigeRetroChange(currentValue, oldValue);
    }
    public final String removeTigeRetro()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.tigeRetro.getValue();
        String removedValue = this.tigeRetro.removeValue();
        String currentValue = this.tigeRetro.getValue();

        fireTigeRetroChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultTigeRetro(String tigeRetro)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.tigeRetro.getValue();
        this.tigeRetro.setDefaultValue(tigeRetro);
        String currentValue = this.tigeRetro.getValue();

        fireTigeRetroChange(currentValue, oldValue);
    }
    
    public final String removeDefaultTigeRetro()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.tigeRetro.getValue();
        String removedValue = this.tigeRetro.removeDefaultValue();
        String currentValue = this.tigeRetro.getValue();

        fireTigeRetroChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getTigeRetroValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.tigeRetro;
    }
    public final void fireTigeRetroChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeTigeRetroChanged(currentValue);
            firePropertyChange(PROPERTYNAME_TIGERETRO, oldValue, currentValue);
            afterTigeRetroChanged(currentValue);
        }
    }

    public void beforeTigeRetroChanged( String tigeRetro)
    { }
    public void afterTigeRetroChanged( String tigeRetro)
    { }

    public boolean isTigeRetroVisible()
    { return true; }

    public boolean isTigeRetroReadOnly()
    { return false; }

    public boolean isTigeRetroEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getTigeRetroPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="32")
        ,@LocalizedTag(language="fr", name="column", value="32")
        ,@LocalizedTag(language="fr", name="label", value="Tige boitier retro")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getTigeRetro()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.tigeRetro.getValue();
    }


    public final void setCoulBsoClass(String coulBsoClass)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.coulBsoClass.getValue();
        this.coulBsoClass.setValue(coulBsoClass);
        String currentValue = this.coulBsoClass.getValue();

        fireCoulBsoClassChange(currentValue, oldValue);
    }
    public final String removeCoulBsoClass()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.coulBsoClass.getValue();
        String removedValue = this.coulBsoClass.removeValue();
        String currentValue = this.coulBsoClass.getValue();

        fireCoulBsoClassChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultCoulBsoClass(String coulBsoClass)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.coulBsoClass.getValue();
        this.coulBsoClass.setDefaultValue(coulBsoClass);
        String currentValue = this.coulBsoClass.getValue();

        fireCoulBsoClassChange(currentValue, oldValue);
    }
    
    public final String removeDefaultCoulBsoClass()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.coulBsoClass.getValue();
        String removedValue = this.coulBsoClass.removeDefaultValue();
        String currentValue = this.coulBsoClass.getValue();

        fireCoulBsoClassChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getCoulBsoClassValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.coulBsoClass;
    }
    public final void fireCoulBsoClassChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeCoulBsoClassChanged(currentValue);
            firePropertyChange(PROPERTYNAME_COULBSOCLASS, oldValue, currentValue);
            afterCoulBsoClassChanged(currentValue);
        }
    }

    public void beforeCoulBsoClassChanged( String coulBsoClass)
    { }
    public void afterCoulBsoClassChanged( String coulBsoClass)
    { }

    public boolean isCoulBsoClassVisible()
    { return true; }

    public boolean isCoulBsoClassReadOnly()
    { return false; }

    public boolean isCoulBsoClassEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getCoulBsoClassPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="33")
        ,@LocalizedTag(language="fr", name="column", value="33")
        ,@LocalizedTag(language="fr", name="label", value="Coulisses bso classique")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getCoulBsoClass()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.coulBsoClass.getValue();
    }


    public final void setCoulBsoMono(String coulBsoMono)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.coulBsoMono.getValue();
        this.coulBsoMono.setValue(coulBsoMono);
        String currentValue = this.coulBsoMono.getValue();

        fireCoulBsoMonoChange(currentValue, oldValue);
    }
    public final String removeCoulBsoMono()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.coulBsoMono.getValue();
        String removedValue = this.coulBsoMono.removeValue();
        String currentValue = this.coulBsoMono.getValue();

        fireCoulBsoMonoChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultCoulBsoMono(String coulBsoMono)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.coulBsoMono.getValue();
        this.coulBsoMono.setDefaultValue(coulBsoMono);
        String currentValue = this.coulBsoMono.getValue();

        fireCoulBsoMonoChange(currentValue, oldValue);
    }
    
    public final String removeDefaultCoulBsoMono()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.coulBsoMono.getValue();
        String removedValue = this.coulBsoMono.removeDefaultValue();
        String currentValue = this.coulBsoMono.getValue();

        fireCoulBsoMonoChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getCoulBsoMonoValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.coulBsoMono;
    }
    public final void fireCoulBsoMonoChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeCoulBsoMonoChanged(currentValue);
            firePropertyChange(PROPERTYNAME_COULBSOMONO, oldValue, currentValue);
            afterCoulBsoMonoChanged(currentValue);
        }
    }

    public void beforeCoulBsoMonoChanged( String coulBsoMono)
    { }
    public void afterCoulBsoMonoChanged( String coulBsoMono)
    { }

    public boolean isCoulBsoMonoVisible()
    { return true; }

    public boolean isCoulBsoMonoReadOnly()
    { return false; }

    public boolean isCoulBsoMonoEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getCoulBsoMonoPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="34")
        ,@LocalizedTag(language="fr", name="column", value="34")
        ,@LocalizedTag(language="fr", name="label", value="Coulisses bso monobloc")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getCoulBsoMono()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.coulBsoMono.getValue();
    }


    public final void setSousFaceBso(String sousFaceBso)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.sousFaceBso.getValue();
        this.sousFaceBso.setValue(sousFaceBso);
        String currentValue = this.sousFaceBso.getValue();

        fireSousFaceBsoChange(currentValue, oldValue);
    }
    public final String removeSousFaceBso()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.sousFaceBso.getValue();
        String removedValue = this.sousFaceBso.removeValue();
        String currentValue = this.sousFaceBso.getValue();

        fireSousFaceBsoChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultSousFaceBso(String sousFaceBso)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.sousFaceBso.getValue();
        this.sousFaceBso.setDefaultValue(sousFaceBso);
        String currentValue = this.sousFaceBso.getValue();

        fireSousFaceBsoChange(currentValue, oldValue);
    }
    
    public final String removeDefaultSousFaceBso()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.sousFaceBso.getValue();
        String removedValue = this.sousFaceBso.removeDefaultValue();
        String currentValue = this.sousFaceBso.getValue();

        fireSousFaceBsoChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getSousFaceBsoValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.sousFaceBso;
    }
    public final void fireSousFaceBsoChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeSousFaceBsoChanged(currentValue);
            firePropertyChange(PROPERTYNAME_SOUSFACEBSO, oldValue, currentValue);
            afterSousFaceBsoChanged(currentValue);
        }
    }

    public void beforeSousFaceBsoChanged( String sousFaceBso)
    { }
    public void afterSousFaceBsoChanged( String sousFaceBso)
    { }

    public boolean isSousFaceBsoVisible()
    { return true; }

    public boolean isSousFaceBsoReadOnly()
    { return false; }

    public boolean isSousFaceBsoEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getSousFaceBsoPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="35")
        ,@LocalizedTag(language="fr", name="column", value="35")
        ,@LocalizedTag(language="fr", name="label", value="Sous face bso")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getSousFaceBso()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.sousFaceBso.getValue();
    }


    public final void setBandeauClass(String bandeauClass)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.bandeauClass.getValue();
        this.bandeauClass.setValue(bandeauClass);
        String currentValue = this.bandeauClass.getValue();

        fireBandeauClassChange(currentValue, oldValue);
    }
    public final String removeBandeauClass()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.bandeauClass.getValue();
        String removedValue = this.bandeauClass.removeValue();
        String currentValue = this.bandeauClass.getValue();

        fireBandeauClassChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultBandeauClass(String bandeauClass)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.bandeauClass.getValue();
        this.bandeauClass.setDefaultValue(bandeauClass);
        String currentValue = this.bandeauClass.getValue();

        fireBandeauClassChange(currentValue, oldValue);
    }
    
    public final String removeDefaultBandeauClass()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.bandeauClass.getValue();
        String removedValue = this.bandeauClass.removeDefaultValue();
        String currentValue = this.bandeauClass.getValue();

        fireBandeauClassChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getBandeauClassValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.bandeauClass;
    }
    public final void fireBandeauClassChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeBandeauClassChanged(currentValue);
            firePropertyChange(PROPERTYNAME_BANDEAUCLASS, oldValue, currentValue);
            afterBandeauClassChanged(currentValue);
        }
    }

    public void beforeBandeauClassChanged( String bandeauClass)
    { }
    public void afterBandeauClassChanged( String bandeauClass)
    { }

    public boolean isBandeauClassVisible()
    { return true; }

    public boolean isBandeauClassReadOnly()
    { return false; }

    public boolean isBandeauClassEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getBandeauClassPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="36")
        ,@LocalizedTag(language="fr", name="column", value="36")
        ,@LocalizedTag(language="fr", name="label", value="Bandeau bso classique")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getBandeauClass()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.bandeauClass.getValue();
    }


    public final void setBandeauMono(String bandeauMono)
    {   // GENCODE b50226a0-c372-11e0-962b-0800200c9a66 SETTER

        String oldValue = this.bandeauMono.getValue();
        this.bandeauMono.setValue(bandeauMono);
        String currentValue = this.bandeauMono.getValue();

        fireBandeauMonoChange(currentValue, oldValue);
    }
    public final String removeBandeauMono()
    {   // GENCODE 9c9874c0-c372-11e0-962b-0800200c9a66 REMOVER
        String oldValue = this.bandeauMono.getValue();
        String removedValue = this.bandeauMono.removeValue();
        String currentValue = this.bandeauMono.getValue();

        fireBandeauMonoChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultBandeauMono(String bandeauMono)
    {   // GENCODE 87a1a280-c372-11e0-962b-0800200c9a66 DEFAULT SETTER
        String oldValue = this.bandeauMono.getValue();
        this.bandeauMono.setDefaultValue(bandeauMono);
        String currentValue = this.bandeauMono.getValue();

        fireBandeauMonoChange(currentValue, oldValue);
    }
    
    public final String removeDefaultBandeauMono()
    {   // GENCODE ec712480-c2b2-11e0-962b-0800200c9a66 DEFAULT REMOVER
        String oldValue = this.bandeauMono.getValue();
        String removedValue = this.bandeauMono.removeDefaultValue();
        String currentValue = this.bandeauMono.getValue();

        fireBandeauMonoChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<String> getBandeauMonoValueContainer()
    {   // GENCODE 42b74530-c372-11e0-962b-0800200c9a66 ValueContainer GETTER
        return this.bandeauMono;
    }
    public final void fireBandeauMonoChange(String currentValue, String oldValue)
    {   // GENCODE 3e207f00-c372-11e0-962b-0800200c9a66 TRIGGER
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeBandeauMonoChanged(currentValue);
            firePropertyChange(PROPERTYNAME_BANDEAUMONO, oldValue, currentValue);
            afterBandeauMonoChanged(currentValue);
        }
    }

    public void beforeBandeauMonoChanged( String bandeauMono)
    { }
    public void afterBandeauMonoChanged( String bandeauMono)
    { }

    public boolean isBandeauMonoVisible()
    { return true; }

    public boolean isBandeauMonoReadOnly()
    { return false; }

    public boolean isBandeauMonoEnabled()
    { return true; /* GENCODE 378184b2-6743-4461-83e3-9a3bd6759318 */}

    public Double getBandeauMonoPrice()
    { return 0d; /* GENCODE f99cc0ee-c3a9-4a5d-8d6a-7badb02cc09b */}

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="column", value="37")
        ,@LocalizedTag(language="fr", name="column", value="37")
        ,@LocalizedTag(language="fr", name="label", value="Bandeau bso monobloc")
    })
    @Visible("false")
    @Filtrable(value = "false", defaultValue = "")
    @AssociatedComment("false")
    @AssociatedDrawing("false")
    public final String getBandeauMono()
    {    //GENCODE b595cc70-c2b4-11e0-962b-0800200c9a66 New ValueContainer getter operations
         return this.bandeauMono.getValue();
    }



    // Business methods 



    @Override
    public void setDefaultValues()
    { 
        super.setDefaultValues();
    }

    // Bound properties

    public static final String PROPERTYNAME_CODE = "code";  
    public static final String PROPERTYNAME_DESCRIPTION = "description";  
    public static final String PROPERTYNAME_RED = "red";  
    public static final String PROPERTYNAME_GREEN = "green";  
    public static final String PROPERTYNAME_BLUE = "blue";  
    public static final String PROPERTYNAME_WOOD = "wood";  
    public static final String PROPERTYNAME_IMGPATH = "imgPath";  
    public static final String PROPERTYNAME_TEXTURE = "texture";  
    public static final String PROPERTYNAME_LAMES = "lames";  
    public static final String PROPERTYNAME_MODELE = "modele";  
    public static final String PROPERTYNAME_VENULTI25 = "venUlti25";  
    public static final String PROPERTYNAME_VENMET25 = "venMet25";  
    public static final String PROPERTYNAME_VENSOFT35 = "venSoft35";  
    public static final String PROPERTYNAME_VENMIDI40 = "venMidi40";  
    public static final String PROPERTYNAME_VENFAB28 = "venFab28";  
    public static final String PROPERTYNAME_VENCLASS50 = "venClass50";  
    public static final String PROPERTYNAME_VENRETRO50 = "venRetro50";  
    public static final String PROPERTYNAME_VENBSO80 = "venBso80";  
    public static final String PROPERTYNAME_SOUSFACE25ULTI = "sousFace25Ulti";  
    public static final String PROPERTYNAME_SOUSFACE25 = "sousFace25";  
    public static final String PROPERTYNAME_SOUSFACE50 = "sousFace50";  
    public static final String PROPERTYNAME_SOUSFACERETRO = "sousFaceRetro";  
    public static final String PROPERTYNAME_GALONFABER = "galonFaber";  
    public static final String PROPERTYNAME_GALONTSF = "galonTsf";  
    public static final String PROPERTYNAME_ECHELLETSF = "echelleTsf";  
    public static final String PROPERTYNAME_ECHELLE16 = "echelle16";  
    public static final String PROPERTYNAME_ECHELLE25 = "echelle25";  
    public static final String PROPERTYNAME_ECHELLE50 = "echelle50";  
    public static final String PROPERTYNAME_ECHELLE70 = "echelle70";  
    public static final String PROPERTYNAME_CADRESTORCLIP = "cadreStorClip";  
    public static final String PROPERTYNAME_MECARETRO = "mecaRetro";  
    public static final String PROPERTYNAME_TIGERETRO = "tigeRetro";  
    public static final String PROPERTYNAME_COULBSOCLASS = "coulBsoClass";  
    public static final String PROPERTYNAME_COULBSOMONO = "coulBsoMono";  
    public static final String PROPERTYNAME_SOUSFACEBSO = "sousFaceBso";  
    public static final String PROPERTYNAME_BANDEAUCLASS = "bandeauClass";  
    public static final String PROPERTYNAME_BANDEAUMONO = "bandeauMono";  
}
