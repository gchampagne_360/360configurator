// generated class, do not edit

package com.client360.configuration.blinds.horizontal;

//Plugin V13.1.2
//2013-12-30

// Imports 

public abstract class HorizontalBladesBase extends com.netappsid.configuration.blinds.horizontalvenitian.HorizontalBlades
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(HorizontalBladesBase.class);

    // attributes

    // Constructors

    public HorizontalBladesBase(com.netappsid.erp.configurator.Configurable parent) // GENCODE b298fff0-4bff-11e0-b8af-0800200c9a66
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.blinds.horizontal.HorizontalBlades"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations


    // Business methods 



    @Override
    public void setDefaultValues()
    { 
        super.setDefaultValues();
    }

    @Override
    protected String getSequences()
    {
        return "angle,thickness,topOffset,leftOffset,rightOffset,dimension,nbColors,colors,ladders,horizontalFinalBlade";  
    }

    // Bound properties

}
