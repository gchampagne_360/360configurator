// generated class, do not edit

package com.client360.configuration.hingeshutters;

//Plugin V14.0.1

// Imports 

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.hingeshutters.enums.HingeShutterPanelTypes;
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class ShutterPanelSectionBase extends com.netappsid.erp.configurator.Configurable
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ShutterPanelSectionBase.class);

    // attributes
    private ValueContainer<Measure<Length>> height = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<HingeShutterPanelTypes> hingeShutterPanelTypes = new ValueContainer<HingeShutterPanelTypes>(this);

    // Bound properties

    public static final String PROPERTYNAME_HEIGHT = "height";  
    public static final String PROPERTYNAME_HINGESHUTTERPANELTYPES = "hingeShutterPanelTypes";  

    // Constructors

    public ShutterPanelSectionBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.hingeshutters.ShutterPanelSection"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Height")
        ,@LocalizedTag(language="fr", name="label", value="Hauteur")
    })
    public final Measure<Length> getHeight()
    {
         return this.height.getValue();
    }

    public final void setHeight(Measure<Length> height)
    {

        height = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(height, getDefaultUnitFor(PROPERTYNAME_HEIGHT,Length.class));

        Measure<Length> oldValue = this.height.getValue();
        this.height.setValue(height);

        Measure<Length> currentValue = this.height.getValue();

        fireHeightChange(currentValue, oldValue);
    }
    public final Measure<Length> removeHeight()
    {
        Measure<Length> oldValue = this.height.getValue();
        Measure<Length> removedValue = this.height.removeValue();
        Measure<Length> currentValue = this.height.getValue();

        fireHeightChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultHeight(Measure<Length> height)
    {
        height = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(height, getDefaultUnitFor(PROPERTYNAME_HEIGHT,Length.class));

        Measure<Length> oldValue = this.height.getValue();
        this.height.setDefaultValue(height);

        Measure<Length> currentValue = this.height.getValue();

        fireHeightChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultHeight()
    {
        Measure<Length> oldValue = this.height.getValue();
        Measure<Length> removedValue = this.height.removeDefaultValue();
        Measure<Length> currentValue = this.height.getValue();

        fireHeightChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getHeightValueContainer()
    {
        return this.height;
    }
    public final void fireHeightChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeHeightChanged(currentValue);
            firePropertyChange(PROPERTYNAME_HEIGHT, oldValue, currentValue);
            afterHeightChanged(currentValue);
        }
    }

    public void beforeHeightChanged( Measure<Length> height)
    { }
    public void afterHeightChanged( Measure<Length> height)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Section type")
        ,@LocalizedTag(language="fr", name="label", value="Type de section")
    })
    public final HingeShutterPanelTypes getHingeShutterPanelTypes()
    {
         return this.hingeShutterPanelTypes.getValue();
    }

    public final void setHingeShutterPanelTypes(HingeShutterPanelTypes hingeShutterPanelTypes)
    {

        HingeShutterPanelTypes oldValue = this.hingeShutterPanelTypes.getValue();
        this.hingeShutterPanelTypes.setValue(hingeShutterPanelTypes);

        HingeShutterPanelTypes currentValue = this.hingeShutterPanelTypes.getValue();

        fireHingeShutterPanelTypesChange(currentValue, oldValue);
    }
    public final HingeShutterPanelTypes removeHingeShutterPanelTypes()
    {
        HingeShutterPanelTypes oldValue = this.hingeShutterPanelTypes.getValue();
        HingeShutterPanelTypes removedValue = this.hingeShutterPanelTypes.removeValue();
        HingeShutterPanelTypes currentValue = this.hingeShutterPanelTypes.getValue();

        fireHingeShutterPanelTypesChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultHingeShutterPanelTypes(HingeShutterPanelTypes hingeShutterPanelTypes)
    {
        HingeShutterPanelTypes oldValue = this.hingeShutterPanelTypes.getValue();
        this.hingeShutterPanelTypes.setDefaultValue(hingeShutterPanelTypes);

        HingeShutterPanelTypes currentValue = this.hingeShutterPanelTypes.getValue();

        fireHingeShutterPanelTypesChange(currentValue, oldValue);
    }
    
    public final HingeShutterPanelTypes removeDefaultHingeShutterPanelTypes()
    {
        HingeShutterPanelTypes oldValue = this.hingeShutterPanelTypes.getValue();
        HingeShutterPanelTypes removedValue = this.hingeShutterPanelTypes.removeDefaultValue();
        HingeShutterPanelTypes currentValue = this.hingeShutterPanelTypes.getValue();

        fireHingeShutterPanelTypesChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<HingeShutterPanelTypes> getHingeShutterPanelTypesValueContainer()
    {
        return this.hingeShutterPanelTypes;
    }
    public final void fireHingeShutterPanelTypesChange(HingeShutterPanelTypes currentValue, HingeShutterPanelTypes oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeHingeShutterPanelTypesChanged(currentValue);
            firePropertyChange(PROPERTYNAME_HINGESHUTTERPANELTYPES, oldValue, currentValue);
            afterHingeShutterPanelTypesChanged(currentValue);
        }
    }

    public void beforeHingeShutterPanelTypesChanged( HingeShutterPanelTypes hingeShutterPanelTypes)
    { }
    public void afterHingeShutterPanelTypesChanged( HingeShutterPanelTypes hingeShutterPanelTypes)
    { }


    public boolean isHingeShutterPanelTypesMandatory()
    {
        return true;
    }


    @Override
    protected String getSequences()
    {
        return "height,hingeShutterPanelTypes";  
    }
}
