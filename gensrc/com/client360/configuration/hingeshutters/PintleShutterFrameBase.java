// generated class, do not edit

package com.client360.configuration.hingeshutters;

//Plugin V14.0.1

// Imports 

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.hingeshutters.enums.PintleShutterFrameTypes;
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class PintleShutterFrameBase extends com.netappsid.configuration.hingeshutters.PintleShutterFrame
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(PintleShutterFrameBase.class);

    // attributes
    private ValueContainer<Measure<Length>> frameSideOffset = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Measure<Length>> frameSideSpacer = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<PintleShutterFrameTypes> pintleShutterFrameTypes = new ValueContainer<PintleShutterFrameTypes>(this);

    // Bound properties

    public static final String PROPERTYNAME_FRAMESIDEOFFSET = "frameSideOffset";  
    public static final String PROPERTYNAME_FRAMESIDESPACER = "frameSideSpacer";  
    public static final String PROPERTYNAME_PINTLESHUTTERFRAMETYPES = "pintleShutterFrameTypes";  

    // Constructors

    public PintleShutterFrameBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.hingeshutters.PintleShutterFrame"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Offset")
        ,@LocalizedTag(language="fr", name="label", value="D�callage")
    })
    public final Measure<Length> getFrameSideOffset()
    {
         return this.frameSideOffset.getValue();
    }

    public final void setFrameSideOffset(Measure<Length> frameSideOffset)
    {

        frameSideOffset = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(frameSideOffset, getDefaultUnitFor(PROPERTYNAME_FRAMESIDEOFFSET,Length.class));

        Measure<Length> oldValue = this.frameSideOffset.getValue();
        this.frameSideOffset.setValue(frameSideOffset);

        Measure<Length> currentValue = this.frameSideOffset.getValue();

        fireFrameSideOffsetChange(currentValue, oldValue);
    }
    public final Measure<Length> removeFrameSideOffset()
    {
        Measure<Length> oldValue = this.frameSideOffset.getValue();
        Measure<Length> removedValue = this.frameSideOffset.removeValue();
        Measure<Length> currentValue = this.frameSideOffset.getValue();

        fireFrameSideOffsetChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultFrameSideOffset(Measure<Length> frameSideOffset)
    {
        frameSideOffset = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(frameSideOffset, getDefaultUnitFor(PROPERTYNAME_FRAMESIDEOFFSET,Length.class));

        Measure<Length> oldValue = this.frameSideOffset.getValue();
        this.frameSideOffset.setDefaultValue(frameSideOffset);

        Measure<Length> currentValue = this.frameSideOffset.getValue();

        fireFrameSideOffsetChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultFrameSideOffset()
    {
        Measure<Length> oldValue = this.frameSideOffset.getValue();
        Measure<Length> removedValue = this.frameSideOffset.removeDefaultValue();
        Measure<Length> currentValue = this.frameSideOffset.getValue();

        fireFrameSideOffsetChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getFrameSideOffsetValueContainer()
    {
        return this.frameSideOffset;
    }
    public final void fireFrameSideOffsetChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeFrameSideOffsetChanged(currentValue);
            firePropertyChange(PROPERTYNAME_FRAMESIDEOFFSET, oldValue, currentValue);
            afterFrameSideOffsetChanged(currentValue);
        }
    }

    public void beforeFrameSideOffsetChanged( Measure<Length> frameSideOffset)
    { }
    public void afterFrameSideOffsetChanged( Measure<Length> frameSideOffset)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Spacer")
        ,@LocalizedTag(language="fr", name="label", value="Largeur du c�dre")
    })
    public final Measure<Length> getFrameSideSpacer()
    {
         return this.frameSideSpacer.getValue();
    }

    public final void setFrameSideSpacer(Measure<Length> frameSideSpacer)
    {

        frameSideSpacer = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(frameSideSpacer, getDefaultUnitFor(PROPERTYNAME_FRAMESIDESPACER,Length.class));

        Measure<Length> oldValue = this.frameSideSpacer.getValue();
        this.frameSideSpacer.setValue(frameSideSpacer);

        Measure<Length> currentValue = this.frameSideSpacer.getValue();

        fireFrameSideSpacerChange(currentValue, oldValue);
    }
    public final Measure<Length> removeFrameSideSpacer()
    {
        Measure<Length> oldValue = this.frameSideSpacer.getValue();
        Measure<Length> removedValue = this.frameSideSpacer.removeValue();
        Measure<Length> currentValue = this.frameSideSpacer.getValue();

        fireFrameSideSpacerChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultFrameSideSpacer(Measure<Length> frameSideSpacer)
    {
        frameSideSpacer = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(frameSideSpacer, getDefaultUnitFor(PROPERTYNAME_FRAMESIDESPACER,Length.class));

        Measure<Length> oldValue = this.frameSideSpacer.getValue();
        this.frameSideSpacer.setDefaultValue(frameSideSpacer);

        Measure<Length> currentValue = this.frameSideSpacer.getValue();

        fireFrameSideSpacerChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultFrameSideSpacer()
    {
        Measure<Length> oldValue = this.frameSideSpacer.getValue();
        Measure<Length> removedValue = this.frameSideSpacer.removeDefaultValue();
        Measure<Length> currentValue = this.frameSideSpacer.getValue();

        fireFrameSideSpacerChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getFrameSideSpacerValueContainer()
    {
        return this.frameSideSpacer;
    }
    public final void fireFrameSideSpacerChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeFrameSideSpacerChanged(currentValue);
            firePropertyChange(PROPERTYNAME_FRAMESIDESPACER, oldValue, currentValue);
            afterFrameSideSpacerChanged(currentValue);
        }
    }

    public void beforeFrameSideSpacerChanged( Measure<Length> frameSideSpacer)
    { }
    public void afterFrameSideSpacerChanged( Measure<Length> frameSideSpacer)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Frame profile")
        ,@LocalizedTag(language="fr", name="label", value="Profil du c�dre")
    })
    public final PintleShutterFrameTypes getPintleShutterFrameTypes()
    {
         return this.pintleShutterFrameTypes.getValue();
    }

    public final void setPintleShutterFrameTypes(PintleShutterFrameTypes pintleShutterFrameTypes)
    {

        PintleShutterFrameTypes oldValue = this.pintleShutterFrameTypes.getValue();
        this.pintleShutterFrameTypes.setValue(pintleShutterFrameTypes);

        PintleShutterFrameTypes currentValue = this.pintleShutterFrameTypes.getValue();

        firePintleShutterFrameTypesChange(currentValue, oldValue);
    }
    public final PintleShutterFrameTypes removePintleShutterFrameTypes()
    {
        PintleShutterFrameTypes oldValue = this.pintleShutterFrameTypes.getValue();
        PintleShutterFrameTypes removedValue = this.pintleShutterFrameTypes.removeValue();
        PintleShutterFrameTypes currentValue = this.pintleShutterFrameTypes.getValue();

        firePintleShutterFrameTypesChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultPintleShutterFrameTypes(PintleShutterFrameTypes pintleShutterFrameTypes)
    {
        PintleShutterFrameTypes oldValue = this.pintleShutterFrameTypes.getValue();
        this.pintleShutterFrameTypes.setDefaultValue(pintleShutterFrameTypes);

        PintleShutterFrameTypes currentValue = this.pintleShutterFrameTypes.getValue();

        firePintleShutterFrameTypesChange(currentValue, oldValue);
    }
    
    public final PintleShutterFrameTypes removeDefaultPintleShutterFrameTypes()
    {
        PintleShutterFrameTypes oldValue = this.pintleShutterFrameTypes.getValue();
        PintleShutterFrameTypes removedValue = this.pintleShutterFrameTypes.removeDefaultValue();
        PintleShutterFrameTypes currentValue = this.pintleShutterFrameTypes.getValue();

        firePintleShutterFrameTypesChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<PintleShutterFrameTypes> getPintleShutterFrameTypesValueContainer()
    {
        return this.pintleShutterFrameTypes;
    }
    public final void firePintleShutterFrameTypesChange(PintleShutterFrameTypes currentValue, PintleShutterFrameTypes oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforePintleShutterFrameTypesChanged(currentValue);
            firePropertyChange(PROPERTYNAME_PINTLESHUTTERFRAMETYPES, oldValue, currentValue);
            afterPintleShutterFrameTypesChanged(currentValue);
        }
    }

    public void beforePintleShutterFrameTypesChanged( PintleShutterFrameTypes pintleShutterFrameTypes)
    { }
    public void afterPintleShutterFrameTypesChanged( PintleShutterFrameTypes pintleShutterFrameTypes)
    { }


}
