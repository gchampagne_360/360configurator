// generated class, do not edit

package com.client360.configuration.hingeshutters;

//Plugin V14.0.1

// Imports 


public abstract class PanelBase extends com.netappsid.configuration.hingeshutters.Panel
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(PanelBase.class);

    // attributes

    // Bound properties


    // Constructors

    public PanelBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.hingeshutters.Panel"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations



    @Override
    protected String getSequences()
    {
        return "width,form,stayDisplay,hingesDisplay,choiceSide,not set,gapToHeld,interiorColor,exteriorColor";  
    }
}
