// generated class, do not edit

package com.client360.configuration.hingeshutters;

//Plugin V14.0.1

// Imports 

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.hingeshutters.enums.HingeShutterPanelProfiles;
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class BoardModelBase extends com.client360.configuration.hingeshutters.PanelModel
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(BoardModelBase.class);

    // attributes
    private ValueContainer<Boolean> boardHorizontal = new ValueContainer<Boolean>(this);
    private ValueContainer<Measure<Length>> zefiralBoardWidth = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Measure<Length>> boardWidth = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Measure<Length>> boardSpacing = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<HingeShutterPanelProfiles> boardProfile = new ValueContainer<HingeShutterPanelProfiles>(this);
    private ValueContainer<com.client360.configuration.wad.StandardColor> boardColor = new ValueContainer<com.client360.configuration.wad.StandardColor>(this);

    // Bound properties

    public static final String PROPERTYNAME_BOARDHORIZONTAL = "boardHorizontal";  
    public static final String PROPERTYNAME_ZEFIRALBOARDWIDTH = "zefiralBoardWidth";  
    public static final String PROPERTYNAME_BOARDWIDTH = "boardWidth";  
    public static final String PROPERTYNAME_BOARDSPACING = "boardSpacing";  
    public static final String PROPERTYNAME_BOARDPROFILE = "boardProfile";  
    public static final String PROPERTYNAME_BOARDCOLOR = "boardColor";  

    // Constructors

    public BoardModelBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.hingeshutters.BoardModel"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Horizontal boards")
        ,@LocalizedTag(language="fr", name="label", value="Planches horizontales")
    })
    public final Boolean getBoardHorizontal()
    {
         return this.boardHorizontal.getValue();
    }

    public final void setBoardHorizontal(Boolean boardHorizontal)
    {

        Boolean oldValue = this.boardHorizontal.getValue();
        this.boardHorizontal.setValue(boardHorizontal);

        Boolean currentValue = this.boardHorizontal.getValue();

        fireBoardHorizontalChange(currentValue, oldValue);
    }
    public final Boolean removeBoardHorizontal()
    {
        Boolean oldValue = this.boardHorizontal.getValue();
        Boolean removedValue = this.boardHorizontal.removeValue();
        Boolean currentValue = this.boardHorizontal.getValue();

        fireBoardHorizontalChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultBoardHorizontal(Boolean boardHorizontal)
    {
        Boolean oldValue = this.boardHorizontal.getValue();
        this.boardHorizontal.setDefaultValue(boardHorizontal);

        Boolean currentValue = this.boardHorizontal.getValue();

        fireBoardHorizontalChange(currentValue, oldValue);
    }
    
    public final Boolean removeDefaultBoardHorizontal()
    {
        Boolean oldValue = this.boardHorizontal.getValue();
        Boolean removedValue = this.boardHorizontal.removeDefaultValue();
        Boolean currentValue = this.boardHorizontal.getValue();

        fireBoardHorizontalChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Boolean> getBoardHorizontalValueContainer()
    {
        return this.boardHorizontal;
    }
    public final void fireBoardHorizontalChange(Boolean currentValue, Boolean oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeBoardHorizontalChanged(currentValue);
            firePropertyChange(PROPERTYNAME_BOARDHORIZONTAL, oldValue, currentValue);
            afterBoardHorizontalChanged(currentValue);
        }
    }

    public void beforeBoardHorizontalChanged( Boolean boardHorizontal)
    { }
    public void afterBoardHorizontalChanged( Boolean boardHorizontal)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Aluminium board width")
        ,@LocalizedTag(language="fr", name="label", value="Largeur planches alu")
    })
    public final Measure<Length> getZefiralBoardWidth()
    {
         return this.zefiralBoardWidth.getValue();
    }

    public final void setZefiralBoardWidth(Measure<Length> zefiralBoardWidth)
    {

        zefiralBoardWidth = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(zefiralBoardWidth, getDefaultUnitFor(PROPERTYNAME_ZEFIRALBOARDWIDTH,Length.class));

        Measure<Length> oldValue = this.zefiralBoardWidth.getValue();
        this.zefiralBoardWidth.setValue(zefiralBoardWidth);

        Measure<Length> currentValue = this.zefiralBoardWidth.getValue();

        fireZefiralBoardWidthChange(currentValue, oldValue);
    }
    public final Measure<Length> removeZefiralBoardWidth()
    {
        Measure<Length> oldValue = this.zefiralBoardWidth.getValue();
        Measure<Length> removedValue = this.zefiralBoardWidth.removeValue();
        Measure<Length> currentValue = this.zefiralBoardWidth.getValue();

        fireZefiralBoardWidthChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultZefiralBoardWidth(Measure<Length> zefiralBoardWidth)
    {
        zefiralBoardWidth = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(zefiralBoardWidth, getDefaultUnitFor(PROPERTYNAME_ZEFIRALBOARDWIDTH,Length.class));

        Measure<Length> oldValue = this.zefiralBoardWidth.getValue();
        this.zefiralBoardWidth.setDefaultValue(zefiralBoardWidth);

        Measure<Length> currentValue = this.zefiralBoardWidth.getValue();

        fireZefiralBoardWidthChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultZefiralBoardWidth()
    {
        Measure<Length> oldValue = this.zefiralBoardWidth.getValue();
        Measure<Length> removedValue = this.zefiralBoardWidth.removeDefaultValue();
        Measure<Length> currentValue = this.zefiralBoardWidth.getValue();

        fireZefiralBoardWidthChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getZefiralBoardWidthValueContainer()
    {
        return this.zefiralBoardWidth;
    }
    public final void fireZefiralBoardWidthChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeZefiralBoardWidthChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ZEFIRALBOARDWIDTH, oldValue, currentValue);
            afterZefiralBoardWidthChanged(currentValue);
        }
    }

    public void beforeZefiralBoardWidthChanged( Measure<Length> zefiralBoardWidth)
    { }
    public void afterZefiralBoardWidthChanged( Measure<Length> zefiralBoardWidth)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Board width")
        ,@LocalizedTag(language="fr", name="label", value="Largeur des planches")
    })
    public final Measure<Length> getBoardWidth()
    {
         return this.boardWidth.getValue();
    }

    public final void setBoardWidth(Measure<Length> boardWidth)
    {

        boardWidth = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(boardWidth, getDefaultUnitFor(PROPERTYNAME_BOARDWIDTH,Length.class));

        Measure<Length> oldValue = this.boardWidth.getValue();
        this.boardWidth.setValue(boardWidth);

        Measure<Length> currentValue = this.boardWidth.getValue();

        fireBoardWidthChange(currentValue, oldValue);
    }
    public final Measure<Length> removeBoardWidth()
    {
        Measure<Length> oldValue = this.boardWidth.getValue();
        Measure<Length> removedValue = this.boardWidth.removeValue();
        Measure<Length> currentValue = this.boardWidth.getValue();

        fireBoardWidthChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultBoardWidth(Measure<Length> boardWidth)
    {
        boardWidth = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(boardWidth, getDefaultUnitFor(PROPERTYNAME_BOARDWIDTH,Length.class));

        Measure<Length> oldValue = this.boardWidth.getValue();
        this.boardWidth.setDefaultValue(boardWidth);

        Measure<Length> currentValue = this.boardWidth.getValue();

        fireBoardWidthChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultBoardWidth()
    {
        Measure<Length> oldValue = this.boardWidth.getValue();
        Measure<Length> removedValue = this.boardWidth.removeDefaultValue();
        Measure<Length> currentValue = this.boardWidth.getValue();

        fireBoardWidthChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getBoardWidthValueContainer()
    {
        return this.boardWidth;
    }
    public final void fireBoardWidthChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeBoardWidthChanged(currentValue);
            firePropertyChange(PROPERTYNAME_BOARDWIDTH, oldValue, currentValue);
            afterBoardWidthChanged(currentValue);
        }
    }

    public void beforeBoardWidthChanged( Measure<Length> boardWidth)
    { }
    public void afterBoardWidthChanged( Measure<Length> boardWidth)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Board spacing")
        ,@LocalizedTag(language="fr", name="label", value="Jour des planches")
    })
    public final Measure<Length> getBoardSpacing()
    {
         return this.boardSpacing.getValue();
    }

    public final void setBoardSpacing(Measure<Length> boardSpacing)
    {

        boardSpacing = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(boardSpacing, getDefaultUnitFor(PROPERTYNAME_BOARDSPACING,Length.class));

        Measure<Length> oldValue = this.boardSpacing.getValue();
        this.boardSpacing.setValue(boardSpacing);

        Measure<Length> currentValue = this.boardSpacing.getValue();

        fireBoardSpacingChange(currentValue, oldValue);
    }
    public final Measure<Length> removeBoardSpacing()
    {
        Measure<Length> oldValue = this.boardSpacing.getValue();
        Measure<Length> removedValue = this.boardSpacing.removeValue();
        Measure<Length> currentValue = this.boardSpacing.getValue();

        fireBoardSpacingChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultBoardSpacing(Measure<Length> boardSpacing)
    {
        boardSpacing = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(boardSpacing, getDefaultUnitFor(PROPERTYNAME_BOARDSPACING,Length.class));

        Measure<Length> oldValue = this.boardSpacing.getValue();
        this.boardSpacing.setDefaultValue(boardSpacing);

        Measure<Length> currentValue = this.boardSpacing.getValue();

        fireBoardSpacingChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultBoardSpacing()
    {
        Measure<Length> oldValue = this.boardSpacing.getValue();
        Measure<Length> removedValue = this.boardSpacing.removeDefaultValue();
        Measure<Length> currentValue = this.boardSpacing.getValue();

        fireBoardSpacingChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getBoardSpacingValueContainer()
    {
        return this.boardSpacing;
    }
    public final void fireBoardSpacingChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeBoardSpacingChanged(currentValue);
            firePropertyChange(PROPERTYNAME_BOARDSPACING, oldValue, currentValue);
            afterBoardSpacingChanged(currentValue);
        }
    }

    public void beforeBoardSpacingChanged( Measure<Length> boardSpacing)
    { }
    public void afterBoardSpacingChanged( Measure<Length> boardSpacing)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Board profile")
        ,@LocalizedTag(language="fr", name="label", value="Profil des planches")
    })
    public final HingeShutterPanelProfiles getBoardProfile()
    {
         return this.boardProfile.getValue();
    }

    public final void setBoardProfile(HingeShutterPanelProfiles boardProfile)
    {

        HingeShutterPanelProfiles oldValue = this.boardProfile.getValue();
        this.boardProfile.setValue(boardProfile);

        HingeShutterPanelProfiles currentValue = this.boardProfile.getValue();

        fireBoardProfileChange(currentValue, oldValue);
    }
    public final HingeShutterPanelProfiles removeBoardProfile()
    {
        HingeShutterPanelProfiles oldValue = this.boardProfile.getValue();
        HingeShutterPanelProfiles removedValue = this.boardProfile.removeValue();
        HingeShutterPanelProfiles currentValue = this.boardProfile.getValue();

        fireBoardProfileChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultBoardProfile(HingeShutterPanelProfiles boardProfile)
    {
        HingeShutterPanelProfiles oldValue = this.boardProfile.getValue();
        this.boardProfile.setDefaultValue(boardProfile);

        HingeShutterPanelProfiles currentValue = this.boardProfile.getValue();

        fireBoardProfileChange(currentValue, oldValue);
    }
    
    public final HingeShutterPanelProfiles removeDefaultBoardProfile()
    {
        HingeShutterPanelProfiles oldValue = this.boardProfile.getValue();
        HingeShutterPanelProfiles removedValue = this.boardProfile.removeDefaultValue();
        HingeShutterPanelProfiles currentValue = this.boardProfile.getValue();

        fireBoardProfileChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<HingeShutterPanelProfiles> getBoardProfileValueContainer()
    {
        return this.boardProfile;
    }
    public final void fireBoardProfileChange(HingeShutterPanelProfiles currentValue, HingeShutterPanelProfiles oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeBoardProfileChanged(currentValue);
            firePropertyChange(PROPERTYNAME_BOARDPROFILE, oldValue, currentValue);
            afterBoardProfileChanged(currentValue);
        }
    }

    public void beforeBoardProfileChanged( HingeShutterPanelProfiles boardProfile)
    { }
    public void afterBoardProfileChanged( HingeShutterPanelProfiles boardProfile)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Board color")
        ,@LocalizedTag(language="fr", name="label", value="Couleur des planches")
    })
    public final com.client360.configuration.wad.StandardColor getBoardColor()
    {
         return this.boardColor.getValue();
    }

    public final void setBoardColor(com.client360.configuration.wad.StandardColor boardColor)
    {

        if (boardColor != null)
        {
            acquire(boardColor, "boardColor");
        }
        com.client360.configuration.wad.StandardColor oldValue = this.boardColor.getValue();
        this.boardColor.setValue(boardColor);

        com.client360.configuration.wad.StandardColor currentValue = this.boardColor.getValue();

        fireBoardColorChange(currentValue, oldValue);
    }
    public final com.client360.configuration.wad.StandardColor removeBoardColor()
    {
        com.client360.configuration.wad.StandardColor oldValue = this.boardColor.getValue();
        com.client360.configuration.wad.StandardColor removedValue = this.boardColor.removeValue();
        com.client360.configuration.wad.StandardColor currentValue = this.boardColor.getValue();

        fireBoardColorChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultBoardColor(com.client360.configuration.wad.StandardColor boardColor)
    {
        if (boardColor != null)
        {
            acquire(boardColor, "boardColor");
        }
        com.client360.configuration.wad.StandardColor oldValue = this.boardColor.getValue();
        this.boardColor.setDefaultValue(boardColor);

        com.client360.configuration.wad.StandardColor currentValue = this.boardColor.getValue();

        fireBoardColorChange(currentValue, oldValue);
    }
    
    public final com.client360.configuration.wad.StandardColor removeDefaultBoardColor()
    {
        com.client360.configuration.wad.StandardColor oldValue = this.boardColor.getValue();
        com.client360.configuration.wad.StandardColor removedValue = this.boardColor.removeDefaultValue();
        com.client360.configuration.wad.StandardColor currentValue = this.boardColor.getValue();

        fireBoardColorChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<com.client360.configuration.wad.StandardColor> getBoardColorValueContainer()
    {
        return this.boardColor;
    }
    public final void fireBoardColorChange(com.client360.configuration.wad.StandardColor currentValue, com.client360.configuration.wad.StandardColor oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeBoardColorChanged(currentValue);
            firePropertyChange(PROPERTYNAME_BOARDCOLOR, oldValue, currentValue);
            afterBoardColorChanged(currentValue);
        }
    }

    public void beforeBoardColorChanged( com.client360.configuration.wad.StandardColor boardColor)
    { }
    public void afterBoardColorChanged( com.client360.configuration.wad.StandardColor boardColor)
    { }



    @Override
    protected String getSequences()
    {
        return "form,offsetLeft,offsetRight,offsetTop,offsetSill,perimeterLeft,perimeterRight,perimeterTop,perimeterSill,byzanceTop,byzanceSill,perimeterProfile,perimeterColor,panelModelCornerTypes,boardHorizontal,boardWidth,boardSpacing,boardProfile,zefiralBoardWidth,not set,boardColor";  
    }
}
