// generated class, do not edit

package com.client360.configuration.hingeshutters;

//Plugin V14.0.1

// Imports 

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class PanelsBase extends com.netappsid.configuration.hingeshutters.Panels
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(PanelsBase.class);

    // attributes
    private ValueContainer<Measure<Length>> offsetFromProduct = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Boolean> displayStay = new ValueContainer<Boolean>(this);
    private ValueContainer<Boolean> displayHinges = new ValueContainer<Boolean>(this);

    // Bound properties

    public static final String PROPERTYNAME_OFFSETFROMPRODUCT = "offsetFromProduct";  
    public static final String PROPERTYNAME_DISPLAYSTAY = "displayStay";  
    public static final String PROPERTYNAME_DISPLAYHINGES = "displayHinges";  

    // Constructors

    public PanelsBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.hingeshutters.Panels"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Offset from product")
        ,@LocalizedTag(language="fr", name="label", value="D�bord du produit")
    })
    public final Measure<Length> getOffsetFromProduct()
    {
         return this.offsetFromProduct.getValue();
    }

    public final void setOffsetFromProduct(Measure<Length> offsetFromProduct)
    {

        offsetFromProduct = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(offsetFromProduct, getDefaultUnitFor(PROPERTYNAME_OFFSETFROMPRODUCT,Length.class));

        Measure<Length> oldValue = this.offsetFromProduct.getValue();
        this.offsetFromProduct.setValue(offsetFromProduct);

        Measure<Length> currentValue = this.offsetFromProduct.getValue();

        fireOffsetFromProductChange(currentValue, oldValue);
    }
    public final Measure<Length> removeOffsetFromProduct()
    {
        Measure<Length> oldValue = this.offsetFromProduct.getValue();
        Measure<Length> removedValue = this.offsetFromProduct.removeValue();
        Measure<Length> currentValue = this.offsetFromProduct.getValue();

        fireOffsetFromProductChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultOffsetFromProduct(Measure<Length> offsetFromProduct)
    {
        offsetFromProduct = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(offsetFromProduct, getDefaultUnitFor(PROPERTYNAME_OFFSETFROMPRODUCT,Length.class));

        Measure<Length> oldValue = this.offsetFromProduct.getValue();
        this.offsetFromProduct.setDefaultValue(offsetFromProduct);

        Measure<Length> currentValue = this.offsetFromProduct.getValue();

        fireOffsetFromProductChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultOffsetFromProduct()
    {
        Measure<Length> oldValue = this.offsetFromProduct.getValue();
        Measure<Length> removedValue = this.offsetFromProduct.removeDefaultValue();
        Measure<Length> currentValue = this.offsetFromProduct.getValue();

        fireOffsetFromProductChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getOffsetFromProductValueContainer()
    {
        return this.offsetFromProduct;
    }
    public final void fireOffsetFromProductChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeOffsetFromProductChanged(currentValue);
            firePropertyChange(PROPERTYNAME_OFFSETFROMPRODUCT, oldValue, currentValue);
            afterOffsetFromProductChanged(currentValue);
        }
    }

    public void beforeOffsetFromProductChanged( Measure<Length> offsetFromProduct)
    { }
    public void afterOffsetFromProductChanged( Measure<Length> offsetFromProduct)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Display stay")
        ,@LocalizedTag(language="fr", name="label", value="Afficher espagnolette")
    })
    public final Boolean getDisplayStay()
    {
         return this.displayStay.getValue();
    }

    public final void setDisplayStay(Boolean displayStay)
    {

        Boolean oldValue = this.displayStay.getValue();
        this.displayStay.setValue(displayStay);

        Boolean currentValue = this.displayStay.getValue();

        fireDisplayStayChange(currentValue, oldValue);
    }
    public final Boolean removeDisplayStay()
    {
        Boolean oldValue = this.displayStay.getValue();
        Boolean removedValue = this.displayStay.removeValue();
        Boolean currentValue = this.displayStay.getValue();

        fireDisplayStayChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultDisplayStay(Boolean displayStay)
    {
        Boolean oldValue = this.displayStay.getValue();
        this.displayStay.setDefaultValue(displayStay);

        Boolean currentValue = this.displayStay.getValue();

        fireDisplayStayChange(currentValue, oldValue);
    }
    
    public final Boolean removeDefaultDisplayStay()
    {
        Boolean oldValue = this.displayStay.getValue();
        Boolean removedValue = this.displayStay.removeDefaultValue();
        Boolean currentValue = this.displayStay.getValue();

        fireDisplayStayChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Boolean> getDisplayStayValueContainer()
    {
        return this.displayStay;
    }
    public final void fireDisplayStayChange(Boolean currentValue, Boolean oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeDisplayStayChanged(currentValue);
            firePropertyChange(PROPERTYNAME_DISPLAYSTAY, oldValue, currentValue);
            afterDisplayStayChanged(currentValue);
        }
    }

    public void beforeDisplayStayChanged( Boolean displayStay)
    { }
    public void afterDisplayStayChanged( Boolean displayStay)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Display hinges")
        ,@LocalizedTag(language="fr", name="label", value="Afficher pentures")
    })
    public final Boolean getDisplayHinges()
    {
         return this.displayHinges.getValue();
    }

    public final void setDisplayHinges(Boolean displayHinges)
    {

        Boolean oldValue = this.displayHinges.getValue();
        this.displayHinges.setValue(displayHinges);

        Boolean currentValue = this.displayHinges.getValue();

        fireDisplayHingesChange(currentValue, oldValue);
    }
    public final Boolean removeDisplayHinges()
    {
        Boolean oldValue = this.displayHinges.getValue();
        Boolean removedValue = this.displayHinges.removeValue();
        Boolean currentValue = this.displayHinges.getValue();

        fireDisplayHingesChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultDisplayHinges(Boolean displayHinges)
    {
        Boolean oldValue = this.displayHinges.getValue();
        this.displayHinges.setDefaultValue(displayHinges);

        Boolean currentValue = this.displayHinges.getValue();

        fireDisplayHingesChange(currentValue, oldValue);
    }
    
    public final Boolean removeDefaultDisplayHinges()
    {
        Boolean oldValue = this.displayHinges.getValue();
        Boolean removedValue = this.displayHinges.removeDefaultValue();
        Boolean currentValue = this.displayHinges.getValue();

        fireDisplayHingesChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Boolean> getDisplayHingesValueContainer()
    {
        return this.displayHinges;
    }
    public final void fireDisplayHingesChange(Boolean currentValue, Boolean oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeDisplayHingesChanged(currentValue);
            firePropertyChange(PROPERTYNAME_DISPLAYHINGES, oldValue, currentValue);
            afterDisplayHingesChanged(currentValue);
        }
    }

    public void beforeDisplayHingesChanged( Boolean displayHinges)
    { }
    public void afterDisplayHingesChanged( Boolean displayHinges)
    { }



    @Override
    protected String getSequences()
    {
        return "dimension,interiorColor,exteriorColor,offset,offsetFromProduct,open,displayStay,displayHinges,astragal,nbPanels,gap,panel";  
    }
}
