// generated class, do not edit

package com.client360.configuration.hingeshutters;

//Plugin V14.0.1

// Imports 

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class LouveredBase extends com.client360.configuration.hingeshutters.BoardModel
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(LouveredBase.class);

    // attributes
    private ValueContainer<Boolean> mullion = new ValueContainer<Boolean>(this);
    private ValueContainer<Measure<Length>> mullionWidth = new ValueContainer<Measure<Length>>(this);

    // Bound properties

    public static final String PROPERTYNAME_MULLION = "mullion";  
    public static final String PROPERTYNAME_MULLIONWIDTH = "mullionWidth";  

    // Constructors

    public LouveredBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.hingeshutters.Louvered"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="With mullion")
        ,@LocalizedTag(language="fr", name="label", value="Avec montantin")
    })
    public final Boolean getMullion()
    {
         return this.mullion.getValue();
    }

    public final void setMullion(Boolean mullion)
    {

        Boolean oldValue = this.mullion.getValue();
        this.mullion.setValue(mullion);

        Boolean currentValue = this.mullion.getValue();

        fireMullionChange(currentValue, oldValue);
    }
    public final Boolean removeMullion()
    {
        Boolean oldValue = this.mullion.getValue();
        Boolean removedValue = this.mullion.removeValue();
        Boolean currentValue = this.mullion.getValue();

        fireMullionChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultMullion(Boolean mullion)
    {
        Boolean oldValue = this.mullion.getValue();
        this.mullion.setDefaultValue(mullion);

        Boolean currentValue = this.mullion.getValue();

        fireMullionChange(currentValue, oldValue);
    }
    
    public final Boolean removeDefaultMullion()
    {
        Boolean oldValue = this.mullion.getValue();
        Boolean removedValue = this.mullion.removeDefaultValue();
        Boolean currentValue = this.mullion.getValue();

        fireMullionChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Boolean> getMullionValueContainer()
    {
        return this.mullion;
    }
    public final void fireMullionChange(Boolean currentValue, Boolean oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeMullionChanged(currentValue);
            firePropertyChange(PROPERTYNAME_MULLION, oldValue, currentValue);
            afterMullionChanged(currentValue);
        }
    }

    public void beforeMullionChanged( Boolean mullion)
    { }
    public void afterMullionChanged( Boolean mullion)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Mullion width")
        ,@LocalizedTag(language="fr", name="label", value="Largeur du montantin")
    })
    public final Measure<Length> getMullionWidth()
    {
         return this.mullionWidth.getValue();
    }

    public final void setMullionWidth(Measure<Length> mullionWidth)
    {

        mullionWidth = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(mullionWidth, getDefaultUnitFor(PROPERTYNAME_MULLIONWIDTH,Length.class));

        Measure<Length> oldValue = this.mullionWidth.getValue();
        this.mullionWidth.setValue(mullionWidth);

        Measure<Length> currentValue = this.mullionWidth.getValue();

        fireMullionWidthChange(currentValue, oldValue);
    }
    public final Measure<Length> removeMullionWidth()
    {
        Measure<Length> oldValue = this.mullionWidth.getValue();
        Measure<Length> removedValue = this.mullionWidth.removeValue();
        Measure<Length> currentValue = this.mullionWidth.getValue();

        fireMullionWidthChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultMullionWidth(Measure<Length> mullionWidth)
    {
        mullionWidth = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(mullionWidth, getDefaultUnitFor(PROPERTYNAME_MULLIONWIDTH,Length.class));

        Measure<Length> oldValue = this.mullionWidth.getValue();
        this.mullionWidth.setDefaultValue(mullionWidth);

        Measure<Length> currentValue = this.mullionWidth.getValue();

        fireMullionWidthChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultMullionWidth()
    {
        Measure<Length> oldValue = this.mullionWidth.getValue();
        Measure<Length> removedValue = this.mullionWidth.removeDefaultValue();
        Measure<Length> currentValue = this.mullionWidth.getValue();

        fireMullionWidthChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getMullionWidthValueContainer()
    {
        return this.mullionWidth;
    }
    public final void fireMullionWidthChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeMullionWidthChanged(currentValue);
            firePropertyChange(PROPERTYNAME_MULLIONWIDTH, oldValue, currentValue);
            afterMullionWidthChanged(currentValue);
        }
    }

    public void beforeMullionWidthChanged( Measure<Length> mullionWidth)
    { }
    public void afterMullionWidthChanged( Measure<Length> mullionWidth)
    { }



    @Override
    protected String getSequences()
    {
        return "form,offsetLeft,offsetRight,offsetTop,offsetSill,perimeterLeft,perimeterRight,perimeterTop,perimeterSill,byzanceTop,byzanceSill,perimeterProfile,perimeterColor,panelModelCornerTypes,boardHorizontal,boardWidth,boardSpacing,boardProfile,zefiralBoardWidth,boardColor,mullion,mullionWidth,customVisualComposition";  
    }
}
