// generated class, do not edit

package com.client360.configuration.hingeshutters.enums;

// Imports 


public enum PanelModelCornerTypes
{
    @SuppressWarnings("nls") FULL_HORIZONTAL("Plein horizontal", "Full horizontal",  ""),
    @SuppressWarnings("nls") FULL_VERTICAL("Plein vertical", "Full vertical",  ""),
    @SuppressWarnings("nls") HALF_CORNER("Demi-coin", "Half corner",  "");

    public String label_fr;
    public String label_en;
    public String image;
    
    PanelModelCornerTypes(String label_fr, String label_en, String image)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
    }

}

