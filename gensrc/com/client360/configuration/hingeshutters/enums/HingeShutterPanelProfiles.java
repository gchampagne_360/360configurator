// generated class, do not edit

package com.client360.configuration.hingeshutters.enums;

// Imports 
import com.netappsid.rendering.common.advanced.Profile;


public enum HingeShutterPanelProfiles
{
    @SuppressWarnings("nls") REGULAR("R�gulier", "Regular",  "", com.netappsid.cad.profile.HingeShutterProfileA.class),
    @SuppressWarnings("nls") BEVEL("Avec biseau", "With bevel",  "", com.netappsid.cad.profile.HingeShutterProfileB.class),
    @SuppressWarnings("nls") FLAT("Plat", "Flat",  "", com.netappsid.cad.profile.HingeShutterProfileFlat.class),
    @SuppressWarnings("nls") AMERICAN("� l\'am�ricaine", "American",  "", com.netappsid.cad.profile.HingeShutterWesternBladeProfile.class),
    @SuppressWarnings("nls") FRENCH("Fran�aise", "French",  "", com.netappsid.cad.profile.HingeShutterFrenchBladeProfile.class),
    @SuppressWarnings("nls") ALUMINIUM("Z�firal Alu", "Aluminium",  "", com.netappsid.cad.profile.HingeShutterAluminium.class);

    public String label_fr;
    public String label_en;
    public String image;
    public Class<? extends Profile> profileClass;
    
    HingeShutterPanelProfiles(String label_fr, String label_en, String image, Class<? extends Profile> profileClass)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
        this.profileClass = profileClass;
    }

}

