// generated class, do not edit

package com.client360.configuration.hingeshutters.enums;

// Imports 
import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.commonutils.math.Units;
import com.netappsid.rendering.common.advanced.Profile;


public enum LouveredModels
{
    @SuppressWarnings("nls") ONLY_BOARDS("Planches seulement", "Only boards", "", "", "", "", "", "", null, null, null, null, Units.inch(1.25d), null, Units.inch(1.5d), Units.ONEIN, Units.ONEIN),
    @SuppressWarnings("nls") WESTERN("Am�ricaine non-ajour�", "Western", "", "", "", "", "", "", com.netappsid.cad.profile.HingeShutterProfileFlat.class, Units.inch(0.5d), Units.inch(0.5d), Units.inch(0.5d), Units.inch(0.75d), com.netappsid.cad.profile.HingeShutterWesternBladeProfile.class, Units.inch(1.5d), Units.ONEIN, Units.ONEIN),
    @SuppressWarnings("nls") WESTERN_BASE("Am�ricaine avec sous-bassement", "Western with base", "", "", "", "", "", "", com.netappsid.cad.profile.HingeShutterProfileFlat.class, Units.inch(0.5d), Units.inch(4d), Units.inch(0.5d), Units.inch(1.5), com.netappsid.cad.profile.HingeShutterWesternBladeProfile.class, Units.inch(1.5d), Units.ONEIN, Units.ONEIN),
    @SuppressWarnings("nls") FRENCH("Fran�aise non-ajour�", "French", "", "", "", "", "", "", com.netappsid.cad.profile.HingeShutterProfileFlat.class, Units.inch(0.5d), Units.inch(0.5d), Units.inch(0.5d), Units.inch(1.5d), com.netappsid.cad.profile.HingeShutterFrenchBladeProfile.class, Units.inch(1.5d), Units.ONEIN, Units.ONEIN),
    @SuppressWarnings("nls") PROVENCAL("Proven�al", "Provencal", "", "", "", "", "", "", com.netappsid.cad.profile.HingeShutterProfileFlat.class, null, null, null, Units.inch(1.25d), null, Units.inch(1.5d), Units.ONEIN, Units.ONEIN);

    public String label_fr;
    public String label_en;
    public String defaultChoice;
    public String enabled;
    public String messageEnabled_fr;
    public String messageEnabled_en;
    public String price;
    public String image;
    public Class<? extends Profile> perimeterProfile;
    public Measure<Length> gapTop;
    public Measure<Length> gapSill;
    public Measure<Length> gapSide;
    public Measure<Length> boardWidth;
    public Class<? extends Profile> boardProfile;
    public Measure<Length> openingGapTop;
    public Measure<Length> openingGapSill;
    public Measure<Length> openingGapSides;
    
    LouveredModels(String label_fr, String label_en, String defaultChoice, String enabled, String messageEnabled_fr, String messageEnabled_en, String price, String image, Class<? extends Profile> perimeterProfile, Measure<Length> gapTop, Measure<Length> gapSill, Measure<Length> gapSide, Measure<Length> boardWidth, Class<? extends Profile> boardProfile, Measure<Length> openingGapTop, Measure<Length> openingGapSill, Measure<Length> openingGapSides)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.defaultChoice = defaultChoice;
        this.enabled = enabled;
        this.messageEnabled_fr = messageEnabled_fr;
        this.messageEnabled_en = messageEnabled_en;
        this.price = price;
        this.image = image;
        this.perimeterProfile = perimeterProfile;
        this.gapTop = gapTop;
        this.gapSill = gapSill;
        this.gapSide = gapSide;
        this.boardWidth = boardWidth;
        this.boardProfile = boardProfile;
        this.openingGapTop = openingGapTop;
        this.openingGapSill = openingGapSill;
        this.openingGapSides = openingGapSides;
    }

}

