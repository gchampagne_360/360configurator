// generated class, do not edit

package com.client360.configuration.hingeshutters.enums;

import com.netappsid.configuration.hingeshutters.PanelModel;

// Imports 


public enum HingeShutterPanelTypes
{
    @SuppressWarnings("nls") LOUVERED("Persienn�", "Louvered",  "", com.client360.configuration.hingeshutters.Louvered.class),
    @SuppressWarnings("nls") BOARD_N_BATTEN("Traverses et �charpes", "Board and batten",  "", com.client360.configuration.hingeshutters.BoardNBatten.class),
    @SuppressWarnings("nls") OPENING_PANEL("Ouvrant � la ni�oise", "Opening panel",  "", com.client360.configuration.hingeshutters.OpeningPanel.class),
    @SuppressWarnings("nls") PROVENCAL("Proven�al", "Provencal",  "", com.client360.configuration.hingeshutters.Provencal.class);

    public String label_fr;
    public String label_en;
    public String image;
    public Class<? extends PanelModel> modelClass;
    
    HingeShutterPanelTypes(String label_fr, String label_en, String image, Class<? extends PanelModel> modelClass)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
        this.modelClass = modelClass;
    }

}

