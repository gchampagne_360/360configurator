// generated class, do not edit

package com.client360.configuration.hingeshutters.enums;

// Imports 
import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.commonutils.math.Units;
import com.netappsid.rendering.common.advanced.Profile;


public enum BoardNBattenModels
{
    @SuppressWarnings("nls") ONLY_BOARDS("Planches seulement", "Only boards", "", "", "", "", "", "", Units.inch(0.5d), Units.inch(0.5d), Units.inch(0.5d), null, Units.inch(2d), com.netappsid.cad.profile.HingeShutterProfileA.class, 0, null, null, null, null, null, false, null),
    @SuppressWarnings("nls") TWO_BATTENS("Deux traverses", "Two muntins", "", "", "", "", "", "", null, null, null, null, Units.inch(1.5d), com.netappsid.cad.profile.HingeShutterProfileA.class, 2, null, com.netappsid.cad.profile.HingeShutterProfileA.class, Units.inch(3d), Units.inch(3d), Units.inch(1d), false, null),
    @SuppressWarnings("nls") THREE_BATTENS("Trois traverses", "Three muntins", "", "", "", "", "", "", Units.inch(0.5d), Units.inch(0.5d), Units.inch(0.5d), null, Units.inch(1.5d), null, 3, null, com.netappsid.cad.profile.HingeShutterProfileB.class, Units.inch(3d), Units.inch(3d), Units.inch(1d), false, null),
    @SuppressWarnings("nls") TWO_BATTENS_AND_Z_BAR("Deux traverses avec �charpe", "Two muntins and Z-bar", "", "", "", "", "", "", Units.inch(0.5d), Units.inch(0.5d), Units.inch(0.5d), null, Units.inch(1.5d), com.netappsid.cad.profile.HingeShutterProfileA.class, 2, null, com.netappsid.cad.profile.HingeShutterProfileA.class, Units.inch(2d), Units.inch(2d), Units.inch(2d), true, null),
    @SuppressWarnings("nls") THREE_BATTENS_AND_Z_BARS("Trois traverses avec �charpes", "Three muntins and Z-bars", "", "", "", "", "", "", null, null, null, null, Units.inch(1.5d), com.netappsid.cad.profile.HingeShutterProfileA.class, 3, null, com.netappsid.cad.profile.HingeShutterProfileB.class, Units.inch(1d), Units.inch(1d), Units.inch(1d), true, null);

    public String label_fr;
    public String label_en;
    public String defaultChoice;
    public String enabled;
    public String messageEnabled_fr;
    public String messageEnabled_en;
    public String price;
    public String image;
    public Measure<Length> gapTop;
    public Measure<Length> gapSide;
    public Measure<Length> gapSill;
    public Class<? extends Profile> perimeterProfile;
    public Measure<Length> boardWidth;
    public Class<? extends Profile> boardProfile;
    public Integer nbBattens;
    public Measure<Length> battenWidth;
    public Class<? extends Profile> battenProfile;
    public Measure<Length> battenGapTop;
    public Measure<Length> battenGapSill;
    public Measure<Length> battenGapSide;
    public Boolean zBars;
    public Measure<Length> zBarWidth;
    
    BoardNBattenModels(String label_fr, String label_en, String defaultChoice, String enabled, String messageEnabled_fr, String messageEnabled_en, String price, String image, Measure<Length> gapTop, Measure<Length> gapSide, Measure<Length> gapSill, Class<? extends Profile> perimeterProfile, Measure<Length> boardWidth, Class<? extends Profile> boardProfile, Integer nbBattens, Measure<Length> battenWidth, Class<? extends Profile> battenProfile, Measure<Length> battenGapTop, Measure<Length> battenGapSill, Measure<Length> battenGapSide, Boolean zBars, Measure<Length> zBarWidth)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.defaultChoice = defaultChoice;
        this.enabled = enabled;
        this.messageEnabled_fr = messageEnabled_fr;
        this.messageEnabled_en = messageEnabled_en;
        this.price = price;
        this.image = image;
        this.gapTop = gapTop;
        this.gapSide = gapSide;
        this.gapSill = gapSill;
        this.perimeterProfile = perimeterProfile;
        this.boardWidth = boardWidth;
        this.boardProfile = boardProfile;
        this.nbBattens = nbBattens;
        this.battenWidth = battenWidth;
        this.battenProfile = battenProfile;
        this.battenGapTop = battenGapTop;
        this.battenGapSill = battenGapSill;
        this.battenGapSide = battenGapSide;
        this.zBars = zBars;
        this.zBarWidth = zBarWidth;
    }

}

