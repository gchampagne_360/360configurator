// generated class, do not edit

package com.client360.configuration.hingeshutters.enums;

// Imports 


public enum PintleShutterFrameTypes
{
    @SuppressWarnings("nls") DEFAULT_FRAME("C�dre par d�faut", "Default frame",  ""),
    @SuppressWarnings("nls") WITH_BEVEL("Avec biseau", "With bevel",  "");

    public String label_fr;
    public String label_en;
    public String image;
    
    PintleShutterFrameTypes(String label_fr, String label_en, String image)
    {
        this.label_fr = label_fr;
        this.label_en = label_en;
        this.image = image;
    }

}

