// generated class, do not edit

package com.client360.configuration.hingeshutters;

//Plugin V14.0.1

// Imports 

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class OpeningPanelBase extends com.client360.configuration.hingeshutters.Louvered
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(OpeningPanelBase.class);

    // attributes
    private ValueContainer<Measure<Length>> offsetSideHinge = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Measure<Length>> offsetTopHinge = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Measure<Length>> imageHingeWidth = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Measure<Length>> imageHingeHeight = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Measure<Length>> openingLeft = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Measure<Length>> openingRight = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Measure<Length>> openingTop = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Measure<Length>> openingSill = new ValueContainer<Measure<Length>>(this);

    // Bound properties

    public static final String PROPERTYNAME_OFFSETSIDEHINGE = "offsetSideHinge";  
    public static final String PROPERTYNAME_OFFSETTOPHINGE = "offsetTopHinge";  
    public static final String PROPERTYNAME_IMAGEHINGEWIDTH = "imageHingeWidth";  
    public static final String PROPERTYNAME_IMAGEHINGEHEIGHT = "imageHingeHeight";  
    public static final String PROPERTYNAME_OPENINGLEFT = "openingLeft";  
    public static final String PROPERTYNAME_OPENINGRIGHT = "openingRight";  
    public static final String PROPERTYNAME_OPENINGTOP = "openingTop";  
    public static final String PROPERTYNAME_OPENINGSILL = "openingSill";  

    // Constructors

    public OpeningPanelBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.hingeshutters.OpeningPanel"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Hinge side offset")
        ,@LocalizedTag(language="fr", name="label", value="D�callage c�t� penture")
    })
    public final Measure<Length> getOffsetSideHinge()
    {
         return this.offsetSideHinge.getValue();
    }

    public final void setOffsetSideHinge(Measure<Length> offsetSideHinge)
    {

        offsetSideHinge = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(offsetSideHinge, getDefaultUnitFor(PROPERTYNAME_OFFSETSIDEHINGE,Length.class));

        Measure<Length> oldValue = this.offsetSideHinge.getValue();
        this.offsetSideHinge.setValue(offsetSideHinge);

        Measure<Length> currentValue = this.offsetSideHinge.getValue();

        fireOffsetSideHingeChange(currentValue, oldValue);
    }
    public final Measure<Length> removeOffsetSideHinge()
    {
        Measure<Length> oldValue = this.offsetSideHinge.getValue();
        Measure<Length> removedValue = this.offsetSideHinge.removeValue();
        Measure<Length> currentValue = this.offsetSideHinge.getValue();

        fireOffsetSideHingeChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultOffsetSideHinge(Measure<Length> offsetSideHinge)
    {
        offsetSideHinge = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(offsetSideHinge, getDefaultUnitFor(PROPERTYNAME_OFFSETSIDEHINGE,Length.class));

        Measure<Length> oldValue = this.offsetSideHinge.getValue();
        this.offsetSideHinge.setDefaultValue(offsetSideHinge);

        Measure<Length> currentValue = this.offsetSideHinge.getValue();

        fireOffsetSideHingeChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultOffsetSideHinge()
    {
        Measure<Length> oldValue = this.offsetSideHinge.getValue();
        Measure<Length> removedValue = this.offsetSideHinge.removeDefaultValue();
        Measure<Length> currentValue = this.offsetSideHinge.getValue();

        fireOffsetSideHingeChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getOffsetSideHingeValueContainer()
    {
        return this.offsetSideHinge;
    }
    public final void fireOffsetSideHingeChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeOffsetSideHingeChanged(currentValue);
            firePropertyChange(PROPERTYNAME_OFFSETSIDEHINGE, oldValue, currentValue);
            afterOffsetSideHingeChanged(currentValue);
        }
    }

    public void beforeOffsetSideHingeChanged( Measure<Length> offsetSideHinge)
    { }
    public void afterOffsetSideHingeChanged( Measure<Length> offsetSideHinge)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Hinge top offset")
        ,@LocalizedTag(language="fr", name="label", value="D�callage haut penture")
    })
    public final Measure<Length> getOffsetTopHinge()
    {
         return this.offsetTopHinge.getValue();
    }

    public final void setOffsetTopHinge(Measure<Length> offsetTopHinge)
    {

        offsetTopHinge = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(offsetTopHinge, getDefaultUnitFor(PROPERTYNAME_OFFSETTOPHINGE,Length.class));

        Measure<Length> oldValue = this.offsetTopHinge.getValue();
        this.offsetTopHinge.setValue(offsetTopHinge);

        Measure<Length> currentValue = this.offsetTopHinge.getValue();

        fireOffsetTopHingeChange(currentValue, oldValue);
    }
    public final Measure<Length> removeOffsetTopHinge()
    {
        Measure<Length> oldValue = this.offsetTopHinge.getValue();
        Measure<Length> removedValue = this.offsetTopHinge.removeValue();
        Measure<Length> currentValue = this.offsetTopHinge.getValue();

        fireOffsetTopHingeChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultOffsetTopHinge(Measure<Length> offsetTopHinge)
    {
        offsetTopHinge = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(offsetTopHinge, getDefaultUnitFor(PROPERTYNAME_OFFSETTOPHINGE,Length.class));

        Measure<Length> oldValue = this.offsetTopHinge.getValue();
        this.offsetTopHinge.setDefaultValue(offsetTopHinge);

        Measure<Length> currentValue = this.offsetTopHinge.getValue();

        fireOffsetTopHingeChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultOffsetTopHinge()
    {
        Measure<Length> oldValue = this.offsetTopHinge.getValue();
        Measure<Length> removedValue = this.offsetTopHinge.removeDefaultValue();
        Measure<Length> currentValue = this.offsetTopHinge.getValue();

        fireOffsetTopHingeChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getOffsetTopHingeValueContainer()
    {
        return this.offsetTopHinge;
    }
    public final void fireOffsetTopHingeChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeOffsetTopHingeChanged(currentValue);
            firePropertyChange(PROPERTYNAME_OFFSETTOPHINGE, oldValue, currentValue);
            afterOffsetTopHingeChanged(currentValue);
        }
    }

    public void beforeOffsetTopHingeChanged( Measure<Length> offsetTopHinge)
    { }
    public void afterOffsetTopHingeChanged( Measure<Length> offsetTopHinge)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Image width")
        ,@LocalizedTag(language="fr", name="label", value="Largeur de l\'image")
    })
    public final Measure<Length> getImageHingeWidth()
    {
         return this.imageHingeWidth.getValue();
    }

    public final void setImageHingeWidth(Measure<Length> imageHingeWidth)
    {

        imageHingeWidth = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(imageHingeWidth, getDefaultUnitFor(PROPERTYNAME_IMAGEHINGEWIDTH,Length.class));

        Measure<Length> oldValue = this.imageHingeWidth.getValue();
        this.imageHingeWidth.setValue(imageHingeWidth);

        Measure<Length> currentValue = this.imageHingeWidth.getValue();

        fireImageHingeWidthChange(currentValue, oldValue);
    }
    public final Measure<Length> removeImageHingeWidth()
    {
        Measure<Length> oldValue = this.imageHingeWidth.getValue();
        Measure<Length> removedValue = this.imageHingeWidth.removeValue();
        Measure<Length> currentValue = this.imageHingeWidth.getValue();

        fireImageHingeWidthChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultImageHingeWidth(Measure<Length> imageHingeWidth)
    {
        imageHingeWidth = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(imageHingeWidth, getDefaultUnitFor(PROPERTYNAME_IMAGEHINGEWIDTH,Length.class));

        Measure<Length> oldValue = this.imageHingeWidth.getValue();
        this.imageHingeWidth.setDefaultValue(imageHingeWidth);

        Measure<Length> currentValue = this.imageHingeWidth.getValue();

        fireImageHingeWidthChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultImageHingeWidth()
    {
        Measure<Length> oldValue = this.imageHingeWidth.getValue();
        Measure<Length> removedValue = this.imageHingeWidth.removeDefaultValue();
        Measure<Length> currentValue = this.imageHingeWidth.getValue();

        fireImageHingeWidthChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getImageHingeWidthValueContainer()
    {
        return this.imageHingeWidth;
    }
    public final void fireImageHingeWidthChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeImageHingeWidthChanged(currentValue);
            firePropertyChange(PROPERTYNAME_IMAGEHINGEWIDTH, oldValue, currentValue);
            afterImageHingeWidthChanged(currentValue);
        }
    }

    public void beforeImageHingeWidthChanged( Measure<Length> imageHingeWidth)
    { }
    public void afterImageHingeWidthChanged( Measure<Length> imageHingeWidth)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Image height")
        ,@LocalizedTag(language="fr", name="label", value="Hauteur de l\'image")
    })
    public final Measure<Length> getImageHingeHeight()
    {
         return this.imageHingeHeight.getValue();
    }

    public final void setImageHingeHeight(Measure<Length> imageHingeHeight)
    {

        imageHingeHeight = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(imageHingeHeight, getDefaultUnitFor(PROPERTYNAME_IMAGEHINGEHEIGHT,Length.class));

        Measure<Length> oldValue = this.imageHingeHeight.getValue();
        this.imageHingeHeight.setValue(imageHingeHeight);

        Measure<Length> currentValue = this.imageHingeHeight.getValue();

        fireImageHingeHeightChange(currentValue, oldValue);
    }
    public final Measure<Length> removeImageHingeHeight()
    {
        Measure<Length> oldValue = this.imageHingeHeight.getValue();
        Measure<Length> removedValue = this.imageHingeHeight.removeValue();
        Measure<Length> currentValue = this.imageHingeHeight.getValue();

        fireImageHingeHeightChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultImageHingeHeight(Measure<Length> imageHingeHeight)
    {
        imageHingeHeight = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(imageHingeHeight, getDefaultUnitFor(PROPERTYNAME_IMAGEHINGEHEIGHT,Length.class));

        Measure<Length> oldValue = this.imageHingeHeight.getValue();
        this.imageHingeHeight.setDefaultValue(imageHingeHeight);

        Measure<Length> currentValue = this.imageHingeHeight.getValue();

        fireImageHingeHeightChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultImageHingeHeight()
    {
        Measure<Length> oldValue = this.imageHingeHeight.getValue();
        Measure<Length> removedValue = this.imageHingeHeight.removeDefaultValue();
        Measure<Length> currentValue = this.imageHingeHeight.getValue();

        fireImageHingeHeightChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getImageHingeHeightValueContainer()
    {
        return this.imageHingeHeight;
    }
    public final void fireImageHingeHeightChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeImageHingeHeightChanged(currentValue);
            firePropertyChange(PROPERTYNAME_IMAGEHINGEHEIGHT, oldValue, currentValue);
            afterImageHingeHeightChanged(currentValue);
        }
    }

    public void beforeImageHingeHeightChanged( Measure<Length> imageHingeHeight)
    { }
    public void afterImageHingeHeightChanged( Measure<Length> imageHingeHeight)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Opening left")
        ,@LocalizedTag(language="fr", name="label", value="Largeur gauche du panneau")
    })
    public final Measure<Length> getOpeningLeft()
    {
         return this.openingLeft.getValue();
    }

    public final void setOpeningLeft(Measure<Length> openingLeft)
    {

        openingLeft = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(openingLeft, getDefaultUnitFor(PROPERTYNAME_OPENINGLEFT,Length.class));

        Measure<Length> oldValue = this.openingLeft.getValue();
        this.openingLeft.setValue(openingLeft);

        Measure<Length> currentValue = this.openingLeft.getValue();

        fireOpeningLeftChange(currentValue, oldValue);
    }
    public final Measure<Length> removeOpeningLeft()
    {
        Measure<Length> oldValue = this.openingLeft.getValue();
        Measure<Length> removedValue = this.openingLeft.removeValue();
        Measure<Length> currentValue = this.openingLeft.getValue();

        fireOpeningLeftChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultOpeningLeft(Measure<Length> openingLeft)
    {
        openingLeft = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(openingLeft, getDefaultUnitFor(PROPERTYNAME_OPENINGLEFT,Length.class));

        Measure<Length> oldValue = this.openingLeft.getValue();
        this.openingLeft.setDefaultValue(openingLeft);

        Measure<Length> currentValue = this.openingLeft.getValue();

        fireOpeningLeftChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultOpeningLeft()
    {
        Measure<Length> oldValue = this.openingLeft.getValue();
        Measure<Length> removedValue = this.openingLeft.removeDefaultValue();
        Measure<Length> currentValue = this.openingLeft.getValue();

        fireOpeningLeftChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getOpeningLeftValueContainer()
    {
        return this.openingLeft;
    }
    public final void fireOpeningLeftChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeOpeningLeftChanged(currentValue);
            firePropertyChange(PROPERTYNAME_OPENINGLEFT, oldValue, currentValue);
            afterOpeningLeftChanged(currentValue);
        }
    }

    public void beforeOpeningLeftChanged( Measure<Length> openingLeft)
    { }
    public void afterOpeningLeftChanged( Measure<Length> openingLeft)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Opening right")
        ,@LocalizedTag(language="fr", name="label", value="Largeur droite du panneau")
    })
    public final Measure<Length> getOpeningRight()
    {
         return this.openingRight.getValue();
    }

    public final void setOpeningRight(Measure<Length> openingRight)
    {

        openingRight = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(openingRight, getDefaultUnitFor(PROPERTYNAME_OPENINGRIGHT,Length.class));

        Measure<Length> oldValue = this.openingRight.getValue();
        this.openingRight.setValue(openingRight);

        Measure<Length> currentValue = this.openingRight.getValue();

        fireOpeningRightChange(currentValue, oldValue);
    }
    public final Measure<Length> removeOpeningRight()
    {
        Measure<Length> oldValue = this.openingRight.getValue();
        Measure<Length> removedValue = this.openingRight.removeValue();
        Measure<Length> currentValue = this.openingRight.getValue();

        fireOpeningRightChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultOpeningRight(Measure<Length> openingRight)
    {
        openingRight = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(openingRight, getDefaultUnitFor(PROPERTYNAME_OPENINGRIGHT,Length.class));

        Measure<Length> oldValue = this.openingRight.getValue();
        this.openingRight.setDefaultValue(openingRight);

        Measure<Length> currentValue = this.openingRight.getValue();

        fireOpeningRightChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultOpeningRight()
    {
        Measure<Length> oldValue = this.openingRight.getValue();
        Measure<Length> removedValue = this.openingRight.removeDefaultValue();
        Measure<Length> currentValue = this.openingRight.getValue();

        fireOpeningRightChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getOpeningRightValueContainer()
    {
        return this.openingRight;
    }
    public final void fireOpeningRightChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeOpeningRightChanged(currentValue);
            firePropertyChange(PROPERTYNAME_OPENINGRIGHT, oldValue, currentValue);
            afterOpeningRightChanged(currentValue);
        }
    }

    public void beforeOpeningRightChanged( Measure<Length> openingRight)
    { }
    public void afterOpeningRightChanged( Measure<Length> openingRight)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Opening top")
        ,@LocalizedTag(language="fr", name="label", value="Largeur haut du panneau")
    })
    public final Measure<Length> getOpeningTop()
    {
         return this.openingTop.getValue();
    }

    public final void setOpeningTop(Measure<Length> openingTop)
    {

        openingTop = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(openingTop, getDefaultUnitFor(PROPERTYNAME_OPENINGTOP,Length.class));

        Measure<Length> oldValue = this.openingTop.getValue();
        this.openingTop.setValue(openingTop);

        Measure<Length> currentValue = this.openingTop.getValue();

        fireOpeningTopChange(currentValue, oldValue);
    }
    public final Measure<Length> removeOpeningTop()
    {
        Measure<Length> oldValue = this.openingTop.getValue();
        Measure<Length> removedValue = this.openingTop.removeValue();
        Measure<Length> currentValue = this.openingTop.getValue();

        fireOpeningTopChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultOpeningTop(Measure<Length> openingTop)
    {
        openingTop = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(openingTop, getDefaultUnitFor(PROPERTYNAME_OPENINGTOP,Length.class));

        Measure<Length> oldValue = this.openingTop.getValue();
        this.openingTop.setDefaultValue(openingTop);

        Measure<Length> currentValue = this.openingTop.getValue();

        fireOpeningTopChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultOpeningTop()
    {
        Measure<Length> oldValue = this.openingTop.getValue();
        Measure<Length> removedValue = this.openingTop.removeDefaultValue();
        Measure<Length> currentValue = this.openingTop.getValue();

        fireOpeningTopChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getOpeningTopValueContainer()
    {
        return this.openingTop;
    }
    public final void fireOpeningTopChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeOpeningTopChanged(currentValue);
            firePropertyChange(PROPERTYNAME_OPENINGTOP, oldValue, currentValue);
            afterOpeningTopChanged(currentValue);
        }
    }

    public void beforeOpeningTopChanged( Measure<Length> openingTop)
    { }
    public void afterOpeningTopChanged( Measure<Length> openingTop)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Opening sill")
        ,@LocalizedTag(language="fr", name="label", value="Largeur bas du panneau")
    })
    public final Measure<Length> getOpeningSill()
    {
         return this.openingSill.getValue();
    }

    public final void setOpeningSill(Measure<Length> openingSill)
    {

        openingSill = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(openingSill, getDefaultUnitFor(PROPERTYNAME_OPENINGSILL,Length.class));

        Measure<Length> oldValue = this.openingSill.getValue();
        this.openingSill.setValue(openingSill);

        Measure<Length> currentValue = this.openingSill.getValue();

        fireOpeningSillChange(currentValue, oldValue);
    }
    public final Measure<Length> removeOpeningSill()
    {
        Measure<Length> oldValue = this.openingSill.getValue();
        Measure<Length> removedValue = this.openingSill.removeValue();
        Measure<Length> currentValue = this.openingSill.getValue();

        fireOpeningSillChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultOpeningSill(Measure<Length> openingSill)
    {
        openingSill = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(openingSill, getDefaultUnitFor(PROPERTYNAME_OPENINGSILL,Length.class));

        Measure<Length> oldValue = this.openingSill.getValue();
        this.openingSill.setDefaultValue(openingSill);

        Measure<Length> currentValue = this.openingSill.getValue();

        fireOpeningSillChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultOpeningSill()
    {
        Measure<Length> oldValue = this.openingSill.getValue();
        Measure<Length> removedValue = this.openingSill.removeDefaultValue();
        Measure<Length> currentValue = this.openingSill.getValue();

        fireOpeningSillChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getOpeningSillValueContainer()
    {
        return this.openingSill;
    }
    public final void fireOpeningSillChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeOpeningSillChanged(currentValue);
            firePropertyChange(PROPERTYNAME_OPENINGSILL, oldValue, currentValue);
            afterOpeningSillChanged(currentValue);
        }
    }

    public void beforeOpeningSillChanged( Measure<Length> openingSill)
    { }
    public void afterOpeningSillChanged( Measure<Length> openingSill)
    { }



    @Override
    protected String getSequences()
    {
        return "form,offsetLeft,offsetRight,offsetTop,offsetSill,perimeterLeft,perimeterRight,perimeterTop,perimeterSill,byzanceTop,byzanceSill,perimeterProfile,perimeterColor,panelModelCornerTypes,boardHorizontal,boardWidth,boardSpacing,boardProfile,zefiralBoardWidth,boardColor,openingLeft,openingRight,openingTop,openingSill,offsetSideHinge,offsetTopHinge,imageHingeWidth,imageHingeHeight,mullionWidth,not set,mullion";  
    }
}
