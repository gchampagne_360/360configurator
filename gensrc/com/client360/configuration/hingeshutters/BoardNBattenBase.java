// generated class, do not edit

package com.client360.configuration.hingeshutters;

//Plugin V14.0.1

// Imports 

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.hingeshutters.enums.HingeShutterPanelProfiles;
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class BoardNBattenBase extends com.client360.configuration.hingeshutters.BoardModel
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(BoardNBattenBase.class);

    // attributes
    private ValueContainer<Integer> nbBattens = new ValueContainer<Integer>(this);
    private ValueContainer<Boolean> ZBars = new ValueContainer<Boolean>(this);
    private ValueContainer<Measure<Length>> zbarWidth = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Measure<Length>> ZBarOffsetTop = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Measure<Length>> ZBarOffsetSill = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Boolean> recess = new ValueContainer<Boolean>(this);
    private ValueContainer<Double> recessAngle = new ValueContainer<Double>(this);
    private ValueContainer<Measure<Length>> recessLength = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Measure<Length>> battensMidOffset = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Measure<Length>> battenWidth = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Measure<Length>> battenGapTop = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Measure<Length>> battenGapLeft = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Measure<Length>> battenGapRight = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Measure<Length>> battenGapSill = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<HingeShutterPanelProfiles> battenProfile = new ValueContainer<HingeShutterPanelProfiles>(this);
    private ValueContainer<com.client360.configuration.wad.StandardColor> battenColor = new ValueContainer<com.client360.configuration.wad.StandardColor>(this);

    // Bound properties

    public static final String PROPERTYNAME_NBBATTENS = "nbBattens";  
    public static final String PROPERTYNAME_ZBARS = "ZBars";  
    public static final String PROPERTYNAME_ZBARWIDTH = "zbarWidth";  
    public static final String PROPERTYNAME_ZBAROFFSETTOP = "ZBarOffsetTop";  
    public static final String PROPERTYNAME_ZBAROFFSETSILL = "ZBarOffsetSill";  
    public static final String PROPERTYNAME_RECESS = "recess";  
    public static final String PROPERTYNAME_RECESSANGLE = "recessAngle";  
    public static final String PROPERTYNAME_RECESSLENGTH = "recessLength";  
    public static final String PROPERTYNAME_BATTENSMIDOFFSET = "battensMidOffset";  
    public static final String PROPERTYNAME_BATTENWIDTH = "battenWidth";  
    public static final String PROPERTYNAME_BATTENGAPTOP = "battenGapTop";  
    public static final String PROPERTYNAME_BATTENGAPLEFT = "battenGapLeft";  
    public static final String PROPERTYNAME_BATTENGAPRIGHT = "battenGapRight";  
    public static final String PROPERTYNAME_BATTENGAPSILL = "battenGapSill";  
    public static final String PROPERTYNAME_BATTENPROFILE = "battenProfile";  
    public static final String PROPERTYNAME_BATTENCOLOR = "battenColor";  

    // Constructors

    public BoardNBattenBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.hingeshutters.BoardNBatten"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Battens count")
        ,@LocalizedTag(language="fr", name="label", value="Nombre de traverses")
    })
    public final Integer getNbBattens()
    {
         return this.nbBattens.getValue();
    }

    public final void setNbBattens(Integer nbBattens)
    {

        Integer oldValue = this.nbBattens.getValue();
        this.nbBattens.setValue(nbBattens);

        Integer currentValue = this.nbBattens.getValue();

        fireNbBattensChange(currentValue, oldValue);
    }
    public final Integer removeNbBattens()
    {
        Integer oldValue = this.nbBattens.getValue();
        Integer removedValue = this.nbBattens.removeValue();
        Integer currentValue = this.nbBattens.getValue();

        fireNbBattensChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultNbBattens(Integer nbBattens)
    {
        Integer oldValue = this.nbBattens.getValue();
        this.nbBattens.setDefaultValue(nbBattens);

        Integer currentValue = this.nbBattens.getValue();

        fireNbBattensChange(currentValue, oldValue);
    }
    
    public final Integer removeDefaultNbBattens()
    {
        Integer oldValue = this.nbBattens.getValue();
        Integer removedValue = this.nbBattens.removeDefaultValue();
        Integer currentValue = this.nbBattens.getValue();

        fireNbBattensChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Integer> getNbBattensValueContainer()
    {
        return this.nbBattens;
    }
    public final void fireNbBattensChange(Integer currentValue, Integer oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeNbBattensChanged(currentValue);
            firePropertyChange(PROPERTYNAME_NBBATTENS, oldValue, currentValue);
            afterNbBattensChanged(currentValue);
        }
    }

    public void beforeNbBattensChanged( Integer nbBattens)
    { }
    public void afterNbBattensChanged( Integer nbBattens)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Z bars")
        ,@LocalizedTag(language="fr", name="label", value="�charpes")
    })
    public final Boolean getZBars()
    {
         return this.ZBars.getValue();
    }

    public final void setZBars(Boolean ZBars)
    {

        Boolean oldValue = this.ZBars.getValue();
        this.ZBars.setValue(ZBars);

        Boolean currentValue = this.ZBars.getValue();

        fireZBarsChange(currentValue, oldValue);
    }
    public final Boolean removeZBars()
    {
        Boolean oldValue = this.ZBars.getValue();
        Boolean removedValue = this.ZBars.removeValue();
        Boolean currentValue = this.ZBars.getValue();

        fireZBarsChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultZBars(Boolean ZBars)
    {
        Boolean oldValue = this.ZBars.getValue();
        this.ZBars.setDefaultValue(ZBars);

        Boolean currentValue = this.ZBars.getValue();

        fireZBarsChange(currentValue, oldValue);
    }
    
    public final Boolean removeDefaultZBars()
    {
        Boolean oldValue = this.ZBars.getValue();
        Boolean removedValue = this.ZBars.removeDefaultValue();
        Boolean currentValue = this.ZBars.getValue();

        fireZBarsChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Boolean> getZBarsValueContainer()
    {
        return this.ZBars;
    }
    public final void fireZBarsChange(Boolean currentValue, Boolean oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeZBarsChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ZBARS, oldValue, currentValue);
            afterZBarsChanged(currentValue);
        }
    }

    public void beforeZBarsChanged( Boolean ZBars)
    { }
    public void afterZBarsChanged( Boolean ZBars)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Z bar width")
        ,@LocalizedTag(language="fr", name="label", value="Largeur des �charpes")
    })
    public final Measure<Length> getZbarWidth()
    {
         return this.zbarWidth.getValue();
    }

    public final void setZbarWidth(Measure<Length> zbarWidth)
    {

        zbarWidth = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(zbarWidth, getDefaultUnitFor(PROPERTYNAME_ZBARWIDTH,Length.class));

        Measure<Length> oldValue = this.zbarWidth.getValue();
        this.zbarWidth.setValue(zbarWidth);

        Measure<Length> currentValue = this.zbarWidth.getValue();

        fireZbarWidthChange(currentValue, oldValue);
    }
    public final Measure<Length> removeZbarWidth()
    {
        Measure<Length> oldValue = this.zbarWidth.getValue();
        Measure<Length> removedValue = this.zbarWidth.removeValue();
        Measure<Length> currentValue = this.zbarWidth.getValue();

        fireZbarWidthChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultZbarWidth(Measure<Length> zbarWidth)
    {
        zbarWidth = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(zbarWidth, getDefaultUnitFor(PROPERTYNAME_ZBARWIDTH,Length.class));

        Measure<Length> oldValue = this.zbarWidth.getValue();
        this.zbarWidth.setDefaultValue(zbarWidth);

        Measure<Length> currentValue = this.zbarWidth.getValue();

        fireZbarWidthChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultZbarWidth()
    {
        Measure<Length> oldValue = this.zbarWidth.getValue();
        Measure<Length> removedValue = this.zbarWidth.removeDefaultValue();
        Measure<Length> currentValue = this.zbarWidth.getValue();

        fireZbarWidthChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getZbarWidthValueContainer()
    {
        return this.zbarWidth;
    }
    public final void fireZbarWidthChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeZbarWidthChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ZBARWIDTH, oldValue, currentValue);
            afterZbarWidthChanged(currentValue);
        }
    }

    public void beforeZbarWidthChanged( Measure<Length> zbarWidth)
    { }
    public void afterZbarWidthChanged( Measure<Length> zbarWidth)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Z bar offset top")
        ,@LocalizedTag(language="fr", name="label", value="D�callage haut �charpe")
    })
    public final Measure<Length> getZBarOffsetTop()
    {
         return this.ZBarOffsetTop.getValue();
    }

    public final void setZBarOffsetTop(Measure<Length> ZBarOffsetTop)
    {

        ZBarOffsetTop = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(ZBarOffsetTop, getDefaultUnitFor(PROPERTYNAME_ZBAROFFSETTOP,Length.class));

        Measure<Length> oldValue = this.ZBarOffsetTop.getValue();
        this.ZBarOffsetTop.setValue(ZBarOffsetTop);

        Measure<Length> currentValue = this.ZBarOffsetTop.getValue();

        fireZBarOffsetTopChange(currentValue, oldValue);
    }
    public final Measure<Length> removeZBarOffsetTop()
    {
        Measure<Length> oldValue = this.ZBarOffsetTop.getValue();
        Measure<Length> removedValue = this.ZBarOffsetTop.removeValue();
        Measure<Length> currentValue = this.ZBarOffsetTop.getValue();

        fireZBarOffsetTopChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultZBarOffsetTop(Measure<Length> ZBarOffsetTop)
    {
        ZBarOffsetTop = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(ZBarOffsetTop, getDefaultUnitFor(PROPERTYNAME_ZBAROFFSETTOP,Length.class));

        Measure<Length> oldValue = this.ZBarOffsetTop.getValue();
        this.ZBarOffsetTop.setDefaultValue(ZBarOffsetTop);

        Measure<Length> currentValue = this.ZBarOffsetTop.getValue();

        fireZBarOffsetTopChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultZBarOffsetTop()
    {
        Measure<Length> oldValue = this.ZBarOffsetTop.getValue();
        Measure<Length> removedValue = this.ZBarOffsetTop.removeDefaultValue();
        Measure<Length> currentValue = this.ZBarOffsetTop.getValue();

        fireZBarOffsetTopChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getZBarOffsetTopValueContainer()
    {
        return this.ZBarOffsetTop;
    }
    public final void fireZBarOffsetTopChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeZBarOffsetTopChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ZBAROFFSETTOP, oldValue, currentValue);
            afterZBarOffsetTopChanged(currentValue);
        }
    }

    public void beforeZBarOffsetTopChanged( Measure<Length> ZBarOffsetTop)
    { }
    public void afterZBarOffsetTopChanged( Measure<Length> ZBarOffsetTop)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Z bar offset sill")
        ,@LocalizedTag(language="fr", name="label", value="D�callage bas �charpe")
    })
    public final Measure<Length> getZBarOffsetSill()
    {
         return this.ZBarOffsetSill.getValue();
    }

    public final void setZBarOffsetSill(Measure<Length> ZBarOffsetSill)
    {

        ZBarOffsetSill = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(ZBarOffsetSill, getDefaultUnitFor(PROPERTYNAME_ZBAROFFSETSILL,Length.class));

        Measure<Length> oldValue = this.ZBarOffsetSill.getValue();
        this.ZBarOffsetSill.setValue(ZBarOffsetSill);

        Measure<Length> currentValue = this.ZBarOffsetSill.getValue();

        fireZBarOffsetSillChange(currentValue, oldValue);
    }
    public final Measure<Length> removeZBarOffsetSill()
    {
        Measure<Length> oldValue = this.ZBarOffsetSill.getValue();
        Measure<Length> removedValue = this.ZBarOffsetSill.removeValue();
        Measure<Length> currentValue = this.ZBarOffsetSill.getValue();

        fireZBarOffsetSillChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultZBarOffsetSill(Measure<Length> ZBarOffsetSill)
    {
        ZBarOffsetSill = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(ZBarOffsetSill, getDefaultUnitFor(PROPERTYNAME_ZBAROFFSETSILL,Length.class));

        Measure<Length> oldValue = this.ZBarOffsetSill.getValue();
        this.ZBarOffsetSill.setDefaultValue(ZBarOffsetSill);

        Measure<Length> currentValue = this.ZBarOffsetSill.getValue();

        fireZBarOffsetSillChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultZBarOffsetSill()
    {
        Measure<Length> oldValue = this.ZBarOffsetSill.getValue();
        Measure<Length> removedValue = this.ZBarOffsetSill.removeDefaultValue();
        Measure<Length> currentValue = this.ZBarOffsetSill.getValue();

        fireZBarOffsetSillChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getZBarOffsetSillValueContainer()
    {
        return this.ZBarOffsetSill;
    }
    public final void fireZBarOffsetSillChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeZBarOffsetSillChanged(currentValue);
            firePropertyChange(PROPERTYNAME_ZBAROFFSETSILL, oldValue, currentValue);
            afterZBarOffsetSillChanged(currentValue);
        }
    }

    public void beforeZBarOffsetSillChanged( Measure<Length> ZBarOffsetSill)
    { }
    public void afterZBarOffsetSillChanged( Measure<Length> ZBarOffsetSill)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Recess")
        ,@LocalizedTag(language="fr", name="label", value="Embr�vement")
    })
    public final Boolean getRecess()
    {
         return this.recess.getValue();
    }

    public final void setRecess(Boolean recess)
    {

        Boolean oldValue = this.recess.getValue();
        this.recess.setValue(recess);

        Boolean currentValue = this.recess.getValue();

        fireRecessChange(currentValue, oldValue);
    }
    public final Boolean removeRecess()
    {
        Boolean oldValue = this.recess.getValue();
        Boolean removedValue = this.recess.removeValue();
        Boolean currentValue = this.recess.getValue();

        fireRecessChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultRecess(Boolean recess)
    {
        Boolean oldValue = this.recess.getValue();
        this.recess.setDefaultValue(recess);

        Boolean currentValue = this.recess.getValue();

        fireRecessChange(currentValue, oldValue);
    }
    
    public final Boolean removeDefaultRecess()
    {
        Boolean oldValue = this.recess.getValue();
        Boolean removedValue = this.recess.removeDefaultValue();
        Boolean currentValue = this.recess.getValue();

        fireRecessChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Boolean> getRecessValueContainer()
    {
        return this.recess;
    }
    public final void fireRecessChange(Boolean currentValue, Boolean oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeRecessChanged(currentValue);
            firePropertyChange(PROPERTYNAME_RECESS, oldValue, currentValue);
            afterRecessChanged(currentValue);
        }
    }

    public void beforeRecessChanged( Boolean recess)
    { }
    public void afterRecessChanged( Boolean recess)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Recess angle")
        ,@LocalizedTag(language="fr", name="label", value="Angle de l\'embr�vement")
    })
    public final Double getRecessAngle()
    {
         return this.recessAngle.getValue();
    }

    public final void setRecessAngle(Double recessAngle)
    {

        Double oldValue = this.recessAngle.getValue();
        this.recessAngle.setValue(recessAngle);

        Double currentValue = this.recessAngle.getValue();

        fireRecessAngleChange(currentValue, oldValue);
    }
    public final Double removeRecessAngle()
    {
        Double oldValue = this.recessAngle.getValue();
        Double removedValue = this.recessAngle.removeValue();
        Double currentValue = this.recessAngle.getValue();

        fireRecessAngleChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultRecessAngle(Double recessAngle)
    {
        Double oldValue = this.recessAngle.getValue();
        this.recessAngle.setDefaultValue(recessAngle);

        Double currentValue = this.recessAngle.getValue();

        fireRecessAngleChange(currentValue, oldValue);
    }
    
    public final Double removeDefaultRecessAngle()
    {
        Double oldValue = this.recessAngle.getValue();
        Double removedValue = this.recessAngle.removeDefaultValue();
        Double currentValue = this.recessAngle.getValue();

        fireRecessAngleChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Double> getRecessAngleValueContainer()
    {
        return this.recessAngle;
    }
    public final void fireRecessAngleChange(Double currentValue, Double oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeRecessAngleChanged(currentValue);
            firePropertyChange(PROPERTYNAME_RECESSANGLE, oldValue, currentValue);
            afterRecessAngleChanged(currentValue);
        }
    }

    public void beforeRecessAngleChanged( Double recessAngle)
    { }
    public void afterRecessAngleChanged( Double recessAngle)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Recess length")
        ,@LocalizedTag(language="fr", name="label", value="Longueur de l\'embr�vement")
    })
    public final Measure<Length> getRecessLength()
    {
         return this.recessLength.getValue();
    }

    public final void setRecessLength(Measure<Length> recessLength)
    {

        recessLength = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(recessLength, getDefaultUnitFor(PROPERTYNAME_RECESSLENGTH,Length.class));

        Measure<Length> oldValue = this.recessLength.getValue();
        this.recessLength.setValue(recessLength);

        Measure<Length> currentValue = this.recessLength.getValue();

        fireRecessLengthChange(currentValue, oldValue);
    }
    public final Measure<Length> removeRecessLength()
    {
        Measure<Length> oldValue = this.recessLength.getValue();
        Measure<Length> removedValue = this.recessLength.removeValue();
        Measure<Length> currentValue = this.recessLength.getValue();

        fireRecessLengthChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultRecessLength(Measure<Length> recessLength)
    {
        recessLength = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(recessLength, getDefaultUnitFor(PROPERTYNAME_RECESSLENGTH,Length.class));

        Measure<Length> oldValue = this.recessLength.getValue();
        this.recessLength.setDefaultValue(recessLength);

        Measure<Length> currentValue = this.recessLength.getValue();

        fireRecessLengthChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultRecessLength()
    {
        Measure<Length> oldValue = this.recessLength.getValue();
        Measure<Length> removedValue = this.recessLength.removeDefaultValue();
        Measure<Length> currentValue = this.recessLength.getValue();

        fireRecessLengthChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getRecessLengthValueContainer()
    {
        return this.recessLength;
    }
    public final void fireRecessLengthChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeRecessLengthChanged(currentValue);
            firePropertyChange(PROPERTYNAME_RECESSLENGTH, oldValue, currentValue);
            afterRecessLengthChanged(currentValue);
        }
    }

    public void beforeRecessLengthChanged( Measure<Length> recessLength)
    { }
    public void afterRecessLengthChanged( Measure<Length> recessLength)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Mid battens offset")
        ,@LocalizedTag(language="fr", name="label", value="D�callage des traverses centrales")
    })
    public final Measure<Length> getBattensMidOffset()
    {
         return this.battensMidOffset.getValue();
    }

    public final void setBattensMidOffset(Measure<Length> battensMidOffset)
    {

        battensMidOffset = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(battensMidOffset, getDefaultUnitFor(PROPERTYNAME_BATTENSMIDOFFSET,Length.class));

        Measure<Length> oldValue = this.battensMidOffset.getValue();
        this.battensMidOffset.setValue(battensMidOffset);

        Measure<Length> currentValue = this.battensMidOffset.getValue();

        fireBattensMidOffsetChange(currentValue, oldValue);
    }
    public final Measure<Length> removeBattensMidOffset()
    {
        Measure<Length> oldValue = this.battensMidOffset.getValue();
        Measure<Length> removedValue = this.battensMidOffset.removeValue();
        Measure<Length> currentValue = this.battensMidOffset.getValue();

        fireBattensMidOffsetChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultBattensMidOffset(Measure<Length> battensMidOffset)
    {
        battensMidOffset = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(battensMidOffset, getDefaultUnitFor(PROPERTYNAME_BATTENSMIDOFFSET,Length.class));

        Measure<Length> oldValue = this.battensMidOffset.getValue();
        this.battensMidOffset.setDefaultValue(battensMidOffset);

        Measure<Length> currentValue = this.battensMidOffset.getValue();

        fireBattensMidOffsetChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultBattensMidOffset()
    {
        Measure<Length> oldValue = this.battensMidOffset.getValue();
        Measure<Length> removedValue = this.battensMidOffset.removeDefaultValue();
        Measure<Length> currentValue = this.battensMidOffset.getValue();

        fireBattensMidOffsetChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getBattensMidOffsetValueContainer()
    {
        return this.battensMidOffset;
    }
    public final void fireBattensMidOffsetChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeBattensMidOffsetChanged(currentValue);
            firePropertyChange(PROPERTYNAME_BATTENSMIDOFFSET, oldValue, currentValue);
            afterBattensMidOffsetChanged(currentValue);
        }
    }

    public void beforeBattensMidOffsetChanged( Measure<Length> battensMidOffset)
    { }
    public void afterBattensMidOffsetChanged( Measure<Length> battensMidOffset)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="fr", name="label", value="Largeur des traverses")
    })
    public final Measure<Length> getBattenWidth()
    {
         return this.battenWidth.getValue();
    }

    public final void setBattenWidth(Measure<Length> battenWidth)
    {

        battenWidth = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(battenWidth, getDefaultUnitFor(PROPERTYNAME_BATTENWIDTH,Length.class));

        Measure<Length> oldValue = this.battenWidth.getValue();
        this.battenWidth.setValue(battenWidth);

        Measure<Length> currentValue = this.battenWidth.getValue();

        fireBattenWidthChange(currentValue, oldValue);
    }
    public final Measure<Length> removeBattenWidth()
    {
        Measure<Length> oldValue = this.battenWidth.getValue();
        Measure<Length> removedValue = this.battenWidth.removeValue();
        Measure<Length> currentValue = this.battenWidth.getValue();

        fireBattenWidthChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultBattenWidth(Measure<Length> battenWidth)
    {
        battenWidth = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(battenWidth, getDefaultUnitFor(PROPERTYNAME_BATTENWIDTH,Length.class));

        Measure<Length> oldValue = this.battenWidth.getValue();
        this.battenWidth.setDefaultValue(battenWidth);

        Measure<Length> currentValue = this.battenWidth.getValue();

        fireBattenWidthChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultBattenWidth()
    {
        Measure<Length> oldValue = this.battenWidth.getValue();
        Measure<Length> removedValue = this.battenWidth.removeDefaultValue();
        Measure<Length> currentValue = this.battenWidth.getValue();

        fireBattenWidthChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getBattenWidthValueContainer()
    {
        return this.battenWidth;
    }
    public final void fireBattenWidthChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeBattenWidthChanged(currentValue);
            firePropertyChange(PROPERTYNAME_BATTENWIDTH, oldValue, currentValue);
            afterBattenWidthChanged(currentValue);
        }
    }

    public void beforeBattenWidthChanged( Measure<Length> battenWidth)
    { }
    public void afterBattenWidthChanged( Measure<Length> battenWidth)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Batten gap top")
        ,@LocalizedTag(language="fr", name="label", value="Espacement haut traverse")
    })
    public final Measure<Length> getBattenGapTop()
    {
         return this.battenGapTop.getValue();
    }

    public final void setBattenGapTop(Measure<Length> battenGapTop)
    {

        battenGapTop = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(battenGapTop, getDefaultUnitFor(PROPERTYNAME_BATTENGAPTOP,Length.class));

        Measure<Length> oldValue = this.battenGapTop.getValue();
        this.battenGapTop.setValue(battenGapTop);

        Measure<Length> currentValue = this.battenGapTop.getValue();

        fireBattenGapTopChange(currentValue, oldValue);
    }
    public final Measure<Length> removeBattenGapTop()
    {
        Measure<Length> oldValue = this.battenGapTop.getValue();
        Measure<Length> removedValue = this.battenGapTop.removeValue();
        Measure<Length> currentValue = this.battenGapTop.getValue();

        fireBattenGapTopChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultBattenGapTop(Measure<Length> battenGapTop)
    {
        battenGapTop = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(battenGapTop, getDefaultUnitFor(PROPERTYNAME_BATTENGAPTOP,Length.class));

        Measure<Length> oldValue = this.battenGapTop.getValue();
        this.battenGapTop.setDefaultValue(battenGapTop);

        Measure<Length> currentValue = this.battenGapTop.getValue();

        fireBattenGapTopChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultBattenGapTop()
    {
        Measure<Length> oldValue = this.battenGapTop.getValue();
        Measure<Length> removedValue = this.battenGapTop.removeDefaultValue();
        Measure<Length> currentValue = this.battenGapTop.getValue();

        fireBattenGapTopChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getBattenGapTopValueContainer()
    {
        return this.battenGapTop;
    }
    public final void fireBattenGapTopChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeBattenGapTopChanged(currentValue);
            firePropertyChange(PROPERTYNAME_BATTENGAPTOP, oldValue, currentValue);
            afterBattenGapTopChanged(currentValue);
        }
    }

    public void beforeBattenGapTopChanged( Measure<Length> battenGapTop)
    { }
    public void afterBattenGapTopChanged( Measure<Length> battenGapTop)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Batten gap left")
        ,@LocalizedTag(language="fr", name="label", value="Espacement gauche traverse")
    })
    public final Measure<Length> getBattenGapLeft()
    {
         return this.battenGapLeft.getValue();
    }

    public final void setBattenGapLeft(Measure<Length> battenGapLeft)
    {

        battenGapLeft = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(battenGapLeft, getDefaultUnitFor(PROPERTYNAME_BATTENGAPLEFT,Length.class));

        Measure<Length> oldValue = this.battenGapLeft.getValue();
        this.battenGapLeft.setValue(battenGapLeft);

        Measure<Length> currentValue = this.battenGapLeft.getValue();

        fireBattenGapLeftChange(currentValue, oldValue);
    }
    public final Measure<Length> removeBattenGapLeft()
    {
        Measure<Length> oldValue = this.battenGapLeft.getValue();
        Measure<Length> removedValue = this.battenGapLeft.removeValue();
        Measure<Length> currentValue = this.battenGapLeft.getValue();

        fireBattenGapLeftChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultBattenGapLeft(Measure<Length> battenGapLeft)
    {
        battenGapLeft = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(battenGapLeft, getDefaultUnitFor(PROPERTYNAME_BATTENGAPLEFT,Length.class));

        Measure<Length> oldValue = this.battenGapLeft.getValue();
        this.battenGapLeft.setDefaultValue(battenGapLeft);

        Measure<Length> currentValue = this.battenGapLeft.getValue();

        fireBattenGapLeftChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultBattenGapLeft()
    {
        Measure<Length> oldValue = this.battenGapLeft.getValue();
        Measure<Length> removedValue = this.battenGapLeft.removeDefaultValue();
        Measure<Length> currentValue = this.battenGapLeft.getValue();

        fireBattenGapLeftChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getBattenGapLeftValueContainer()
    {
        return this.battenGapLeft;
    }
    public final void fireBattenGapLeftChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeBattenGapLeftChanged(currentValue);
            firePropertyChange(PROPERTYNAME_BATTENGAPLEFT, oldValue, currentValue);
            afterBattenGapLeftChanged(currentValue);
        }
    }

    public void beforeBattenGapLeftChanged( Measure<Length> battenGapLeft)
    { }
    public void afterBattenGapLeftChanged( Measure<Length> battenGapLeft)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Batten gap right")
        ,@LocalizedTag(language="fr", name="label", value="Espacement droit traverse")
    })
    public final Measure<Length> getBattenGapRight()
    {
         return this.battenGapRight.getValue();
    }

    public final void setBattenGapRight(Measure<Length> battenGapRight)
    {

        battenGapRight = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(battenGapRight, getDefaultUnitFor(PROPERTYNAME_BATTENGAPRIGHT,Length.class));

        Measure<Length> oldValue = this.battenGapRight.getValue();
        this.battenGapRight.setValue(battenGapRight);

        Measure<Length> currentValue = this.battenGapRight.getValue();

        fireBattenGapRightChange(currentValue, oldValue);
    }
    public final Measure<Length> removeBattenGapRight()
    {
        Measure<Length> oldValue = this.battenGapRight.getValue();
        Measure<Length> removedValue = this.battenGapRight.removeValue();
        Measure<Length> currentValue = this.battenGapRight.getValue();

        fireBattenGapRightChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultBattenGapRight(Measure<Length> battenGapRight)
    {
        battenGapRight = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(battenGapRight, getDefaultUnitFor(PROPERTYNAME_BATTENGAPRIGHT,Length.class));

        Measure<Length> oldValue = this.battenGapRight.getValue();
        this.battenGapRight.setDefaultValue(battenGapRight);

        Measure<Length> currentValue = this.battenGapRight.getValue();

        fireBattenGapRightChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultBattenGapRight()
    {
        Measure<Length> oldValue = this.battenGapRight.getValue();
        Measure<Length> removedValue = this.battenGapRight.removeDefaultValue();
        Measure<Length> currentValue = this.battenGapRight.getValue();

        fireBattenGapRightChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getBattenGapRightValueContainer()
    {
        return this.battenGapRight;
    }
    public final void fireBattenGapRightChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeBattenGapRightChanged(currentValue);
            firePropertyChange(PROPERTYNAME_BATTENGAPRIGHT, oldValue, currentValue);
            afterBattenGapRightChanged(currentValue);
        }
    }

    public void beforeBattenGapRightChanged( Measure<Length> battenGapRight)
    { }
    public void afterBattenGapRightChanged( Measure<Length> battenGapRight)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Batten gap sill")
        ,@LocalizedTag(language="fr", name="label", value="Espacement bas traverse")
    })
    public final Measure<Length> getBattenGapSill()
    {
         return this.battenGapSill.getValue();
    }

    public final void setBattenGapSill(Measure<Length> battenGapSill)
    {

        battenGapSill = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(battenGapSill, getDefaultUnitFor(PROPERTYNAME_BATTENGAPSILL,Length.class));

        Measure<Length> oldValue = this.battenGapSill.getValue();
        this.battenGapSill.setValue(battenGapSill);

        Measure<Length> currentValue = this.battenGapSill.getValue();

        fireBattenGapSillChange(currentValue, oldValue);
    }
    public final Measure<Length> removeBattenGapSill()
    {
        Measure<Length> oldValue = this.battenGapSill.getValue();
        Measure<Length> removedValue = this.battenGapSill.removeValue();
        Measure<Length> currentValue = this.battenGapSill.getValue();

        fireBattenGapSillChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultBattenGapSill(Measure<Length> battenGapSill)
    {
        battenGapSill = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(battenGapSill, getDefaultUnitFor(PROPERTYNAME_BATTENGAPSILL,Length.class));

        Measure<Length> oldValue = this.battenGapSill.getValue();
        this.battenGapSill.setDefaultValue(battenGapSill);

        Measure<Length> currentValue = this.battenGapSill.getValue();

        fireBattenGapSillChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultBattenGapSill()
    {
        Measure<Length> oldValue = this.battenGapSill.getValue();
        Measure<Length> removedValue = this.battenGapSill.removeDefaultValue();
        Measure<Length> currentValue = this.battenGapSill.getValue();

        fireBattenGapSillChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getBattenGapSillValueContainer()
    {
        return this.battenGapSill;
    }
    public final void fireBattenGapSillChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeBattenGapSillChanged(currentValue);
            firePropertyChange(PROPERTYNAME_BATTENGAPSILL, oldValue, currentValue);
            afterBattenGapSillChanged(currentValue);
        }
    }

    public void beforeBattenGapSillChanged( Measure<Length> battenGapSill)
    { }
    public void afterBattenGapSillChanged( Measure<Length> battenGapSill)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Batten profile")
        ,@LocalizedTag(language="fr", name="label", value="Profil des traverses")
    })
    public final HingeShutterPanelProfiles getBattenProfile()
    {
         return this.battenProfile.getValue();
    }

    public final void setBattenProfile(HingeShutterPanelProfiles battenProfile)
    {

        HingeShutterPanelProfiles oldValue = this.battenProfile.getValue();
        this.battenProfile.setValue(battenProfile);

        HingeShutterPanelProfiles currentValue = this.battenProfile.getValue();

        fireBattenProfileChange(currentValue, oldValue);
    }
    public final HingeShutterPanelProfiles removeBattenProfile()
    {
        HingeShutterPanelProfiles oldValue = this.battenProfile.getValue();
        HingeShutterPanelProfiles removedValue = this.battenProfile.removeValue();
        HingeShutterPanelProfiles currentValue = this.battenProfile.getValue();

        fireBattenProfileChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultBattenProfile(HingeShutterPanelProfiles battenProfile)
    {
        HingeShutterPanelProfiles oldValue = this.battenProfile.getValue();
        this.battenProfile.setDefaultValue(battenProfile);

        HingeShutterPanelProfiles currentValue = this.battenProfile.getValue();

        fireBattenProfileChange(currentValue, oldValue);
    }
    
    public final HingeShutterPanelProfiles removeDefaultBattenProfile()
    {
        HingeShutterPanelProfiles oldValue = this.battenProfile.getValue();
        HingeShutterPanelProfiles removedValue = this.battenProfile.removeDefaultValue();
        HingeShutterPanelProfiles currentValue = this.battenProfile.getValue();

        fireBattenProfileChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<HingeShutterPanelProfiles> getBattenProfileValueContainer()
    {
        return this.battenProfile;
    }
    public final void fireBattenProfileChange(HingeShutterPanelProfiles currentValue, HingeShutterPanelProfiles oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeBattenProfileChanged(currentValue);
            firePropertyChange(PROPERTYNAME_BATTENPROFILE, oldValue, currentValue);
            afterBattenProfileChanged(currentValue);
        }
    }

    public void beforeBattenProfileChanged( HingeShutterPanelProfiles battenProfile)
    { }
    public void afterBattenProfileChanged( HingeShutterPanelProfiles battenProfile)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Battens color")
        ,@LocalizedTag(language="fr", name="label", value="Couleur des traverses")
    })
    public final com.client360.configuration.wad.StandardColor getBattenColor()
    {
         return this.battenColor.getValue();
    }

    public final void setBattenColor(com.client360.configuration.wad.StandardColor battenColor)
    {

        if (battenColor != null)
        {
            acquire(battenColor, "battenColor");
        }
        com.client360.configuration.wad.StandardColor oldValue = this.battenColor.getValue();
        this.battenColor.setValue(battenColor);

        com.client360.configuration.wad.StandardColor currentValue = this.battenColor.getValue();

        fireBattenColorChange(currentValue, oldValue);
    }
    public final com.client360.configuration.wad.StandardColor removeBattenColor()
    {
        com.client360.configuration.wad.StandardColor oldValue = this.battenColor.getValue();
        com.client360.configuration.wad.StandardColor removedValue = this.battenColor.removeValue();
        com.client360.configuration.wad.StandardColor currentValue = this.battenColor.getValue();

        fireBattenColorChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultBattenColor(com.client360.configuration.wad.StandardColor battenColor)
    {
        if (battenColor != null)
        {
            acquire(battenColor, "battenColor");
        }
        com.client360.configuration.wad.StandardColor oldValue = this.battenColor.getValue();
        this.battenColor.setDefaultValue(battenColor);

        com.client360.configuration.wad.StandardColor currentValue = this.battenColor.getValue();

        fireBattenColorChange(currentValue, oldValue);
    }
    
    public final com.client360.configuration.wad.StandardColor removeDefaultBattenColor()
    {
        com.client360.configuration.wad.StandardColor oldValue = this.battenColor.getValue();
        com.client360.configuration.wad.StandardColor removedValue = this.battenColor.removeDefaultValue();
        com.client360.configuration.wad.StandardColor currentValue = this.battenColor.getValue();

        fireBattenColorChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<com.client360.configuration.wad.StandardColor> getBattenColorValueContainer()
    {
        return this.battenColor;
    }
    public final void fireBattenColorChange(com.client360.configuration.wad.StandardColor currentValue, com.client360.configuration.wad.StandardColor oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeBattenColorChanged(currentValue);
            firePropertyChange(PROPERTYNAME_BATTENCOLOR, oldValue, currentValue);
            afterBattenColorChanged(currentValue);
        }
    }

    public void beforeBattenColorChanged( com.client360.configuration.wad.StandardColor battenColor)
    { }
    public void afterBattenColorChanged( com.client360.configuration.wad.StandardColor battenColor)
    { }



    @Override
    protected String getSequences()
    {
        return "form,offsetLeft,offsetRight,offsetTop,offsetSill,perimeterLeft,perimeterRight,perimeterTop,perimeterSill,byzanceTop,byzanceSill,perimeterColor,perimeterProfile,panelModelCornerTypes,boardHorizontal,boardWidth,boardSpacing,boardColor,boardProfile,nbBattens,battenWidth,battenGapTop,battenGapLeft,battenGapRight,battenGapSill,battensMidOffset,battenProfile,zefiralBoardWidth,battenColor,zBars,zbarWidth,zBarOffsetSill,zBarOffsetTop,recess,recessAngle,recessLength,not set";  
    }
}
