// generated class, do not edit

package com.client360.configuration.hingeshutters;

//Plugin V14.0.1

// Imports 

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class MixedPanelBase extends com.client360.configuration.hingeshutters.PanelModel
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(MixedPanelBase.class);

    // attributes
    private ValueContainer<Integer> nbSubPanels = new ValueContainer<Integer>(this);
    private ValueContainer<ShutterPanelSection[]> shutterPanelSection = new ValueContainer<ShutterPanelSection[]>(this);
    private ValueContainer<PanelModel[]> panelModel = new ValueContainer<PanelModel[]>(this);

    // Bound properties

    public static final String PROPERTYNAME_NBSUBPANELS = "nbSubPanels";  
    public static final String PROPERTYNAME_SHUTTERPANELSECTION = "shutterPanelSection";  
    public static final String PROPERTYNAME_PANELMODEL = "panelModel";  

    // Constructors

    public MixedPanelBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.hingeshutters.MixedPanel"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Sections count")
        ,@LocalizedTag(language="fr", name="label", value="Nombre de sections")
    })
    public final Integer getNbSubPanels()
    {
         return this.nbSubPanels.getValue();
    }

    public final void setNbSubPanels(Integer nbSubPanels)
    {

        Integer oldValue = this.nbSubPanels.getValue();
        this.nbSubPanels.setValue(nbSubPanels);

        Integer currentValue = this.nbSubPanels.getValue();

        fireNbSubPanelsChange(currentValue, oldValue);
    }
    public final Integer removeNbSubPanels()
    {
        Integer oldValue = this.nbSubPanels.getValue();
        Integer removedValue = this.nbSubPanels.removeValue();
        Integer currentValue = this.nbSubPanels.getValue();

        fireNbSubPanelsChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultNbSubPanels(Integer nbSubPanels)
    {
        Integer oldValue = this.nbSubPanels.getValue();
        this.nbSubPanels.setDefaultValue(nbSubPanels);

        Integer currentValue = this.nbSubPanels.getValue();

        fireNbSubPanelsChange(currentValue, oldValue);
    }
    
    public final Integer removeDefaultNbSubPanels()
    {
        Integer oldValue = this.nbSubPanels.getValue();
        Integer removedValue = this.nbSubPanels.removeDefaultValue();
        Integer currentValue = this.nbSubPanels.getValue();

        fireNbSubPanelsChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Integer> getNbSubPanelsValueContainer()
    {
        return this.nbSubPanels;
    }
    public final void fireNbSubPanelsChange(Integer currentValue, Integer oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeNbSubPanelsChanged(currentValue);
            firePropertyChange(PROPERTYNAME_NBSUBPANELS, oldValue, currentValue);
            afterNbSubPanelsChanged(currentValue);
        }
    }

    public void beforeNbSubPanelsChanged( Integer nbSubPanels)
    { }
    public void afterNbSubPanelsChanged( Integer nbSubPanels)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Sections")
        ,@LocalizedTag(language="fr", name="label", value="Sections")
    })
    public final ShutterPanelSection[] getShutterPanelSection()
    {
         return this.shutterPanelSection.getValue();
    }

    public final void setShutterPanelSection(ShutterPanelSection[] shutterPanelSection)
    {

        if (shutterPanelSection != null)
        {
            acquire(shutterPanelSection, "shutterPanelSection");
        }
        ShutterPanelSection[] oldValue = this.shutterPanelSection.getValue();
        this.shutterPanelSection.setValue(shutterPanelSection);

        ShutterPanelSection[] currentValue = this.shutterPanelSection.getValue();

        fireShutterPanelSectionChange(currentValue, oldValue);
    }
    public final ShutterPanelSection[] removeShutterPanelSection()
    {
        ShutterPanelSection[] oldValue = this.shutterPanelSection.getValue();
        ShutterPanelSection[] removedValue = this.shutterPanelSection.removeValue();
        ShutterPanelSection[] currentValue = this.shutterPanelSection.getValue();

        fireShutterPanelSectionChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultShutterPanelSection(ShutterPanelSection[] shutterPanelSection)
    {
        if (shutterPanelSection != null)
        {
            acquire(shutterPanelSection, "shutterPanelSection");
        }
        ShutterPanelSection[] oldValue = this.shutterPanelSection.getValue();
        this.shutterPanelSection.setDefaultValue(shutterPanelSection);

        ShutterPanelSection[] currentValue = this.shutterPanelSection.getValue();

        fireShutterPanelSectionChange(currentValue, oldValue);
    }
    
    public final ShutterPanelSection[] removeDefaultShutterPanelSection()
    {
        ShutterPanelSection[] oldValue = this.shutterPanelSection.getValue();
        ShutterPanelSection[] removedValue = this.shutterPanelSection.removeDefaultValue();
        ShutterPanelSection[] currentValue = this.shutterPanelSection.getValue();

        fireShutterPanelSectionChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<ShutterPanelSection[]> getShutterPanelSectionValueContainer()
    {
        return this.shutterPanelSection;
    }
    public final void fireShutterPanelSectionChange(ShutterPanelSection[] currentValue, ShutterPanelSection[] oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeShutterPanelSectionChanged(currentValue);
            firePropertyChange(PROPERTYNAME_SHUTTERPANELSECTION, oldValue, currentValue);
            afterShutterPanelSectionChanged(currentValue);
        }
    }

    public void beforeShutterPanelSectionChanged( ShutterPanelSection[] shutterPanelSection)
    { }
    public void afterShutterPanelSectionChanged( ShutterPanelSection[] shutterPanelSection)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Panel model")
        ,@LocalizedTag(language="fr", name="label", value="Mod�le de panneau")
    })
    public final PanelModel[] getPanelModel()
    {
         return this.panelModel.getValue();
    }

    public final void setPanelModel(PanelModel[] panelModel)
    {

        if (panelModel != null)
        {
            acquire(panelModel, "panelModel");
        }
        PanelModel[] oldValue = this.panelModel.getValue();
        this.panelModel.setValue(panelModel);

        PanelModel[] currentValue = this.panelModel.getValue();

        firePanelModelChange(currentValue, oldValue);
    }
    public final PanelModel[] removePanelModel()
    {
        PanelModel[] oldValue = this.panelModel.getValue();
        PanelModel[] removedValue = this.panelModel.removeValue();
        PanelModel[] currentValue = this.panelModel.getValue();

        firePanelModelChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultPanelModel(PanelModel[] panelModel)
    {
        if (panelModel != null)
        {
            acquire(panelModel, "panelModel");
        }
        PanelModel[] oldValue = this.panelModel.getValue();
        this.panelModel.setDefaultValue(panelModel);

        PanelModel[] currentValue = this.panelModel.getValue();

        firePanelModelChange(currentValue, oldValue);
    }
    
    public final PanelModel[] removeDefaultPanelModel()
    {
        PanelModel[] oldValue = this.panelModel.getValue();
        PanelModel[] removedValue = this.panelModel.removeDefaultValue();
        PanelModel[] currentValue = this.panelModel.getValue();

        firePanelModelChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<PanelModel[]> getPanelModelValueContainer()
    {
        return this.panelModel;
    }
    public final void firePanelModelChange(PanelModel[] currentValue, PanelModel[] oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforePanelModelChanged(currentValue);
            firePropertyChange(PROPERTYNAME_PANELMODEL, oldValue, currentValue);
            afterPanelModelChanged(currentValue);
        }
    }

    public void beforePanelModelChanged( PanelModel[] panelModel)
    { }
    public void afterPanelModelChanged( PanelModel[] panelModel)
    { }



    @Override
    protected String getSequences()
    {
        return "form,offsetLeft,offsetRight,offsetTop,offsetSill,perimeterLeft,perimeterRight,perimeterTop,perimeterSill,byzanceTop,byzanceSill,perimeterProfile,perimeterColor,panelModelCornerTypes,nbSubPanels,shutterPanelSection,not set,customVisualComposition";  
    }
}
