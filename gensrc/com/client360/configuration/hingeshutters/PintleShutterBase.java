// generated class, do not edit

package com.client360.configuration.hingeshutters;

//Plugin V14.0.1

// Imports 

import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class PintleShutterBase extends com.netappsid.configuration.hingeshutters.PintleShutter
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(PintleShutterBase.class);

    // attributes
    private ValueContainer<Boolean> displayPintles = new ValueContainer<Boolean>(this);
    private ValueContainer<Boolean> displayStoppers = new ValueContainer<Boolean>(this);
    private ValueContainer<Boolean> displayHoldBacks = new ValueContainer<Boolean>(this);

    // Bound properties

    public static final String PROPERTYNAME_DISPLAYPINTLES = "displayPintles";  
    public static final String PROPERTYNAME_DISPLAYSTOPPERS = "displayStoppers";  
    public static final String PROPERTYNAME_DISPLAYHOLDBACKS = "displayHoldBacks";  

    // Constructors

    public PintleShutterBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.hingeshutters.PintleShutter"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Display left pintles")
        ,@LocalizedTag(language="fr", name="label", value="Gonds")
    })
    public final Boolean getDisplayPintles()
    {
         return this.displayPintles.getValue();
    }

    public final void setDisplayPintles(Boolean displayPintles)
    {

        Boolean oldValue = this.displayPintles.getValue();
        this.displayPintles.setValue(displayPintles);

        Boolean currentValue = this.displayPintles.getValue();

        fireDisplayPintlesChange(currentValue, oldValue);
    }
    public final Boolean removeDisplayPintles()
    {
        Boolean oldValue = this.displayPintles.getValue();
        Boolean removedValue = this.displayPintles.removeValue();
        Boolean currentValue = this.displayPintles.getValue();

        fireDisplayPintlesChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultDisplayPintles(Boolean displayPintles)
    {
        Boolean oldValue = this.displayPintles.getValue();
        this.displayPintles.setDefaultValue(displayPintles);

        Boolean currentValue = this.displayPintles.getValue();

        fireDisplayPintlesChange(currentValue, oldValue);
    }
    
    public final Boolean removeDefaultDisplayPintles()
    {
        Boolean oldValue = this.displayPintles.getValue();
        Boolean removedValue = this.displayPintles.removeDefaultValue();
        Boolean currentValue = this.displayPintles.getValue();

        fireDisplayPintlesChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Boolean> getDisplayPintlesValueContainer()
    {
        return this.displayPintles;
    }
    public final void fireDisplayPintlesChange(Boolean currentValue, Boolean oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeDisplayPintlesChanged(currentValue);
            firePropertyChange(PROPERTYNAME_DISPLAYPINTLES, oldValue, currentValue);
            afterDisplayPintlesChanged(currentValue);
        }
    }

    public void beforeDisplayPintlesChanged( Boolean displayPintles)
    { }
    public void afterDisplayPintlesChanged( Boolean displayPintles)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Display stoppers")
        ,@LocalizedTag(language="fr", name="label", value="But�es")
    })
    public final Boolean getDisplayStoppers()
    {
         return this.displayStoppers.getValue();
    }

    public final void setDisplayStoppers(Boolean displayStoppers)
    {

        Boolean oldValue = this.displayStoppers.getValue();
        this.displayStoppers.setValue(displayStoppers);

        Boolean currentValue = this.displayStoppers.getValue();

        fireDisplayStoppersChange(currentValue, oldValue);
    }
    public final Boolean removeDisplayStoppers()
    {
        Boolean oldValue = this.displayStoppers.getValue();
        Boolean removedValue = this.displayStoppers.removeValue();
        Boolean currentValue = this.displayStoppers.getValue();

        fireDisplayStoppersChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultDisplayStoppers(Boolean displayStoppers)
    {
        Boolean oldValue = this.displayStoppers.getValue();
        this.displayStoppers.setDefaultValue(displayStoppers);

        Boolean currentValue = this.displayStoppers.getValue();

        fireDisplayStoppersChange(currentValue, oldValue);
    }
    
    public final Boolean removeDefaultDisplayStoppers()
    {
        Boolean oldValue = this.displayStoppers.getValue();
        Boolean removedValue = this.displayStoppers.removeDefaultValue();
        Boolean currentValue = this.displayStoppers.getValue();

        fireDisplayStoppersChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Boolean> getDisplayStoppersValueContainer()
    {
        return this.displayStoppers;
    }
    public final void fireDisplayStoppersChange(Boolean currentValue, Boolean oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeDisplayStoppersChanged(currentValue);
            firePropertyChange(PROPERTYNAME_DISPLAYSTOPPERS, oldValue, currentValue);
            afterDisplayStoppersChanged(currentValue);
        }
    }

    public void beforeDisplayStoppersChanged( Boolean displayStoppers)
    { }
    public void afterDisplayStoppersChanged( Boolean displayStoppers)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Display holdbacks")
        ,@LocalizedTag(language="fr", name="label", value="Arr�ts")
    })
    public final Boolean getDisplayHoldBacks()
    {
         return this.displayHoldBacks.getValue();
    }

    public final void setDisplayHoldBacks(Boolean displayHoldBacks)
    {

        Boolean oldValue = this.displayHoldBacks.getValue();
        this.displayHoldBacks.setValue(displayHoldBacks);

        Boolean currentValue = this.displayHoldBacks.getValue();

        fireDisplayHoldBacksChange(currentValue, oldValue);
    }
    public final Boolean removeDisplayHoldBacks()
    {
        Boolean oldValue = this.displayHoldBacks.getValue();
        Boolean removedValue = this.displayHoldBacks.removeValue();
        Boolean currentValue = this.displayHoldBacks.getValue();

        fireDisplayHoldBacksChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultDisplayHoldBacks(Boolean displayHoldBacks)
    {
        Boolean oldValue = this.displayHoldBacks.getValue();
        this.displayHoldBacks.setDefaultValue(displayHoldBacks);

        Boolean currentValue = this.displayHoldBacks.getValue();

        fireDisplayHoldBacksChange(currentValue, oldValue);
    }
    
    public final Boolean removeDefaultDisplayHoldBacks()
    {
        Boolean oldValue = this.displayHoldBacks.getValue();
        Boolean removedValue = this.displayHoldBacks.removeDefaultValue();
        Boolean currentValue = this.displayHoldBacks.getValue();

        fireDisplayHoldBacksChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Boolean> getDisplayHoldBacksValueContainer()
    {
        return this.displayHoldBacks;
    }
    public final void fireDisplayHoldBacksChange(Boolean currentValue, Boolean oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeDisplayHoldBacksChanged(currentValue);
            firePropertyChange(PROPERTYNAME_DISPLAYHOLDBACKS, oldValue, currentValue);
            afterDisplayHoldBacksChanged(currentValue);
        }
    }

    public void beforeDisplayHoldBacksChanged( Boolean displayHoldBacks)
    { }
    public void afterDisplayHoldBacksChanged( Boolean displayHoldBacks)
    { }



    @Override
    protected String getSequences()
    {
        return "paintable,renderingNumber,leftPintlesOffset,rightPintlesOffset,leftPintlesDisplay,RightPintlesDisplay,displayPintles,displayStoppers,displayHoldBacks,panels,pintleShutterFrame,customVisualCompositions,not set,rightHoldBacksDisplay,leftHoldBacksDisplay,stoppersDisplay";  
    }
}
