// generated class, do not edit

package com.client360.configuration.hingeshutters;

//Plugin V14.0.1

// Imports 

import com.client360.configuration.hingeshutters.enums.HingeShutterPanelProfiles;
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class AstragalBase extends com.netappsid.configuration.hingeshutters.Astragal
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AstragalBase.class);

    // attributes
    private ValueContainer<HingeShutterPanelProfiles> hingeShutterPanelProfiles = new ValueContainer<HingeShutterPanelProfiles>(this);

    // Bound properties

    public static final String PROPERTYNAME_HINGESHUTTERPANELPROFILES = "hingeShutterPanelProfiles";  

    // Constructors

    public AstragalBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.hingeshutters.Astragal"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Profile")
        ,@LocalizedTag(language="fr", name="label", value="Profile")
    })
    public final HingeShutterPanelProfiles getHingeShutterPanelProfiles()
    {
         return this.hingeShutterPanelProfiles.getValue();
    }

    public final void setHingeShutterPanelProfiles(HingeShutterPanelProfiles hingeShutterPanelProfiles)
    {

        HingeShutterPanelProfiles oldValue = this.hingeShutterPanelProfiles.getValue();
        this.hingeShutterPanelProfiles.setValue(hingeShutterPanelProfiles);

        HingeShutterPanelProfiles currentValue = this.hingeShutterPanelProfiles.getValue();

        fireHingeShutterPanelProfilesChange(currentValue, oldValue);
    }
    public final HingeShutterPanelProfiles removeHingeShutterPanelProfiles()
    {
        HingeShutterPanelProfiles oldValue = this.hingeShutterPanelProfiles.getValue();
        HingeShutterPanelProfiles removedValue = this.hingeShutterPanelProfiles.removeValue();
        HingeShutterPanelProfiles currentValue = this.hingeShutterPanelProfiles.getValue();

        fireHingeShutterPanelProfilesChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultHingeShutterPanelProfiles(HingeShutterPanelProfiles hingeShutterPanelProfiles)
    {
        HingeShutterPanelProfiles oldValue = this.hingeShutterPanelProfiles.getValue();
        this.hingeShutterPanelProfiles.setDefaultValue(hingeShutterPanelProfiles);

        HingeShutterPanelProfiles currentValue = this.hingeShutterPanelProfiles.getValue();

        fireHingeShutterPanelProfilesChange(currentValue, oldValue);
    }
    
    public final HingeShutterPanelProfiles removeDefaultHingeShutterPanelProfiles()
    {
        HingeShutterPanelProfiles oldValue = this.hingeShutterPanelProfiles.getValue();
        HingeShutterPanelProfiles removedValue = this.hingeShutterPanelProfiles.removeDefaultValue();
        HingeShutterPanelProfiles currentValue = this.hingeShutterPanelProfiles.getValue();

        fireHingeShutterPanelProfilesChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<HingeShutterPanelProfiles> getHingeShutterPanelProfilesValueContainer()
    {
        return this.hingeShutterPanelProfiles;
    }
    public final void fireHingeShutterPanelProfilesChange(HingeShutterPanelProfiles currentValue, HingeShutterPanelProfiles oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeHingeShutterPanelProfilesChanged(currentValue);
            firePropertyChange(PROPERTYNAME_HINGESHUTTERPANELPROFILES, oldValue, currentValue);
            afterHingeShutterPanelProfilesChanged(currentValue);
        }
    }

    public void beforeHingeShutterPanelProfilesChanged( HingeShutterPanelProfiles hingeShutterPanelProfiles)
    { }
    public void afterHingeShutterPanelProfilesChanged( HingeShutterPanelProfiles hingeShutterPanelProfiles)
    { }


    public boolean isHingeShutterPanelProfilesMandatory()
    {
        return true;
    }

}
