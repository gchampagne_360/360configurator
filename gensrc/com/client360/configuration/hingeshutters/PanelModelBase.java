// generated class, do not edit

package com.client360.configuration.hingeshutters;

//Plugin V14.0.1

// Imports 

import javax.measure.quantities.Length;

import org.jscience.physics.measures.Measure;

import com.client360.configuration.hingeshutters.enums.HingeShutterPanelProfiles;
import com.client360.configuration.hingeshutters.enums.PanelModelCornerTypes;
import com.netappsid.erp.configurator.ValueContainer;
import com.netappsid.erp.configurator.ValueContainerGetter;
import com.netappsid.erp.configurator.annotations.LocalizedTag;
import com.netappsid.erp.configurator.annotations.LocalizedTags;

public abstract class PanelModelBase extends com.netappsid.configuration.hingeshutters.PanelModel
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(PanelModelBase.class);

    // attributes
    private ValueContainer<Measure<Length>> offsetLeft = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Measure<Length>> offsetRight = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Measure<Length>> offsetTop = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Measure<Length>> offsetSill = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Measure<Length>> perimeterLeft = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Measure<Length>> perimeterRight = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Measure<Length>> perimeterTop = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Measure<Length>> perimeterSill = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Measure<Length>> byzanceTop = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<Measure<Length>> byzanceSill = new ValueContainer<Measure<Length>>(this);
    private ValueContainer<PanelModelCornerTypes> panelModelCornerTypes = new ValueContainer<PanelModelCornerTypes>(this);
    private ValueContainer<HingeShutterPanelProfiles> perimeterProfile = new ValueContainer<HingeShutterPanelProfiles>(this);
    private ValueContainer<com.client360.configuration.wad.StandardColor> perimeterColor = new ValueContainer<com.client360.configuration.wad.StandardColor>(this);

    // Bound properties

    public static final String PROPERTYNAME_OFFSETLEFT = "offsetLeft";  
    public static final String PROPERTYNAME_OFFSETRIGHT = "offsetRight";  
    public static final String PROPERTYNAME_OFFSETTOP = "offsetTop";  
    public static final String PROPERTYNAME_OFFSETSILL = "offsetSill";  
    public static final String PROPERTYNAME_PERIMETERLEFT = "perimeterLeft";  
    public static final String PROPERTYNAME_PERIMETERRIGHT = "perimeterRight";  
    public static final String PROPERTYNAME_PERIMETERTOP = "perimeterTop";  
    public static final String PROPERTYNAME_PERIMETERSILL = "perimeterSill";  
    public static final String PROPERTYNAME_BYZANCETOP = "byzanceTop";  
    public static final String PROPERTYNAME_BYZANCESILL = "byzanceSill";  
    public static final String PROPERTYNAME_PANELMODELCORNERTYPES = "panelModelCornerTypes";  
    public static final String PROPERTYNAME_PERIMETERPROFILE = "perimeterProfile";  
    public static final String PROPERTYNAME_PERIMETERCOLOR = "perimeterColor";  

    // Constructors

    public PanelModelBase(com.netappsid.erp.configurator.Configurable parent)
    {
         super(parent);
        if (this.getClass().getName().equals("com.client360.configuration.hingeshutters.PanelModel"))  
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Offset left")
        ,@LocalizedTag(language="fr", name="label", value="D�callage gauche")
    })
    public final Measure<Length> getOffsetLeft()
    {
         return this.offsetLeft.getValue();
    }

    public final void setOffsetLeft(Measure<Length> offsetLeft)
    {

        offsetLeft = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(offsetLeft, getDefaultUnitFor(PROPERTYNAME_OFFSETLEFT,Length.class));

        Measure<Length> oldValue = this.offsetLeft.getValue();
        this.offsetLeft.setValue(offsetLeft);

        Measure<Length> currentValue = this.offsetLeft.getValue();

        fireOffsetLeftChange(currentValue, oldValue);
    }
    public final Measure<Length> removeOffsetLeft()
    {
        Measure<Length> oldValue = this.offsetLeft.getValue();
        Measure<Length> removedValue = this.offsetLeft.removeValue();
        Measure<Length> currentValue = this.offsetLeft.getValue();

        fireOffsetLeftChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultOffsetLeft(Measure<Length> offsetLeft)
    {
        offsetLeft = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(offsetLeft, getDefaultUnitFor(PROPERTYNAME_OFFSETLEFT,Length.class));

        Measure<Length> oldValue = this.offsetLeft.getValue();
        this.offsetLeft.setDefaultValue(offsetLeft);

        Measure<Length> currentValue = this.offsetLeft.getValue();

        fireOffsetLeftChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultOffsetLeft()
    {
        Measure<Length> oldValue = this.offsetLeft.getValue();
        Measure<Length> removedValue = this.offsetLeft.removeDefaultValue();
        Measure<Length> currentValue = this.offsetLeft.getValue();

        fireOffsetLeftChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getOffsetLeftValueContainer()
    {
        return this.offsetLeft;
    }
    public final void fireOffsetLeftChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeOffsetLeftChanged(currentValue);
            firePropertyChange(PROPERTYNAME_OFFSETLEFT, oldValue, currentValue);
            afterOffsetLeftChanged(currentValue);
        }
    }

    public void beforeOffsetLeftChanged( Measure<Length> offsetLeft)
    { }
    public void afterOffsetLeftChanged( Measure<Length> offsetLeft)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Offset right")
        ,@LocalizedTag(language="fr", name="label", value="D�callage droite")
    })
    public final Measure<Length> getOffsetRight()
    {
         return this.offsetRight.getValue();
    }

    public final void setOffsetRight(Measure<Length> offsetRight)
    {

        offsetRight = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(offsetRight, getDefaultUnitFor(PROPERTYNAME_OFFSETRIGHT,Length.class));

        Measure<Length> oldValue = this.offsetRight.getValue();
        this.offsetRight.setValue(offsetRight);

        Measure<Length> currentValue = this.offsetRight.getValue();

        fireOffsetRightChange(currentValue, oldValue);
    }
    public final Measure<Length> removeOffsetRight()
    {
        Measure<Length> oldValue = this.offsetRight.getValue();
        Measure<Length> removedValue = this.offsetRight.removeValue();
        Measure<Length> currentValue = this.offsetRight.getValue();

        fireOffsetRightChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultOffsetRight(Measure<Length> offsetRight)
    {
        offsetRight = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(offsetRight, getDefaultUnitFor(PROPERTYNAME_OFFSETRIGHT,Length.class));

        Measure<Length> oldValue = this.offsetRight.getValue();
        this.offsetRight.setDefaultValue(offsetRight);

        Measure<Length> currentValue = this.offsetRight.getValue();

        fireOffsetRightChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultOffsetRight()
    {
        Measure<Length> oldValue = this.offsetRight.getValue();
        Measure<Length> removedValue = this.offsetRight.removeDefaultValue();
        Measure<Length> currentValue = this.offsetRight.getValue();

        fireOffsetRightChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getOffsetRightValueContainer()
    {
        return this.offsetRight;
    }
    public final void fireOffsetRightChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeOffsetRightChanged(currentValue);
            firePropertyChange(PROPERTYNAME_OFFSETRIGHT, oldValue, currentValue);
            afterOffsetRightChanged(currentValue);
        }
    }

    public void beforeOffsetRightChanged( Measure<Length> offsetRight)
    { }
    public void afterOffsetRightChanged( Measure<Length> offsetRight)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Offset top")
        ,@LocalizedTag(language="fr", name="label", value="D�callage haut")
    })
    public final Measure<Length> getOffsetTop()
    {
         return this.offsetTop.getValue();
    }

    public final void setOffsetTop(Measure<Length> offsetTop)
    {

        offsetTop = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(offsetTop, getDefaultUnitFor(PROPERTYNAME_OFFSETTOP,Length.class));

        Measure<Length> oldValue = this.offsetTop.getValue();
        this.offsetTop.setValue(offsetTop);

        Measure<Length> currentValue = this.offsetTop.getValue();

        fireOffsetTopChange(currentValue, oldValue);
    }
    public final Measure<Length> removeOffsetTop()
    {
        Measure<Length> oldValue = this.offsetTop.getValue();
        Measure<Length> removedValue = this.offsetTop.removeValue();
        Measure<Length> currentValue = this.offsetTop.getValue();

        fireOffsetTopChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultOffsetTop(Measure<Length> offsetTop)
    {
        offsetTop = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(offsetTop, getDefaultUnitFor(PROPERTYNAME_OFFSETTOP,Length.class));

        Measure<Length> oldValue = this.offsetTop.getValue();
        this.offsetTop.setDefaultValue(offsetTop);

        Measure<Length> currentValue = this.offsetTop.getValue();

        fireOffsetTopChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultOffsetTop()
    {
        Measure<Length> oldValue = this.offsetTop.getValue();
        Measure<Length> removedValue = this.offsetTop.removeDefaultValue();
        Measure<Length> currentValue = this.offsetTop.getValue();

        fireOffsetTopChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getOffsetTopValueContainer()
    {
        return this.offsetTop;
    }
    public final void fireOffsetTopChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeOffsetTopChanged(currentValue);
            firePropertyChange(PROPERTYNAME_OFFSETTOP, oldValue, currentValue);
            afterOffsetTopChanged(currentValue);
        }
    }

    public void beforeOffsetTopChanged( Measure<Length> offsetTop)
    { }
    public void afterOffsetTopChanged( Measure<Length> offsetTop)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Offset sill")
        ,@LocalizedTag(language="fr", name="label", value="D�callage bas")
    })
    public final Measure<Length> getOffsetSill()
    {
         return this.offsetSill.getValue();
    }

    public final void setOffsetSill(Measure<Length> offsetSill)
    {

        offsetSill = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(offsetSill, getDefaultUnitFor(PROPERTYNAME_OFFSETSILL,Length.class));

        Measure<Length> oldValue = this.offsetSill.getValue();
        this.offsetSill.setValue(offsetSill);

        Measure<Length> currentValue = this.offsetSill.getValue();

        fireOffsetSillChange(currentValue, oldValue);
    }
    public final Measure<Length> removeOffsetSill()
    {
        Measure<Length> oldValue = this.offsetSill.getValue();
        Measure<Length> removedValue = this.offsetSill.removeValue();
        Measure<Length> currentValue = this.offsetSill.getValue();

        fireOffsetSillChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultOffsetSill(Measure<Length> offsetSill)
    {
        offsetSill = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(offsetSill, getDefaultUnitFor(PROPERTYNAME_OFFSETSILL,Length.class));

        Measure<Length> oldValue = this.offsetSill.getValue();
        this.offsetSill.setDefaultValue(offsetSill);

        Measure<Length> currentValue = this.offsetSill.getValue();

        fireOffsetSillChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultOffsetSill()
    {
        Measure<Length> oldValue = this.offsetSill.getValue();
        Measure<Length> removedValue = this.offsetSill.removeDefaultValue();
        Measure<Length> currentValue = this.offsetSill.getValue();

        fireOffsetSillChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getOffsetSillValueContainer()
    {
        return this.offsetSill;
    }
    public final void fireOffsetSillChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeOffsetSillChanged(currentValue);
            firePropertyChange(PROPERTYNAME_OFFSETSILL, oldValue, currentValue);
            afterOffsetSillChanged(currentValue);
        }
    }

    public void beforeOffsetSillChanged( Measure<Length> offsetSill)
    { }
    public void afterOffsetSillChanged( Measure<Length> offsetSill)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Perimeter left")
        ,@LocalizedTag(language="fr", name="label", value="P�rim�tre gauche")
    })
    public final Measure<Length> getPerimeterLeft()
    {
         return this.perimeterLeft.getValue();
    }

    public final void setPerimeterLeft(Measure<Length> perimeterLeft)
    {

        perimeterLeft = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(perimeterLeft, getDefaultUnitFor(PROPERTYNAME_PERIMETERLEFT,Length.class));

        Measure<Length> oldValue = this.perimeterLeft.getValue();
        this.perimeterLeft.setValue(perimeterLeft);

        Measure<Length> currentValue = this.perimeterLeft.getValue();

        firePerimeterLeftChange(currentValue, oldValue);
    }
    public final Measure<Length> removePerimeterLeft()
    {
        Measure<Length> oldValue = this.perimeterLeft.getValue();
        Measure<Length> removedValue = this.perimeterLeft.removeValue();
        Measure<Length> currentValue = this.perimeterLeft.getValue();

        firePerimeterLeftChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultPerimeterLeft(Measure<Length> perimeterLeft)
    {
        perimeterLeft = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(perimeterLeft, getDefaultUnitFor(PROPERTYNAME_PERIMETERLEFT,Length.class));

        Measure<Length> oldValue = this.perimeterLeft.getValue();
        this.perimeterLeft.setDefaultValue(perimeterLeft);

        Measure<Length> currentValue = this.perimeterLeft.getValue();

        firePerimeterLeftChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultPerimeterLeft()
    {
        Measure<Length> oldValue = this.perimeterLeft.getValue();
        Measure<Length> removedValue = this.perimeterLeft.removeDefaultValue();
        Measure<Length> currentValue = this.perimeterLeft.getValue();

        firePerimeterLeftChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getPerimeterLeftValueContainer()
    {
        return this.perimeterLeft;
    }
    public final void firePerimeterLeftChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforePerimeterLeftChanged(currentValue);
            firePropertyChange(PROPERTYNAME_PERIMETERLEFT, oldValue, currentValue);
            afterPerimeterLeftChanged(currentValue);
        }
    }

    public void beforePerimeterLeftChanged( Measure<Length> perimeterLeft)
    { }
    public void afterPerimeterLeftChanged( Measure<Length> perimeterLeft)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Perimeter right")
        ,@LocalizedTag(language="fr", name="label", value="P�rim�tre droite")
    })
    public final Measure<Length> getPerimeterRight()
    {
         return this.perimeterRight.getValue();
    }

    public final void setPerimeterRight(Measure<Length> perimeterRight)
    {

        perimeterRight = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(perimeterRight, getDefaultUnitFor(PROPERTYNAME_PERIMETERRIGHT,Length.class));

        Measure<Length> oldValue = this.perimeterRight.getValue();
        this.perimeterRight.setValue(perimeterRight);

        Measure<Length> currentValue = this.perimeterRight.getValue();

        firePerimeterRightChange(currentValue, oldValue);
    }
    public final Measure<Length> removePerimeterRight()
    {
        Measure<Length> oldValue = this.perimeterRight.getValue();
        Measure<Length> removedValue = this.perimeterRight.removeValue();
        Measure<Length> currentValue = this.perimeterRight.getValue();

        firePerimeterRightChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultPerimeterRight(Measure<Length> perimeterRight)
    {
        perimeterRight = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(perimeterRight, getDefaultUnitFor(PROPERTYNAME_PERIMETERRIGHT,Length.class));

        Measure<Length> oldValue = this.perimeterRight.getValue();
        this.perimeterRight.setDefaultValue(perimeterRight);

        Measure<Length> currentValue = this.perimeterRight.getValue();

        firePerimeterRightChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultPerimeterRight()
    {
        Measure<Length> oldValue = this.perimeterRight.getValue();
        Measure<Length> removedValue = this.perimeterRight.removeDefaultValue();
        Measure<Length> currentValue = this.perimeterRight.getValue();

        firePerimeterRightChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getPerimeterRightValueContainer()
    {
        return this.perimeterRight;
    }
    public final void firePerimeterRightChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforePerimeterRightChanged(currentValue);
            firePropertyChange(PROPERTYNAME_PERIMETERRIGHT, oldValue, currentValue);
            afterPerimeterRightChanged(currentValue);
        }
    }

    public void beforePerimeterRightChanged( Measure<Length> perimeterRight)
    { }
    public void afterPerimeterRightChanged( Measure<Length> perimeterRight)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Perimeter top")
        ,@LocalizedTag(language="fr", name="label", value="P�rim�tre haut")
    })
    public final Measure<Length> getPerimeterTop()
    {
         return this.perimeterTop.getValue();
    }

    public final void setPerimeterTop(Measure<Length> perimeterTop)
    {

        perimeterTop = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(perimeterTop, getDefaultUnitFor(PROPERTYNAME_PERIMETERTOP,Length.class));

        Measure<Length> oldValue = this.perimeterTop.getValue();
        this.perimeterTop.setValue(perimeterTop);

        Measure<Length> currentValue = this.perimeterTop.getValue();

        firePerimeterTopChange(currentValue, oldValue);
    }
    public final Measure<Length> removePerimeterTop()
    {
        Measure<Length> oldValue = this.perimeterTop.getValue();
        Measure<Length> removedValue = this.perimeterTop.removeValue();
        Measure<Length> currentValue = this.perimeterTop.getValue();

        firePerimeterTopChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultPerimeterTop(Measure<Length> perimeterTop)
    {
        perimeterTop = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(perimeterTop, getDefaultUnitFor(PROPERTYNAME_PERIMETERTOP,Length.class));

        Measure<Length> oldValue = this.perimeterTop.getValue();
        this.perimeterTop.setDefaultValue(perimeterTop);

        Measure<Length> currentValue = this.perimeterTop.getValue();

        firePerimeterTopChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultPerimeterTop()
    {
        Measure<Length> oldValue = this.perimeterTop.getValue();
        Measure<Length> removedValue = this.perimeterTop.removeDefaultValue();
        Measure<Length> currentValue = this.perimeterTop.getValue();

        firePerimeterTopChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getPerimeterTopValueContainer()
    {
        return this.perimeterTop;
    }
    public final void firePerimeterTopChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforePerimeterTopChanged(currentValue);
            firePropertyChange(PROPERTYNAME_PERIMETERTOP, oldValue, currentValue);
            afterPerimeterTopChanged(currentValue);
        }
    }

    public void beforePerimeterTopChanged( Measure<Length> perimeterTop)
    { }
    public void afterPerimeterTopChanged( Measure<Length> perimeterTop)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Perimeter sill")
        ,@LocalizedTag(language="fr", name="label", value="P�rim�tre bas")
    })
    public final Measure<Length> getPerimeterSill()
    {
         return this.perimeterSill.getValue();
    }

    public final void setPerimeterSill(Measure<Length> perimeterSill)
    {

        perimeterSill = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(perimeterSill, getDefaultUnitFor(PROPERTYNAME_PERIMETERSILL,Length.class));

        Measure<Length> oldValue = this.perimeterSill.getValue();
        this.perimeterSill.setValue(perimeterSill);

        Measure<Length> currentValue = this.perimeterSill.getValue();

        firePerimeterSillChange(currentValue, oldValue);
    }
    public final Measure<Length> removePerimeterSill()
    {
        Measure<Length> oldValue = this.perimeterSill.getValue();
        Measure<Length> removedValue = this.perimeterSill.removeValue();
        Measure<Length> currentValue = this.perimeterSill.getValue();

        firePerimeterSillChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultPerimeterSill(Measure<Length> perimeterSill)
    {
        perimeterSill = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(perimeterSill, getDefaultUnitFor(PROPERTYNAME_PERIMETERSILL,Length.class));

        Measure<Length> oldValue = this.perimeterSill.getValue();
        this.perimeterSill.setDefaultValue(perimeterSill);

        Measure<Length> currentValue = this.perimeterSill.getValue();

        firePerimeterSillChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultPerimeterSill()
    {
        Measure<Length> oldValue = this.perimeterSill.getValue();
        Measure<Length> removedValue = this.perimeterSill.removeDefaultValue();
        Measure<Length> currentValue = this.perimeterSill.getValue();

        firePerimeterSillChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getPerimeterSillValueContainer()
    {
        return this.perimeterSill;
    }
    public final void firePerimeterSillChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforePerimeterSillChanged(currentValue);
            firePropertyChange(PROPERTYNAME_PERIMETERSILL, oldValue, currentValue);
            afterPerimeterSillChanged(currentValue);
        }
    }

    public void beforePerimeterSillChanged( Measure<Length> perimeterSill)
    { }
    public void afterPerimeterSillChanged( Measure<Length> perimeterSill)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Byzance top")
        ,@LocalizedTag(language="fr", name="label", value="Byzanc� haut")
    })
    public final Measure<Length> getByzanceTop()
    {
         return this.byzanceTop.getValue();
    }

    public final void setByzanceTop(Measure<Length> byzanceTop)
    {

        byzanceTop = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(byzanceTop, getDefaultUnitFor(PROPERTYNAME_BYZANCETOP,Length.class));

        Measure<Length> oldValue = this.byzanceTop.getValue();
        this.byzanceTop.setValue(byzanceTop);

        Measure<Length> currentValue = this.byzanceTop.getValue();

        fireByzanceTopChange(currentValue, oldValue);
    }
    public final Measure<Length> removeByzanceTop()
    {
        Measure<Length> oldValue = this.byzanceTop.getValue();
        Measure<Length> removedValue = this.byzanceTop.removeValue();
        Measure<Length> currentValue = this.byzanceTop.getValue();

        fireByzanceTopChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultByzanceTop(Measure<Length> byzanceTop)
    {
        byzanceTop = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(byzanceTop, getDefaultUnitFor(PROPERTYNAME_BYZANCETOP,Length.class));

        Measure<Length> oldValue = this.byzanceTop.getValue();
        this.byzanceTop.setDefaultValue(byzanceTop);

        Measure<Length> currentValue = this.byzanceTop.getValue();

        fireByzanceTopChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultByzanceTop()
    {
        Measure<Length> oldValue = this.byzanceTop.getValue();
        Measure<Length> removedValue = this.byzanceTop.removeDefaultValue();
        Measure<Length> currentValue = this.byzanceTop.getValue();

        fireByzanceTopChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getByzanceTopValueContainer()
    {
        return this.byzanceTop;
    }
    public final void fireByzanceTopChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeByzanceTopChanged(currentValue);
            firePropertyChange(PROPERTYNAME_BYZANCETOP, oldValue, currentValue);
            afterByzanceTopChanged(currentValue);
        }
    }

    public void beforeByzanceTopChanged( Measure<Length> byzanceTop)
    { }
    public void afterByzanceTopChanged( Measure<Length> byzanceTop)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Byzance sill")
        ,@LocalizedTag(language="fr", name="label", value="Byzanc� bas")
    })
    public final Measure<Length> getByzanceSill()
    {
         return this.byzanceSill.getValue();
    }

    public final void setByzanceSill(Measure<Length> byzanceSill)
    {

        byzanceSill = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(byzanceSill, getDefaultUnitFor(PROPERTYNAME_BYZANCESILL,Length.class));

        Measure<Length> oldValue = this.byzanceSill.getValue();
        this.byzanceSill.setValue(byzanceSill);

        Measure<Length> currentValue = this.byzanceSill.getValue();

        fireByzanceSillChange(currentValue, oldValue);
    }
    public final Measure<Length> removeByzanceSill()
    {
        Measure<Length> oldValue = this.byzanceSill.getValue();
        Measure<Length> removedValue = this.byzanceSill.removeValue();
        Measure<Length> currentValue = this.byzanceSill.getValue();

        fireByzanceSillChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultByzanceSill(Measure<Length> byzanceSill)
    {
        byzanceSill = (Measure<Length>)com.netappsid.commonutils.utils.MeasureUtils.toUnit(byzanceSill, getDefaultUnitFor(PROPERTYNAME_BYZANCESILL,Length.class));

        Measure<Length> oldValue = this.byzanceSill.getValue();
        this.byzanceSill.setDefaultValue(byzanceSill);

        Measure<Length> currentValue = this.byzanceSill.getValue();

        fireByzanceSillChange(currentValue, oldValue);
    }
    
    public final Measure<Length> removeDefaultByzanceSill()
    {
        Measure<Length> oldValue = this.byzanceSill.getValue();
        Measure<Length> removedValue = this.byzanceSill.removeDefaultValue();
        Measure<Length> currentValue = this.byzanceSill.getValue();

        fireByzanceSillChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<Measure<Length>> getByzanceSillValueContainer()
    {
        return this.byzanceSill;
    }
    public final void fireByzanceSillChange(Measure<Length> currentValue, Measure<Length> oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforeByzanceSillChanged(currentValue);
            firePropertyChange(PROPERTYNAME_BYZANCESILL, oldValue, currentValue);
            afterByzanceSillChanged(currentValue);
        }
    }

    public void beforeByzanceSillChanged( Measure<Length> byzanceSill)
    { }
    public void afterByzanceSillChanged( Measure<Length> byzanceSill)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Perimeter corners")
        ,@LocalizedTag(language="fr", name="label", value="Coins du p�rim�tre")
    })
    public final PanelModelCornerTypes getPanelModelCornerTypes()
    {
         return this.panelModelCornerTypes.getValue();
    }

    public final void setPanelModelCornerTypes(PanelModelCornerTypes panelModelCornerTypes)
    {

        PanelModelCornerTypes oldValue = this.panelModelCornerTypes.getValue();
        this.panelModelCornerTypes.setValue(panelModelCornerTypes);

        PanelModelCornerTypes currentValue = this.panelModelCornerTypes.getValue();

        firePanelModelCornerTypesChange(currentValue, oldValue);
    }
    public final PanelModelCornerTypes removePanelModelCornerTypes()
    {
        PanelModelCornerTypes oldValue = this.panelModelCornerTypes.getValue();
        PanelModelCornerTypes removedValue = this.panelModelCornerTypes.removeValue();
        PanelModelCornerTypes currentValue = this.panelModelCornerTypes.getValue();

        firePanelModelCornerTypesChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultPanelModelCornerTypes(PanelModelCornerTypes panelModelCornerTypes)
    {
        PanelModelCornerTypes oldValue = this.panelModelCornerTypes.getValue();
        this.panelModelCornerTypes.setDefaultValue(panelModelCornerTypes);

        PanelModelCornerTypes currentValue = this.panelModelCornerTypes.getValue();

        firePanelModelCornerTypesChange(currentValue, oldValue);
    }
    
    public final PanelModelCornerTypes removeDefaultPanelModelCornerTypes()
    {
        PanelModelCornerTypes oldValue = this.panelModelCornerTypes.getValue();
        PanelModelCornerTypes removedValue = this.panelModelCornerTypes.removeDefaultValue();
        PanelModelCornerTypes currentValue = this.panelModelCornerTypes.getValue();

        firePanelModelCornerTypesChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<PanelModelCornerTypes> getPanelModelCornerTypesValueContainer()
    {
        return this.panelModelCornerTypes;
    }
    public final void firePanelModelCornerTypesChange(PanelModelCornerTypes currentValue, PanelModelCornerTypes oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforePanelModelCornerTypesChanged(currentValue);
            firePropertyChange(PROPERTYNAME_PANELMODELCORNERTYPES, oldValue, currentValue);
            afterPanelModelCornerTypesChanged(currentValue);
        }
    }

    public void beforePanelModelCornerTypesChanged( PanelModelCornerTypes panelModelCornerTypes)
    { }
    public void afterPanelModelCornerTypesChanged( PanelModelCornerTypes panelModelCornerTypes)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Perimeter profile")
        ,@LocalizedTag(language="fr", name="label", value="Profil du p�rim�tre")
    })
    public final HingeShutterPanelProfiles getPerimeterProfile()
    {
         return this.perimeterProfile.getValue();
    }

    public final void setPerimeterProfile(HingeShutterPanelProfiles perimeterProfile)
    {

        HingeShutterPanelProfiles oldValue = this.perimeterProfile.getValue();
        this.perimeterProfile.setValue(perimeterProfile);

        HingeShutterPanelProfiles currentValue = this.perimeterProfile.getValue();

        firePerimeterProfileChange(currentValue, oldValue);
    }
    public final HingeShutterPanelProfiles removePerimeterProfile()
    {
        HingeShutterPanelProfiles oldValue = this.perimeterProfile.getValue();
        HingeShutterPanelProfiles removedValue = this.perimeterProfile.removeValue();
        HingeShutterPanelProfiles currentValue = this.perimeterProfile.getValue();

        firePerimeterProfileChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultPerimeterProfile(HingeShutterPanelProfiles perimeterProfile)
    {
        HingeShutterPanelProfiles oldValue = this.perimeterProfile.getValue();
        this.perimeterProfile.setDefaultValue(perimeterProfile);

        HingeShutterPanelProfiles currentValue = this.perimeterProfile.getValue();

        firePerimeterProfileChange(currentValue, oldValue);
    }
    
    public final HingeShutterPanelProfiles removeDefaultPerimeterProfile()
    {
        HingeShutterPanelProfiles oldValue = this.perimeterProfile.getValue();
        HingeShutterPanelProfiles removedValue = this.perimeterProfile.removeDefaultValue();
        HingeShutterPanelProfiles currentValue = this.perimeterProfile.getValue();

        firePerimeterProfileChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<HingeShutterPanelProfiles> getPerimeterProfileValueContainer()
    {
        return this.perimeterProfile;
    }
    public final void firePerimeterProfileChange(HingeShutterPanelProfiles currentValue, HingeShutterPanelProfiles oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforePerimeterProfileChanged(currentValue);
            firePropertyChange(PROPERTYNAME_PERIMETERPROFILE, oldValue, currentValue);
            afterPerimeterProfileChanged(currentValue);
        }
    }

    public void beforePerimeterProfileChanged( HingeShutterPanelProfiles perimeterProfile)
    { }
    public void afterPerimeterProfileChanged( HingeShutterPanelProfiles perimeterProfile)
    { }

    @LocalizedTags
    ({
        @LocalizedTag(language="en", name="label", value="Perimeter color")
        ,@LocalizedTag(language="fr", name="label", value="Couleur du p�rim�tre")
    })
    public final com.client360.configuration.wad.StandardColor getPerimeterColor()
    {
         return this.perimeterColor.getValue();
    }

    public final void setPerimeterColor(com.client360.configuration.wad.StandardColor perimeterColor)
    {

        if (perimeterColor != null)
        {
            acquire(perimeterColor, "perimeterColor");
        }
        com.client360.configuration.wad.StandardColor oldValue = this.perimeterColor.getValue();
        this.perimeterColor.setValue(perimeterColor);

        com.client360.configuration.wad.StandardColor currentValue = this.perimeterColor.getValue();

        firePerimeterColorChange(currentValue, oldValue);
    }
    public final com.client360.configuration.wad.StandardColor removePerimeterColor()
    {
        com.client360.configuration.wad.StandardColor oldValue = this.perimeterColor.getValue();
        com.client360.configuration.wad.StandardColor removedValue = this.perimeterColor.removeValue();
        com.client360.configuration.wad.StandardColor currentValue = this.perimeterColor.getValue();

        firePerimeterColorChange(currentValue, oldValue);
        return removedValue;
    }
    
    public final void setDefaultPerimeterColor(com.client360.configuration.wad.StandardColor perimeterColor)
    {
        if (perimeterColor != null)
        {
            acquire(perimeterColor, "perimeterColor");
        }
        com.client360.configuration.wad.StandardColor oldValue = this.perimeterColor.getValue();
        this.perimeterColor.setDefaultValue(perimeterColor);

        com.client360.configuration.wad.StandardColor currentValue = this.perimeterColor.getValue();

        firePerimeterColorChange(currentValue, oldValue);
    }
    
    public final com.client360.configuration.wad.StandardColor removeDefaultPerimeterColor()
    {
        com.client360.configuration.wad.StandardColor oldValue = this.perimeterColor.getValue();
        com.client360.configuration.wad.StandardColor removedValue = this.perimeterColor.removeDefaultValue();
        com.client360.configuration.wad.StandardColor currentValue = this.perimeterColor.getValue();

        firePerimeterColorChange(currentValue, oldValue);
        return removedValue;
    }
    public final ValueContainerGetter<com.client360.configuration.wad.StandardColor> getPerimeterColorValueContainer()
    {
        return this.perimeterColor;
    }
    public final void firePerimeterColorChange(com.client360.configuration.wad.StandardColor currentValue, com.client360.configuration.wad.StandardColor oldValue)
    {
        if (!org.apache.commons.lang.ObjectUtils.equals(currentValue, oldValue))
        {
            beforePerimeterColorChanged(currentValue);
            firePropertyChange(PROPERTYNAME_PERIMETERCOLOR, oldValue, currentValue);
            afterPerimeterColorChanged(currentValue);
        }
    }

    public void beforePerimeterColorChanged( com.client360.configuration.wad.StandardColor perimeterColor)
    { }
    public void afterPerimeterColorChanged( com.client360.configuration.wad.StandardColor perimeterColor)
    { }



    @Override
    protected String getSequences()
    {
        return "form,offsetLeft,offsetRight,offsetTop,offsetSill,perimeterLeft,perimeterRight,perimeterTop,perimeterSill,byzanceTop,byzanceSill,perimeterProfile,perimeterColor,panelModelCornerTypes,not set";  
    }
}
