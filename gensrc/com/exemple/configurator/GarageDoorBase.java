// generated class, do not edit

package com.exemple.configurator;

//Plugin V13.1.4
//2014-01-29

// Imports

public abstract class GarageDoorBase extends com.netappsid.wadconfigurator.GarageDoor
{
    // Log4J logger
    protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(GarageDoorBase.class);

    // attributes

    // Constructors

    public GarageDoorBase(com.netappsid.erp.configurator.Configurable parent) // GENCODE b298fff0-4bff-11e0-b8af-0800200c9a66
    {
         super(parent);
        if (this.getClass().getName().equals("com.exemple.configurator.GarageDoor"))
        {
             // activate listener before setting the default value to listen the changes
             activateListener();

             // Load the default values dynamically
             setDefaultValues();

             // load 'collection' preferences and values coming from prior items in the order
             applyPreferences();
        }
        
    }

    // Operations


    // Business methods



    @Override
    public void setDefaultValues()
    {
        super.setDefaultValues();
    }

    // Bound properties

}
